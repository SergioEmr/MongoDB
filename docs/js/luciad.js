var Luciad = (function() {
  var components = ['ria'];

  return {
    addComponent: function(componentName) {
      components.push(componentName);
    },

    hasComponent: function(componentName) {
      for (var i = 0; i < components.length; i++) {
        if (components[i] == componentName) {
          return true;
        }
      }
      return false;
    }
  };

})();

/**
 * This script:
 *
 * 1) Checks if LuciadRIA server is running
 * 2) Removes all list items that don't have the required component
 * 3) Disables all links inside a div that don't have the required component
 * 4) Disables all sample links if server is not running
 **/
$(function() {

  $.loadImage = function(url) {
    var loadImage = function(deferred) {
      var image = new Image();

      image.onload = loaded;
      image.onerror = errored;
      image.onabort = errored;

      function loaded() {
        unbindEvents();
        deferred.resolve(image);
      }

      function errored() {
        unbindEvents();
        deferred.reject(image);
      }

      function unbindEvents() {
        // Ensures the event callbacks only get called once.
        image.onload = null;
        image.onerror = null;
        image.onabort = null;
      }

      image.src = "http://" + url + "/docs/assets/logo_luciad.jpg?t=" + (new Date()).getTime();
    };

    return $.Deferred(loadImage).promise();
  };

  function checkServerAvailable(postProcess) {
    if (document.location.protocol === "file:") {
      $.when($.loadImage("localhost:8072"))
          .done(function() {
            postProcess(true, "localhost:8072");
          })
          .fail(function() {
            postProcess(false, "");
          });
    } else {
      $.when($.loadImage(document.location.host)).done(
          function() {
            postProcess(true, document.location.host);
          })
          .fail(
              function() {
                postProcess(false, "");
              }
          );
    }

    setTimeout(function(){
      checkServerAvailable(postProcess);
    }, 10000);
  }

  function updateSampleLinks(serverRunning, serverPath) {
    //Samples
    var allSampleDivs = $("div[data-component]");
    var allSampleLinks = allSampleDivs.find("a");
    //Update all links
    allSampleLinks.each(function() {
      this.href = this.href.replace("server_link", serverPath);
    });
    if (serverRunning) {
      //Server running. Check and disable all samples without available components.
      allSampleDivs.each(function(index, currentSampleDiv) {
        var currentSampleDivJQuery = $(currentSampleDiv);
        var sampleComponentName = currentSampleDivJQuery.attr("data-component");
        if (!Luciad.hasComponent(sampleComponentName)) {
          var currentSampleLinks = currentSampleDivJQuery.find("a");
          currentSampleLinks.addClass("disabled");
          currentSampleLinks.css("cursor", "not-allowed");
          currentSampleLinks.css("pointer-events", "none");
          currentSampleDivJQuery.css("cursor", "not-allowed");
          currentSampleDivJQuery.tooltip({
            title: "You do not have the " + sampleComponentName + " optional component. This sample has been disabled.",
            container: "body"
          });
        }
      });
    } else {
      //No server running. Disable all sample buttons.
      allSampleLinks.addClass('disabled');
      allSampleLinks.css("cursor", "not-allowed");
      allSampleLinks.css("pointer-events", "none");
      allSampleDivs.css("cursor", "not-allowed");
      allSampleDivs.tooltip({
        title: "Start the sample server (startSampleServer.bat) and refresh this page to run the samples.",
        container: "body"
      });
    }
  }

  checkServerAvailable(updateSampleLinks);

  //If we find any list items that have an unsupported data-component tag, remove them from the list
  var allDataComponentListItems = $("li[data-component]");
  allDataComponentListItems.each(function(index, currentDataComponentListItem) {
    var currentDataComponentListItemJQuery = $(currentDataComponentListItem);
    var requiredComponentName = currentDataComponentListItemJQuery.attr("data-component");
    if (!Luciad.hasComponent(requiredComponentName)) {
      currentDataComponentListItemJQuery.remove();
    }
  });
});

