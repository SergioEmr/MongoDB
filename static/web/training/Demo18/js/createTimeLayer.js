/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define([
    "luciad/shape/ShapeFactory",
    "luciad/model/feature/Feature",
    "luciad/reference/ReferenceProvider",
    "luciad/model/store/MemoryStore",
    "luciad/model/feature/FeatureModel",
    "luciad/view/feature/FeatureLayer",
    "./painters/layerPainter"
], function (ShapeFactory, Feature, ReferenceProvider, MemoryStore, FeatureModel, 
    FeatureLayer, layerPainter) {
    var baseDate = "20AA/mes/dia hora:00";
    var reference = ReferenceProvider.getReference("CRS:84");
    
    function createFeatures(data) {
        var properties=[], propertie, tipo, actual, i, j, k=0, n, m;
        var features = [], feature, keys=[], longitud, latitud;
        for(var key in data) {
            actual = data[key];
            i=0;
            for(var key2 in actual) {
                if(key2 === "TipoDelito")
                    tipo = actual[key2];
                else {
                    properties[i] = actual[key2];
                    keys[i] = key2;
                    i++;
                }
            }
            n = properties.length;
            var p ={};
            propertie = properties[0];
            m = propertie.length;
            for(j=0; j<m;j++ ) {
                
                for(i=0;i<n;i++){
                    p[keys[i]] = properties[i][j];
                    if(keys[i] === "Lat")
                        latitud = properties[i][j];
                    if(keys[i] === "Lon")
                        longitud = properties[i][j];
                    
                }
                p.TipoDelito = tipo;
                if(p.MES) {
                    
                    p.EventTime = baseDate.replace("AA", A).replace("mes", getMes(p.MES)).replace("dia", p.DIANUM).replace("hora", p.HLLEGADA);
                } else 
                    p.EventTime = baseDate.replace("AA", A).replace("mes", "01").replace("dia", p.DIANUM).replace("hora", p.HLLEGADA);
                features[k] = createPoint(longitud, latitud, 0, k, p);
                k++;
                p={};
            }
        }
        return features;
    }
    
    function getMes(mes) {
        switch(mes) {
            case "ENERO": return "01";
            case "FEBRERO": return "02";
            case "MARZO": return "03";
            case "ABRIL": return "04";
            case "MAYO": return "05";
            case "JUNIO": return "06";
            case "JULIO": return "07";
            case "AGOSTO": return "08";
            case "SEPTIEMBRE": return "09";
            case "OCTUBRE": return "10";
            case "NOVIEMBRE": return "11";
            case "DICIEMBRE": return "12";
            default: console.log("No mes: " + mes); return "01";
        }
    }
    
    function createPoint(x, y, z, i, properties) {
        if(properties === undefined)
            properties = {};
        return new Feature(
            ShapeFactory.createPoint(reference, [x, y, z]), properties, i
        );
    }
    
    function createLayer(data) {
        var features = createFeatures(data);
        var store = new MemoryStore({data: features});
        var featureModel = new FeatureModel(store, {reference: reference});
        var layer = new FeatureLayer(featureModel, {
            label: "Eventos", 
            id:"eventos", 
            selectable: true, 
            editable: false, 
            painter: new layerPainter()});
        return layer;
    }
    
    function createTimeFeature(features, A) {
        var f = [];
        for(var i=0; i<features.length; i++) {
            var p = features[i].properties;
            var longitud = features[i].geometry.coordinates[0], latitud = features[i].geometry.coordinates[1];
            var hora = p.HORA;
            if(!hora)
                hora = "00:00";
            var fecha = p.FECHA;
            var eventTime = fecha+" "+hora;
            p.EventTime = eventTime;
            f[i] = createPoint(longitud, latitud, 0, i, p);
        }
        return f;
    }
    
    return {createLayer: createLayer,
        createFeatures: createFeatures,
        createTimeFeature: createTimeFeature
    };
});

