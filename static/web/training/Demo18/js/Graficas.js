/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define([
    "luciad/shape/ShapeFactory",
    "luciad/reference/ReferenceProvider",
    "luciad/model/feature/Feature"
], function (ShapeFactory, ReferenceProvider, Feature) {
    
    var graficas = new Array(), datoGrafica ={label: new Array(), type: new Array()}, idGrafica=-1, y=0;
    /*
     * 
     * @param {String} div
     * @param {Array[][]} datos
     * @param {String} tipo
     * @param {String} titulo
     * @returns {unresolved}
     */
    function dibujarGraficaArreglo(div, datos, tipo, titulo) {
        document.getElementById(div).innerHTML="";
        switch(tipo.toLowerCase()) {
            case "piechart":
                PieChart(div, datos, titulo);
                break;
            case "columnchart":
                ColumnChart(div, datos, titulo);
                break;
            case "areachart":
                AreaChart(div, datos);
                break;
            default: console.log("Tipo de Grafica desconocida "+ tipo);return null;
        }
        
    }
    /*
     * 
     * @param {String} div
     * @param {Array[][]} datos
     * @param {String} titulo
     * @returns {undefined}
     */
    function ColumnChart(div, datos, titulo) {
        var chart;
        try{
            //google.charts.load('current', {'packages':['line']});
            google.charts.load('current', {packages: ['corechart', 'bar']});
            google.charts.setOnLoadCallback(drawColumnChart);
            
        }catch(e){
            console.log("Error al crear Grafica de columna\n");
            console.log(e);
        }
        function drawColumnChart() {
            chart = new google.visualization.ColumnChart(document.getElementById(div));
            var datosNuevos = new google.visualization.arrayToDataTable(datos);
            var options= getColumnOptions();
            options.title = titulo;
            chart.draw(datosNuevos, options);
            graficas[y] = chart;
            y++;
        }
    }
    /*
     * 
     * @param {String} div
     * @param {Array[][]} datos
     * @param {String} titulo
     * @returns {unresolved}
     */
    function PieChart(div, datos, titulo){
        var chart;
        try{
            //google.charts.load('current', {'packages':['line']});
            google.charts.load('current', {'packages':['corechart']});
            return google.charts.setOnLoadCallback(drawPieChart);
        }catch(e){
            console.log("Error al crear Grafica de lineas\n");
            console.log(e);
        }
        function drawPieChart() {
            chart = new google.visualization.PieChart(document.getElementById(div));
            var datosNuevos = new google.visualization.arrayToDataTable(datos);
            var options= getPieOptions();
            var colorsSlices = getColors(datos.length);
            //options.slices = colorsSlices;
            //console.log(colorsSlices);
            options.title = titulo;
            chart.draw(datosNuevos, options);
            google.visualization.events.addListener(chart, 'select', function() {
                try{
                var x = chart.getSelection(), row = x[0].row, col = 0;
                var dato1 = datosNuevos.getValue(row, 0);
                
                filtrarMunicipio(dato1);
            }catch(e) {
                console.log(e);
            }
            });
            graficas[y] = chart;
            y++;
        }
    }
    /*
     * 
     * @param {String} div
     * @param {Array[][]} datos
     * @returns {unresolved}
     */
    function AreaChart(div, datos){
        var chart;
        try{
            //google.charts.load('current', {'packages':['line']});
            google.charts.load('current', {'packages':['corechart']});
            return google.charts.setOnLoadCallback(drawAreaChart);
        }catch(e){
            console.log("Error al crear Grafica de lineas\n");
            console.log(e);
        }
        function drawAreaChart() {
            chart = new google.visualization.AreaChart(document.getElementById(div));
            var datosNuevos = new google.visualization.arrayToDataTable(datos);
            var options= getAreaOptions();
            chart.draw(datosNuevos, options);
            google.visualization.events.addListener(chart, 'select', function() {
                try{
                var x = chart.getSelection(), row = x[0].row, col = 0;
                var dato1 = datosNuevos.getValue(row, 0);
                
                filtrarTexto(dato1);
            }catch(e) {
                console.log(e);
            }
            });
            graficas[y] = chart;
            y++;
        }
    }
    /*
     * 
     * @param {Object} features
     * @returns {Array[][]}
     */
    function crearArreglo(features, div) {
        var tabla = new Array(), encavezados = new Array(), valores = new Array();
        var key, i=0, j=0, k=0, x, coordenadas;
        for(key in features[0].properties) {
            encavezados[i] = key;
            i++;
        }
        encavezados[i] = "Longitud";
        encavezados[i+1] = "Latitud";
        tabla[j] = encavezados;
        for(i=0; i<features.length; i++) {
            for(key in features[i].properties) {
                try {
                    x = parseFloat(features[i].properties[key]);
                    if(x)
                        valores[k] = x;
                    else
                        valores[k] = features[i].properties[key];
                }catch (e) {
                    
                }
                k++;
            }
            coordenadas = features[i].geometry.coordinates;
            if(coordenadas.length === 3) {
                valores[k] = coordenadas[0];
                valores[k+1] = coordenadas[1];
            } else {
                valores[k] = coordenadas[0].x;
                valores[k+1] = coordenadas[0].y;
            }
            j++;
            tabla[j]=valores;
            valores = new Array();
            k=0;
        }
        if(div) {
            var etiqueta = //block, flex
                '<div class="labelDatos" style="display:block !important">' +
                '<table>'+
                '$tabla' +
                '</table>' +
                '</div>';
            var columna = '<td align="center" ><b>$dato</b></td >';
            var columnas ='';
            var fila = '<tr bgcolor="$COLOR">$columna</tr>' ;
            var filas= '', etiquetaTabla ='', n=tabla.length , m = tabla[0].length;
    
            var datos, color;
            for(i=0; i<n; i++) {
                datos = tabla[i];
                for(j=0;j<m;j++) {
                    
                    columnas += columna.replace('$dato', datos[j]);
                }
                if(i ===0) {
                    color = '';
                } else {
                    if(i%2 === 0 )
                        color = '#5d6d7e';
                    else
                        color = '';
                }
                columna = '<td align="center" >$dato</td >';
                filas += fila.replace('$columna', columnas).replace('$COLOR', color);
                etiquetaTabla += filas;
                columnas='';
                filas ='';
            }
            etiqueta = etiqueta.replace('$tabla', etiquetaTabla);
            document.getElementById(div).innerHTML=etiqueta;
        }
        return tabla;
    }
    
    /*
     * 
     * @param {String} div
     * @param {Array[][]} tablaDatos
     * @param {String} chartType
     * @param {String} title
     * @param {int} id
     * @returns {Number}
     */
    function crearGraficaTop(div, tablaDatos, chartType, label, max) {
        idGrafica++;
        console.log("Creando Grafica Top "+label+" id Grafica"+idGrafica);
        var datos = datosChartTexto(tablaDatos, label, max);
        dibujarGraficaArreglo(div, datos.tabla, chartType, label);
        datoGrafica.label[idGrafica] = label;
        datoGrafica.type[idGrafica] = chartType;
        datos.idGrafica = idGrafica;
        return datos;
    }
    /*
     * 
     * @param {String} div
     * @param {Array[][]} tablaDatos
     * @param {String} chartType
     * @param {String} title
     * @param {int} id
     * @returns {Number}
     */
    function crearGraficaIdTexto(div, tablaDatos, chartType, label) {
        idGrafica++;
        console.log("Creando Grafica "+label+" id Grafica"+idGrafica);
        var datos = datosChartTexto(tablaDatos, label);
        dibujarGraficaArreglo(div, datos.tabla, chartType, label);
        datoGrafica.label[idGrafica] = label;
        datoGrafica.type[idGrafica] = chartType;
        datos.idGrafica = idGrafica;
        return datos;
    }
    /*
     * 
     * @param {String} div
     * @param {Array[][]} tablaDatos
     * @param {String} chartType
     * @param {String} title
     * @param {int} idLabel
     * @param {int} idValor
     * @returns {Number}
     */
    function crearGraficaLabelValor(div, tablaDatos, chartType, title, idLabel, idValor) {
        idGrafica++;
        console.log("Creando Grafica "+title+" id Grafica"+idGrafica);
        var datos = datosChartLabelValor(tablaDatos, idLabel, idValor, title);
        dibujarGraficaArreglo(div, datos.tabla, chartType, title);
        return idGrafica;
    }
    /*
     * 
     * @param {Array[][]} tablaDatos
     * @param {int} id
     * @param {String} label
     * @returns {Array}
     */
    function datosChartTexto(tablaDatos, label, max) {
        var tabla = [[label, "number"]];
        var tipoDato = new Array(), numeroDatos = new Array(), valor, nuevoDato=false;
        var i=0, j=0, k;
        valor = tablaDatos[0];
        for(k=0;k<valor.length;k++) {
            if(valor[k] === label)
                break;
        }
        for(i=1; i<tablaDatos.length; i++) {
            
            valor = tablaDatos[i][k];
            if(tipoDato.length===0) {
                tipoDato[0] = valor;
                numeroDatos[0] = 1;
            }
            else {
                for(j=0;j<tipoDato.length;j++) {
                    if(valor === tipoDato[j]) {
                        numeroDatos[j]++;
                        nuevoDato = false;
                        break;
                    }
                    else
                        nuevoDato =true;
                }
                if(nuevoDato){
                    tipoDato[j] = valor;
                    numeroDatos[j] = 1;
                }
            }
        }
        var ordenado = ordenarMayorMenor(numeroDatos, tipoDato);
        tipoDato = quitarAcentos(ordenado.nombres);
        numeroDatos = ordenado.valores;
        var z = tipoDato.length;
        if(max) {
            if(z>max)
                z = max;
        } else {
            if(z>30)
                z = 30;
                
        }
        for(i=0; i<z; i++) {
             tabla[i+1] = [""+tipoDato[i], numeroDatos[i]];
        }
        var otros =0;
        for(i=i; i<tipoDato.length; i++) {
            if((i-z) > 10)
                break;
            otros += numeroDatos[i];
        }
        tabla[z] = ["Otros", otros];
        return {
            tabla: tabla,
            nombres: tipoDato,
            valores: numeroDatos
        };
    }
    
    /*
     * Se encarga de preparar los datos y enviarlos a la funcion para crear la grafica.
     * @param {int} idGrafica
     * @param {Array[][]} tablaDatos
     * @param {String} typeChart
     * @param {String} title
     * @param {int} id
     * @returns {undefined}
     */
    function actualizarGraficaIdTexto(idGrafica, tablaDatos) {
        var datos = datosChartTexto(tablaDatos, datoGrafica.label[idGrafica]).tabla;
        if(graficas[idGrafica])
            updateChart(graficas[idGrafica], datos, datoGrafica.type[idGrafica]);
        //else 
            //console.log("Grafica no actualizada");
    }
    /*
     * 
     * @param {int} idGrafica
     * @param {Array[][]} tablaDatos
     * @param {String} typeChart
     * @param {String} title
     * @param {int} idLabel
     * @param {int} idValor
     * @returns {undefined}
     */
    function actualizarGraficaLabelValor(idGrafica, tablaDatos, typeChart, title, idLabel, idValor) {
        var datos = datosChartLabelValor(tablaDatos, idLabel, idValor, title);
        updateChart(graficas[idGrafica], datos, typeChart);
    }
    /*
     * 
     * @param {Object} chart
     * @param {Array[][]} newData
     * @param {String} chartType
     * @returns {undefined}
     */
    function updateChart(chart, newData, chartType) {
        var newOptions;
        switch(chartType) {
            case "piechart": 
                newOptions = getPieOptions();
                break;
            case "columnchart":
                newOptions = getColumnOptions();
                break;
            case "areachart":
                newOptions = getAreaOptions();
                break;
        }
        var table = new google.visualization.arrayToDataTable(newData);
        chart.draw(table, newOptions);
    }
    
    /*
     * Se encarga de leer los datos de la columna dada por id de la tabla tablaDatos. 
     * Y regresar un Arreglo de datos listos para crear la grafica.
     * @param {Array[][]} tablaDatos
     * @param {int} id
     * @param {String} label
     * @returns {Array}
     */
    function datosChart(tablaDatos, id, label) {
        var datos = [[label, "number"]], dato;
        var tipoDato = new Array(), numeroDatos = new Array(), nuevoDato=false;
        var i, j;
        
        for(i=1; i<tablaDatos.length; i++) {
            dato = tablaDatos[i][id];
            if(tipoDato.length===0) {
                tipoDato[0] = dato;
                numeroDatos[0] = 1;
            }
            else {
                for(j=0;j<tipoDato.length;j++) {
                    if(dato === tipoDato[j]) {
                        numeroDatos[j]++;
                        nuevoDato = false;
                        break;
                    }
                    else
                        nuevoDato =true;
                }
                if(nuevoDato){
                    tipoDato[j] = dato;
                    numeroDatos[j] = 1;
                }
            }
        }
        var ordenado = ordenarMayorMenor(numeroDatos, tipoDato);
        tipoDato = ordenado.nombres;
        numeroDatos = ordenado.valores;
        var z = tipoDato.length;
        if(z>50)
            z = 50;
        for(i=0; i<z; i++) {
            datos[i+1] = [tipoDato[i], numeroDatos[i]];
        }
        return datos;
    }
    /*
     * 
     * @param {Array[][]} tablaDatos
     * @param {int} idLabel
     * @param {int} idValor
     * @param {String} label
     * @returns {Array}
     */
    function datosChartLabelValor(tablaDatos, idLabel, idValor, label) {
        var datos = [[label, "number"]];
        var tipoDato = new Array(), numeroDatos = new Array(), nombre, valor, nuevoDato=false;
        var i=0, j=0;
        
        for(i=1; i<tablaDatos.length; i++) {
            nombre = tablaDatos[i][idLabel];
            valor = tablaDatos[i][idValor];
            if(tipoDato.length===0) {
                tipoDato[0] = nombre;
                numeroDatos[0] = valor;
            }
            else {
                for(j=0;j<tipoDato.length;j++) {
                    if(nombre === tipoDato[j]) {
                        numeroDatos[j]+= parseFloat(valor);
                        nuevoDato = false;
                        break;
                    }
                    else
                        nuevoDato =true;
                }
                if(nuevoDato){
                    tipoDato[j] = nombre;
                    numeroDatos[j] = valor;
                }
            }
        }
        
        for(i=0; i<tipoDato.length; i++) {
            datos[i+1] = [tipoDato[i], numeroDatos[i]];
        }
        return datos;
    }
    function datosPieChartUbicacion(tablaDatos, id, label, geometry) {
        var cultivos = [[label, "number"]];
        var tipoDato = new Array(), numeroDatos = new Array(), cultivo, nuevoDato=false;
        var lon, lat;
        var i=0, j=0;
        var x = geometry.center.x, y = geometry.center.y;
        var h = geometry.bounds.height, w = geometry.bounds.width;
        var r = h/2;
        var distancia;
        
        for(i=1; i<tablaDatos.length; i++) {
            
            cultivo = tablaDatos[i][id];
            lon = tablaDatos[i][16];
            lat = tablaDatos[i][17];
            distancia = Math.sqrt(Math.pow(x-lon,2) + Math.pow(y-lat,2));
            if(distancia <= r) {
            //if(lon >= minX && lon <= maxX && lat >= minY && lat <= maxY) {
            if(tipoDato.length===0) {
                tipoDato[0] = cultivo;
                numeroDatos[0] = 1;
            }
            else {
                for(j=0;j<tipoDato.length;j++) {
                    if(cultivo === tipoDato[j]) {
                        numeroDatos[j]++;
                        nuevoDato = false;
                        break;
                    }
                    else
                        nuevoDato =true;
                }
                if(nuevoDato){
                    tipoDato[j] = cultivo;
                    numeroDatos[j] = 1;
                }
            }
            }
        }
        
        for(i=0; i<tipoDato.length; i++) {
            cultivos[i+1] = [tipoDato[i], numeroDatos[i]];
        }
        return cultivos;
    }
    
    
    
    
    function datosPieChartLabelValorUbicacion(tablaDatos, idLabel, idValor, label, geometry) {
        var datos = [[label, "number"]];
        var tipoDato = new Array(), numeroDatos = new Array(), nombre, nuevoDato=false, valor;
        var lon, lat;
        var i=0, j=0;
        var x = geometry.center.x, y = geometry.center.y;
        var h = geometry.bounds.height, w = geometry.bounds.width;
        var r = h/2;
        var maxX = x + r, minX = x - r;
        var maxY = y +r, minY = y - r;
        
        for(i=1; i<tablaDatos.length; i++) {
            
            nombre = tablaDatos[i][idLabel];
            valor = tablaDatos[i][idValor];
            lon = tablaDatos[i][16];
            lat = tablaDatos[i][17];
            if(lon >= minX && lon <= maxX && lat >= minY && lat <= maxY) {
            if(tipoDato.length===0) {
                tipoDato[0] = nombre;
                numeroDatos[0] = valor;
            }
            else {
                for(j=0;j<tipoDato.length;j++) {
                    if(nombre === tipoDato[j]) {
                        numeroDatos[j]+= parseFloat(valor);;
                        nuevoDato = false;
                        break;
                    }
                    else
                        nuevoDato =true;
                }
                if(nuevoDato){
                    tipoDato[j] = nombre;
                    numeroDatos[j] = valor;
                }
            }
            }
        }
        
        for(i=0; i<tipoDato.length; i++) {
            datos[i+1] = [tipoDato[i], numeroDatos[i]];
        }
        return datos;
    }
    function getId(label, list) {
        for( var k=0;k<list.length;k++) {
            if(list[k] === label)
                return k;
        }
    }
    function getMes(mes) {
        switch(mes) {
            case "ENERO": return "01";
            case "FEBRERO": return "02";
            case "MARZO": return "03";
            case "ABRIL": return "04";
            case "MAYO": return "05";
            case "JUNIO": return "06";
            case "JULIO": return "07";
            case "AGOSTO": return "08";
            case "SEPTIEMBRE": return "09";
            case "OCTUBRE": return "10";
            case "NOVIEMBRE": return "11";
            case "DICIEMBRE": return "12";
            default: console.log("No mes: " + mes); return "01";
        }
    }
    /*
     * 
     * @param {type} tablaDatos
     * @param {type} timeEventLayer
     * @param {type} mapBounds
     * @param {type} selectedFeature
     * @returns {Array}
     */
    
    function filtrarPorBounds(tablaDatos, timeEventLayer, mapBounds, selectedFeature, year) {
        var tabla = [tablaDatos[0]], i, j=1, lon, lat;
        var baseDate = "aaaa/mes/dia hora:00";
        var zonaFilterNode = document.getElementById( "zonaFiltro" );
        var crimenFilterNode = document.getElementById( "crimenFiltro" );
        var crimen = crimenFilterNode.selectedOptions[0].value;
        var zona = zonaFilterNode.selectedOptions[0].value;
        for(i=1;i<tablaDatos.length;i++) {
            lat = tablaDatos[i][tablaDatos[i].length-1];
            lon = tablaDatos[i][tablaDatos[i].length-2];
            //var mes = getMes(tablaDatos[i][getId("MES", tablaDatos[0])]);
            var fecha = tablaDatos[i][getId("FECHA", tablaDatos[0])], hora = tablaDatos[i][getId("HORA", tablaDatos[0])];
            var eventTime = fecha+" "+hora;
            var point = createPoint(lon, lat, 0, i, {EventTime: eventTime});
            var f = timeEventLayer.shapeProvider.provideShape(point);
            var z = tablaDatos[i][5], c = tablaDatos[i][0];
            if(selectedFeature){
                var cuadrante = obtenerCuadrante(selectedFeature.properties.name);
                if(cuadrante === tablaDatos[i][6]) {
                    tabla[j] = tablaDatos[i];
                    j++;
                }
                /*
                if(mapBounds.contains2D(f) && selectedFeature.geometry.bounds.contains2D(point.shape)) {
                    if((c === crimen || crimen === "Todos") && (z === zona || zona === "Todos")){
                        tabla[j] = tablaDatos[i];
                        j++;
                    }
                    
                }*/
            } else {
                if(mapBounds.contains2D(f)) {
                    if((c === crimen || crimen === "Todos") && (z === zona || zona === "Todos")){
                        tabla[j] = tablaDatos[i];
                        j++;
                    }
                }
            }
        }
        if(tabla.length > 1)
            return tabla;
        else {
            console.log("tabla no actualizada");
            return tablaDatos;
        }
            
    }
    function createPoint(x, y, z, id, properties) {
        var reference = ReferenceProvider.getReference("CRS:84");
        return new Feature(
            ShapeFactory.createPoint(reference, [x, y, z]), properties, id
        );
    }
    
    /*
     * 
     * @param {type} valores
     * @param {type} nombres
     * @returns {GraficasL#11.ordenarMayorMenor.GraficasAnonym$14}
     */
    function ordenarMayorMenor(valores, nombres) {
        var n = valores.length, x, y;
        for(var j=1; j<n; j++) {
            if(valores[j] > valores[j-1]) {
                x = valores[j-1];
                y = nombres[j-1];
                valores[j-1] = valores[j];
                nombres[j-1] = nombres[j];
                valores[j] = x;
                nombres[j] = y;
                j = 0;
            }
        }
        return {
            valores: valores, nombres: nombres
        };
    }
    
    function obtenerCuadrante(name) {
        switch(name) {
            case "PROXPOL 1": return 1;
            case "PROXPOL 2": return 2;
            case "PROXPOL 3": return 3;
            case "PROXPOL 4": return 4;
            case "PROXPOL 5": return 5;
            case "PROXPOL 6": return 6;
            case "PROXPOL 7": return 7;
            case "PROXPOL 8": return 8;
            case "PROXPOL 9": return 9;
            case "PROXPOL 10": return 10;
            case "PROXPOL 11": return 11;
        }
    }
    
    function getColors(n) {
        var slices = {0: {color: ""}}, j=0;
        
        for(var i=0;i<n; i++) {
            if(j>9)
                j=0;
            var color = getGradiantColor(j);
            //var x = {0: {color: ""}};
            slices[i+""] = {color: color};
            //slices = slices.concat(x);
            j++;
        }
        return slices;
    }
    
    function getGradiantColor(j) {
        switch(j) {
            case 0: return "#fdf2e9";
            case 1: return "#fae5d3";
            case 2: return "#f5cba7";
            case 3: return "#f0b27a";
            case 4: return "#eb984e";
            case 5: return "#e67e22";
            case 6: return "#ca6f1e";
            case 7: return "#af601a";
            case 8: return "#935116";
            case 9: return "#784212";
            default: return "#fdfefe";
        }
    }
    
    
    function getPieOptions() {
        return {
        width: 350,
        height: 220,
        titleTextStyle: {
            color: '#FFFFFF',
            frontSize: 30
          },
        legend: {
            position: "left",
          textStyle: {
            color: '#FFFFFF',
            frontSize: 18
          }
        },
        backgroundColor:{
            fill: "#17202a"
        },
        chartArea: {
            left: 60,
            width: 270,
            height: 180,
            backgroundColor: "#17202a"
        },
        pieSliceTextStyle: {
            color: '#FFFFFF'
        }
      };
    }
    function getColumnOptions() {
        return {
        width: 300,
        height: 220,
        orientation: "vertical",
        titleTextStyle: {
            color: '#FFFFFF',
            frontSize: 20
          },
        legend: {
            position: "bottom",
          textStyle: {
            color: '#FFFFFF'
          }
        },
        backgroundColor:{
            fill: "#17202a"
        },
        chartArea: {
            left: 100,
            width: 150,
            height: 230,
            backgroundColor: "#17202a"
        },
        annotations: {
          textStyle: {
            fontSize: 12,
            color: '#FFFFFF',
            auraColor: 'none'
          }
        },
        //axisTitlesPosition: "out",
        hAxis: {
          logScale: false,
          slantedText: false,
          format: 'short',
          textStyle: {
            color: '#FFFFFF',
            frontSize: 10
          },
          titleTextStyle: {
            color: '#FFFFFF',
            frontSize: 16
          }
        },
        vAxis: {
          title: 'Presupuesto',
          
          direction: 1,
          textStyle: {
            color: '#FFFFFF'
          }
          
        }
      };
    }
    function getAreaOptions () {
        return {
        width: 370,
        height: 320,
        titleTextStyle: {
            color: '#FFFFFF',
            frontSize: 20
          },
        legend: {
            position: "bottom",
          textStyle: {
            color: '#FFFFFF'
          }
        },
        backgroundColor:{
            fill: "#17202a"
        },
        chartArea: {
            right: 10,
            width: 300,
            height: 280,
            backgroundColor: "#17202a"
        }
        };
    }
    
    function quitarAcentos(nombres) {
        var c, i,j;
        for(i=0; i<nombres.length; i++) {
            var nombre = nombres[i], nuevoNombre = "";
            for(j=0; j<nombre.length; j++) {
                c = nombre.charAt(j);
                switch(c) {
                    case 'á': c = 'a';break;
                    case 'é': c = 'e';break;
                    case 'í': c = 'i';break;
                    case 'ó': c = 'o';break;
                    case 'ú': c = 'u'; break;
                    case 'Á': c = 'A';break;
                    case 'É': c = 'E';break;
                    case 'Í': c = 'I';break;
                    case 'Ó': c = 'O';break;
                    case 'Ú': c = 'U'; break; 
                    default: break;
                }
                nuevoNombre += c;
            }
            nombres[i] = nuevoNombre;
        }
        return nombres;
    }
    
    return {
        dibujarGraficaArreglo: dibujarGraficaArreglo,
        updateChart: updateChart,
        crearArreglo: crearArreglo,
        crearGraficaIdTexto: crearGraficaIdTexto,
        actualizarGraficaIdTexto: actualizarGraficaIdTexto,
        crearGraficaLabelValor: crearGraficaLabelValor,
        actualizarGraficaLabelValor: actualizarGraficaLabelValor,
        filtrarPorBounds: filtrarPorBounds,
        crearGraficaTop: crearGraficaTop
    };
    
});
