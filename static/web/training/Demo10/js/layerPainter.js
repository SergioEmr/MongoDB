/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define(["luciad/view/feature/FeaturePainter",
        "luciad/shape/ShapeFactory",
        'samplecommon/IconFactory',
        "./main"
    ], function (FeaturePainter, ShapeFactory, IconFactory, Main) {
    
    function layerPainter() {
    } 
    layerPainter.prototype = new FeaturePainter();
    layerPainter.prototype.constructor = layerPainter;
    layerPainter.prototype.getDetailLevelScales = function() {
        return [1 / 3000000, 1 / 2000000, 1 / 1000000,1 / 50000,1 / 5000];
    };
    
    var sBlanco = 'rgb(255, 255, 255)';
    var sGrisOscuro = 'rgb(50, 50, 50)';
    var sGrisClaro = 'rgb(200, 200, 200)', GrisClaro = 'rgba(200, 200, 200, 0.5)';
    var sRojo = 'rgb(255, 0, 0)', Rojo = 'rgba(255, 0, 0, 0.5)';
    var sAmarilloClaro = 'rgb(255, 255, 204)';
    var sGris = 'rgb(230, 230, 230)', Gris = "rgba(230,230,230, 0.5)";
    var sNaranjaClaro = 'rgb(255, 239, 204)';
    var sNaranja = 'rgb(255, 174, 102)', Naranja = 'rgba(255, 174, 102, 0.5)';
    var Verde = "rgba(50,230,50,0.5)", sVerde = "rgb(0,200,0)", sVerdeClaro = "rgb(128, 255, 0)";
    var sMorado ="rgb(200,0,200)", Morado = "rgba(200,0,200, 0.5)";
    var Azul = "rgba(70, 100, 230, 0.5)", sAzul = "rgb(70, 100, 230)";
    var VerdeClaro = "rgba( 134, 255, 132 , 0.5)", sVerdeClaro = "rgb( 134, 255, 132)";
    var sCafe = "rgb(156, 96, 50)";
    var maxRadio = 0, minRadio = 1;
    
    layerPainter.prototype.paintBody = function (geoCanvas, feature, shape, layer, map, state) {
        
        var color;
        var cultivo = feature.properties.Cultivo;
        switch(cultivo) {
            case "MAIZ": color = sNaranja; break;
            case "TRIGO": color = sVerde; break;
            case "ALGODON": color = sAmarilloClaro; break;
            case "FRIJOL": color = sCafe; break;
            case "SORGO": color = sVerdeClaro; break;
            default: color = sAzul; break;
        }
        
        if(!feature.geometry.x) {
            var texto = "";
            texto += feature.properties.Folio_pred +"\t"+ feature.properties.Latitud+"\t"+feature.properties.Longitud+
                "\t"+feature.geometry.focusPoint.x+"\t"+feature.geometry.focusPoint.y;
            //console.log(texto);
            
            
            geoCanvas.drawShape(shape,{ 
                    fill: {color: color},
                    stroke: {
                        color: state.selected ? sRojo : sGris,
                        width: state.selected ? 2 : 1} 
                });
                
            }
            else {
                var icon, dimension= 14;
                if(state.selected) 
                    icon = IconFactory.circle({stroke: sBlanco, fill: sRojo, width: dimension, height: dimension});
                else 
                    icon  = IconFactory.circle({stroke: sBlanco, fill: color, width: dimension, height: dimension});
                geoCanvas.drawIcon(shape.focusPoint,{
                    width: dimension+"px",
                    height: dimension+"px",
                    image: icon
                });
            }
    };
    
    
    layerPainter.prototype.paintLabel = function (labelCanvas, feature, shape, layer, map, state) {
        var label, labelName = "", i=0, properties = feature.properties;
        
        if(properties) {
            if(properties.Ejido) {
                labelName = properties.Ejido;
            }
            else {
            for(var key in properties) {
                if(i===0)
                    labelName = properties[key];
                i++;
            } 
            }
        } else {
            labelName = feature.id;
        }
        
        label = "<span style='color: $color' class='label'>" + labelName + "</span>";
        if(state.level > 3) {     
            if(state.selected)
                labelCanvas.drawLabel(label.replace("$color", sRojo),shape.focusPoint, {});
            else
                labelCanvas.drawLabel(label.replace("$color", sBlanco),shape.focusPoint, {});
        }
    }
 
    return layerPainter;
});

