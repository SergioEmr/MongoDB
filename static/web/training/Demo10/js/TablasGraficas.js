/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define([], function () {
    
    var graficas = new Array(), x=0;
    
    function dibujarGraficaArreglo(div, datos, tipo, titulo) {
        document.getElementById(div).innerHTML="";
        switch(tipo.toLowerCase()) {
            case "piechart":
                PieChart(div, datos, titulo);
                break;
            case "columnchart":
                ColumnChart(div, datos);
                break;
            case "areachart":
                AreaChart(div, datos);
                break;
            default: return null;
        }
        
    }
    
    function ColumnChart(div, datos, titulo) {
        var chart;
        try{
            //google.charts.load('current', {'packages':['line']});
            google.charts.load('current', {packages: ['corechart', 'bar']});
            google.charts.setOnLoadCallback(drawColumnChart);
            
        }catch(e){
            console.log("Error al crear Grafica de columna\n");
            console.log(e);
        }
        function drawColumnChart() {
            chart = new google.visualization.ColumnChart(document.getElementById(div));
            var datosNuevos = new google.visualization.arrayToDataTable(datos);
            var options= getColumnOptions();
            options.title = titulo;
            chart.draw(datosNuevos, options);
            graficas[x] = chart;
            x++;
            return chart;
        }
        return chart;
    }
    
    function PieChart(div, datos, titulo){
        var chart;
        try{
            //google.charts.load('current', {'packages':['line']});
            google.charts.load('current', {'packages':['corechart']});
            return google.charts.setOnLoadCallback(drawPieChart);
        }catch(e){
            console.log("Error al crear Grafica de lineas\n");
            console.log(e);
        }
        function drawPieChart() {
            chart = new google.visualization.PieChart(document.getElementById(div));
            var datosNuevos = new google.visualization.arrayToDataTable(datos);
            var options= getPieOptions();
            options.title = titulo;
            chart.draw(datosNuevos, options);
            google.visualization.events.addListener(chart, 'select', function() {
                try{
                var x = chart.getSelection(), row = x[0].row, col = 0;
                var dato1 = datosNuevos.getValue(row, 0);
                
                filtrarTexto(dato1);
            }catch(e) {
                console.log(e);
            }
            });
            graficas[x] = chart;
            x++;
            return chart;
        }
        return chart;
    }
    
    function AreaChart(div, datos){
        var chart;
        try{
            //google.charts.load('current', {'packages':['line']});
            google.charts.load('current', {'packages':['corechart']});
            return google.charts.setOnLoadCallback(drawAreaChart);
        }catch(e){
            console.log("Error al crear Grafica de lineas\n");
            console.log(e);
        }
        function drawAreaChart() {
            chart = new google.visualization.AreaChart(document.getElementById(div));
            var datosNuevos = new google.visualization.arrayToDataTable(datos);
            var options= getAreaOptions();
            chart.draw(datosNuevos, options);
            google.visualization.events.addListener(chart, 'select', function() {
                try{
                var x = chart.getSelection(), row = x[0].row, col = 0;
                var dato1 = datosNuevos.getValue(row, 0);
                
                filtrarTexto(dato1);
            }catch(e) {
                console.log(e);
            }
            });
            graficas[x] = chart;
            x++;
        }
    }
    
    
    
    function crearArreglo(features) {
        var tabla = new Array(), encavezados = new Array(), valores = new Array();
        var key, i=0, j=0, k=0, x, coordenadas;
        for(key in features[0].properties) {
            encavezados[i] = key;
            i++;
        }
        tabla[j] = encavezados;
        for(i=0; i<features.length; i++) {
            for(key in features[i].properties) {
                try {
                    x = parseFloat(features[i].properties[key]);
                    if(x)
                        valores[k] = x;
                    else
                        valores[k] = features[i].properties[key];
                }catch (e) {
                    
                }
                k++;
            }
            coordenadas = features[i].geometry.coordinates;
            if(coordenadas.length === 3 || coordenadas.length === 2) {
                valores[k] = coordenadas[0];
                valores[k+1] = coordenadas[1];
            } else {
                valores[k] = coordenadas[0][0][0];
                valores[k+1] = coordenadas[0][0][1];
            }
                
            j++;
            tabla[j]=valores;
            valores = new Array();
            k=0;
        }
        
        return tabla;
    }
    
    function crearArregloSensores(Sensores17, Sensores16) {
        var i,j,k, est6, est7, sum6, sum7;
        var tabla16, tabla17, tabla;
        tabla17 = crearArreglo(Sensores17);
        tabla16 = crearArreglo(Sensores16);
        for(i=0;i<tabla17[0].length;i++) {
            if(tabla17[0][i] === "ESTACION")
                est7 = i;
            if(tabla17[0][i] === "SUMA")
                sum7 = i;
        }
        for(i=0;i<tabla16[0].length;i++) {
            if(tabla16[0][i] === "ESTACION  " || tabla16[0][i] === "ESTACION" )
                est6 = i;
            if(tabla16[0][i] === "SUMA")
                sum6 = i;
        }
        tabla = [["label","number","number"]];
        k=1;
        var n6, n7, t6=0,t7=0;
        for(i=1;i<tabla17.length;i++) {
            n7 = tabla17[i][est7];
            for(j=1;j<tabla16.length;j++) {
                
                n6 = tabla16[j][est6];
                if( n7=== n6) {
                    tabla[k] = [n7, tabla17[i][sum7], tabla16[j][sum6]];
                    k++;
                    var x = tabla16[j][sum6];
                    t6 += parseInt(x);
                    t7 += tabla17[i][sum7];
                    break;
                }
            }
        }
        tabla[k] =["total", t7, t6];
        graficasSensores(tabla, "total");
        //dibujarGraficaArreglo("graficaComparacion", tabla, "columnchart", "Comparacion");
        return tabla;
    }
    
    function graficasSensores(tabla, label) {
        var datos = [[label, "number"]];
        var i,j, id = -1;
        
        for(i=0;i<tabla.length;i++) {
            if(tabla[i][0] === label) {
                id = i;
                break;
            }
        }
        if(id>0) {
            datos[1] = [label+"2017", parseInt(tabla[i][1])];
            datos[2] = [label+"2016", parseInt(tabla[i][2])];
            dibujarGraficaArreglo("graficaComparacion", datos, "columnchart", "Comparacion");
        } else {
            console.log("No hay comparacion");
        }
    }
    
    function crearGraficaIdTexto(div, tablaDatos, chartType, title, id) {
        var cultivos = datosPieChart(tablaDatos, id, title);
        dibujarGraficaArreglo(div, cultivos, chartType, title);
        return x;
    }
    
    function datosPieChart(tablaDatos, id, label) {
        var cultivos = [[label, "number"]];
        var tipoDato = new Array(), numeroDatos = new Array(), cultivo, nuevoDato=false;
        var i=0, j=0;
        
        for(i=1; i<tablaDatos.length; i++) {
            
            cultivo = tablaDatos[i][id];
            if(tipoDato.length===0) {
                tipoDato[0] = cultivo;
                numeroDatos[0] = 1;
            }
            else {
                for(j=0;j<tipoDato.length;j++) {
                    if(cultivo === tipoDato[j]) {
                        numeroDatos[j]++;
                        nuevoDato = false;
                        break;
                    }
                    else
                        nuevoDato =true;
                }
                if(nuevoDato){
                    tipoDato[j] = cultivo;
                    numeroDatos[j] = 1;
                }
            }
        }
        
        for(i=0; i<tipoDato.length; i++) {
            cultivos[i+1] = [tipoDato[i], numeroDatos[i]];
        }
        return cultivos;
    }
    
    function actualizarGrafica(idGrafica, tablaDatos, geometry, typeChart, title, id) {
        var cultivos = datosPieChartUbicacion(tablaDatos, id, title, geometry);
        updateChart(graficas[idGrafica], cultivos, typeChart);
    }
    function actualizarGraficaDatos(idGrafica, tablaDatos, periodo, cultivo, typeChart, title, id) {
        var Datos = datosPieChartFiltro(tablaDatos, id, title, periodo, cultivo,);
        updateChart(graficas[idGrafica], Datos, typeChart);
    }
    
    function actualizarGraficaUbicacionDatos(idGrafica, tablaDatos, geometry, periodo, cultivo, typeChart, title, id) {
        var Datos;
        if(geometry === null)
            Datos = datosPieChartFiltro(tablaDatos, id, title, periodo, cultivo,);
        else
            Datos = datosPieChartUbicacionFiltro(tablaDatos, id, title, geometry, periodo, cultivo,);
        updateChart(graficas[idGrafica], Datos, typeChart);
    }
    
    function datosPieChartFiltro(tablaDatos, id, label, periodo, cul) {
        var cultivos = [[label, "number"]];
        var tipoDato = new Array(), numeroDatos = new Array(), cultivo, nuevoDato=false;
        var i=0, j=0;
        
        for(i=1; i<tablaDatos.length; i++) {
            if ( (tablaDatos[i][9] === periodo || periodo === "Todos" ) &&
               (tablaDatos[i][2] === cul || cul === "Todos") ){
            cultivo = tablaDatos[i][id];
            if(tipoDato.length===0) {
                tipoDato[0] = cultivo;
                numeroDatos[0] = 1;
            }
            else {
                for(j=0;j<tipoDato.length;j++) {
                    if(cultivo === tipoDato[j]) {
                        numeroDatos[j]++;
                        nuevoDato = false;
                        break;
                    }
                    else
                        nuevoDato =true;
                }
                if(nuevoDato){
                    tipoDato[j] = cultivo;
                    numeroDatos[j] = 1;
                }
            }
            }
        }
        
        for(i=0; i<tipoDato.length; i++) {
            cultivos[i+1] = [tipoDato[i], numeroDatos[i]];
        }
        return cultivos;
    }
    
    function updateChart(chart, newData, chartType) {
        var newOptions;
        switch(chartType) {
            case "piechart": 
                newOptions = getPieOptions();
                break;
            case "columnchart":
                newOptions = getColumnOptions();
                break;
            case "areachart":
                newOptions = getAreaOptions();
                break;
        }
        var table = new google.visualization.arrayToDataTable(newData);
        chart.draw(table, newOptions);
    }
    
    
    function datosPieChartUbicacion(tablaDatos, id, label, geometry) {
        var cultivos = [[label, "number"]];
        var tipoDato = new Array(), numeroDatos = new Array(), cultivo, nuevoDato=false;
        var lon, lat;
        var i=0, j=0;
        var x = geometry.center.x, y = geometry.center.y;
        var h = geometry.bounds.height, w = geometry.bounds.width;
        var r = h/2;
        var distancia;
        
        for(i=1; i<tablaDatos.length; i++) {
            
            cultivo = tablaDatos[i][id];
            lon = tablaDatos[i][16];
            lat = tablaDatos[i][17];
            distancia = Math.sqrt(Math.pow(x-lon,2) + Math.pow(y-lat,2));
            if(distancia <= r) {
            //if(lon >= minX && lon <= maxX && lat >= minY && lat <= maxY) {
            if(tipoDato.length===0) {
                tipoDato[0] = cultivo;
                numeroDatos[0] = 1;
            }
            else {
                for(j=0;j<tipoDato.length;j++) {
                    if(cultivo === tipoDato[j]) {
                        numeroDatos[j]++;
                        nuevoDato = false;
                        break;
                    }
                    else
                        nuevoDato =true;
                }
                if(nuevoDato){
                    tipoDato[j] = cultivo;
                    numeroDatos[j] = 1;
                }
            }
            }
        }
        
        for(i=0; i<tipoDato.length; i++) {
            cultivos[i+1] = [tipoDato[i], numeroDatos[i]];
        }
        return cultivos;
    }
    function datosPieChartUbicacionFiltro(tablaDatos, id, label, geometry, periodo, cul) {
        var cultivos = [[label, "number"]];
        var tipoDato = new Array(), numeroDatos = new Array(), cultivo, nuevoDato=false;
        var lon, lat;
        var i=0, j=0;
        var x = geometry.center.x, y = geometry.center.y;
        var h = geometry.bounds.height, w = geometry.bounds.width;
        var r = h/2;
        var distancia;
        
        for(i=1; i<tablaDatos.length; i++) {
            
            cultivo = tablaDatos[i][id];
            lon = tablaDatos[i][16];
            lat = tablaDatos[i][17];
            distancia = Math.sqrt(Math.pow(x-lon,2) + Math.pow(y-lat,2));
            var per = tablaDatos[i][9];
            var cu = tablaDatos[i][2];
            if((per === periodo || periodo === "Todos" ) &&
               ( cu=== cul || cul === "Todos")) {
            if(distancia <= r ) {
            if(tipoDato.length===0) {
                tipoDato[0] = cultivo;
                numeroDatos[0] = 1;
            }
            else {
                for(j=0;j<tipoDato.length;j++) {
                    if(cultivo === tipoDato[j]) {
                        numeroDatos[j]++;
                        nuevoDato = false;
                        break;
                    }
                    else
                        nuevoDato =true;
                }
                if(nuevoDato){
                    tipoDato[j] = cultivo;
                    numeroDatos[j] = 1;
                }
            }
            }}
        }
        
        for(i=0; i<tipoDato.length; i++) {
            cultivos[i+1] = [tipoDato[i], numeroDatos[i]];
        }
        return cultivos;
    }
    
    function crearGraficaLabelValor(div, tablaDatos, chartType, title, idLabel, idValor) {
        console.log("Creando Grafica ");
        var cultivos = datosPieChartLabelValor(tablaDatos, idLabel, idValor, title);
        dibujarGraficaArreglo(div, cultivos, chartType, title);
        return x;
    }
    
    function datosPieChartLabelValor(tablaDatos, idLabel, idValor, label) {
        var datos = [[label, "number"]];
        var tipoDato = new Array(), numeroDatos = new Array(), nombre, valor, nuevoDato=false;
        var i=0, j=0;
        
        for(i=1; i<tablaDatos.length; i++) {
            
            nombre = tablaDatos[i][idLabel];
            valor = tablaDatos[i][idValor];
            if(tipoDato.length===0) {
                tipoDato[0] = nombre;
                numeroDatos[0] = valor;
            }
            else {
                for(j=0;j<tipoDato.length;j++) {
                    if(nombre === tipoDato[j]) {
                        numeroDatos[j]+= parseFloat(valor);
                        nuevoDato = false;
                        break;
                    }
                    else
                        nuevoDato =true;
                }
                if(nuevoDato){
                    tipoDato[j] = nombre;
                    numeroDatos[j] = valor;
                }
            }
        }
        
        for(i=0; i<tipoDato.length; i++) {
            datos[i+1] = [tipoDato[i], numeroDatos[i]];
        }
        return datos;
    }
    
    /*
     * 
     */
    function actualizarGraficaLabelValor(idGrafica, tablaDatos, geometry, typeChart, title, idLabel, idValor) {
        var cultivos = datosPieChartLabelValorUbicacion(tablaDatos, idLabel, idValor, title, geometry);
        updateChart(graficas[idGrafica], cultivos, typeChart);
    }
    function actualizarGraficaLabelValorDatos(idGrafica, tablaDatos,periodo, cultivo, typeChart, title, idLabel, idValor) {
        var cultivos = datosPieChartLabelValorFiltro(tablaDatos, idLabel, idValor, title,periodo, cultivo);
        updateChart(graficas[idGrafica], cultivos, typeChart);
    }
    function actualizarGraficaLabelValorUbicacionDatos(idGrafica, tablaDatos, geometry, periodo, cultivo, typeChart, title, idLabel, idValor) {
        var Datos;
        if(geometry === null)
            Datos = datosPieChartLabelValorFiltro(tablaDatos, idLabel, idValor, title, periodo, cultivo,);
        else
            Datos = datosPieChartLabelValorUbicacionDatos(tablaDatos, idLabel, idValor, title, geometry, periodo, cultivo,);
        updateChart(graficas[idGrafica], Datos, typeChart);
    }
    function datosPieChartLabelValorFiltro(tablaDatos, idLabel, idValor, label, periodo, cul) {
        var datos = [[label, "number"]];
        var tipoDato = new Array(), numeroDatos = new Array(), nombre, valor, nuevoDato=false;
        var i=0, j=0;
        
        for(i=1; i<tablaDatos.length; i++) {
            if ( (tablaDatos[i][9] === periodo || periodo === "Todos" ) &&
               (tablaDatos[i][2] === cul || cul === "Todos") ){
            nombre = tablaDatos[i][idLabel];
            valor = tablaDatos[i][idValor];
            if(tipoDato.length===0) {
                tipoDato[0] = nombre;
                numeroDatos[0] = valor;
            }
            else {
                for(j=0;j<tipoDato.length;j++) {
                    if(nombre === tipoDato[j]) {
                        numeroDatos[j]+= parseFloat(valor);
                        nuevoDato = false;
                        break;
                    }
                    else
                        nuevoDato =true;
                }
                if(nuevoDato){
                    tipoDato[j] = nombre;
                    numeroDatos[j] = valor;
                }
            }
            }
        }
        
        for(i=0; i<tipoDato.length; i++) {
            datos[i+1] = [tipoDato[i], numeroDatos[i]];
        }
        return datos;
    }
    
    function datosPieChartLabelValorUbicacion(tablaDatos, idLabel, idValor, label, geometry) {
        var datos = [[label, "number"]];
        var tipoDato = new Array(), numeroDatos = new Array(), nombre, nuevoDato=false, valor;
        var lon, lat;
        var i=0, j=0;
        var x = geometry.center.x, y = geometry.center.y;
        var h = geometry.bounds.height, w = geometry.bounds.width;
        var r = h/2;
        var maxX = x + r, minX = x - r;
        var maxY = y +r, minY = y - r;
        
        for(i=1; i<tablaDatos.length; i++) {
            
            nombre = tablaDatos[i][idLabel];
            valor = tablaDatos[i][idValor];
            lon = tablaDatos[i][16];
            lat = tablaDatos[i][17];
            var distancia = Math.sqrt(Math.pow(x-lon,2) + Math.pow(y-lat,2));
            if(distancia <= r ) {
            if(tipoDato.length===0) {
                tipoDato[0] = nombre;
                numeroDatos[0] = valor;
            }
            else {
                for(j=0;j<tipoDato.length;j++) {
                    if(nombre === tipoDato[j]) {
                        numeroDatos[j]+= parseFloat(valor);;
                        nuevoDato = false;
                        break;
                    }
                    else
                        nuevoDato =true;
                }
                if(nuevoDato){
                    tipoDato[j] = nombre;
                    numeroDatos[j] = valor;
                }
            }
            }
        }
        
        for(i=0; i<tipoDato.length; i++) {
            datos[i+1] = [tipoDato[i], numeroDatos[i]];
        }
        return datos;
    }
    
    function datosPieChartLabelValorUbicacionDatos(tablaDatos, idLabel, idValor, label, geometry, periodo, cul) {
        var datos = [[label, "number"]];
        var tipoDato = new Array(), numeroDatos = new Array(), nombre, nuevoDato=false, valor;
        var lon, lat;
        var i=0, j=0;
        var x = geometry.center.x, y = geometry.center.y;
        var h = geometry.bounds.height, w = geometry.bounds.width;
        var r = h/2;
        var maxX = x + r, minX = x - r;
        var maxY = y +r, minY = y - r;
        
        for(i=1; i<tablaDatos.length; i++) {
            
            nombre = tablaDatos[i][idLabel];
            valor = tablaDatos[i][idValor];
            lon = tablaDatos[i][16];
            lat = tablaDatos[i][17];
            var distancia = Math.sqrt(Math.pow(x-lon,2) + Math.pow(y-lat,2));
            var per = tablaDatos[i][9];
            var cu = tablaDatos[i][2];
            if((per === periodo || periodo === "Todos" ) &&
               (cu === cul || cul === "Todos")) {
            if(distancia <= r) {
            if(tipoDato.length===0) {
                tipoDato[0] = nombre;
                numeroDatos[0] = valor;
            }
            else {
                for(j=0;j<tipoDato.length;j++) {
                    if(nombre === tipoDato[j]) {
                        numeroDatos[j]+= parseFloat(valor);;
                        nuevoDato = false;
                        break;
                    }
                    else
                        nuevoDato =true;
                }
                if(nuevoDato){
                    tipoDato[j] = nombre;
                    numeroDatos[j] = valor;
                }
            }
            }}
        }
        
        for(i=0; i<tipoDato.length; i++) {
            datos[i+1] = [tipoDato[i], numeroDatos[i]];
        }
        return datos;
    }
    
    function getPieOptions() {
        return {
        width: 370,
        height: 320,
        titleTextStyle: {
            color: '#FFFFFF',
            frontSize: 20
          },
        legend: {
            position: "left",
          textStyle: {
            color: '#FFFFFF'
          }
        },
        backgroundColor:{
            fill: "#17202a"
        },
        chartArea: {
            left: 20,
            width: 330,
            height: 300,
            backgroundColor: "#17202a"
        }
      };
    }
    function getColumnOptions() {
        return {
        width: 370,
        height: 320,
        orientation: "vertical",
        titleTextStyle: {
            color: '#FFFFFF',
            frontSize: 20
          },
        legend: {
            position: "bottom",
          textStyle: {
            color: '#FFFFFF'
          }
        },
        backgroundColor:{
            fill: "#17202a"
        },
        chartArea: {
            left: 150,
            width: 250,
            height: 300,
            backgroundColor: "#17202a"
        },
        annotations: {
          textStyle: {
            fontSize: 12,
            color: '#FFFFFF',
            auraColor: 'none'
          }
        },
        //axisTitlesPosition: "out",
        hAxis: {
          logScale: false,
          slantedText: false,
          format: 'short',
          textStyle: {
            color: '#FFFFFF',
            frontSize: 10
          },
          titleTextStyle: {
            color: '#FFFFFF',
            frontSize: 16
          }
        },
        vAxis: {
          title: 'Presupuesto',
          
          direction: 1,
          textStyle: {
            color: '#FFFFFF'
          }
          
        }
      };
    }
    function getAreaOptions () {
        return {
        width: 370,
        height: 320,
        titleTextStyle: {
            color: '#FFFFFF',
            frontSize: 20
          },
        legend: {
            position: "bottom",
          textStyle: {
            color: '#FFFFFF'
          }
        },
        backgroundColor:{
            fill: "#17202a"
        },
        chartArea: {
            right: 10,
            width: 300,
            height: 280,
            backgroundColor: "#17202a"
        }
        };
    }
    
    return {
        dibujarGraficaArreglo: dibujarGraficaArreglo,
        updateChart: updateChart,
        crearArreglo: crearArreglo,
        crearGraficaIdTexto: crearGraficaIdTexto,
        actualizarGrafica: actualizarGrafica,
        crearGraficaLabelValor: crearGraficaLabelValor,
        actualizarGraficaLabelValor: actualizarGraficaLabelValor,
        actualizarGraficaLabelValorDatos: actualizarGraficaLabelValorDatos,
        actualizarGraficaDatos: actualizarGraficaDatos,
        actualizarGraficaUbicacionDatos: actualizarGraficaUbicacionDatos,
        actualizarGraficaLabelValorUbicacionDatos: actualizarGraficaLabelValorUbicacionDatos,
        crearArregloSensores: crearArregloSensores,
        graficasSensores: graficasSensores
    };
    
});
