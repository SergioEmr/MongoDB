/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define([
    'luciad/view/LayerType',
    'luciad/view/feature/FeatureLayer',
    "luciad/model/feature/FeatureModel",
    "./SensorPainter",
    "luciad/view/feature/transformation/ClusteringTransformer",
    "luciad/model/store/UrlStore"
], function (LayerType, FeatureLayer, FeatureModel, SensorPainter, ClusteringTransformer, UrlStore) {
    return {
        createLayer: function (reference, map, SensorColorService) {
        SensorColorService.registerForMap(map);
        var store = new UrlStore({target:"data/sensors.json"});
        var model = new FeatureModel(
            store,
            {reference: reference}
        );

        return new FeatureLayer(
            model, {
                label: "Sensors",
                layerType: LayerType.STATIC,
                selectable: true,
                editable: false,
                painter: new SensorPainter(SensorColorService),
                transformer: ClusteringTransformer.create({
                    classifier: {
                        getClassification: function (object) {
                            return object.properties.isActive;
                        }
                    },
                    defaultParameters: {
                        clusterSize: 100,
                        minimumPoints: 2
                    }
                })
            });
    }
    };
});
