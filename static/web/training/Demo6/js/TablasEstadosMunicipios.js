/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define([
    "./GoogleCharts",
    "./data"
],
function (GoogleCharts, datos) {
   
    var Estados = datos.Estados;
    var totalApoyo=datos.totalApoyo, totalEjercido=Array(Estados.length),
        productoresEstados= datos.productoresEstados, apoyoEstados=datos.apoyoEstados,
        ejercidoEstados=datos.ejercidoEstados, cultivos=datos.cultivos, superficie = datos.superficie;
    
    
    var formatNumber = {
        separador: ",", // separador para los miles
        sepDecimal: '.', // separador para los decimales
        formatear:function (num){
            num +='';
            var splitStr = num.split('.');
            var splitLeft = splitStr[0];
            var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
            var regx = /(\d+)(\d{3})/;
            while (regx.test(splitLeft)) {
                splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
            }
            return this.simbol + splitLeft +splitRight;
        },
        new:function(num, simbol){
            this.simbol = simbol ||'';
            return this.formatear(num);
        }
    };
        
    var i,j,l=-1,l=0, grafica =true, c =true;
    for(i=0; i<Estados.length;i++) {
            totalApoyo[i]=0;
            totalEjercido[i]=0;
    }
    
    function dibujarGrafica2(div, datos, tipo, titulo) {
        document.getElementById(div).innerHTML="";
        switch(tipo.toLowerCase()) {
            case "piechart":
                GoogleCharts.PieChart(div, datos, titulo);
                break;
            case "columnchart":
                GoogleCharts.ColumnChart(div, datos);
                break;
            default: break;
        }
        
    }
    
    function datosCultivos(div, estado, m) {
        document.getElementById(div).innerHTML="";
        document.getElementById("TablaCabezera").innerHTML="<p>Cultivos por Hectarias "+ estado;
        var maiz=Array(Estados.length), trigo=Array(Estados.length), pastos=Array(Estados.length);
        for(i=0;i<Estados.length;i++) {
            maiz[i]=0;
            trigo[i]=0;
            pastos[i]=0;
        }
        
        for(i=0;i<Estados.length;i++) {
            for(j=0;j<cultivos[i].length;j++) {
                switch(cultivos[i][j]) {
                    case "MAIZ":
                        maiz[i]+=superficie[i][j];
                        break;
                    case "TRIGO":
                        trigo[i]+=superficie[i][j];
                        break;
                    case "PASTOS PERENES":
                        pastos[i]+=superficie[i][j];
                        break;
                    default: break;
                }
            }
        }
        
        var tabla=[
            ['Cultivo', 'number'],
            ['Maiz', maiz[m]],
            ['Trigo', trigo[m]],
            ['Pastos Perenes', pastos[m]]];
        console.log("\n"+Estados[l]+":\tMaiz:\t"+maiz[l]+"\tTrigo:\t"+trigo[l]+"\tPastos:\t"+pastos[l]);
        GoogleCharts.PieChart(div, tabla);
    }
    
    function top10() {
        var may = Array(10), k, x, y =true, z, pro= Array(10);
        for(k =0;k<10;k++)
            may[k] = 0;
        k=0;
        for(i=0;i<Estados.length;i++) {
            for(j=0;j<apoyoEstados[i].length;j++) {
                if(y===true) {
                    if(k<10) {
                        if(apoyoEstados[i][j] > may[k] && productoresEstados[i][j] !== pro[k]) {
                            may[k]=apoyoEstados[i][j];
                            pro[k] = productoresEstados[i][j];
                            k++;
                        }
                    }
                    }else {
                        if(k<10) {
                            if(apoyoEstados[i][j] > may[k] && productoresEstados[i][j] !== pro[k]) {
                                may[k]=apoyoEstados[i][j];
                        
                            for(k=1;k<10;k++) {
                                if(may[k-1]>may[k]) {
                                    x=may[k-1];
                                    may[k-1]=may[k];
                                    may[k]=x;
                                    x=pro[k-1];
                                    pro[k-1]=pro[k];
                                    pro[k]=x;
                                    k=0;
                                }
                            }
                            k=0;
                            y=false;
                            i=0;j=0;
                        }
                    }
                }
            }
        }
        
        for(k=1;k<10;k++) {
            if(may[k-1]<may[k]) {
                x=may[k-1];
                may[k-1]=may[k];
                may[k]=x;
                x=pro[k-1];
                pro[k-1]=pro[k];
                pro[k]=x;
                k=0;
            }
        }
        //console.log(may+"\n"+pro);
        var tabla = Array(10);
        for(k=9;k>-1;k--) {
            tabla[k] = [pro[k], may[k], "$"+Math.round(may[k]/1000)+"K"];
        }
        return tabla;
    }
    
    function getTablaDatos(nombres, apoyo, ejercido) {
        var k, tabla =Array(nombres.length);
        for(k=0;k<nombres.length;k++ ) {
            tabla[k] = [nombres[k], apoyo[k], "$"+Math.round(apoyo[k]/1000)+"K", ejercido[k], "$"+Math.round(ejercido[k]/1000)+"K"];
        }
        return tabla;
    }
    
    function dibujarGrafica(div, tabladatos, estado) {
        document.getElementById(div).innerHTML="";
        document.getElementById("Tabla").innerHTML="Tabla";
        document.getElementById("TablaCabezera").innerHTML="<p>Presupuesto por Productor de "+ estado;
        GoogleCharts.drawColumnChart(div, tabladatos, "Presupuesto por Productor de "+ estado);
    }
    
    function dibujarGraficaEstados(div) {
        document.getElementById(div).innerHTML="";
        //datosCultivos();
        
        for(j=0;j<Estados.length;j++){
            for(i=0;i< apoyoEstados[j].length;i++){
                totalApoyo[j] += apoyoEstados[j][i];
                totalEjercido[j] += ejercidoEstados[j][i];
            }
        }
        
        
        var etiqueta = '<div class="labelVotaciones" style="display:block !important">' +
'<table ><thead>'+
    '<tr>' + //columnas
                '<td align="center">' +
                '<b>Estados</b>' +
                '</td >' +
                '<td align="center">' +
                '<b>Apoyo Total</b>' +
                '</td >' +
                '<td align="center">' +
                '<b>Ejercido Total</b>' +
                '</td >' +
            '</tr>' +
            '$DATOS'+
    '</table></div>';
        var _dato = '<tr>' +
                '<td align="center" style="border: 2px solid $Color">' +
                '<button class="botonTabla" id="$EstadoId"> $Estado </button>' +
                '</td >' +
                '<td align="center">' +
                '$$Apoyo' +
                '</td >' +
                '<td align="center">' +
                '$$Ejercido' +
                '</td >' +
                '</tr>';
        var dato= '';
        for(i=0; i<Estados.length;i++) {
            dato+= _dato.replace("$EstadoId",  Estados[i])
                    .replace("$Estado", Estados[i])
                    .replace("$Apoyo", formatNumber.new(totalApoyo[i]))
                    .replace("$Ejercido", formatNumber.new(totalEjercido[i]))
                    .replace("$Color", getColor(i));
        }
        etiqueta= etiqueta.replace("$DATOS", dato);
        document.getElementById(div).innerHTML=etiqueta;
        
        
        //GoogleCharts.drawTopChart("graficaTop", top10(),true);
        
        Estados.forEach(function(v) {
        $("#"+v).click(function() {
            var k=0;
            for(k=0;k<Estados.length;k++) {
                if(Estados[k] === v) {
                    Mostrar("2","1");
                    l=k;
                    datosCultivos("infopanel-chart", Estados[k], k);
                    c=false;
                }
            }
        });
        });
    }
    
    function getColor(x) {
        switch(x) {
            case 0: return "rgb(244,250,88)";
            case 1: return "rgb(46,254,100)";
                
        }
    }
    
    $("#Tabla").click(function() {
        if( l >= 0 ) {
            if(c==true) {
                datosCultivos("infopanel-chart", Estados[l], l);
                c=false;
            }
            else {
                c=true;
        if(grafica === true) {
            dibujarGrafica("infopanel-chart", 
                getTablaDatos(productoresEstados[l], apoyoEstados[l], ejercidoEstados[l]),
                Estados[l]);
            grafica = false;
            }
            else {
                dibujarTabla("infopanel-chart", productoresEstados[l], apoyoEstados[l], ejercidoEstados[l]);
                grafica=true;
            }
        }
        }
    });
    
    /*
     * 
     * @param {type} div
     * @param {Array} datos
     * @returns null
     */
    function dibujarTablaArray(div, datos) {
        document.getElementById(div).innerHTML="";
        var _etiqueta = '<div style="display:block !important">' +
        '<table><thead>'+
            '$Filas'+
        '</table></div>';
        var etiqueta ='';
        var columnaTitulo = '<td align="center"><b>$Titulo</b></td >';
        var _columna = '<td align="center">$Dato</td >';
        var columna='';
        var _filas = '<tr>$Columnas</tr>';
        var filas ='';
        var i,j;
        for(i=0;i<datos.length;i++) {
            for(j=0;j<datos[i].length;j++) {
                if(i===0) {
                    columna+= columnaTitulo.replace("$Titulo", datos[i][j]);
                }
                else {
                    if(typeof(datos[i][j])==='number') {
                        if(j===datos[i].length-1)
                            columna+= _columna.replace("$Dato", '$'+formatNumber.new(datos[i][j]));
                        else
                            columna+= _columna.replace("$Dato", formatNumber.new(datos[i][j]));
                    }
                    else
                        columna+= _columna.replace("$Dato", datos[i][j]);
                }
            }
            filas += _filas.replace("$Columnas", columna);
            columna='';
        }
        etiqueta += _etiqueta.replace("$Filas", filas);
        document.getElementById(div).innerHTML=etiqueta;
    }
    
    
    function dibujarTabla(div, productores, apoyo, ejercido) {
        document.getElementById(div).innerHTML="";
        document.getElementById("Tabla").innerHTML="Grafica";
        var etiqueta = '<div class="labelVotaciones" style="display:block !important">' +
'<table><thead>'+

            '<tr>' + //columnas
                '<td align="center">' +
                '<b>Beneficiario</b>' +
                '</td >' +
                '<td align="center">' +
                '<b>Apoyo</b>' +
                '</td >' +
                '<td align="center">' +
                '<b>Ejercido</b>' +
                '</td >' +
            '</tr>' +
            '$DATO'+
                
'</table></div>';
        var _dato = '<tr>' +
                '<td align="left">' +
                '<b>$Beneficiario</b>' +
                '</td >' +
                '<td align="center">' +
                '<b>$$Apoyo.00</b>' +
                '</td >' +
                '<td align="center">' +
                '<b>$$Ejercido</b>' +
                '</td >' +
                '</tr>';
        var dato='';
        for(i=0; i<productores.length;i++) {
            dato+= _dato.replace("$Beneficiario", productores[i]).replace("$Apoyo", apoyo[i])
                    .replace("$Ejercido", ejercido[i]);
        }
        etiqueta= etiqueta.replace("$DATO", dato);
        document.getElementById(div).innerHTML=etiqueta;
    }
    
    function Mostrar(I,O) {
        $("button").removeClass("active");
        //$(".addui").hide();
        $("#panel"+O).fadeOut("slow");
        //$("#panel"+O).addClass("active");
        $("#panel"+I).fadeIn("slow");
    }
    
    return { 
        dibujarGraficaEstados: dibujarGraficaEstados,
        dibujarGrafica: dibujarGrafica,
        dibujarGrafica2: dibujarGrafica2,
        dibujarTablaArray: dibujarTablaArray
    };
});

