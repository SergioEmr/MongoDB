define([],
function () {
    var Estados = ["Sinaloa", "Durango"];
    var totalApoyo=Array(Estados.length), totalEjercido=Array(Estados.length),
            productoresEstados= Array(Estados.length), apoyoEstados=Array(Estados.length),
            ejercidoEstados=Array(Estados.length), cultivos=Array(Estados.length),
            superficie = Array(Estados.length);
    
    /*
     * datos de los municipios de sinaloa y durango
     */
    productoresEstados[0] = ["MIGUEL HUMBERTO RUELAS", "JULIO CESAR APODACA", 
            "CARLOS RAMIREZ","JOSE JUAN BRISEÑO","EDUARDO RAMIREZ",
            "JAVIER OCTAVIO PEREZ","ELISEO QUINTERO","CESAR LEONEL BORQUEZ",
            "RAMON NAZERAU","EPIFANIA VALENZUELA"];
    apoyoEstados[0] = [46900,4900,28000,2400,35000,14000,
            17495.00,17437.00,37437.00,23437.00];
    ejercidoEstados[0] = [29531.57,1205.60,17162.72 ,880.12,12250.07,13964.00,
            9097.76,15693.50,25693.50,15693.50];
        
    productoresEstados[1] = ["VALPEZ AGRICULTORES SPR DE RI","ALMA DELIA RANGEL JIMENEZ",
    "JAVIER ALFREDO RIOS SOTO", "ELVA ESTHER ARMENTA FIERRO", "MARISELA APODACA VALENZUELA",
    "PLUTARCO VEGA CASTRO","GERTRUDIS JOCOBI COTA"];
    apoyoEstados[1] =       [40250,18400,24680,4613,15000,9000,12000];
    ejercidoEstados[1] =    [30200.50,10020,15320,3320,12320,5000,4320];
    
    cultivos[0] =["TRIGO","MAIZ","MAIZ","TRIGO","MAIZ","MAIZ","TRIGO","MAIZ","TRIGO","PASTOS PERENES"];
    cultivos[1]=["MAIZ","MAIZ","MAIZ","TRIGO","MAIZ","TRIGO","PASTOS PERENES"];
    
    superficie[0] = [67,7,40,3,50,2,4,9,2,55];
    superficie[1] = [57.5,12,35.27,6.59,9.5,7,40];
    
    /*
     * tabla de impacto
     */
    var datosTablaImpacto = [
            ["Estado","Municipio","Superficie sembrada(Ha)",
                "Producción obtenida(Ton)","Valor de la producción(Miles de pesos)"],
            ["Sinaloa","Badiraguato", 	10490,      15210.3,    52995.83],
            ["Sinaloa","Concordia",  	16271.74,   89267.02,	191715.73],
            ["Sinaloa","Cosalá",          9281.87,    34702.81,	57758.95],
            ["Sinaloa","Culiacán",   	194704.53,  2391849.08,	7664931.19],
            ["Sinaloa","Choix",      	23657,      33101.35,	203131.65],
            ["Sinaloa","Elota",       	47311.53,   480326.09,	1838994.93],
            ["Sinaloa","Mazatlán",   	27146.69,   227412.4,	518417.69],
            ["Sinaloa","Rosario",    	30923.37,   311373.88,	876358.13],
            ["Sinaloa","San Ignacio",	20576.96,   91836.46,	277479.37],
            ["Sinaloa","Sinaloa",    	144096.85,  678897.16,	2566550.37]
        ];
    var datosPieImpacto = [
        ["Producto", "number"],
        ["Carne en canal de ave",	2598620.006],
        ["Carne en canal de bovino",	1489677.43],
        ["Carne en canal de caprino",	12093.312],
        ["Carne en canal de ovino",	16939.67],
        ["Carne en canal de porcino",	270669.32],
        ["Cera en greña",               70.783],
        ["Huevo para plato",            180288.123],
        ["Leche de bovino*",            373497.549],
        ["Miel",                        2734.351]
    ];
    
    return {
        Estados: Estados,
        totalApoyo:totalApoyo,
        productoresEstados:productoresEstados,
        apoyoEstados:apoyoEstados,
        ejercidoEstados:ejercidoEstados,
        cultivos:cultivos,
        superficie:superficie,
        datosTablaImpacto: datosTablaImpacto,
        datosPieImpacto: datosPieImpacto
    };
});


