define([
    "./BalloonDatos",
    "./GoogleCharts",
    "./LayerBeneficiarios",
    "./OnHoverController",
    "./TablasEstadosMunicipios",
    "./LayerEstados",
    "./data",
    
    "recursos/js/MapFactory",
    "recursos/js/BingMapsDataLoader",
    "recursos/js/GoogleMap",
    "recursos/js/LayerFactory",
    "recursos/js/OpenFileOrUrl",
    
    "samples/common/dojo/DojoMap",
    "luciad/reference/ReferenceProvider",
    "samples/common/LayerConfigUtil",
    "luciad/view/LayerGroup",
    
    "samples/template/sample",
    "luciad/shape/ShapeFactory",
    
    "luciad/model/store/UrlStore",
    "luciad/model/feature/FeatureModel",
    "luciad/view/feature/FeatureLayer",
    "luciad/view/feature/FeaturePainter",
    
    "luciad/model/codec/GeoJsonCodec",
    "cop/store/RestStore",
    "luciad/model/feature/Feature",
    "template/sampleReady!"
], function (BalloonDatos, GoogleCharts, LayerBeneficiarios, OnHoverController, TablasEstadosMunicipios, 
        LayerEstados, Data,
        MapFactory, BingMapsDataLoader, GoogleMap, LayerFactory, OpenFileOrUrl,
        DojoMap, ReferenceProvider, LayerConfigUtil, LayerGroup,
        sampleTemplate, ShapeFactory, 
        UrlStore, FeatureModel,FeatureLayer, FeaturePainter,
        GeoJsonCodec, RestStore, Feature) {
    
    var referenceC = ReferenceProvider.getReference("CRS:84");
    var referenceE = ReferenceProvider.getReference("EPSG:4978");
    var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i
            .test(navigator.userAgent);
    var map, googleMap = false, grafica = true, paneles=false, paneles2 = false;
    var capaMapa;
    Start();
    
    /*
     * Funcion encargada de crear el mala dependiendo de si esta en un dispositivo mobil o no
     * creara el mapa en 3d o 2d, ademas de si esta activado e mapa de google 
     * 
     * Google: satellite, hybrid, roadmap o terrain
     */
    function Start() {
        if(googleMap)
            map = GoogleMap.crearMapaGoogle("map", "roadmap"); 
        else
        {
            if(isMobile)
                map = MapFactory.makeMap3d("map", {reference: referenceC}, {
                    includeBackground: false, includeElevation: true, includeLayerControl: true, newLayerControl: true});
            else {
                map = MapFactory.makeMap3d("map", {reference: referenceE}, {
                    includeBackground: true, 
                    includeElevation: false, 
                    includeLayerControl: true,
                    newLayerControl: true, 
                    noLayerDelete: true,
                    includeMouseLocation: true,
                    layerControlOpen: true
                });    
            }
        } 
        try {
            var onHoverController = new OnHoverController();
            map.controller = onHoverController;
            CrearCapas();
        }catch(e){console.log(e);}
        var coordenadas = [-100.36955613125707, 1, 25.60444047783617, 1];
        fit(null, coordenadas, true);
        
    }
    
    function fit(bounds, coordenadas, animate) {
        if(bounds === null || bounds === undefined) {
            map.mapNavigator.fit({
                bounds: ShapeFactory.createBounds(referenceC, coordenadas), animate: animate
            });
        }
        else {
            map.mapNavigator.fit({ bounds: bounds, animate: animate });
        }
    }
    function CrearCapas() {
        DojoMap.configure(map);
        
        if(!googleMap){
            try{
                BingMapsDataLoader.createBingLayer("Aerial", map); //AerialWithLabels Road Aerial
            }catch(e){ }
        }
        
        var optionsImg = {
            width: 7959,
            height: 6506,
            x: -105.3112857689999, // -105.31155343428512, 
            y: 20.67651496300005, //20.676064845094572,
            x2: -105.246469011, //-105.24672085404029,
            y2: 20.72625879900005 //20.72630643425921
        };
        
        var layer = LayerFactory.createKmlLayer("data/MunisipioSanPedro.kml", "San Pedro", true);
        map.layerTree.addChild(layer, "bottom");
        /*
        capaMapa = LayerFactory.createLayerImage(referenceC, "data/map1/mapa1.png", "23/10/2015", optionsImg);
        //capaMapa = LayerFactory.createLayerImage(referenceC, "data/map1/0_0_0.png", "Mapa Jalisco", c);
        map.layerTree.addChild(capaMapa, "bottom");
        //capaMapa.visible = false;
        
        var capaMapa2 = LayerFactory.createLayerImage(referenceC, "data/map2/mapa2.png", "25/10/2015", optionsImg);
        map.layerTree.addChild(capaMapa2, "bottom");
        var alpha = parseFloat($("#opacity").val());
        capaMapa.rasterStyle = {alpha: alpha};
        
        */
        
        LayerConfigUtil.addLonLatGridLayer(map);
    }
    
    
    
    ["bDetalles1", "bDetalles2"].forEach(function(v) {
        $("#"+v).click(function() {
            switch(v) {
                case "bDetalles1":
                    
                    if(paneles===true){
                        MostrarOcultar(null,"panel1");
                        paneles=false;
                    }
                    else {
                        if(paneles2===true) {
                            MostrarOcultar(null, "panel2");
                            paneles2=false;
                        }
                        MostrarOcultar("panel1", null);
                        paneles=true;
                    
                    }
                    break;
                case "bDetalles2":
                    
                    if(paneles2===true) {
                        MostrarOcultar(null,"panel2");
                        paneles2=false;
                    }
                    else {
                        if(paneles===true) {
                            MostrarOcultar(null, "panel1");
                            paneles=false;
                        }
                        MostrarOcultar("panel2", null);
                        paneles2=true;
                    }
                    
                    break;
                default: break;
            }
        });
    });
    
    $("#opacity").on("change input", function() {
        var alpha = parseFloat($("#opacity").val());
        capaMapa.rasterStyle = {alpha: alpha};
    });
});

function MostrarOcultar(I,O) {
        $("button").removeClass("active");
        $("#"+O).fadeOut("slow");
        $("#"+I).fadeIn("slow");
    }
