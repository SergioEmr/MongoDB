define([], 
function() {

    var div,  data, tabladatos, tablaColumnas, chart, titulo= "";
    //var boton = document.getElementById("cambiarchat");
    var options;
     
    function PieChart(d, datos, t){
        div = d;
        tabladatos= datos;
        titulo= t;
        try{
            //google.charts.load('current', {'packages':['line']});
            google.charts.load('current', {'packages':['corechart']});
            google.charts.setOnLoadCallback(drawPieChart);
        }catch(e){
            console.log("Error al crear Grafica de lineas\n\n"+e);
        }
    }
    
    function drawLineChart(d){
        div = d;
        try{
            //google.charts.load('current', {'packages':['line']});
            google.charts.load('current', {'packages':['corechart']});
            google.charts.setOnLoadCallback(drawChart);
        }catch(e){
            console.log("Error al crear Grafica de lineas\n\n"+e);
        }
    }
    
    function drawColumnChart(d, datos) {
        div = d;
        tabladatos= datos;
        try{
            //google.charts.load('current', {'packages':['line']});
            google.charts.load('current', {packages: ['corechart', 'bar']});
            google.charts.setOnLoadCallback(drawAnnotations);
            
        }catch(e){
            console.log("Error al crear Grafica de columna\n\n"+e);
        }
    }
    
    function ColumnChart(d, datos) {
        div = d;
        tabladatos= datos;
        try{
            //google.charts.load('current', {'packages':['line']});
            google.charts.load('current', {packages: ['corechart', 'bar']});
            google.charts.setOnLoadCallback(drawNewColumnChart);
            
        }catch(e){
            console.log("Error al crear Grafica de columna\n\n"+e);
        }
    }
    function drawTopChart(d, datos) {
        div = d;
        tabladatos= datos;
        try{
            //google.charts.load('current', {'packages':['line']});
            google.charts.load('current', {packages: ['corechart', 'bar']});
            google.charts.setOnLoadCallback(drawChart);
            
        }catch(e){
            console.log("Error al crear Grafica de columna\n\n"+e);
        }
    }
    
    function drawAnnotations () {
        data = new google.visualization.DataTable();
        data.addColumn('string', 'Beneficiario');
        data.addColumn('number', 'Apoyo');
        data.addColumn({type: 'string', role: 'annotation'});
        data.addColumn('number', 'Ejercido');
        data.addColumn({type: 'string', role: 'annotation'});
        
        for( var i=0; i< tabladatos.length;i++)
                data.addRow(tabladatos[i]);

        var chart = new google.visualization.ColumnChart(document.getElementById(div));
        options= getOptions("");
        chart.draw(data, options);
    
    }
    
    function drawNewColumnChart() {
        chart = new google.visualization.ColumnChart(document.getElementById(div));
        data = new google.visualization.arrayToDataTable(tabladatos);
        options= getOptions("");
        chart.draw(data, options);
    }
    
    function drawPieChart() {
        chart = new google.visualization.PieChart(document.getElementById(div));
        data = new google.visualization.arrayToDataTable(tabladatos);
        options= getPieOptions();/*
        options.chartArea.left = 20;
        options.chartArea.width= 300;
        options.legend.position = "right";*/
        chart.draw(data, options);
    }
    
    function drawChart() {
        chart = new google.visualization.ColumnChart(document.getElementById(div));
        data = new google.visualization.DataTable();
        
        data.addColumn('string', 'Beneficiario');
        data.addColumn('number', 'Apoyo');
        data.addColumn({type: 'string', role: 'annotation'});
      
        for( var i=0; i< tabladatos.length;i++)
            data.addRow(tabladatos[i]);
        options= getOptions("");
        chart.draw(data, options);
        //console.log(data.toJSON());
    }
    
    var i=0, j=0,k=false;
    
    function getPieOptions() {
        return {
        width: 370,
        height: 320,
        title: "titulo",
        titleTextStyle: {
            color: '#FFFFFF',
            frontSize: 20
          },
        legend: {
            position: "left",
          textStyle: {
            color: '#FFFFFF'
          }
        },
        backgroundColor:{
            fill: "#17202a"
        },
        chartArea: {
            left: 20,
            width: 330,
            height: 300,
            backgroundColor: "#17202a"
        }
      };
    }
    function getOptions() {
        return {
        width: 370,
        height: 320,
        orientation: "vertical",
        title: "titulo",
        titleTextStyle: {
            color: '#FFFFFF',
            frontSize: 20
          },
        legend: {
            position: "bottom",
          textStyle: {
            color: '#FFFFFF'
          }
        },
        backgroundColor:{
            fill: "#17202a"
        },
        chartArea: {
            left: 150,
            width: 250,
            height: 300,
            backgroundColor: "#17202a"
        },
        annotations: {
          alwaysOutside: true,
          textStyle: {
            fontSize: 12,
            color: '#FFFFFF',
            auraColor: 'none'
          }
        },
        //axisTitlesPosition: "out",
        hAxis: {
          logScale: false,
          slantedText: false,
          format: 'short',
          textStyle: {
            color: '#FFFFFF',
            frontSize: 10
          },
          titleTextStyle: {
            color: '#FFFFFF',
            frontSize: 16
          }
        },
        vAxis: {
          title: 'Presupuesto',
          
          direction: 1,
          textStyle: {
            color: '#FFFFFF'
          }
          
        }
      };
    }
    
    
    function updateChart()
    {
        var a,b,c;
        var n = data.getNumberOfRows()-1;
        a=data.getValue(0,1);
        b=data.getValue(0,2);
        c=data.getValue(0,3);
        for(j=0;j<n;j++)
            for( var i=1;i<4;i++)
                data.setValue(j,i,data.getValue(j+1,i));
        data.setValue(n,1,a);
        data.setValue(n,2,b);
        data.setValue(n,3,c);
        chart.draw(data, options);
    }
    
    return {
        drawLineChart: drawLineChart,
        drawColumnChart: drawColumnChart,
        drawTopChart: drawTopChart,
        PieChart:PieChart,
        ColumnChart: ColumnChart
    };
});