/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define([
    'luciad/util/Promise',
    "samples/timeslider/TimeSlider",
    'luciad/shape/ShapeFactory',
    "samples/timeslider/TimeSlider",
    "time/TimeSliderWithOverview",
    'luciad/util/ColorMap',
    'luciad/transformation/TransformationFactory',
    
    "./Graficas",
    "./TimeLineLayer",
    "luciad/model/store/MemoryStore",
    "luciad/model/feature/FeatureModel",
    "luciad/view/feature/FeatureLayer",
    "samples/timeslider/TimeSlider",
    "time/TimeSliderWithOverview",
    
    'luciad/view/feature/ParameterizedLinePainter',
    'luciad/util/ColorMap',
    'luciad/view/feature/ShapeProvider',
    "./painters/EventTimePainter",
    "./Contador"
], function (Promise, TimeSlider, ShapeFactory,TimeSlider,TimeSliderWithOverview,ColorMap,TransformationFactory,
            Graficas, TimeLineLayer, MemoryStore, FeatureModel, FeatureLayer, TimeSlider, TimeSliderWithOverview,
            ParameterizedLinePainter, ColorMap, ShapeProvider, EventTimePainter, Contador ) {
    
    var timeSlider;
     /*
     * updateHistogram, esta funcion se encarga de actualizar toda la informacion necesario
     * al mover la linea de tiempo
     * @returns {undefined}
     */
    var timeFilter;
    function updateHistogram(map, features, layer, reference, startTime, endTime, tablaDatos, graficas, municipios) {
        var store = new MemoryStore({data: features});
        var originalModel = new FeatureModel(store, { reference: reference });
        var eventPromise = originalModel.query();
        Promise.when(eventPromise, function(cursor) {
            var quakes = [];
            var index = 0;
            while (cursor.hasNext()) {
                var quake = cursor.next();
                quakes[index] = quake;
                index++;
            }
            //console.log("Loading " + index + " events");
            try {
                document.getElementById("totalEventos").innerHTML = "Total eventos: "+index;
            } catch(e) {}
            startTime = startTime - 1000;
            endTime += 1000;
            var shapeProvider = new ShapeProvider();
            shapeProvider.reference = TimeSlider.REFERENCE;
            shapeProvider.provideShape = function(feature) {
                // var time = offset + feature.properties.time / 1000 | 0;
                var time = Date.parse(feature.properties.EventTime) / 1000 | 0;
                // var depth = 800-feature.properties.depth;
            // var depth = 800-feature.shape.z;
                var depth = 10-feature.shape.z;
                return ShapeFactory.createPoint(TimeSlider.REFERENCE, [time, depth]);
            };
            var timeEventLayer = new FeatureLayer(layer.model, {
                selectable: true,
                shapeProvider: shapeProvider,
                painter: new EventTimePainter()
            });
            
            var updateTimeFilters = function() {
                
                var mapBounds = timeSlider.mapBounds;
                var filterStart = (timeSlider.mapBounds.x | 0 );
                var filterWidth = (timeSlider.mapBounds.width | 0 );
                var filterEnd = filterStart + filterWidth;
                var crimen = crimenFilterNode.selectedOptions[0].value;
                var zona = zonaFilterNode.selectedOptions[0].value;
                
                var puntos = [], np=0;
                    layer.filter = function(feature) {
                        var f = timeEventLayer.shapeProvider.provideShape(feature);
                        var c = feature.properties.EMPRESA;
                        var z = feature.properties.NOMBRE;
                        if(mapBounds.contains2D(f)) {
                            if((c === crimen || crimen === "Todos") && (z === zona || zona === "Todos")) {
                                //Contador.reestartLayers(municipios);
                                //Contador.filtrarConteo(feature.municipios);
                                return true;
                            }
                                
                            else
                                return false;
                        } else
                            return false;
                    };
                try {
                    var nuevaTabla = Graficas.actualizarTabla(tablaDatos,timeEventLayer, mapBounds);
                    for(var k =0; k<graficas.length;k++) {
                        Graficas.actualizarGraficaIdTexto(graficas[k].id, nuevaTabla, graficas[k].tipo, graficas[k].label);
                    }
                    document.getElementById("totalEventos").innerHTML = "Total eventos: "+(tablaDatos.length-1);
                }catch(e) {
                    console.log("Error al actualizar grafica");
                    //console.log(e);
                
                }
                if(puntos.length===0) {
                    puntos = layer.model.query().array;
                }
                Contador.reestartLayers(municipios);
                Contador.contadorMunicipios(puntos, municipios);
                
            }; /// fin UpdateFilters
    
            var combineModeChangedFunction = function(transform) {
                timeFilter = undefined;
                var combineMode = timeSlider.getCombineMode();

                var painter = new ParameterizedLinePainter({
                    lineWidth: 10,
                    rangePropertyProvider: function(feature, shape, pointIndex) {
                    // var time = offset + feature.properties.time / 1000 | 0;
                        var time = Date.parse(feature.properties.EventTime) / 1000 | 0;

                        return transform(time);
                    },
                    rangeWindow: [0, 999999999999]
                });
                painter.density = {
                    colorMap: ColorMap.createGradientColorMap([
                    {level: 0, color: "rgba(  0,   0,   0, 0.0)"},
                    {level: 1, color: "rgba(  0, 0,   255, 0.5)"},
                    {level: 5, color: "rgba(  0, 255,   255, 1.0)"},
                    {level: 10, color: "rgba(  255, 255,   0, 1.0)"},
                    {level: 20, color: "rgba(255, 0, 0, 1.0)"}
                    ])
                };

                var previousPainter = layer.painter;
                var quakesModel = layer.model;
                var layer2d = new FeatureLayer(quakesModel, {
                    label: "Eventos 2",
                    selectable: true,
                    id: "eventos",
                    painter: painter
                });
                map.layerTree.removeChild(map.layerTree.findLayerById("eventos"));
                // map3d.layerTree.removeChild(map3d.layerTree.findLayerById("12345"));
                //map.layerTree.addChild(layer2d);
                // map3d.layerTree.addChild(layer3d);

                eventos = layer2d;

                timeSlider.layerTree.removeChild(timeEventLayer);

                var shapeProvider = new ShapeProvider();
                shapeProvider.reference = TimeSlider.REFERENCE;
                shapeProvider.provideShape = function(feature) {
                    var time = Date.parse(feature.properties.EventTime) / 1000 | 0;
                     time = transform(time);

                    var depth = 10-feature.shape.getPoint(0).z;
                    return ShapeFactory.createPoint(TimeSlider.REFERENCE, [time, depth]);
                };
                timeEventLayer = new FeatureLayer(quakesModel, {
                    selectable: true,
                    shapeProvider: shapeProvider,
                    painter: new EventTimePainter()
                });

                //timeSlider.layerTree.addChild(timeEventLayer);
            
                updateTimeFilters();
                var lonlatBounds = transformation.transformBounds(map.mapBounds);
                timeEventLayer.filter = function(feature) {
                    return lonlatBounds.contains2DPoint(feature.shape.getPoint(0));
                };
            
            // timeSlider.getHistogram().updateHistogram(histogramUpdater);
            };

            var combineModeFilterChangedFunction = function(filter) {
                timeFilter = filter;
                if (timeFilter) {
                        map.layerTree.findLayerById("eventos").filter = function(feature) {
                        // var time = offset + feature.properties.time / 1000 | 0;
                        var time = Date.parse(feature.properties.EventTime) / 1000 | 0;
                        return timeFilter(time);
                    };
                    updateTimeFilters();
                    var lonlatBounds = transformation.transformBounds(map.mapBounds);
                    timeEventLayer.filter = function(feature) {
                    // var time = offset + feature.properties.time / 1000 | 0;
                        var time = Date.parse(feature.properties.EventTime) / 1000 | 0;
                        if (timeFilter(time)) {
                            return lonlatBounds.contains2DPoint(feature.shape.getPoint(0));
                        }
                        else {
                            return false;
                        }
                    };
                }
            };

            timeSlider = new TimeSliderWithOverview("timeSlider", "timeSliderOverview", startTime, endTime, combineModeChangedFunction, combineModeFilterChangedFunction);


            var columnsDayLayer = TimeLineLayer.createLayer(features, "days", 36, startTime, endTime);
            timeSlider.layerTree.addChild(columnsDayLayer);
            /*var queryFinishedHandleSlider = columnsDayLayer.workingSet.on("QueryFinished", function() {

                queryFinishedHandleSlider.remove();
            });*/
                if(columnsDayLayer.bounds) {
                            try{
                                timeSlider.mapNavigator.fit({
                                    bounds: columnsDayLayer.bounds,
                                    animation: {duration: 10000},
                                    allowWarpXYAxis: true,
                                    fitMargin: "5%"
                                });
                             }catch(e) {
                                    console.log(e);
                             }
                        }
            var columnsHourLayer = TimeLineLayer.createLayer(features, "hours", 30*36, startTime, endTime);
            timeSlider.layerTree.addChild(columnsHourLayer);

            timeSlider.refit();

            var transformation = TransformationFactory.createTransformation(map.reference, reference);
            var histogramUpdater = function(accumulate) {
                    var lonlatBounds = transformation.transformBounds(map.mapBounds);
                    for (var i = 0; i < quakes.length; i++) {
                        if(lonlatBounds.contains2DPoint(quakes[i].shape)) {
                        // var time = offset + quakes[i].properties.time / 1000 | 0;
                            var time = Date.parse(quakes[i].properties.EventTime) / 1000 | 0;
                // var magnitude = quakes[i].properties.magnitude;
                            var magnitude = quakes[i].properties.mag;
                            accumulate(time, magnitude);
                        }
                    }
                };
            timeSlider.getHistogram().updateHistogram(histogramUpdater);


            var crimenFilterNode = document.getElementById( "empresasFiltro" );
            crimenFilterNode.addEventListener( "change", function () {
                //fitColumns = true;
                updateTimeFilters();
            });
            var zonaFilterNode = document.getElementById( "estadosFiltro" );
            zonaFilterNode.addEventListener( "change", function () {
                //fitColumns = true;
                updateTimeFilters();
            });

            map.on("idle", function() {
                timeSlider.getHistogram().updateHistogram(histogramUpdater);
            });
            map.on("MapChange", function() {
                var lonlatBounds = transformation.transformBounds(map.mapBounds);

                if (timeFilter) {
                    timeEventLayer.filter = function(feature) {
                    // var time = offset + feature.properties.time / 1000 | 0;
                        var time = Date.parse(feature.properties.EventTime) / 1000 | 0;
                        if (timeFilter(time)) {
                            return lonlatBounds.contains2DPoint(feature.shape);
                        }
                        else {
                            return false;
                        }
                    };
                }
                else {
                    timeEventLayer.filter = function(feature) {
                        return lonlatBounds.contains2DPoint(feature.shape);
                    };
                }
            });

            timeSlider.on("MapChange", function() {
                //if(fitColumns === false)
                    updateTimeFilters();
            });
        
        //Fin Promise
        });
        
    //fin UpdateHistogram
    }
    ////================================================================================////////
    
    return {
        updateHistogram: updateHistogram,
        timeSlider: timeSlider
    };
});

