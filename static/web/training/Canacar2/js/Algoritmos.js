/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function leer (name) {
    $.ajax({
        type:'Get',
        dataType: "json",
        url: "data/json/RespuestasRed/"+name+".json"
    }).done(function(data) {
        console.log("Respuesta de Red leida");
         if(data.features) {
            var n = data.features.length, feature;
            for(var i=0; i<n; i++) {
                feature = data.features[i];
                ids[i] = feature.id;
                intervalos[i] = feature.intervalo;
                predicciones.rn = feature.predicción["R.Negocio"];
                predicciones.rp = feature.predicción["R.Persona"];
                predicciones.vf = feature.predicción["Violencia Familiar"];
                predicciones.t = feature.predicción["Tóxico"];
            }
            var div = "eligeCuadrante";
            var base = "<option value='$VALUE'>$LABEL</option>", etiqueta = base.replace("$VALUE", "Todos").replace("$LABEL", "Todos");
            var  n = ids.length;
            for(var i =0; i<n; i++) {
                etiqueta += base.replace("$VALUE", ids[i]).replace("$LABEL", ids[i]);
            }
            document.getElementById(div).innerHTML = "";
            document.getElementById(div).innerHTML = etiqueta;
         }
    }).fail(function(e) {
        console.log("No se leyo la respuesta de la red");
        console.log(e);
    });
}
var alpha={}, beta={}, delta={}, echo={}, lambda={}, ids = new Array(), predicciones = {vf: new Array(), rn: new Array(), rp: new Array(), t: new Array()}, intervalos = new Array();
//leer ("EscobedoEcho1");