/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define(["luciad/view/feature/FeaturePainter",
        "luciad/shape/ShapeFactory",
        'samplecommon/IconFactory',
        'luciad/util/ColorMap',
    ], function (FeaturePainter, ShapeFactory, IconFactory, ColorMap) {
    
    function layerPainter() {
    } 
    layerPainter.prototype = new FeaturePainter();
    layerPainter.prototype.constructor = layerPainter;
    layerPainter.prototype.getDetailLevelScales = function() {
        return [1 / 3000000, 1 / 2000000, 1 / 1000000,1 / 100000,1 / 50000];
    };
    
    var sBlanco = 'rgb(255, 255, 255)',     Blanco = 'rgba(255, 255, 255, 0.2)';
    var sGrisOscuro = 'rgb(50, 50, 50)',    GrisOscuro = 'rgba(50, 50, 50, 0.2)';
    var sGrisClaro = 'rgb(200, 200, 200)',  GrisClaro = 'rgba(200, 200, 200, 0.5)';
    var sRojo = 'rgb(255, 0, 0)',           Rojo = 'rgba(255, 0, 0, 0.2)';
    var sAmarilloClaro = 'rgb(255, 255, 204)', AmarilloClaro = 'rgba(255, 255, 204, 0.2)';
    var sGris = 'rgb(130, 130, 130)',       Gris = "rgba(130,130,130, 0.2)";
    var sNaranjaClaro = 'rgb(255, 239, 204)', NaranjaClaro = 'rgba(255, 239, 204, 0.2)';
    var sNaranja = 'rgb(255, 174, 102)',    Naranja = 'rgba(255, 174, 102, 0.2)';
    var Verde = "rgba(50,230,50,0.2)",      sVerde = "rgb(50,230,50)";
    var sMorado ="rgb(200,0,200)",          Morado = "rgba(200,0,200, 0.2)";
    var Azul = "rgba(70, 100, 230, 0.2)",   sAzul = "rgb(70, 100, 230)";
    var VerdeClaro = "rgba( 134, 255, 132 , 0.2)", sVerdeClaro = "rgb( 134, 255, 132 )";
    var maxRadio = 0, minRadio = 1;
    
    layerPainter.prototype.paintBody = function (geoCanvas, feature, shape, layer, map, state) {
        var color = getColor("3");
        if(feature.geometry.x) {
            var regularStyle = {
                url: "data/img/alarma2.png",
                width: "50px",
                height: "50px",
                draped: false
            };
            geoCanvas.drawIcon(shape, regularStyle);
        }else {
        if(state.level > 4) {
            geoCanvas.drawShape(shape,{ 
                fill: {color: state.selected ? color.selected : color.normal},
                stroke: {
                    color: sBlanco ,
                    width: 2} 
            });
        }}
    };
    function getColor(id) {
        switch(id) {
            case "0": return {normal: Azul, selected: sAzul};
            case "1": return {normal: Morado, selected: sMorado};
            case "2": return {normal: AmarilloClaro, selected: sAmarilloClaro};
            case "3": return {normal: Naranja, selected: sNaranja};
            case "5": return {normal: GrisOscuro, selected: sGrisOscuro};
            case "6": return {normal: NaranjaClaro, selected: sNaranjaClaro};
            case "7": return {normal: Rojo, selected: sRojo};
            case "8": return {normal: VerdeClaro, selected: sVerdeClaro};
            case "9": return {normal: GrisClaro, selected: sGrisClaro};
            case "10": return {normal: Verde, selected: sVerde};
            default: return {normal: Gris, selected: sGris};
        }
    }
    
 
    return layerPainter;
});


