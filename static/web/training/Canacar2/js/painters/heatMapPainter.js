/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define(["luciad/view/feature/FeaturePainter",
        "luciad/shape/ShapeFactory",
        'samplecommon/IconFactory',
        'luciad/util/ColorMap'
    ], function (FeaturePainter, ShapeFactory, IconFactory, ColorMap) {
    
    function layerPainter() {
    } 
    layerPainter.prototype = new FeaturePainter();
    layerPainter.prototype.constructor = layerPainter;
    layerPainter.prototype.getDetailLevelScales = function() {
        return [1 / 3000000, 1 / 2000000, 1 / 1000000,1 / 50000,1 / 5000];
    };
    
    
    var sBlanco = 'rgb(255, 255, 255)';
    var sGrisOscuro = 'rgb(50, 50, 50)';
    var sGrisClaro = 'rgb(200, 200, 200)', GrisClaro = 'rgba(200, 200, 200, 0.5)';
    var sRojo = 'rgb(255, 0, 0)', Rojo = 'rgba(255, 0, 0, 0.5)';
    var sAmarilloClaro = 'rgb(255, 255, 204)';
    var sGris = 'rgb(230, 230, 230)', Gris = "rgba(230,230,230, 0.5)";
    var sNaranjaClaro = 'rgb(255, 239, 204)';
    var sNaranja = 'rgb(255, 174, 102)', Naranja = 'rgba(255, 174, 102, 0.5)';
    var Verde = "rgba(50,230,50,0.5)", sVerde = "rgb(50,230,50)";
    var sMorado ="rgb(200,0,200)", Morado = "rgba(200,0,200, 0.5)";
    var Azul = "rgba(70, 100, 230, 0.5)", sAzul = "rgb(70, 100, 230)";
    var VerdeClaro = "rgba( 134, 255, 132 , 0.5)", sVerdeClaro = "rgb( 134, 255, 132 )";
    var maxRadio = 0, minRadio = 1;
    
    var x = 1, y = 0;
    x = Math.round(parseFloat($("#Densidad").val()));
    layerPainter.prototype.density = {
                colorMap: ColorMap.createGradientColorMap([
                {level: y, color: "rgba(  0,   0,   255, 0.5)"},
                {level: y+=x, color: "rgba(  0, 100,   255, 0.5)"},
                {level: y+=x*2, color: "rgba(  0, 255,   255, 1.0)"},
                {level: y+=x*3, color: "rgba(  255, 255,   0, 1.0)"},
                {level: y+=x*4, color: "rgba(255, 0, 0, 1.0)"}
                ])
            };
    layerPainter.prototype.paintBody = function (geoCanvas, feature, shape, layer, map, state) {
        var icon;
        if(state.selected) 
                icon = IconFactory.circle({stroke: sBlanco, fill: sGrisOscuro, width: dimension, height: dimension});
            else 
                icon  = IconFactory.circle({stroke: sBlanco, fill: sAzul, width: dimension, height: dimension});
           
        var regularStyle = {
            image: icon,
            width: "20px",
            height: "20px",
            draped: true
        };
        var densityStyle = {
            width: "48px",
            height: "48px",
            draped: true
        };
        var style = state.selected ? regularStyle :
                layerPainter.density ? densityStyle : regularStyle;
        //console.log(feature.geometry.coordinateType);
        if(!feature.geometry.x) {
                    geoCanvas.drawShape(shape,{ 
                    fill: {color: state.selected ? Verde : Azul},
                    stroke: {
                        color: state.selected ? sRojo : sGris,
                        width: 2} 
                });
        }
        else {
            var  dimension= 14;
             geoCanvas.drawIcon(shape.focusPoint, style);
        }
    };
    
 
    return layerPainter;
});

