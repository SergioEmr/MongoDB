define([
  'luciad/transformation/TransformationFactory',
  'luciad/shape/ShapeFactory',
  "luciad/view/controller/Controller",
  "luciad/view/controller/HandleEventResult",
  "luciad/view/input/GestureEventType"
], function(TransformationFactory, ShapeFactory, Controller, HandleEventResult, GestureEventType) {

  function OverviewerController(model, overviewFeature, timeSlider) {
    Controller.call(this);
    this._model = model;
    this._overviewFeature = overviewFeature;
    this._timeSlider = timeSlider;
    // this.overViewMode = 0;

    this.mode = 0;
    // 0 = nothing, 1 = left, 2 = right, 3 = translate
  }

  OverviewerController.prototype = Object.create(Controller.prototype);
  OverviewerController.prototype.constructor = OverviewerController;

  OverviewerController.prototype.onActivate = function(map) {
    Controller.prototype.onActivate.apply(this, arguments);
    this.timePoint = ShapeFactory.createPoint(this.map.reference, [0, 0]);
    this.previousTimePoint = null;
    this.viewPoint = ShapeFactory.createPoint();
  };

  OverviewerController.prototype.onDeactivate = function(map) {
    Controller.prototype.onDeactivate.apply(this, arguments);
  };

  // OverviewerController.prototype.setOverviewMode = function(newOverviewMode) {
  //   this.overViewMode = newOverviewMode;
  // };

  OverviewerController.prototype.onDraw = function(geoCanvas) {
    // if (this._feature) {
    //   geoCanvas.drawShape(this._feature.shape, {
    //     stroke: {
    //       color: "rgb(255,0,0)",
    //       width: 2
    //     }
    //   });
    // }
  };

  OverviewerController.prototype.onGestureEvent = function(event) {
    if (event.type === GestureEventType.DRAG) {
      var x = event.viewPosition[0];
      var y = event.viewPosition[1];

      this.viewPoint.move2D(x, y);
      this.map.viewToMapTransformation.transform(this.viewPoint, this.timePoint);

      if (this.previousTimePoint) {
        var difference = this.timePoint.x - this.previousTimePoint.x;

        if (this.mode === 3) {
          this._overviewFeature.shape.translate2D(difference, 0);
          this._model.put(this._overviewFeature);
          this.previousTimePoint.move2D(this.timePoint);
          this._timeSlider.setCurrentTime(this._overviewFeature.shape.x + this._overviewFeature.shape.width / 2)
        }
        if (this.mode === 1) {
          this._overviewFeature.shape.translate2D(difference, 0);
          this._overviewFeature.shape.width += -difference;
          this._model.put(this._overviewFeature);

          var fitBoundsLeft = ShapeFactory.createBounds(this.map.reference, [
            this._overviewFeature.shape.x, this._overviewFeature.shape.width, 0, 1000
          ]);
          this._timeSlider.mapNavigator.fit({bounds: fitBoundsLeft, animate: false, fitMargin: "0px"});
        }
        if (this.mode === 2) {
          this._overviewFeature.shape.width += difference;
          this._model.put(this._overviewFeature);

          var fitBoundsRight = ShapeFactory.createBounds(this.map.reference, [
            this._overviewFeature.shape.x, this._overviewFeature.shape.width, 0, 1000
          ]);
          this._timeSlider.mapNavigator.fit({bounds: fitBoundsRight, animate: false, fitMargin: "0px"});
        }
        this.previousTimePoint.move2D(this.timePoint);
      }
      else {
        var leftMapPoint = ShapeFactory.createPoint(this.map.reference, [this._overviewFeature.shape.x, 0]);
        var rightMapPoint = ShapeFactory.createPoint(this.map.reference,
            [this._overviewFeature.shape.x + this._overviewFeature.shape.width, 0]);

        var left = this.map.mapToViewTransformation.transform(leftMapPoint).x;
        var right = this.map.mapToViewTransformation.transform(rightMapPoint).x;

        console.log([left, x, right]);

        this.mode = 0;
        var handleSize = 50;
        if (left + handleSize <= x && x <= right - handleSize) {
          this.mode = 3;
        }
        if (left - handleSize < x && x < left + handleSize) {
          this.mode = 1;
        }
        if (right - handleSize < x && x < right + handleSize) {
          this.mode = 2;
        }
        var middle = ( left + right ) / 2;
        if (middle - handleSize < x && x < middle + handleSize && left < x && x < right) {
          this.mode = 3;
        }

        this.previousTimePoint = ShapeFactory.createPoint(this.map.reference, [this.timePoint.x, this.timePoint.y]);
      }
    }

    if (event.type === GestureEventType.DRAG_END) {
      this.previousTimePoint = null;
    }
    return HandleEventResult.EVENT_HANDLED;
  };

  return OverviewerController;
});