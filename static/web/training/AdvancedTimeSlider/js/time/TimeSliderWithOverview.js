define([
  'luciad/dojo/_base/lang',
  './DynamicHistogram',
  './HistogramPainter',
  './OverviewerController',
  'luciad/view/feature/FeatureLayer',
  'luciad/view/feature/FeaturePainter',
  'luciad/model/feature/Feature',
  'luciad/model/store/MemoryStore',
  'luciad/model/feature/FeatureModel',
  // 'samples/timeslider/TimeSlider',
  './DynamicTimeSlider',
  "luciad/view/feature/ShapeProvider",
  "luciad/reference/ReferenceProvider",
  "luciad/shape/ShapeFactory",
  "luciad/uom/UnitOfMeasureRegistry",
  "luciad/view/Map",
  "luciad/view/controller/Controller",
  "luciad/view/controller/HandleEventResult",
  "luciad/view/input/GestureEventType",
  "luciad/util/Evented"
], function(Lang, DynamicHistogram, HistogramPainter, OverviewerController, FeatureLayer, FeaturePainter, Feature,
            MemoryStore, FeatureModel, DynamicTimeSlider,
            ShapeProvider, ReferenceProvider, ShapeFactory, UnitOfMeasureRegistry, Map,
            Controller, HandleEventResult, GestureEventType, Evented) {
  var self;

  var yearInSeconds = 365 * 24 * 60 * 60;
  var weekInSeconds = 7 * 24 * 60 * 60;
  var dayInSeconds = 24 * 60 * 60;
  var hourInSeconds = 60 * 60;
  var minuteInSeconds = 60;

  /* Jump modes:
   0 : 1 year
   1 : 1 month
   2 : 1 week
   3 : 1 day
   4 : 1 hour
   5 : 5 minutes
   6 : 1 minute
   */
  var currentJumpMode = 0;
  var jumpFunction = function(time, addToArgument, amount) {
    var thisDate = new Date(time * 1000);
    var arguments = [
      thisDate.getFullYear(),
      thisDate.getMonth(),
      thisDate.getDate(),
      thisDate.getHours(),
      thisDate.getMinutes(),
      thisDate.getSeconds(),
      thisDate.getMilliseconds()
    ];

    arguments[addToArgument] += amount;

    var nextDate = new Date(arguments[0], arguments[1], arguments[2], arguments[3],
        arguments[4], arguments[5], arguments[6]);
    return nextDate.getTime() / 1000 | 0;
  };
  var jumpModes = {
    0: {
      jump: function(time, next) {
        if (next) {
          return jumpFunction(time, 0, 1);
        }
        else {
          return jumpFunction(time, 0, -1);
        }
      },
      text: "1 YEAR"
    },
    1: {
      jump: function(time, next) {
        if (next) {
          return jumpFunction(time, 1, 1);
        }
        else {
          return jumpFunction(time, 1, -1);
        }
      },
      text: "1 MONTH"
    },
    2: {
      jump: function(time, next) {
        if (next) {
          return jumpFunction(time, 2, 7);
        }
        else {
          return jumpFunction(time, 2, -7);
        }
      },
      text: "1 WEEK"
    },
    3: {
      jump: function(time, next) {
        if (next) {
          return jumpFunction(time, 2, 1);
        }
        else {
          return jumpFunction(time, 2, -1);
        }
      },
      text: "1 DAY"
    },
    4: {
      jump: function(time, next) {
        if (next) {
          return jumpFunction(time, 3, 1);
        }
        else {
          return jumpFunction(time, 3, -1);
        }
      },
      text: "1 HOUR"
    },
    5: {
      jump: function(time, next) {
        if (next) {
          return jumpFunction(time, 4, 5);
        }
        else {
          return jumpFunction(time, 4, -5);
        }
      },
      text: "5 MIN"
    },
    6: {
      jump: function(time, next) {
        if (next) {
          return jumpFunction(time, 4, 1);
        }
        else {
          return jumpFunction(time, 4, -1);
        }
      },
      text: "1 MIN"
    }
  };

  var yearPickerVisible = false;
  var monthPickerVisible = false;
  var dayPickerVisible = false;

  var monthChooserVisible = false;

  var flagReplay = false;
  var flagLockRange = false;

  var flagDoNotFitCurrent = false;

  var flagsAll = {
    year: false,
    month: false,
    dayOfWeek: false,
    dayOfMonth: false
  };
  var flagsEmpty = {
    year: false,
    month: false,
    dayOfWeek: false,
    dayOfMonth: false
  };

  function TimeSliderWithOverview(domNode, overViewDomNode, startTime, endTime, combineModeChangedCallback,
                                  combineModeFilterChangedCallback, options) {
    var overviewHistogramMax = 950;
    if (options) {
      if (options.histogram) {
        if (options.histogram.max) {
          overviewHistogramMax = options.histogram.max;
        }
      }
    }

    this.combineModeChangedCallback = combineModeChangedCallback;
    this.combineModeFilterChangedCallback = combineModeFilterChangedCallback;
    // this.timeSlider = new TimeSlider(domNode);
    this.overViewDomNode = overViewDomNode;
    this.overView = new DynamicTimeSlider(this.overViewDomNode, 0);
    this.initialStartTime = startTime;
    this.initialEndTime = endTime;
    this.realInitialStartTime = startTime;
    this.realInitialEndTime = endTime;
    this.startTime = startTime;
    this.endTime = endTime;

    this.currentDate;
    this.currentYear;
    this.currentMonth;
    this.currentDay;
    this.currentHours;
    this.currentMinutes;
    this.currentSeconds;

    self = this;
    DynamicTimeSlider.call(this, domNode);

    self.setOverviewMode(-1);

    connectButtons(self);

    $(window).resize(function() {
      self.resizeAndRefit();

      $('#timelabelstartpicker').data('DateTimePicker').toggle();
      $('#timelabelstartpicker').data('DateTimePicker').toggle();
      $('#timelabelendpicker').data('DateTimePicker').toggle();
      $('#timelabelendpicker').data('DateTimePicker').toggle();
      $('#currentTimePicker').data('DateTimePicker').toggle();
      $('#currentTimePicker').data('DateTimePicker').toggle();
    });

    var currentTime = ( startTime + endTime ) * 0.5;
    this.overView.setValidRange(startTime, endTime, 0, 1000);
    this.overView.setCurrentTime(currentTime);
    this.setValidRange(startTime, endTime, 0, 1000);
    // this.setValidRange(0, 99999999999, 0, 1000);
    this.setCurrentTime(currentTime);

    var bounds = ShapeFactory.createBounds(DynamicTimeSlider.REFERENCE,
        [startTime, endTime - startTime, self.mapBounds.y,
         self.mapBounds.height]);
    self.mapNavigator.fit({bounds: bounds, animate: false, fitMargin: "0px", allowWarpXYAxis: true});

    // Current time line
    var currentTimeLineModel = new FeatureModel(new MemoryStore(), {reference: DynamicTimeSlider.REFERENCE});
    var currrentTimeLineShape = ShapeFactory.createPolyline(self.mapBounds.reference, [
      ShapeFactory.createPoint(self.mapBounds.reference, [self.mapBounds.x + ( 0.5 * self.mapBounds.width), -100]),
      ShapeFactory.createPoint(self.mapBounds.reference, [self.mapBounds.x + ( 0.5 * self.mapBounds.width), 990]),
    ]);
    var currentTimeLineFeature = new Feature(currrentTimeLineShape, {}, 0);
    currentTimeLineModel.add(currentTimeLineFeature);
    var currentTimeLinePainter = new FeaturePainter();
    currentTimeLinePainter.paintBody = function(geocanvas, feature, shape, layer, map, paintState) {
      var currrentTimeLine = ShapeFactory.createPolyline(shape.reference, [
        ShapeFactory.createPoint(shape.reference, [shape.x + ( 0.5 * shape.width), -100]),
        ShapeFactory.createPoint(shape.reference, [shape.x + ( 0.5 * shape.width), 990]),
      ]);
      geocanvas.drawShape(shape, {
        stroke: {color: "#00a9ef", width: 3}
      });
    };
    var currentTimeLineLayer = new FeatureLayer(currentTimeLineModel, {
      painter: currentTimeLinePainter
    });
    self.layerTree.addChild(currentTimeLineLayer, "top");
    self.on("MapChange", function() {
      currentTimeLineFeature.shape = ShapeFactory.createPolyline(self.mapBounds.reference, [
        ShapeFactory.createPoint(self.mapBounds.reference, [self.mapBounds.x + ( 0.5 * self.mapBounds.width), -100]),
        ShapeFactory.createPoint(self.mapBounds.reference, [self.mapBounds.x + ( 0.5 * self.mapBounds.width), 990]),
      ]);
      currentTimeLineModel.put(currentTimeLineFeature);
    });
    self.layerTree.on("NodeAdded", function() {
      self.layerTree.moveChild(currentTimeLineLayer, "top");
    });

    // Overview
    var overviewModel = new FeatureModel(new MemoryStore(), {reference: DynamicTimeSlider.REFERENCE});
    var overviewFeature = new Feature(ShapeFactory.createBounds(DynamicTimeSlider.REFERENCE,
        [startTime, endTime - startTime, -100, 1050]), {}, 0);

    overviewModel.add(overviewFeature);
    var overviewPainter = new FeaturePainter();
    overviewPainter.paintBody = function(geocanvas, feature, shape, layer, map, paintState) {
      geocanvas.drawShape(shape, {
        stroke: {color: "rgba(192, 217,42,1.0)", width: 5},
        fill: {color: "rgba(192, 217,42, 0.3)"}
      });

      var currrentTimeLine = ShapeFactory.createPolyline(shape.reference, [
        ShapeFactory.createPoint(shape.reference, [shape.x + ( 0.5 * shape.width), -100]),
        ShapeFactory.createPoint(shape.reference, [shape.x + ( 0.5 * shape.width), 990]),
      ]);
      geocanvas.drawShape(currrentTimeLine, {
        stroke: {color: "#00a9ef", width: 3}
      });
    };
    this.overviewLayer = new FeatureLayer(overviewModel, {
      label: "TimeTiles",
      painter: overviewPainter
    });
    this.overView.layerTree.addChild(this.overviewLayer);
    this.overView.controller = new OverviewerController(overviewModel, overviewFeature, this);

    // var viewBounds = ShapeFactory.createBounds(null,[0, 0, 0, 0]);
    // var mapViewBounds = ShapeFactory.createBounds(null,[0, 0, 0, 0]);
    // overviewModel.on("ModelChanged", function() {
    //   self.overView.mapToViewTransformation.transformBounds(overviewModel.get(0).shape, viewBounds);
    //   self.overView.mapToViewTransformation.transformBounds(self.overView.mapBounds, mapViewBounds);
    //
    //   var left = ( viewBounds.x - 50 ) | 0;
    //   var width = ( viewBounds.width + 100 ) | 0;
    //   if (left < 0) {
    //     width += left;
    //     left = 0;
    //   }
    //
    //   if (left + width > mapViewBounds.width) {
    //     width = mapViewBounds.width - left;
    //   }
    //
    //   $('#overviewFeatureHandle').css("left", left + "px");
    //   $('#overviewFeatureHandle').css("width", width + "px");
    // });

    var endDate = moment(endTime * 1000);
    var startDate = moment(startTime * 1000);

    var $timelabelstartbtn = $('#datetimelabelstart');
    $('#timelabelstartpicker').datetimepicker({
      widgetParent: $timelabelstartbtn,
      format: "HH:mm:ss DD MMM YYYY",
      minDate: startDate,
      maxDate: endDate,
      date: startDate,
      defaultDate: startDate,
      showClear: true,
      showClose: true
    });
    $('#timelabelstartpicker').data("DateTimePicker").defaultDate(startDate);
    $('#timelabelstartpicker').data('DateTimePicker').viewDate(startDate);
    $('#timelabelstartpicker').data("DateTimePicker").date(startDate);
    $('#timelabelstartpicker').data('DateTimePicker').toggle();
    $('#timelabelstartpicker').data('DateTimePicker').toggle();

    var openStartPicker = function() {
      var currentStartDate = moment(self.mapBounds.x * 1000);
      $('#timelabelstartpicker').data('DateTimePicker').viewDate(currentStartDate);

      $('#timelabelstartpicker').data('DateTimePicker').showClose(true);
      $('#timelabelstartpicker').data('DateTimePicker').toggle();
      $('#datetimelabelstart .bootstrap-datetimepicker-widget').addClass('pickRangeStart');
      $('#datetimelabelstart .bootstrap-datetimepicker-widget').removeClass('chooseRangeStart');

      $(".glyphicon-trash").parent().removeAttr("data-action");
      $(".glyphicon-trash").parent().click(function() {
        $('#timelabelstartpicker').data("DateTimePicker").date(startDate);
        $('#timelabelstartpicker').data('DateTimePicker').viewDate(startDate);
      });
      // $(".glyphicon-trash").toggleClass('glyphicon-trash glyphicon-step-backward');

      // $('#timelabelendpicker').data('DateTimePicker').toggle();

      // glyphicon glyphicon-step-backward

      // $('#datetimelabelstart').find("div").css("top", "-299px");
      // $('#datetimelabelstart').find("div").css("bottom", "118px");
      // $('#datetimelabelstart').find("div").css("left", "-10px");

    };
    $('#timelabelstart').click(openStartPicker);
    $('#datelabelstart').click(openStartPicker);
    // $('#datetimelabelstart').click(openStartPicker);

    var flagDoNotFitStart = false;
    $("#timelabelstartpicker").on("dp.change", function(e) {
      if (flagDoNotFitStart) {
        flagDoNotFitStart = false;
      } else {
        var mapBounds = self.mapBounds;
        var filterStart = (self.mapBounds.x ) | 0;
        var filterWidth = (self.mapBounds.width  ) | 0;
        var filterEnd = filterStart + filterWidth;

        var newStart = e.date.valueOf() / 1000 | 0;
        var newWidth = filterEnd - newStart;

        if (newStart != filterStart) {
          var bounds = ShapeFactory.createBounds(DynamicTimeSlider.REFERENCE, [newStart, newWidth, self.mapBounds.y,
                                                                               self.mapBounds.height]);
          self.mapNavigator.fit({bounds: bounds, animate: true, fitMargin: "0px", allowWarpXYAxis: true});

          setTimeLabels(newStart, filterEnd);
        }
      }
    });

    // THE SAME FOR CURRENT TIME? OPTIMIZE?

    var currentDate = moment((startTime + endTime) * 500);

    var $timelabelstartbtn = $('#divChooseCurrentTime');
    $('#currentTimePicker').datetimepicker({
      widgetParent: $timelabelstartbtn,
      format: "HH:mm:ss DD MMM YYYY",
      minDate: startDate,
      maxDate: endDate,
      date: currentDate,
      defaultDate: currentDate,
      showClear: true,
      showClose: true
    });
    $('#currentTimePicker').data("DateTimePicker").defaultDate(currentDate);
    $('#currentTimePicker').data('DateTimePicker').viewDate(currentDate);
    $('#currentTimePicker').data("DateTimePicker").date(currentDate);
    $('#currentTimePicker').data('DateTimePicker').toggle();
    $('#currentTimePicker').data('DateTimePicker').toggle();

    var openCurrentTimePicker = function() {
      var currentDate = moment(self.mapBounds.x * 1000 + self.mapBounds.width * 500);
      $('#currentTimePicker').data('DateTimePicker').viewDate(currentDate);

      var overviewMode = self.getOverviewMode();
      if (overviewMode == 0) {
        $('#currentTimePicker').data('DateTimePicker').viewMode('years');
      }
      if (overviewMode == 1) {
        $('#currentTimePicker').data('DateTimePicker').viewMode('years');
      }
      if (overviewMode == 2) {
        $('#currentTimePicker').data('DateTimePicker').viewMode('months');
      }
      if (overviewMode == 3) {
        // Add another for time?
        $('#currentTimePicker').data('DateTimePicker').viewMode('days');
      }

      $('#currentTimePicker').data('DateTimePicker').toggle();
      $('#divChooseCurrentTime .bootstrap-datetimepicker-widget').addClass('chooseCurrentTime');
      $('#divChooseCurrentTime .bootstrap-datetimepicker-widget').removeClass('chooseYear');
      $('#divChooseCurrentTime .bootstrap-datetimepicker-widget').removeClass('chooseMonth');
      $('#divChooseCurrentTime .bootstrap-datetimepicker-widget').removeClass('chooseDay');

      $(".glyphicon-trash").parent().removeAttr("data-action");
      $(".glyphicon-trash").parent().click(function() {
        $('#currentTimePicker').data("DateTimePicker").date(currentDate);
        $('#currentTimePicker').data('DateTimePicker').viewDate(currentDate);
      });
      // $(".glyphicon-trash").toggleClass('glyphicon-trash glyphicon-step-backward');

    };
    $('#btnChooseCurrentTime').click(openCurrentTimePicker);

    $("#currentTimePicker").on("dp.change", function(e) {
      if (flagDoNotFitCurrent) {
        flagDoNotFitCurrent = false;
      } else {
        var mapBounds = self.mapBounds;
        var filterStart = (self.mapBounds.x ) | 0;
        var filterWidth = (self.mapBounds.width  ) | 0;
        var filterEnd = filterStart + filterWidth;
        var filterCurrentTime = filterStart + 0.5 * filterWidth;

        var newCurrentTime = e.date.valueOf() / 1000 | 0;

        if (newCurrentTime != filterCurrentTime) {
          // var bounds = ShapeFactory.createBounds(DynamicTimeSlider.REFERENCE, [newStart, newWidth, self.mapBounds.y,
          //                                                                      self.mapBounds.height]);
          // self.mapNavigator.fit({bounds: bounds, animate: true, fitMargin: "0px", allowWarpXYAxis: true});

          self.setCurrentTime(newCurrentTime);

          var difference = newCurrentTime - filterCurrentTime;

          setTimeLabels(filterStart + difference, filterEnd + difference);

          var overviewMode = self.getOverviewMode();
          if (overviewMode == 0) {
            getCurrentDateTime();
            fitOverviewOnYear(currentYear, true);
            setRange(31 * 24 * 60 * 60, true);
          }
          if (overviewMode == 1) {
            getCurrentDateTime();
            fitOverviewOnMonth(currentYear, currentMonth, true);
            setRange(dayInSeconds, true);
          }
          if (overviewMode == 2) {
            getCurrentDateTime();
            fitOverviewOnDay(currentYear, currentMonth, currentDay, true);
            setRange(hourInSeconds, true);
          }
          if (overviewMode == 3) {
            getCurrentDateTime();
            fitOverviewOnHour(currentYear, currentMonth, currentDay, currentHour, true);
            setRange(5 * minuteInSeconds, true);
          }
        }
      }
    });

    // THE SAME FOR END : OPTIMIZZE!!

    var $timelabelendbtn = $('#datetimelabelend');
    $('#timelabelendpicker').datetimepicker({
      widgetParent: $timelabelendbtn,
      format: "HH:mm:ss DD MMM YYYY",
      minDate: startDate,
      maxDate: endDate,
      date: endDate,
      defaultDate: endDate,
      showClear: true,
      showClose: true
    });
    $('#timelabelendpicker').data("DateTimePicker").defaultDate(endDate);
    $('#timelabelendpicker').data('DateTimePicker').viewDate(endDate);
    $('#timelabelendpicker').data("DateTimePicker").date(endDate);
    $('#timelabelendpicker').data('DateTimePicker').toggle();
    $('#timelabelendpicker').data('DateTimePicker').toggle();

    // $("#timelabelendpicker").on("dp.show", function(e) {
    //
    //   // $('#logo-luciad').css('bottom', '550px');
    //   // var currentEndDate = moment((self.mapBounds.x + self.mapBounds.width) * 1000);
    //   // $('#timelabelendpicker').data('DateTimePicker').viewDate(currentEndDate);
    //   //
    //   // $(".glyphicon-trash").parent().removeAttr("data-action");
    //   // $(".glyphicon-trash").parent().click(function() {
    //   //   $('#timelabelendpicker').data("DateTimePicker").date(endDate);
    //   //   $('#timelabelendpicker').data('DateTimePicker').viewDate(endDate);
    //   // });
    //   // $(".glyphicon-trash").toggleClass('glyphicon-trash glyphicon-step-forward');
    //
    //   // $('#datetimelabelend').find("div").css("top", "-299px");
    //   // $('#datetimelabelend').find("div").css("bottom", "118px");
    //   // $('#datetimelabelend').find("div").css("left", "-160px");
    // });
    $("#timelabelendpicker").on("dp.hide", function(e) {
      $('#logo-luciad').css('bottom', '270px');
    });
    var openEndPicker = function() {
      // $('#timelabelendpicker').data('DateTimePicker').toggle();

      $('#logo-luciad').css('bottom', '550px');

      var currentEndDate = moment((self.mapBounds.x + self.mapBounds.width) * 1000);
      $('#timelabelendpicker').data('DateTimePicker').viewDate(currentEndDate);

      $('#timelabelendpicker').data('DateTimePicker').showClose(true);
      $('#timelabelendpicker').data('DateTimePicker').toggle();
      $('#datetimelabelend .bootstrap-datetimepicker-widget').addClass('pickRangeEnd');
      $('#datetimelabelend .bootstrap-datetimepicker-widget').removeClass('chooseRangeEnd');

      $(".glyphicon-trash").parent().removeAttr("data-action");
      $(".glyphicon-trash").parent().click(function() {
        $('#timelabelendpicker').data("DateTimePicker").date(currentEndDate);
        $('#timelabelendpicker').data('DateTimePicker').viewDate(currentEndDate);
      });
    };
    $('#timelabelend').click(openEndPicker);
    $('#datelabelend').click(openEndPicker);
    // $('#datetimelabelend').click(openEndPicker);

    var flagDoNotFitEnd = false;
    $("#timelabelendpicker").on("dp.change", function(e) {
      if (flagDoNotFitEnd) {
        flagDoNotFitEnd = false;
      } else {
        var mapBounds = self.mapBounds;
        var filterStart = (self.mapBounds.x ) | 0;
        var filterWidth = (self.mapBounds.width  ) | 0;
        var filterEnd = filterStart + filterWidth;

        var newEnd = e.date.valueOf() / 1000 | 0;
        var newWidth = newEnd - filterStart;

        if (newEnd != filterEnd) {
          var bounds = ShapeFactory.createBounds(DynamicTimeSlider.REFERENCE, [filterStart, newWidth, self.mapBounds.y,
                                                                               self.mapBounds.height]);
          self.mapNavigator.fit({bounds: bounds, animate: true, fitMargin: "0px", allowWarpXYAxis: true});

          setTimeLabels(filterStart, newEnd);
        }
      }
    });

    // END SAME FOR END

    setTimeLabels(startTime, endTime);
    this.on("MapChange", function() {
      var mapBounds = self.mapBounds;
      var filterStart = (self.mapBounds.x | 0 );
      var filterWidth = (self.mapBounds.width | 0 );
      var filterEnd = filterStart + filterWidth;

      setTimeLabels(filterStart, filterEnd);

      overviewFeature.shape.x = filterStart;
      overviewFeature.shape.width = filterWidth;
      overviewModel.put(overviewFeature);

      // var currentStartDate = moment(self.mapBounds.x * 1000);
      // $('#timelabelstartpicker').data('DateTimePicker').viewDate(currentStartDate);
      // $('#timelabelstartpicker').data("DateTimePicker").date(currentStartDate);
    });
    this.on("idle", function() {
      var currentStartDate = moment(self.mapBounds.x * 1000);
      $('#timelabelstartpicker').data('DateTimePicker').viewDate(currentStartDate);
      $('#timelabelstartpicker').data("DateTimePicker").date(currentStartDate);

      var currentEndDate = moment((self.mapBounds.x + self.mapBounds.width ) * 1000);
      $('#timelabelendpicker').data('DateTimePicker').viewDate(currentEndDate);
      flagDoNotFitEnd = true;
      $('#timelabelendpicker').data("DateTimePicker").date(currentEndDate);

      var currentDate = moment((self.mapBounds.x + 0.5 * self.mapBounds.width) * 1000);
      $('#currentTimePicker').data('DateTimePicker').viewDate(currentDate);
      $('#currentTimePicker').data("DateTimePicker").date(currentDate);
    });

    this.histogram = new DynamicHistogram(DynamicTimeSlider.REFERENCE, startTime, endTime, 350, overviewHistogramMax);
    var histogramPainter = new HistogramPainter();
    var histogramLayerOverview = new FeatureLayer(this.histogram, {
      painter: histogramPainter,
      incrementalRendering: false
    });
    this.overView.layerTree.addChild(histogramLayerOverview, "top");
  }

  var monthNames = ["January", "February", "March", "April", "May", "June",
                    "July", "August", "September", "October", "November", "December"
  ];
  var weekDayNames = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

  function setTimeLabels(filterStart, filterEnd) {
    // setTimeLabel('timelabel', timeSlider.getCurrentTime());
    setTimeLabel('labelstart', filterStart);
    setTimeLabel('labelend', filterEnd);

    // 11 years 168 days 12 hours 16 minutes
    var range = filterEnd - filterStart;
    var rangeYears = range / yearInSeconds | 0;
    var rangeDays = (range / dayInSeconds) % 365 | 0;
    var rangeHours = (range / hourInSeconds) % 24 | 0;
    var rangeMinutes = (range / minuteInSeconds) % 60 | 0;

    $('#inpRangeMinutes').attr('value', rangeMinutes);
    $('#inpRangeHours').attr('value', rangeHours);
    // $('#inpRangeDays').attr('value', rangeDays);
    $("#inpRangeDays").val(rangeDays);
    // $('#inpRangeYears').attr('value', rangeYears);
    $("#inpRangeYears").val(rangeYears);

    rangeYears = addLeadingZeros(rangeYears, 2);
    rangeDays = addLeadingZeros(rangeDays, 3);
    rangeHours = addLeadingZeros(rangeHours, 2);
    rangeMinutes = addLeadingZeros(rangeMinutes, 2);
    var rangeString = rangeYears + " yrs " + rangeDays + " dys " + rangeHours + " hrs " + rangeMinutes + " mn";
    $('#inpTimeRange').attr('value', rangeString);

    function addLeadingZeros(number, digits) {
      var numberString = "" + number;
      while (numberString.length < (digits || 2)) {
        numberString = "0" + numberString;
      }
      return numberString;
    };

    this.currentDate = new Date((filterStart + filterEnd ) * 500);

    this.currentYear = currentDate.getFullYear();
    this.currentMonth = currentDate.getMonth();
    this.currentDay = currentDate.getDate();
    this.currentHours = currentDate.getHours();
    this.currentMinutes = currentDate.getMinutes();
    this.currentSeconds = currentDate.getSeconds();

    var minutes = (this.currentMinutes < 10 ? "0" + this.currentMinutes : this.currentMinutes );
    var seconds = (this.currentSeconds < 10 ? "0" + this.currentSeconds : this.currentSeconds );
    var hours = (this.currentHours < 10 ? "0" + this.currentHours : this.currentHours );
    var currentTime = hours + ":" + minutes + ":" + seconds;

    // if (self.combineMode > 0) {
    //   var startYear = new Date(self.realInitialStartTime * 1000).getFullYear();
    //   var all = true;
    //   var none = true;
    //   for (var i = 0; i < self.combineSelectedYears.length; i++) {
    //     var selectedYear = self.combineSelectedYears[i];
    //     if (selectedYear) {
    //       none = false;
    //     }
    //     else {
    //       all = false;
    //     }
    //   }
    //
    //   if (all || none) {
    //     $('#btnCurrentYear').text("ALL");
    //   }
    //   else {
    //     $('#btnCurrentYear').text("SOME");
    //   }
    // }
    // else {
    //   if (flagsAll.year) {
    //     $('#btnCurrentYear').text("ALL");
    //   }
    //   else {
    //     if (flagsEmpty.year) {
    //       $('#btnCurrentYear').text("");
    //     }
    //     else {
    //       $('#btnCurrentYear').text(this.currentYear);
    //     }
    //   }
    // }
    //
    // if (self.combineMode > 1) {
    //   var all = true;
    //   var none = true;
    //   for (var i = 0; i < self.combineSelectedMonths.length; i++) {
    //     var selected = self.combineSelectedMonths[i];
    //     if (selected) {
    //       none = false;
    //     }
    //     else {
    //       all = false;
    //     }
    //   }
    //   if (all || none) {
    //     $('#btnCurrentMonth').text("ALL");
    //   }
    //   else {
    //     $('#btnCurrentMonth').text("SOME");
    //   }
    // }
    // else {
    //   if (flagsAll.month) {
    //     $('#btnCurrentMonth').text("ALL");
    //   }
    //   else {
    //     if (flagsEmpty.month) {
    //       $('#btnCurrentMonth').text("");
    //     }
    //     else {
    //       $('#btnCurrentMonth').text(monthNames[this.currentMonth].toUpperCase());
    //     }
    //   }
    // }

    // if (self.combineMode > 2) {
    //   var all = true;
    //   var none = true;
    //   for (var i = 0; i < self.combineSelectedDays.length; i++) {
    //     var selected = self.combineSelectedDays[i];
    //     if (selected) {
    //       none = false;
    //     }
    //     else {
    //       all = false;
    //     }
    //   }
    //   if (all || none) {
    //     $('#btnCurrentDay').text("ALL");
    //   }
    //   else {
    //     $('#btnCurrentDay').text("SOME");
    //   }
    // }

    if (self.combineMode > 0) {
      $('#btnCurrentDayWeek').text("");
      $('#btnCurrentDayMonth').text("");
      if (self.combineMode > 1) {
        if (self.combineMode > 2) {
          var all = true;
          var none = true;
          var someString = "";
          var count = 0;
          var onlyOneString;
          for (var i = 0; i < self.combineSelectedDays.length; i++) {
            var selected = self.combineSelectedDays[i];
            if (selected) {
              none = false;
              someString += weekDayNames[i].substring(0, 1);
              count++;
              onlyOneString = weekDayNames[i];
            }
            else {
              all = false;
              someString += "-";
            }
          }
          if (all || none) {
            $('#btnCurrentDayCenter').text("ALL");
          }
          else {
            // $('#btnCurrentDay').text("SOME");
            if (count == 1) {
              $('#btnCurrentDayCenter').text(onlyOneString.toUpperCase());
            }
            else {
              $('#btnCurrentDayCenter').text(someString.toLowerCase());
            }
          }
        }
        else {
          $('#btnCurrentDayCenter').text(this.currentDay);
          // $('#btnCurrentDayWeek').text(weekDayNames[currentDate.getDay()].toUpperCase());
          // $('#btnCurrentMonth').text(monthNames[this.currentMonth].toUpperCase());
          // $('#btnCurrentYear').text(this.currentYear);
        }

        var all = true;
        var none = true;
        var someString = "";
        var count = 0;
        var onlyOneString;
        for (var i = 0; i < self.combineSelectedMonths.length; i++) {
          var selected = self.combineSelectedMonths[i];
          if (selected) {
            none = false;
            someString += monthNames[i].substring(0, 1);
            count++;
            onlyOneString = monthNames[i];
          }
          else {
            all = false;
            someString += "-";
          }
        }
        if (all || none) {
          $('#btnCurrentMonth').text("ALL");
        }
        else {
          // $('#btnCurrentMonth').text("SOME");
          if (count == 1) {
            $('#btnCurrentMonth').text(onlyOneString.toUpperCase());
          }
          else {
            $('#btnCurrentMonth').text(someString.toLowerCase());
          }
        }
      }
      else {
        $('#btnCurrentDayCenter').text(this.currentDay);
        // $('#btnCurrentDayWeek').text(weekDayNames[currentDate.getDay()].toUpperCase());
        $('#btnCurrentMonth').text(monthNames[this.currentMonth].toUpperCase());
        // $('#btnCurrentYear').text(this.currentYear);
      }

      var startYear = new Date(self.realInitialStartTime * 1000).getFullYear();
      var all = true;
      var none = true;
      var count = 0;
      var onlyOneString;
      for (var i = 0; i < self.combineSelectedYears.length; i++) {
        var selectedYear = self.combineSelectedYears[i];
        if (selectedYear) {
          none = false;
          count++;
          onlyOneString = i + startYear;
        }
        else {
          all = false;
        }
      }

      if (all || none) {
        $('#btnCurrentYear').text("ALL");
      }
      else {
        // $('#btnCurrentYear').text("SOME");
        if (count == 1) {
          $('#btnCurrentYear').text(onlyOneString);
        }
        else {
          // $('#btnCurrentYear').text("SOME");
          $('#btnCurrentYear').text(count + " yrs");
        }
      }
    }
    else {
      $('#btnCurrentDayCenter').text("");
      $('#btnCurrentDayMonth').text(this.currentDay);
      $('#btnCurrentDayWeek').text(weekDayNames[currentDate.getDay()].toUpperCase());
      $('#btnCurrentMonth').text(monthNames[this.currentMonth].toUpperCase());
      $('#btnCurrentYear').text(this.currentYear);
    }

    $('#btnCurrentTime').text(currentTime);
  }

  function setTimeLabel(labelId, currentTime) {
    // var currentDate = new Date(currentTime * 1000 + 1000 * 60 * 60 * 2);
    var currentDate = new Date(currentTime * 1000);
    // var dateTimeStrings = currentDate.toISOString().slice(0, 19).split("T"); // .replace("T", " ")
    // var dateString = dateTimeStrings[0];
    // var timeString = dateTimeStrings[1];
    var dateString;
    if (self.combineMode == 0) {
      dateString = currentDate.toUTCString().slice(5, 16).toUpperCase();
    }
    if (self.combineMode == 1) {
      dateString = currentDate.toUTCString().slice(5, 11).toUpperCase();
    }
    if (self.combineMode == 2) {
      dateString = currentDate.toUTCString().slice(5, 7).toUpperCase();
    }
    if (self.combineMode == 3) {
      dateString = "";
    }

    var timeString = currentDate.toTimeString().slice(0, 8);

    $('#date' + labelId).text(dateString);
    $('#time' + labelId).text(timeString);
  }

  function connectButtons(self) {

    /* Jump modes:
     0 : 1 year
     1 : 1 month
     2 : 1 week
     3 : 1 day
     4 : 1 hour
     5 : 5 minutes
     6 : 1 minute
     */

    $('#btnChooseJump').on("click", function() {
      $('#btnGrpChooseJump').toggleClass('hidden');
    });

    /**
     *
     <button class="btn btn-default btnComb" id="btnJump1Min">1 MIN</button>
     <button class="btn btn-default btnComb" id="btnJump5Min">5 MIN</button>
     <button class="btn btn-default btnComb" id="btnJump1Hour">1 HOUR</button>
     <button class="btn btn-default btnComb" id="btnJump1Day">1 DAY</button>
     <button class="btn btn-default btnComb" id="btnJump1Week">1 WEEK</button>
     <button class="btn btn-default btnComb" id="btnJump1Month">1 MONTH</button>
     <button class="btn btn-default btnComb" id="btnJump1Year">1 YEAR</button>
     */


    $('.btnJump').on("click", function() {
      $('#btnGrpChooseJump').addClass('hidden');
      currentJumpMode = parseInt($(this).attr('jumpMode'));
      $('#btnChooseJump').text(jumpModes[currentJumpMode].text);
    });

    var overviewRangeFitted = false;
    $('#btnFitOverviewRange').on("click", function() {
      if (overviewRangeFitted) {
        overviewRangeFitted = false;
        $('#btnFitOverviewRange span').toggleClass('glyphicon-zoom-out glyphicon-zoom-in');

        self.startTime = self.initialStartTime;
        self.endTime = self.initialEndTime;

        var bounds = ShapeFactory.createBounds(DynamicTimeSlider.REFERENCE,
            [self.startTime, self.endTime - self.startTime, self.overView.mapBounds.y, self.overView.mapBounds.height]);
        self.overView.mapNavigator.fit({bounds: bounds, animate: true, allowWarpXYAxis: true});

        // self.histogram.refit(self.startTime, self.endTime);
      }
      else {
        overviewRangeFitted = true;
        $('#btnFitOverviewRange span').toggleClass('glyphicon-zoom-in glyphicon-zoom-out');

        self.startTime = self.mapBounds.x;
        self.endTime = self.startTime + self.mapBounds.width;

        var bounds = ShapeFactory.createBounds(DynamicTimeSlider.REFERENCE,
            [self.startTime, self.endTime - self.startTime, self.overView.mapBounds.y, self.overView.mapBounds.height]);
        self.overView.mapNavigator.fit({bounds: bounds, animate: true, allowWarpXYAxis: true});
      }
    });

    self.overView.on('idle', function() {
      if (self.histogram) {
        self.histogram.refit(self.overView.mapBounds.x, self.overView.mapBounds.x + self.overView.mapBounds.width);
      }
    });

    $('#btnAnalysis').on("click", function() {
      if (flagReplay) {
        flagReplay = false;
        $('#btnAnalysis').addClass('active');
        $('#btnReplay').removeClass('active');
      }
    });
    $('#btnReplay').on("click", function() {
      if (!flagReplay) {
        flagReplay = true;
        $('#btnAnalysis').removeClass('active');
        $('#btnReplay').addClass('active');
      }
    });

    $('#btnLockRange').on("click", function() {
      if (flagLockRange) {
        flagLockRange = false;
        $('#btnLockRange').removeClass('active');
      }
      else {
        flagLockRange = true;
        $('#btnLockRange').addClass('active');
      }
    });

    var flagChoosingRange = false;
    $('#btnChooseRange').on("click", function() {
      $('#btnGrpChooseRange').toggleClass('hidden');

      if (flagChoosingRange) {
        $('#logo-luciad').css('bottom', '270px');
        $('#timelabelstartpicker').data('DateTimePicker').hide();
        $('#timelabelendpicker').data('DateTimePicker').hide();
        flagChoosingRange = false;
      }
      else {
        $('#logo-luciad').css('bottom', '640px');
        flagChoosingRange = true;

        var currentStartDate = moment(self.mapBounds.x * 1000);
        $('#timelabelstartpicker').data('DateTimePicker').viewDate(currentStartDate);

        $('#timelabelstartpicker').data('DateTimePicker').showClose(false);
        $('#timelabelstartpicker').data('DateTimePicker').show();
        $('#datetimelabelstart .bootstrap-datetimepicker-widget').removeClass('pickRangeStart');
        $('#datetimelabelstart .bootstrap-datetimepicker-widget').addClass('chooseRangeStart');

        $(".glyphicon-trash").parent().removeAttr("data-action");
        $(".glyphicon-trash").parent().click(function() {
          $('#timelabelstartpicker').data("DateTimePicker").date(startDate);
          $('#timelabelstartpicker').data('DateTimePicker').viewDate(startDate);
        });
        // $(".glyphicon-trash").toggleClass('glyphicon-trash glyphicon-step-backward');

        var currentEndDate = moment((self.mapBounds.x + self.mapBounds.width) * 1000);
        $('#timelabelendpicker').data('DateTimePicker').viewDate(currentEndDate);

        $('#timelabelendpicker').data('DateTimePicker').showClose(false);
        $('#timelabelendpicker').data('DateTimePicker').show();
        $('#datetimelabelend .bootstrap-datetimepicker-widget').removeClass('pickRangeEnd');
        $('#datetimelabelend .bootstrap-datetimepicker-widget').addClass('chooseRangeEnd');

        $(".glyphicon-trash").parent().removeAttr("data-action");
        $(".glyphicon-trash").parent().click(function() {
          $('#timelabelendpicker').data("DateTimePicker").date(currentEndDate);
          $('#timelabelendpicker').data('DateTimePicker').viewDate(currentEndDate);
        });
        // $(".glyphicon-trash").toggleClass('glyphicon-trash glyphicon-step-forward');

      }
    });

    $('#btnRangeYears').on("click", function() {
      var years = parseInt($('#inpRangeYears').attr('value'));
      setRange(years * yearInSeconds, false);
    });

    $("#inpRangeYears").on("change paste keyup", function() {
      var newRangeYears = parseInt($("#inpRangeYears").val());
      if (!isNaN(newRangeYears)) {
        var range = getRange();
        var rangeYears = range / yearInSeconds | 0;
        var newRange = range + (newRangeYears - rangeYears) * yearInSeconds;
        $("#inpRangeYears").val(newRangeYears);
        setRange(newRange, false);
      }
    });

    $('#btnRangeYearsPlus').on("click", function() {
      setRange(getRange() + yearInSeconds, false);
    });

    $('#btnRangeYearsMin').on("click", function() {
      setRange(getRange() - yearInSeconds, false);
    });

    $('#btnRangeDays').on("click", function() {
      var days = parseInt($('#inpRangeDays').attr('value'));
      setRange(days * dayInSeconds, false);
    });

    $("#inpRangeDays").on("change paste keyup", function() {
      var newRangeDays = parseInt($("#inpRangeDays").val());
      if (!isNaN(newRangeDays)) {
        var range = getRange();
        var rangeDays = (range / dayInSeconds) % 365 | 0;
        var newRange = range + (newRangeDays - rangeDays) * dayInSeconds;
        $("#inpRangeDays").val(newRangeDays);
        setRange(newRange, false);
      }
    });

    $('#btnRangeDaysPlus').on("click", function() {
      setRange(getRange() + dayInSeconds, false);
    });

    $('#btnRangeDaysMin').on("click", function() {
      setRange(getRange() - dayInSeconds, false);
    });

    $('#btnRangeHours').on("click", function() {
      var hours = parseInt($('#inpRangeHours').attr('value'));
      setRange(hours * hourInSeconds, false);
    });

    $("#inpRangeHours").on("change paste keyup", function() {
      var newRangeHours = parseInt($("#inpRangeHours").val());
      if (!isNaN(newRangeHours)) {
        var range = getRange();
        var rangeHours = (range / hourInSeconds) % 24 | 0;
        var newRange = range + (newRangeHours - rangeHours) * hourInSeconds;
        $("#inpRangeHours").val(newRangeHours);
        setRange(newRange, false);
      }
    });

    $('#btnRangeHoursPlus').on("click", function() {
      setRange(getRange() + hourInSeconds, false);
    });

    $('#btnRangeHoursMin').on("click", function() {
      setRange(getRange() - hourInSeconds, false);
    });

    $('#btnRangeMinutes').on("click", function() {
      var minutes = parseInt($('#inpRangeMinutes').attr('value'));
      setRange(minutes * minuteInSeconds, false);
    });

    $("#inpRangeMinutes").on("change paste keyup", function() {
      var newRangeMinutes = parseInt($("#inpRangeMinutes").val());
      if (!isNaN(newRangeMinutes)) {
        var range = getRange();
        var rangeMinutes = (range / minuteInSeconds) % 60 | 0;
        var newRange = range + (newRangeMinutes - rangeMinutes) * minuteInSeconds;
        $("#inpRangeMinutes").val(newRangeMinutes);
        setRange(newRange, false);
      }
    });

    $('#btnRangeMinutesPlus').on("click", function() {
      setRange(getRange() + minuteInSeconds, false);
    });

    $('#btnRangeMinutesMin').on("click", function() {
      setRange(getRange() - minuteInSeconds, false);
    });

    var playing = false;
    var lastTime = 0;
    var currTimeLineTime;// = timeSlider.getCurrentTime();

    function playStep(currentTimeInMilliseconds) {
      var currRealTimeInSeconds = (currentTimeInMilliseconds / 1000);
      var currRealDeltaTimeInSeconds = currRealTimeInSeconds - lastTime;
      var currTimeScale = self.mapScale[0]; //in cm/seconds
      var deltaTimeLineTime = currRealDeltaTimeInSeconds / currTimeScale; //always try to advance time by 1cm/sec
      currTimeLineTime += deltaTimeLineTime;
      self.setCurrentTime(currTimeLineTime);
      lastTime = currRealTimeInSeconds;
      if (playing) {
        requestAnimationFrame(playStep);
      }
    }

    var startPlaying = function() {
      playing = !playing;
      if (playing) {
        currTimeLineTime = self.getCurrentTime();
        lastTime = performance.now() / 1000;
        requestAnimationFrame(playStep);
      }
      $('#btnPlay').toggleClass('active');
      $('#btnPlay').toggleClass('glyphicon-pause', playing);
      $('#btnPlay').toggleClass('glyphicon-play', !playing);
    };
    $('#btnPlay').on("click", startPlaying);

    // var btnToggleTime = dom.byId("btnToggleTime");
    // var flagTime = true;
    // btnToggleTime.onclick = function() {
    //   if (flagTime) {
    //     flagTime = false;
    //     $('#btnToggleTime').removeClass('active');
    //     $('#timelabelstartpicker').data('DateTimePicker').hide();
    //     $('#timelabelendpicker').data('DateTimePicker').hide();
    //     $('#logo-luciad').css('bottom', '10px');
    //     $('#timemenu').css('bottom', '10px');
    //   }
    //   else {
    //     flagTime = true;
    //     $('#btnToggleTime').addClass('active');
    //     $('#logo-luciad').css('bottom', '220px');
    //     $('#timemenu').css('bottom', '210px');
    //     // timeSlider.resize();
    //   }
    // };

    $('#btnFitTimeRange').on("click", function() {
      self.refit();
    });

    // $('#btn1Year').on("click", function() {
    //   setRange(yearInSeconds);
    // });
    //
    // $('#btn1Week').on("click", function() {
    //   setRange(weekInSeconds);
    // });
    //
    // $('#btn1Day').on("click", function() {
    //   setRange(dayInSeconds);
    // });
    //
    // $('#btn1Hour').on("click", function() {
    //   setRange(hourInSeconds);
    // });
    //
    // $('#btn1Minute').on("click", function() {
    //   setRange(minuteInSeconds);
    // });

    self.setOverviewMode(-1);
    self.combineMode = 0;

    var currentButtonClicked = function(btnId) {
      if (self.combineMode == 0) {
        $('.btnCurrentLcd').removeClass('active');
        $('#btnRemoveTimeFilter').removeClass('hidden');
        $(btnId).addClass('active');
        $('#btnRemoveCombine').addClass("hidden-width");
      }

      // $('#btnStepBack').removeClass('disabled');
      // $('#btnStepForward').removeClass('disabled');
      // $('#btnStepFastForward').removeClass('disabled');
      // $('#btnStepFastBack').removeClass('disabled');
      $('#btnCombine').removeClass('disabled');
      $('.btnGrpCombine').addClass('hidden');
      $('#btnCompare').removeClass("active");
      $('.btnGrpCompare').addClass('hidden');
      $('#btnRemoveCompare').addClass("hidden");

      $('.lcdPicker').addClass('hidden');
      yearPickerVisible = false;
      monthPickerVisible = false;
      dayPickerVisible = false;
    };
    // $('.btnCurrentLcd').on("click", currentButtonClicked);

    $('#btnRemoveTimeFilter').on("click", function() {
      $('.btnCurrentLcd').removeClass('active');
      $('#btnRemoveTimeFilter').addClass('hidden');

      self.setOverviewMode(-1);
      self.resetOverview();
      currentJumpMode = 0;
      $('#btnChooseJump').text(jumpModes[currentJumpMode].text);

      overviewRangeFitted = false;
      $('#btnFitOverviewRange span').toggleClass('glyphicon-zoom-out glyphicon-zoom-in');

      self.startTime = self.initialStartTime;
      self.endTime = self.initialEndTime;

      var bounds = ShapeFactory.createBounds(DynamicTimeSlider.REFERENCE,
          [self.startTime, self.endTime - self.startTime, self.overView.mapBounds.y, self.overView.mapBounds.height]);
      self.overView.mapNavigator.fit({bounds: bounds, animate: true, allowWarpXYAxis: true});

      // self.histogram.refit(self.startTime, self.endTime);

      if (!flagLockRange) {
        if (flagReplay) {
          setRange(yearInSeconds);
        }
        else {
          self.refit();
        }
      }

      // $('#btnStepBack').addClass('disabled');
      // $('#btnStepForward').addClass('disabled');
      // $('#btnStepFastForward').addClass('disabled');
      // $('#btnStepFastBack').addClass('disabled');
      // $('#btnCombine').addClass('disabled');
      $('.btnGrpCombine').addClass('hidden');
    });

    $('#btnCombine').on("click", function() {
      $('.btnGrpCompare').addClass('hidden');
      // $('#btnGrpCombineYears').css('top', '-64px');
      // // $('#btnGrpCombineYears').css('width', '308px');
      // $('#btnGrpCombineYears').css('width', '107px');
      // // $('#btnGrpCombineMonths').css('top', '-184px');
      // $('#btnGrpCombineMonths').css('top', '-118px');
      // // $('#btnGrpCombineMonths').css('width', '308px');
      // // $('#btnGrpCombineDays').css('top', '-469px');
      // $('#btnGrpCombineDays').css('top', '-172px');
      $('#btnGrpCombineDays').css('top', '-130px');
      // $('#btnGrpCombineDays').css('width', '107px');
      $('#btnGrpCombineDays').css('width', '133px');
      $('#btnGrpCombineDays').toggleClass('hidden');
      // $('#btnGrpCombineMonths').toggleClass('hidden');
      // $('#btnGrpCombineYears').toggleClass('hidden');

      // var mode = self.getOverviewMode();
      // $('#btnGrpCombineYears').css('top', '-64px');
      // $('#btnGrpCombineYears').css('width', '');
      // $('#btnGrpCombineMonths').css('top', '-130px');
      // $('#btnGrpCombineMonths').css('width', '');
      // $('#btnGrpCombineDays').css('top', '-295px');
      // if (mode == -1) {
      //   $('#btnGrpCombineYears').css('top', '-64px');
      //   $('#btnGrpCombineYears').css('width', '308px');
      //   $('#btnGrpCombineMonths').css('top', '-184px');
      //   $('#btnGrpCombineMonths').css('width', '308px');
      //   $('#btnGrpCombineDays').css('top', '-469px');
      //   $('#btnGrpCombineDays').toggleClass('hidden');
      //   $('#btnGrpCombineMonths').toggleClass('hidden');
      //   $('#btnGrpCombineYears').toggleClass('hidden');
      // }
      // if (mode == 0) {
      //   $('#btnGrpCombineYears').toggleClass('hidden');
      // }
      // if (mode == 1) {
      //   // $('.btnGrpCombine').not('#btnGrpCombineMonths').addClass('hidden');
      //   $('#btnGrpCombineMonths').toggleClass('hidden');
      // }
      // if (mode == 2) {
      //   // $('.btnGrpCombine').not('#btnGrpCombineDays').addClass('hidden');
      //   $('#btnGrpCombineDays').toggleClass('hidden');
      // }
      // if (mode == 3) {
      //   // $('.btnGrpCombine').not('#btnGrpCombineHours').addClass('hidden');
      //   $('#btnGrpCombineHours').toggleClass('hidden');
      // }
    });

    $('.btnGrpCombine button').on("click", function() {
      $('#btnCombine').addClass("active");
      $('#btnRemoveCombine').removeClass("hidden-width");
      $('.btnGrpCombine').addClass('hidden');
    });

    $('#btnRemoveCombine').on("click", function() {
      flagsAll.year = false;
      flagsAll.month = false;
      flagsAll.dayOfWeek = false;
      flagsAll.dayOfMonth = false;
      flagsEmpty.year = false;
      flagsEmpty.month = false;
      flagsEmpty.dayOfWeek = false;
      flagsEmpty.dayOfMonth = false;
      // $('#btnCurrentDayWeek').text("WED");
      // $('#btnCurrentDayMonth').text("22");
      // $('#btnCurrentMonth').text("NOVEMBER");
      // $('#btnCurrentYear').text("2005");

      $('#btnCombine').removeClass("active");
      // $('#btnCombine').addClass("disabled");
      $('#btnRemoveCombine').addClass("hidden-width");

      $('.btnCurrentLcd').removeClass('active');

      flagsAll.year = false;
      // and all the others as well...

      // var start2016 = new Date();
      var startTime = self.realInitialStartTime;
      var endTime = self.realInitialEndTime;
      self.startTime = self.realInitialStartTime;
      self.endTime = self.realInitialEndTime;
      self.initialStartTime = self.realInitialStartTime;
      self.initialEndTime = self.realInitialEndTime;

      // self.overView = new DynamicTimeSlider(self.overViewDomNode, 1);

      var currentTime = ( startTime + endTime ) * 0.5;
      self.overView.setValidRange(startTime, endTime, 0, 1000);
      self.overView.setCurrentTime(currentTime);
      self.setValidRange(startTime, endTime, 0, 1000);
      self.setCurrentTime(currentTime);
      self.combineMode = 0;

      currentJumpMode = 0;
      $('#btnChooseJump').text(jumpModes[currentJumpMode].text);

      var combineMode = self.combineMode;
      var transform = function(time) {
        if (combineMode >= 1) {
          var thisDate = new Date(time * 1000);
          var startDate;
          if (combineMode == 1) {
            startDate = new Date(thisDate.getFullYear(), 0, 0);
          }
          if (combineMode == 2) {
            startDate = new Date(thisDate.getFullYear(), thisDate.getMonth(), 0);
          }
          if (combineMode == 3) {
            startDate = new Date(thisDate.getFullYear(), thisDate.getMonth(), thisDate.getDate());
          }
          time = ( thisDate.getTime() - startDate.getTime() ) / 1000 | 0;
        }
        return time;
      };
      self.histogram.setCombineMode(self.combineMode, transform);
      self.combineModeChangedCallback(transform);

      setTimeLabels(startTime, endTime);
    });

    $('.btnGrpCompare button').on("click", function() {
      $('#btnCompare').addClass("active");
      $('#btnRemoveCompare').removeClass("hidden");
      $('.btnGrpCompare').addClass('hidden');
    });

    $('.btnComb').on("click", function() {
      $('.btnCurrentLcd').removeClass('active');
      flagsAll.year = false;
      flagsAll.month = false;
      flagsAll.dayOfWeek = false;
      flagsAll.dayOfMonth = false;
      flagsEmpty.year = false;
      flagsEmpty.month = false;
      flagsEmpty.dayOfWeek = false;
      flagsEmpty.dayOfMonth = false;

      $('#btnRemoveTimeFilter').addClass('hidden');
      self.setOverviewMode(-1);

      // $('#btnStepBack').addClass('disabled');
      // $('#btnStepForward').addClass('disabled');
      // $('#btnStepFastForward').addClass('disabled');
      // $('#btnStepFastBack').addClass('disabled');
    });

    $('#btnCombineAllDays').on("click", function() {
      currentJumpMode = 4;
      $('#btnChooseJump').text(jumpModes[currentJumpMode].text);

      $('#btnCurrentDay').addClass('active');
      // $('#btnCurrentDayWeek').text("");
      // $('#btnCurrentDayMonth').text("ALL");
      // $('#btnCurrentMonth').text("ALL");
      // $('#btnCurrentYear').text("ALL");
      flagsAll.year = true;
      flagsAll.month = true;
      flagsAll.dayOfMonth = true;
      flagsEmpty.dayOfWeek = true;

      var startTime = 0;
      var endTime = 24 * 60 * 60;

      self.startTime = startTime;
      self.endTime = endTime;

      // self.overView = new DynamicTimeSlider(self.overViewDomNode, 1);

      var currentTime = ( startTime + endTime ) * 0.5;
      self.overView.setValidRange(startTime, endTime, 0, 1000);
      self.overView.setCurrentTime(currentTime);
      self.setValidRange(startTime, endTime, 0, 1000);
      self.setCurrentTime(currentTime);
      self.combineMode = 3;

      // self.emit("CombineModeChange", {});
      var combineMode = self.combineMode;
      var transform = function(time) {
        if (combineMode >= 1) {
          var thisDate = new Date(time * 1000);
          var startDate;
          if (combineMode == 1) {
            startDate = new Date(thisDate.getFullYear(), 0, 0);
          }
          if (combineMode == 2) {
            startDate = new Date(thisDate.getFullYear(), thisDate.getMonth(), 0);
          }
          if (combineMode == 3) {
            startDate = new Date(thisDate.getFullYear(), thisDate.getMonth(), thisDate.getDate());
          }
          time = ( thisDate.getTime() - startDate.getTime() ) / 1000 | 0;
        }
        return time;
      };
      self.histogram.setCombineMode(self.combineMode, transform);
      self.combineModeChangedCallback(transform);
      setTimeLabels(startTime, endTime);
    });

    $('#btnCombineAllMonths').on("click", function() {
      currentJumpMode = 2;
      $('#btnChooseJump').text(jumpModes[currentJumpMode].text);

      $('#btnCurrentMonth').addClass('active');
      // $('#btnCurrentDayWeek').text("");
      // $('#btnCurrentMonth').text("ALL");
      // $('#btnCurrentYear').text("ALL");
      flagsAll.year = true;
      flagsAll.month = true;
      flagsEmpty.dayOfWeek = true;

      // $('#btnRemoveTimeFilter').addClass('hidden');
      // self.setOverviewMode(-1);
      //
      // $('#btnStepBack').addClass('disabled');
      // $('#btnStepForward').addClass('disabled');
      // $('#btnStepFastForward').addClass('disabled');
      // $('#btnStepFastBack').addClass('disabled');

      var startTime = 0;
      var endTime = 31 * 24 * 60 * 60;

      self.startTime = startTime;
      self.endTime = endTime;

      // self.overView = new DynamicTimeSlider(self.overViewDomNode, 1);

      var currentTime = ( startTime + endTime ) * 0.5;
      self.overView.setValidRange(startTime, endTime, 0, 1000);
      self.overView.setCurrentTime(currentTime);
      self.setValidRange(startTime, endTime, 0, 1000);
      self.setCurrentTime(currentTime);
      self.combineMode = 2;

      // self.emit("CombineModeChange", {});
      var combineMode = self.combineMode;
      var transform = function(time) {
        if (combineMode >= 1) {
          var thisDate = new Date(time * 1000);
          var startDate;
          if (combineMode == 1) {
            startDate = new Date(thisDate.getFullYear(), 0, 0);
          }
          if (combineMode == 2) {
            startDate = new Date(thisDate.getFullYear(), thisDate.getMonth(), 0);
          }
          if (combineMode == 3) {
            startDate = new Date(thisDate.getFullYear(), thisDate.getMonth(), thisDate.getDate());
          }
          time = ( thisDate.getTime() - startDate.getTime() ) / 1000 | 0;
        }
        return time;
      };
      self.histogram.setCombineMode(self.combineMode, transform);
      self.combineModeChangedCallback(transform);
      setTimeLabels(startTime, endTime);
    });

    $('#btnCombineAllYears').on("click", function() {
      currentJumpMode = 1;
      $('#btnChooseJump').text(jumpModes[currentJumpMode].text);

      $('#btnCurrentYear').addClass('active');

      // $('#btnCurrentYear').text("ALL");
      // $('#btnRemoveTimeFilter').addClass('hidden');
      // self.setOverviewMode(-1);
      //
      // $('#btnStepBack').addClass('disabled');
      // $('#btnStepForward').addClass('disabled');
      // $('#btnStepFastForward').addClass('disabled');
      // $('#btnStepFastBack').addClass('disabled');

      flagsAll.year = true;
      flagsEmpty.dayOfWeek = true;

      var startTime = 0;
      var endTime = 366 * 24 * 60 * 60;

      self.startTime = startTime;
      self.endTime = endTime;

      // self.overView = new DynamicTimeSlider(self.overViewDomNode, 1);

      var currentTime = ( startTime + endTime ) * 0.5;
      self.overView.setValidRange(startTime, endTime, 0, 1000);
      self.overView.setCurrentTime(currentTime);
      self.setValidRange(startTime, endTime, 0, 1000);
      self.setCurrentTime(currentTime);
      self.combineMode = 1;

      // self.emit("CombineModeChange", {});

      var combineMode = self.combineMode;
      var transform = function(time) {
        if (combineMode >= 1) {
          var thisDate = new Date(time * 1000);
          var startDate;
          if (combineMode == 1) {
            startDate = new Date(thisDate.getFullYear(), 0, 0);
          }
          if (combineMode == 2) {
            startDate = new Date(thisDate.getFullYear(), thisDate.getMonth(), 0);
          }
          if (combineMode == 3) {
            startDate = new Date(thisDate.getFullYear(), thisDate.getMonth(), thisDate.getDate());
          }
          time = ( thisDate.getTime() - startDate.getTime() ) / 1000 | 0;
        }
        return time;
      };
      self.histogram.setCombineMode(self.combineMode, transform);
      self.combineModeChangedCallback(transform);
      setTimeLabels(startTime, endTime);
    });

    $('#btnCompare').on("click", function() {
      $('.btnGrpCombine').addClass('hidden');
      $('#btnGrpCompare').toggleClass('hidden');
    });

    // var mode = 0;
    var currentTime;
    var currentYear;
    var currentMonth;
    var currentDay;
    var currentHour;
    var currentMinute;

    $('#btnStepForward').on("click", function() {
      currentTime = ( self.mapBounds.x + 0.5 * self.mapBounds.width ) | 0;
      var currentDate = new Date(currentTime * 1000);
      currentYear = currentDate.getFullYear();
      currentMonth = currentDate.getMonth();
      currentDay = currentDate.getDate();
      currentHour = currentDate.getHours();
      currentMinute = currentDate.getMinutes();

      var mode = self.getOverviewMode();
      var defaultJump = function() {
        var newTime = jumpModes[currentJumpMode].jump(currentTime, true);
        jumpTimeSlider(newTime - currentTime);
      }
      if (mode < 0) {
        defaultJump();
      }
      if (mode == 0) {
        if (currentJumpMode == 0) {
          currentYear++;
          var startTime = new Date(currentYear, 0, 1, 0, 0, 0, 0).getTime() / 1000 | 0;
          var endTime = new Date(currentYear + 1, 0, 1, 0, 0, 0, 0).getTime() / 1000 | 0;
          setStartAndEnd(startTime, endTime);
        }
        else {
          defaultJump();
        }
      }
      if (mode == 1) {
        if (currentJumpMode == 1) {
          currentMonth++;
          var startTime = new Date(currentYear, currentMonth, 1, 0, 0, 0, 0).getTime() / 1000 | 0;
          var endTime = new Date(currentYear, currentMonth + 1, 1, 0, 0, 0, 0).getTime() / 1000 | 0;
          setStartAndEnd(startTime, endTime);
        }
        else {
          defaultJump();
        }
      }
      if (mode == 2) {
        if (currentJumpMode == 3) {
          currentDay++;
          var startTime = new Date(currentYear, currentMonth, currentDay, 0, 0, 0, 0).getTime() / 1000 | 0;
          var endTime = new Date(currentYear, currentMonth, currentDay + 1, 0, 0, 0, 0).getTime() / 1000 | 0;
          setStartAndEnd(startTime, endTime);
        }
        else {
          defaultJump();
        }
      }
      // var mode = self.getOverviewMode();
      // if (mode == 0) {
      //   currentYear++;
      //   fitOverviewOnYear(currentYear, false);
      //   jumpTimeSlider(yearInSeconds);
      // }
      // if (mode == 1) {
      //   var previousDay = new Date(currentYear, currentMonth, currentDay, 0, 0, 0, 0).getTime() / 1000 | 0;
      //   currentMonth++;
      //   var nextDay = new Date(currentYear, currentMonth, currentDay, 0, 0, 0, 0).getTime() / 1000 | 0;
      //
      //   var jump = nextDay - previousDay;
      //
      //   fitOverviewOnMonth(currentYear, currentMonth, false);
      //   jumpTimeSlider(jump);
      // }
      // if (mode == 2) {
      //   currentDay++;
      //   fitOverviewOnDay(currentYear, currentMonth, currentDay, false);
      //   jumpTimeSlider(dayInSeconds);
      // }
      // if (mode == 3) {
      //   currentHour++;
      //   fitOverviewOnHour(currentYear, currentMonth, currentDay, currentHour, false);
      //   jumpTimeSlider(hourInSeconds);
      // }
    });

    $('#btnStepFastForward').on("click", function() {
      var mode = self.getOverviewMode();
      if (mode == 0) {
        currentYear += 10;
        fitOverviewOnYear(currentYear, false);
        jumpTimeSlider(10 * yearInSeconds);
      }
      if (mode == 1) {
        var previousDay = new Date(currentYear, currentMonth, currentDay, 0, 0, 0, 0).getTime() / 1000 | 0;
        currentYear++;
        var nextDay = new Date(currentYear, currentMonth, currentDay, 0, 0, 0, 0).getTime() / 1000 | 0;

        var jump = nextDay - previousDay;

        fitOverviewOnMonth(currentYear, currentMonth, false);
        jumpTimeSlider(jump);
      }
      if (mode == 2) {
        currentDay += 7;
        fitOverviewOnDay(currentYear, currentMonth, currentDay, false);
        jumpTimeSlider(7 * dayInSeconds);
      }
      if (mode == 3) {
        currentDay++;
        fitOverviewOnHour(currentYear, currentMonth, currentDay, currentHour, false);
        jumpTimeSlider(dayInSeconds);
      }
    });

    $('#btnStepFastBack').on("click", function() {
      var mode = self.getOverviewMode();
      if (mode == 0) {
        currentYear -= 10;
        fitOverviewOnYear(currentYear, false);
        jumpTimeSlider(-yearInSeconds);
      }
      if (mode == 1) {
        var previousDay = new Date(currentYear, currentMonth, currentDay, 0, 0, 0, 0).getTime() / 1000 | 0;
        currentYear--;
        var nextDay = new Date(currentYear, currentMonth, currentDay, 0, 0, 0, 0).getTime() / 1000 | 0;

        var jump = nextDay - previousDay;

        fitOverviewOnMonth(currentYear, currentMonth, false);
        jumpTimeSlider(jump);
      }
      if (mode == 2) {
        currentDay -= 7;
        fitOverviewOnDay(currentYear, currentMonth, currentDay, false);
        jumpTimeSlider(-7 * dayInSeconds);
      }
      if (mode == 3) {
        currentDay--;
        fitOverviewOnHour(currentYear, currentMonth, currentDay, currentHour, false);
        jumpTimeSlider(-dayInSeconds);
      }
    });

    $('#btnStepBack').on("click", function() {
      currentTime = ( self.mapBounds.x + 0.5 * self.mapBounds.width ) | 0;
      var currentDate = new Date(currentTime * 1000);
      currentYear = currentDate.getFullYear();
      currentMonth = currentDate.getMonth();
      currentDay = currentDate.getDate();
      currentHour = currentDate.getHours();
      currentMinute = currentDate.getMinutes();

      var mode = self.getOverviewMode();
      var defaultJump = function() {
        var newTime = jumpModes[currentJumpMode].jump(currentTime, false);
        jumpTimeSlider(newTime - currentTime);
      }
      if (mode < 0) {
        defaultJump();
      }
      if (mode == 0) {
        if (currentJumpMode == 0) {
          currentYear--;
          var startTime = new Date(currentYear, 0, 1, 0, 0, 0, 0).getTime() / 1000 | 0;
          var endTime = new Date(currentYear + 1, 0, 1, 0, 0, 0, 0).getTime() / 1000 | 0;
          setStartAndEnd(startTime, endTime);
        }
        else {
          defaultJump();
        }
      }
      if (mode == 1) {
        if (currentJumpMode == 1) {
          // var previousDay = new Date(currentYear, currentMonth, currentDay, 0, 0, 0, 0).getTime() / 1000 | 0;
          // currentMonth--;
          // var nextDay = new Date(currentYear, currentMonth, currentDay, 0, 0, 0, 0).getTime() / 1000 | 0;
          //
          // var jump = nextDay - previousDay;
          //
          // fitOverviewOnMonth(currentYear, currentMonth, false);
          // jumpTimeSlider(jump);

          currentMonth--;
          var startTime = new Date(currentYear, currentMonth, 1, 0, 0, 0, 0).getTime() / 1000 | 0;
          var endTime = new Date(currentYear, currentMonth + 1, 1, 0, 0, 0, 0).getTime() / 1000 | 0;
          setStartAndEnd(startTime, endTime);
        }
        else {
          defaultJump();
        }
      }
      if (mode == 2) {
        if (currentJumpMode == 3) {
          currentDay--;
          // fitOverviewOnDay(currentYear, currentMonth, currentDay, false);
          // jumpTimeSlider(-dayInSeconds);
          var startTime = new Date(currentYear, currentMonth, currentDay, 0, 0, 0, 0).getTime() / 1000 | 0;
          var endTime = new Date(currentYear, currentMonth, currentDay + 1, 0, 0, 0, 0).getTime() / 1000 | 0;
          setStartAndEnd(startTime, endTime);
        }
        else {
          defaultJump();
        }
      }
      // if (mode == 3) {
      //   currentHour--;
      //   fitOverviewOnHour(currentYear, currentMonth, currentDay, currentHour, false);
      //   jumpTimeSlider(-hourInSeconds);
      // }

      // if (mode < 0 ) {
      //   var newTime = currentJumpFunction(currentTime, false);
      //   jumpTimeSlider(newTime - currentTime);
      // }
      // if (mode == 0) {
      //   // currentYear--;
      //   // fitOverviewOnYear(currentYear, false);
      //   // jumpTimeSlider(-yearInSeconds);
      // }
      // if (mode == 1) {
      //   var previousDay = new Date(currentYear, currentMonth, currentDay, 0, 0, 0, 0).getTime() / 1000 | 0;
      //   currentMonth--;
      //   var nextDay = new Date(currentYear, currentMonth, currentDay, 0, 0, 0, 0).getTime() / 1000 | 0;
      //
      //   var jump = nextDay - previousDay;
      //
      //   fitOverviewOnMonth(currentYear, currentMonth, false);
      //   jumpTimeSlider(jump);
      // }
      // if (mode == 2) {
      //   currentDay--;
      //   fitOverviewOnDay(currentYear, currentMonth, currentDay, false);
      //   jumpTimeSlider(-dayInSeconds);
      // }
      // if (mode == 3) {
      //   currentHour--;
      //   fitOverviewOnHour(currentYear, currentMonth, currentDay, currentHour, false);
      //   jumpTimeSlider(-hourInSeconds);
      // }
    });

    function getCurrentDateTime() {
      currentTime = ( self.mapBounds.x + 0.5 * self.mapBounds.width ) | 0;
      var currentDate = new Date(currentTime * 1000);
      currentYear = currentDate.getFullYear();
      currentMonth = currentDate.getMonth();
      currentDay = currentDate.getDate();
      currentHour = currentDate.getHours();
      currentMinute = currentDate.getMinutes();
      // var currentSeconds = currentDate.getSeconds();
    }

    self.combineSelectedYears = [];
    var startYear = new Date(self.realInitialStartTime * 1000).getFullYear();
    var endYear = new Date(self.realInitialEndTime * 1000).getFullYear();
    for (var i = 1970; i < startYear; i++) {
      $('#lcdYearPicker span.year[year="' + i + '"]').addClass('disabled');
    }
    for (var i = startYear; i <= endYear; i++) {
      self.combineSelectedYears[i - startYear] = false;
    }
    for (var i = endYear; i < 2020; i++) {
      $('#lcdYearPicker span.year[year="' + i + '"]').addClass('disabled');
    }

    $('#lcdYearPicker span.year').on("click", function() {
      $(this).toggleClass('active');
      var year = parseInt($(this).text());
      self.combineSelectedYears[year - startYear] = $(this).hasClass('active');
      updateCombineSelection();
    });
    $('#btnYearPickerSelectAll').on("click", function() {
      $('#lcdYearPicker span.year').addClass('active');
      for (var i = 0; i < 12; i++) {
        self.combineSelectedYears[i] = true;
      }
      updateCombineSelection();
    });
    $('#btnYearPickerSelectNone').on("click", function() {
      $('#lcdYearPicker span.year').removeClass('active');
      for (var i = 0; i < 12; i++) {
        self.combineSelectedYears[i] = false;
      }
      updateCombineSelection();
    });
    $('#btnYearPickerClose').on("click", function() {
      $('#lcdYearPicker').addClass('hidden');
      yearPickerVisible = false;
    });

    $('#btnCurrentYear').on("click", function() {
      if (self.combineMode > 0) {
        if (self.combineMode > 0) {
          if (yearPickerVisible) {
            $('#lcdYearPicker').addClass('hidden');
            yearPickerVisible = false;
          }
          else {
            $('#lcdYearPicker').removeClass('hidden');
            yearPickerVisible = true;
          }
        }
        else {
          // do nothing
        }
      }
      else {

        if (flagReplay) {
          getCurrentDateTime();
          fitOverviewOnYear(currentYear, true);
          if (!flagLockRange) {
            setRange(31 * dayInSeconds, true);
          }

          self.setOverviewMode(0);
          currentJumpMode = 0;
          $('#btnChooseJump').text(jumpModes[currentJumpMode].text);
        }
        else {
          flagDoNotFitCurrent = true;
          var currentDate = moment(self.mapBounds.x * 1000 + self.mapBounds.width * 500);
          $('#currentTimePicker').data('DateTimePicker').viewDate(currentDate);
          $('#currentTimePicker').data('DateTimePicker').viewMode('years');
          $('#currentTimePicker').data('DateTimePicker').toggle();
          $('#divChooseCurrentTime .bootstrap-datetimepicker-widget').removeClass('chooseCurrentTimme');
          $('#divChooseCurrentTime .bootstrap-datetimepicker-widget').addClass('chooseYear');
          $('#divChooseCurrentTime .bootstrap-datetimepicker-widget').removeClass('chooseMonth');
          $('#divChooseCurrentTime .bootstrap-datetimepicker-widget').removeClass('chooseDay');

          var setupButtons = function() {
            $("#divChooseCurrentTime span.year").removeAttr("data-action");
            $("#divChooseCurrentTime span.year").off('click');
            $("#divChooseCurrentTime span.year").click(function() {
              var selectedYear = $(this).text();
              fitOverviewOnYear(selectedYear, true);
              $('#currentTimePicker').data('DateTimePicker').toggle();
              if (!flagLockRange) {
                self.refit();
              }
              currentButtonClicked('#btnCurrentYear');

              self.setOverviewMode(0);
              currentJumpMode = 0;
              $('#btnChooseJump').text(jumpModes[currentJumpMode].text);
            });
          }
          setupButtons();
          $("#currentTimePicker").on("dp.update", setupButtons);
        }
      }
    });

    var getMonthNumber = function(monthName) {
      if (monthName === "Jan") {
        return 0;
      }
      if (monthName === "Feb") {
        return 1;
      }
      if (monthName === "Mar") {
        return 2;
      }
      if (monthName === "Apr") {
        return 3;
      }
      if (monthName === "May") {
        return 4;
      }
      if (monthName === "Jun") {
        return 5;
      }
      if (monthName === "Jul") {
        return 6;
      }
      if (monthName === "Aug") {
        return 7;
      }
      if (monthName === "Sep") {
        return 8;
      }
      if (monthName === "Oct") {
        return 9;
      }
      if (monthName === "Nov") {
        return 10;
      }
      if (monthName === "Dec") {
        return 11;
      }
      return null;
    };

    self.combineSelectedMonths = [];
    for (var i = 0; i < 12; i++) {
      self.combineSelectedMonths[i] = false;
    }

    $('#lcdMonthPicker span.month').on("click", function() {
      $(this).toggleClass('active');
      var selectedMonth = $(this).attr('month');
      self.combineSelectedMonths[selectedMonth] = $(this).hasClass('active');
      updateCombineSelection();
    });
    $('#btnMonthPickerSelectAll').on("click", function() {
      $('#lcdMonthPicker span.month').addClass('active');
      for (var i = 0; i < 12; i++) {
        self.combineSelectedMonths[i] = true;
      }
      updateCombineSelection();
    });
    $('#btnMonthPickerSelectNone').on("click", function() {
      $('#lcdMonthPicker span.month').removeClass('active');
      for (var i = 0; i < 12; i++) {
        self.combineSelectedMonths[i] = false;
      }
      updateCombineSelection();
    });
    $('#btnMonthPickerClose').on("click", function() {
      $('#lcdMonthPicker').addClass('hidden');
      monthPickerVisible = false;
    });

    $('#btnCurrentMonth').on("click", function() {
      if (self.combineMode > 0) {
        if (self.combineMode > 1) {
          if (monthPickerVisible) {
            $('#lcdMonthPicker').addClass('hidden');
            monthPickerVisible = false;
          }
          else {
            $('#lcdMonthPicker').removeClass('hidden');
            monthPickerVisible = true;
          }
        }
        else {
          // do nothing
        }
      }
      else {
        if (flagReplay) {
          getCurrentDateTime();
          fitOverviewOnMonth(currentYear, currentMonth, true);
          if (!flagLockRange) {
            setRange(dayInSeconds, true);
          }

          currentJumpMode = 1;
          $('#btnChooseJump').text(jumpModes[currentJumpMode].text);
          self.setOverviewMode(1);
        }
        else {
          flagDoNotFitCurrent = true;
          var currentDate = moment(self.mapBounds.x * 1000 + self.mapBounds.width * 500);
          $('#currentTimePicker').data('DateTimePicker').viewDate(currentDate);
          $('#currentTimePicker').data('DateTimePicker').viewMode('months');
          $('#currentTimePicker').data('DateTimePicker').toggle();
          $('#divChooseCurrentTime .bootstrap-datetimepicker-widget').removeClass('chooseCurrentTimme');
          $('#divChooseCurrentTime .bootstrap-datetimepicker-widget').removeClass('chooseYear');
          $('#divChooseCurrentTime .bootstrap-datetimepicker-widget').addClass('chooseMonth');
          $('#divChooseCurrentTime .bootstrap-datetimepicker-widget').removeClass('chooseDay');
          // var selectedYear = $('#currentTimePicker').data('DateTimePicker').date().year();

          var setupButtons = function() {
            $("#divChooseCurrentTime span.month").removeAttr("data-action");
            $("#divChooseCurrentTime span.month").off('click');
            $("#divChooseCurrentTime span.month").click(function() {
              var monthName = $(this).text();
              var selectedMonth = getMonthNumber(monthName);

              var selectedYear = $('#currentTimePicker').data('DateTimePicker').viewDate().year();
              console.log(selectedYear);

              fitOverviewOnMonth(selectedYear, selectedMonth, true);
              $('#currentTimePicker').data('DateTimePicker').toggle();
              if (!flagLockRange) {
                self.refit();
              }
              currentButtonClicked('#btnCurrentMonth');
              currentJumpMode = 1;
              $('#btnChooseJump').text(jumpModes[currentJumpMode].text);
              self.setOverviewMode(1);
            });
            // $("#divChooseCurrentTime span.year").click(function() {
            //   selectedYear = $(this).text();
            // });
          }
          setupButtons();

          // $("#divChooseCurrentTime .next").click(function() {
          //   selectedYear++;
          // });
          // $("#divChooseCurrentTime .prev").click(function() {
          //   selectedYear--;
          // });

          $("#currentTimePicker").on("dp.update", setupButtons);
        }
      }
    });

    self.combineSelectedDays = [];
    for (var i = 0; i < 7; i++) {
      self.combineSelectedDays[i] = false;
    }

    $('#lcdDayPicker span.day').on("click", function() {
      $(this).toggleClass('active');
      var day = $(this).attr('day');
      self.combineSelectedDays[day] = $(this).hasClass('active');
      updateCombineSelection();
    });
    $('#btnDayPickerSelectAll').on("click", function() {
      $('#lcdDayPicker span.day').addClass('active');
      for (var i = 0; i < 7; i++) {
        self.combineSelectedDays[i] = true;
      }
      updateCombineSelection();
    });
    $('#btnDayPickerSelectNone').on("click", function() {
      $('#lcdDayPicker span.day').removeClass('active');
      for (var i = 0; i < 7; i++) {
        self.combineSelectedDays[i] = false;
      }
      updateCombineSelection();
    });
    $('#btnDayPickerClose').on("click", function() {
      $('#lcdDayPicker').addClass('hidden');
      dayPickerVisible = false;
    });

    $('#btnCurrentDay').on("click", function() {
      if (self.combineMode > 0) {
        if (self.combineMode > 2) {
          if (dayPickerVisible) {
            $('#lcdDayPicker').addClass('hidden');
            dayPickerVisible = false;
          }
          else {
            $('#lcdDayPicker').removeClass('hidden');
            dayPickerVisible = true;
          }
          // for (var i = 0; i < self.combineSelectedDays.length; i++) {
          //   if (self.combineSelectedDays[i]) {
          //     $('#lcdDayPicker span.day[day="' + i + '"]').addClass('active');
          //   }
          //   else {
          //     $('#lcdDayPicker span.day[day="' + i + '"]').removeClass('active');
          //   }
          // }
        }
        else {
          // do nothing
        }
      }
      else {
        self.setOverviewMode(2);
        if (flagReplay) {
          getCurrentDateTime();
          fitOverviewOnDay(currentYear, currentMonth, currentDay, true);
          if (!flagLockRange) {
            setRange(hourInSeconds, true);
          }
          currentJumpMode = 3;
          $('#btnChooseJump').text(jumpModes[currentJumpMode].text);
        }
        else {
          flagDoNotFitCurrent = true;

          var currentDate = moment(self.mapBounds.x * 1000 + self.mapBounds.width * 500);
          $('#currentTimePicker').data('DateTimePicker').viewDate(currentDate);
          $('#currentTimePicker').data('DateTimePicker').viewMode('days');
          $('#currentTimePicker').data('DateTimePicker').toggle();
          $('#divChooseCurrentTime .bootstrap-datetimepicker-widget').removeClass('chooseCurrentTimme');
          $('#divChooseCurrentTime .bootstrap-datetimepicker-widget').removeClass('chooseYear');
          $('#divChooseCurrentTime .bootstrap-datetimepicker-widget').removeClass('chooseMonth');
          $('#divChooseCurrentTime .bootstrap-datetimepicker-widget').addClass('chooseDay');
          var setupButtons = function() {
            $("#divChooseCurrentTime td.day").removeAttr("data-action");
            $("#divChooseCurrentTime td.day").off('click');
            $("#divChooseCurrentTime td.day").click(function() {
              var selectedDay = $(this).text();

              var selectedMonth = $('#currentTimePicker').data('DateTimePicker').viewDate().month();
              var selectedYear = $('#currentTimePicker').data('DateTimePicker').viewDate().year();

              fitOverviewOnDay(selectedYear, selectedMonth, selectedDay, true);
              $('#currentTimePicker').data('DateTimePicker').toggle();
              if (!flagLockRange) {
                self.refit();
              }
              currentButtonClicked('#btnCurrentDay');

              currentJumpMode = 3;
              $('#btnChooseJump').text(jumpModes[currentJumpMode].text);
            });
          }
          setupButtons();
          $("#currentTimePicker").on("dp.update", setupButtons);
        }
      }
    });

    // var dayLetters = ["M", "T", "W", "T", "F", "S", "S" ];
    // var updateCombineButtons = function() {
    //   if (self.combineMode > 0) {
    //     if (self.combineMode > 1) {
    //       if (self.combineMode > 2) {
    //         var all = true;
    //         var none = true;
    //         var someString = "";
    //         var count = 0;
    //         var onlyOneString;
    //         for (var i = 0; i < self.combineSelectedDays.length; i++) {
    //           var selected = self.combineSelectedDays[i];
    //           if (selected) {
    //             none = false;
    //             someString += weekDayNames[i].substring(0, 1);
    //             count++;
    //             onlyOneString = weekDayNames[i];
    //           }
    //           else {
    //             all = false;
    //             someString += "-";
    //           }
    //         }
    //         if (all || none) {
    //           $('#btnCurrentDay').text("ALL");
    //         }
    //         else {
    //           // $('#btnCurrentDay').text("SOME");
    //           if (count == 1) {
    //             $('#btnCurrentDay').text(onlyOneString.toUpperCase());
    //           }
    //           else {
    //             $('#btnCurrentDay').text(someString.toLowerCase());
    //           }
    //         }
    //       }
    //
    //       var all = true;
    //       var none = true;
    //       var someString = "";
    //       var count = 0;
    //       var onlyOneString;
    //       for (var i = 0; i < self.combineSelectedMonths.length; i++) {
    //         var selected = self.combineSelectedMonths[i];
    //         if (selected) {
    //           none = false;
    //           someString += monthNames[i].substring(0, 1);
    //           count++;
    //           onlyOneString = monthNames[i];
    //         }
    //         else {
    //           all = false;
    //           someString += "-";
    //         }
    //       }
    //       if (all || none) {
    //         $('#btnCurrentMonth').text("ALL");
    //       }
    //       else {
    //         // $('#btnCurrentMonth').text("SOME");
    //         if (count == 1) {
    //           $('#btnCurrentMonth').text(onlyOneString.toUpperCase());
    //         }
    //         else {
    //           $('#btnCurrentMonth').text(someString.toLowerCase());
    //         }
    //       }
    //     }
    //
    //     var startYear = new Date(self.realInitialStartTime * 1000).getFullYear();
    //     var all = true;
    //     var none = true;
    //     var count = 0;
    //     var onlyOneString;
    //     for (var i = 0; i < self.combineSelectedYears.length; i++) {
    //       var selectedYear = self.combineSelectedYears[i];
    //       if (selectedYear) {
    //         none = false;
    //         count++;
    //         onlyOneString = i + startYear;
    //       }
    //       else {
    //         all = false;
    //       }
    //     }
    //
    //     if (all || none) {
    //       $('#btnCurrentYear').text("ALL");
    //     }
    //     else {
    //       // $('#btnCurrentYear').text("SOME");
    //       if (count == 1) {
    //         $('#btnCurrentYear').text(onlyOneString);
    //       }
    //       else {
    //         // $('#btnCurrentYear').text("SOME");
    //         $('#btnCurrentYear').text(count + " yrs");
    //       }
    //     }
    //   }
    // };

    var updateCombineSelection = function() {
      // updateCombineButtons();
      // setTimeLabels(self.startTime, self.endTime);

      var startYear = new Date(self.realInitialStartTime * 1000).getFullYear();
      var all = true;
      var none = true;
      for (var i = 0; i < self.combineSelectedYears.length; i++) {
        var selectedYear = self.combineSelectedYears[i];
        if (selectedYear) {
          none = false;
        }
        else {
          all = false;
        }
      }
      var allYears = all || none;

      all = true;
      none = true;
      for (var i = 0; i < self.combineSelectedMonths.length; i++) {
        var selected = self.combineSelectedMonths[i];
        if (selected) {
          none = false;
        }
        else {
          all = false;
        }
      }
      var allMonths = all || none;

      all = true;
      none = true;
      for (var i = 0; i < self.combineSelectedDays.length; i++) {
        var selected = self.combineSelectedDays[i];
        if (selected) {
          none = false;
        }
        else {
          all = false;
        }
      }
      var allDays = all || none;

      var selectedYears = self.combineSelectedYears;
      var selectedMonths = self.combineSelectedMonths;
      var selectedDays = self.combineSelectedDays;
      var combineMode = self.combineMode;
      var filter = function(time) {
        if (combineMode >= 1) {
          var thisDate = new Date(time * 1000);
          var startDate;
          if (combineMode >= 1) {
            if (!allYears) {
              var year = thisDate.getFullYear();
              // console.log([year, year - startYear], selectedYears[year - startYear]);
              if (!selectedYears[year - startYear]) {
                return false;
              }
            }
          }
          if (combineMode >= 2) {
            if (!allMonths) {
              var month = thisDate.getMonth();
              if (!selectedMonths[month]) {
                return false;
              }
            }
          }
          if (combineMode >= 3) {
            if (!allDays) {
              var day = thisDate.getDay();
              if (!selectedDays[day]) {
                return false;
              }
            }
          }
          return true;
        }
      };

      self.histogram.setFilter(filter);
      self.combineModeFilterChangedCallback(filter);

      var mapBounds = self.mapBounds;
      var filterStart = (self.mapBounds.x | 0 );
      var filterWidth = (self.mapBounds.width | 0 );
      var filterEnd = filterStart + filterWidth;
      setTimeLabels(filterStart, filterEnd);
    };

    $('#btnCurrentTime').on("click", function() {
      self.setOverviewMode(3);
      if (flagReplay) {
        getCurrentDateTime();
        fitOverviewOnHour(currentYear, currentMonth, currentDay, currentHour, true);
        if (!flagLockRange) {
          setRange(5 * minuteInSeconds, true);
        }
      }
      else {
        flagDoNotFitCurrent = true;
        var currentDate = moment(self.mapBounds.x * 1000 + self.mapBounds.width * 500);
        $('#currentTimePicker').data('DateTimePicker').viewDate(currentDate);
        $('#currentTimePicker').data('DateTimePicker').viewMode('days');
        $('#currentTimePicker').data('DateTimePicker').toggle();
        $("#divChooseCurrentTime td.day").removeAttr("data-action");
        $("#divChooseCurrentTime td.day").click(function() {
          var selectedDay = $(this).text();
          console.log("fix: cant open time chooser by default, find another widget!!");
          getCurrentDateTime();
          fitOverviewOnHour(currentYear, currentMonth, selectedDay, currentHour, true);
          $('#currentTimePicker').data('DateTimePicker').toggle();
          if (!flagLockRange) {
            self.refit();
          }
        });
      }
    });

    function jumpTimeSlider(jump) {
      var bounds = ShapeFactory.createBounds(DynamicTimeSlider.REFERENCE,
          [self.mapBounds.x + jump, self.mapBounds.width, self.mapBounds.y, self.mapBounds.height]);
      self.mapNavigator.fit({bounds: bounds, animate: false, fitMargin: "0px", allowWarpXYAxis: true});
    }

    function setStartAndEnd(startTime, endTime) {
      var bounds = ShapeFactory.createBounds(DynamicTimeSlider.REFERENCE,
          [startTime, endTime - startTime, self.mapBounds.y, self.mapBounds.height]);
      self.mapNavigator.fit({bounds: bounds, animate: false, fitMargin: "0px", allowWarpXYAxis: true});
    }

    function fitOverviewOnYear(year, animate) {
      var date = new Date(year, 0, 1, 0, 0, 0, 0);
      var startTime = date.getTime() / 1000 | 0;
      self.setCurrentOverviewRange(startTime, startTime + yearInSeconds);
      setOverviewRange(startTime, yearInSeconds, animate);
    }

    function fitOverviewOnMonth(year, month, animate) {
      var date = new Date(year, month, 1, 0, 0, 0, 0);
      var startTime = date.getTime() / 1000 | 0;
      var thisMonthInSeconds = new Date(year, month + 1, 0).getDate() * dayInSeconds;
      self.setCurrentOverviewRange(startTime, startTime + thisMonthInSeconds);
      setOverviewRange(startTime, thisMonthInSeconds, animate);
    }

    function fitOverviewOnDay(year, month, day, animate) {
      var date = new Date(year, month, day, 0, 0, 0, 0);
      var startTime = date.getTime() / 1000 | 0;
      self.setCurrentOverviewRange(startTime, startTime + dayInSeconds);
      setOverviewRange(startTime, dayInSeconds, animate);
    }

    function fitOverviewOnHour(year, month, day, hour, animate) {
      var date = new Date(year, month, day, hour, 0, 0, 0);
      var startTime = date.getTime() / 1000 | 0;
      self.setCurrentOverviewRange(startTime, startTime + hourInSeconds);
      setOverviewRange(startTime, hourInSeconds, animate);
    }

    function setOverviewRange(startTime, range, animate) {
      // var bounds = ShapeFactory.createBounds(DynamicTimeSlider.REFERENCE,
      //     [startTime, range, self.overView.mapBounds.y, self.overView.mapBounds.height]);
      // self.overView.mapNavigator.fit({bounds: bounds, animate: animate, allowWarpXYAxis: true});
    }

    function setRange(range, animate) {
      var currentTime = ( self.mapBounds.x + 0.5 * self.mapBounds.width ) | 0;
      var startTime = currentTime - 0.5 * range;
      var bounds = ShapeFactory.createBounds(DynamicTimeSlider.REFERENCE,
          [startTime, range, self.mapBounds.y, self.mapBounds.height]);
      self.mapNavigator.fit({bounds: bounds, animate: animate, fitMargin: "0px", allowWarpXYAxis: true});
    }

    function getRange() {
      return self.mapBounds.width;
    }

    var flagDown = false;
    var moveDown = {
      'bottom': "-=110px"
    };
    var moveUp = {
      'bottom': "+=110px"
    };
    $('#btnToggleTimeSlider').on("click", function() {
      $('#btnToggleTimeSlider').toggleClass('glyphicon-collapse-down glyphicon-collapse-up');
      if (flagDown) {
        $('#timemenu').animate(moveUp);
        $('#timeSliderOverview').animate(moveUp);
        $('#timeSliderOverview div').animate(moveUp);
        $('.lcd-datetimelabel').animate(moveUp);
      }
      else {
        $('#timemenu').animate(moveDown);
        $('#timeSliderOverview').animate(moveDown);
        $('#timeSliderOverview div').animate(moveDown);
        $('.lcd-datetimelabel').animate(moveDown);
      }
      $('#timeSlider').toggleClass('hidden');
      flagDown = !flagDown;
    });

    // $('#btnYear').on("click", function() {
    // });
    //
    // $('#btnToggleTimeSlider').on("click", function() {
    // });
  }

  // Lang.mixin(RestStore.prototype, Evented.prototype);

  TimeSliderWithOverview.prototype = Object.create(DynamicTimeSlider.prototype);
  // TimeSliderWithOverview.prototype = Lang.mixin(DynamicTimeSlider.prototype, Evented.prototype);
  TimeSliderWithOverview.prototype.constructor = TimeSliderWithOverview;

  TimeSliderWithOverview.REFERENCE = DynamicTimeSlider.REFERENCE;

  TimeSliderWithOverview.prototype.addOverviewLayer = function(layer, position) {
    var result = this.overView.layerTree.addChild(layer, position);
    this.overView.layerTree.moveChild(this.overviewLayer, "top");
    return result;
  };

  TimeSliderWithOverview.prototype.getHistogram = function() {
    return this.histogram;
  };

  TimeSliderWithOverview.prototype.getOverview = function() {
    return this.overView;
  };

  TimeSliderWithOverview.prototype.getOverviewMode = function() {
    return this.overViewMode;
  };

  TimeSliderWithOverview.prototype.getCombineMode = function() {
    return this.combineMode;
  };

  TimeSliderWithOverview.prototype.setOverviewMode = function(newOverviewMode) {
    // this.overView.controller.setOverviewMode(newOverviewMode);
    this.overViewMode = newOverviewMode;
  };

  TimeSliderWithOverview.prototype.resizeAndRefit = function() {
    var previousBounds = this.mapBounds;
    var previousOverviewBounds = this.overView.mapBounds;

    $('#timeSlider').css('width', 'calc(100% - 20px)');
    $('#timeSliderOverview').css('width', 'calc(100% - 232px)');

    this.resize();
    this.overView.resize();

    this.mapNavigator.fit({bounds: previousBounds, animate: false, fitMargin: "0px", allowWarpXYAxis: true});
    this.overView.mapNavigator.fit(
        {bounds: previousOverviewBounds, animate: false, fitMargin: "0px", allowWarpXYAxis: true});
  };

  TimeSliderWithOverview.prototype.refit = function() {
    var bounds = ShapeFactory.createBounds(DynamicTimeSlider.REFERENCE,
        [this.startTime, this.endTime - this.startTime, this.mapBounds.y, this.mapBounds.height]);
    this.mapNavigator.fit({bounds: bounds, animate: true, fitMargin: "0px", allowWarpXYAxis: true});
  };

  TimeSliderWithOverview.prototype.resetOverview = function() {
    this.startTime = this.initialStartTime;
    this.endTime = this.initialEndTime;
    //
    // var bounds = ShapeFactory.createBounds(DynamicTimeSlider.REFERENCE,
    //     [this.initialStartTime, this.initialEndTime - this.initialStartTime, this.mapBounds.y, this.mapBounds.height]);
    // this.overView.mapNavigator.fit({bounds: bounds, animate: true, allowWarpXYAxis: true});
  };

  TimeSliderWithOverview.prototype.setCurrentOverviewRange = function(startTime, endTime) {
    this.startTime = startTime;
    this.endTime = endTime;
  };

  // TimeSliderWithOverview.prototype.setCurrentTime = function(timeInSeconds) {
  //   return this.timeSlider.setCurrentTime(timeInSeconds);
  // };
  //
  // TimeSliderWithOverview.prototype.getCurrentTime = function() {
  //   return this.timeSlider.getCurrentTime();
  // };
  //
  // TimeSliderWithOverview.prototype.setValidRange = function(startTime, endTime, yMin, yMax) {
  //   return this.timeSlider.setValidRange(startTime, endTime, yMin, yMax);
  // };
  //
  // TimeSliderWithOverview.prototype.fitToTimeRange = function(startTime, endTime) {
  //   return this.timeSlider.fitToTimeRange(startTime, endTime);
  // };

  return TimeSliderWithOverview;
});