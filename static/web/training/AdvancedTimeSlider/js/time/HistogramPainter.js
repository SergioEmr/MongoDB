define([
  'luciad/shape/ShapeFactory',
  "luciad/view/feature/FeaturePainter"
], function(ShapeFactory, FeaturePainter) {

  function HistogramPainter() {
    FeaturePainter.call(this);
  }

  HistogramPainter.prototype = Object.create(FeaturePainter.prototype);
  HistogramPainter.prototype.constructor = HistogramPainter;

  var style = {
    fill: {
      // color: "rgba(192, 217, 42, 0.5)"
      color: "rgba(255, 255, 255, 0.5)"
    },
    stroke: {
      // color: "rgba(0, 62, 81, 1.0)",
      color: "rgba(255, 255, 255, 0.5)",
      width: 2
    }
  };

  HistogramPainter.prototype.paintBody = function(geocanvas, feature, shape, layer, map, paintState) {
    geocanvas.drawShape(shape, style);
  };

  HistogramPainter.prototype.paintLabel = function(labelcanvas, feature, shape, layer, map, paintState) {
    // var label = "<span style='color: #FFFFFF'>" + "LABEL" + "</span>";
    // labelcanvas.drawLabel(label, shape.focusPoint, {});
  };

  return HistogramPainter;
});