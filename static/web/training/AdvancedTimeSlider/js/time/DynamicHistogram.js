define([
  "luciad/shape/ShapeFactory",
  "luciad/model/feature/Feature",
  "luciad/model/store/MemoryStore",
  "luciad/model/feature/FeatureModel"
], function(ShapeFactory, Feature, MemoryStore, FeatureModel) {

  /**
   * A FeatureModel that contains features with Bounds geometry that represent histogram buckets.
   *
   * The bucket's height indicates the content of the bucket, and is always equalized to the given max value.
   *
   * You should not populate (add/put/remove) this histogram yourself, instead use {@link #updateHistogram} to refresh the buckets.
   *
   * @param reference The reference the buckets will be in, for example X is time and Y is bucket content size
   * @param start The start of the bucket range
   * @param end The end of the bucket range
   * @param count The number of buckets
   * @param equalizeMax All buckets will be rescaled to this max value
   */

  function DynamicHistogram(reference, start, end, count, equalizeMax) {
    FeatureModel.call(this, new MemoryStore(), {reference: reference});

    /*
    RANGE MODES:
      0: default
      1: 1 year
      2: 1 month
      3: 1 day
      //4: 1 hour
     */
    this.combineMode = 0;

    this._reference = reference;
    this._start = start;
    this._end = end;
    this._initialStart = start;
    this._initialEnd = end;
    this._count = count;
    this._step = (end - start) / count;
    this._equalizeMax = equalizeMax;
    this._buckets = [];

    for (var i = 0; i < count; i++) {
      var x = this._start + (i * this._step);
      var w = this._step;
      var feature = new Feature(ShapeFactory.createBounds(reference, [x, w, 0, 0]), {}, i);
      this._buckets[i] = feature;
      this.put(feature);
    }
  }

  DynamicHistogram.prototype = Object.create(FeatureModel.prototype);
  DynamicHistogram.prototype.constructor = DynamicHistogram;


  DynamicHistogram.prototype.setFilter = function(filter) {
    this.filter = filter;
    // this.setCombineMode(this.combineMode);
    this.updateHistogram(this.updater);
  };

  DynamicHistogram.prototype.refit = function(start, end) {
    for (var id = 0; id < this._count; id++) {
      this.remove(id);
    }

    this._start = start;
    this._end = end;

    this._step = (this._end - this._start) / this._count;
    this._buckets = [];

    for (var i = 0; i < this._count; i++) {
      var x = this._start + (i * this._step);
      var w = this._step;
      var feature = new Feature(ShapeFactory.createBounds(this._reference, [x, w, 0, 0]), {}, i);
      this._buckets[i] = feature;
      this.put(feature);
    }

    this.updateHistogram(this.updater);
  };

  DynamicHistogram.prototype.setCombineMode = function(combineMode, transform) {
    this.filter = undefined;
    this.transform = transform;
    for (var id = 0; id < this._count; id++) {
      this.remove(id);
    }

    this.combineMode = combineMode;

    if (this.combineMode == 0) {
      this._start = this._initialStart;
      this._end = this._initialEnd;
    }
    else {
      this._start = 0;

      if (this.combineMode == 1) {
        // this._end = 366 * 24 * 60 * 60 ;
        this._end = 400 * 24 * 60 * 60 ;
      }
      if (this.combineMode == 2) {
        this._end = 32 * 24 * 60 * 60;
      }
      if (this.combineMode == 3) {
        this._end = 24 * 60 * 60;
      }
    }

    this._step = (this._end - this._start) / this._count;
    this._buckets = [];

    for (var i = 0; i < this._count; i++) {
      var x = this._start + (i * this._step);
      var w = this._step;
      var feature = new Feature(ShapeFactory.createBounds(this._reference, [x, w, 0, 0]), {}, i);
      this._buckets[i] = feature;
      this.put(feature);
    }

    this.updateHistogram(this.updater);
  };

  /**
   * Updates the histogram.  The caller must pass a function to do the updates.
   * That function gets an "accumulate" function that the caller can invoke with x and y values.
   */
  DynamicHistogram.prototype.updateHistogram = function(updater) {
    this.updater = updater;

    for (var i = 0; i < this._buckets.length; i++) {
      var bucket = this._buckets[i];
      bucket.properties.previousHeight = bucket.shape.height;
      bucket.shape.height = 0;
    }

    var self = this;
    updater(function(x, y) {
      var time = x;

      if (self.transform) {
        // console.log("transforming");
        // console.log(time);
        // console.log(self.transform);
        // console.log(self.transform(x));
        time = self.transform(x);
      }

      if (self.filter) {
        // console.log("filtering");
        // console.log(time);
        // console.log(self.filter);
        // console.log(self.filter(time));
        if (self.filter(x)) {
          var i = Math.floor((time - self._start) / self._step);
          if (0 <= i && i < self._buckets.length) {
            var bucket = self._buckets[i];
            bucket.shape.height += y;
          }
        }
      }
      else {
        var i = Math.floor((time - self._start) / self._step);
        if (0 <= i && i < self._buckets.length) {
          var bucket = self._buckets[i];
          bucket.shape.height += y;
        }
      }

      // if (self.combineMode >= 1) {
      //   var thisDate = new Date(time * 1000);
      //   var startDate;
      //   if (self.combineMode == 1) {
      //     startDate = new Date(thisDate.getFullYear(), 0, 0);
      //   }
      //   if (self.combineMode == 2) {
      //     startDate = new Date(thisDate.getFullYear(), thisDate.getMonth(), 0);
      //   }
      //   if (self.combineMode == 3) {
      //     startDate = new Date(thisDate.getFullYear(), thisDate.getMonth(), thisDate.getDate());
      //   }
      //   time = ( thisDate.getTime() - startDate.getTime() ) / 1000 | 0;
      //
      //   // console.log(time);
      //   // console.log([self._start, self._end]);
      // }
    });

    var max = 0;
    for (var i = 0; i < this._buckets.length; i++) {
      var bucket = this._buckets[i];
      max = Math.max(bucket.shape.height, max);
    }

    for (var i = 0; i < this._buckets.length; i++) {
      var bucket = this._buckets[i];
      bucket.shape.height = this._equalizeMax * (bucket.shape.height / max);
      if (bucket.properties.previousHeight !== bucket.shape.height) {
        this.put(bucket);
      }
    }
  };

  return DynamicHistogram;
});
