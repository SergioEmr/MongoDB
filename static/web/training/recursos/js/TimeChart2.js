define([
], function(){

    var playing = false;
    var speedup = 3000;
    var dataSetEndTime = new Date().getTime();
    var dataSetStartTime = dataSetEndTime - 86860;
    //console.log(dataSetStartTime + "   " + dataSetEndTime);
    var timeChart = initTimeChart();
    timeChart.addInteractionListener(timeUpdated);
    timeChart.addZoomListener(function(currZoomLevel, prevZoomLevel) {
        speedup *= prevZoomLevel / currZoomLevel;
    });
    setTime(12 * 60);
    
    
    $("#play").click(function() {
      lasttime = Date.now();
      play(!playing);
    });
    function initTimeChart() {
        var tC = new TimeChart(dom.byId("timeChart"), new Date(dataSetStartTime * 1000), new Date(dataSetEndTime * 1000));
        var gmtPlus = (new Date().getTimezoneOffset() / -60);
        $('#timezone').text("GMT" + (gmtPlus > 0 ? "+" : "") + gmtPlus);
        return tC;
    }
    function setTime(minutes) {
        timeChart.setCurrentTime(new Date(timeChart.startDate.getTime() + (1000 * 60 * minutes)));
        timeUpdated();
    }
    
    function timeUpdated() {
    var currentTime = timeChart.getCurrentTime();
    var dataSetTimeValue = timeChart.getCurrentTime().getTime() / 1000 - dataSetStartTime; // as noted earlier the time values inside the file are relative to 'dataSetStartTime'
    var trajectoriesPainter = trajectorylayer.painter;
    map.effects.light = LightEffect.createSunLight({time : currentTime});
    trajectoriesPainter.timeWindow = [dataSetTimeValue - 1200, dataSetTimeValue];
    $('#timelabel').text(timeChart.getFormattedCurrentTime());
  }
    
    
  function playStep() {
    if (playing) {
      var currTime = Date.now();
      var deltaTime = (currTime - lasttime);
      lasttime = currTime;
      var timeChartTime = timeChart.getCurrentTime().getTime();
      var newTime = timeChartTime + (deltaTime) * speedup;
      if (newTime >= timeChart.endDate.getTime()) {
        newTime = timeChart.startDate.getTime();
      }
      timeChart.setCurrentTime(new Date(newTime));
      window.requestAnimationFrame(playStep);
    }
  }

  function play(shouldPlay) {
    playing = shouldPlay;
    if (playing) {
      playStep();
    }
    var playBtn = $('#play');
    playBtn.toggleClass("active", playing);
    playBtn.find("> span").toggleClass("glyphicon-pause", playing);
    playBtn.find("> span").toggleClass("glyphicon-play", !playing);
  }

  });
