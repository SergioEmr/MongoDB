define([
  "luciad/util/Promise"
], function(Promise) {

  /**
   * A {@link luciad/model/Store} that reads content from a browser-local HTML5 File object.
   */

  function LocalFileStore(file, codec) {

    this._codec = codec;

    this._reading = new Promise(function(resolve, reject) {
      var reader = new FileReader();
      reader.readAsText(file);
      reader.onload = function() {
        resolve(reader.result);
      };
      reader.onerror = function() {
        reject("Cannot load file " + file.name);
      };
    });
  }

  LocalFileStore.prototype.query = function() {
    var self = this;
    return this._reading.then(function(result) {
      return self._codec.decode({content: result});
    });
  };

  return LocalFileStore;
});
