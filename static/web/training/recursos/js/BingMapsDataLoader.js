define([
  "luciad/view/LayerType",
  "luciad/view/tileset/RasterTileSetLayer",
  "luciad/model/tileset/BingMapsTileSetModel",
  "luciad/util/Promise",
  "template/sample",
  
  //"samples/common/balloon/SimpleBalloonContentProvider",
  "dojo/request"
], function(LayerType, RasterTileSetLayer, BingMapsTileSetModel, Promise, sampleTemplate,
         request) {

    var queryFinishedHandle = null;
    
    function addLayer(layerOrPromise, map) {
      Promise.when(layerOrPromise,
        function(layer) {
          // Remove any existing scheduled fit
          if (queryFinishedHandle) {
            queryFinishedHandle.remove();
            queryFinishedHandle = null;
          }

          if (layer.workingSet) {
            // On data loading error, show warning and remove the layer
            layer.workingSet.on("QueryError", function(error) {
              sampleTemplate.error(null, "Error loading " + layer.label + ":<br>" + error.message, error);
              map.layerTree.removeChild(layer);
            });

            // On data loading finished, fit on the layer
            queryFinishedHandle = layer.workingSet.on("QueryFinished", function() {
              queryFinishedHandle.remove();
              if (layer.bounds) {
                //map.mapNavigator.fit({bounds: layer.bounds, animate: true});
              }
            });

            // Add simple balloon
            /*if (!layer.balloonContentProvider) {
              layer.balloonContentProvider = SimpleBalloonContentProvider;
            }*/
          } else {
            // Fit on raster layer
            //map.mapNavigator.fit({bounds: layer.model.bounds, animate: true});
          }

          // Actually add the layer to the map
          map.layerTree.addChild(layer, layer.type !== LayerType.BASE ? "top" : "bottom");
          
          layer.visible = true;
          //return layer;
        },
        function(error) {
            sampleTemplate.error(null, "Cannot add layer: " + error.message, error);
        });
    }
    
    function initLayer(type){
            //#snippet createModel
            var getBingMetadata = request("/bingproxy/" + type, {handleAs: "json"});

            return getBingMetadata.then(
                function(data) {
                //Inspect the response
                    var resourceSet;
                    var resource = data;

                    if (Object.prototype.toString.call(data.resourceSets) === '[object Array]' && data.resourceSets.length > 0) 
                    {
                        resourceSet = data.resourceSets[0];
                        if (Object.prototype.toString.call(resourceSet.resources) === '[object Array]' && resourceSet.resources.length > 0) 
                        {
                            resource = resourceSet.resources[0];
                        }
                    }
                    resource.brandLogoUri = resource.brandLogoUri || data.brandLogoUri;
                    resource.zoomMax=19;
                    var model = new BingMapsTileSetModel(resource);

                    var layer = new RasterTileSetLayer(model, {label: type + " (Bing)", layerType: LayerType.BASE, id: type+"Bing"});

                    return layer;
                });
      //#endsnippet createModel
    }
    
    function createBingLayer(type, map)
    {
        return addLayer(initLayer(type), map);
    }
    
  return {

    /**
     * Creates a layer for Bing Maps data.
     *
     * Summary:
     * - Contact Bing proxy to initialize session
     * - Create a {@link luciad/model/tileset/BingMapsTileSetModel}
     * - Create a {@link luciad/view/tileset/RasterTileSetLayer}
     *
     * @param type {String} One of "road", "aerial" or "AerialWithLabels"
     * @return {Promise} A promise for a layer.
     */

    createBingLayer: createBingLayer
  };

});