define([], function() {

  /**
   * Creates a 3D ellipsoid with given radial dimensions in X, Y, and Z axis, and with the given number of
   * vertical and horizontal subdivisions of the surface.
   *
   * A sphere can be obtained by setting the three radial parameters to the same value.
   *
   * @param {Number} radiusX the radial dimension along the X axis
   * @param {Number} radiusY the radial dimension along the Y axis
   * @param {Number} radiusZ the radial dimension along the Z axis
   * @param {Number} verticalSlicesCount the number of vertical subdivisions of the surface (similar to lines of longitude)
   * @param {Number} horizontalSlicesCount the number of horizontal subdivisions of the surface (similar to lines of latitude)
   *
   **/

  function Ellipsoid3DMesh(radiusX, radiusY, radiusZ, verticalSlicesCount, horizontalSlicesCount) {
    this._radiusX = radiusX;
    this._radiusY = radiusY;
    this._radiusZ = radiusZ;
    this._verticalSlicesCount = verticalSlicesCount;
    this._horizontalSlicesCount = horizontalSlicesCount;

    // Used for domes
    this._horizontalSlicesEndIndex = this._horizontalSlicesCount;
  }

  Ellipsoid3DMesh.prototype.createVertices = function() {
    var vertices = [];
    var dPhi = 2 * Math.PI / (this._verticalSlicesCount);
    var dTheta = Math.PI / (this._horizontalSlicesCount);
    var x, y, z;
    for (var i = 0; i <= this._horizontalSlicesEndIndex; i++) {
      var tht = -Math.PI / 2 + i * dTheta;
      for (var j = 0; j <= this._verticalSlicesCount; j++) {
        var phi = j * dPhi;
        x = this._radiusX * Math.cos(tht) * Math.cos(phi);
        y = this._radiusY * Math.cos(tht) * Math.sin(phi);
        z = this._radiusZ * Math.sin(tht);
        // base vertex
        vertices.push(x);
        vertices.push(y);
        vertices.push(z);
      }
    }
    return vertices;
  };

  Ellipsoid3DMesh.prototype.createIndices = function() {
    var indices = [];
    for (var i = 0; i < this._horizontalSlicesEndIndex; i++) {
      for (var j = 0; j < this._verticalSlicesCount; j++) {
        var index1 = i * (this._verticalSlicesCount + 1) + j;
        var index2 = i * (this._verticalSlicesCount + 1) + (j + 1);
        var index3 = (i + 1) * (this._verticalSlicesCount + 1) + (j + 1);
        var index4 = (i + 1) * (this._verticalSlicesCount + 1) + j;

        // Triangle 1
        indices.push(index2);
        indices.push(index4);
        indices.push(index3);

        // Triangle 2
        indices.push(index2);
        indices.push(index4);
        indices.push(index1);
      }
    }

    return indices;
  };

  Ellipsoid3DMesh.prototype.createNormals = function() {
    var normals = [];
    var dPhi = 2 * Math.PI / (this._verticalSlicesCount);
    var dTheta = Math.PI / (this._horizontalSlicesCount);
    var nx, ny, nz;
    for (var i = 0; i <= this._horizontalSlicesEndIndex; i++) {
      var tht = -Math.PI / 2 + i * dTheta;
      for (var j = 0; j <= this._verticalSlicesCount; j++) {
        var phi = j * dPhi;
        nx = Math.cos(tht) * Math.cos(phi);
        ny = Math.cos(tht) * Math.sin(phi);
        nz = Math.sin(tht);
        // base vertex
        normals.push(nx);
        normals.push(ny);
        normals.push(nz);
      }
    }
    return normals;
  };

  Ellipsoid3DMesh.prototype.createTextureCoordinates = function() {
    var texCoords = [];
    var dPhi = 2 * Math.PI / (this._verticalSlicesCount);
    var dTheta = Math.PI / (this._horizontalSlicesCount);
    for (var i = 0; i <= this._horizontalSlicesEndIndex; i++) {
      var tht = -Math.PI / 2 + i * dTheta;
      for (var j = 0; j <= this._verticalSlicesCount; j++) {
        var phi = j * dPhi;
        var tx = phi / (2 * Math.PI);
        var ty = 0.5 + tht / Math.PI;
        texCoords.push(tx); // u
        texCoords.push(ty); // v
      }
    }
    return texCoords;
  };

  return Ellipsoid3DMesh;
});
