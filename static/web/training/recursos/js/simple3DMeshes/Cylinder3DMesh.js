define([], function() {

  /**
   * Creates a 3D cylinder mesh
   *
   * @param radius the radius of the cylinder
   * @param height the height of the cylinder
   * @param sliceCount the number of slices (subdivisions) of the side surface of the stick and the tip
   */

  function Cylinder3DMesh(radius, height, sliceCount) {
    this._radius = radius;
    this._height = height;
    this._sliceCount = sliceCount;

    this._indices = [];
    this.zOffset = 0;
  }

  Cylinder3DMesh.prototype.createVertices = function() {
    var vertices = [];
    var dphi = 2 * Math.PI / (this._sliceCount);
    var baseZ = -0.5 * this._height + this.zOffset;
    var topZ = 0.5 * this._height + this.zOffset;

    var offset = 0;

    // Side surface
    for (var i = 0; i <= this._sliceCount; i++) {
      var phi = i * dphi;
      var nx = Math.cos(phi);
      var ny = Math.sin(phi);
      var x0 = this._radius * nx;
      var y0 = this._radius * ny;
      var x1 = x0;
      var y1 = y0;
      // base vertex
      vertices.push(x0);
      vertices.push(y0);
      vertices.push(baseZ);
      this._indices.push(offset + 2 * i);
      // top vertex
      vertices.push(x1);
      vertices.push(y1);
      vertices.push(topZ);
      this._indices.push(offset + 2 * i + 1);
    }
    offset = this._indices.length;

    // Base surface
    for (var i = 0; i <= this._sliceCount; i++) {
      var phi = i * dphi;
      var nx = Math.cos(phi);
      var ny = Math.sin(phi);
      var x0 = 0;
      var y0 = 0;
      var x1 = this._radius * nx;
      var y1 = this._radius * ny;
      // base vertex
      vertices.push(x0);
      vertices.push(y0);
      vertices.push(baseZ);
      this._indices.push(offset + 2 * i);
      // top vertex
      vertices.push(x1);
      vertices.push(y1);
      vertices.push(baseZ);
      this._indices.push(offset + 2 * i + 1);
    }

    offset = this._indices.length;

    // Top base surface
    for (var i = 0; i <= this._sliceCount; i++) {
      var phi = i * dphi;
      var nx = Math.cos(phi);
      var ny = Math.sin(phi);
      var x0 = 0;
      var y0 = 0;
      var x1 = this._radius * nx;
      var y1 = this._radius * ny;
      // base vertex
      vertices.push(x0);
      vertices.push(y0);
      vertices.push(topZ);
      this._indices.push(offset + 2 * i);
      // top vertex
      vertices.push(x1);
      vertices.push(y1);
      vertices.push(topZ);
      this._indices.push(offset + 2 * i + 1);
    }

    return vertices;
  };

  Cylinder3DMesh.prototype.createIndices = function() {
    if (this._indices.length === 0) {
      this.createVertices();
    }

    var triangles = [];
    for (var i = 0; i < 3; i++) {
      var numberOfIndicesPerSide = this._indices.length / 3;
      for (var j = 1; j < numberOfIndicesPerSide - 1; j++) {
        triangles.push(this._indices[j - 1 + i * numberOfIndicesPerSide]);
        triangles.push(this._indices[j + i * numberOfIndicesPerSide]);
        triangles.push(this._indices[j + 1 + i * numberOfIndicesPerSide]);
      }
    }

    return triangles;
  };

  Cylinder3DMesh.prototype.createNormals = function() {
    var normals = [];
    var dPhi = 2 * Math.PI / (this._sliceCount);

    // Side
    for (var i = 0; i <= this._sliceCount; i++) {
      var phi = i * dPhi;
      var nx = Math.cos(phi);
      var ny = Math.sin(phi);
      // base vertex
      normals.push(nx);
      normals.push(ny);
      normals.push(0);
      // top vertex
      normals.push(nx);
      normals.push(ny);
      normals.push(0);
    }

    // Base
    for (var i = 0; i <= this._sliceCount; i++) {
      // inner vertex
      normals.push(0);
      normals.push(0);
      normals.push(-1);
      // outer vertex
      normals.push(0);
      normals.push(0);
      normals.push(-1);
    }

    // Top base
    for (var i = 0; i <= this._sliceCount; i++) {
      // inner vertex
      normals.push(0);
      normals.push(0);
      normals.push(-1);
      // outer vertex
      normals.push(0);
      normals.push(0);
      normals.push(-1);
    }

    return normals;
  };

  Cylinder3DMesh.prototype.createTextureCoordinates = function() {
    var texCoords = [];
    var dphi = 2 * Math.PI / (this._sliceCount);

    // Side
    for (var i = 0; i <= this._sliceCount; i++) {
      var phi = i * dphi;
      var tx = phi / (2 * Math.PI);
      // base vertex
      texCoords.push(tx);
      texCoords.push(0);
      // top vertex
      texCoords.push(tx);
      texCoords.push(1);
    }

    // Base
    for (var i = 0; i <= this._sliceCount; i++) {
      var phi = i * dphi;
      var tx = phi / (2 * Math.PI);
      // inner vertex
      texCoords.push(tx);
      texCoords.push(0);
      // outer vertex
      texCoords.push(tx);
      texCoords.push(1);
    }

    // Top Base
    for (var i = 0; i <= this._sliceCount; i++) {
      var phi = i * dphi;
      var tx = phi / (2 * Math.PI);
      // inner vertex
      texCoords.push(tx);
      texCoords.push(0);
      // outer vertex
      texCoords.push(tx);
      texCoords.push(1);
    }

    return texCoords;
  };

  return Cylinder3DMesh;
});
