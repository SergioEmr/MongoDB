/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define([
    'luciad/util/Promise',
    "samples/timeslider/TimeSlider",
    'luciad/shape/ShapeFactory',
    "samples/timeslider/TimeSlider",
    "time/TimeSliderWithOverview",
    'luciad/util/ColorMap',
    'luciad/transformation/TransformationFactory',
    
    "recursos/js/Graficas/Graficas",
    "./TimeLineLayer",
    "luciad/model/store/MemoryStore",
    "luciad/model/feature/FeatureModel",
    "luciad/view/feature/FeatureLayer",
    "samples/timeslider/TimeSlider",
    "time/TimeSliderWithOverview",
    
    'luciad/view/feature/ParameterizedLinePainter',
    'luciad/util/ColorMap',
    'luciad/view/feature/ShapeProvider',
    "./painters/EventTimePainter",
    "./Contador",
    "./Tablas",
    "recursos/js/Util",
    "recursos/js/MapFactory"
], function (Promise, TimeSlider, ShapeFactory,TimeSlider,TimeSliderWithOverview,ColorMap,TransformationFactory,
            Graficas, TimeLineLayer, MemoryStore, FeatureModel, FeatureLayer, TimeSlider, TimeSliderWithOverview,
            ParameterizedLinePainter, ColorMap, ShapeProvider, EventTimePainter, Contador, Tablas, Util, MapFactory) {
    
    var timeSlider;
     /*
     * updateHistogram, esta funcion se encarga de actualizar toda la informacion necesario
     * al mover la linea de tiempo
     * @returns {undefined}
     */
    var timeFilter;
    var usuario = document.getElementById("tipoUsuario").innerHTML;
    switch(usuario.toLowerCase()) {
        case "088": usuario="u"; break;
        case "dev": usuario = "u"; break;
        default: usuario="ig"; break;
    }
    function updateHistogram(map, features, layer, reference, startTime, endTime, tablaDatos, graficas, estados, filtros) {
        var store = new MemoryStore({data: features});
        var originalModel = new FeatureModel(store, { reference: reference });
        var eventPromise = originalModel.query();
        Promise.when(eventPromise, function(cursor) {
            var quakes = [];
            var index = 0;
            while (cursor.hasNext()) {
                var quake = cursor.next();
                quakes[index] = quake;
                index++;
            }
            console.log("Loading " + index + " events");

            startTime = startTime - 1000;
            endTime += 1000;
            var shapeProvider = new ShapeProvider();
            shapeProvider.reference = TimeSlider.REFERENCE;
            shapeProvider.provideShape = function(feature) {
                var time = obtenerFecha(feature.properties);
                var depth = 500-feature.shape.z;
                return ShapeFactory.createPoint(TimeSlider.REFERENCE, [time, depth]);
            };
            var timeEventLayer = new FeatureLayer(layer.model, {
                selectable: true,
                shapeProvider: shapeProvider,
                painter: new EventTimePainter()
            });
            
            var check = false;
            var updateTimeFilters = function(c) {
                var municipios=[];
                var mapBounds = timeSlider.mapBounds;
                try {
                    var nuevaTabla = Graficas.actualizarTabla(tablaDatos,timeEventLayer, mapBounds);
                    for(var k =0; k<graficas.length;k++) {
                        Graficas.actualizarGraficaIdTexto(graficas[k].id, nuevaTabla, graficas[k].tipo, graficas[k].label);
                    }
                    /*
                    var mun = municipiosFilterNode.selectedOptions[0].innerHTML;
                    municipios = listarMunicipios(nuevaTabla);
                    if(mun === "Todos")
                        Util.setOptions("filtroMunicipios", municipios);
                    */
                    if(nuevaTabla.length !== tablaDatos.length) {
                        Tablas.crearTablaDIV("tablaCompleta", nuevaTabla, 500);
                        console.log("Tabla Actualizada");
                    }
                }catch(e) {
                    console.log("Error al actualizar grafica");
                }
                var filterStart = (timeSlider.mapBounds.x | 0 );
                var filterWidth = (timeSlider.mapBounds.width | 0 );
                var filterEnd = filterStart + filterWidth;
                var violencia = violenciaFilterNode.selectedOptions[0].value;
                var municipio = municipiosFilterNode.selectedOptions[0].value;
                var colonia = coloniasNode.selectedOptions[0].innerHTML;
                var dia = diaNode.selectedOptions[0].innerHTML;
                mun = municipiosFilterNode.selectedOptions[0].innerHTML;
                var puntos = [], np=0;
                if(check === true || c === true) {
                    check = false;
                    layer.filter = function(feature) {
                        var f = timeEventLayer.shapeProvider.provideShape(feature);
                        var m = feature.properties.MUNICIPIO;
                        var d = feature.properties.DIA_HECHO;
                        var v = feature.properties.VIOLENCIA;
                        var c = feature.properties.COLONIA;
                        if(mapBounds.contains2DPoint(f)) {
                            if((m === municipio || municipio === "Todos") && (v === violencia || violencia === "Todos")&& 
                                    (c === colonia || colonia === "Todos") && (d === dia || dia==="Todos") ) {
                                return true;
                            } 
                            else
                                return false;
                        } else
                            return false;
                    };
                } else {
                    check = true;
                }
                /*
                if(puntos.length===0) {
                    puntos = layer.model.query().array;
                }
                Contador.reestartLayers(municipios);
                Contador.contadorMunicipios(layer, municipios);
                */
            }; /// fin UpdateFilters
    
            var combineModeChangedFunction = function(transform) {
                timeFilter = undefined;
                var combineMode = timeSlider.getCombineMode();

                var painter = new ParameterizedLinePainter({
                    lineWidth: 10,
                    rangePropertyProvider: function(feature, shape, pointIndex) {
                        var time = Date.parse(feature.properties.alerta) / 1000 | 0;
                        return transform(time);
                    },
                    rangeWindow: [0, 999999999999]
                });
                painter.density = {
                    colorMap: ColorMap.createGradientColorMap([
                    {level: 0, color: "rgba(  0,   0,   0, 0.0)"},
                    {level: 1, color: "rgba(  0, 0,   255, 0.5)"},
                    {level: 5, color: "rgba(  0, 255,   255, 1.0)"},
                    {level: 10, color: "rgba(  255, 255,   0, 1.0)"},
                    {level: 20, color: "rgba(255, 0, 0, 1.0)"}
                    ])
                };

                var previousPainter = layer.painter;
                var quakesModel = layer.model;
                var layer2d = new FeatureLayer(quakesModel, {
                    label: "Eventos 2",
                    selectable: true,
                    id: "eventos",
                    painter: painter
                });
                map.layerTree.removeChild(map.layerTree.findLayerById("eventos"));

                eventos = layer2d;

                timeSlider.layerTree.removeChild(timeEventLayer);

                var shapeProvider = new ShapeProvider();
                shapeProvider.reference = TimeSlider.REFERENCE;
                shapeProvider.provideShape = function(feature) {
                    var time = obtenerFecha(feature.properties);
                     time = transform(time);

                    var depth = 1000-feature.shape.getPoint(0).z;
                    return ShapeFactory.createPoint(TimeSlider.REFERENCE, [time, depth]);
                };
                timeEventLayer = new FeatureLayer(quakesModel, {
                    selectable: true,
                    shapeProvider: shapeProvider,
                    painter: new EventTimePainter()
                });

                //timeSlider.layerTree.addChild(timeEventLayer);
            
                updateTimeFilters();
                var lonlatBounds = transformation.transformBounds(map.mapBounds);
                timeEventLayer.filter = function(feature) {
                    return lonlatBounds.contains2DPoint(feature.shape.getPoint(0));
                };
            
            // timeSlider.getHistogram().updateHistogram(histogramUpdater);
            };

            var combineModeFilterChangedFunction = function(filter) {
                timeFilter = filter;
                if (timeFilter) {
                        map.layerTree.findLayerById("eventos").filter = function(feature) {
                        var time = obtenerFecha(feature.properties);
                        //var time = Date.parse(feature.properties.EventTime) / 1000 | 0;
                        return timeFilter(time);
                    };
                    updateTimeFilters();
                    var lonlatBounds = transformation.transformBounds(map.mapBounds);
                    timeEventLayer.filter = function(feature) {
                        var time = obtenerFecha(feature.properties);
                        //var time = Date.parse(feature.properties.EventTime) / 1000 | 0;
                        if (timeFilter(time)) {
                            return lonlatBounds.contains2DPoint(feature.shape.getPoint(0));
                        }
                        else {
                            return false;
                        }
                    };
                }
            };

            timeSlider = new TimeSliderWithOverview("timeSlider", "timeSliderOverview", startTime, endTime, combineModeChangedFunction, combineModeFilterChangedFunction, 3000);

            var meses = Math.round((endTime - startTime) / 2628000);
            var dias = Math.round((endTime - startTime) / 86400);
            var columnsDayLayer = TimeLineLayer.createLayer(features, "meses", meses, startTime, endTime);
            timeSlider.layerTree.addChild(columnsDayLayer);
            var queryFinishedHandleSlider = columnsDayLayer.workingSet.on("QueryFinished", function() {
                if(columnsDayLayer.bounds) {
                            try{
                                timeSlider.mapNavigator.fit({
                                    bounds: columnsDayLayer.bounds,
                                    animation: {duration: 1000},
                                    allowWarpXYAxis: true,
                                    fitMargin: "5%"
                                });
                             }catch(e) {
                                    console.log(e);
                             }
                        }
                queryFinishedHandleSlider.remove();
            });
                
            var columnsHourLayer = TimeLineLayer.createLayer(features, "dias", dias, startTime, endTime);
            timeSlider.layerTree.addChild(columnsHourLayer);

            //timeSlider.refit();

            var transformation = TransformationFactory.createTransformation(map.reference, reference);
            var histogramUpdater = function(accumulate) {
                    var lonlatBounds = transformation.transformBounds(map.mapBounds);
                    for (var i = 0; i < quakes.length; i++) {
                        if(lonlatBounds.contains2DPoint(quakes[i].shape)) {
                            var time = obtenerFecha(quakes[i].properties);
                            //var time = Date.parse(quakes[i].properties.EventTime) / 1000 | 0;
                            // var magnitude = quakes[i].properties.magnitude;
                            var magnitude = quakes[i].properties.mag;
                            accumulate(time, magnitude);
                        }
                    }
                };
            timeSlider.getHistogram().updateHistogram(histogramUpdater);

            var hacerfit = false;
            var violenciaFilterNode = document.getElementById( "violenciaFiltro" );
            violenciaFilterNode.addEventListener( "change", function () {
                //fitColumns = true;
                updateTimeFilters(true);
            });
            var municipiosFilterNode = document.getElementById("municipiosFiltro");
            municipiosFilterNode.addEventListener( "change", function () {
                fitColumns = true;
                updateTimeFilters(true);
            });
            var nombresMunicipios = [], estadoSeleccionado;
            /*var municipiosFilterNode = document.getElementById( "municipiosFiltro" );
            municipiosFilterNode.addEventListener( "change", function () {
                //fitColumns = true;
                var value = municipiosFilterNode.selectedOptions[0].value;
                if(value !== "Todos" && estados) {
                    estados.forEach(function (capaEstado) {
                        var label = capaEstado.label;
                        var check = false;
                        switch(value.toLowerCase()) {
                            default:
                                switch(value) {
                                    case "Guanajuato": check = true; break;
                                    case "Hidalgo": check = true; break;
                                    case "Queretaro": check = true; break;
                                    default: check = false; break;
                                }
                                break;
                            case "nuevo león":
                            case "nuevo leon":
                                if(label === "NuevoLeon") check = true;
                                else check = false;
                                break;
                            case "michoacán de ocampo":
                            case "michoacan de ocampo":
                                if(label === "Michoacan")  check = true;
                                else  check = false;
                                break;
                            case "méxico":
                                if(label === "EdoMex")  check = true;
                                else  check = false;
                                break;
                            case "distrito federal":
                                if(label === "CDMX")  check = true;
                                else  check = false;
                                break;
                            case "":
                                if(label === "CDMX")  check = true;
                                else  check = false;
                                break;
                        }
                        if(check === true) {
                            capaEstado.visible = true;
                            hacerfit = true;
                            hacerFit(capaEstado.bounds);
                            estadoSeleccionado = capaEstado;
                            nombresMunicipios = getNames(capaEstado);
                        } else {
                            capaEstado.visible = false;
                        }
                    });
                    Graficas.setOptions("filtroMunicipios", nombresMunicipios);
                } else {
                    Graficas.setOptions("filtroMunicipios", []);
                }
                updateTimeFilters();
            });
            
            function getNames (capaEsatdo) {
                var municipios = capaEsatdo.model.query().array;
                var n = municipios.length, nombres = [];
                for(var i=0; i<n; i++) {
                    var municipio = municipios[i];
                    nombres[i] = municipio.properties.name? municipio.properties.name: municipio.properties.NOMBRE? municipio.properties.NOMBRE: "Sin Nombre "+municipio.id;
                }
                return nombres;
            }
            var municipiosFilterNode2 = document.getElementById( "filtroMunicipios" );
            municipiosFilterNode2.addEventListener( "change", function (data) {
                var value = municipiosFilterNode2.selectedOptions[0].value;
                hacerfit = true;
                if(estadoSeleccionado) {
                    estadoSeleccionado.filter = function (municipio) {
                        var name = municipio.properties.name? municipio.properties.name: municipio.properties.NOMBRE? municipio.properties.NOMBRE: "Sin Nombre "+municipio.id;
                        if(value === "Todos" || value === name) {
                            hacerFit(municipio.geometry.bounds);
                            return true;
                        }
                        else 
                            return false;
                    };
                }
                updateTimeFilters();
            });
            */
            var coloniasNode = document.getElementById( "coloniasFiltro" );
            coloniasNode.addEventListener( "change", function (data) {
                updateTimeFilters();
            });
            var diaNode = document.getElementById( "diaFiltro" );
            diaNode.addEventListener( "change", function (data) {
                updateTimeFilters();
            });
            
            function hacerFit(bounds) {
                if(bounds && hacerfit === true) {
                    hacerfit = false;
                    map.mapNavigator.fit({bounds: bounds, animate: true});
                }
            }
            
            map.on("idle", function() {
                timeSlider.getHistogram().updateHistogram(histogramUpdater);
            });
            map.on("MapChange", function() {
                var lonlatBounds = transformation.transformBounds(map.mapBounds);

                if (timeFilter) {
                    timeEventLayer.filter = function(feature) {
                        var time = obtenerFecha(feature.properties);
                        //var time = Date.parse(feature.properties.EventTime) / 1000 | 0;
                        if (timeFilter(time)) {
                            return lonlatBounds.contains2DPoint(feature.shape);
                        }
                        else {
                            return false;
                        }
                    };
                }
                else {
                    timeEventLayer.filter = function(feature) {
                        return lonlatBounds.contains2DPoint(feature.shape);
                    };
                }
            });

            timeSlider.on("MapChange", function() {
                //if(fitColumns === false)
                if(esVisible())
                    updateTimeFilters();
            });
        
        //Fin Promise
        });
        
        function obtenerFecha(properties) {
            if(properties.EventTime) {
                return Date.parse(properties.EventTime) / 1000 | 0;
            }
            try{
            var fecha = properties.FECHA_HECH;
            var nfecha = "", n = 0;
            var c, a = "", m = "", d = "";

            for(var j=0; j<fecha.length;j++) {
                c = fecha.charAt(j);
                if(c === '/' || (j=== (fecha.length-1))) {
                    if(n===0) 
                        d = nfecha;
                    else {
                        if(n===1)
                            m = nfecha;
                        else {
                            if(n===2) {
                                a = nfecha+c;
                                n=0;
                            }
                        }
                    }
                    nfecha = "";
                    n++;
                } else {
                    nfecha+=c;
                }

            }
            return (Date.parse(a+'/'+m+'/'+d) /1000) | 0;
            } catch(e) {
                console.log(e);
                return 0;
            }
        }
        
        function listarMunicipios(tablaDatos) {
            //console.log(tablaDatos[0]);
            var id = Util.getIdArray("municipio", tablaDatos[0]);
            var municipios = [], j=0;
            var nombre;
            for(var i=1; i<tablaDatos.length; i++) {
                nombre = tablaDatos[i][id];
                if(municipios.length===0) {
                    municipios[j]=nombre;
                    j++;
                } else {
                    var nuevoDato=true;
                    for(var k in municipios) {
                        if(nombre === municipios[k]){
                            nuevoDato=false;
                            break;
                        }
                    }
                    if(nuevoDato) {
                        municipios[j]=nombre;
                        j++;
                    }
                }
            }
            return municipios;
        }
    //fin UpdateHistogram
    }
    ////================================================================================////////
    
    function esVisible() {
        var value = document.getElementById("botonGraficas").value;
        if(value === "true" || value === true)
            return true;
        else
            return false;
    }
    function destruirComponentes() {
        MapFactory.destroyMapAndComponents(timeSlider.overView);
        MapFactory.destroyMapAndComponents(timeSlider);
    }
    return {
        updateHistogram: updateHistogram,
        timeSlider: timeSlider,
        destruirComponentes: destruirComponentes
    };
});

