/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define([], function () {
    
    function getColor(id) {
        switch(id) {
            case 0 : return "#2ecc71";
            case 1: return "#f7dc6f";
            case 2: return "#f39c12";
            case 3: return "#d35400";
            case 4: return "#85929e";
            default: return "#19232c";
        }
    }
    function getDia(id) {
        switch(id) {
            case 0: return "Domingo";
            case 1: return "Lunes";
            case 2: return "Martes";
            case 3: return "Miercoles";
            case 4: return "Jueves";
            case 5: return "Viernes";
            case 6: return "Sabado";
            default: return "No Dia";
        }
    }
    
    
    return  {
        crearHorario: function(div) {
            var nivel = [3,3,2,1,0,0,0,1,1,2,2,3,3,1,1,1,2,0,0,0,0,1,1,2,2,3,3,3,2,2,0,0,0,0,0,0,1,1,1,1,3,3,3,3,3,2,2,0,0,0,0,1,1,1,,3,3,3,2,2,1,3,3,1,1,0,0,0,0,0,0,0];
            var etiqueta = //block, flex
                '<div class="labelDatos" style="display:block !important">' +
                '<table>'+
                '$tabla' +
                '</table>' +
                '</div>';
            var columna = '<td align="center" bgcolor="$COLOR">$dato</td >';
            var columnas ='';
            var fila = '<tr>$columna</tr>' ;
            var filas= '', etiquetaTabla ='', n , m ;
    
            var datos, k=0;
            for(var i=0; i<7; i++) {
                //datos = tabla[i];
                columnas += columna.replace('$dato', "<b>"+getDia(i)+"</b>").replace("$COLOR", getColor(4));
                for(var j=1;j<24;j++) {
                    var color;
                    if(k< nivel.length)
                        color= getColor(nivel[j+k]);
                    else
                        color = "";
                    k++;
                    columnas += columna.replace('$dato', j+" Hrs").replace("$COLOR", color);
                }
                k=Math.round(getRandom(0,15));
                //columna = '<td align="center">$dato</td >';
                filas += fila.replace('$columna', columnas);
                etiquetaTabla += filas;
                columnas='';
                filas ='';
            }
            etiqueta = etiqueta.replace('$tabla', etiquetaTabla);
            document.getElementById(div).innerHTML=etiqueta;
            function getRandom(min, max) {
                return Math.random() * (max - min) + min;
            }
        }
        
    };

});
