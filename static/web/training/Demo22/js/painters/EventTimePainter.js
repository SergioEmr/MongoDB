/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define([
  "luciad/util/IconFactory",
  "luciad/view/feature/FeaturePainter",
  'luciad/util/ColorMap'
], function (IconFactory, FeaturePainter, ColorMap) {

  function EventPainter() {
    this._iconCache = {};
  }

  EventPainter.prototype = new FeaturePainter();
  EventPainter.prototype.constructor = EventPainter;
  EventPainter.prototype.density = {
                colorMap: ColorMap.createGradientColorMap([
                {level: 0, color: "rgba(  0,   0,   0, 0.5)"},
                {level: 1, color: "rgba(  0, 0,   255, 0.5)"},
                {level: 2, color: "rgba(  0, 255,   255, 1.0)"},
                {level: 3, color: "rgba(  255, 255,   0, 1.0)"},
                {level: 5, color: "rgba(255, 0, 0, 1.0)"}
                ])
            };
  EventPainter.prototype.paintBody = function (geoCanvas, feature, shape, layer, map, state) {


    // var dimension = Math.pow(1.5, Math.round(feature.properties.magnitude));
    var dimension = 20;
    var outlineColor = "rgba(255,255,255,0.9)";
    // var fillColor = state.selected ? "rgba(192, 217, 42, 1)" : getEarthquakeColor(feature.properties.magnitude);
    var fillColor = "rgba(192, 217, 42, 1)" ;
    var cacheKey = "" + dimension + outlineColor + fillColor;
    var icon = this._iconCache[cacheKey];
    if (!icon) {
      icon = this._iconCache[cacheKey] = IconFactory.circle({
        stroke: outlineColor,
        fill: fillColor,
        width: dimension,
        height: dimension
      });
    }
    var densityStyle = {
            width: "30px",
            height: "30px",
            draped: true
        };
    var normalStyle = {
        width: dimension + "px",
        height: dimension + "px",
        image: icon
      }; 
    geoCanvas.drawIcon(shape.focusPoint, normalStyle);
  };

  return EventPainter;
});

