/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define(["luciad/view/feature/FeaturePainter",
        "luciad/shape/ShapeFactory",
        'samplecommon/IconFactory',
        'luciad/util/ColorMap',
        "luciad/view/style/PointLabelPosition"
    ], function (FeaturePainter, ShapeFactory, IconFactory, ColorMap, PointLabelPosition) {
    
    function layerPainter() {
    } 
    layerPainter.prototype = new FeaturePainter();
    layerPainter.prototype.constructor = layerPainter;
    layerPainter.prototype.getDetailLevelScales = function() {
        return [1 / 3000000, 1 / 2000000, 1 / 1000000,1 / 800000,1 / 500000];
    };
    
    var sBlanco = 'rgb(255, 255, 255)',     Blanco = 'rgba(255, 255, 255, 0.2)';
    var sGrisOscuro = 'rgb(50, 50, 50)',    GrisOscuro = 'rgba(50, 50, 50, 0.2)';
    var sGrisClaro = 'rgb(200, 200, 200)',  GrisClaro = 'rgba(200, 200, 200, 0.5)';
    var sRojo = 'rgb(255, 0, 0)',           Rojo = 'rgba(255, 0, 0, 0.7)';
    var sAmarilloClaro = 'rgb(255, 255, 204)', AmarilloClaro = 'rgba(255, 255, 204, 0.7)';
    var sAmarillo = "rgb( 244, 208, 63 )";
    var sGris = 'rgb(130, 130, 130)',       Gris = "rgba(130,130,130, 0.2)";
    var sNaranjaClaro = 'rgb(255, 239, 204)', NaranjaClaro = 'rgba(255, 239, 204, 0.2)';
    var sNaranja = 'rgb(255, 174, 102)',    Naranja = 'rgba(255, 174, 102, 0.7)';
    var Verde = "rgba(50,230,50,0.7)",      sVerde = "rgb(50,230,50)";
    var sMorado ="rgb(200,0,200)",          Morado = "rgba(200,0,200, 0.2)";
    var Azul = "rgba(70, 100, 230, 0.2)",   sAzul = "rgb(70, 100, 230)";
    var VerdeClaro = "rgba( 134, 255, 132 , 0.7)", sVerdeClaro = "rgb( 134, 255, 132 )";
    var maxRadio = 0, minRadio = 1;
    var check = new Array(41);
    check = initArray(check);
    function initArray(array) {
        for(var i=0; i<array.length; i++) {
            array[i] = false;
        }
        return array;
    }
    layerPainter.prototype.paintBody = function (geoCanvas, feature, shape, layer, map, state) {
        var color;
        try {
            var name = feature.properties.NOMMUN;
            var tabla = getTablaMunicipios(), maxvalue = tabla[1][1];
            if(!feature.properties.Incidentes) {
                feature.properties.Incidentes = 0;
                for(var i=1; i<tabla.length; i++) {
                    var tmunicipio = tabla[i][0];
                    if(name === tmunicipio) {
                        feature.properties.Incidentes = tabla[i][1];
                        break;
                    }
                }
            } 
            var id = feature.properties.Incidentes;
            var porcent = (id/maxvalue) *100;
            if(porcent<20 || !id) {
                color = getColor(0);
            }else {
            if(porcent < 40) {
                color = getColor(1);
            } else {
                if(porcent < 60) {
                    color = getColor(2);
                } else {
                    if(porcent < 80)
                        color = getColor(3);
                    else 
                        color = getColor(4);

                }
            }
            }
        }catch(e) {
            color = getColor(1);
        }
        
        if(!feature.geometry.x){
            if(state.level > 4) {
                geoCanvas.drawShape(shape,{ 
                    stroke: {
                        color: state.selected ? sBlanco : color.selected,
                        width: 3} 
                });
            } else {
                geoCanvas.drawShape(shape,{ 
                    fill: {color: state.selected ? color.selected : color.normal},
                    stroke: {
                        color: state.selected ? sBlanco : color.selected,
                        width: 2} 
                });
            }
        }
    };
    function getColor(id) {
        switch(id) {
            case 0: return {normal: VerdeClaro, selected: sVerdeClaro};
            case 1: return {normal: Verde, selected: sVerde};
            case 2: return {normal: AmarilloClaro, selected: sAmarilloClaro};
            case 3: return {normal: Naranja, selected: sNaranja};
            case 4: return {normal: Rojo, selected: sRojo};
            default: return {normal: Gris, selected: sGris};
        }
    }
    
    
    layerPainter.prototype.paintLabel = function (labelCanvas, feature, shape, layer, map, state) {
        var label, labelName = "", i=0, properties = feature.properties, index;
        
        if(properties) {
            if(properties.NOMMUN) {
                labelName = properties.NOMMUN;
            }
            else {
                if(properties.NOMBRE) {
                    labelName = properties.NOMBRE;
                } else {
                for(var key in properties) {
                    if(i===0)
                         labelName = properties[key];
                    i++;
            } 
            }
        }
        if(!properties.Incidentes) {
            //labelName = feature.id;
            feature.properties.Incidentes = 0;
        }
        }
        index = feature.properties.Incidentes;
        
        var labelStyle = {
          positions: (PointLabelPosition.NORTH),
          offset: 5,
          priority : 2
        };
        //label = "<span style='color: $color' class='label'>" + labelName + "</span>";
        
       
            label  = '<div class="labelwrapper">' +
                                '<div class="sensorLabel blueColorLabel">' +
                                '<div class="theader">' +
                                '<div class="leftTick blueColorTick"></div>' +
                                '<div class="rightTick blueColorTick"></div>' +
                                '<div class="name">'+labelName+'</div>' +
                                '</div>' +
                                '<div class="type">Incidentes : '+index+'</div>' +
                                '</div>' +
                                '</div>';
            labelCanvas.drawLabel(label,shape.focusPoint, labelStyle);
        
                
        
    };
 
    return layerPainter;
});


