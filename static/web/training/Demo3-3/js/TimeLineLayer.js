/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define([
    'luciad/view/LayerType',
    'luciad/view/feature/FeatureLayer',
    'luciad/reference/ReferenceProvider',
    "luciad/model/feature/FeatureModel",
    "luciad/model/store/MemoryStore",
    "luciad/shape/ShapeFactory",
    "./TimeSlider",
    "./painters/painterTimeLayer",
    "luciad/model/feature/Feature"
], function (LayerType, FeatureLayer, ReferenceProvider, FeatureModel, MemoryStore, 
        ShapeFactory, TimeSlider, painterTimeLayer, Feature) {
   
    function createTimeLineModel(feature, start, end) {
        var dataSetStartTime = start;
        var dataSetEndTime = end;
        var data, elements = [], intervalo = 3600, nIncidentes=0;
        var n = (dataSetEndTime - dataSetStartTime)/intervalo;
        var firstTime = dataSetStartTime, secondTime = firstTime+intervalo;
        for ( var i = 0; i < n; i++) {
            for(var j=0;j<feature.length;j++) {
                data = feature[j];
                var timeInMillis = Date.parse(data.properties.EventTime[0])/1000;
                if(timeInMillis >= firstTime && timeInMillis < secondTime)
                    nIncidentes++;
            }
            
            var p1 = ShapeFactory.createPoint(TimeSlider.REFERENCE, [firstTime, 0]);
            var p2 = ShapeFactory.createPoint(TimeSlider.REFERENCE, [secondTime, 0]);
            var p3 = ShapeFactory.createPoint(TimeSlider.REFERENCE, [secondTime, nIncidentes]);
            var p4 = ShapeFactory.createPoint(TimeSlider.REFERENCE, [firstTime, nIncidentes]);
            var shape = ShapeFactory.createPolygon(TimeSlider.REFERENCE, [p1, p2, p3, p4]);
            var properties = { sensorId:feature.id };
            elements.push( new Feature(shape, properties, i));
            nIncidentes=0;
            firstTime+= intervalo;
            secondTime+= intervalo;
      }
      return new FeatureModel(
          new MemoryStore( {data:elements}),
          {reference: TimeSlider.REFERENCE}
      );
      
    }
    
    return {
        createLayer: function(feature, start, end) {
            var model = createTimeLineModel( feature, start, end);
            //console.log("creando Capa en TimeSlider");
            return new FeatureLayer(
                model, {
                    id: ""+feature.id,
                    layerType: LayerType.STATIC,
                    selectable: true,
                    painter: new painterTimeLayer()
                });
        }
    };
});

