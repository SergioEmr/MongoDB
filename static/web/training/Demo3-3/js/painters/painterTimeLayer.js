/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define([
    "luciad/view/feature/FeaturePainter"
], function (FeaturePainter) {
    var self = this;
    function painterTimeLayer() {
        self._labelStyle = {
          //positions: (PointLabelPosition.NORTH),
          offset: 5,
          priority : 2
        };
    }

    painterTimeLayer.prototype = new FeaturePainter();


    painterTimeLayer.prototype.paintBody = function (geocanvas, feature, shape, layer, map, paintState) {
        //console.log("dibujando Capa");
        if (!paintState.selected) {
            geocanvas.drawShape(shape, {
                stroke: {color: "rgb(13, 20, 28)", width: 3},
                fill: {color: "rgba(230, 20, 100, 0.5)"}
            });
        } else {
            geocanvas.drawShape(shape, {
                stroke: {color: "rgb(13, 20, 28)", width: 3},
                fill: {color: "rgba(210, 160, 120, 0.8)"}
            });
        }
    };

    painterTimeLayer.prototype.paintLabel = function (labelcanvas, feature, shape, layer, map, paintState) {
        if (paintState.selected) {
            var labelText = '<div class="label">' + Math.floor(shape.bounds.height*100)/100 + '</div>';
            labelcanvas.drawLabel( labelText, shape, self._labelStyle );
        }
    };
    return painterTimeLayer;
});


