 
function crearTabla (features) {
    var div = "tabla", i, j, data;
    document.getElementById(div).innerHTML="";
        
    var etiqueta = //block, flex
        '<div class="labelDatos" style="display:block !important">' +
        '<table>'+
        '$tabla' +
       '</table>' +
        '</div>';
    var columna = '<td align="center"><b>$dato</b></td >';
    var columnas ='';
    var fila = '<tr>$columna</tr>' ;
    var filas= '', tabla ='', coordenadas, n=features.length , m;
    
    var encabezado = ['Ajustador','Fecha y Hora del Siniestro', 'Ubicacion Siniestro', 
        'Hora de Arribo','Tiempo estimado','Distancia Recorrida', 'Estado Actual', 'Tiempo Real','¿Llego a tiempo?'];
    for(j=0;j<encabezado.length;j++) {
        columnas += columna.replace('$dato', encabezado[j]);
    }
    columna = '<td align="center">$dato</td >';
    filas += fila.replace('$columna', columnas);
    tabla += filas;
    for(i=0;i<n;i++) {
        
        data = features[i];
        columnas='';
        filas ='';
        //Ajustador
        columnas += columna.replace('$dato', data.properties.Ajustador);
        //Folio
        //Fecha hora siniestro
        columnas += columna.replace('$dato', formatDate(data.properties.EventTime[0]));
        coordenadas = data.geometry.coordinates[0];
        m = data.geometry.coordinates.length;
        //Ubicacion Siniestro
        columnas += columna.replace('$dato', 'lon: '+coordenadas.y.toFixed(4)+'\n  lat: '+coordenadas.x.toFixed(4));
        
        coordenadas = data.geometry.coordinates[m-1];
        //columnas += columna.replace('$dato', 'lon: '+coordenadas.y.toFixed(6)+'\n  lat: '+coordenadas.x.toFixed(6));
        //Hora de llegada
        m = data.properties.EventTime.length;
        columnas += columna.replace('$dato', data.properties.EventTime[m-1]);
        
        //Tiempo de espera
        var tiempoEstimado = calcularTiempo(data.properties.EventTime[0], data.properties.EventTime[m-1]);
        columnas += columna.replace('$dato', tiempoEstimado);
        tiempoEstimado = Date.parse(tiempoEstimado);
        //Distancia
        var distance = data.properties.Distancia/1000;
        columnas += columna.replace('$dato', distance.toFixed(2) +"km");
        //Estado Actual
        var estado;
        
        if(!data.properties.Terminado)
        //if(tiempoActual < Date.parse(data.properties.EventTime[m-1]))
            estado = "En Camino";
        else
            estado = "Terminado";
        columnas += columna.replace('$dato', estado);
        // tiempo Transcurrido
        var tiempoActual = getTiempo(), inicio = data.properties.timestamps[0], tiempoReal = calcularTiempo2(inicio, tiempoActual);
        if(estado === "Terminado")
            tiempoReal = calcularTiempo2(inicio, Date.parse(data.properties.endTime));
        columnas += columna.replace('$dato', tiempoReal);
        
        var tiempoLimite = $("#tiempoLimite").val()*60*1000;
        if(!tiempoLimite)
            tiempoLimite=900000;
        if(estado ==="Terminado") {
            if((data.properties.Duracion ) > tiempoLimite)
                estado = "Atrazado";
            else
                estado = "A tiempo";
        } else 
            estado ="";
        columnas += columna.replace('$dato', estado);
        //columnas += columna.replace('$dato', '5 horas');
        filas += fila.replace('$columna', columnas);
        tabla += filas;
        
    }
    etiqueta = etiqueta.replace('$tabla', tabla);
    document.getElementById(div).innerHTML=etiqueta;
}

function formatDate(d) {
    var n = Date.parse(d);
    //n-= 600000;
    var date = new Date(n);
    var h = date.getHours() <10 ? "0"+date.getHours(): date.getHours();
    var m = date.getMinutes() <10 ? "0"+date.getMinutes(): date.getMinutes();
    var s = date.getSeconds() <10 ? "0"+date.getSeconds(): date.getSeconds();
    return date.getFullYear()+"/"+(date.getMonth()+1)+"/"+date.getDate()+" "+h+":"+m+":"+s;
}
function calcularTiempo(tiempo1, tiempo2) {
    var sTime = Date.parse(tiempo1);
    var eTime = Date.parse(tiempo2);
    var tiempo = eTime - sTime;
    
    var secondsToTime = function (s) {

	  function addZ(n) {
              return (n<10? '0':'') + n;
	  }

	  var ms = s % 1000;
	  s = (s - ms) / 1000;
	  var secs = s % 60;
	  s = (s - secs) / 60;
	  var mins = s % 60;
	  var hrs = (s - mins) / 60;

	  return addZ(hrs) + ':' + addZ(mins) + ':' + addZ(secs);
    };
    return secondsToTime(tiempo);
}

function calcularTiempo2(tiempo1, tiempo2) {
    var sTime = tiempo1;
    var eTime = tiempo2;
    var tiempo = eTime - sTime;
    
    var secondsToTime = function (s) {

	  function addZ(n) {
	    return (n<10? '0':'') + n;
	  }

	  var ms = s % 1000;
	  s = (s - ms) / 1000;
	  var secs = s % 60;
	  s = (s - secs) / 60;
	  var mins = s % 60;
	  var hrs = (s - mins) / 60;

	  return addZ(hrs) + ':' + addZ(mins) + ':' + addZ(secs);
    };
    return secondsToTime(tiempo);
}

function crearDetallesAjustador(features, ajustador, div) {
    //MostrarOcultar("panelControl2");
    var etiqueta = "", pTiempoEspera=0, tiemposEspera=0, distancias=0, pDistancia=0, puntualidad=0;
    document.getElementById(div).innerHTML=etiqueta;
    var i,j, n=0, properties, duration;
    for(i=0; i<features.length;i++) {
        properties = features[i].properties;
        if(properties.Ajustador === ajustador) {
            n++;
            j = properties.EventTime.length;
            var sTime = Date.parse(properties.EventTime[0] );
            var eTime = Date.parse(properties.EventTime[j-1]);
            duration = eTime - sTime;
            tiemposEspera += duration;
            distancias += properties.Distancia/1000;
            if(duration < 900000)
                puntualidad++;
            
        }
            
    }
    
    if(n>0) {
        pTiempoEspera = tiemposEspera/n;
        pDistancia = (distancias/n).toFixed(2);
        puntualidad = ((puntualidad*100)/n).toFixed(2);
        console.log(pTiempoEspera + "\n"+pDistancia+"\n"+puntualidad);
        etiqueta += '<p><big>'+ajustador+'</big></p><table>'+
                '<tr><td><b>Porcentaje de Puntualidad: </b></td><td>'+puntualidad+'%</td></tr>'+
                '<tr><td><b>Promedio de tiempos de recorrido: </b></td><td>'+formatoHora(pTiempoEspera)+'</tr></tr>'+
                '<tr><td><b>Promedio distancias recorridas: </b></td><td>'+pDistancia+' Km</td></tr>';
        etiqueta += '</table>';
    } else {
        etiqueta = "<p><big>No hay informacion de "+ajustador+"</big></p>";
    }
    document.getElementById(div).innerHTML=etiqueta;
    
}

function formatoHora(s) {

	  function addZ(n) {
	    return (n<10? '0':'') + n;
	  }

	  var ms = s % 1000;
	  s = (s - ms) / 1000;
	  var secs = s % 60;
	  s = (s - secs) / 60;
	  var mins = s % 60;
	  var hrs = (s - mins) / 60;

	  return addZ(hrs) + ':' + addZ(mins) + ':' + addZ(secs);
    }
