var trayectoriasGlow, layerTracksT, features =[], featuresT = null, lightEfect=false, tiempoActual, BalloonDatos2, map, layerPuntos, incidentes, trayectoriasLayer;
var datos, tablaDatos= new Array(), realGPS,ubicacionesAjustadores, nombreAjustador, estadoAjustador, GraficasEx, timesliderLayer;
define([
    "recursos/js/MapFactory",
    "recursos/js/BingMapsDataLoader",
    "recursos/js/GoogleMap",
    //"recursos/js/LayerFactory",
    
    "samplecommon/dojo/DojoMap",
    "luciad/reference/ReferenceProvider",
    "samplecommon/LayerConfigUtil",
    "luciad/view/LayerGroup",
    "dojo/dom",
    "dojo/on",
    "luciad/shape/ShapeFactory",
    "luciad/view/LightEffect",
    
    "./TimeChart",
    "./LayerFactory",
    "./Tablas",
    "./BalloonDatos",
    "luciad/view/PaintRepresentation",
    "./Shapes",
    "./DefaultBalloon",
    "./painters/painterTrayectorias",
    
    "luciad/model/store/MemoryStore",
    "luciad/model/feature/FeatureModel",
    "luciad/view/feature/FeatureLayer",
    "luciad/view/controller/BasicCreateController",
    "luciad/view/controller/EditController",
    "luciad/shape/ShapeType",
    "dojo/query",
    "./painters/painterIncidentes",
    'samplecommon/IconFactory',
    
    "./painters/painterAjustadores",
    "./Graficas",
    "./TimeSlider",
    "./TimeLineLayer",
    "luciad/view/LayerTreeVisitor",
    "luciad/view/LayerTreeNode",
    "samples/common/store/RestStore",
    "recursos/js/CustomJsonCodec",
    "./painters/painterTaller",
    'luciad/util/Promise',
    "recursos/js/LayerFactory",
    "template/sampleReady!"
], function (MapFactory, BingMapsDataLoader, GoogleMap, //LayerFactory, 
        DojoMap, ReferenceProvider, LayerConfigUtil, LayerGroup, dom, on, ShapeFactory, LightEffect,
        TimeChart, LayerFactory, Tablas, BalloonDatos, PaintRepresentation, Shapes, DefaultBalloon, painterTrayectorias,
        MemoryStore, FeatureModel, FeatureLayer, BasicCreateController, EditController, ShapeType, query, painterIncidentes, IconFactory,
        painterAjustadores, Graficas, TimeSlider, TimeLineLayer, LayerTreeVisitor, LayerTreeNode, RestStore, CustomJsonCodec, painterTaller, Promise, LayerFactory2) {
    
    var baseUrl = "https://www.olly.science/"; //https://178.128.76.236/
    var baseDataUrl = "data/"; //"../static/web/training/Demo3-3/data/"; 
    var baseRecursosUrl = "../../proyecto/recursos/"; //"../static/web/proyecto/recursos/"; 
    
    var referenceC = ReferenceProvider.getReference("CRS:84");
    var referenceE = ReferenceProvider.getReference("EPSG:4978");
    var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i
            .test(navigator.userAgent);
    //isMobile=true;
    var graficaTiempos, graficaTrabajo, historialGPS= new Array(10);
    BalloonDatos2 = BalloonDatos;
    GraficasEx =Graficas;
    var aN=0;
    var light, leyendo = false, l=0, l2=0;
    var actualTime = new Date(), estadoAnterior = new Array(10);
    var dataSetStartTime = Date.parse(actualTime);
    var dataSetEndTime = dataSetStartTime + 12*60*60*1000, talleres = [];
    var startTime =0, endTime = Date.parse("2017/06/28 00:00:00");
    var trayectoriasLocal;
    tiempoActual =dataSetStartTime;
    estadoAjustador = ["libre","libre","libre","libre","libre","libre","libre","libre","libre","libre","libre"];
    ubicacionesAjustadores = [[19.342438327962142,-99.06167505510022],[19.308226005330525,-99.06853118818007],
            [19.35700609858175,-99.11054466413465],[19.436413596205327,-99.16223145533553], [19.429676632996603,-99.11510975359194],[19.355189135381863,-99.19969927537619],
            [19.36576064368488,-99.05618189103764],[19.276410293509358,-99.16626313760459], [19.344846383456797,-99.23971292951384]];
    nombreAjustador = ["Tony Dominguez", "Angeles Dominguez","Alberto mendoza","Mauricio Garza",
                    "Julio Garza", "Manuel Bargas","Diana Gonzalez", "Juan Perez", "Cristina del Valle"];
    
    MostrarOcultar(null, "panelTimeSlider");
    var ptimeslider = false, trayectorias = new Array(10);
    
    iniciarHistorial();
    function iniciarHistorial() {
        for(var i=0; i<10; i++) 
            historialGPS[i] = {x: new Array(), y: new Array(), z: 0};
    }
    
    
    Start();
    
    /*
     * Funcion encargada de crear el mala dependiendo de si esta en un dispositivo mobil o no
     * creara el mapa en 3d o 2d, ademas de si esta activado e mapa de google 
     * 
     * Google: satellite, hybrid, roadmap o terrain
     */
    function Start() {
        if(!isMobile) {
                map = MapFactory.makeMap3d("map", {reference: referenceE}, {
                    includeBackground: false, 
                    includeElevation: true, 
                    includeLayerControl: true,
                    newLayerControl: true, 
                    noLayerDelete: true,
                    includeMouseLocation: false});
        } else {
            map = MapFactory.makeMap3d("map", {reference: referenceC}, {
                    includeBackground: false, 
                    includeElevation: false, 
                    includeLayerControl: true,
                    newLayerControl: true, 
                    noLayerDelete: true,
                    includeMouseLocation: false});
        }
        
        light = map.effects.light;
        CrearCapas();
        var coordenadas = [-99.17861340342239,0,18.848666772818145, 1, 10, 0];
        map.mapNavigator.fit({
                bounds: ShapeFactory.createBounds(referenceC, coordenadas),
                animate: true
        });
    }
    
    function CrearCapas() {
        DojoMap.configure(map);
        LayerConfigUtil.addLonLatGridLayer(map);
        
        try{
            BingMapsDataLoader.createBingLayer("AerialWithLabels", map); //AerialWithLabels Road Aerial
            BingMapsDataLoader.createBingLayer("Road", map);
        }catch(e){ console.log("Error al cargar Bing Maps"); }
        
        layerPuntos = LayerFactory.createLayer(baseDataUrl+"ajustadores2.json", referenceC,{label:"Ajustadores", id:"ajustadores", painter: new painterAjustadores(), selectable: true});
        //map.layerTree.addChild(layerPuntos);
        //console.log(layerPuntos);
        //layerPuntos.setPaintRepresentationVisible(PaintRepresentation.LABEL, false);
        layerPuntos.balloonContentProvider = function (feature) {
            return BalloonDatos.getBallon(feature);
        };
        //actualizarUbicaciones();
        //LayerFactory.createGlowSlopesLayerData("data/tracksTerminadas.json", dataSetStartTime, 
          //  dataSetEndTime, "rgb(30, 10, 200)", "Trayectos Terminados", map, true);
        var codec = new CustomJsonCodec();//GeoJsonCodec();
        var store= new RestStore({target: "/createandedit", codec: codec});
        //var store = new MemoryStore({url: "/createandedit"});
        var featureModel = new FeatureModel(store, {reference: referenceC});
        trayectoriasLayer =new FeatureLayer(featureModel, {label: "Trayectorias", id:"trayectorias", selectable: true, editable: false, painter: new painterTrayectorias()});
        map.layerTree.addChild(trayectoriasLayer, "top");
        trayectoriasLayer.balloonContentProvider = function (feature) {
            return DefaultBalloon.getBallon(feature);
        };
        trayectoriasLocal = LayerFactory2.createFeatureLayer(referenceC, {label: "Trayectorias (L)", editable: false, selectable: true, painter: new painterTrayectorias()});
        map.layerTree.addChild(trayectoriasLocal, "top");
        trayectoriasLocal.balloonContentProvider = function (feature) {
            return DefaultBalloon.getBallon(feature);
        };
        
        trayectoriasGlow = LayerFactory.createGlowSlopesLayerData(null, 'rgb(255, 255, 204)', "Trayectoria en Movimiento", tiempoActual);
        map.layerTree.addChild(trayectoriasGlow, "top");  
        
        var store2 = new MemoryStore({});
        var featureModel2 = new FeatureModel(store2, {reference: referenceC});
        incidentes =new FeatureLayer(featureModel2, {label: "Incidentes", id:"incidentes", selectable: true, editable: true, painter: new painterIncidentes()});
        map.layerTree.addChild(incidentes, "top");
        
        var store3 = new MemoryStore([]);
        var featureModel3 = new FeatureModel(store3, {reference: referenceC});
        realGPS =new FeatureLayer(featureModel3, {label: "GPS", id:"gps", selectable: true, editable: false, painter: new painterAjustadores()});
        map.layerTree.addChild(realGPS, "top");
        realGPS.balloonContentProvider = function (feature) {
             return BalloonDatos.getBallon(feature);
        };
        actualizarUbicaciones(baseDataUrl+"ajustadores2.json?actualizacion="+aN, "json", 9);
    }
    
    
    /*
     * Al presionar los botones
     */
    var paneles=false;
    ["paneles", "bDetalles", "switchMap", "exportXLS", "btimeslider", "botonPanico", "botonOk", "restart"].forEach(function(v) {
        $("#"+v).click(function(e) {
            switch(v) {
                case "restart":
                    restartFeature(trayectoriasLayer);
                    alert("El demo a sido reiniciado");
                    break;
                case "botonPanico":
                    estadoAnterior[0] = estadoAjustador[0];
                    estadoAjustador[0] = "Peligro";
                    console.log(estadoAjustador[0]+" Ajustador "+nombreAjustador[0]);
                    break;
                case "botonOk":
                    estadoAjustador[0] = "sinPeligro";
                    var f2 = trayectoriasLayer.model.get("001");
                    var f3 = trayectoriasLocal.model.get("001");
                    try {
                        f2.properties.Estado = 1;
                        trayectoriasLayer.model.put(f2);
                        f3.properties.Estado = 1;
                        trayectoriasLocal.model.put(f3);
                    }catch(e) { 
                        console.log(e);
                    }
                    console.log(estadoAjustador +" Ajustador "+nombreAjustador[0]);
                    break;
                case "paneles":
                    if(paneles) {
                        MostrarOcultar(null, "panelControl");
                        
                        paneles=false;
                    } else {
                        MostrarOcultar("panelControl", "panel1");
                        paneles=true;
                        
                    }
                    break;
                case "bDetalles":
                    MostrarOcultar("panelControl2", null);
                    detallesTodosAjustadores();
                    //creargrafica(features);
                    break;
                case "exportXLS":
                    e.preventDefault();
                    var dataType = 'data:application/vnd.ms-excel';
                    var tableDiv = document.getElementById("tabla");
                    var tableHTML = tableDiv.outerHTML.replace(/ /g, '%20');
                    //var d = new Date();
                    var a = document.createElement('a');
                    a.href = dataType +', '+ tableHTML;
                    a.download = formatDate()+".xls";
                    a.click();
                    break;
                case "btimeslider":
                    if(ptimeslider === true) {
                        MostrarOcultar(null, "panelTimeSlider");
                        ptimeslider = false;
                    } else {
                        MostrarOcultar("panelTimeSlider", null);
                        ptimeslider = true;
                    }
                        
                    break;
                default: break;
            }
        });
    });
    
    function formatDate(time) {
        var date;
        if(time)
            date = new Date(time);
        else
            date= new Date();
        var h = date.getHours() <10 ? "0"+date.getHours(): date.getHours();
        var m = date.getMinutes() <10 ? "0"+date.getMinutes(): date.getMinutes();
        var s = date.getSeconds() <10 ? "0"+date.getSeconds(): date.getSeconds();
        if(time)
            return date.getFullYear()+"/"+(date.getMonth()+1)+"/"+date.getDate()+" "+h+":"+m+":"+s;
        else
            return date.getFullYear()+""+(date.getMonth()+1)+""+date.getDate()+"_"+h+""+m;
    }
    function formatTime() {
        var date = new Date();
        var h = date.getHours() <10 ? "0"+date.getHours(): date.getHours();
        var m = date.getMinutes() <10 ? "0"+date.getMinutes(): date.getMinutes();
        var s = date.getSeconds() <10 ? "0"+date.getSeconds(): date.getSeconds();
        return h+":"+m+":"+s;
    }
    
    /*
     * linkCreateButton: encargado de crear los controles de creacion de poligonos nuevos para la capa de editable.
     * @param {Object} map
     * @param {String} domId
     * @param {int} shapeType
     * @returns {undefined}
     */
    function linkCreateButton(map, domId, shapeType) {
        try {
          on(dom.byId(domId), "click", function() {
            var stylePressed = "pressed";
            query(".pressed").removeClass(stylePressed);
            query("#" + domId).addClass(stylePressed);
            map.selectObjects([]);
            var createController = new BasicCreateController(ShapeType.POINT, {}, {finishOnSingleClick: true});
            /*createController.onCreateNewObject = function(a, b,c) {
                return ShapeFactory.createCircleByCenterPoint();
            };*/
            createController.onObjectCreated = function() {
                BasicCreateController.prototype.onObjectCreated.apply(this, arguments);
                query(".pressed").removeClass(stylePressed);
                var selectedFeature = arguments[2];
                if(shapeType ===0)
                    buscarRuta(selectedFeature.geometry.coordinates, selectedFeature.id-idTaller);
                if(shapeType === 1)
                    crearTaller(selectedFeature);
            };
            map.controller = createController;
            
          });
        }catch(e) {
            console.log("no se creo el control para el boton "+ domId);
            console.log(e);
        }
    }
    
    linkCreateButton(map, "createPoint", 0);
    linkCreateButton(map, "createCircle", 1);
    var idTaller=0;
    
    function crearTaller(selectedFeature) {
        console.log("Taller " + idTaller);
        var coordenadas = selectedFeature.geometry.coordinates;
        var circle = Shapes.createCircle(referenceC, coordenadas.x, coordenadas.y, 0, 200, "taller"+idTaller, {type: "Taller"});
        talleres[idTaller] = circle;
        idTaller++;
        trayectoriasLayer.model.put(circle); 
        trayectoriasLocal.model.put(circle);
    }
    
    function buscarRuta(destinos, id) {
        try {
        var coordenadasDestino = [[destinos.y, destinos.x]];
        console.log(JSON.stringify(ubicacionesAjustadores));
        console.log(JSON.stringify(coordenadasDestino));
        $.getJSON('https://api.sintrafico.com/matrix', {
            key: '57550ca0ea132fa418cc2420955a7adcc390ddb8f2c6366e4d2cfb53231f6e2d',
            start:  JSON.stringify(ubicacionesAjustadores),
            end:  JSON.stringify(coordenadasDestino)
        })
        .done(function(data, textStatus, jqXHR) {
            //console.log(data);
            var datos = Shapes.crearRuta(referenceC, data);
            var i, y=0, feature=datos[0], menorT=datos[0].properties.duration, properties;
            for(i=0;i<datos.length;i++){
                if(menorT > datos[i].properties.duration && estadoAjustador[i] ==="libre"){
                    feature = datos[i];
                    properties = datos[i].properties;
                    menorT = datos[i].properties.duration;
                    y=i;
                    i=-1;
                }
            }
            estadoAjustador[y] = "en camino";
            estadoAnterior[y] = "en camino";
            trayectorias[y] = feature.geometry.coordinates;
            feature.id=id;
            feature.properties.Ajustador = nombreAjustador[y];
            feature.properties.LongitudDestino = destinos.x;
            feature.properties.LatitudDestino = destinos.y;
            
            var featureIncidente = Shapes.createPoint(referenceC, destinos.x, destinos.y, 0, id+0.1, feature.properties);
            console.log(nombreAjustador[y] + " "+estadoAjustador[y]);
            var featureTime = LayerFactory.crearFeatureTime(feature);
            featureTime.id=id;
            features [id-1]= featureTime; 
            try {
                //trayectorias.model.remove(id);
                
                trayectoriasLayer.model.put(feature); 
                trayectoriasLayer.model.put(featureIncidente);
                trayectoriasLocal.model.put(feature);
                trayectoriasLocal.model.put(featureIncidente);
                trayectoriasGlow.model.add(featureTime);
            } catch(e) {
                trayectoriasLayer.model.remove(id);
                trayectoriasLayer.model.add(feature); 
                trayectoriasLocal.model.remove(id);
                trayectoriasLocal.model.add(feature);
                trayectoriasGlow.model.remove(id);
                trayectoriasGlow.model.add(featureTime);
            }
            tablaDatos = Graficas.crearArreglo(features);
            console.log(tablaDatos);
            crearTabla(features);
            
            Graficas.actualizarGraficaIdTexto(graficaTrabajo, tablaDatos, "columnchart", "Actividsad", 2);
            Graficas.actualizarGraficaLabelValor(graficaTiempos, tablaDatos, "piechart", "Tiempos", 2, 3);
            displayTimeLineForSensor();
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
            //alert('Error ' + jqXHR.status);
            console.log("Error getJSON: "+ jqXHR.status);
        });
        }catch(e) {
            console.log(e);
        }
    }
    
    graficaTrabajo = Graficas.crearGraficaIdTexto("graficaTrabajo", tablaDatos, "columnchart", "Actividad", 2);
    graficaTiempos = Graficas.crearGraficaLabelValor("graficaTiempos", tablaDatos, "piechart", "Tiempos", 2, 3);
    /*
     * dentro de esta funcion esta la parte de seleccion. despues de crear un poligono (linea ) y al seleccionarlo se crea el control de edicion para poder cambiar el poligono.
     */
    map.on("SelectionChanged", function (event) {
        var selectedFeature = null;
        if (event.selectionChanges.length > 0 && event.selectionChanges[0].selected.length > 0 && event.selectionChanges[0].layer.id === 'incidentes' && !isMobile) {
            selectedFeature = event.selectionChanges[0].selected[0];
            var stylePressed = "pressed";
            query(".pressed").removeClass(stylePressed);
            var control = new EditController(event.selectionChanges[0].layer, selectedFeature, {finishOnSingleClick: true});
            
            control.onDraw = function(geoCanvas) {
                 var icon, dimension= 14;
                icon  = IconFactory.circle({stroke: 'rgb(255, 255, 255)', fill: "rgba(170, 20, 60, 0.5)", width: dimension, height: dimension});
                geoCanvas.drawIcon(selectedFeature.geometry.focusPoint,{
                    width: dimension+"px",
                    height: dimension+"px",
                    image: icon
                });
                trayectoriasLayer.model.remove(selectedFeature.id);
                trayectoriasLocal.model.remove(selectedFeature.id);
                trayectoriasGlow.model.remove(selectedFeature.id);
                buscarRuta(selectedFeature.geometry.coordinates, selectedFeature.id);
            };
            map.controller = control;
            
        }
    });
    
    function restartFeature(layer) {
        //ar f = layer.model.store._data, id;
        var promise = layer.model.query();
        Promise.when(promise, function(cursor) {
            var index = 0;
            while (cursor.hasNext()) {
                var feature = cursor.next();
                layer.model.remove(feature.id);
                index++;
            }
            console.log("Borrados " + index + " features");
        });
        /*layer.model.remove("001");
        for(var i=0;i<20;i++){
            try { layer.model.remove(i); }catch(e) {}
            try { layer.model.remove(i+0.1); } catch(e) {}
            try { layer.model.remove("taller"+i); }catch(e) { }
        }*/
    }
    function actualizarEstados(features) {
        var n = features.length, feature, i, j, k, tiempoActual, tiempoFinal;
        for(i=0;i<n;i++) {
            feature = features[i];
            tiempoActual = getTiempo();
            k = feature.properties.EventTime.length;
            tiempoFinal = Date.parse(feature.properties.EventTime[k-1]);
            var distancia ;
            /*
            if(tiempoActual > tiempoFinal && !feature.properties.Terminado) {
                for(j=0;j<nombreAjustador.length;j++) {
                    if(nombreAjustador[j] === feature.properties.Ajustador){
                        estadoAjustador[j] = "libre";
                        estadoAnterior[j] = "libre";
                        trayectorias[j] = [];
                        features[i].properties.Terminado = "terminado";
                        features[i].properties.endTime = formatDate(tiempoActual);
                        console.log(nombreAjustador[j]+" "+estadoAjustador[j]);
                        break;
                    }
                }
            }
            */
        }
    }
    
    function actualizarUbicaciones(url, type, id) {
        $.ajax({
            type:'Get',
            dataType: type,//"json",
            url: url//"data/gps/infoGPS.json"
        }).done(function(data) {
            var properties, feature, x, y, oldx, oldy, z=0, newFeatures = [];
            //console.log("Leido: "+url);
            
            if(data.features) {
                map.layerTree.removeChild(layerPuntos, false);
                layerPuntos=null;
                for(var i=0; i<data.features.length;i++) {
                    feature = data.features[i];
                    properties = feature.properties;
                    x = feature.geometry.coordinates[0];
                    y = feature.geometry.coordinates[1];
                    ubicacionesAjustadores[i] = [y,x];
                    if(nombreAjustador[i]!==properties.Ajustador){
                        nombreAjustador[i] = properties.Ajustador;
                    } 
                    newFeatures[i] = Shapes.createPoint(referenceC, x, y, z, feature.id, properties);
                    historialGPS[i].properties = properties;
                }
                var store = new MemoryStore({data: newFeatures});
                var featureModel = new FeatureModel(store, {reference: referenceC});
                realGPS = new FeatureLayer(featureModel, {label: "Ajustadores GPS", id:"ajustadores", selectable: true, editable: false, painter: new painterAjustadores()});
                map.layerTree.addChild(realGPS, "top");
                realGPS.balloonContentProvider = function (feature) {
                    return BalloonDatos.getBallon(feature);
                };
                
            } else {
                x = data.position.longitude;
                y = data.position.latitude;
                if(x>0)
                    x= x*(-1);
                var n = nombreAjustador.length, m;
                if(nombreAjustador[0]==="Juan Carlos") {
                    ubicacionesAjustadores[0] = [y,x];
                    nombreAjustador[0] = "Juan Carlos";
                    m=0;
                } else {
                    ubicacionesAjustadores[n] = [y,x];
                    nombreAjustador[n] = "Juan Carlos";
                    m=n;
                }
                var f = realGPS.model.get(data.serial);
                
                properties = data.datetime;
                properties.Ajustador= "Juan Carlos";
                properties.Velocidad = data.speed;
                properties.Direccion = data.angle;
                properties.Estado = data.antennaState;
                var f2 = trayectoriasLayer.model.get("001");
                var f3 = trayectoriasLocal.model.get("001");
                f2 = f2 || f3;
                if(f2.properties) {
                    if(f.properties.Estado === "Peligro" || estadoAjustador[0] === "Peligro" || f2.properties.Estado === "Peligro") {
                        properties.Estado = "Peligro";
                        console.log("Estado de Juan Carlos "+f.properties.Estado);
                    }
                } else {
                    if(f.properties.Estado === "Peligro" || estadoAjustador[0] === "Peligro" ) {
                        properties.Estado = "Peligro";
                        console.log("Estado de Juan Carlos "+f.properties.Estado);
                    }
                }
                switch(estadoAjustador[0]) {
                    case "sinPeligro":
                        properties.Estado = "sinPeligro";
                        estadoAjustador[0] = estadoAnterior[0];
                        console.log("Estado de Juan Carlos "+f.properties.Estado);
                        break;
                    case "Taller":
                        properties.Estado = "Taller";
                        console.log("Estado de Juan Carlos "+f.properties.Estado);
                        break;
                    default: break;
                }
                
                historialGPS[id].x[aN-1] = x;
                historialGPS[id].y[aN-1] = y;
                historialGPS[id].properties = properties;
                
                //console.log(m);
                newFeatures[0] = Shapes.createPoint(referenceC, x, y, z, data.serial, properties);
                newFeatures[1] = Shapes.createPolyline(referenceC, historialGPS[id].x, historialGPS[id].y, z, "track"+id, historialGPS[id].properties);
                try {
                    realGPS.model.put(newFeatures[0]);
                    realGPS.model.put(newFeatures[1]);
                    trayectoriasLayer.model.put(newFeatures[0]);
                    trayectoriasLocal.model.put(newFeatures[0]);
                }catch(e) {
                    console.log(e);
                }
                
            }
        }).fail(function(e) {
            console.log("No actualizado "+url);
            console.log(e);
        });
    }
    
    function actualizarImagen() {
        url = baseDataUrl+"gpsv4/mpeg4.JPG?actualizacion="+aN;
        document.getElementById("imagen").innerHTML = "<img  width=500  height=450 src=\""+url+"\">";
    }
    
    function actualizarLayer() {
        if(trayectoriasLayer) {
            map.layerTree.removeChild(trayectoriasLayer, false);
            trayectoriasLayer=null;
            var codec = new CustomJsonCodec();
            var store= new RestStore({target: "/createandedit", codec: codec});
            var featureModel = new FeatureModel(store, {reference: referenceC});
            trayectoriasLayer =new FeatureLayer(featureModel, {label: "Trayectorias", id:"trayectorias", selectable: true, editable: false, painter: new painterTrayectorias()});
            map.layerTree.addChild(trayectoriasLayer, "below", realGPS);
            trayectoriasLayer.balloonContentProvider = function (feature) {
                return DefaultBalloon.getBallon(feature);
            };
        }
    }
    /*
     * Iniciar Variables de tiempo y el TimeChart
     */
    var speedup = 10, lasttime = Date.now(),  playing = true;
    var timeChart = new TimeChart(dom.byId("timeChart"), new Date(dataSetStartTime), new Date(dataSetEndTime));
    
    function timeUpdated() {
        var dataSetTime = timeChart.getCurrentTime().getTime();
        //tiempoActual = dataSetTime;
        try{
            trayectoriasGlow.painter.timeWindow = [dataSetTime - 100000, dataSetTime];
        }catch(e){}
        if(l >250) {
            l=0;
            l2++;
            actualizarUbicaciones(baseDataUrl+"gpsv4/infoGPS.json?actualizacion="+aN, "json", 0);
            comprovarUbicacion(0, features);
            if(l2 > 3) {
                console.log(formatTime());
                l2 = 0;
                for(var i=1; i<nombreAjustador.length; i++) {
                    continuarTrayectoria(i, historialGPS[i].z);
                    comprovarUbicacion(i, features);
                }
                
                //continuarTrayectoriaGPS();
            }
            aN++;
            //actualizarUbicaciones("data/ajustadores2.json?actualizacion="+aN, "json");
            actualizarLayer();
            if(!isMobile)
                actualizarImagen();
            
        }
        else
            l++;
        crearTabla(features);
        actualizarEstados(features);
        $('#timelabel').text(formatTime());
        
     }
     
     function wireListeners() {
        timeChart.addInteractionListener(timeUpdated);
        timeChart.addZoomListener(function(currZoomLevel, prevZoomLevel) {
            speedup *= prevZoomLevel / currZoomLevel;
        });
        $("#play").click(function() {
            lasttime = Date.now();
            play(!playing);
        });
        window.navigateTo = function(moveTo) {
            var wgs84 = ReferenceProvider.getReference("EPSG:4326");
            var center = ShapeFactory.createPoint(wgs84, [moveTo.lon, moveTo.lat]);
            var radius = moveTo.radius || 5.0;
            var bounds = ShapeFactory.createBounds(wgs84, [center.x - radius, 2 * radius, center.y - radius, 2 * radius]);
            return map.mapNavigator.fit(bounds);
        };
    }
    
    function playStep() {
        if (playing) {
            var currTime = Date.now();
            var deltaTime = (currTime - lasttime);
            lasttime = currTime;
            var timeChartTime = timeChart.getCurrentTime().getTime();
            var newTime = timeChartTime + (deltaTime) * speedup;
            if (newTime >= timeChart.endDate.getTime()) {
            newTime = timeChart.startDate.getTime();
        }
        tiempoActual = currTime;
        timeChart.setCurrentTime(new Date(currTime));
        window.requestAnimationFrame(playStep);
        }
    }

    function play(shouldPlay) {
        playing = shouldPlay;
        if (playing) {
            playStep();
        }
        var playBtn = $('#play');
        playBtn.toggleClass("active", playing);
        playBtn.find("> span").toggleClass("glyphicon-pause", playing);
        playBtn.find("> span").toggleClass("glyphicon-play", !playing);
    }

    function setTime(milliseconds) {
        timeChart.setCurrentTime(new Date(timeChart.startDate.getTime() + milliseconds));
        timeUpdated();
    }
    
    wireListeners();
    setTime(0);
    play(playing);
    
    /*
     * TimeSlider
     */
    var timeSlider;
    timeSlider = new TimeSlider("timeslider");
    timeSlider.setValidRange(dataSetStartTime/1000, dataSetEndTime/1000, 0, 10);
    
    function handleSelectionEvent(event) {
        for(var i in event.selectionChanges) {
            var selection = event.selectionChanges[i];
            if (selection.layer.label === "Incidentes") {
                for(var j in selection.selected) {
                    var selectedSensor = selection.selected[j];
                    displayTimeLineForSensor();
                }
            }
        }
    };
        
     function displayTimeLineForSensor() {
          //var sensorId = sensor.id;
        if(!timesliderLayer){
            timesliderLayer = TimeLineLayer.createLayer(features, dataSetStartTime/1000, dataSetEndTime/1000);
            timeSlider.layerTree.addChild(timesliderLayer, "top");
        } else {
            timeSlider.layerTree.removeChild(timesliderLayer, false);
            timesliderLayer = TimeLineLayer.createLayer(features, dataSetStartTime/1000, dataSetEndTime/1000);
            timeSlider.layerTree.addChild(timesliderLayer, "top");
        }
          
          //layers[""+sensorId].visible = true;

          if (!!timesliderLayer.bounds) {
            /**
             * LayerTree Children visitor used to fit the bounds ont the bounds of visible layers.
             * @type {{foundLayer: null, visitLayer: findQueryableLayerVisitor.visitLayer, visitLayerGroup: findQueryableLayerVisitor.visitLayerGroup}}
             */
            var visibleLayerVisitor = {
              bounds:{},
              visitLayer: function(layer) {
                if(layer.visible === true) {
                  this.bounds.setTo3DUnion(layer.bounds);
                }
                return LayerTreeVisitor.ReturnValue.CONTINUE;
              },
              visitLayerGroup: function(layerGroup) {
                layerGroup.visitChildren(this, LayerTreeNode.VisitOrder.TOP_DOWN);
              }
            };
            visibleLayerVisitor.bounds = timesliderLayer.bounds.copy();
            timeSlider.layerTree.visitChildren(visibleLayerVisitor, LayerTreeNode.VisitOrder.TOP_DOWN);
            try{
                timeSlider.mapNavigator.fit(
                {
                  bounds: visibleLayerVisitor.bounds,
                  animation: {duration: 10000},
                  allowWarpXYAxis: true,
                  fitMargin: "5%"
                });
            }catch(e) {
                console.log(e);
            }
          }
    };
    
    function continuarTrayectoriaGPS() {
        var id =0;
        if(trayectorias[id]) {
            var trayectoria = trayectorias[id];
            var limite = trayectoria.length;
                var f = realGPS.model.get("001"), i;
                var lon = trayectoria[0][0], lonGPS = f.geometry.x;
                var lat = trayectoria[0][1], latGPS = f.geometry.y;
                var distancia = Math.sqrt(Math.pow((lon - lonGPS),2) + Math.pow((lat - latGPS),2));
                var menorDistancia = distancia, x = lon, y = lat;
                for(i=0;i<limite;i++) {
                    lon = trayectoria[i][0];
                    lat = trayectoria[i][1];
                    distancia = Math.sqrt(Math.pow((lon - lonGPS),2) + Math.pow((lat - latGPS),2));
                    console.log(distancia +" "+menorDistancia);
                    if(distancia > menorDistancia) {
                        menorDistancia = distancia;
                        x = lon;
                        y = lat;
                    }
                }
                var properties = {Type: "Trayectoria", Ajustador: nombreAjustador[id]};
                var historial = historialGPS[id];
            
                historial.x[historial.z] = x;
                historial.y[historial.z] = y;
                //historialGPS[id].properties = properties;
                ubicacionesAjustadores[id][0] = lat;
                ubicacionesAjustadores[id][1] = lon;
                var feature = Shapes.createPoint(referenceC, lon, lat, 0, id+"", historial.properties);
                realGPS.model.put(feature);
                feature = null;
                feature = Shapes.createPolyline(referenceC, historial.x, historial.y, 0, "track"+id, properties);
                realGPS.model.put(feature);
                historialGPS[id].z = historialGPS[id].z +1;
            }
        
    }
    
    function continuarTrayectoria(id,x) {
        
        if(trayectorias[id]) {
            var trayectoria = trayectorias[id];
            var limite = trayectoria.length;
            if(x < limite) {
                var lon = trayectoria[x][0];
                var lat = trayectoria[x][1];
                var properties = {Type: "Trayectoria", Ajustador: nombreAjustador[id]};
                var historial = historialGPS[id];
            
                historial.x[historial.z] = lon;
                historial.y[historial.z] = lat;
                //historialGPS[id].properties = properties;
                ubicacionesAjustadores[id][0] = lat;
                ubicacionesAjustadores[id][1] = lon;
                var feature = Shapes.createPoint(referenceC, lon, lat, 0, id+"", historial.properties);
                realGPS.model.put(feature);
                feature = null;
                feature = Shapes.createPolyline(referenceC, historial.x, historial.y, 0, "track"+id, properties);
                realGPS.model.put(feature);
                historialGPS[id].z = historialGPS[id].z +1;
            }
        }
    }
    
    var tiempoTaller= new Array(10);
    function comprovarUbicacion(id, features) {
        var trayectoria = trayectorias[id], nombre = nombreAjustador[id], i, j, feature;
        var n = historialGPS[id].x.length-1, lastx = historialGPS[id].x[n], lasty = historialGPS[id].y[n];
        if(talleres.length >0) {
            var f;
            if(id === 0 )
                f= realGPS.model.get("001");
            else
                f = realGPS.model.get(id);
            for(j=0;j<talleres.length;j++) {
                var geometry = talleres[j].geometry;
                var h = geometry.bounds.height, w = geometry.bounds.width;
                r = h/2;
                var fx = geometry.focusPoint.x, fy = geometry.focusPoint.y;
                distanciat= Math.sqrt(Math.pow(lastx-fx,2) + Math.pow(lasty-fy,2));
                console.log("Distancia a tallar "+distanciat);
                if(distanciat < r) {
                    if(tiempoTaller[id]) {
                        var actual = getTiempo(), transcurrido = actual - tiempoTaller[id], lim = 2*60*1000;
                        console.log("tiempo dentro del Taller "+ formatTime(transcurrido));
                       
                       
                        estadoAjustador[id] = "Taller";
                        if(transcurrido > lim)
                            alert(nombreAjustador[id]+" Paso Mucho tiempo en el Area del Taller");
                    } else {
                        console.log("Cercano a Taller");
                        alert(nombre +" dentro del area de un Taller");
                        tiempoTaller[id] = getTiempo();
                    }
                } else {
                    tiempoTaller[id] = 0;
                }
            }
            if(tiempoTaller[id]) {
                estadoAjustador[id] = "Taller";
                f.properties.Estado = "Taller";
            } else {
                estadoAjustador[id] = estadoAnterior[id];
                f.properties.Estado = 1;
            }
            realGPS.model.put(f);
        }
        if(trayectorias[id]) {
            
            for(i=0; i<features.length;i++) {
                feature = features[i];
                var nombre2 = feature.properties.Ajustador;
                if(nombre2 === nombre)
                    break;
            }
            n = feature.geometry.coordinates.length -1;
            var lon = feature.geometry.coordinates[n][0], lat= feature.geometry.coordinates[n][1];
            var destinoX = feature.properties.LongitudDestino, destinoY = feature.properties.LatitudDestino;
            var distancia = Math.sqrt(Math.pow((lon - lastx),2) + Math.pow((lat - lasty),2));
            var r, tx, ty, distanciat;
            
            
            console.log("Distancia de "+nombre+" "+distancia);
            if(distancia ===0 ) {
                for(var j=0;j<nombreAjustador.length;j++) {
                    if(nombreAjustador[j] === nombre && features[i].properties.Terminado !== "terminado" ){
                        estadoAjustador[j] = "libre";
                        features[i].properties.Terminado = "terminado";
                        features[i].properties.endTime = formatDate(tiempoActual);
                        historialGPS[i].z = 0;
                        console.log(nombreAjustador[j]+" "+estadoAjustador[j]);
                        break;
                    }
                }
            } else {
                if(id ===0) {
                    if(distancia < 0.00010102816342695557 && features[i].properties.Terminado !== "terminado"  ) {
                        estadoAjustador[0] = "libre";
                        features[i].properties.Terminado = "terminado";
                        features[i].properties.endTime = formatDate(tiempoActual);
                        historialGPS[0].z = 0;
                        console.log(nombreAjustador[0]+" "+estadoAjustador[0]);
                    }
                }
            }
        }
    }
    
});

var names = [], featureTerminado;
function setTrayectoryGlowLayer(layer, d) {
    trayectoriasGlow = layer;
    trayectoriasGlow.balloonContentProvider = function (feature) {
            return BalloonDatos2.getBallon(feature);
    };
}

function setLayer2(layer, d) {
    layerTracksT = layer;
    layerTracksT.balloonContentProvider = function (feature) {
            return BalloonDatos2.getBallon(feature);
    };
    featuresT = d;
    
}

function getTiempo() {
    return tiempoActual;
}
    
function MostrarOcultar(node1, node2) {
        $("#"+node2).fadeOut("slow");
        $("#"+node1).fadeIn("slow");
    }

function deleteFilters() {
    layerPuntos.filter = null;
    jQuery('#tags').val('');
    console.log("Filtros borrados");
}
function filter(label) {
    if (label === 'none') {
        label = jQuery('#tags').val();
        if (label === '') {
            return;
        }
    }

    //label = label.trim();
    var navigator = false;
    deleteFilters();
    
    if (layerPuntos.visible) {
        layerPuntos.filter = function (feature) {
            switch(label) {
                case feature.properties.Ajustador: return true;
                case feature.properties.Transpoorte: return true;
                case feature.properties.Tipo: return true;
                case feature.properties.Situacion: return true;
                case feature.properties.Poliza: return true;
                case feature.properties.Cliente.Nombre: return true;
                default: return false;
            }
        };
        if (!navigator) {
            map.mapNavigator.fit({bounds: layerPuntos.bounds, animate: true});
            navigator = true;
        }
    }
    if (layerTracksT.visible) {
        layerTracksT.filter = function (feature) {
            switch(label) {
                case feature.properties.Ajustador: return true;
                case feature.properties.Transporte: return true;
                default: return false;
            }
        };
        if (!navigator) {
            map.mapNavigator.fit({bounds: layerTracksT.bounds, animate: true});
            navigator = true;
        }
    }
}

function detallesTodosAjustadores() {
    var i, j;
    var etiqueta ="<table>", etiquetaFinal ="";
    document.getElementById("detallesAjustador").innerHTML=etiqueta;
    //GraficasEx.crearGraficaIdTexto("graficaAjustadores", tablaDatos, "piechart", "Puntualidad", 3);
    for(i=0;i<nombreAjustador.length;i++){
        etiqueta += '<tr><td><div id="ajustador'+i+'"></div><div id="grafica'+i+'"></div></td></tr>';
        document.getElementById("detallesAjustador").innerHTML=etiqueta;
        crearDetallesAjustador(features, nombreAjustador[i], "ajustador"+i+"");
        etiqueta = document.getElementById("detallesAjustador").innerHTML;
    }
    etiqueta += "</table>";
    document.getElementById("detallesAjustador").innerHTML=etiqueta;
}

function DetallesAjustador(ajustador) {
    console.log("crear detalles de "+ajustador);
    crearDetallesAjustador(features, ajustador, "detallesAjustador");
}


