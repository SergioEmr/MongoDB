define([], function () {
    
    function getBallon(feature) {
        var _etiqueta = //block, flex
                '<div class="labelDatos" style="display:block !important">' +
                '<table>'+
                 '$DATOS' +
                '</table>' +
                '<a onclick=\'MostrarOcultar("panelControl2");\' title="Cerrar" role="button" class="btn btn-default">Detalles Ajustador </a>' +
                '<a onclick=\'MostrarOcultar("panelVideo");\' title="Video" role="button" class="btn btn-default">Video </a>' +
                '</div>';
        var _etiquetaDatos = '<tr >' +
                '<td align="center">' +
                '<b>$Propertie</b>' +
                '</td >' +
                '<td align="center">' +
                '<b>$Value</b>' +
                '</td >' +
                '</tr>';
        var etiqueta="", etiquetaDatos="";
        
        
        //var min = duracion.toFixed(), seg = ((duracion > min? (duracion-min):(min-duracion))*100).toFixed();
        var startTime = feature.properties.startTime;
        var endTime = feature.properties.endTime;
        var x = feature.geometry.x, y = feature.geometry.y;
        var ubicacion;
        var folio = feature.properties.Folio;
        var ajustador = feature.properties.Ajustador;
        var transporte = feature.properties.Transporte;
        var poliza = feature.properties.Poliza;
        var cliente = feature.properties.Cliente;
        var foto = feature.properties.Foto;
        var camara = feature.properties.Camara;
        var tipo = feature.properties.Tipo;
        var situacion = feature.properties.Situacion;
        var tiempo = feature.properties.TiempoTotal;
        
        if(ajustador) 
            etiquetaDatos+= _etiquetaDatos.replace("$Propertie", "Nombre del ajustador").replace("$Value", ajustador);
        
        if(transporte)
            etiquetaDatos+= _etiquetaDatos.replace("$Propertie", "Medio de Transporte del Ajustador:").replace("$Value", transporte);
        
        if(tipo)
            etiquetaDatos+= _etiquetaDatos.replace("$Propertie", "Tipo de seguro:").replace("$Value", tipo);
        
        if(poliza)
            etiquetaDatos+= _etiquetaDatos.replace("$Propertie", "Numero de Poliza:").replace("$Value", poliza);
        
        if(situacion)
            etiquetaDatos+= _etiquetaDatos.replace("$Propertie", "Tipo de siniestro:").replace("$Value", situacion);
        
        if(feature.properties.Velocidad || feature.properties.Velocidad===0)
            etiquetaDatos+= _etiquetaDatos.replace("$Propertie", "Velocidad:").replace("$Value", feature.properties.Velocidad+" Km/H");
        
        if(feature.properties.Direccion || feature.properties.Direccion===0) {
            var dir = feature.properties.Direccion, direccion="";
            if(dir>=45 && dir< 135)
                direccion = "Norte";
            if(dir>=135 && dir <180)
                direccion = "Oeste";
            if(dir>= 180 && dir <225)
                direccion = "Sur";
            if(dir>=225 || dir <45)
                direccion = "Este";
            etiquetaDatos+= _etiquetaDatos.replace("$Propertie", "Direccion:").replace("$Value", direccion);
        }
        if(feature.properties.Estado || feature.properties.Estado === 0) {
            var estado;
            if(feature.properties.Estado ===1)
                estado = "Bueno";
            else
                estado = "Malo";
            etiquetaDatos+= _etiquetaDatos.replace("$Propertie", "Estado de la Antena:").replace("$Value", estado);
        }
        
        if(x) {
            ubicacion = "Lon: "+y+" Lat: "+x;
            etiquetaDatos+= _etiquetaDatos.replace("$Propertie", "Ubicacion del Ajustador").replace("$Value", ubicacion);
        }
        
        if(tiempo)
            etiquetaDatos+= _etiquetaDatos.replace("$Propertie", "Tiempo que se demoro en Resolver el siniestro:").replace("$Value", tiempo);
        
        if(cliente)
            etiquetaDatos+= _etiquetaDatos.replace("$Propertie", "Ver informacion del Cliente:")
                .replace("$Value", '<button class="botonTabla" onclick=\'MostrarOcultar("panelCliente", null);\'>Detalles</button>');
        
        //etiquetaDatos+= _etiquetaDatos.replace("$Propertie", "Hora del Evento").replace("$Value", startTime);
        if(endTime) 
            etiquetaDatos+= _etiquetaDatos.replace("$Propertie", "Hora de Atencion").replace("$Value", endTime);
        
        etiqueta= _etiqueta.replace("$DATOS", etiquetaDatos).replace('$Ajustador', ajustador);
        var balloon = etiqueta;
        
        infoCliente (cliente);
        DetallesAjustador(ajustador);
        return balloon;   
    }
    
    function infoCliente(info) {
        var etiquetaC = "", etiquetaDC = "";
        var _etiquetaC = //block, flex
                '<div class="labelDatos" style="display:block !important">' +
                '<table>'+
                 '$DATOS' +
                '</table>' +
                '</div>';
        var _etiquetaDC = '<tr >' +
                '<td align="center">' +
                '<b>$Propertie</b>' +
                '</td >' +
                '<td align="center">' +
                '<b>$Value</b>' +
                '</td >' +
                '</tr>';
        
        for (var key in info) {
            etiquetaDC+= _etiquetaDC.replace("$Propertie", key+":").replace("$Value", info[key]);
            console.log(' name=' + key + ' value=' + info[key]);
        }
        etiquetaC= _etiquetaC.replace("$DATOS", etiquetaDC);
        document.getElementById("infoCliente").innerHTML=etiquetaC;
    }
    
    return {
        getBallon: getBallon
    };
});