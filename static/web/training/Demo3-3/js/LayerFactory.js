/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define([
  "luciad/geodesy/LineType",
  "luciad/model/codec/GeoJsonCodec",
  "luciad/model/feature/FeatureModel",
  "luciad/model/store/UrlStore",
  "luciad/model/tileset/FusionTileSetModel",
  "luciad/reference/ReferenceProvider",
  "luciad/shape/ShapeFactory",
  "luciad/view/feature/FeatureLayer",
  "luciad/view/feature/TrajectoryPainter",
  "luciad/view/LayerType",
  "luciad/view/tileset/RasterTileSetLayer",
  "luciad/view/kml/KMLLayer",
  "luciad/model/kml/KMLModel",
  "luciad/view/feature/FeaturePainter",
  "luciad/util/ColorMap",
  'samplecommon/IconFactory',
  "luciad/view/style/PointLabelPosition",
  
  "./Shapes",
  "luciad/model/store/MemoryStore",
  "./main",
  "samplecommon/LayerConfigUtil",
], function(LineType, GeoJsonCodec, FeatureModel, UrlStore, FusionTileSetModel, ReferenceProvider, ShapeFactory,
            FeatureLayer, TrajectoryPainter, LayerType, RasterTileSetLayer, KMLLayer, KMLModel, FeaturePainter,
            ColorMap, IconFactory, PointLabelPosition,
            Shapes, MemoryStore, main, LayerConfigUtil) {
    
    
    
    
    function createGlowSlopesLayer(url, dataSetStartTime, dataSetEndTime, glowColor, label) {
      var trajectoriesPainter = new TrajectoryPainter({
        properties: ["origin", "airline", "destination"],
        defaultColor: glowColor,
        selectionColor: glowColor,
        lineWidth: 3,
        lineType: LineType.SHORTEST_DISTANCE,
        timeWindow: [0, dataSetEndTime - dataSetStartTime],
        timeProvider: function(feature, shape, pointIndex) {
          return feature.properties.timestamps[pointIndex];
        },
        draped: true
      });
      //al =0;
      var trajectoryStore;
      if (url ===null) {
            trajectoryStore = new MemoryStore({});
        } else {
            trajectoryStore = new MemoryStore({ data: url });
        }
      var reference = ReferenceProvider.getReference("CRS:84");
      var trajectoryModel = new FeatureModel(trajectoryStore, {reference: reference});
      return new FeatureLayer(trajectoryModel, {
        label: label,
        selectable: true,
        visible: true,
        painter: trajectoriesPainter
      });
    }
    
    function createGlowSlopesLayerData(data, glowColor, label, tiempoActual) {
        var features = null;
        var dataSetStartTime = tiempoActual + 600000;
        var dataSetEndTime = dataSetStartTime + 12*60*60*1000;
        var layer;
        if(data!== null) {
            features = crearFeatureTime(data, ReferenceProvider.getReference("CRS:84"), 
                dataSetStartTime);
        }
        layer = createGlowSlopesLayer(features, dataSetStartTime,dataSetEndTime, glowColor, label);
        //map.layerTree.addChild(layer);
                    //console.log(features);
        return layer;      
        //setTrayectoryGlowLayer(layer, features);
        
    }
    
    function getRandom(min, max) {
        return Math.random() * (max - min) + min;
    }
    
    function crearFeatureTime (data) {
        var i=0, j, n,m=0,v=getRandom(0,10),x=[],y=[], features,timestamps=[], eventTime=[], velocities =[],
                properties, feature, coordenada, distancia, tiempoAtencion, avance, startTime = getTiempo();
        var sFecha = new Date(startTime).toGMTString(), nFecha = startTime, reference = ReferenceProvider.getReference("CRS:84");
        var sTime, eTime, duration, sDuration;
        
        //for(i=0; i<data.length;i++) {
            feature = data;
            avance =0;
            //if(feature.geometry.type === 'LineString') {
                coordenada = feature.geometry.coordinates;
                n= coordenada.length;
                sTime =startTime ;
                duration = feature.properties.duration*1000;
                sDuration = formatDate(new Date(duration));
                
                eTime = sTime + duration;
                
                tiempoAtencion = duration;
                sFecha = formatDate(new Date(sTime));
                //sFecha = new Date(sTime).toGMTString();
                nFecha = sTime;
                avance = duration/n;
                properties = feature.properties;
                for(j=0; j<n; j++) {
                    if(j<n) {
                        x[j] = coordenada[j][0];
                        y[j] = coordenada[j][1];
                    }
                    eventTime [j]= sFecha;
                    timestamps[j] = nFecha;
                    velocities[j] = v;
                    v = getRandom(5, 15);
                    nFecha += avance;
                    sFecha = formatDate(new Date(nFecha));
                    //sFecha = new Date(nFecha).toGMTString();
                }
                var hours = Math.floor( tiempoAtencion / 3600000 );  
                var minutes = Math.floor( (tiempoAtencion % 3600000) / 60000 );
                var seconds = tiempoAtencion % 60000;
                //console.log(eventTime);
                var propiedades ={
                    Summary: properties.summary,
                    Distancia: properties.distance,
                    Ajustador: properties.Ajustador,
                    Duracion: duration,
                    timestamps: timestamps,
                    EventTime: eventTime,
                    velocities: velocities,
                    endTime: eventTime[eventTime.length -1]
                };
                //properties = properties.menu.concat(feature.properties);
                
                features = Shapes.createPolyline(reference, x, y, 0, i, propiedades);
                eventTime = [];
                timestamps =[];
                velocities =[];
                x=[];
                y=[];
                v=getRandom(0,10);
            
        
        return features;
    }
    
    function formatDate(date) {
        var h = date.getHours() <10 ? "0"+date.getHours(): date.getHours();
        var m = date.getMinutes() <10 ? "0"+date.getMinutes(): date.getMinutes();
        var s = date.getSeconds() <10 ? "0"+date.getSeconds(): date.getSeconds();
        return date.getFullYear()+"/"+(date.getMonth()+1)+"/"+date.getDate()+" "+h+":"+m+":"+s;
    }
    
    function crearJSONLayer(url, reference, label)
    {
        var URL = new UrlStore({target: url});
        var modelo = new FeatureModel(URL, {reference: reference});
        var painter = new FeaturePainter();
        
        painter.paintBody = function(geocanvas, feature, shape, layer, map, paintState)
        {
            var color = "rgba(230,230,230, 0.5)", colorS = "rgba(50,230,50,0.5)";
            var colorL = "rgb(230,230,230)", colorLS = "rgb(50,230,50)";
            geocanvas.drawShape(shape,{ 
                fill: {color: paintState.selected ? colorS : color},
                stroke: {
                    color: paintState.selected ? colorLS : colorL,
                    width: 1} 
            });
            };
        painter.paintLabel = function(labelCanvas, feature, shape, layer, map, paintState)
        {
            var label = "<span style='color: $color'>" + feature.id + "</span>";
            if(paintState.selected)
                labelCanvas.drawLabel(label.replace("$color", "rgb(200,0,200"),shape.focusPoint, {});
            else
                labelCanvas.drawLabel(label.replace("$color", "rgb(252,255,255"),shape.focusPoint, {});
        };
        
        //dLang.mixin(layerOptions, {painter: painter});
        return new FeatureLayer(modelo, {
            id: label,
            label: label,
            selectable: true,
            painter: painter}
        );
    }
    
    function onCreateContextMenu(contextMenu, map, contextMenuInfo) {
      var selectedObjects = getLayerSelectedFeatures(map, contextMenuInfo.layer);
      LayerConfigUtil.createContextMenu(contextMenu, map, contextMenuInfo);
      
    }
    
    function createLayer (url, reference, options) {
        var URL = new UrlStore({target: url});
        var modelo = new FeatureModel(URL, {reference: reference});
        var painter = new FeaturePainter();
        painter.paintBody = getPainter();
        painter.paintLabel = function(labelCanvas, feature, shape, layer, map, paintState)
        {
            var label = "<span style='color: $color'>" + feature.id + "</span>";
            if(paintState.selected)
                labelCanvas.drawLabel(label.replace("$color", "rgb(200,0,200"),shape.focusPoint, {});
            else
                labelCanvas.drawLabel(label.replace("$color", "rgb(252,255,255"),shape.focusPoint, {});
        };
        if(!options.painter)
            options.painter = painter;
        
        return new FeatureLayer(modelo, options);
    }
    function getPainter() {
        return function (geocanvas, feature, shape, layer, map, paintState) {
            var color1 = "rgba(30,30,230, 0.5)", color2 = "rgba(230,30,30, 0.5)", colorS = "rgba(70, 100, 230, 0.5)";
            var colorL = "rgb(230,230,230)", colorLS = "rgb(50,230,50)";
            var icon,color, dimension= 20;
            
            if(paintState.selected) {
                icon = IconFactory.circle({stroke: colorLS, fill: colorS, width: dimension, height: dimension});
            }
            else {
                icon  = IconFactory.circle({stroke: colorL, fill: "rgba(200,0,200, 0.5)", width: dimension, height: dimension});
            }
            geocanvas.drawIcon(shape.focusPoint,{
                width: "20px",
                height: "20px",
                image: icon
            });
        };
    }
    
    return {
        createGlowSlopesLayer: createGlowSlopesLayer,
        crearJSONLayer: crearJSONLayer,
        createGlowSlopesLayerData: createGlowSlopesLayerData,
        onCreateContextMenu: onCreateContextMenu,
        createLayer: createLayer,
        getPainter: getPainter,
        crearFeatureTime: crearFeatureTime
    };
});

