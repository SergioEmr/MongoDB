/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define([
    "luciad/shape/ShapeFactory",
    "luciad/shape/ShapeType",
    "luciad/model/feature/Feature"
], function (ShapeFactory, ShapeType, Feature) {
    
    function createPolygonRandom (reference, rangoX, rangoY, rangoZ, n) {
        var x=[], y=[], z=[], poligonos = [], puntas;
        if(rangoZ === 0 || rangoZ === "undefined") {
            for(var j=0;j<x.length;j++)
                z[j] =0;
        }
        for(var i=0;i<n;i++) {
            puntas = getRandom(4, 11);
            for(var j=0;j<puntas;j++) {
                x[j] = getRandom(rangoX.min, rangoX.max);
                y[j] = getRandom(rangoY.min, rangoY.max);
            }
            poligonos[i] = createPolygon(reference, x, y, z, i, {});
        }
        return poligonos;
    }
    
    function createPointRandom(reference, rangoX, rangoY, rangoZ, n, startDate, endDate) {
        var x, y, z, k,a, poligonos = [], properties, fechas=startDate, fechaS, avance =0;
        if(rangoZ === 0 || rangoZ === "undefined") {
            z =0;
        }
        if(n>30)
            a=30;
        else
            a=n;
        fechaS = new Date(fechas*1000).toGMTString();//.toUTCString();
        avance = (endDate - startDate)/a;
        //console.log(fechaS + "\n"+ fechas+"\n"+avance);
        k=0;
        
        for(var j=0;j<a;j++) {
            for(var i=0;i<n;i++) {
                x = getRandom(rangoX.min, rangoX.max);
                y = getRandom(rangoY.min, rangoY.max);
                properties={EventTime: fechaS};
                poligonos[k] = createPoint(reference, x, y, z, k, properties);
                k++;
            }
            fechas+=avance;
            fechaS = new Date(fechas*1000).toGMTString();
        }
        return poligonos;
    }
    
    function getRandom(min, max) {
        return Math.random() * (max - min) + min;
    }
    
    function movePolygon(polygon, direction) {
        
    }
    
    function createPolygon(reference, x, y, z, i, properties) {
        if(z === 0) {
            z= [];
            for(var j=0;j<x.length;j++)
                z[j] =0;
        }
        if(properties === "undefined" || properties === undefined) {
            properties = {};
        }
        return new Feature(
            ShapeFactory.createPolygon(reference, multiPoints(reference, x,y,z)), properties, i
        );
    }
    
    function createCircle(reference, x, y, z, r, i, properties) {
        if(z === 0) {
            z= [];
            for(var j=0;j<x.length;j++)
                z[j] =0;
        }
        if(properties === "undefined" || properties === undefined) {
            properties = {};
        }
        return new Feature(
            ShapeFactory.createCircleByCenterPoint(reference, ShapeFactory.createPoint(reference,[x,y]), r), properties, i
        );
    }
    
    function createPolyline(reference, x, y, z, i, properties) {
        if(z === 0 || z === undefined) {
            z= [];
            for(var j=0;j<x.length;j++)
                z[j] =0;
        }
        if(properties === "undefined" || properties === undefined) {
            properties = {};
        }
        return new Feature(
            ShapeFactory.createPolyline(reference, multiPoints(reference, x,y,z)), properties, i
        );
    }
    
    function multiPoints(reference, x, y, z) {
        var points = [];
        for(var i=0; i<x.length;i++) {
            points[i] = ShapeFactory.createPoint(reference, [x[i], y[i], z[i]]);
        }
        return points;
    }
    
    function createPoint(reference, x, y, z, i, properties) {
        if(properties === undefined)
            properties = {};
        return new Feature(
            ShapeFactory.createPoint(reference, [x, y, z]), properties, i
        );
    }
    
    function crearFeatureTime (data, reference, startTime, endTime) {
        var i, j, n,m=0,v=3,x=[],y=[], features = [],timestamps=[], eventTime=[], velocities =[],
                properties, feature, coordenada, distancia, tiempoAtencion, avance;
        var sFecha = new Date(startTime).toGMTString(), nFecha = startTime;
        var sTime, eTime;
        
        for(i=0; i<data.length;i++) {
            feature = data[i];
            avance =0;
            if(feature.geometry.type === 'LineString') {
                //avance = 1200000;
                coordenada = feature.geometry.coordinates;
                n= coordenada.length;
                if(feature.properties.startTime ) 
                    sTime = Date.parse(feature.properties.startTime);
                else 
                    sTime =startTime;
                if(feature.properties.endTime)
                    eTime = Date.parse(feature.properties.endTime);
                else {
                    eTime = sTime + (120000 * n);
                }
                tiempoAtencion = eTime - sTime;
                sFecha = formatDate(new Date(sTime));
                //sFecha = new Date(sTime).toGMTString();
                nFecha = sTime;
                avance = (eTime - sTime)/n;
                properties = feature.properties;
                for(j=0; j<n; j++) {
                    if(j<n) {
                        x[j] = coordenada[j][0];
                        y[j] = coordenada[j][1];
                    }
                    eventTime [j]= sFecha;
                    timestamps[j] = nFecha;
                    velocities[j] = v;
                    v = getRandom(5, 15);
                    nFecha += avance;
                    sFecha = formatDate(new Date(nFecha));
                    //sFecha = new Date(nFecha).toGMTString();
                }
                var hours = Math.floor( tiempoAtencion / 3600000 );  
                var minutes = Math.floor( (tiempoAtencion % 3600000) / 60000 );
                var seconds = tiempoAtencion % 60000;
                //console.log(eventTime);
                var propiedades = Object.assign({
                    timestamps: timestamps,
                    EventTime: eventTime,
                    TiempoTotal: hours+" hr. "+minutes+" min. "+seconds+" seg.",
                    velocities: velocities,
                    //startTime: sTime,
                    endTime: eventTime[eventTime.length -1]
                }, properties);
                //properties = properties.menu.concat(feature.properties);
                
                features[i] = createPolyline(reference, x, y, 0, i, propiedades);
                eventTime = [];
                timestamps =[];
                velocities =[];
                x=[];
                y=[];
                v=getRandom(0,10);
            }
        }
        return features;
    }
    function formatDate(date) {
        var h = date.getHours() <10 ? "0"+date.getHours(): date.getHours();
        var m = date.getMinutes() <10 ? "0"+date.getMinutes(): date.getMinutes();
        var s = date.getSeconds() <10 ? "0"+date.getSeconds(): date.getSeconds();
        return date.getFullYear()+"/"+(date.getMonth()+1)+"/"+date.getDate()+" "+h+":"+m+":"+s;
    }
    
    function distancia(x1, y1, x2, y2 ) {
        return Math.sqrt(Math.pow(x2-x1,2) + Math.pow(y2-y1,2));
    }
    
    function crearRuta(reference, data, ids) {
        var i,j, k, x=[],y=[];
        var route, coordenadas, properties, features = [];
        console.log("creando ruta");
        if(data.routes) {
            for(i=0; i<data.routes.length;i++) {
                route = data.routes[i];
                for(j=0;j<route.length;j++) {
                    coordenadas = route[j].geometry.coordinates;
                    for(k=0;k<coordenadas.length;k++){ 
                        x[k] = coordenadas[k][0];
                        y[k] = coordenadas[k][1];
                    }
                    properties = {
                        cost: route[j].cost,
                        distance: route[j].distance,
                        duration: route[j].duration,
                        summary: route[j].summary
                    };
                }
                
                features[i] = createPolyline(reference, x, y, 0, i, properties);
                ids++;
                properties = {};
                x=[];
                y=[];
                route=[];
            }
        }
        return features;
    }
    
    function crearJSON(reference, data, ids) {
        var i,j, k, x=[],y=[];
        var route, coordenadas, properties, features = '{"type":"FeatureCollection","features":[\n{';
        console.log("creando json");
        features+='{ "type":"Feature","id":"1", "properties": {'+
		'"name":"ruta"},'+
                '"geometry":{"type": "LineString", "coordinates":';
        if(data.routes) {
            for(i=0; i<data.routes.length;i++) {
                route = data.routes[i];
                for(j=0;j<route.length;j++) {
                    coordenadas = route[j].geometry.coordinates;
                    features+= coordenadas+"";
                    console.log(features);
                    for(k=0;k<coordenadas.length;k++){ 
                        x[k] = coordenadas[k][0];
                        y[k] = coordenadas[k][1];
                    }
                    properties = {
                        cost: route[j].cost,
                        distance: route[j].distance,
                        duration: route[j].duration,
                        summary: route[j].summary
                    };
                }
                
                features[i] = createPolyline(reference, x, y, 0, i, properties);
                ids++;
                properties = {};
                x=[];
                y=[];
                route=[];
            }
        }
        return features;
    }
    
    return {
        createPoint: createPoint,
        createPolygon: createPolygon,
        createPolygonRandom: createPolygonRandom,
        createPointRandom: createPointRandom,
        crearFeatureTime: crearFeatureTime,
        crearRuta: crearRuta,
        createPolyline: createPolyline,
        createCircle: createCircle,
        crearJSON: crearJSON
    };
});

