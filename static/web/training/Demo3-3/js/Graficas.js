/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define([], function () {
    
    var graficas = new Array(), idGrafica=-1, y=0;
    /*
     * 
     * @param {String} div
     * @param {Array[][]} datos
     * @param {String} tipo
     * @param {String} titulo
     * @returns {unresolved}
     */
    function dibujarGraficaArreglo(div, datos, tipo, titulo) {
        document.getElementById(div).innerHTML="";
        switch(tipo.toLowerCase()) {
            case "piechart":
                PieChart(div, datos, titulo);
                break;
            case "columnchart":
                ColumnChart(div, datos, titulo);
                break;
            case "areachart":
                AreaChart(div, datos);
                break;
            default: console.log("Tipo de Grafica desconocida "+ tipo);return null;
        }
        
    }
    /*
     * 
     * @param {String} div
     * @param {Array[][]} datos
     * @param {String} titulo
     * @returns {undefined}
     */
    function ColumnChart(div, datos, titulo) {
        var chart;
        try{
            //google.charts.load('current', {'packages':['line']});
            google.charts.load('current', {packages: ['corechart', 'bar']});
            google.charts.setOnLoadCallback(drawColumnChart);
            
        }catch(e){
            console.log("Error al crear Grafica de columna\n");
            console.log(e);
        }
        function drawColumnChart() {
            chart = new google.visualization.ColumnChart(document.getElementById(div));
            var datosNuevos = new google.visualization.arrayToDataTable(datos);
            var options= getColumnOptions();
            options.title = titulo;
            chart.draw(datosNuevos, options);
            graficas[y] = chart;
            y++;
        }
    }
    /*
     * 
     * @param {String} div
     * @param {Array[][]} datos
     * @param {String} titulo
     * @returns {unresolved}
     */
    function PieChart(div, datos, titulo){
        var chart;
        try{
            //google.charts.load('current', {'packages':['line']});
            google.charts.load('current', {'packages':['corechart']});
            return google.charts.setOnLoadCallback(drawPieChart);
        }catch(e){
            console.log("Error al crear Grafica de lineas\n");
            console.log(e);
        }
        function drawPieChart() {
            chart = new google.visualization.PieChart(document.getElementById(div));
            var datosNuevos = new google.visualization.arrayToDataTable(datos);
            var options= getPieOptions();
            options.title = titulo;
            chart.draw(datosNuevos, options);
            google.visualization.events.addListener(chart, 'select', function() {
                try{
                var x = chart.getSelection(), row = x[0].row, col = 0;
                var dato1 = datosNuevos.getValue(row, 0);
                
                filtrarTexto(dato1);
            }catch(e) {
                console.log(e);
            }
            });
            graficas[y] = chart;
            y++;
        }
    }
    /*
     * 
     * @param {String} div
     * @param {Array[][]} datos
     * @returns {unresolved}
     */
    function AreaChart(div, datos){
        var chart;
        try{
            //google.charts.load('current', {'packages':['line']});
            google.charts.load('current', {'packages':['corechart']});
            return google.charts.setOnLoadCallback(drawAreaChart);
        }catch(e){
            console.log("Error al crear Grafica de lineas\n");
            console.log(e);
        }
        function drawAreaChart() {
            chart = new google.visualization.AreaChart(document.getElementById(div));
            var datosNuevos = new google.visualization.arrayToDataTable(datos);
            var options= getAreaOptions();
            chart.draw(datosNuevos, options);
            google.visualization.events.addListener(chart, 'select', function() {
                try{
                var x = chart.getSelection(), row = x[0].row, col = 0;
                var dato1 = datosNuevos.getValue(row, 0);
                
                filtrarTexto(dato1);
            }catch(e) {
                console.log(e);
            }
            });
            graficas[y] = chart;
            y++;
        }
    }
    /*
     * 
     * @param {Object} features
     * @returns {Array[][]}
     */
    function crearArreglo(features) {
        var tabla = new Array(), encavezados = new Array(), valores = new Array();
        var key, i=0, j=0, k=0, x, coordenadas;
        for(key in features[0].properties) {
            encavezados[i] = key;
            i++;
        }
        encavezados[i] = "Longitud";
        encavezados[i+1] = "Latitud";
        tabla[j] = encavezados;
        for(i=0; i<features.length; i++) {
            for(key in features[i].properties) {
                try {
                    x = parseFloat(features[i].properties[key]);
                    if(x)
                        valores[k] = x;
                    else
                        valores[k] = features[i].properties[key];
                }catch (e) {
                    
                }
                k++;
            }
            coordenadas = features[i].geometry.coordinates;
            if(coordenadas.length === 3) {
                valores[k] = coordenadas[0];
                valores[k+1] = coordenadas[1];
            } else {
                valores[k] = coordenadas[0].x;
                valores[k+1] = coordenadas[0].y;
            }
            j++;
            tabla[j]=valores;
            valores = new Array();
            k=0;
        }
        return tabla;
    }
    
    /*
     * 
     * @param {String} div
     * @param {Array[][]} tablaDatos
     * @param {String} chartType
     * @param {String} title
     * @param {int} id
     * @returns {Number}
     */
    function crearGraficaIdTexto(div, tablaDatos, chartType, title, id) {
        idGrafica++;
        console.log("Creando Grafica "+title+" id Grafica"+idGrafica);
        var datos = datosChartTexto(tablaDatos, id, title);
        dibujarGraficaArreglo(div, datos, chartType, title);
        return idGrafica;
    }
    /*
     * 
     * @param {String} div
     * @param {Array[][]} tablaDatos
     * @param {String} chartType
     * @param {String} title
     * @param {int} idLabel
     * @param {int} idValor
     * @returns {Number}
     */
    function crearGraficaLabelValor(div, tablaDatos, chartType, title, idLabel, idValor) {
        idGrafica++;
        console.log("Creando Grafica "+title+" id Grafica"+idGrafica);
        var datos = datosChartLabelValor(tablaDatos, idLabel, idValor, title);
        dibujarGraficaArreglo(div, datos, chartType, title);
        return idGrafica;
    }
    /*
     * 
     * @param {Array[][]} tablaDatos
     * @param {int} id
     * @param {String} label
     * @returns {Array}
     */
    function datosChartTexto(tablaDatos, id, label) {
        var cultivos = [[label, "number"]];
        var tipoDato = new Array(), numeroDatos = new Array(), cultivo, nuevoDato=false;
        var i=0, j=0;
        
        for(i=1; i<tablaDatos.length; i++) {
            
            cultivo = tablaDatos[i][id];
            if(tipoDato.length===0) {
                tipoDato[0] = cultivo;
                numeroDatos[0] = 1;
            }
            else {
                for(j=0;j<tipoDato.length;j++) {
                    if(cultivo === tipoDato[j]) {
                        numeroDatos[j]++;
                        nuevoDato = false;
                        break;
                    }
                    else
                        nuevoDato =true;
                }
                if(nuevoDato){
                    tipoDato[j] = cultivo;
                    numeroDatos[j] = 1;
                }
            }
        }
        
        for(i=0; i<tipoDato.length; i++) {
            cultivos[i+1] = [tipoDato[i], numeroDatos[i]];
        }
        return cultivos;
    }
    
    /*
     * Se encarga de preparar los datos y enviarlos a la funcion para crear la grafica.
     * @param {int} idGrafica
     * @param {Array[][]} tablaDatos
     * @param {String} typeChart
     * @param {String} title
     * @param {int} id
     * @returns {undefined}
     */
    function actualizarGraficaIdTexto(idGrafica, tablaDatos, typeChart, title, id) {
        var datos = datosChartTexto(tablaDatos, id, title);
        updateChart(graficas[idGrafica], datos, typeChart);
    }
    /*
     * 
     * @param {int} idGrafica
     * @param {Array[][]} tablaDatos
     * @param {String} typeChart
     * @param {String} title
     * @param {int} idLabel
     * @param {int} idValor
     * @returns {undefined}
     */
    function actualizarGraficaLabelValor(idGrafica, tablaDatos, typeChart, title, idLabel, idValor) {
        var datos = datosChartLabelValor(tablaDatos, idLabel, idValor, title);
        updateChart(graficas[idGrafica], datos, typeChart);
    }
    /*
     * 
     * @param {Object} chart
     * @param {Array[][]} newData
     * @param {String} chartType
     * @returns {undefined}
     */
    function updateChart(chart, newData, chartType) {
        var newOptions;
        switch(chartType) {
            case "piechart": 
                newOptions = getPieOptions();
                break;
            case "columnchart":
                newOptions = getColumnOptions();
                break;
            case "areachart":
                newOptions = getAreaOptions();
                break;
        }
        var table = new google.visualization.arrayToDataTable(newData);
        chart.draw(table, newOptions);
    }
    
    /*
     * Se encarga de leer los datos de la columna dada por id de la tabla tablaDatos. 
     * Y regresar un Arreglo de datos listos para crear la grafica.
     * @param {Array[][]} tablaDatos
     * @param {int} id
     * @param {String} label
     * @returns {Array}
     */
    function datosChart(tablaDatos, id, label) {
        var datos = [[label, "number"]], dato;
        var tipoDato = new Array(), numeroDatos = new Array(), nuevoDato=false;
        var i, j;
        
        for(i=1; i<tablaDatos.length; i++) {
            dato = tablaDatos[i][id];
            if(tipoDato.length===0) {
                tipoDato[0] = dato;
                numeroDatos[0] = 1;
            }
            else {
                for(j=0;j<tipoDato.length;j++) {
                    if(dato === tipoDato[j]) {
                        numeroDatos[j]++;
                        nuevoDato = false;
                        break;
                    }
                    else
                        nuevoDato =true;
                }
                if(nuevoDato){
                    tipoDato[j] = dato;
                    numeroDatos[j] = 1;
                }
            }
        }
        
        for(i=0; i<tipoDato.length; i++) {
            datos[i+1] = [tipoDato[i], numeroDatos[i]];
        }
        return datos;
    }
    /*
     * 
     * @param {Array[][]} tablaDatos
     * @param {int} idLabel
     * @param {int} idValor
     * @param {String} label
     * @returns {Array}
     */
    function datosChartLabelValor(tablaDatos, idLabel, idValor, label) {
        var datos = [[label, "number"]];
        var tipoDato = new Array(), numeroDatos = new Array(), nombre, valor, nuevoDato=false;
        var i=0, j=0;
        
        for(i=1; i<tablaDatos.length; i++) {
            nombre = tablaDatos[i][idLabel];
            valor = tablaDatos[i][idValor];
            if(tipoDato.length===0) {
                tipoDato[0] = nombre;
                numeroDatos[0] = valor;
            }
            else {
                for(j=0;j<tipoDato.length;j++) {
                    if(nombre === tipoDato[j]) {
                        numeroDatos[j]+= parseFloat(valor);
                        nuevoDato = false;
                        break;
                    }
                    else
                        nuevoDato =true;
                }
                if(nuevoDato){
                    tipoDato[j] = nombre;
                    numeroDatos[j] = valor;
                }
            }
        }
        
        for(i=0; i<tipoDato.length; i++) {
            datos[i+1] = [tipoDato[i], numeroDatos[i]];
        }
        return datos;
    }
    function datosPieChartUbicacion(tablaDatos, id, label, geometry) {
        var cultivos = [[label, "number"]];
        var tipoDato = new Array(), numeroDatos = new Array(), cultivo, nuevoDato=false;
        var lon, lat;
        var i=0, j=0;
        var x = geometry.center.x, y = geometry.center.y;
        var h = geometry.bounds.height, w = geometry.bounds.width;
        var r = h/2;
        var distancia;
        
        for(i=1; i<tablaDatos.length; i++) {
            
            cultivo = tablaDatos[i][id];
            lon = tablaDatos[i][16];
            lat = tablaDatos[i][17];
            distancia = Math.sqrt(Math.pow(x-lon,2) + Math.pow(y-lat,2));
            if(distancia <= r) {
            //if(lon >= minX && lon <= maxX && lat >= minY && lat <= maxY) {
            if(tipoDato.length===0) {
                tipoDato[0] = cultivo;
                numeroDatos[0] = 1;
            }
            else {
                for(j=0;j<tipoDato.length;j++) {
                    if(cultivo === tipoDato[j]) {
                        numeroDatos[j]++;
                        nuevoDato = false;
                        break;
                    }
                    else
                        nuevoDato =true;
                }
                if(nuevoDato){
                    tipoDato[j] = cultivo;
                    numeroDatos[j] = 1;
                }
            }
            }
        }
        
        for(i=0; i<tipoDato.length; i++) {
            cultivos[i+1] = [tipoDato[i], numeroDatos[i]];
        }
        return cultivos;
    }
    
    
    
    
    function datosPieChartLabelValorUbicacion(tablaDatos, idLabel, idValor, label, geometry) {
        var datos = [[label, "number"]];
        var tipoDato = new Array(), numeroDatos = new Array(), nombre, nuevoDato=false, valor;
        var lon, lat;
        var i=0, j=0;
        var x = geometry.center.x, y = geometry.center.y;
        var h = geometry.bounds.height, w = geometry.bounds.width;
        var r = h/2;
        var maxX = x + r, minX = x - r;
        var maxY = y +r, minY = y - r;
        
        for(i=1; i<tablaDatos.length; i++) {
            
            nombre = tablaDatos[i][idLabel];
            valor = tablaDatos[i][idValor];
            lon = tablaDatos[i][16];
            lat = tablaDatos[i][17];
            if(lon >= minX && lon <= maxX && lat >= minY && lat <= maxY) {
            if(tipoDato.length===0) {
                tipoDato[0] = nombre;
                numeroDatos[0] = valor;
            }
            else {
                for(j=0;j<tipoDato.length;j++) {
                    if(nombre === tipoDato[j]) {
                        numeroDatos[j]+= parseFloat(valor);;
                        nuevoDato = false;
                        break;
                    }
                    else
                        nuevoDato =true;
                }
                if(nuevoDato){
                    tipoDato[j] = nombre;
                    numeroDatos[j] = valor;
                }
            }
            }
        }
        
        for(i=0; i<tipoDato.length; i++) {
            datos[i+1] = [tipoDato[i], numeroDatos[i]];
        }
        return datos;
    }
    
    function getPieOptions() {
        return {
        width: 300,
        height: 220,
        titleTextStyle: {
            color: '#FFFFFF',
            frontSize: 20
          },
        legend: {
            position: "left",
          textStyle: {
            color: '#FFFFFF'
          }
        },
        backgroundColor:{
            fill: "#17202a"
        },
        chartArea: {
            left: 20,
            width: 230,
            height: 180,
            backgroundColor: "#17202a"
        }
      };
    }
    function getColumnOptions() {
        return {
        width: 300,
        height: 220,
        orientation: "vertical",
        titleTextStyle: {
            color: '#FFFFFF',
            frontSize: 20
          },
        legend: {
            position: "bottom",
          textStyle: {
            color: '#FFFFFF'
          }
        },
        backgroundColor:{
            fill: "#17202a"
        },
        chartArea: {
            left: 100,
            width: 150,
            height: 230,
            backgroundColor: "#17202a"
        },
        annotations: {
          textStyle: {
            fontSize: 12,
            color: '#FFFFFF',
            auraColor: 'none'
          }
        },
        //axisTitlesPosition: "out",
        hAxis: {
          logScale: false,
          slantedText: false,
          format: 'short',
          textStyle: {
            color: '#FFFFFF',
            frontSize: 10
          },
          titleTextStyle: {
            color: '#FFFFFF',
            frontSize: 16
          }
        },
        vAxis: {
          title: 'Presupuesto',
          
          direction: 1,
          textStyle: {
            color: '#FFFFFF'
          }
          
        }
      };
    }
    function getAreaOptions () {
        return {
        width: 370,
        height: 320,
        titleTextStyle: {
            color: '#FFFFFF',
            frontSize: 20
          },
        legend: {
            position: "bottom",
          textStyle: {
            color: '#FFFFFF'
          }
        },
        backgroundColor:{
            fill: "#17202a"
        },
        chartArea: {
            right: 10,
            width: 300,
            height: 280,
            backgroundColor: "#17202a"
        }
        };
    }
    
    return {
        dibujarGraficaArreglo: dibujarGraficaArreglo,
        updateChart: updateChart,
        crearArreglo: crearArreglo,
        crearGraficaIdTexto: crearGraficaIdTexto,
        actualizarGraficaIdTexto: actualizarGraficaIdTexto,
        crearGraficaLabelValor: crearGraficaLabelValor,
        actualizarGraficaLabelValor: actualizarGraficaLabelValor
    };
    
});
