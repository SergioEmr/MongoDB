





(function ($) {
    "use strict";
    
    
    var formLogin = document.getElementById( "formLogin");
   
    
    var action = "";
    
    var selectDemoNode = document.getElementById( "selectDemo" );
    selectDemoNode.addEventListener( "change", function () {
        var value = selectDemoNode.selectedOptions[0].value;
         switch(value) {
             default: break;
             case "0": 
                 console.log("No se Selecciono un demo");
                 alert("No se selecciono ningun Demo");
                 action = "../login_olly/index.html";
                 break;
             case "1": 
                 action = "../Canacar2/index.html";
                 break;
             case "2":
                 action = "../C4SanPedro/index.html";
                 break;
             case "3":
                 action = "../Saltillo/index.html";
                 break;
             case "4": 
                 action = "../Canacar/indexn.html";
                 break;
             case "5":
                 action = "../C4Escobedo/index.html";
         }
          
         formLogin.action = action;
         console.log("action: "+formLogin.action);
    });


    /*==================================================================
    [ Focus input ]*/
    $('.input100').each(function(){
        $(this).on('blur', function(){
            if($(this).val().trim() !== "") {
                $(this).addClass('has-val');
            }
            else {
                $(this).removeClass('has-val');
            }
        }) ;   
    });
  
  
    /*==================================================================
    [ Validate ]*/
    var input = $('.validate-input .input100');

    $('.validate-form').on('submit',function(){
        var check = true;

        for(var i=0; i<input.length; i++) {
            var val = validate(input[i]);
            if(val === false){
                showValidate(input[i]);
                check=false;
            }
        }

        return check;
    });


    $('.validate-form .input100').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });

    function validate (input) {
        if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
            if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) === null) {
                return false;
            }
        }
        else {
            var t = $(input).val().trim();
            if(t !== 'admin'){
                return false;
            }
        }
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('alert-validate');
    }
    
    /*==================================================================
    [ Show pass ]*/
    var showPass = 0;
    $('.btn-show-pass').on('click', function(){
        if(showPass == 0) {
            $(this).next('input').attr('type','text');
            $(this).addClass('active');
            showPass = 1;
        }
        else {
            $(this).next('input').attr('type','password');
            $(this).removeClass('active');
            showPass = 0;
        }
        
    });
    

})(jQuery);