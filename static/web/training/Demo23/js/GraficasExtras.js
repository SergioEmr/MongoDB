/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define(["./GoogleCharts"], function (Graficas) {
    function graficaFrecuencia(div) {
        var datos = getNumerosA();
        document.getElementById(div).innerHTML="";
        var etiqueta = "<h1>Numeros mas Frecuentes</h1><div id='panelC'></div>";
        document.getElementById(div).innerHTML=etiqueta;
        var data=[], i, d1=buscarMasFrecuentes(datos).valores, d2= buscarMasFrecuentes(datos).frecuencias;
        data[0] = ["String", "Veces llamadas", {type: 'string', role: 'annotation'}];
        for(i=0;i<10;i++) {
            data[i+1] = ["Tel "+d1[i], d2[i], d2[i]+" veces"];
        }
        Graficas.ColumnChart("panelC", data, 1);
    }
    function getNumerosA () {
        return [6251058248,6251058248,6255943357,6255943357,6255943357,6255943357,6255943357,6255943357,6593937332,6593937332,6255943357,6255943357,6251217936,6251217936,6255943357,6255943357,6593937332,6593937332,6251058582,6255943357,6255943357,6255943357,6255943357,6251217936,6251217936,6251058582,6251058582,6255943357,6255943357,6251255288,6251255288,6251255288,6251255288,6251255288,6251255288,6255943357,6255943357,6255943357,6255943357,6255943357,6255943357,6251058582,6255943357,6255943357,6255943357,6255943357,6255943357,6255943357,6255943357,6255943357,6251068351,6251068351,6251058582,6251058582,6251201857,6251058582,6251201857,6251058582,6251058582,6251153051,6251058582,6251058582,6251058582,6251058582,6251058582,6251058582,6251058582,6251058582,6251058582,6251201857,6251100178,6251100178,6251058582,6251058582,6251201857,6251041619,6251041619,6251058582,6251201857,6255943357,6255943357,6255943357,6255943357,6255943357,6255943357,6251058582,6255943357,6255943357,6255943357,6255943357,6251058582,6251068351,6251068351,6251201857,6251058582,6251058582,6251058582,6251058582,6251201857,6251058582,6255943357,6255943357,6251201857,6251100178,6251100178,6251058582,6251201857,6255943357,6255943357,6251058582,6251201857,6251058582,6251201857,6251058582,6251201857,6251201857,6251201857,6255943357,6255943357,6255943357,6255943357,6251184160,6251184160,6251058582,6251058582,6251058582,6251058582,6251058582,6251058582,6251344910,6251344910,6251058582,6251058582,6251058582,6251058582,6251058582,6251058582,6258374950,6258374950,6255943357,6255943357,6251058582,6251058582,6255943357,6255943357,6251058582,6251058582,6251344910,6251344910,6251058582,6251058582,6251058582,6251058582,6251344910,6251344910,6255943357,6255943357,6251217936,6251217936,6255943357,6255943357,6255943357,6255943357,6255943357,6255943357,6251035201,6251004850,6251004850,6251058582,6251035201,6251058582,6251035201,6251058582,6251035201,6251035201,6251058582,6251058582,6251058582,6258374950,6251035201,6258374950,6251058582,6251035201,6251035201,6251058582,6251035201,6251058582,6251058582,6251058582,6593937332,6593937332,6251035201,6251058582,6251035201,6251058582,6251058582,6251058582,6251058582,6251035201,6251100178,6251100178,6251152906,6251152906,6251041619,6251041619,6251217936,6251217936,6593937332,6593937332,6255943357,6255943357,6255943357,6255943357,6593937332,6593937332,6255943357,6255943357,6251058582,6251058582,6593937332,6593937332,6593937332,6593937332,6255943357,6255943357,6593937332,6593937332,6593937332,6593937332,6255943357,6255943357];
    }
    
    function buscarMasFrecuentes(datos) {
        var i, j=0, r=[],r1=0, diferentes =[];
        diferentes[0] = datos[0];
        for(i=0;i<datos.length;i++) {
            r1=0;
            for(j=0;j<diferentes.length;j++) {
                if(datos[i] === diferentes[j])
                    r1++;
            }
            if(r1===0) {
                diferentes[j] = datos[i];
            }
        }
        for(i=0;i<diferentes.length;i++) {
            r[i] = -1;
            for(j=0;j<datos.length;j++) {
                if(diferentes[i] === datos[j]) {
                    r[i]++;
                }
            }
        }
        var d = ordenarMayorMenor(r);
        diferentes = ordenarArreglo(diferentes, d.nuevoOrden);
        return {
            frecuencias: d.arregloOrdenado,
            valores: diferentes
        };
    }
    function ordenarMayorMenor(datos) {
        var i, j, dato, nuevoOrden= [];
        for(i=0;i<datos.length; i++) {
            nuevoOrden[i] = i;
        }
        for(i=1;i<datos.length; i++) {
            if(datos[i-1]<datos[i]) {
                dato = datos[i];
                datos[i]=datos[i-1];
                datos[i-1]= dato;
                
                j=nuevoOrden[i];
                nuevoOrden[i] = nuevoOrden[i-1];
                nuevoOrden[i-1] = j;
                i=0;
            }
        }
        return {
            nuevoOrden:nuevoOrden,
            arregloOrdenado: datos
        };
    }
    function ordenarArreglo(datos, nuevoOrden) {
        var i, j, nuevoArreglo = [];
        if(datos.length === nuevoOrden.length) {
            for(i=0; i<datos.length;i++) {
                j = nuevoOrden[i];
                nuevoArreglo[i] = datos[j];
            }
        }
        return nuevoArreglo;
    }
    
    return {
        graficaFrecuencia: graficaFrecuencia
    };
});
