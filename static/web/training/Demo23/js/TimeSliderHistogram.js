/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define([
    "recursos/js/TimeSlider",
    "recursos/js/Histogram",
    "recursos/js/LayerFactory",
    "luciad/transformation/TransformationFactory",
    "luciad/view/feature/ShapeProvider",
    "luciad/shape/ShapeFactory",
    "luciad/view/feature/FeatureLayer",
], function (TimeSlider, Histogram, LayerFactory, TransformationFactory, ShapeProvider,
    ShapeFactory, FeatureLayer) {
    
    function createTimeSliderHistogram(divTimeSlider, options) {
        if(!options.startTime || !options.endTime) {
            alert("Se necesita un rango de tiempo valido");
            return null;
        }
        var timeSlider =  new TimeSlider(divTimeSlider);
        timeSlider.setValidRange(options.startTime, options.endTime, options.botom|0, options.top|1000);
        var histogram = new Histogram(TimeSlider.REFERENCE, options.startTime, options.endTime, 12 * 12, 800);
        //var histogramLayer = 
        return {
            timeSlider: timeSlider,
            histogram: histogram
        };
    }
    function createHistorgamLayer(histogram) {
        return LayerFactory.createDefaultHistogramLayer(histogram);
    }
    function getEventTime(feature) {
        var time = feature.properties.EventTimeAsDate;
        if (!time) {  // Cache parsed times
            time = feature.properties.EventTimeAsDate = Date.parse(feature.properties.EventTime) / 1000;
        }
        return time;
    }
    /*
     * 
     * @param {type} model
     * @returns {FeatureLayer}
     */
    function crearLayerTimeShapes(model, painter) {
        var shapeProvider = new ShapeProvider();
        shapeProvider.reference = TimeSlider.REFERENCE;
        shapeProvider.provideShape = function(feature) {
            return ShapeFactory.createPoint(TimeSlider.REFERENCE, [getEventTime(feature), 500]);
        };
        var layer = new FeatureLayer(model, {
            selectable: true, 
            shapeProvider: shapeProvider, 
            painter: painter});
        return layer;
    }
    /*
     * 
     * @param {type} map
     * @param {type} layer
     * @param {type} mapTime
     * @param {type} layerTime
     * @returns {undefined}
     */
    function syncVisibleRanges(map, layer, mapTime, layerTime, histogram) {
        // Visible spatial area of map is used to filter time
        map.on("MapChange", function() {
            var bounds = map.mapBounds;
            var transformation = TransformationFactory.createTransformation(map.reference, layer.model.reference);
            bounds = transformation.transformBounds(bounds);
            layerTime.filter = function(feature) {
                return bounds.contains2D(feature.shape);
            };
            updateHistogram(histogram, layer, layerTime);
        });

        // Visible time range is used to filter spatial map
        mapTime.on("MapChange", function() {
            //if(!layerTime.visible)
            //  layerTime.visible = true;
            var bounds = mapTime.mapBounds;
            layer.filter = function(feature) {
                return bounds.contains2D(layerTime.shapeProvider.provideShape(feature));
        };
        });
    }
    // Update the histogram with the visible earthquakes
    function updateHistogram(histogram, layer, layerTime) {
        histogram.updateHistogram(function(accumulate) {
            
            var features = layer.workingSet.get();
            for (var i = 0; i < features.length; i++) {
                if(layerTime.filter) {
                if (layerTime.filter(features[i])) {
                    var time = getEventTime(features[i]);
                    var magnitude = features[i].properties.Magnitude;
                    //accumulate(time, magnitude);
                }
                }
            }
            //console.log(timeSlider.getCurrentTime());
        });
        
    }
    
    function syncSelection(map1, layer1, map2, layer2) {
        function oneWaySync(map1, layer1, map2, layer2) {
            map1.on("SelectionChanged", function (event) {
                for (var i = 0; i < event.selectionChanges.length; i++) {
                    if (event.selectionChanges[i].layer === layer1) {
                        var pickInfo = {
                            layer: layer2,
                            objects: event.selectionChanges[i].selected
                        };
                        map2.selectObjects([pickInfo]);
                    }
                }
            });
        }
        oneWaySync(map1, layer1, map2, layer2);
        oneWaySync(map2, layer2, map1, layer1);
    }
    return {
        histogram: new Histogram,
        createTimeSliderHistogram: createTimeSliderHistogram,
        crearLayerTimeShapes: crearLayerTimeShapes,
        createHistorgamLayer: createHistorgamLayer,
        syncVisibleRanges: syncVisibleRanges,
        updateHistogram: updateHistogram,
        syncSelection: syncSelection
    };
});
