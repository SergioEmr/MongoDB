/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define({
    
}, function () {
    
    
    
    function registrar(url, envio, folio) {
        var http = new XMLHttpRequest();
        var res = '{\n\
  "msg": "Registrado"\n\
}\n\
';
        //var url = baseUrl+"report/registro";
        http.open("POST", url, true);
        //http.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        http.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        http.onreadystatechange = function() {
            console.log("Status "+http.status, " Response "+ http.responseText);
            if(http.readyState === 4 && (http.status === 200 || http.status === 202)) { 
                //if(http.response === res){
                  //if(http.responseText !== "CURP ya asignado" && http.responseText !== "Folio ya creado") {
                alert('Los datos fueron Registrados'); 
               //}
            }
        };
        console.log(envio);
        http.send(JSON.stringify(envio));
    }
    
    
    function registrarDesaparecido(divs, divsSelect) {
        var divsDatos = [], valor;
        for(var i in divs) {
            divsDatos[i] = document.getElementById(divs[i]).value;
        }
        divsDatos[i] = document.getElementById("pCalidad").selectedOptions[0].innerHTML;
        i++;
        for(var j in divsSelect) {
            valor = document.getElementById(divsSelect[j]).selectedOptions[0].value;
            switch(valor) {
                default:
                case "0":
                case "3": divsDatos[i] = "No Señala"; break;
                case "1": divsDatos[i] = "Si"; break;
                case "2": divsDatos[i] = "No"; break;
            }
            i++;
        }
        var json = {
            fecha_1: divsDatos[0],
            status: divsDatos[2],
            
            denunciante: divsDatos[6],
            parentezco: divsDatos[7],
            dir_denunciante: divsDatos[8],
            dir_agraviado: divsDatos[9],
            telefono: divsDatos[10],
            cargo_persona: divsDatos[11],
            
            agraviado: divsDatos[12]+" "+divsDatos[13]+" "+divsDatos[14],
            edad: parseInt(divsDatos[15]),
            sexo: divsDatos[16],
            nacionalidad: divsDatos[17],
            
            autoridades_responsables: divsDatos[18],
            fecha_hechos: divsDatos[21],
            hora_hechos: divsDatos[22],
            entidad: divsDatos[24],
            municipio: divsDatos[23],
            lugar_hechos: divsDatos[25],
            lng: parseFloat(divsDatos[27])||0.0,
            lat: parseFloat(divsDatos[26])||0.0,
            
            num_personas: parseInt(divsDatos[28]),
            sujetos_uniformados: divsDatos[35],
            sujetos_armas: divsDatos[36],
            vehiculo_activo: divsDatos[37],
            descripcion_vehiculo: divsDatos[30],
            activos_delincuencia: divsDatos[39],
            pasivo_delincuencia: divsDatos[40],
            narracion: divsDatos[32]
        };
        //console.log(json);
        //var url = "http://68.183.175.36/sedena/request";
        var url = "https://www.olly.science/sedena/request";
        registrar(url, json);
    }
    
    return {
        registrar: registrar,
        registrarDesaparecido: registrarDesaparecido
    };
});
