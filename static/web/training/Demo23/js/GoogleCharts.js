define([], 
function() {

    var div,  data, tabladatos, tablaColumnas, chart, titulo= "", cualSelect;
    //var boton = document.getElementById("cambiarchat");
    var options;
     
    function PieChart(d, datos, t){
        div = d;
        tabladatos= datos;
        titulo= t;
        try{
            //google.charts.load('current', {'packages':['line']});
            google.charts.load('current', {'packages':['corechart']});
            google.charts.setOnLoadCallback(drawPieChart);
        }catch(e){
            console.log("Error al crear Grafica de lineas\n\n"+e);
        }
    }
    function ColumnChart(d, datos, cs) {
        div = d;
        tabladatos= datos;
        cualSelect = cs;
        try{
            //google.charts.load('current', {'packages':['line']});
            google.charts.load('current', {packages: ['corechart', 'bar']});
            google.charts.setOnLoadCallback(drawColumnChart);
            
        }catch(e){
            console.log("Error al crear Grafica de columna\n\n"+e);
        }
    }
    
    function drawColumnChart() {
        chart = new google.visualization.ColumnChart(document.getElementById(div));
        data = new google.visualization.arrayToDataTable(tabladatos);
        options= getOptions();
        chart.draw(data, options);
        var select;
        if(cualSelect === 1) 
            select = select1;
        else
            select = select2;
            
        google.visualization.events.addListener(chart, 'select', select);
    }
    
    function select1() {
            try{
                var x = chart.getSelection(), row = x[0].row, col = 0;
                var dato1 = data.getValue(row, 0), dato2= data.getValue(row, 1), c ="", texto2 = "";
                for(var i=0; i<dato1.length;i++) {
                    c += dato1.charAt(i);
                    if(isNumeric(c)) {
                        texto2+= c;
                    }
                    c="";
                }
                //var y = parseInt(texto2);
                var y = texto2 + " "+ dato2;
                console.log(y);
                console.log(texto2);
            }catch(e) {}
            //filter(y);
        datosPersonales("datosFicha");
        $("#panelFicha1").fadeIn("slow");
    }
    
    function datosPersonales(div) {
        document.getElementById(div).innerHTML="";
        
        var etiqueta = "";
        var datosPersonales = '<p><p><table>'+
                '<tr >' +
                '<td align="center">' +
                '<b>Nombre</b>' +
                '</td >' +
                '<td align="center">' +
                '<b>Abrahan Reimer</b>' +
                '</td >' +
                '</tr>' +
                '<tr >' +
                '<td align="center">' +
                '<b>Numero Telefono</b>' +
                '</td >' +
                '<td align="center">' +
                '<b>62 51 05 82 48</b>' +
                '</td >' +
                '</tr>' +
                '<tr >' +
                '<td align="center">' +
                '<b>IMEI</b>' +
                '</td >' +
                '<td align="center">' +
                '<b>013119063826859</b>' +
                '</td >' +
                '</tr> </table>'+
                '<div id="Imagenes" class="form-inline"><p>'+
                    '<img src="data/img/credencial.jpg" WIDTH=250 HEIGHT=150>'+
                '</div><div id="res"></div>';
        etiqueta+= datosPersonales;
        document.getElementById(div).innerHTML=etiqueta;
        
        //resumen("res");
        
    }
    
    function select2() {
            try{
                var x = chart.getSelection(), row = x[0].row, col = 0;
                var dato1 = data.getValue(row, 0), dato2= data.getValue(row, 1), c ="", texto2 = "";
                for(var i=0; i<dato1.length;i++) {
                    c += dato1.charAt(i);
                    if(isNumeric(c)) {
                        texto2+= c;
                    }
                    c="";
                }
                //var y = parseInt(texto2);
                var y = parseInt(texto2);
                console.log(y);
                console.log(texto2);
            }catch(e) {}
            filter(y);
    }
    
    function drawPieChart() {
        chart = new google.visualization.PieChart(document.getElementById(div));
        data = new google.visualization.arrayToDataTable(tabladatos);
        options= getPieOptions();
        chart.draw(data, options);
        google.visualization.events.addListener(chart, 'select', function() {
            try {
                var x = chart.getSelection(), row = x[0].row, col = 0;
                filter(data.getValue(row, col));
            }catch(e) {}
        });
    }

    function isNumeric(number) {
        switch(number) {
            case "0": return true;
            case "1": return true;
            case "2": return true;
            case "3": return true;
            case "4": return true;
            case "5": return true;
            case "6": return true;
            case "7": return true;
            case "8": return true;
            case "9": return true;
            default: return false;
        }
    }
    function getPieOptions() {
        return {
        width: 370,
        height: 320,
        title: "titulo",
        titleTextStyle: {
            color: '#FFFFFF',
            frontSize: 20
          },
        legend: {
            position: "left",
          textStyle: {
            color: '#FFFFFF'
          }
        },
        backgroundColor:{
            fill: "#17202a"
        },
        chartArea: {
            left: 20,
            width: 330,
            height: 300,
            backgroundColor: "#17202a"
        }
      };
    }
    function getOptions() {
        return {
        width: 370,
        height: 380,
        orientation: "vertical",
        //title: "titulo",
        titleTextStyle: {
            color: '#FFFFFF',
            frontSize: 20
          },
        legend: {
            //position: "left",
          textStyle: {
            color: '#FFFFFF'
          }
        },
        backgroundColor:{
            fill: "#17202a"
        },
        chartArea: {
            left: 100,
            width: 250,
            height: 350,
            backgroundColor: "#17202a"
        },
        annotations: {
          alwaysOutside: false,
          textStyle: {
            fontSize: 12,
            color: '#FFFFFF',
            auraColor: 'none'
          }
        },
        //axisTitlesPosition: "out",
        hAxis: {
          logScale: false,
          slantedText: false,
          format: 'short',
          textStyle: {
            color: '#FFFFFF',
            frontSize: 10
          },
          titleTextStyle: {
            color: '#FFFFFF',
            frontSize: 16
          }
        },
        vAxis: {
          title: 'Presupuesto',
          
          direction: 1,
          textStyle: {
            color: '#FFFFFF'
          }
          
        }
      };
    }
   
    
    return {
        PieChart:PieChart,
        ColumnChart: ColumnChart
    };
});