/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define(["luciad/view/feature/FeaturePainter",
        "luciad/shape/ShapeFactory",
        'samplecommon/IconFactory',
        'luciad/util/ColorMap',
        "luciad/view/style/PointLabelPosition"
    ], function (FeaturePainter, ShapeFactory, IconFactory, ColorMap, PointLabelPosition) {
    
    function layerPainter() {
    } 
    layerPainter.prototype = new FeaturePainter();
    layerPainter.prototype.constructor = layerPainter;
    layerPainter.prototype.getDetailLevelScales = function() {
        return [1 / 3000000, 1 / 2000000, 1 / 1000000,1 / 500000,1 / 5000];
    };
    
    var sBlanco = 'rgb(255, 255, 255)',     Blanco = 'rgba(255, 255, 255, 0.2)';
    var sGrisOscuro = 'rgb(50, 50, 50)',    GrisOscuro = 'rgba(50, 50, 50, 0.2)';
    var sGrisClaro = 'rgb(200, 200, 200)',  GrisClaro = 'rgba(200, 200, 200, 0.5)';
    var sRojo = 'rgb(255, 0, 0)',           Rojo = 'rgba(255, 0, 0, 0.2)';
    var sAmarilloClaro = 'rgb(255, 255, 204)', AmarilloClaro = 'rgba(255, 255, 204, 0.2)';
    var sGris = 'rgb(130, 130, 130)',       Gris = "rgba(130,130,130, 0.2)";
    var sNaranjaClaro = 'rgb(255, 239, 204)', NaranjaClaro = 'rgba(255, 239, 204, 0.2)';
    var sNaranja = 'rgb(255, 174, 102)',    Naranja = 'rgba(255, 174, 102, 0.2)';
    var Verde = "rgba(50,230,50,0.2)",      sVerde = "rgb(50,230,50)";
    var sMorado ="rgb(200,0,200)",          Morado = "rgba(200,0,200, 0.2)";
    var Azul = "rgba(70, 100, 230, 0.2)",   sAzul = "rgb(70, 100, 230)";
    var VerdeClaro = "rgba( 134, 255, 132 , 0.2)", sVerdeClaro = "rgb( 134, 255, 132 )";
    var maxRadio = 0, minRadio = 1;
    
    layerPainter.prototype.paintBody = function (geoCanvas, feature, shape, layer, map, state) {
        var color = getColor("1");
        try {
            var name = feature.properties.NOMBRE;
            var tabla = getTablaEstados(), maxvalue = 500;
            var actualTime = getTime();
            var estados = tabla.ent_2014; 
            var datosActuales=0;
            var check = feature.properties.cantidad_2013;
            var date =  new Date(actualTime);
            var y = date.getFullYear();
            var key = "cantidad_"+ y;
            if(!check  && estados) {
                var idMunicipio = parseInt(feature.properties.name);
                if(idMunicipio) {
                    //feature.properties.cantidad_2013 = tabla["cantidad_2013"][idMunicipio] || 0;
                    feature.properties.cantidad_2014 = tabla["cantidad_2014"][idMunicipio] || 0;
                    feature.properties.cantidad_2015 = tabla["cantidad_2015"][idMunicipio] || 0;
                    feature.properties.cantidad_2016 = tabla["cantidad_2016"][idMunicipio] || 0;
                    feature.properties.cantidad_2017 = tabla["cantidad_2017"][idMunicipio] || 0;
                    feature.properties.cantidad_2018 = tabla["cantidad_2018"][idMunicipio] || 0;
                    datosActuales = tabla[key][idMunicipio];
                } else {
                    var key2 = key.replace("cantidad", "ent");
                    estados = tabla[key2];
                    for(var i=1; i<estados.length; i++) {
                        var tmunicipio = estados[i];
                        if(name === tmunicipio) {
                            //feature.properties.cantidad_2013 = tabla["cantidad_2013"][i];
                            feature.properties.cantidad_2014 = tabla["cantidad_2014"][i] || 0;
                            feature.properties.cantidad_2015 = tabla["cantidad_2015"][i] || 0;
                            feature.properties.cantidad_2016 = tabla["cantidad_2016"][i] || 0;
                            feature.properties.cantidad_2017 = tabla["cantidad_2017"][i] || 0;
                            feature.properties.cantidad_2018 = tabla["cantidad_2018"][i] || 0;
                            datosActuales = tabla[key][i];
                            break;
                        }
                    }
                    if(i === estados.length)
                        feature.properties[key] = 0;
                }
            } else {
                datosActuales = feature.properties[key];
            }
            var id = datosActuales;
            var porcent = (id/maxvalue) *100;
            if(porcent<25 || !id) {
                color = getColor(1);
            }else {
            if(porcent < 50) {
                color = getColor(2);
            } else {
                if(porcent < 75) {
                    color = getColor(3);
                } else {
                    color = getColor(4);

                }
            }
            }
        }catch(e) {
            console.log(e);
            color = getColor(1);
        }
        
        if(!feature.geometry.x) {
            if(state.level > 3) {
                geoCanvas.drawShape(shape,{ 
                    stroke: {
                        color: state.selected ? sBlanco : color.selected,
                        width: 3} 
                });
            } else {
                geoCanvas.drawShape(shape,{ 
                    fill: {color: state.selected ? color.selected : color.normal},
                    stroke: {
                        color: state.selected ? sBlanco : color.selected,
                        width: 3} 
                });
            }
        
        }
    };
    function getColor(id) {
        switch(id) {
            case 0: return {normal: VerdeClaro, selected: sVerdeClaro};
            case 1: return {normal: Verde, selected: sVerde};
            case 2: return {normal: AmarilloClaro, selected: sAmarilloClaro};
            case 3: return {normal: Naranja, selected: sNaranja};
            case 4: return {normal: Rojo, selected: sRojo};
            case "1": return {normal: Morado, selected: sMorado};
            default: return {normal: Gris, selected: sGris};
        }
    }
    
    layerPainter.prototype.paintLabel = function (labelCanvas, feature, shape, layer, map, state) {
        var label, labelName = "", i=0, properties = feature.properties, index;
        
        if(properties) {
            if(properties.description) {
                label = properties.description;
            }
            else {
                if(properties.NOMBRE) {
                    labelName = properties.NOMBRE;
                } else {
                for(var key in properties) {
                    if(i===0)
                         labelName = properties[key];
                    i++;
            } 
            }
        }
        } else {
            labelName = feature.id;
        }
        var date =  new Date(getTime());
        var y = date.getFullYear();
        var baseKey = "cantidad_Y";
        var key = baseKey.replace("Y", y);
        index = feature.properties[key];
        if(!index) {
            index = 0;
        }
        if(properties.description) {
            label += "<span style='color: $color' class='label'>Incidentes: " + index + "</span>";
        } else {
            label  = '<div class="labelwrapper">' +
                                '<div class="sensorLabel blueColorLabel">' +
                                '<div class="theader">' +
                                '<div class="leftTick blueColorTick"></div>' +
                                '<div class="rightTick blueColorTick"></div>' +
                                '<div class="name">'+labelName+'</div>' +
                                '</div>' +
                                '<div class="type">Incidentes : '+index+'</div>' +
                                '</div>' +
                                '</div>';
        }
            
        var labelStyle = {
          positions: (PointLabelPosition.NORTH),
          offset: 5,
          priority : 2
        };
        //label  = "<span class='label'>" + labelName + "</span>";
        labelCanvas.drawLabel(label,shape.focusPoint, labelStyle);
    };
    
    return layerPainter;
});




