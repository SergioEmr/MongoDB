/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define(["luciad/view/feature/FeaturePainter",
        "luciad/shape/ShapeFactory",
        'samplecommon/IconFactory',
        'luciad/util/ColorMap'
    ], function (FeaturePainter, ShapeFactory, IconFactory, ColorMap) {
    
    function layerPainter() {
    } 
    layerPainter.prototype = new FeaturePainter();
    layerPainter.prototype.constructor = layerPainter;
    layerPainter.prototype.getDetailLevelScales = function() {
        return [1 / 3000000, 1 / 2000000, 1 / 1000000,1 / 50000,1 / 5000];
    };
    
    var sBlanco = 'rgb(255, 255, 255)';
    var sGrisOscuro = 'rgb(50, 50, 50)';
    var sGrisClaro = 'rgb(200, 200, 200)', GrisClaro = 'rgba(200, 200, 200, 0.3)';
    var sRojo = 'rgb(255, 0, 0)', Rojo = 'rgba(255, 0, 0, 0.3)';
    var sAmarilloClaro = 'rgb(255, 255, 204)', AmarilloClaro = 'rgba(255, 255, 204, 0.3)';
    var sGris = 'rgb(230, 230, 230)', Gris = "rgba(230,230,230, 0.3)";
    var sNaranjaClaro = 'rgb(255, 239, 204)', NaranjaClaro = 'rgba(255, 239, 204, 0.3)';
    var sNaranja = 'rgb(255, 174, 102)', Naranja = 'rgba(255, 174, 102, 0.3)';
    var Verde = "rgba(50,230,50,0.3)", sVerde = "rgb(50,230,50)";
    var sMorado ="rgb(200,0,200)", Morado = "rgba(200,0,200, 0.5)";
    var Azul = "rgba(70, 100, 230, 0.3)", sAzul = "rgb(70, 100, 230)";
    var VerdeClaro = "rgba( 134, 255, 132 , 0.3)", sVerdeClaro = "rgb( 134, 255, 132 )";
    var maxRadio = 0, minRadio = 1;
    
    var baseIconsUrl = "../../proyecto/recursos/icons/";
    layerPainter.prototype.paintBody = function (geoCanvas, feature, shape, layer, map, state) {
        /*
        if(state.selected) 
            icon = IconFactory.circle({stroke: sBlanco, fill: sRojo, width: dimension, height: dimension});
        else 
            icon  = IconFactory.circle({stroke: sBlanco, fill: Azul, width: dimension, height: dimension});
                */
        
        var id = parseInt(feature.id);
            
        var properties = feature.properties;
        var tipo = properties.GEOGRAFICO || properties.nombre_act;
        tipo = coregirTipo(tipo);
        var url, colors, dimension = 35;
        switch(tipo.toLowerCase()) {
            default: url = baseIconsUrl+"lugares/iconComercio.png"; 
                colors = {normal: Gris, selected: sGris}; break;
                //console.log("no se encontro icono para " + tipo+"\t "+ properties.GEOGRAFICO || properties.nombre_act); break;
            case "escuela": url = baseIconsUrl+"lugares/iconEscuela.png"; 
                colors = {normal: VerdeClaro, selected: sVerdeClaro}; break;
            case "camell�n": colors = {normal: VerdeClaro, selected: sVerdeClaro}; break;
            case "instalaciones":
            case "instalaci�n": colors = {normal: Naranja, selected: sNaranja}; 
                url = baseIconsUrl+"lugares/iconGimnasio.png"; break;
            case "templo": colors = {normal: AmarilloClaro, selected: sAmarilloClaro};
                url = baseIconsUrl+"lugares/iconTemplo.png"; break;
            case "mercado": colors = {normal: NaranjaClaro, selected: sNaranjaClaro};
                url = baseIconsUrl+"lugares/iconComercio.png"; break;
            case "plaza": colors = {normal: VerdeClaro, selected: sVerdeClaro};
                url = baseIconsUrl+"lugares/iconParque.png"; break;
            case "centro": colors = {normal: AmarilloClaro, selected: sAmarilloClaro};
                url = baseIconsUrl+"lugares/iconHospital.png"; break;
            case "elaboraci�n":
            case "subestaci�n":
            case "fabricaci�n": colors = {normal: Gris, selected: sGris}; 
                url = baseIconsUrl+"lugares/iconFabrica.png"; break;
            case "cementerio": colors = {normal: Gris, selected: sGris}; 
                url = baseIconsUrl+"lugares/iconCementerio.png"; break;
            case "restaurantes": colors = {normal: Azul, selected: sAzul}; 
                url = baseIconsUrl+"lugares/iconRestaurante.png"; break;
            case "Salon": colors = {normal: Azul, selected: sAzul}; 
                url = baseIconsUrl+"lugares/iconBarberShop.png"; break;
        } 
        
        if(!feature.geometry.x) {
                geoCanvas.drawShape(shape,{ 
                    fill: {
                        color: state.selected ? colors.selected : colors.normal
                    },
                    stroke: {
                        color: colors.selected,
                        width: 5} 
                });
        }
        else {
            dimension = state.selected ? (dimension+15): dimension;
            var iconStyle = {
                url: url,
                width: dimension + "px",
                height: dimension + "px",
                draped: true
            };
            geoCanvas.drawIcon(shape.focusPoint, iconStyle);
        }
    };
    
    function coregirTipo(tipo) {
        var nTipo = "", c= '';
        for(var i in tipo) {
            c = tipo.charAt(i);
            if(c === ' ')
                return nTipo;
            nTipo += c;
        }
        return nTipo;
    }
    
    layerPainter.prototype.paintLabel = function (labelCanvas, feature, shape, layer, map, state){
        var label, labelName = "", i=0, properties = feature.properties;
        
        if(properties) {
            if(properties.GEOGRAFICO) {
                labelName = properties.GEOGRAFICO;
            }
            else {
                if(properties.nom_estab || properties.nombre_act)
                    labelName = properties.nom_estab || properties.nombre_act;
                else {
                for(var key in properties) {
                    if(i===0)
                        labelName = properties[key];
                    i++;
                } 
                }
            }
        } else {
            labelName = feature.id;
        }
        
        label = "<span style='color: $color' class='label'>" + labelName + "</span>";
        if(state.level > 1 && feature.geometry.x) {     
            if(state.selected)
                labelCanvas.drawLabel(label.replace("$color", sRojo),shape.focusPoint, {});
            else
                labelCanvas.drawLabel(label.replace("$color", sBlanco),shape.focusPoint, {});
        }
    };
    return layerPainter;
});

