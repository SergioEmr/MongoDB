/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define(["luciad/view/feature/FeaturePainter", "luciad/util/IconFactory",], 
function (FeaturePainter, IconFactory) {
    function layerPainter() { 
        this._iconCache = {};
    }
    layerPainter.prototype = new FeaturePainter();
    layerPainter.prototype.constructor = layerPainter;
    layerPainter.prototype.getDetailLevelScales = function() {
        return [1 / 3000000, 1 / 1500000, 1 / 750000,1 / 350000,1 / 30000];
    };
    layerPainter.prototype.paintBody = function (geoCanvas, feature, shape, layer, map, state) {
        var colorE  = "rgba(230,50,50, 0.5)",   colorLE = "rgb(230,50,50)";
        var colorD  = "rgba(225,230,235, 0.5)", colorLD = "rgb(225,230,235)";
        var colorS  = "rgba(50,230,50,0.5)",    colorLS = "rgb(50,230,50)";
        var colorSa = "rgba(52,150,236,0.5)",   colorLSa = "rgba(52,150,236)";
        var colorM  = "rgba(224,256,52,0.5)",   colorLM = "rgb(224,256,52)";
        var dimension ;
        //dimension = Math.pow(1.5, Math.round(feature.properties.Duracion));
        dimension = (feature.properties.Duracion*100)/300;
        if(dimension >100)
            dimension=100;
        if(dimension <15)
            dimension=15;
        var tipo= feature.properties.Tipo, color, colorL;
        switch(tipo.toUpperCase()) {
            case "VOZ SALIENTE": 
                color = colorSa;
                colorL = colorLSa;
                break;
            case "VOZ ENTRANTE": 
                color = colorE;
                colorL = colorLE; 
                break;
            case "MENSAJES 2 VIAS": 
                color = colorM;
                colorL = colorLM;
                break;
            default: 
                color = colorD;
                colorL = colorLD;
                break;
        }
        var icon, cacheKey;
        if(state.selected) {
            cacheKey = "" +dimension + colorS + colorLS;
            icon = this._iconCache[cacheKey] = IconFactory.circle({stroke: colorLS, fill: colorS, width: dimension, height: dimension});
        }
        else {
            cacheKey = "" +dimension + color + colorL;
            icon = this._iconCache[cacheKey] = IconFactory.circle({stroke: colorL, fill: color, width: dimension, height: dimension});
        }
        if(feature.geometry.x) {
            geoCanvas.drawIcon(shape.focusPoint,{
                width: dimension + "px",
                height: dimension + "px",
                image: icon
            });
        } else {
            geoCanvas.drawShape(shape,{ 
                    stroke: {
                        color: 'rgb(255, 174, 102)',
                        width: 5} 
                });
        }
    };
    return layerPainter;
});

