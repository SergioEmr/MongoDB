var map, municipios = [], tablaIncidentes = null, tablaMunicipios = [], tabalColonias =[];
var llamadasLayer, tracksLayer, layerTime, actualTime, tablaEstados =[];
define([
    "recursos/js/MapFactory",
    "recursos/js/GoogleMap",
    "recursos/js/LayerFactory",
    "dojo/dom",
    "dojo/on",
    "dojo/query",
    "luciad/view/PaintRepresentation",
    "samplecommon/dojo/DojoMap",
    "luciad/reference/ReferenceProvider",
    "samplecommon/LayerConfigUtil",
    
    
    'luciad/util/ColorMap',
    "./createTimeLayer",
    "recursos/js/Graficas/Graficas",
    "recursos/js/Shapes",
    
    "./painters/municipiosPainter",
    "./painters/paisPainter",
    "./painters/sectoresPainter",
    "./painters/localidadesPainter",
    
    "./BalloonStyles/defaultBalloon",
    "./updateHistogram",
    "./painters/heatMapPainter",
    
    "./Tablas",
    "luciad/shape/ShapeFactory",
    "recursos/js/Util",
    'luciad/util/Promise',
    "./BalloonStyles/eventosBalloon",
    "recursos/js/Cluster/CustomClusterPainter",
    "recursos/js/Cluster/ClusterBalloon",
    
    "./TimeSliderHistogram",
    "./painters/layerPainterPoints",
    "./GraficasExtras",
    "recursos/js/Util",
    "recursos/js/TimeChart",
    "./painters/alertasPainter",
    
    "./Conexiones",
    "./painters/layerPainter",
    "template/sampleReady!"
], function (MapFactory, GoogleMap, LayerFactory, dom, on,query, PaintRepresentation, DojoMap, ReferenceProvider, LayerConfigUtil,
        ColorMap, createTimeLayer, Graficas, Shapes,
         municipiosPainter, paisPainter, sectoresPainter, localidadesPainter,
        defaultBalloon, updateHistogram, heatMapPainter,
        Tablas, ShapeFactory, Util, Promise, eventosBalloon, CustomClusterPainter, ClusterBalloon, 
        TimeSliderHistogram, layerPainterPoints, GraficasExtras, Util, TimeChart, alertasPainter,
        Conexiones, layerPainter
) {
    
    var referenceC = ReferenceProvider.getReference("CRS:84");
    var referenceE = ReferenceProvider.getReference("EPSG:4978");
    var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i
            .test(navigator.userAgent);
    var googleMap = false;
    //Layer list
    var patrulla, zonas, sectores, ageb,datosMunicipios, eventos = null, features =null, rutas = [], puntos =[], editable, mexico;
    var localidades, comercios, localidades2, colonias;
    
    var selected = null, tablaDatos, menuAbierto=true, menuInteligencia = false;
     /*
     * Creacion de la barra de tiempo
     */
    var nuevoRegistro = 0;
    var timeFilter, timeSlider, startTime, endTime, Year, fitColumns = false;
    var cargarDatos = true, alertas;
    var habilitar = true;
    /*
     * Funcion encargada de crear el mala dependiendo de si esta en un dispositivo mobil o no
     * creara el mapa en 3d o 2d, ademas de si esta activado e mapa de google 
     * 
     * Google: satellite, hybrid, roadmap o terrain
     * BingMap: AerialWithLabels, Road o Aerial
    */
    var allowLayers=true;
    var baseUrl = "https://www.olly.science/"; //https://178.128.76.236/
    var baseDataUrl = "data/"; //"../static/web/proyecto/Canacar/data/"; 
    var baseRecursosUrl = "../../proyecto/recursos/"; //"../static/web/proyecto/recursos/"; 
    var usuario = document.getElementById("tipoUsuario").innerHTML;
    switch(usuario.toLowerCase()) {
        case "088": usuario="u"; break;
        case "dev": usuario="u"; break;
        default: usuario="ig"; break;
    }
    Start();
    function Start() {
        if(googleMap)
            map = GoogleMap.crearMapaGoogle("map", "roadmap"); 
        else {
            if(isMobile)
                map = MapFactory.makeMap3d("map", {reference: referenceC}, {
                    includeBackground: false, includeElevation: false, includeLayerControl: true,
                    includeMouseLocation: false});
            else
                map = MapFactory.makeMap3d("map", {reference: referenceE}, {
                    includeBackground: false, includeElevation: true, includeLayerControl: true,
                    includeMouseLocation: false, includeBingLayer: ["Road", "Aerial"]});
        } 
        CrearCapas();
        //onHoverController = new OnHoverController();
        //map.controller = onHoverController;
    }
    /*
     * Funcion encagrada de la creacion de todas las capas que seran mostradas en el mapa principal.
     * ademas aqui se encuentra una funcion ajax con la que leemos la informacion necesaria para crear la capa de eventos.
     * 
     * @returns {undefined}
     */
    function CrearCapas() {
        mexico = LayerFactory.createFeatureLayer(referenceC, {label: "Mexico", painter: new paisPainter(), selectable: true, editable: false}, baseRecursosUrl+"json/estatal.json");
        map.layerTree.addChild(mexico);
        mexico.balloonContentProvider = function(feature) {
            return defaultBalloon.getBallon(feature);
        };
        
        var urls =["todos"]; //["Mexico"]
        municipios[0] = LayerFactory.createMunicipiosLayer(urls, referenceC, {label: "Municipios", visible: false, selectable: true, 
                editable: false, painter: new municipiosPainter()}, baseRecursosUrl, true);
        municipios.forEach(function(layer) {
            layer.balloonContentProvider = function(feature) {
                return defaultBalloon.getBallon(feature);
            };
            map.layerTree.addChild(layer);
        });
        
        datosMunicipios = LayerFactory.createFeatureLayer(referenceC, {label: "Informacion", visible: false, selectable: true, painter: new layerPainter(), selected: true}, "data/datosnuevos.json");
        map.layerTree.addChild(datosMunicipios);
        datosMunicipios.balloonContentProvider = function(feature) {
            return defaultBalloon.getBallon(feature);
        };
        
        colonias = LayerFactory.createFeatureLayer(referenceC, {label: "Colonias", visible:false, editable: false, selectable: true, painter: new sectoresPainter()}, baseDataUrl+"colonias.geojson");
        map.layerTree.addChild(colonias);
        colonias.balloonContentProvider = function(feature) {
            return defaultBalloon.getBallon(feature);
        };
        //colonias.setPaintRepresentationVisible(PaintRepresentation.LABEL, false);
        
        localidades = LayerFactory.createUrlLayer(referenceC, {label: "Localidades", visible: false, selectable: true, painter: new localidadesPainter()}, "data/sip_ecatepec.geojson");
        map.layerTree.addChild(localidades);
        localidades.balloonContentProvider = function(feature) {
            return defaultBalloon.getBallon(feature);
        };
        localidades.setPaintRepresentationVisible(PaintRepresentation.LABEL, false);
        
        localidades2 = LayerFactory.createUrlLayer(referenceC, {label: "Localidades Poligonos", visible: false, selectable: true, painter: new localidadesPainter()}, "data/sia_ecatepec.geojson");
        map.layerTree.addChild(localidades2);
        localidades2.balloonContentProvider = function(feature) {
            return defaultBalloon.getBallon(feature);
        };
        localidades2.setPaintRepresentationVisible(PaintRepresentation.LABEL, false);
        
        var cluster = CustomClusterPainter.newCluster(new localidadesPainter(), {clusterSize: 100, minimumPoints: 50});
        comercios = LayerFactory.createUrlLayer(referenceC, 
            {label: "Comercios", visible: false, selectable: true, painter: cluster.painter, transformer: cluster.transformer},
            "data/small_ecatepec.geojson");
        map.layerTree.addChild(comercios);
        comercios.balloonContentProvider = function(feature) {
            return ClusterBalloon.getBalloon(feature);
        };
        comercios.setPaintRepresentationVisible(PaintRepresentation.LABEL, false);
        
        llamadasLayer = LayerFactory.createUrlLayer(referenceC, {label:"Actividad Telefonica", editable: false, selectable: true, painter: new layerPainterPoints()},
            "data/data.json");
        map.layerTree.addChild(llamadasLayer);
        llamadasLayer.balloonContentProvider = function(feature) {
            return defaultBalloon.getBallon(feature);
        };
        
        alertas = LayerFactory.createMemoryLayer(referenceC, {label: "Alertas", editable: false, selectable:true, painter: new alertasPainter()});
        map.layerTree.addChild(alertas);
        alertas.balloonContentProvider = function(feature) {
            return defaultBalloon.getBallon(feature);
        };
        
        DojoMap.configure(map);
        LayerConfigUtil.addLonLatGridLayer(map);
        
        //crearDatosLayer(municipios);
        
        /*
         * 
         * Aqui creamos una variable la cual usaremos para identificar el a�o del cual mostraremos la informacion
         */
        var sStart = "2017/01/01 00:00:00", sEnd = "2019/01/01 00:00:00";
        startTime = Date.parse(sStart)/1000, endTime = Date.parse(sEnd)/1000;
        try {
            leerJSON();
            crearTimeSlider();
        }catch(e) {
            console.log(e);
        }
    }
    /*
     *============================================================================================================
     *                                      Craear la capa de Incidentes
     *============================================================================================================
     */
    var heatpainter, clusterpainter, intentos = 0, totalIncidentes = 0;
    
    
    
    
    function cagrarDatos () {
        intentos++;
        console.log("intento "+intentos+" de cargar datos...");
        $.ajax({
            type:'Get',
            dataType: "json",
            url: baseDataUrl+"ecatepec.geojson"
        }).done(function(data) {
            console.log("archivo leido "+ baseDataUrl+"ecatepec.geojson");
            features = createTimeLayer.createTimeFeature(data.features);
            var graficas = crearGraficas(features);
            updateHistogram.updateHistogram(map, features, eventos, referenceC, startTime, endTime, tablaDatos, graficas,municipios );
        }).fail(function (e) {
            console.log(e);
            if(intentos<5)
                setTimeout(cargarDatos, 100);
        });
    }
    function leerJSON() {
        console.log("Cargando capa "+ baseDataUrl+"ecatepec.geojson" );
        heatpainter = new heatMapPainter();
        var options = {
            label: "Incidentes", id:"eventos", painter: heatpainter,
            editable: false, selectable: true
        };
        
        eventos = LayerFactory.createFeatureLayer(referenceC, options, baseDataUrl+"ecatepec.geojson");
        eventos.balloonContentProvider = function(feature) {
            return eventosBalloon.getBallon(feature);
        };
        map.layerTree.addChild(eventos);
        var queryFinishedHandle = eventos.workingSet.on("QueryFinished", function() {
            if(eventos.bounds) 
                map.mapNavigator.fit({bounds: eventos.bounds, animate: true});
            
            queryFinishedHandle.remove();
        });
        setTimeout(cagrarDatos,10000);
    }
    
    function leerServidor() {
        var options = {label: "Incidentes", 
                id: "eventos", 
                editable:false, 
                painter: new heatMapPainter(), 
                visible: true,
                selectable: true
        };
        eventos = LayerFactory.createFeatureLayer(referenceC, options, baseUrl+"heatmap/data="+usuario);
        map.layerTree.addChild(eventos);
        eventos.balloonContentProvider = function(feature) {
            return eventosBalloon.getBallon(feature);
        };
        var promiseFeatures = eventos.model.query();
        Promise.when(promiseFeatures, function(cursor) {
            features = [];
            var index = 0;
            while (cursor.hasNext()) {
                var feature = cursor.next();
                features[index] = feature;
                index++;
            }
            console.log("Cargados "+index+" eventos");
            features = createTimeLayer.createTimeFeature(features);
            var graficas = crearGraficas(features);
            updateHistogram.updateHistogram(map, features, eventos, referenceC, startTime, endTime, tablaDatos, graficas,municipios );
        });
    }
    
    function reestablecerEventos() {
        map.layerTree.removeChild(eventos);
        updateHistogram.destruirComponentes();
        setTimeout(leerServidor, 500);
    }
    
    leerDatosMunicipios();
    function leerDatosMunicipios() {
        $.ajax({
            type:'Get',
            dataType: "json",
            url: "data/datosMunicipios.json"
        }).done(function(datos) {
            console.log("archivo leido datosMunicipios.json");
            var encabezados = ["ENTIDAD", "REGION","NÚMERO DE HOMICIDIOS EN EL MES DE DICIEMBRE DE 2018", "PORCENTAJE NACIONAL", "TASA DE HOMICIDIOS"];
            var tablaMunicipios = [encabezados], columnas = [], fila;
            for(var i=0; i<datos.length; i++) {
                fila = datos[i];
                for(var j in encabezados) {
                    columnas [j] = fila[encabezados[j]] || "";
                }
                tablaMunicipios[i+1] = columnas;
                columnas = [];
            }
            Tablas.crearTablaDIV("tablaMunicipios", tablaMunicipios);
        }).fail(function (e) {
            console.log(e);
            if(intentos<5)
                setTimeout(cargarDatos, 100);
        });
    }
    /*
     *============================================================================================================
     *                                     Conexiones con Endpoints
     *============================================================================================================
     */
    function leerEndpointXMLHttps() {
        console.log("intentando conectar...");
        var http = new XMLHttpRequest();
        http.open("GET", "http://68.183.175.36/sedena/request", true);
        http.onreadystatechange = function(aEvent) {
            console.log("Status "+http.status);
            if(http.readyState === 4 && (http.status === 200 || http.status === 202)) { 
                var data = JSON.parse(http.responseText);
                var features = data.features;
                console.log("Conectado con http://68.183.175.36/sedena/request mediante XMLHttpRequest");
                console.log(features);
                comprobarAlertas(features);
            }
        };
        http.send(null);
    }
    function leerEndpoint() {
        $.ajax({
            type:'Get',
            dataType: "json",
            url: "https://www.olly.science/sedena/request"
            //url: "http://68.183.175.36/sedena/request"
        }).done(function(data) {
            console.log("Conectado con https://www.olly.science/sedena/request");
            var features  = data.features;
            comprobarAlertas(features);
        }).fail(function (e) {
            console.log(e);
        });
    }
    
    function comprobarAlertas(features) {
        if(totalIncidentes ===0 || !totalIncidentes) {
                totalIncidentes = features.length;
            Tablas.crearArreglo(features, "tNuevosEventos");
        }
            else {
                if(features.length > totalIncidentes) {
                    alert("Se detecto un nuevo incidente");
                    var feature = features[totalIncidentes];
                    var newFeature, id=totalIncidentes;
                    var fecha = createTimeLayer.corregirFecha(feature.properties.fecha_hechos);
                    var hora = feature.properties.hora_hechos;
                    var eventTime = fecha +" "+hora;
                    feature.properties.EventTime = eventTime;
                    newFeature = Shapes.createNewFeature(referenceC, feature, {id: id});
                    try {
                        //eventos.model.add(newFeature);
                        newFeature.id = 0;
                        alertas.model.put(newFeature);
                        map.mapNavigator.fit({bounds: newFeature.geometry.bounds, animate: true});
                        totalIncidentes = features.length;
                    }catch(e) {
                        console.log(e);
                    }
                    Tablas.crearArreglo(features, "tNuevosEventos");
                } 
            }
    }
    var divs = ["rfRegistro", "rviaEntrada","rEstadoExpediente","rconclucion","rfConclusion","rCargo",//5
            "rDNombre","rDParentesco","rDDirecion","pADireccion","pDtelefono","pDCargo",//11
            "pANombre","pAPaterno","pAMaterno","pAEdad","pASexo","pANacionalidad",//17
            "rAutoridad","rHechos","rPais","rfHechos","rHora","rMunicipio","rEntidad","rLugar","rLatitud","rLongitud",//27
            "rNPersonas","rDArma","rDescripcionVehiculo1","rDescripcionVehiculo2",//31
            "rNarracion","rAsunto"];//33
        var divsSelect = ["rUniformados","rArmados","rVehiculoActivo","rVehiculoPasivo",//38
            "rDOrganizadaActivo","rDOrganizadaPasivo","rVehiculoActivo"];//41
    $("#registrarPersona").click(function() {
        Conexiones.registrarDesaparecido(divs, divsSelect);
        $("#panelRegistroRobo").fadeOut("slow");
        limpiarDatos();
    });
    $("#bLimpiar").click(function() {
        limpiarDatos();
    });
    function limpiarDatos() {
        for(var i in divs) {
            document.getElementById(divs[i]).value = "";
        }
    }
    $("#bLlenar").click(function() {
        document.getElementById("rfRegistro").value = "12/20/10",
        document.getElementById("rEstadoExpediente").value ="Espera",
        document.getElementById("rDNombre").value ="Nombre",
        document.getElementById("rDDirecion").value ="Dirección",
        document.getElementById("rDParentesco").value ="Hermano",
        document.getElementById("pADireccion").value ="Dirección",
        document.getElementById("pDtelefono").value ="5532908004",
        document.getElementById("pDCargo").value ="cargo",
        document.getElementById("pANombre").value ="Agraviado",
        document.getElementById("pAEdad").value =20,
        document.getElementById("pASexo").value ="Hombre",
        document.getElementById("pANacionalidad").value ="Mexicano",
        document.getElementById("rAutoridad").value ="Autoridades",
        document.getElementById("rHora").value ="12:20",
        document.getElementById("rfHechos").value ="20/10/18",
        document.getElementById("rEntidad").value ="Sinaloa",
        document.getElementById("rMunicipio").value = "Mun",
        document.getElementById("rLugar").value ="",
        document.getElementById("rNPersonas").value =2,
        
        //document.getElementById("rArmados").value ="Sí",
        //document.getElementById("rUniformados").value ="No",
        //document.getElementById("rVehiculoActivo").value ="",
        document.getElementById("rDescripcionVehiculo1").value ="",
        //document.getElementById("rDOrganizadaActivo").value ="Sí",
        //document.getElementById("rDOrganizadaPasivo").value ="Sí",
        
        document.getElementById("rNarracion").value ="Voló sobre mí y me disparó con su rayo láser",
        document.getElementById("rLatitud").value =19.689236,
        document.getElementById("rLongitud").value =-99.86973
    });
    /*
     *============================================================================================================
     *                                     Funciones de algunos botones
     *============================================================================================================
     */
    /*
     * botones superiores en el mapa
     */
    document.getElementById("botonGraficas").value = menuAbierto;
    $("#botonGraficas").click(function () {
        if(menuAbierto) {
            $("#menuGraficas").fadeOut("slow");
            $("#afterafter").fadeOut("slow");
            menuAbierto=false;
        }else {
            //$("#menuInteligencia").fadeOut("slow");
            $("#menuGraficas").fadeIn("slow");
            $("#panelHorario").fadeOut("slow");
            $("#afterafter").fadeIn("slow");
            menuAbierto=true;
            menuInteligencia = false;
        }
        document.getElementById("botonGraficas").value = menuAbierto;
    });
    $("#botonInteligencia").click(function () {
        if(menuInteligencia) {
            $("#panelHorario").fadeOut("slow");
            //$("#menuInteligencia").fadeOut("slow");
            menuInteligencia = false;
        }else {
            $("#menuGraficas").fadeOut("slow");
            $("#afterafter").fadeOut("slow");
            $("#panelHorario").fadeIn("slow");
            //$("#menuInteligencia").fadeIn("slow");
            menuAbierto=false;
            menuInteligencia = true;
        }
    });
    
    $("#bInformacion").click(function() {
        datosMunicipios.visible = !datosMunicipios.visible;
        if(datosMunicipios.visible === false) {
            $("#panelTablaMunicipios").fadeOut("slow");
            return 0;
        }
        var queryFinishedHandle = datosMunicipios.workingSet.on("QueryFinished", function() {
            if(datosMunicipios.bounds) 
                map.mapNavigator.fit({bounds: datosMunicipios.bounds, animate: true});
            queryFinishedHandle.remove();
        });
        $("#panelTablaMunicipios").fadeIn("slow");
        municipios.forEach(function (municipio) {
            municipio.visible = false;
        });
    });
    
    var cb = document.getElementById("cbPeticiones");
    cb.addEventListener("change", function() {
        habilitar = !habilitar;
        console.log("Permitir conexiones "+habilitar);
    });
    /*
     *============================================================================================================
     *                                      Exportar Informacion
     *============================================================================================================
     */
    $("#exportatTodaInformacion").click(function(boton) {
        Tablas.crearTablaDIV("tablaTemporal",  tablaDatos, 1000);
        Util.exportarTablaXLS(boton, "tablaTemporal");
    });
    $("#exportatInformacion").click(function(boton) {
        Util.exportarTablaXLS(boton, "tablaCompleta");
    });
    /*
     *============================================================================================================
     *                                      Reloj
     *============================================================================================================
     */
    var X = -1, x=0, y = 30, y2 = 0, espera = 0;
    timeUpdated();
    function timeUpdated() {
        //$('#timelabel').text(formatTime());
        espera++;
        if(espera >15) {
            espera = 0;
            //leerEndpointXMLHttps();
            if(habilitar)
                leerEndpoint();
        }
        
        setTimeout(timeUpdated,1000);
     }
     /*
      * con esta funcion daremos formato a la etiqueta del reloj, utilizando el tiempo actual del equipo.
      * @returns {String}
      */
     function formatTime() {
        var date = new Date();
        var h = date.getHours() <10 ? "0"+date.getHours(): date.getHours();
        var m = date.getMinutes() <10 ? "0"+date.getMinutes(): date.getMinutes();
        var s = date.getSeconds() <10 ? "0"+date.getSeconds(): date.getSeconds();
        return h+":"+m+":"+s;
    }
   
    ////========================================================================================///
    function formatDate( fecha, hora) {
        var i, j=0, y, m, d, c, newDate="";
        var baseDate = "aa/mes/dia hora:00";
        for(i=0; i<fecha.length; i++) {
            c = fecha.charAt(i);
            if(c ==='/' || c===' ' || c==='/n' || c===':') {
                switch(j) {
                    case 0: d = newDate; break;
                    case 1: m = newDate; break;
                    case 2: y = newDate; break;
                }
                newDate = "";
                j++;
            } else {
                newDate+= c;
            }
        }
        y = newDate;
        newDate = baseDate.replace("aa", y).replace("mes", m).replace("dia", d).replace("hora:00", hora);
        return newDate;
    }
    /*
     *============================================================================================================
     *                                      Ocultar Etiquetas
     *============================================================================================================
     */
    $("#labels").click(function () {
        var domId = document.getElementById("labels");
        var value;
        if(domId.value === "false" || domId.value===false || !domId.value ) {
            value = false;
        } else
            value = true;
        if(allowLayers===true) {
        for(var i=0; i<municipios.length; i++) {
            municipios[i].setPaintRepresentationVisible(PaintRepresentation.LABEL, value);
            municipios[i].selectable = !value;
        }
        }
        mexico.setPaintRepresentationVisible(PaintRepresentation.LABEL, value);
        colonias.setPaintRepresentationVisible(PaintRepresentation.LABEL, value);
        if(value===true) 
            domId.title = "Ocultar Etiquetas";
        else
            domId.title = "Mostrar Etiquetas";
        domId.value = !value;
    });
    $("#bMunicipios").click(function() {
       municipios.forEach(function(layer) {
           layer.visible = !layer.visible;
       });
    });
    $("#bColonias").click(function() {
        colonias.visible = !colonias.visible;
    });
    $("#bEstados").click(function() {
        mexico.visible = !mexico.visible;
    });
    $("#bComercios").click(function() {
        comercios.visible = !comercios.visible;
    });
    $("#bLocalidades").click(function() {
        localidades.visible = !localidades.visible;
        localidades2.visible = !localidades2.visible;
    });
    $("#bEventos").click(function() {
        eventos.visible = !eventos.visible;
    });
    $("#datosPersona").click(function() {
        document.getElementById("rNombre").value = "Antonio";
        document.getElementById("rPaterno").value = "Del Valle";
        document.getElementById("rMaterno").value = "Soliz";
        document.getElementById("rEntidad").value = "Ecatepec";
        document.getElementById("rEdad").value = "20";
        document.getElementById("rColonia").value = "Jardines de Ecatepec";
        document.getElementById("rFecha").value = "05/11/2018";
        document.getElementById("rHora").value = "05:00 pm";
        document.getElementById("rCoordenadas").value = "-99.034139 19.593936";
        document.getElementById("rCarpeta").value = "MP de Ecatepec";
        document.getElementById("rModulo").value = "Fiscalia";
    });
    /*
     *============================================================================================================
     *                                      CREACION DE GRAFICAS
     *============================================================================================================
     */
    var fil = ["violenciaFiltro", "municipiosFiltro", "coloniasFiltro", "diaFiltro"];
    var lab =["SEXO_D", "EDAD_D", "OCUPACION_D", "COLONIA_D"];
    var titles = ["Sexo","Edad","Ocupacion", "Colonia"];
    var types = ["piechart",  "piechart", "piechart", "piechart"];
    var tops = [5, 6, 10, 10];
    var gEmpresas={}, gNombres={}, gMotivo={};
    var nombres = [];
    function crearGraficas(features) {
        var datos = Graficas.crearGraficasFeatures(features, "Graficas", lab, types, fil, null, titles, null, tops);
        var graficas = datos.graficas;
        tablaDatos = datos.tablaDatos;
        Tablas.crearTablaDIV("tablaCompleta", tablaDatos, 1000);
        getNombres(tablaDatos);
        
        datos = null;
        tabalColonias = graficas[3].tabla;
        return graficas;
    }
    
    function getNombres(tabla) {
        var nombres = [], j=0;
        var nombre, paterno, materno;
        for(var i=1; i<tabla.length; i++) {
            nombre = tabla[i][12]+"";
            paterno = tabla[i][13]+"";
            materno = tabla[i][14]+"";
            nombres[j] = nombre+" "+paterno+" "+materno;
            j++;
            nombres[j] = tabla[i][0]+"";
            j++;
        }
        $("#tags").autocomplete({
                source: nombres,
                position: {
                    my: "left bottom",
                    at: "left top"
                },
                select: function (event, ui) {
                    var label = ui.item.label;
                    $('#tags').val(label);

                    buscarPersona(label);

                    return false;
                }
            });
    }
    $("#bBuscarPersona").click(function () {
        var nombre = document.getElementById("tags").value;
        if(nombre === ""){
            alert("no se escribio ningun nombre");
            return 0;
        }
        buscarPersona(nombre.toUpperCase());
    });
    $("#bQuitarFiltro").click(function () {
        eventos.filter = null;
         document.getElementById("tags").value = "";
    });
    function buscarPersona(nombreOId) {
        eventos.filter = function (feature) {
            var n = feature.properties.NOMBRE_D;
            var p = feature.properties.PATERNO_D;
            var m = feature.properties.MATERNO_D;
            var completo = n+" "+p+" "+m;
            var id = feature.properties.IDUNICO+"";
            if(completo === nombreOId || id === nombreOId) 
                return true;
            else
                return false;
            
        };
    }
    /*
     *      Control de Densidad del mapa de calor
     */
    $("#Densidad").on("change input", function() {
        var x = 1, y = 0;
        x = Math.round(parseFloat($("#Densidad").val()));
        if(x<=10 && x >0) {
            eventos.painter.density = {
                colorMap: ColorMap.createGradientColorMap([
                {level: y, color: "rgba(  0,   0,   255, 0.5)"},
                {level: y+=x, color: "rgba(  0, 100,   255, 0.5)"},
                {level: y+=x*2, color: "rgba(  0, 255,   255, 1.0)"},
                {level: y+=x*3, color: "rgba(  255, 255,   0, 1.0)"},
                {level: y+=x*4, color: "rgba(255, 0, 0, 1.0)"}
                ])
            };
        } else {
            eventos.painter.density = null;
        }
    });
    
    
    /*
     * ==========================================================================================
     *              Time Slider llamadas
     * ==========================================================================================
     */
    function crearTimeSlider() {
        var sStart = "2018/01/01 00:00:00", sEnd = "2019/01/01 00:00:00";
        var startTime2 = Date.parse(sStart)/1000, endTime2 = Date.parse(sEnd)/1000;
        timeSlider = TimeSliderHistogram.createTimeSliderHistogram("mapaLlamadas", {startTime: startTime2, endTime: endTime2, top:1000, botom: 0});
        var histogram = timeSlider.histogram;
        timeSlider = timeSlider.timeSlider;
        var histogramLayer = TimeSliderHistogram.createHistorgamLayer(histogram);
        layerTime = TimeSliderHistogram.crearLayerTimeShapes(llamadasLayer.model, new layerPainterPoints());
        timeSlider.layerTree.addChild(layerTime, "top");
        timeSlider.layerTree.addChild(histogramLayer, "top");

        TimeSliderHistogram.syncVisibleRanges(map, llamadasLayer, timeSlider, layerTime, histogram);
        TimeSliderHistogram.syncSelection(map, llamadasLayer, timeSlider, layerTime);
        llamadasLayer.workingSet.on("QueryFinished", function () {
            TimeSliderHistogram.updateHistogram(histogram, llamadasLayer, layerTime);
            $("#panelMapaLlamadas").fadeOut();
            $("#panelDatosLlamadas").fadeOut();
            llamadasLayer.visible = false;
        });
        crearTracksLayer(llamadasLayer, histogram);
       
    }
    
    $("#bLlamadas").click(function() {
        llamadasLayer.visible = !llamadasLayer.visible;
        tracksLayer.visible = llamadasLayer.visible;
        if(llamadasLayer.visible === true) {
            $("#afterafter").fadeOut();
            $("#menuGraficas").fadeOut();
            $("#panelMapaLlamadas").fadeIn("slow");
            $("#panelDatosLlamadas").fadeIn("slow");
        } else {
            $("#panelMapaLlamadas").fadeOut();
            $("#panelDatosLlamadas").fadeOut();
        }
    });
    
    function crearTracksLayer(layer, histogram) {
        var promiseFeatures = layer.model.query();
        Promise.when(promiseFeatures, function(cursor) {
            var features = [], feature;
            var index = 0;
            while(cursor.hasNext()) {
                feature = cursor.next();
                features[index] = feature;
                index++;
            }
            console.log("cargados "+index+" llamadas");
            var entrante, saliente, j=0, tracks = [];
            for(var i=1; i< index; i+=2) {
                var tipo1 = features[i-1].properties.Tipo;
                switch(tipo1.toLowerCase()) {
                    case "voz saliente": saliente = features[i-1];
                        entrante = features[i]; break;
                    case "voz entrante": entrante = features[i-1];
                        saliente = features[i]; break;
                    default: entrante = features[i-1];
                        saliente = features[i]; break;
                }
                var x = [entrante.geometry.x, saliente.geometry.x];
                var y = [entrante.geometry.y, saliente.geometry.y];
                var line = Shapes.createPolyline(referenceC, x, y, 0, j, entrante.properties);
                tracks[j] = line;
                j++;
            }
            
            tracksLayer = LayerFactory.createMemoryLayer(referenceC, {label: "Tracks Llamadas", visible: false, selectable: false, editable: false, painter: new layerPainterPoints()}, tracks);
            map.layerTree.addChild(tracksLayer);
            
            var layerTime2 = TimeSliderHistogram.crearLayerTimeShapes(tracksLayer.model, new layerPainterPoints());
            timeSlider.layerTree.addChild(layerTime2);
            TimeSliderHistogram.syncVisibleRanges(map, tracksLayer, timeSlider, layerTime2, histogram);
            tracksLayer.workingSet.on("QueryFinished", function () {
                TimeSliderHistogram.updateHistogram(histogram, tracksLayer, layerTime2);
            });
        });
        GraficasExtras.graficaFrecuencia("GraficaFrecuencias");
    }
    
    var personaFiltro = document.getElementById("personaFiltro");
    personaFiltro.addEventListener("change", function () {
        var persona = personaFiltro.selectedOptions[0].value;
        if(persona === "0") {
            llamadasLayer.filter = null;
            tracksLayer.filter = null;
            return 0;
        }
        var filter = function (feature) {
            var p = feature.properties.persona;
            if(p === parseInt(persona))
                return true;
            else
                return false;
        };
        llamadasLayer.filter = filter;
        tracksLayer.filter = filter;
    });
    
    /*
     * ==========================================================================================
     *                Tabla Municipios
     * ==========================================================================================
     */
    leerTablaMunicipios();
    function leerTablaMunicipios() {
        $.ajax({
            type:'Get',
            dataType: "json",
            url: baseDataUrl+"municipios_data.json"
        }).done(function(data) {
            console.log("archivo leido "+ baseDataUrl+"municipios_data.json");
            tablaMunicipios = data;
        }).fail(function (e) {
            console.log(e);
        });
    }
    leerTablaEstados();
    function leerTablaEstados() {
        $.ajax({
            type:'Get',
            dataType: "json",
            url: baseDataUrl+"estados_data.json"
        }).done(function(data) {
            console.log("archivo leido "+ baseDataUrl+"estados_data.json");
            tablaEstados = data;
        }).fail(function (e) {
            console.log(e);
        });
    }
    
     /*
     * ==========================================================================================
     *              TimeChart
     * ==========================================================================================
     */
    var dataSetStartTime = Date.parse("2014/01/01 00:00:00") / 1000;//1421143477; // Tue Jan 13 11:04:37 CET 2015
    var dataSetEndTime = Date.parse("2019/01/01 00:00:00") / 1000;
    var speedup = 5000000; //speedup
    var lasttime = Date.now();
    var playing = false;
    var timeChart = new TimeChart(dom.byId("timeChart"), new Date(dataSetStartTime * 1000), new Date(dataSetEndTime * 1000));
    wireListeners();
    setTime(0);
    
    function timeChartUpdated() {
        actualTime = timeChart.getCurrentTime().getTime();
        $('#timeChartLabel').text(Util.formatDate(actualTime, "dd/mm/aaaa"));
        $('#timelabel').text(Util.formatDate(actualTime, "dd/mm/aaaa"));
        municipios.forEach(function(layer) {
            layer.filter = function(feature) {
                return true;
            };
        });
        colonias.filter = function(feature) {
            return true;
        };
        mexico.filter = function ( feature) {
            return true;
        };
        /*
        if(selectedChart && selectedFeature) {
            var nuevaTabla = tablas.tablaActual(selectedFeature, getDetailTime());
            if(nuevaTabla.sumaTotal ===0)
                document.getElementById("contentBalloon").innerHTML += "<p>No ocurrieron delitos en este periodo</p>";
            else 
                Graficas.actualizarGrafica(selectedChart.idGrafica, nuevaTabla.tabla, "piechart");
        }*/
     }
     
     function wireListeners() {
    //timechart
        timeChart.addInteractionListener(timeChartUpdated);
        timeChart.addZoomListener(function(currZoomLevel, prevZoomLevel) {
            speedup *= prevZoomLevel / currZoomLevel;
        });

        $("#play").click(function() {
            lasttime = Date.now();
            play(!playing);
        });

        //expose navigateTo on window so it can be called from index.html
        window.navigateTo = function(moveTo) {
            var wgs84 = ReferenceProvider.getReference("EPSG:4326");
            var center = ShapeFactory.createPoint(wgs84, [moveTo.lon, moveTo.lat]);
            var radius = moveTo.radius || 5.0;
            var bounds = ShapeFactory.createBounds(wgs84, [center.x - radius, 2 * radius, center.y - radius, 2 * radius]);
            return map.mapNavigator.fit(bounds);
        };
    }
    
    function playStep() {
        if (playing) {
            var currTime = Date.now();
            var deltaTime = (currTime - lasttime);
            lasttime = currTime;
            var timeChartTime = timeChart.getCurrentTime().getTime();
            var newTime = timeChartTime + (deltaTime) * speedup;
            if (newTime >= timeChart.endDate.getTime()) {
            newTime = timeChart.startDate.getTime();
        }
        timeChart.setCurrentTime(new Date(newTime));
        window.requestAnimationFrame(playStep);
        }
    }

    function play(shouldPlay) {
        playing = shouldPlay;
        if (playing) {
            playStep();
        }
        var playBtn = $('#play');
        playBtn.toggleClass("active", playing);
        playBtn.find("> span").toggleClass("glyphicon-pause", playing);
        playBtn.find("> span").toggleClass("glyphicon-play", !playing);
    }

    function setTime(milliseconds) {
        timeChart.setCurrentTime(new Date(timeChart.startDate.getTime() + milliseconds));
        timeChartUpdated();
        
    }
    /*
     *== ==========================================================================================================
     *                                      cambio en el mapa
     *============================================================================================================
     */

    function setMapChange() {
        var roadL, arealL, world, googleLayer, selectMap=1;
        var bingTypes = ["satellite", "hybrid", "roadmap", "terrain"];
        $("#cambiarMapa").click(function () {
            var Layers = map.layerTree.children;
            //if(googleMap === false) {
                if(!roadL || !arealL || !googleLayer) {
                    for(var l in Layers) {
                        var id = Layers[l].id;
                        if(id === "RoadBing")
                            roadL = Layers[l];
                        if(id === "AerialBing" || id === "AerialWithLabelsBing")
                            arealL = Layers[l];
                        if(id === "World")
                            world = Layers[l];
                        var label = Layers[l].label;
                        if(label === "Google Maps")
                            googleLayer = Layers[l];
                    }
                }
                var logo = "B";
                if(googleLayer) {
                    selectMap ++;
                    selectMap = selectMap === bingTypes.length? 0: selectMap;
                    googleLayer.mapType = bingTypes[selectMap];
                } else {
                    if(roadL) {
                        roadL.visible = !roadL.visible;
                    }
                    if(arealL) {
                        arealL.visible = !roadL.visible;
                    }
                }
            //} 
        });
    }
    setMapChange();
});


var hacerFit = false;
function filtrarMunicipio(nombre) {
    hacerFit = true;
    for(var id in municipios) {
        municipios[id].filter = function (feature) {
            var name = feature.properties.NOMBRE;
            if(name ===nombre && hacerFit === true) {
                map.mapNavigator.fit({bounds: feature.geometry.bounds, animate: true});
                hacerFit = false;
            }
            
            return true;
        };
    }
}

function borrarFiltro () {
    for(var layer in municipios) {
        municipios[layer].filter = null;
    };
}

function getTime() {
    return actualTime;
}

function getIncidentes() {
    return tablaIncidentes;
}

function getTablaColonias () {
    return tabalColonias;
}
function getTablaMunicipios() {
    return tablaMunicipios;
}
function getTablaEstados() {
    return tablaEstados;
}
