/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


define([
    "recursos/js/Util"
], function (Util) {
    
    /*
     * 
     * @param {Object} features
     * @returns {Array[][]}
     */
    function crearArreglo(features, div) {
        var tabla = new Array(), encavezados = new Array(), valores = new Array();
        var key, i=0, j=0, k=0, x, coordenadas;
        for(key in features[0].properties) {
            encavezados[i] = key;
            i++;
        }
        encavezados[i] = "Longitud";
        encavezados[i+1] = "Latitud";
        tabla[j] = encavezados;
        for(i=0; i<features.length; i++) {
            for(key in features[i].properties) {
                try {
                    valores[k] = features[i].properties[key];
                    /*
                    x = parseFloat(features[i].properties[key]);
                    if(x)
                        valores[k] = x;
                    else
                        */
                }catch (e) {
                    
                }
                k++;
            }
            coordenadas = features[i].geometry.coordinates;
            if(coordenadas.length === 3) {
                valores[k] = coordenadas[0];
                valores[k+1] = coordenadas[1];
            } else {
                valores[k] = coordenadas[0].x;
                valores[k+1] = coordenadas[0].y;
            }
            j++;
            tabla[j]=valores;
            valores = new Array();
            k=0;
        }
        if(div) {
            crearTablaDIV(div, tabla);
        }
        return tabla;
    }
    /*
     * 
     * @param {String} div
     * @param {Array [] []} tabla
     * @returns {undefined}
     */
    function crearTablaToken(div, tabla) {
        var etiqueta = //block, flex
                '<div class="labelDatos" style="display:block !important">' +
                '<table>'+
                '$tabla' +
                '</table>' +
                '</div>';
            var columna = '<td align="center"><b>$dato</b></td >';
            var columnas ='';
            var fila = '<tr><td align="center">Usuario</td >\n\
<td ><select class="selectPanel" id="IDAl">opciones1\n\
</select></td >\n\
<td ><select class="selectPanel" id="IDCo">opciones2\n\
</select</td >\n\
<td ><select class="selectPanel" id="IDCu">opciones3\n\
</select</td >\n\
'+ //<td><button onclick="Funcion" disabled="true">Actualizar</button></td >\n\
'</tr>' ;
        
            var filas= '<tr><td align="center"><b>Usuario (Email)</b></td >\n\
<td align="center"><b>Alerta</b></td >\n\
<td align="center"><b>Confirmacion</b></td >\n\
<td align="center"><b>Cuestionario</b></td >\n\
</tr>', 
                    etiquetaTabla ='', n=tabla.length , m = tabla[0].length;
    
            var datos, nuevaFila ='', opcion1 = '<option value="denegar">Denegar</option>',
                    opcion2 ='<option value="aprobar">Aprobar</option>';
            var al, cu, co;
            for(var i=1; i<n; i++) {
                datos = tabla[i];
                if(datos[1] === "denegar") {
                    al = opcion1 + opcion2;
                } else {
                    al = opcion2.replace("Aprobar", datos[1]) + opcion1;
                }
                if(datos[2] === "denegar") {
                    co = opcion1 + opcion2;
                } else {
                    co = opcion2.replace("Aprobar", datos[2]) + opcion1;
                }
                if(datos[3] === "denegar") {
                    cu = opcion1 + opcion2;
                } else {
                    cu = opcion2.replace("Aprobar", datos[3]) + opcion1;
                }
                nuevaFila = fila.replace("Usuario", datos[0])
                        .replace("opciones1", al)
                        .replace("opciones2", co)
                        .replace("opciones3", cu)
                        .replace("IDAl", "al"+(i-1))
                        .replace("IDCo", "co"+(i-1))
                        .replace("IDCu", "cu"+(i-1));
                
                filas += nuevaFila;
                etiquetaTabla += filas;
                columnas='';
                filas ='';
            }
            etiqueta = etiqueta.replace('$tabla', etiquetaTabla);
            document.getElementById(div).innerHTML=etiqueta;
    }
    
    
    function crearTablaDIV(div, tabla, max) {
        try {
            var etiqueta = //block, flex
                '<div class="labelDatos" style="display:block !important">' +
                '<table>'+
                '$tabla' +
                '</table>' +
                '</div>';
            var columna = '<td align="center"><b>$dato</b></td >';
            var columnas ='';
            var color = '#424949';
            var fila = '\t<tr bgcolor="$Color">$columna</tr>\n' ;
            var filas= '', etiquetaTabla ='', n=tabla.length , m = tabla[0].length;
            if(max) {
                n = n>max? max: n;
            }
            var datos;
            for(var i=0; i<n; i++) {
                datos = tabla[i];
                for(var j=0;j<m;j++) {
                    if(i===0) {
                        columnas += columna.replace('$dato', datos[j]);
                    } else {
                        
                        columnas += columna.replace("$dato", datos[j]);
                    }
                    
                }
                columna = '<td align="center">$dato</td>';
                color = i%2 === 0? "#424949": "";
                filas += fila.replace('$columna', columnas).replace("$Color", color);
                etiquetaTabla += filas;
                columnas='';
                filas ='';
            }
            etiqueta = etiqueta.replace('$tabla', etiquetaTabla);
            document.getElementById(div).innerHTML=etiqueta;
        }catch(e) {
            console.log(e);
        }
    }
    
    
    function crearTablaFolios(div, tabla) {
        var etiqueta = //block, flex
                '<div class="labelDatos" style="display:block !important">' +
                '<table>'+
                '$tabla' +
                '</table>' +
                '</div>';
            var columna = '<td align="center"><b>$dato</b></td>';
            var columnas ='';
            var fila = '<tr>$columnas</tr>';
            var b = '';
            var filas= '', etiquetaTabla ='', n=tabla.length , m = tabla[0].length;
    
            var datos;
            for(var i=0; i<n; i++) {
                datos = tabla[i];
                for(var j=0;j<m;j++) {
                    if(i===0) {
                        columnas += columna.replace('$dato', datos[j]);
                    } else {
                        columnas += columna.replace("$dato", datos[j]);
                    }
                    
                }
                columna = '<td align="center">$dato</td >\n';
                if(i>0) {
                    fila = '<tr>$columnas <td><button id="bFolioIDF" class="buttonTable">Completar</button></td></tr>';
                    filas += fila.replace('$columnas', columnas)
                        .replace("IDF",i);
                } else {
                    filas += fila.replace('$columnas', columnas);
                }
                etiquetaTabla += filas;
                columnas='';
                filas ='';
            }
            etiqueta = etiqueta.replace('$tabla', etiquetaTabla);
            document.getElementById(div).innerHTML=etiqueta;
    }
    
    function crearTablaColaboradores(div, datos) {
        var tabla = [["CURP","Nombres", "Apellidos","Apodo"]], i=1;        
        for(var curp in datos) {
            var nombres = datos[curp].nombres;
            var apellidos = datos[curp].paterno +" "+datos[curp].materno;
            var apodo = datos[curp].nickname|| " ";
            tabla[i] = [curp, nombres, apellidos, apodo];
            i++;
        }
        crearTablaDIV(div, tabla);
    }
    
    function crearTablaDatosExtras(div, tablaDatos, max) {
        var tabla = [["Empresa","Fecha", "Lugar","Piloto","Copiloto","Placa","Tipo de carga"]], i=1;        
        for(i=1; i<tablaDatos.length; i++) {
            var empresa = tablaDatos[i][Util.getIdArray("empresa", tablaDatos[0])] || "";
            var municipio = tablaDatos[i][Util.getIdArray("municipio", tablaDatos[0])]|| "";
            var piloto = tablaDatos[i][Util.getIdArray("curp", tablaDatos[0])]|| "";
            var copiloto = tablaDatos[i][Util.getIdArray("copiloto", tablaDatos[0])]|| "";
            var placa = tablaDatos[i][Util.getIdArray("placas", tablaDatos[0])]|| "";
            var tipo = tablaDatos[i][Util.getIdArray("carga", tablaDatos[0])]|| "";
            var hora = tablaDatos[i][Util.getIdArray("hora", tablaDatos[0])]|| "";
            var fecha = tablaDatos[i][Util.getIdArray("fecha", tablaDatos[0])]|| "";
            tabla[i] = [empresa, fecha+" "+hora, municipio, piloto, copiloto, placa, tipo];
        }
        crearTablaDIV(div, tabla, 100);
    }
    
    return {
        
        crearArreglo: crearArreglo,
        crearTablaToken:crearTablaToken,
        crearTablaDIV: crearTablaDIV,
        crearTablaFolios: crearTablaFolios,
        crearTablaColaboradores: crearTablaColaboradores,
        crearTablaDatosExtras: crearTablaDatosExtras
    };

});