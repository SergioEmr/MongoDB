var map, actualTime;

define([
    "recursos/js/MapFactory",
    "recursos/js/GoogleMap",
    "recursos/js/LayerFactory",
    "dojo/dom",
    "dojo/on",
    "recursos/js/Shapes",
    "luciad/shape/ShapeFactory",
    
    "samplecommon/dojo/DojoMap",
    "luciad/reference/ReferenceProvider",
    "samplecommon/LayerConfigUtil",
    "./painters/layerPainter",
    "./painters/MunicipiosPainter",
    "./DefaultBalloon",
    "recursos/js/Util",
    'luciad/util/Promise',
    
    "recursos/js/TimeChart",
    "recursos/js/Graficas/Graficas",
    "./tablas",
    "./painters/EstadosPainter",
    "./painters/MunicipiosPainter2",
    "template/sampleReady!"
], function (MapFactory, GoogleMap, LayerFactory, dom, on, Shapes, ShapeFactory,
        DojoMap, ReferenceProvider, LayerConfigUtil, layerPainter, MunicipiosPainter, DefaultBalloon, util, Promise,
        TimeChart, Graficas, tablas, EstadosPainter, MunicipiosPainter2) {
    
    var referenceC = ReferenceProvider.getReference("urn:ogc:def:crs:OGC::CRS84");
    var referenceE = ReferenceProvider.getReference("EPSG:4978");
    var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i
            .test(navigator.userAgent);
    var googleMap = false;
    
    var municipiosName = [], nEstados = 0, intentos = 0;
    /*
     * Funcion encargada de crear el mala dependiendo de si esta en un dispositivo mobil o no
     * creara el mapa en 3d o 2d, ademas de si esta activado e mapa de google 
     * 
     * Google: satellite, hybrid, roadmap o terrain
     * BingMap: AerialWithLabels, Road o Aerial
    */
    Start();
    function Start() {
        if(googleMap)
            map = GoogleMap.crearMapaGoogle("map", "roadmap"); 
        else {
            if(isMobile)
                map = MapFactory.makeMap3d("map", {reference: referenceC}, {
                    includeBackground: false, includeElevation: false, includeLayerControl: true,
                    includeMouseLocation: false, includeBingLayer: "Aerial"});
            else
                map = MapFactory.makeMap3d("map", {reference: referenceE}, {
                    includeBackground: false, includeElevation: false, includeLayerControl: true,
                    includeMouseLocation: false, includeBingLayer: "Aerial"});
        } 
        CrearCapas();
    }
    //=================================== CAPAS ===============================================
    var estados15, estados16, estados17, municipios15, baseEstados, municipiosMorelos, municipiosVeracruz;
    var Features;
    function CrearCapas() {
        DojoMap.configure(map);
        LayerConfigUtil.addLonLatGridLayer(map);
        
        estados15 = LayerFactory.createMemoryLayer(referenceC, 
            {label: "Estados 2015", id: "estados5", selectable: true, editable: false, painter: new EstadosPainter()});
        map.layerTree.addChild(estados15);
        datosEstados(estados15, 2015);
        estados16 = LayerFactory.createMemoryLayer(referenceC, 
            {label: "Estados 2016", id: "estados6", selectable: true, editable: false, painter: new EstadosPainter()});
        map.layerTree.addChild(estados16);
        datosEstados(estados16, 2016);
        estados17 = LayerFactory.createMemoryLayer(referenceC, 
            {label: "Estados 2017", id: "estados7", selectable: true, editable: false, painter: new EstadosPainter()});
        map.layerTree.addChild(estados17);
        datosEstados(estados17, 2017);
        
        municipios15 = LayerFactory.createMemoryLayer(referenceC,
        {label: "Veracruz 2015", selectable: true, editable: false, painter: new MunicipiosPainter()});
        //map.layerTree.addChild(municipios15);
        //addProperties(municipios15, 2015);
        
        municipiosVeracruz = LayerFactory.createMemoryLayer(referenceC, {
            label: "Veracruz",  selectable: true, editable: true, painter: new MunicipiosPainter2() 
        });
        map.layerTree.addChild(municipiosVeracruz);
        createProperties(municipiosVeracruz, "data/municipiosVeracruz.json", "filtroVeracruz");
        
        municipiosMorelos = LayerFactory.createMemoryLayer(referenceC, {
            label: "Morelos", selectable: true, editable: true, painter: new MunicipiosPainter2() 
        });
        map.layerTree.addChild(municipiosMorelos);
        createProperties(municipiosMorelos, "../recursos/json/Morelos2.json", "filtroMorelos");
        
        
        var queryFinishedHandle = estados15.workingSet.on("QueryFinished", function() {
            if(estados15.bounds) 
                map.mapNavigator.fit({bounds: estados15.bounds, animate: true});
            else {
                var coordenadas = [-96.94186764996043,0, 16.8100862892485, 5.2];
                util.fitCoordinates(map, coordenadas);
            }
            queryFinishedHandle.remove();
        });
    }
    var ti = 0, p = false;
    /* 
     * =============================================================================================================
     *                                          FUNCION PARA EL RELOJ   
     * @returns {undefined}
     * =============================================================================================================
     */
    //timeUpdated();
    function timeUpdated() {
        ti++;
        if(ti >= 10 && p ===false) {
            p= true;
            addProperties(municipiosVeracruz);
        }
        $('#timelabel').text(util.formatDate() +" "+util.formatTime());
        setTimeout(timeUpdated,1000);
     }
     
     
     function createProperties(layer, url, filtrodiv) {
         var idEstado = nEstados, nombreEstado = layer.label;
         nEstados ++;
         municipiosName[idEstado] = [];
         $.ajax({
            type:'Get',
            dataType: "json",
            //url: "../recursos/json/Morelos2.json"
            url:url // "data/Municipio.geojson"
        }).done(function(data) {
            intentos=0;
            var features = data.features;
            var index = features.length;
            var delitos = ["Amenazas","Homicidio","Narcomenudeo","Robo", "Secuestro","Trata de personas","Violencia familiar"];
            var year = 2015, id=0, properties={}, feature;
            var nMunicipios=0;
            for(var y=0; y<3; y++) {
            for(var i=0; i<index; i++) {
                var name = "";
                feature = features[i];
                name = feature.properties.name? feature.properties.name: feature.properties.NOMGEO? feature.properties.NOMGEO: "";
                properties["name"] = name;
                if(y===0) {
                    if(isNaN(name)) {
                        municipiosName[idEstado][nMunicipios] = name;
                        nMunicipios++;
                    }
                }
                var coordenates = feature.geometry.coordinates;
                for(var j=0; j<delitos.length; j++) {
                    var values = [];
                    for(var k=0;k<12;k++) {
                        values[k] = Math.round(util.getRandom(0, 100));
                    }
                    properties[delitos[j]] = values;
                    properties["year"] = year;
                }
                try {
                    feature = Shapes.createPolygonCoordinates(referenceC, coordenates, id, properties);
                    layer.model.put(feature);
                }catch(e) {
                    console.log("Error al crear poligono, id: "+id+" Coordenadas: ");
                    console.log(coordenates);
                    console.log(e);
                }
                feature = null;
                properties = {};
                //Features[i] = feature;
                id++;
            }
            year++;
            }
            Graficas.setOptions(filtrodiv, municipiosName[idEstado]);
         }).fail(function(e) {
             intentos++;
             console.log("intento "+intentos+" de cargar "+url);
             if(intentos < 5) {
                 createProperties(layer, url, filtrodiv);
             } else 
                console.log(e);
        });
     }
     function addProperties(layer, year) {
         $.ajax({
            type:'Get',
            dataType: "json",
            url: "data/Municipio.geojson"
        }).done(function(data) {
            Features = data.features;
            var index = Features.length;
            
            $.ajax({
                type:'Get',
                dataType: "json",
                url: "data/Veracruz"+year+".json"
            }).done(function(data) {
                console.log("Archivo leido");
                var datos = data.features;
                var nDatos = datos.length, nDelitos=0, nClaves=0;;
                var claves = [];
                var delitos = [];
                var meses = [0,0,0,0,0,0,0,0,0,0,0,0];
                for(var i=0;i<nDatos; i++) {
                    var properties = datos[i].properties;
                    var clave = properties["Cve. Municipio"];
                    var c, d="";
                    for(var k=2; k<clave.length; k++) {
                        c = clave.charAt(k);
                        if(c !== '.')
                            d +=c;
                        else
                            break;
                    }
                    clave = parseInt(d);
                    var nuevaClave = true;
                    for(k=0; k<claves.length; k++) {
                        if(claves[k] === clave) {
                            nuevaClave = false;
                            break;
                        }
                    }
                    if(nuevaClave===true) {
                        claves[nClaves] = clave;
                        //Properties["Clave"] = clave;
                    }
                    var delito = properties["Delito"];
                    var nuevoDelito = true;
                    for(k=0; k<delitos.length; k++) {
                        if(delitos[k] === delito) {
                            nuevoDelito = false;
                            break;
                        }
                    }
                    if(nuevoDelito===true) {
                        delitos[nDelitos] = delito;
                        nDelitos++;
                        //Properties["delito"] = [];
                    }
                    var keys = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
                    for(var i=0; i<keys.length; i++)  {
                        meses[i] += properties[keys[i]];
                    }
                    
                    for(var j=0;j<index; j++) {
                        var municipio = Features[j].properties.CVE_MUN;
                        if(clave === municipio) {
                            break;
                        }
                    }
                    try {
                        var coordinates = Features[j].geometry.coordinates;
                        //properties = datos[i].properties;
                        properties["year"] = year;
                        var newFeature = Shapes.createPolygonCoordinates(referenceC, coordinates, i, properties);
                        layer.model.add(newFeature);
                    }catch(e) {
                        console.log(e);
                    }
                }
            });
            //layer.visible = false;
         }).fail(function(e) {
            console.log(e);
        });
     }
     function datosEstados(layer, year){
         $.ajax({
            type:'Get',
            dataType: "json",
            url: "../recursos/estados.json"
        }).done(function(data) {
            var features = data.features;
            var nFeatures = features.length;
            $.ajax({
                type:'Get',
                dataType: "json",
                url: "data/Nacional"+year+".json"
            }).done(function(data) {
                var datos = data.features;
                var nDatos = datos.length;
                var newFeature, coordinates, properties;
                for(var i=0; i<nDatos; i++) {
                    var id = (parseInt(datos[i].id)+1)+"";
                    id = id<10? "0"+id: id;
                    for(var j=0; j<nFeatures; j++) {
                        var featureName = features[j].properties.name;
                        if(featureName === id) {
                            break;
                        }
                    }
                    coordinates = features[j].geometry.coordinates;
                    if(!coordinates){
                        var f = features[j];
                        var g = f.geometry;
                        if(g.geometries.length>1) {
                            g = g.geometries;
                            var mayor = g[0].coordinates[0].length; 
                            var l=0;
                            for(var k=1;k<g.length; k++) {
                                var n = g[k].coordinates[0].length;
                                if(n > mayor) {
                                    mayor = n;
                                    l=k;
                                }
                            }
                            coordinates = g[l].coordinates;
                        } else {
                            coordinates = g.geometries[0].coordinates;
                        }
                        //alert("Geometria desconosida");
                    }
                    properties = datos[i].properties;
                    properties["year"] = year;
                    newFeature = Shapes.createPolygonCoordinates(referenceC, coordinates, id, properties);
                    layer.model.add(newFeature);
                }
            });
        }).fail(function(e) {
            console.log(e);
        });
     }
     /*
     *============================================================================================================
     *                                      SELECCION EN EL MAPA
     *============================================================================================================
     */
    var selectedFeature, selectedChart = null;
    map.on("SelectionChanged", function (event) {
        selectedFeature = null;
        if (event.selectionChanges.length > 0 && event.selectionChanges[0].selected.length > 0 ) {
            selectedFeature = event.selectionChanges[0].selected[0];
            $("#BalloonPanel").fadeIn("slow");
            //var content = DefaultBalloon.getBalloon(selectedFeature);
            var content = '<label>'+selectedFeature.properties.name+'</label> \n\
<div id="graficaContenido"></div>';
            document.getElementById("contentBalloon").innerHTML = content;
            var datos = tablas.tablaActual(selectedFeature, getDetailTime());
            if(datos.sumaTotal ===0) {
                document.getElementById("contentBalloon").innerHTML += "<p>No ocurrieron delitos en este periodo</p>";
            }else 
                selectedChart = Graficas.crearGraficaTabla("graficaContenido", datos.tabla, "piechart", "Delitos");
        } else {
            $("#BalloonPanel").fadeOut("slow");
            selectedChart = null;
        }
    });
    $('#cerrarBalloon').click(function () {
        $('#BalloonPanel').fadeOut('slow');
        selectedChart = null;
    });
    
    var mVisibles = true;
    $('#verMunicipios').click(function () {
        mVisibles = !mVisibles;
        municipiosVeracruz.visible = mVisibles;
        municipiosMorelos.visible = mVisibles;
        estados15.visible = !mVisibles;
        estados16.visible = !mVisibles;
        estados17.visible = !mVisibles;
        if(mVisibles) {
            var coordenadas = [-96.94186764996043,0, 16.8100862892485, 5.2];
            util.fitCoordinates(map, coordenadas, true);
        }
        else {
            var queryFinishedHandle = estados15.workingSet.on("QueryFinished", function() {
                if(estados15.bounds) 
                    map.mapNavigator.fit({bounds: estados15.bounds, animate: true});
                queryFinishedHandle.remove();
            });
        }
        var mensaje = mVisibles? "Ver Estados": "Ver Municipios";
        document.getElementById("verMunicipios").innerHTML = mensaje;
    });
     
     /*
     * 
     * @type TimeChart
     */
    var dataSetStartTime = Date.parse("2015/01/01 00:00:00") / 1000;//1421143477; // Tue Jan 13 11:04:37 CET 2015
    var dataSetEndTime = Date.parse("2018/01/01 00:00:00") / 1000;
    var speedup = 1000000; //speedup
    var lasttime = Date.now();
    var playing = false;
    var timeChart = new TimeChart(dom.byId("timeChart"), new Date(dataSetStartTime * 1000), new Date(dataSetEndTime * 1000));
    wireListeners();
    setTime(0);
    
    function timeUpdated() {
        actualTime = timeChart.getCurrentTime().getTime();
        $('#timelabel').text(util.formatDate(actualTime));
        municipiosVeracruz.filter = function (feature) {
            return true;
        };
        municipiosMorelos.filter = function (feature) {
            return true;
        };
        estados15.filter = function (feature) {
            return true;
        };
        estados16.filter = function (feature) {
            return true;
        };
        estados17.filter = function (feature) {
            return true;
        };
        if(selectedChart && selectedFeature) {
            var nuevaTabla = tablas.tablaActual(selectedFeature, getDetailTime());
            if(nuevaTabla.sumaTotal ===0)
                document.getElementById("contentBalloon").innerHTML += "<p>No ocurrieron delitos en este periodo</p>";
            else 
                Graficas.actualizarGrafica(selectedChart.idGrafica, nuevaTabla.tabla, "piechart");
        }
     }
     
     function wireListeners() {
    //timechart
        timeChart.addInteractionListener(timeUpdated);
        timeChart.addZoomListener(function(currZoomLevel, prevZoomLevel) {
            speedup *= prevZoomLevel / currZoomLevel;
        });

        $("#play").click(function() {
            lasttime = Date.now();
            play(!playing);
        });

        //expose navigateTo on window so it can be called from index.html
        window.navigateTo = function(moveTo) {
            var wgs84 = ReferenceProvider.getReference("EPSG:4326");
            var center = ShapeFactory.createPoint(wgs84, [moveTo.lon, moveTo.lat]);
            var radius = moveTo.radius || 5.0;
            var bounds = ShapeFactory.createBounds(wgs84, [center.x - radius, 2 * radius, center.y - radius, 2 * radius]);
            return map.mapNavigator.fit(bounds);
        };
    }
    
    function playStep() {
        if (playing) {
            var currTime = Date.now();
            var deltaTime = (currTime - lasttime);
            lasttime = currTime;
            var timeChartTime = timeChart.getCurrentTime().getTime();
            var newTime = timeChartTime + (deltaTime) * speedup;
            if (newTime >= timeChart.endDate.getTime()) {
            newTime = timeChart.startDate.getTime();
        }
        timeChart.setCurrentTime(new Date(newTime));
        window.requestAnimationFrame(playStep);
        }
    }

    function play(shouldPlay) {
        playing = shouldPlay;
        if (playing) {
            playStep();
        }
        var playBtn = $('#play');
        playBtn.toggleClass("active", playing);
        playBtn.find("> span").toggleClass("glyphicon-pause", playing);
        playBtn.find("> span").toggleClass("glyphicon-play", !playing);
    }

    function setTime(milliseconds) {
        timeChart.setCurrentTime(new Date(timeChart.startDate.getTime() + milliseconds));
        timeUpdated();
        
    }
    
    /*
     *============================================================================================================
     *                                      CREACION DE GRAFICAS
     *============================================================================================================
     */
    var fil = ["filtroDelitos", "filtroMunicipios"], lab =["DelitoA","DelitoB","DelitoC","DelitoD", "DelitoE","DelitoF","DelitoG"];
    var titles = ["Empresas","Estados","Motivo del Incidentes"];
    var types = ["piechart",  "piechart", "piechart"];
    var gEmpresas={}, gNombres={}, gMotivo={};
    function crearGraficas(features) {
        var datos = Graficas.crearGraficasFeatures(features, "Graficas", lab, types, fil, null, titles);
        var graficas = datos.graficas;
        tablaDatos = datos.tablaDatos;
        datos = null;
        return graficas;
        
    }
    
    /*
     *============================================================================================================
     *                                      FILTROS
     *============================================================================================================
     */
    var filterV = document.getElementById("filtroVeracruz");
    var makeFit = false;
    filterV.addEventListener("change", function () {
        var selection = filterV.selectedOptions[0].innerHTML;
        makeFit = true;
        municipiosVeracruz.filter = function (feature) {
            var name = feature.properties.name;
            if(selection === "Todos") {
                makeFit = false;
                return true;
            }
            if(name === selection) {
                if(feature.geometry.bounds && makeFit ===true) {
                    map.mapNavigator.fit({bounds: feature.geometry.bounds, animate: true});
                    makeFit = false;
                }
                return true;
            } else {
                false;
            }
        };
    });
    var filterM = document.getElementById("filtroMorelos");
    var makeFit2 = false;
    filterM.addEventListener("change", function () {
        var selection = filterM.selectedOptions[0].innerHTML;
        makeFit2 = true;
        municipiosMorelos.filter = function (feature) {
            var name = feature.properties.name;
            if(selection === "Todos") {
                makeFit2 = false;
                return true;
            }
            if(name === selection) {
                if(feature.geometry.bounds && makeFit2 ===true) {
                    map.mapNavigator.fit({bounds: feature.geometry.bounds, animate: true});
                    makeFit2 = false;
                }
                return true;
            } else {
                false;
            }
        };
    });
    
});


function getTime() {
    return actualTime;
}

function getDetailTime() {
    var date = new Date(actualTime);
    var m = date.getMonth();
    var y = date.getFullYear();
    return {month: m, year: y};
}

