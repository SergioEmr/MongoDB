/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define([
], function () {
    
    /*
     * 
     * @param {Object} features
     * @returns {Array[][]}
     */
    function crearArreglo(features, div, options) {
        var tabla = new Array(), encabezados = new Array(), valores = new Array();
        var key, i=0, j=0, k=0, x, coordenadas;
        for(key in features[0].properties) {
            if(typeof features[0].properties === "object") {
                encabezados[i] = key;
                //properties[i] = 
                i++;
            }
        }
        
        tabla[j] = encabezados;
        for(i=0; i<features.length; i++) {
            var p = features[i].properties;
            for(i=0; i<encabezados.length; i++) {
                valores[k] = p[encabezados[i]];
                k++;
            }
            j++;
            tabla[j]=valores;
            valores = new Array();
            k=0;
        }
        if(div) {
            crearTablaDIV(div, tabla, options);
        }
        return tabla;
    }
    
    function tablaActual(feature, time) {
        var year = feature.properties.year;
        var tabla = [], encabezados=[], i=0, n=0;
        var suma = 0;
        if(time.year === year) {
            for(var key in feature.properties) {
                if(typeof feature.properties[key] === "object") {
                    encabezados[n] = key;
                    n++;
                }
            }
            var mes = time.month, j=0;
            tabla[j] = ["Delitos", "Cantidad"];
            var properties = feature.properties;
            for(i=0; i<n; i++) {
                var valores = properties[encabezados[i]];
                j++;
                tabla[j] = [encabezados[i], valores[mes]];
                suma += valores[mes];
            }
        }
        return {
            tabla: tabla,
            sumaTotal: suma
        };
    }
    
    return {
        crearArreglo: crearArreglo,
        tablaActual: tablaActual
    };
});
