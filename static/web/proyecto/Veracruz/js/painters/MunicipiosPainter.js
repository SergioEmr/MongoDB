/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define(["luciad/view/feature/FeaturePainter",
        "luciad/shape/ShapeFactory",
        'samplecommon/IconFactory',
        "luciad/view/style/PointLabelPosition"
    ], function (FeaturePainter, ShapeFactory, IconFactory, PointLabelPosition) {
    
    function layerPainter() {
    } 
    layerPainter.prototype = new FeaturePainter();
    layerPainter.prototype.constructor = layerPainter;
    layerPainter.prototype.getDetailLevelScales = function() {
        return [1 / 10000000, 1 / 3000000, 1 / 2000000, 1 / 1000000,1 / 500000,1 /90000, 1 / 30000];
    };
    
    var sBlanco = 'rgb(255, 255, 255)';
    var sGrisOscuro = 'rgb(50, 50, 50)';
    var sGrisClaro = 'rgb(200, 200, 200)', GrisClaro = 'rgba(200, 200, 200, 0.5)';
    var sRojo = 'rgb(255, 0, 0)', Rojo = 'rgba(255, 0, 0, 0.5)';
    var sAmarilloClaro = 'rgb(255, 255, 204)';
    var sGris = 'rgb(230, 230, 230)', Gris = "rgba(230,230,230, 0.5)";
    var sNaranjaClaro = 'rgb(255, 239, 204)';
    var sNaranja = 'rgb(255, 174, 102)', Naranja = 'rgba(255, 174, 102, 0.5)';
    var Verde = "rgba(50,230,50,0.5)", sVerde = "rgb(50,230,50)";
    var sMorado ="rgb(200,0,200)", Morado = "rgba(200,0,200, 0.5)";
    var Azul = "rgba(70, 100, 230, 0.5)", sAzul = "rgb(70, 100, 230)";
    var VerdeClaro = "rgba( 134, 255, 132 , 0.5)", sVerdeClaro = "rgb( 134, 255, 132 )";
    var maxRadio = 0, minRadio = 1;
    
    var promedio = 0;
    layerPainter.prototype.paintBody = function (geoCanvas, feature, shape, layer, map, state) {
        
        var time = getDetailTime();
        var year = feature.properties.year;
        
        if(time.year === year) {
            var color = sAzul, sColor = sAzul, mes = time.month;
            
            var total = 0, i=0, level = 0;
            var properties = feature.properties;
            for(var key in properties) {
                if(typeof properties[key] === "object") {
                    var valores = properties[key];
                    total += valores[mes];
                    i++;
                }
            }
            total = total / i;
            promedio = Math.round(total);
            if(total <=10) {
                color = "rgb(69, 250, 7)";
                level = 0;
            } else {
                if(total <= 20) {
                    color = "rgb(139, 229, 54)";
                    level = 1;
                } else {
                    if(total <= 30) {
                        color = "rgb(198, 228, 54)";
                        level = 2;
                    } else {
                        if(total <=40) {
                            color = "rgb(215, 216, 36)";
                            level = 3;
                        } else {
                            if(total <=50) {
                                color = "rgb(252, 214, 1)";
                                level = 4;
                            } else {
                                if(total <=60) {
                                    color = "rgb(244, 170, 45)";
                                    level = 5;
                                } else {
                                    if(total <=70) {
                                        color = "rgb(247, 144, 42)";
                                        level = 6;
                                    } else {
                                        if(total <80) {
                                            color = "rgb(250, 98, 18)";
                                            level = 7;
                                        } else { 
                                            color = "rgb(251, 64, 6)";
                                            level = 8;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            var shape3d, height=1000 * level;
            
            if(height >0) {
                shape3d = ShapeFactory.createExtrudedShape(
                        shape.reference, shape, 0, height);
            }

            if(!feature.geometry.x) {

                geoCanvas.drawShape(shape3d? shape3d : shape,{ 
                    fill: {color: color},
                    stroke: {
                        color: state.selected ? sAzul : sBlanco,
                        width: 2} 
                });

            }
            else {
                var icon, dimension= 14;
                if(state.selected) 
                    icon = IconFactory.circle({stroke: sBlanco, fill: sRojo, width: dimension, height: dimension});
                else 
                    icon  = IconFactory.circle({stroke: sBlanco, fill: color, width: dimension, height: dimension});
                if(state.level >3) {
                    geoCanvas.drawIcon(shape.focusPoint,{
                        width: dimension+"px",
                        height: dimension+"px",
                        image: icon
                    });
                }
            }
        }
    };
    
    
    layerPainter.prototype.paintLabel = function (labelCanvas, feature, shape, layer, map, state) {
        var label, labelName = "", i=0, properties = feature.properties;
        
        if(properties) {
            if(properties.Folio_pred) {
                labelName = properties.Folio_pred;
            }
            else {
            for(var key in properties) {
                if(i===0)
                    labelName = properties[key];
                i++;
            } 
            }
        } else {
            labelName = feature.id;
        }
        var labelStyle = {
          positions: (PointLabelPosition.NORTH),
          offset: 5,
          priority : 2
        };
        //label = "<span style='color: $color' class='label'>" + labelName + "</span>";
        label  = '<div class="labelwrapper">' +
                                '<div class="sensorLabel blueColorLabel">' +
                                '<div class="theader">' +
                                '<div class="leftTick blueColorTick"></div>' +
                                '<div class="rightTick blueColorTick"></div>' +
                                '<div class="name">'+labelName+'</div>' +
                                '</div>' +
                                '<div class="type">Promedio : '+promedio+'</div>' +
                                '</div>' +
                                '</div>';
           
        labelCanvas.drawLabel(label,shape.focusPoint, labelStyle);
        if(state.level > 3) {     
            labelCanvas.drawLabel(label,shape.focusPoint, labelStyle);
        }
    };
 
    return layerPainter;
});

