/**
 * A navigable time line chart.
 *
 * It uses D3 to draw time ticks allow user interaction (panning, zooming).
 */
define(["samples/lib/d3/d3.min"], function(d3) {

  function TimeChart(domElement, startDate, endDate, customFormat) {
    //margin conventions: https://gist.github.com/mbostock/3019563
    this.parent = domElement;
    this.outerWidth = $(domElement).innerWidth();
    this.outerHeight = 34;
    this.margin = {top: 0, right: 10, bottom: 0, left: 10};

    this.width = this.outerWidth - this.margin.left - this.margin.right;
    this.height = this.outerHeight - this.margin.top - this.margin.bottom;

    this.startDate = startDate;
    this.endDate = endDate;

    var self = this;
    var format = d3.time.format.multi([
      ["", function(d) {
        return (d.getTime() < self.startDate.getTime() || d.getTime() > self.endDate.getTime())
      }], //don't show time labels for values outside the dataset
      [".%L", function(d) {
        return d.getMilliseconds();
      }],
      [":%S", function(d) {
        return d.getSeconds();
      }],
      ["%H:%M", function(d) {
        return d.getMinutes();
      }],
      ["%H:%M", function(d) {
        return d.getHours();
      }],
      ["%Y-%m-%d", function(d) {
        return d.getDay() && d.getDate() != 1;
      }],
      ["%b %d", function(d) {
        return d.getDate() != 1;
      }],
      ["%B", function(d) {
        return d.getMonth();
      }],
      ["%Y", function() {
        return true;
      }]
    ]);
    if(customFormat) {
    format = function (time) {
        var date = time? new Date(time) : new Date();
        var y = date.getFullYear();
        var mo = (date.getMonth()+1)<10? "0"+(date.getMonth()+1): (date.getMonth()+1);
        var d = date.getDate() <10? "0"+date.getDate(): date.getDate();
        switch(customFormat) {
            default: return mo+"/"+d+"/"+y;
            case "hh:mm": return formatTime(time);
            case "mm/dd/aaaa": return mo+"/"+d+"/"+y;
            case "ddmmaaa": return d+mo+y+"";
            case "dd/mm/aaaa": return d+"/"+mo+"/"+y;
            case "dd/mm/aaaa hh:mm": return d+"/"+mo+"/"+y+" "+formatTime(time);
            case "dd-mm-aaaa": return d+"-"+mo+"-"+y;
            case "mm/aaaa": return mo+"/"+y;
            case "dd/mm": return d+"/"+mo;
            case "dd-mm-aaaaThh:mm":return d+"-"+mo+"-"+y+"T"+formatTime(time);
        }
        return  mo+"/"+d+"/"+y;
            console.log(time);
        };
        function formatTime(time) {
            var date = time? new Date(time) : new Date();
            var h = date.getHours() <10 ? "0"+date.getHours(): date.getHours();
            var m = date.getMinutes() <10 ? "0"+date.getMinutes(): date.getMinutes();
            return h+":"+m;
        }
    }
    var timespanInMillis = this.endDate.getTime() - this.startDate.getTime();
    this.xExtent = [
      new Date(this.startDate.getTime() - (timespanInMillis / 2)),
      new Date(this.endDate.getTime() + (timespanInMillis / 2))
    ];

    this.currentTime = this.startDate;

    this.x = d3.time.scale.utc()
        .domain([this.xExtent[0], new Date(this.startDate.getTime() + (timespanInMillis / 2))])
        .range([0, this.width]);

    this.zoom = d3.behavior.zoom()
        .x(this.x)
        .xExtent(this.xExtent)
        .scaleExtent([1, 10])
        .on("zoom", zoom);

    this.svgParent = d3.select(domElement).append("svg")
        .attr("width", this.outerWidth)
        .attr("height", this.outerHeight)
        .attr("class", "chart")
        .call(this.zoom);
    this.svg = this.svgParent
        .append("g")
        .attr("transform", "translate(" + this.margin.left + ", " + this.margin.top + ")");
    this.xAxis = d3.svg.axis()
        .scale(this.x)
        .orient("top")
        .ticks(this.width / 100)
        .tickFormat(format)
        .tickSize(10);
    this.svg.append("g")
        .attr('class', 'x axis')
        .attr('transform', 'translate(0, ' + this.height + ')')
        .call(this.xAxis);
    this.belowBounds = this.svgParent.append("rect")
        .attr('class', 'outofbounds below')
        .attr("x", 0)
        .attr('y', 0)
        .attr('height', this.outerHeight)
        .attr('width', Math.max(0, this.x(this.startDate) + this.margin.left - 1));
    this.aboveBounds = this.svgParent.append("rect")
        .attr('class', 'outofbounds above')
        .attr("x", Math.min(this.outerWidth, this.x(this.endDate) + this.margin.left + 1))
        .attr('y', 0)
        .attr('height', this.outerHeight)
        .attr('width', Math.max(0, this.outerWidth - this.x(this.endDate) + this.margin.left - 1));
    this.svg.append("line")
        .attr("x1", this.width / 2)
        .attr("y1", this.height)
        .attr("x2", this.width / 2)
        .attr("y2", 0)
        .attr('class', 'marker');
    d3.select(window).on('resize', resize.bind(this));
    this.resize = resize.bind(this);

    function draw() {
      self.svg.select("g.x.axis").call(self.xAxis);
      self.belowBounds
          .attr("x", 0)
          .attr('y', 0)
          .attr('height', self.outerHeight)
          .attr('width', Math.max(0, self.x(self.startDate) + self.margin.left - 1));
      self.aboveBounds
          .attr("x", Math.min(self.outerWidth, self.x(self.endDate) + self.margin.left + 1))
          .attr('y', 0)
          .attr('height', self.outerHeight)
          .attr('width', Math.max(0, self.outerWidth - self.x(self.endDate) + self.margin.left - 1));
    }

    function resize() {
      self.outerWidth = $(self.parent).width();
      self.outerHeight = 30;

      self.width = self.outerWidth - self.margin.left - self.margin.right;
      self.height = self.outerHeight - self.margin.top - self.margin.bottom;

      d3.select(self.parent).select(".chart")
          .attr("width", self.outerWidth)
          .attr("height", self.outerHeight);

      self.x.range([0, self.width]);
      self.zoom.x(self.x);

      self.svg.select("g.x.axis").attr('transform', 'translate(0, ' + self.height + ')');

      self.svg.select("line.marker")
          .attr("x1", self.width / 2)
          .attr("y1", self.height)
          .attr("x2", self.width / 2)
          .attr("y2", 0);
      draw();
    }

    function update() {
      draw();
      interactionListeners.forEach(function(interactionListener) {
        interactionListener(self);
      });
    }

    var prevScale = 1;

    function zoom() {
      var timespanInMillis = self.endDate.getTime() - self.startDate.getTime();
      self.xExtent = [
        new Date(self.startDate.getTime() - (timespanInMillis / (2 * self.zoom.scale()))),
        new Date(self.endDate.getTime() + (timespanInMillis / (2 * self.zoom.scale())))
      ];
      self.zoom.xExtent(self.xExtent);
      if (self.zoom.scale() == prevScale) {
        //pan: set time to center of chart
        var newTime = self.x.invert(self.width / 2);
        self.setCurrentTime(newTime);
      } else {
        //zoom: keep current time, but fix the chart translation
        self.zoom.center([self.width / 2, 0])
        zoomListeners.forEach(function(zoomListener) {
          zoomListener(self.zoom.scale(), prevScale);
        });
      }
      update();
      prevScale = self.zoom.scale();

    }

    this.getCurrentTime = function() {
      return this.currentTime;
    };

    this.getFormattedCurrentTime = function() {
      return d3.time.format("%H:%M")(this.currentTime);
    };

    /**
     * Set the current time.
     * The current time will be under the marker.
     * No zooming will be done, only translation.
     * Time will be clamped between this.startDate and this.endDate
     * @param a JS date object
     */
    this.setCurrentTime = function(time) {
      time = Math.max(this.startDate.getTime(), Math.min(this.endDate.getTime(), time.getTime())); //clamp
      var newTime = new Date(time);
      var newX = this.x(newTime);
      var oldX = this.x(this.currentTime); //x-value of currTime
      var dX = oldX - newX;
      var oldTrans = this.zoom.translate();
      this.zoom.translate([oldTrans[0] + dX, oldTrans[1]]);
      this.currentTime = newTime;
      update();
    };

    this.setCurrentTimeWithZoom = function(time, trackStart, trackEnd){
      this.x.domain([new Date(trackStart),new Date(trackEnd)]);
      this.setCurrentTime(time);
    };

    var interactionListeners = [];
    this.addInteractionListener = function(interactionListener) {
      interactionListeners.push(interactionListener);
    };

    var zoomListeners = [];
    this.addZoomListener = function(zoomListener) {
      zoomListeners.push(zoomListener);
    };

  }

  return TimeChart;
});