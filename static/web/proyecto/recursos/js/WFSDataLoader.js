define([
  "luciad/reference/ReferenceProvider",
  "luciad/view/feature/FeatureLayer",
  "luciad/model/feature/FeatureModel",
  "luciad/model/store/WFSFeatureStore",
  "luciad/model/codec/GeoJsonCodec",
  "luciad/uom/UnitOfMeasureRegistry"
], function(ReferenceProvider, FeatureLayer, FeatureModel, WFSFeatureStore, GeoJsonCodec, UnitOfMeasureRegistry) {

  return {

    /**
     * Creates a layer for WFS data.
     *
     * You can style WFS data using a FeaturePainter.
     * See for example the <i>Vector Data<i> sample.
     *
     * Summary:
     * - Create a {@link luciad/model/store/WFSFeatureStore} to access the server
     * - Create a {@link luciad/model/feature/FeatureModel} for the store
     * - Create a {@link luciad/view/feature/FeatureLayer}
     * - Create a {@link luciad/view/feature/FeaturePainter} to style the layer (optional)
     */

    createLayer: function(url, layerName, typeName, painter, newReference) {
        var reference;
        if(newReference) 
            reference = newReference;
        else
            reference = ReferenceProvider.getReference("urn:ogc:def:crs:OGC::CRS84");
      //console.log(reference);
      var versions = ['1.1.0'];
      //#snippet createWFSModel
      var store = new WFSFeatureStore({
        serviceURL: url,
        typeName: typeName,
        outputFormat: "application/json",
        codec: new GeoJsonCodec(),
        versions: versions,
        reference: reference 
      });

      var model = new FeatureModel(store);

      var layer = new FeatureLayer(model, {label: layerName + "(WFS)", selectable: true, painter: painter});
      //#endsnippet createWFSModel

      return layer;
    }
  };

});