 /* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 
define([
    "luciad/shape/ShapeFactory",
    "luciad/reference/ReferenceProvider",
    "luciad/model/feature/Feature",
    "./Tablas",
    "./OpcionesGraficas"
], function (ShapeFactory, ReferenceProvider, Feature, Tablas, OpcionesGraficas) {
    
    var graficas = new Array(), idGrafica=0, ids = 0;
    var Filtros = [], Labels = [], opciones = [];
    var divDetalles=[];
    function obtenerGrafica(id) {
        return graficas[id] || null;
    }
    /*
     * 
     * @param {object} features
     * @param {string} div
     * @param {array strings} labels
     * @param {array strings} type
     * @param {array strings} filtros
     * @param {string} divTable
     * @param (array strings) titles
     * @param (array strings) formatAnotattion
     * @Param (array ints) topDatos
     * @Param (object) opcionesGraficas
     * @param (
     * @returns {GraficasL#12.crearGraficas.GraficasAnonym$2}
     */
    function crearGraficasFeatures(features, div, labels, type, filtros, divTable, titles, formatAnotattion, topDatos, opcionesGraficas, detalles) {
        var divs = [];
        document.getElementById(div).innerHTML = "";
        var ng = labels.length, etiqueta = "";
        Labels = labels;
        var todosFiltros = false, fil=[], j=0, todosDetalles=false;
        if(filtros === true){
            todosFiltros=true;
            filtros=[];
        }
        if(detalles === true) {
            todosDetalles = true;
            detalles = [];
        }
        detalles = detalles || [];
        var baseGrafica = '<label><big>$label</big></label> <div id="$div"></div>';
        var baseFiltros = '<div><select class="selectPanel form-control" id="$div">'+
                            '<option value="Todos">Todos</option>'+
                        '</select></div><p>';
        var botonDetalles = '<button id="IDBDEtalles" class="form-control" onclick="$(\'#IDPanelDetalles\').fadeIn(\'slow\');$(\'#IDBDEtalles\').fadeOut()">Ver Detalles</button>';
        var baseDetalles = 'BotonDetalles\n\
<div id="IDPanelDetalles" style="display: none">\n\
    <button class="form-control" onclick="$(\'#IDPanelDetalles\').fadeOut(\'slow\');$(\'#IDBDEtalles\').fadeIn()">Ocultar Detalles</button>\n\
    <div id="TDetalles"></div>\n\
</div>';
        for(var i=0; i<ng; i++) {
            if(titles[i] && titles[i]!== "")
                divs[i] = titles[i];
            else {
                if( typeof labels[i] === "string") 
                    divs[i] = labels[i];
                else {
                    divs[i] = labels[i][0];
                }
            }
            etiqueta += baseGrafica.replace("$label", divs[i]).replace("$div", divs[i]+i);
            if(filtros[i] || todosFiltros === true) {
                if(todosFiltros=== true || filtros[i] === true)
                    filtros[i] = divs[i]+"F"+i;
                try {
                    fil[j] = document.getElementById(filtros[i]);
                }catch(e) {}
                if(!fil[j]) {
                    etiqueta += "\n"+baseFiltros.replace("$div", filtros[i]);
                }
                fil[j] = filtros[i];
                j++;
            }
            
            if(detalles[i] || todosDetalles===true) {
                detalles[i] = "PD"+divs[i]+i;
                var D = botonDetalles.replace("IDBDEtalles", "B"+detalles[i])
                        .replace("IDBDEtalles", "B"+detalles[i]).replace("IDPanelDetalles", detalles[i]);
                D = baseDetalles.replace("BotonDetalles", D).replace("IDBDEtalles", "B"+detalles[i])
                        .replace("IDPanelDetalles", detalles[i]).replace("IDPanelDetalles", detalles[i]).replace("TDetalles", "T"+detalles[i]);
                etiqueta += D;
            }
        }
        document.getElementById(div).innerHTML = etiqueta;
        
        divDetalles = detalles;
        topDatos = topDatos || 0;
        var tablaDatos;
        if(!features[0].properties)
            tablaDatos = features;
        else
            tablaDatos = Tablas.crearArreglo(features, divTable);
        console.log(tablaDatos[0]);
        var Graficas = [], t;
        for(i=0; i<ng; i++) {
            if(type[i]) {
                t = type[i];
            } else {
                t = type[0];
            }
            var opciones;
            if(opcionesGraficas) {
                if(opcionesGraficas[i] || opcionesGraficas[i] === null) {
                    opciones = opcionesGraficas[i];
                } else
                    opciones = opcionesGraficas;
            } else
                opciones = opcionesGraficas;
            if( typeof labels[i] === "string") {
                Graficas[i] = crearGraficaIdTexto(divs[i]+i, tablaDatos, t, labels[i], opciones, topDatos[i], detalles[i]);
            } else {
                //Graficas[i] = crearGraficaMultiLabels(divs[i]+i, tablaDatos, t, labels[i], opciones, titles[i], topDatos[i]);
                Graficas[i] = crearGraficaLabelValor(divs[i]+i, tablaDatos, t, labels[i][0], labels[i][1], formatAnotattion, opciones, topDatos[i], detalles[i]);
            }
           
            if(fil[i]) {
                if(fil[i] && fil[i] !== "")
                    setOptions(fil[i], Graficas[i].nombres);
            }
            Filtros = fil;
            Graficas[i] = {
                id: Graficas[i].idGrafica,
                label: labels[i],
                tipo: t,
                div: divs[i],
                top: topDatos[i],
                tabla: Graficas[i].tablaCompleta
            };
        }
        
        return  {
            graficas: Graficas,
            tablaDatos: tablaDatos,
            filtros: fil
        };
    }
    
    /*
     * 
     * @param {type} div
     * @param {type} opciones
     * @returns {undefined}
     */
    function setOptions(div, opciones, firstOption) {
        if(!firstOption)
            firstOption = "Todos";
        try {
            document.getElementById(div).innerHTML = "";
            var base = "<option value='$VALUE'>$LABEL</option>", etiqueta = base.replace("$VALUE", firstOption).replace("$LABEL", firstOption);
            var  n = opciones.length>100? 100: opciones.length;
            for(var i =0; i<n; i++) {
                etiqueta += base.replace("$VALUE", opciones[i]).replace("$LABEL", opciones[i]);
            }
            document.getElementById(div).innerHTML = etiqueta;
        } catch(e) {
            console.log("No se encontro el filtro "+div);
            console.log(e);
        }
    }
    
    /*
     * 
     * @param {String} div
     * @param {Array[][]} datos
     * @param {String} tipo
     * @param {String} titulo
     * @returns {unresolved}
     */
    function dibujarGraficaArreglo(div, datos, tipo, titulo, id, chartOptions) {
        document.getElementById(div).innerHTML="";
        var c;
        
        switch(tipo.toLowerCase()) {
            case "pie":
            case "piechart":
                chartOptions = chartOptions || OpcionesGraficas.getPieOptions();
                c = PieChart(div, datos, titulo, id | idGrafica, chartOptions);
                break;
            case "columns":
            case "columnchart":
                chartOptions = chartOptions || OpcionesGraficas.getColumnsOptions();
                c = ColumnChart(div, datos, titulo, id | idGrafica, chartOptions);
                break;
            case "area":
            case "areachart":
                chartOptions = chartOptions || OpcionesGraficas.getAreaOptions();
                c = AreaChart(div, datos, titulo, id | idGrafica, chartOptions);
                break;
            default: console.log("Tipo de Grafica desconocida "+ tipo);return null;
        }
        opciones[id] = chartOptions;
    }
    /*
     * 
     * @param {Object} chart
     * @param {Array[][]} newData
     * @param {String} chartType
     * @returns {undefined}
     */
    function updateChart(chart, newData, chartType, chartOptions) {
        var newOptions;
        switch(chartType) {
            case "pie":
            case "piechart": 
                newOptions = chartOptions || OpcionesGraficas.getPieOptions();
                break;
            case "columns":
            case "columnchart":
                newOptions = chartOptions || OpcionesGraficas.getColumnsOptions();
                break;
            case "area":
            case "areachart":
                newOptions = chartOptions || OpcionesGraficas.getAreaOptions();
                break;
        }
        var table = new google.visualization.arrayToDataTable(newData);
        chart.draw(table, newOptions);
    }
    function actualizarGrafica(idGrafica, newData, chartType) {
        if(graficas[idGrafica])
            updateChart(graficas[idGrafica], newData, chartType);
    }
    /*
     * 
     * @param {String} div
     * @param {Array[][]} datos
     * @param {String} titulo
     * @returns {undefined}
     */
    function ColumnChart(div, datos, titulo, id, chartOptions) {
        var chart;
        try{
            //google.charts.load('current', {'packages':['line']});
            google.charts.load('current', {packages: ['corechart', 'bar']});
            return google.charts.setOnLoadCallback(drawColumnChart);
            
        }catch(e){
            console.log("Error al crear Grafica de columna\n");
            console.log(e);
        }
        function drawColumnChart() {
            chart = new google.visualization.ColumnChart(document.getElementById(div));
            var datosNuevos = new google.visualization.arrayToDataTable(datos);
            chartOptions.title = titulo;
            chart.draw(datosNuevos, chartOptions);
            graficas[id] = chart;
        }
    }
    /*
     * 
     * @param {String} div
     * @param {Array[][]} datos
     * @param {String} titulo
     * @returns {unresolved}
     */
    function PieChart(div, datos, titulo, id, chartOptions){
        var chart;
        try{
            //google.charts.load('current', {'packages':['line']});
            google.charts.load('current', {'packages':['corechart']});
            return google.charts.setOnLoadCallback(drawPieChart);
        }catch(e){
            console.log("Error al crear Grafica de lineas\n");
            console.log(e);
        }
        function drawPieChart() {
            chart = new google.visualization.PieChart(document.getElementById(div));
            var datosNuevos = new google.visualization.arrayToDataTable(datos);
            chartOptions.title = titulo;
            chart.draw(datosNuevos, chartOptions);
            graficas[id] = chart;
        }
    }
    /*
     * 
     * @param {String} div
     * @param {Array[][]} datos
     * @returns {unresolved}
     */
    function AreaChart(div, datos, titulo, id, chartOptions){
        var chart;
        try{
            //google.charts.load('current', {'packages':['line']});
            google.charts.load('current', {'packages':['corechart']});
            return google.charts.setOnLoadCallback(drawAreaChart);
        }catch(e){
            console.log("Error al crear Grafica de lineas\n");
            console.log(e);
        }
        function drawAreaChart() {
            chart = new google.visualization.AreaChart(document.getElementById(div));
            var datosNuevos = new google.visualization.arrayToDataTable(datos);
            chartOptions.title = titulo;
            chart.draw(datosNuevos, chartOptions);
            /*google.visualization.events.addListener(chart, 'select', function() {
                try{
                var x = chart.getSelection(), row = x[0].row, col = 0;
                var dato1 = datosNuevos.getValue(row, 0);
                
                filtrarTexto(dato1);
            }catch(e) {
                console.log(e);
            }
            });*/
            graficas[id] = chart;
        }
    }    
    /*
     * 
     * @param {type} div
     * @param {type} tablaDatos
     * @param {type} chartType
     * @param {type} label
     * @returns {GraficasL#11.crearGraficaTabla.datos}
     */
    function crearGraficaTabla(div, tablaDatos, chartType, label) {
        var numeroDatos =[], tipoDato = [];
        for(var i = 1; i<tablaDatos.length; i++) {
            tipoDato[i-1] = tablaDatos[i][0];
            numeroDatos[i-1] = tablaDatos[i][1];
        }
        var ordenado = ordenarMayorMenor(numeroDatos, tipoDato);
        tipoDato = ordenado.nombres;
        numeroDatos = ordenado.valores;
        var z = tipoDato.length;
        tablaDatos = [];
        tablaDatos[0] = [label, "Cantidad"];
        if(z>10)
            z = 10;
        z=tipoDato.length;
        for(i=0; i<z; i++) {
            tablaDatos[i+1] = [""+tipoDato[i], numeroDatos[i]];
        }
        var otros =0;
        for(i=i; i<tipoDato.length; i++) {
            if((i-z) > 10)
                break;
            otros += numeroDatos[i];
        }
        //tablaDatos[z] = ["Otros", otros];
        idGrafica++;
        var datos = {
            tabla: tablaDatos,
            nombres: tipoDato,
            valores: numeroDatos,
            idGrafica: idGrafica
        };
        //crearGraficaIdTexto(div, datos.tabla, chartType, label);
        dibujarGraficaArreglo(div, datos.tabla, chartType, label, idGrafica);
        
        return datos;
    }
    /*
     * 
     * @param {String} div
     * @param {Array[][]} tablaDatos
     * @param {String} chartType
     * @param {String} title
     * @param {int} id
     * @returns {Number}
     */
    function crearGraficaIdTexto(div, tablaDatos, chartType, label, options, topDatos, divDetalles) {
        
        console.log("Creando Grafica "+label+" id Grafica"+idGrafica);
        var datos = datosChartTexto(tablaDatos, label, topDatos);
        dibujarGraficaArreglo(div, datos.tabla, chartType, label, idGrafica, options);
        datos.idGrafica = idGrafica;
        datos.top = topDatos;
        idGrafica++;
        if(divDetalles) {
            getDetalles(divDetalles, datos.tablaCompleta);
        }
        return datos;
    }
    /*
     * 
     * @param {String} div
     * @param {Array[][]} tablaDatos
     * @param {String} chartType
     * @param {String} title
     * @param {int} idLabel
     * @param {int} idValor
     * @returns {Number}
     */
    function crearGraficaLabelValor(div, tablaDatos, chartType, label, valor, formatAnotation, options, topDatos, divDetalles) {
        
        console.log("Creando Grafica "+label+" id Grafica"+idGrafica);
        var datos = datosChartLabelValor(tablaDatos, label, valor, formatAnotation, topDatos);
        dibujarGraficaArreglo(div, datos.tabla, chartType, label, idGrafica, options);
        datos.idGrafica = idGrafica;
        datos.top = topDatos;
        idGrafica++;
        if(divDetalles) {
            getDetalles(divDetalles, datos.tablaCompleta);
        }
        return datos;
    }
    /*
     * 
     * @param {Array[][]} tablaDatos
     * @param {int} id
     * @param {String} label
     * @returns {Array}
     */
    function datosChartTexto(tablaDatos, label, topDatos) {
        var tabla = [[label, "number"]], tablaCompleta = [];
        var tipoDato = new Array(), numeroDatos = new Array(), valor, nuevoDato=false;
        var i=0, j=0, k;
        valor = tablaDatos[0];
        for(k=0;k<valor.length;k++) {
            if(valor[k] === label)
                break;
        }
        for(i=1; i<tablaDatos.length; i++) {
            
            valor = tablaDatos[i][k];
            if(tipoDato.length===0) {
                tipoDato[0] = valor;
                numeroDatos[0] = 1;
            }
            else {
                for(j=0;j<tipoDato.length;j++) {
                    if(valor === tipoDato[j]) {
                        numeroDatos[j]++;
                        nuevoDato = false;
                        break;
                    }
                    else
                        nuevoDato =true;
                }
                if(nuevoDato){
                    tipoDato[j] = valor;
                    numeroDatos[j] = 1;
                }
            }
        }
        var ordenado = ordenarMayorMenor(numeroDatos, tipoDato);
        tipoDato = ordenado.nombres;
        numeroDatos = ordenado.valores;
        var z = tipoDato.length;
        if(z>topDatos && topDatos !==0)
            z = topDatos;
        for(i=0; i<z; i++) {
            tabla[i+1] = [""+tipoDato[i], numeroDatos[i]];
            tablaCompleta[i] = [""+tipoDato[i], numeroDatos[i]];
        }
        if(tipoDato.length>topDatos) {
            var otros =0;
            for(i=i; i<tipoDato.length; i++) {
                if((i-z) < 10)
                    otros += numeroDatos[i];
                tablaCompleta[i] = [""+tipoDato[i], numeroDatos[i]];
            }
            tabla[z] = ["Otros", otros];
        }
        return {
            tabla: tabla,
            tablaCompleta: tablaCompleta,
            nombres: tipoDato,
            valores: numeroDatos
        };
    }
    /*
     * 
     * @param {Array[][]} tablaDatos
     * @param {int} idLabel
     * @param {int} idValor
     * @param {String} label
     * @returns {Array}
     */
    function datosChartLabelValor(tablaDatos, label, lValor, formatAnotation, topDatos) {
        var tabla = [[label, "number", {type: 'string', role: 'annotation'}]];
        var tablaCompleta=[];
        var tipoDato = new Array(), numeroDatos = new Array(), nombre, valor, nuevoDato=false;
        var i=0, j=0;
        var idLabel, idValor;
        valor = tablaDatos[0];
        for(var k=0;k<valor.length;k++) {
            if(valor[k] === label)
                idLabel = k;
            if(valor[k] === lValor)
                idValor = k;
        }
        
        for(i=1; i<tablaDatos.length; i++) {
            nombre = tablaDatos[i][idLabel];
            valor = tablaDatos[i][idValor];
            if(tipoDato.length===0) {
                tipoDato[0] = nombre;
                numeroDatos[0] = valor;
            }
            else {
                for(j=0;j<tipoDato.length;j++) {
                    if(nombre === tipoDato[j]) {
                        numeroDatos[j]+= parseFloat(valor);
                        nuevoDato = false;
                        break;
                    }
                    else
                        nuevoDato =true;
                }
                if(nuevoDato){
                    tipoDato[j] = nombre;
                    numeroDatos[j] = valor;
                }
            }
            
        }
        var ordenado = ordenarMayorMenor(numeroDatos, tipoDato);
        tipoDato = ordenado.nombres;
        numeroDatos = ordenado.valores;
        if(!formatAnotation || formatAnotation=== null) 
            formatAnotation = defaultFormat;
        var z = tipoDato.length;
        topDatos = topDatos || 10;
        if(z>topDatos)
            z = topDatos;
        for(i=0; i<z; i++) {
            tabla[i+1] = [""+tipoDato[i], numeroDatos[i], formatAnotation(numeroDatos[i])];
        }
        if(tipoDato.length>10) {
            var otros =0;
            for(i=i; i<tipoDato.length; i++) {
                if((i-z) > 10)
                    break;
                otros += numeroDatos[i];
            }
            tabla[z] = ["Otros", otros, formatAnotation(numeroDatos[i])];
        }
        
        return {
            tabla: tabla,
            nombres: tipoDato,
            valores: numeroDatos
        };
    }
    function defaultFormat(number) {
        var anotation, c, j=0, x=false;
        anotation = Math.round(number) +"";
        number = "";
        for(var i=anotation.length-1;i>=0;i--) {
            c = anotation.charAt(i);
            if(c !=='K') {
                j++;
            }
            if(j>3) {
                number+=",";
                j=1;
                x=true;
            }
            number+=c;
        }
        if(x) {
            anotation = "";
            for(i=number.length-1; i>=0;i--) {
                c = number.charAt(i);
                anotation += c;
            }
        }
        return anotation;
    }
    /*
     * 
     * @param {string} div
     * @param {Array} tablaDatos
     * @param {string} chartType
     * @param {Aray strings} labels
     * @param {object} options
     * @param {string} title
     * @param {Array int} topDatos
     * @returns {GraficasL#13.datosChartMultiLabels.GraficasAnonym$24|GraficasL#13.crearGraficaMultiLabels.datos}
     */
    function crearGraficaMultiLabels(div, tablaDatos, chartType, labels, options, title, topDatos) {
        console.log("Creando Grafica "+title+" id Grafica"+idGrafica);
        var datos = datosChartMultiLabels(tablaDatos, labels, topDatos);
        dibujarGraficaArreglo(div, datos.tabla, chartType, title, idGrafica, options);
        datos.idGrafica = idGrafica;
        datos.top = topDatos;
        idGrafica++;
        return datos;
    }
    /*
     * 
     * @param {Array Array} tablaDatos
     * @param {Array Strings} keys
     * @param {Array Int} topDatos
     * @returns {GraficasL#13.datosChartMultiLabels.GraficasAnonym$24}
     */
    function datosChartMultiLabels(tablaDatos, keys, topDatos){
        var valor = tablaDatos[0], ids=[], i=0;
        for(var k in keys) {
            for(var e in valor) {
                if(valor[e] === keys[k]) {
                    ids[k] = e;
                    break;
                }
            }
        }
        var tipoDato=[], numeroDatos=[], nuevoDato=true;
        for(i=1; i<tablaDatos.length; i++) {
            for(k in keys) {
                valor = tablaDatos[i][ids[k]];
                if(tipoDato.length===0) {
                    tipoDato[0] = keys[k];
                    numeroDatos[0] = valor;
                } else {
                    for(j=0;j<tipoDato.length;j++) {
                        if(keys[k] === tipoDato[j]) {
                            numeroDatos[j]+= parseFloat(valor) || valor;
                            nuevoDato = false;
                            break;
                        }
                        else
                            nuevoDato =true;
                    }
                    if(nuevoDato){
                        tipoDato[j] = keys[k];
                        numeroDatos[j] = valor;
                    }
                }
            }
        }
        var ordenado = ordenarMayorMenor(numeroDatos, tipoDato);
        var tabla=[["Names", "number"]], tablaCompleta = [];
        tipoDato = ordenado.nombres;
        numeroDatos = ordenado.valores;
        var z = tipoDato.length;
        if(z>topDatos && topDatos !==0)
            z = topDatos;
        for(i=0; i<z; i++) {
            tabla[i+1] = [""+tipoDato[i], numeroDatos[i]];
            tablaCompleta[i+1] = [""+tipoDato[i], numeroDatos[i]];
        }
        if(tipoDato.length>topDatos) {
            var otros =0;
            for(i=i; i<tipoDato.length; i++) {
                if((i-z) < 10)
                    otros += numeroDatos[i];
                tablaCompleta[i+1] = [""+tipoDato[i], numeroDatos[i]];
            }
            tabla[z] = ["Otros", otros];
        }
        return {
            tabla: tabla,
            tablaCompleta: tablaCompleta,
            nombres: tipoDato,
            valores: numeroDatos
        };
    }
    /*
     * Se encarga de preparar los datos y enviarlos a la funcion para crear la grafica.
     * @param {int} idGrafica
     * @param {Array[][]} tablaDatos
     * @param {String} typeChart
     * @param {String} title
     * @param {int} id
     * @returns {undefined}
     */
    function actualizarGraficaIdTexto(idGrafica, tablaDatos, typeChart, label, topDatos) {
        var datos = datosChartTexto(tablaDatos, label, topDatos);
        if(graficas[idGrafica])
            updateChart(graficas[idGrafica], datos.tabla, typeChart, opciones[idGrafica]);
        else 
            console.log("Grafica aun no cargada");
        if(divDetalles[idGrafica])
            getDetalles(divDetalles[idGrafica], datos.tablaCompleta);
    }
    /*
     * 
     * @param {int} idGrafica
     * @param {Array[][]} tablaDatos
     * @param {String} typeChart
     * @param {String} title
     * @param {int} idLabel
     * @param {int} idValor
     * @returns {undefined}
     */
    function actualizarGraficaLabelValor(idGrafica, tablaDatos, typeChart, label, valor, formatAnotation, topDatos) {
        var datos = datosChartLabelValor(tablaDatos, label, valor, formatAnotation, topDatos);
        if(graficas[idGrafica])
            updateChart(graficas[idGrafica], datos.tabla, typeChart, opciones[idGrafica]);
        else
            console.log("Grafica aun no cargada");
        if(divDetalles[idGrafica])
            getDetalles(divDetalles[idGrafica], datos.tablaCompleta);
    }
    /*
     * 
     * @param {type} idGrafica
     * @param {type} tablaDatos
     * @param {type} typeChart
     * @param {type} labels
     * @param {type} topDatos
     * @returns {undefined}
     */
    function actualizarGraficaMultiLabels(idGrafica, tablaDatos, typeChart, labels, topDatos) {
        var datos = datosChartMultiLabels(tablaDatos, labels, topDatos).tabla;
        if(graficas[idGrafica])
            updateChart(graficas[idGrafica], datos, typeChart, opciones[idGrafica]);
        else 
            console.log("Grafica aun no cargada");
    }
    /*
     * Se encarga de leer los datos de la columna dada por id de la tabla tablaDatos. 
     * Y regresar un Arreglo de datos listos para crear la grafica.
     * @param {Array[][]} tablaDatos
     * @param {int} id
     * @param {String} label
     * @returns {Array}
     */
    function datosChart(tablaDatos, id, label) {
        var datos = [[label, "number"]], dato;
        var tipoDato = new Array(), numeroDatos = new Array(), nuevoDato=false;
        var i, j;
        
        for(i=1; i<tablaDatos.length; i++) {
            dato = tablaDatos[i][id];
            if(tipoDato.length===0) {
                tipoDato[0] = dato;
                numeroDatos[0] = 1;
            }
            else {
                for(j=0;j<tipoDato.length;j++) {
                    if(dato === tipoDato[j]) {
                        numeroDatos[j]++;
                        nuevoDato = false;
                        break;
                    }
                    else
                        nuevoDato =true;
                }
                if(nuevoDato){
                    tipoDato[j] = dato;
                    numeroDatos[j] = 1;
                }
            }
        }
        var ordenado = ordenarMayorMenor(numeroDatos, tipoDato);
        tipoDato = ordenado.nombres;
        numeroDatos = ordenado.valores;
        var z = tipoDato.length;
        if(z>50)
            z = 50;
        for(i=0; i<z; i++) {
            datos[i+1] = [tipoDato[i], numeroDatos[i]];
        }
        return datos;
    }
                      
    function getId(label, list) {
        for( var k=0;k<list.length;k++) {
            if(list[k] === label)
                return k;
        }
    }
    /*
     * 
     * @param {type} tablaDatos
     * @param {type} timeEventLayer
     * @param {type} mapBounds
     * @param {type} selectedFeature
     * @returns {Array}
     */
    
    function filtrarPorBounds(tablaDatos, timeEventLayer, mapBounds, selectedFeature, year) {
        var tabla = [tablaDatos[0]], i, j=1, lon, lat;
        var baseDate = "aaaa/mes/dia hora:00";
        var zonaFilterNode = document.getElementById( "zonaFiltro" );
        var crimenFilterNode = document.getElementById( "crimenFiltro" );
        var crimen = crimenFilterNode.selectedOptions[0].value;
        var zona = zonaFilterNode.selectedOptions[0].value;
        var n = tablaDatos.length;
        for(i=1;i<n;i++) {
            lat = tablaDatos[i][n-1];
            lon = tablaDatos[i][n-2];
            var eventTime = tablaDatos[i][n-3];
            
            var point = createPoint(lon, lat, 0, i, {EventTime: eventTime});
            var f = timeEventLayer.shapeProvider.provideShape(point);
            var z = tablaDatos[i][5], c = tablaDatos[i][0];
            if(selectedFeature){
                var cuadrante = obtenerCuadrante(selectedFeature.properties.name);
                if(cuadrante === tablaDatos[i][6]) {
                    tabla[j] = tablaDatos[i];
                    j++;
                }
                /*
                if(mapBounds.contains2D(f) && selectedFeature.geometry.bounds.contains2D(point.shape)) {
                    if((c === crimen || crimen === "Todos") && (z === zona || zona === "Todos")){
                        tabla[j] = tablaDatos[i];
                        j++;
                    }
                    
                }*/
            } else {
                if(mapBounds.contains2D(f)) {
                    if((c === crimen || crimen === "Todos") && (z === zona || zona === "Todos")){
                        tabla[j] = tablaDatos[i];
                        j++;
                    }
                }
            }
        }
        if(tabla.length > 1)
            return tabla;
        else {
            console.log("tabla no actualizada");
            return tablaDatos;
        }
            
    }
    function createPoint(x, y, z, id, properties) {
        var reference = ReferenceProvider.getReference("CRS:84");
        return new Feature(
            ShapeFactory.createPoint(reference, [x, y, z]), properties, id
        );
    }
    
    /*
     * 
     * @param {type} valores
     * @param {type} nombres
     * @returns {GraficasL#11.ordenarMayorMenor.GraficasAnonym$14}
     */
    function ordenarMayorMenor(valores, nombres) {
        var n = valores.length, x, y, k =0;
        for(var j=1; j<n; j++) {
            if(valores[j] > valores[j-1]) {
                x = valores[j-1];
                y = nombres[j-1];
                valores[j-1] = valores[j];
                nombres[j-1] = nombres[j];
                valores[j] = x;
                nombres[j] = y;
                j = 0;
                k++;
                if(k>(n*10)) {
                    break;
                }
            }
        }
        return {
            valores: valores, nombres: nombres
        };
    }
    
    
    /*
     * 
     * @param {type} tablaDatos
     * @param {type} timeEventLayer
     * @param {type} mapBounds
     * @param {type} selectedFeature
     * @returns {Array}
     */
    
    function actualizarTabla(tablaDatos, timeEventLayer, mapBounds) {
        var tabla = [tablaDatos[0]], i=0, j=1, lon, lat;
        var n = tablaDatos[0].length, filtros = [], values = [];
        try{
        for(j=0;j<Filtros.length;j++) {
            if(Filtros[j] && Filtros[j]!=="") {
                filtros[i] = document.getElementById( Filtros[j] );
                values[i] = filtros[i].selectedOptions[0].value;
                try {
                    var x = Number(values[i]);
                } catch(e) {}
                if(x) {
                    values[i] = x;
                } 
                i++;
            }
        }
        }catch(e) {
            console.log("No se encontro el filtro");
            console.log(e);
            return tablaDatos;
        }
        if(timeEventLayer && mapBounds) {
            var k=1;
            for(i=1;i<tablaDatos.length;i++) {
                lon = tablaDatos[i][getId("LONGITUD", tablaDatos[0])];
                lat = tablaDatos[i][getId("LATITUD", tablaDatos[0])];
                var eventTime = tablaDatos[i][getId("EventTime", tablaDatos[0])];;
                var point = createPoint(lon, lat, 0, i, {EventTime: eventTime});
                var f = timeEventLayer.shapeProvider.provideShape(point);
                var vFeatures = [];
                for(j=0;j<filtros.length;j++) {
                    var idLabel;
                    if(typeof Labels[j] === "string")
                        idLabel = getId(Labels[j], tablaDatos[0]);
                    else
                        idLabel = getId(Labels[j][0], tablaDatos[0]);
                    vFeatures[j] = tablaDatos[i][idLabel];
                }
                var xFiltros = 0;
                if(mapBounds.contains2DBounds(f.bounds)) {
                    for(j=0;j<filtros.length;j++) {
                        if(vFeatures[j] === values[j] || values[j] === "Todos") {
                            xFiltros++;
                        }
                    }
                    if(xFiltros === filtros.length){
                        tabla[k] = tablaDatos[i];
                        k++;
                    }
                }
            }
            if(tabla.length > 1)
                return tabla;
            else {
                console.log("tabla no actualizada");
                return tablaDatos;
            }
        } else {
            var k=1;
            for(i=1;i<tablaDatos.length;i++) {
                var x=0, l=0;
                for(j=0;j<filtros.length;j++) {
                    var label;
                    while(typeof Labels[l] !== "string") {
                        l++;
                    }
                    label = Labels[l];
                    l++;
                    var vTabla = tablaDatos[i][getId(label, tablaDatos[0])];
                    var value = filtros[j].selectedOptions[0].value;
                    if(vTabla === value || value === "Todos") {
                        x++;
                    }
                }
                if(x === filtros.length){
                    tabla[k] = tablaDatos[i];
                    k++;
                }
            }
            if(tabla.length > 1)
                return tabla;
            else {
                console.log("tabla no actualizada");
                return tablaDatos;
            }
        }    
            
    }
    function createPoint(x, y, z, id, properties) {
        var reference = ReferenceProvider.getReference("CRS:84");
        return new Feature(
            ShapeFactory.createPoint(reference, [x, y, z]), properties, id
        );
    }
    
    function getDetalles(div, tabla) {
        var n = tabla.length -1;
        var max = tabla[1][0]+" "+tabla[1][1];
        var min = tabla[n][0]+" "+tabla[n][1];
        var pro = 0, con=0;
        for(var v=1; v<n; v++) {
            con += tabla[v][1];
        }
        pro = con / n;
        pro = pro.toFixed(2);
        var etiqueta = '<table><tr>\n\
<td>Maximo:</td><td><label>'+max+'</label></td></tr>\n\
<tr><td>Minimo:</td><td><label>'+min+'</label></td></tr>\n\
<tr><td>Promedio:</td><td><label>'+pro+'</label></td></tr>\n\
<tr><td>Conteo:</td><td><label>'+con+'</label></td></tr></table>';
        document.getElementById("T"+div).innerHTML = etiqueta;
    }
    
    return {
        crearGraficasFeatures: crearGraficasFeatures,
        dibujarGraficaArreglo: dibujarGraficaArreglo,
        updateChart: updateChart,
        //rearArreglo: crearArreglo,
        crearGraficaIdTexto: crearGraficaIdTexto,
        actualizarGraficaIdTexto: actualizarGraficaIdTexto,
        crearGraficaLabelValor: crearGraficaLabelValor,
        actualizarGraficaLabelValor: actualizarGraficaLabelValor,
        filtrarPorBounds: filtrarPorBounds,
        crearGraficaTabla: crearGraficaTabla,
        actualizarTabla: actualizarTabla,
        setOptions: setOptions,
        actualizarGrafica: actualizarGrafica,
        datosChartTexto: datosChartTexto,
        ordenarMayorMenor: ordenarMayorMenor,
        crearArreglo: Tablas.crearArreglo,
        obtenerGrafica: obtenerGrafica,
        crearGraficaMultiLabels: crearGraficaMultiLabels,
        actualizarGraficaMultiLabels: actualizarGraficaMultiLabels
    };
    
});

