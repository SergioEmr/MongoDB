/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


define([
    "../Util",
    "../Shapes"
], function (Util, Shapes) {
    
    /*
     * 
     * @param {Object} features
     * @returns {Array[][]}
     */
    function crearArreglo(features, div, options) {
        var tabla = new Array(), encavezados = new Array(), valores = new Array();
        var key, i=0, j=0, k=0, x, coordenadas;
        for(key in features[0].properties) {
            encavezados[i] = key;
            i++;
        }
        encavezados[i] = "LONGITUD";
        encavezados[i+1] = "LATITUD";
        tabla[j] = encavezados;
        for(i=0; i<features.length; i++) {
            var p = features[i].properties;
            for(key in p) {
                try {
                    
                    x = Util.esNumero(p[key])? parseFloat(p[key]): p[key];
                    if(x && key !== "EventTime")
                        valores[k] = x;
                    else
                        valores[k] = p[key];
                }catch (e) {
                    valores[k] = p[key];
                }
                k++;
            }
            
            var geometry = features[i].geometry;
            var tipo = Shapes.getShapeType(features[i].shape);
            switch(tipo) {
                case "Point": 
                    valores[k] = geometry.x;
                    valores[k+1] = geometry.y;
                    break;
                default:
                    valores[k] = geometry.focusPoint.x || 0.0;
                    valores[k+1] = geometry.focusPoint.y || 0.0;
                    break;
            }
            /*if(!geometry.coordenadas) {
                valores[k] = features[i].properties.lon? parseFloat(features[i].properties.lon): 0.0;
                valores[k+1] = features[i].properties.lat? parseFloat(features[i].properties.lat): 0.0;
            } else {
                
                if(coordenadas.length === 3) {
                    valores[k] = coordenadas[0];
                    valores[k+1] = coordenadas[1];

                } else {
                    if(coordenadas.length===2) {
                    } else {
                        valores[k] = coordenadas[0].x;
                        valores[k+1] = coordenadas[0].y;
                    }
                }
            }*/
            j++;
            tabla[j]=valores;
            valores = new Array();
            k=0;
        }
        if(div) {
            crearTablaDIV(div, tabla, options);
        }
        return tabla;
    }
    /*
     * 
     * @param {String} div
     * @param {Array [] []} tabla
     * @returns {undefined}
     */
    function crearTablaCheckBox(div, tabla) {
        var etiqueta = //block, flex
                '<div class="labelDatos" style="display:block !important">' +
                '<table>'+
                '$tabla' +
                '</table>' +
                '</div>';
            var columna = '<td align="center"><b>$dato</b></td >';
            var columnas ='';
            var fila = '<tr>$columna</tr>' ;
            var filas= '', etiquetaTabla ='', n=tabla.length , m = tabla[0].length;
    
            var datos;
            for(var i=0; i<n; i++) {
                datos = tabla[i];
                for(var j=0;j<m;j++) {
                    if(i===0) {
                        columnas += columna.replace('$dato', datos[j]);
                    } else {
                        columnas += columna.replace("$dato", datos[j]);
                    }
                    
                }
                if(i===0) {
                    columnas += "<td><b>Analizar</b></td><td><b>Visualizar</b></td>";
                } else {
                    columnas += '<td align="center"><input type="checkbox" value="0'+i+'" checked></td>\n\
                    <td align="center"><input type="checkbox" value="1'+i+'" checked></td>';
                }
                columna = '<td align="center">$dato</td >\n';
                filas += fila.replace('$columna', columnas);
                etiquetaTabla += filas;
                columnas='';
                filas ='';
            }
            etiqueta = etiqueta.replace('$tabla', etiquetaTabla);
            document.getElementById(div).innerHTML=etiqueta;
    }
    /*
     *  div: de donde se colocara la tabla
     *  tabla: Array de datos con encabezados
     */
    function crearTablaDIV(div, tabla, options) {
        var etiqueta = //block, flex
                //'<div style="top: 50px; position: absolute">$Encabezados</div>'+
                //'<div class="top" style="position: absolute; top: 100px; display:block; overflow-y: scroll; overflow-x: scroll; !important">' +
                '<table id="content'+div+'">'+
                '$tabla' +
                '</table>' ;
                //'</div>';
            var columna = '<td align="center"><b>$dato</b></td >';
            var columnas ='';
            var fila = '<tr bgcolor="$COLOR">$columna</tr>' ;
            var filas= '', etiquetaTabla ='', n=tabla.length , m = tabla[0].length;
            var style = options || getDefaultOptions();
            var datos = tabla[0], color;
            /*for(var i=0; i<m; i++) {
                columnas += columna.replace('$dato', datos[i]);
            }
            columna = '<td align="center">$dato</td >\n';
            filas += fila.replace('$columna', columnas).replace("$COLOR", color);
            etiquetaTabla += filas;
            etiqueta = etiqueta.replace('$Encabezados', etiquetaTabla);
            
            filas = '';
            columnas = '';
            etiquetaTabla = '';*/
            for(var i=0; i<n; i++) {
                datos = tabla[i];
                for(var j=0;j<m;j++) {
                    columnas += columna.replace('$dato', datos[j]);
                }
                if(i%2 === 0 )
                    color = style.color1;
                else
                    color = style.color2;
                filas += fila.replace('$columna', columnas).replace("$COLOR", color);
                etiquetaTabla += filas;
                columnas='';
                filas ='';
            }
            etiqueta = etiqueta.replace('$tabla', etiquetaTabla);
            document.getElementById(div).innerHTML=etiqueta;
    }
    /*
     * 
     */
    function getDefaultOptions() {
        return {
            color1: '#4b6a86',
            color2: ''
        };
    }
    
    function crearTablaUtilidades(div, tabla, error) {
        var etiqueta = //block, flex
                '<div class="labelDatos" style="display:block !important">' +
                '<table>'+
                '$tabla' +
                '</table>' +
                '</div>';
            var columna = '<td align="center"><b>$dato</b></td >';
            var columnas ='';
            var fila = '<tr>$columna</tr>' ;
            var filas= '', etiquetaTabla ='', n=tabla.length , m = tabla[0].length;
    
            var datos, color = "#ff5733", r;
            for(var i=0; i<n; i++) {
                datos = tabla[i];
                r =Math.round(Math.random() * ((m-1) - 0) + 0);
                for(var j=0;j<m;j++) {
                    
                    if(i===0) {
                        columnas += columna.replace('$dato', datos[j]);
                    } else {
                        if(r ===j)
                            columnas += columna.replace("$dato", datos[j]).replace("$color", color);
                        else 
                            columnas += columna.replace("$dato", datos[j]).replace("$color", "");
                    }
                    
                }
                columna = '<td align="center" bgcolor="$color">$dato</td >\n';
                //columna = '<td align="center" bgcolor="$color"><input type="text" value="$dato"></td >\n';
                filas += fila.replace('$columna', columnas);
                etiquetaTabla += filas;
                columnas='';
                filas ='';
            }
            etiqueta = etiqueta.replace('$tabla', etiquetaTabla);
            document.getElementById(div).innerHTML=etiqueta;
    }
    
    function tablaActual(feature, time) {
        var year = feature.properties.year;
        var tabla = [], encabezados=[], i=0, n=0;
        var suma = 0;
        if(time.year === year) {
            for(var key in feature.properties) {
                if(typeof feature.properties[key] === "object") {
                    encabezados[n] = key;
                    n++;
                }
            }
            var mes = time.month, j=0;
            tabla[j] = ["Delitos", "Cantidad"];
            var properties = feature.properties;
            for(i=0; i<n; i++) {
                var valores = properties[encabezados[i]];
                j++;
                tabla[j] = [encabezados[i], valores[mes]];
                suma += valores[mes];
            }
        }
        return {
            tabla: tabla,
            sumaTotal: suma
        };
    }
    
    return {
        
        crearArreglo: crearArreglo,
        crearTablaUtilidades: crearTablaUtilidades,
        crearTablaCheckBox:crearTablaCheckBox,
        crearTablaDIV: crearTablaDIV,
        tablaActual: tablaActual
    };

});