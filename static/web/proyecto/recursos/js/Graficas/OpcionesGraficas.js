/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define([], function () {
    return {
        getPieOptions: function () {
            return {
            width: 350,
            height: 220,
            titleTextStyle: {
                color: '#FFFFFF',
                frontSize: 20
              },
            legend: {
                position: "left",
              textStyle: {
                color: '#FFFFFF'
              }
            },
            backgroundColor:{
                fill: "#17202a"
            },
            chartArea: {
                left: 50,
                width: 270,
                height: 210,
                backgroundColor: "#17202a"
            },
            pieSliceBorderColor: "#17202a"
          };
        },
        getColumnsOptions: function() {
            return {
                width: 350,
                height: 400,
                orientation: "vertical",
                titleTextStyle: {
                    color: '#FFFFFF',
                    frontSize: 20
                  },
                legend: {
                    position: "bottom",
                  textStyle: {
                    color: '#FFFFFF'
                  }
                },
                backgroundColor:{
                    fill: "#17202a"
                },
                chartArea: {
                    left: 100,
                    width: 250,
                    height: 300,
                    backgroundColor: "#17202a"
                },
                annotations: {
                  textStyle: {
                    fontSize: 12,
                    color: '#FFFFFF',
                    auraColor: 'none'
                  }
                },
                //axisTitlesPosition: "out",
                hAxis: {
                  logScale: false,
                  slantedText: false,
                  format: 'short',
                  textStyle: {
                    color: '#FFFFFF',
                    frontSize: 10
                  },
                  titleTextStyle: {
                    color: '#FFFFFF',
                    frontSize: 16
                  }
                },
                vAxis: {

                  direction: 1,
                  textStyle: {
                    color: '#FFFFFF'
                  }

                },
                animation:{
                    duration: 100,
                    easing: 'linear'
                }
            };
        },
        getAreaOptions: function () {
            return {
                width: 370,
                height: 320,
                titleTextStyle: {
                    color: '#FFFFFF',
                    frontSize: 20
                  },
                legend: {
                    position: "bottom",
                  textStyle: {
                    color: '#FFFFFF'
                  }
                },
                backgroundColor:{
                    fill: "#17202a"
                },
                chartArea: {
                    right: 10,
                    width: 300,
                    height: 280,
                    backgroundColor: "#17202a"
                }
            };
        }
    };
});


