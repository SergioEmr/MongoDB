/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define([
    "luciad/geometry/constructive/ConstructiveGeometryFactory",
    "samplecommon/LayerConfigUtil",
    "luciad/shape/ShapeType",
    "dojo/on",
    "dojo/dom",
    "dojo/query",
    
    "luciad/view/controller/BasicCreateController",
    "luciad/view/controller/EditController",
    "luciad/model/feature/Feature",
    "luciad/geometry/constructive/ConstructiveGeometryFactory",
    
    "luciad/util/Promise",
    "samples/common/store/RestStore",
    "luciad/model/feature/FeatureModel",
    "luciad/view/feature/FeatureLayer",
    "luciad/model/codec/GeoJsonCodec",
    "./CustomJsonCodec",
    "luciad/model/store/MemoryStore",
    "template/sample"
], function (ConstructiveGeometryFactory, LayerConfigUtil, ShapeType, on, dom, query,
        BasicCreateController, EditController, Feature, ConstructiveGeometryFactory,
        Promise, RestStore, FeatureModel, FeatureLayer, GeoJsonCodec, CustomJsonCodec, MemoryStore,
        sampleTemplate) {
    
   
    function editableLayerAndButtons(map, reference, o, url, botones) {
        
        var options = o || {id: "dibujo", selectable: true, editable: true, label: "Capa Dibujo"};
        var capadibujo ;
        if(url === null)
            capadibujo = crearMemoryStoreLayer(reference, options);
        else if(url === undefined || url === "") {
            capadibujo = createFeatureLayer(reference, options, "/createandedit");
        } else {
            capadibujo = createFeatureLayer(reference, options, url);
        }
        var geometry = ConstructiveGeometryFactory.createEllipsoidal(reference);
        capadibujo.onCreateContextMenu = function(contextMenu, map, contextMenuInfo) {
            var selectedObjects = getLayerSelectedFeatures(map, contextMenuInfo.layer);
            if (selectedObjects && selectedObjects.length > 1) {
            contextMenu.addItem({
                label: "Union",
                action: function() {
                    var union = geometry.union(getSelectedShapes(selectedObjects));
                    addGeometryToModel(union, contextMenuInfo.layer, map);
                }
            });
            contextMenu.addItem({
                label: "Intersection",
                action: function() {
                    var intersection = geometry.intersection(getSelectedShapes(selectedObjects));
                    addGeometryToModel(intersection, contextMenuInfo.layer, map);
                }
            });
            contextMenu.addItem({
                label: "Difference",
                action: function() {
                    var difference = geometry.difference(getSelectedShapes(selectedObjects));
                    addGeometryToModel(difference, contextMenuInfo.layer, map);
                }
            });
            } else {
                LayerConfigUtil.createContextMenu(contextMenu, map, contextMenuInfo);
            }
        };
        if(map !== null || map !== undefined && botones)
            crearBotones(map, botones);
        return capadibujo;
    }
    
    function editableLayer(reference, options, url) {
        return editableLayerAndButtons(null, reference, options, url, null);
    }
    function crearMemoryStoreLayer(reference, options) {
        var store = new MemoryStore();
        var featureModel = new FeatureModel(store, {reference: reference});
        return new FeatureLayer(featureModel, options);
    }
    function createFeatureLayer(reference, options, url) {
        var codec = new CustomJsonCodec();//GeoJsonCodec();
        var store= new RestStore({target: url, codec: codec});
        var featureModel = new FeatureModel(store, {reference: reference});
        return new FeatureLayer(featureModel, options);
    }
    var b = [{tipo:"point", div: "div"}, {tipo: "", div:""}];
    function crearBotones(map, botones){
        try {
            for(var i in botones) {
                var tipo = botones[i].tipo, div = botones[i].div;
                switch(tipo.toLowerCase()) {
                    default: console.log("no se reconoce el tipo de figura "+tipo); break;
                    case "point": linkCreateButton(map, div, ShapeType.POINT); break;
                    case "polyline": linkCreateButton(map, div, ShapeType.POLYLINE); break;
                    case "polygon": linkCreateButton(map,div, ShapeType.POLYGON); break;
                    case "circle":
                    case "circlebycenterpoint": linkCreateButton(map, div, ShapeType.CIRCLE_BY_CENTER_POINT); break;
                    case "edit": botoneditar(map, div); break;
                    case "borrar": botondelete(map, div); break;
                }
            }
        } catch(e) {
             console.log(e);
        }
        /*
        linkCreateButton(map,"createpoint", ShapeType.POINT);
        linkCreateButton(map,"createpolyline", ShapeType.POLYLINE);
        linkCreateButton(map,"createpolygon", ShapeType.POLYGON);
        linkCreateButton(map, "createcirclecenterpoint", ShapeType.CIRCLE_BY_CENTER_POINT);
        botoneditar(map);
        botondelete(map);*/
    }
    
    function getLayerSelectedFeatures(map, layer) {
        var i;
        var selection = map.selectedObjects;
        for (i = 0; i < selection.length; i++) {
            if (selection[i].layer === layer) {
                return selection[i].selected; //array of the selected objects for layer
            }
        }
        return null;
    }
    function getSelectedShapes(featuresArray) {
        var selectedShapes = [];
        for(var i = 0; i < featuresArray.length; i++) {
            selectedShapes.push(featuresArray[i].geometry);
        }
        return selectedShapes;
    }
    function addGeometryToModel(geometry, layer, map) {
        // If the result of the operation is an empty shape list don't enable the edit controller
        if (!(ShapeType.SHAPE_LIST === geometry.type && geometry.shapeCount === 0)) {
            var feature = new Feature(geometry, {});
            var promiseId = layer.model.add(feature);

            Promise.when(promiseId, function() {
                map.clearSelection();
                map.controller = new EditController(layer, feature, {finishOnSingleClick: true});
            });
        }
    }
    function botoneditar(map, div){
        var domNode = document.getElementById( div || "edit" );
        domNode.addEventListener( "click", editAction );
        function editAction() {
            var selected = fetchSelectedObject( map );
            if ( selected ) {
                map.controller = new EditController( selected.layer, selected.feature );
            }
        }
    }
    function botondelete(map, div){
        var domNode = document.getElementById( div || "delete" );
        domNode.addEventListener( "click", deleteAction );
        function deleteAction() {
            var selected = fetchSelectedObject( map );
            if ( selected )
                selected.layer.model.remove( selected.feature.id );
        }
    }
    function fetchSelectedObject( map ) {
        var objects = map.selectedObjects;
        for ( var idx = 0; idx < objects.length; idx++ ) {
            if ( objects[idx].selected.length > 0 && objects[idx].layer.editable && objects[idx].layer.id === "dibujo" ) {
                return {
                    layer: objects[idx].layer,
                    feature: objects[idx].selected[0]
                };
            }
        }
    }
    function linkCreateButton(map, domId, shapeType) {
        try{
        on(dom.byId(domId), "click", function() {
            var stylePressed = "pressed";
            query(".pressed").removeClass(stylePressed);
            query("#" + domId).addClass(stylePressed);
            map.selectObjects([]);
            var createController = new BasicCreateController(shapeType, {}, {finishOnSingleClick: true});
            createController.onObjectCreated = function() {
                BasicCreateController.prototype.onObjectCreated.apply(this, arguments);
                query(".pressed").removeClass(stylePressed);
            };
            map.controller = createController;
        });
        }catch(e) {
            console.log("no se creo el control para el boton "+ domId);
            console.log(e);
        }
    }
    
    return {
        createEditableLayerAndButtons: editableLayerAndButtons,
        createEditableLayer: editableLayer,
        editableLayerAndButtons: editableLayerAndButtons,
        linkCreateButton: linkCreateButton,
        crearBotones: crearBotones
    };
});


    
    