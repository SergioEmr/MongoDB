define(["luciad/shape/ShapeFactory"], function(ShapeFactory) {

  /**
   * Creates a 3D cone with given base and top radius, height and number of subdivisions
   * of the side surface.
   * A higher subdivision number will ensure a smoother appearance of the side surface of the cone.
   * @param baseRadius the base radius of the cone
   * @param topRadius the top radius of the cone
   * @param height the cone height
   * @param sliceCount the number of subdivisions of the side surface of the cone around the cone main axis.
   **/

  function Cone3DMesh(baseRadius, topRadius, height, sliceCount) {
    this._baseRadius = baseRadius;
    this._topRadius = topRadius;
    this._height = height;
    this._sliceCount = sliceCount;

    this._indices = [];
    this.zOffset = 0;
  }

  Cone3DMesh.prototype.createVertices = function() {
    var vertices = [];
    var point;
    var dPhi = 2 * Math.PI / (this._sliceCount);
    var x0, y0, baseZ, x1, y1, topZ;
    baseZ = -0.5 * this._height + this.zOffset;
    topZ = 0.5 * this._height + this.zOffset;

    var offset = 0;

    // do side surface
    for (var i = 0; i <= this._sliceCount; i++) {
      var phi = i * dPhi;
      var nx = Math.cos(phi);
      var ny = Math.sin(phi);
      x0 = this._baseRadius * nx;
      y0 = this._baseRadius * ny;
      x1 = this._topRadius * nx;
      y1 = this._topRadius * ny;
      // base vertex
      point = ShapeFactory.createPoint(null, [x0, y0, baseZ]);
      vertices.push(point.x);
      vertices.push(point.y);
      vertices.push(point.z);
      this._indices.push(offset + 2 * i);
      // top vertex
      point = ShapeFactory.createPoint(null, [x1, y1, topZ]);
      vertices.push(point.x);
      vertices.push(point.y);
      vertices.push(point.z);
      this._indices.push(offset + 2 * i + 1);
    }
    offset = this._indices.length;

    // do base surface
    for (i = 0; i <= this._sliceCount; i++) {
      phi = i * dPhi;
      nx = Math.cos(phi);
      ny = Math.sin(phi);
      x0 = 0;
      y0 = 0;
      x1 = this._baseRadius * nx;
      y1 = this._baseRadius * ny;
      // inner vertex
      point = ShapeFactory.createPoint(null, [x0, y0, baseZ]);
      vertices.push(point.x);
      vertices.push(point.y);
      vertices.push(point.z);
      this._indices.push(offset + 2 * i);
      // outer vertex
      point = ShapeFactory.createPoint(null, [x1, y1, baseZ]);
      vertices.push(point.x);
      vertices.push(point.y);
      vertices.push(point.z);
      this._indices.push(offset + 2 * i + 1);
    }
    offset = this._indices.length;

    // do top surface
    for (i = 0; i <= this._sliceCount; i++) {
      phi = i * dPhi;
      nx = Math.cos(phi);
      ny = Math.sin(phi);
      x0 = 0;
      y0 = 0;
      x1 = this._topRadius * nx;
      y1 = this._topRadius * ny;
      // inner vertex
      point = ShapeFactory.createPoint(null, [x0, y0, topZ]);
      vertices.push(point.x);
      vertices.push(point.y);
      vertices.push(point.z);
      this._indices.push(offset + 2 * i);
      // outer vertex
      point = ShapeFactory.createPoint(null, [x1, y1, topZ]);
      vertices.push(point.x);
      vertices.push(point.y);
      vertices.push(point.z);
      this._indices.push(offset + 2 * i + 1);
    }

    return vertices;
  };

  Cone3DMesh.prototype.createIndices = function() {
    if (this._indices.length === 0) {
      this.createVertices();
    }

    var triangles = [];
    for (var i = 0; i < 3; i++) {
      var numberOfIndicesPerSide = this._indices.length / 3;
      for (var j = 1; j < numberOfIndicesPerSide - 1; j++) {
        triangles.push(this._indices[j - 1 + i * numberOfIndicesPerSide]);
        triangles.push(this._indices[j + i * numberOfIndicesPerSide]);
        triangles.push(this._indices[j + 1 + i * numberOfIndicesPerSide]);
      }
    }

    return triangles;
  };

  Cone3DMesh.prototype.createNormals = function() {
    var normals = [];
    var point;
    var dPhi = 2 * Math.PI / (this._sliceCount);

    // do side surface
    for (var i = 0; i <= this._sliceCount; i++) {
      var phi = i * dPhi;
      var nx = Math.cos(phi);
      var ny = Math.sin(phi);
      // base vertex
      point = ShapeFactory.createPoint(null, [nx, ny, 0]);
      normals.push(point.x);
      normals.push(point.y);
      normals.push(point.z);
      // top vertex
      normals.push(point.x);
      normals.push(point.y);
      normals.push(point.z);
    }

    // do base surface
    for (i = 0; i <= this._sliceCount; i++) {
      // inner vertex
      point = ShapeFactory.createPoint(null, [0, 0, -1]);
      normals.push(point.x);
      normals.push(point.y);
      normals.push(point.z);
      // outer vertex
      normals.push(point.x);
      normals.push(point.y);
      normals.push(point.z);
    }

    // do top surface
    for (i = 0; i <= this._sliceCount; i++) {
      // inner vertex
      point = ShapeFactory.createPoint(null, [0, 0, 1]);
      normals.push(point.x);
      normals.push(point.y);
      normals.push(point.z);
      // outer vertex
      normals.push(point.x);
      normals.push(point.y);
      normals.push(point.z);
    }

    return normals;
  };

  Cone3DMesh.prototype.createTextureCoordinates = function() {
    var texCoords = [];
    var dPhi = 2 * Math.PI / (this._sliceCount);

    // do side surface
    for (var i = 0; i <= this._sliceCount; i++) {
      var phi = i * dPhi;
      var tx = phi / (2 * Math.PI);
      // base vertex
      texCoords.push(tx); // u
      texCoords.push(0);  // v
      // top vertex
      texCoords.push(tx); // u
      texCoords.push(1);  // v
    }

    // do base surface
    for (i = 0; i <= this._sliceCount; i++) {
      phi = i * dPhi;
      tx = phi / (2 * Math.PI);
      // inner vertex
      texCoords.push(tx); // u
      texCoords.push(0);  // v
      // outer vertex
      texCoords.push(tx); // u
      texCoords.push(1);  // v
    }

    // do top surface
    for (i = 0; i <= this._sliceCount; i++) {
      phi = i * dPhi;
      tx = phi / (2 * Math.PI);
      // inner vertex
      texCoords.push(tx); // u
      texCoords.push(0);  // v
      // outer vertex
      texCoords.push(tx); // u
      texCoords.push(1);  // v
    }

    return texCoords;
  };

  return Cone3DMesh;
});
