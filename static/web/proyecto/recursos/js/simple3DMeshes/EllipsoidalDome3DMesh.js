define(["./Ellipsoid3DMesh"], function(Ellipsoid3DMesh) {

  /**
   * Creates a 3D ellipsoidal dome with given radial dimensions in X, Y, and Z axis, and with the given number of
   * vertical and horizontal subdivisions of the surface.
   *
   * A dome can be obtained by setting the three radial parameters to the same value.
   *
   * @param {Number} radiusX the radial dimension along the X axis
   * @param {Number} radiusY the radial dimension along the Y axis
   * @param {Number} radiusZ the radial dimension along the Z axis
   * @param {Number} verticalSlicesCount the number of vertical subdivisions of the surface (similar to lines of longitude)
   * @param {Number} horizontalSlicesCount the number of horizontal subdivisions of the surface (similar to lines of latitude)
   *
   **/

  function EllipsoidalDome3DMesh(radiusX, radiusY, radiusZ, verticalSlicesCount, horizontalSlicesCount) {
    Ellipsoid3DMesh.call(
        this,
        radiusX,
        radiusY,
        radiusZ,
        verticalSlicesCount % 2 === 0 ? verticalSlicesCount : verticalSlicesCount + 1,
        horizontalSlicesCount % 2 === 0 ? horizontalSlicesCount : horizontalSlicesCount + 1
    );

    this._horizontalSlicesEndIndex = this._horizontalSlicesCount / 2;
  }

  EllipsoidalDome3DMesh.prototype = Object.create(Ellipsoid3DMesh.prototype);

  return EllipsoidalDome3DMesh;
});
