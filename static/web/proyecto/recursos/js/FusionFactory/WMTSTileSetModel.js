define([
  "dojo/_base/lang",
  "luciad/model/tileset/UrlTileSetModel",
  "luciad/error/ProgrammingError"
], function(dLang, UrlTileSetModel, ProgrammingError) {

  /**
   * The WMTS tileset model inherits from UrlTileSetModel.
   */
  function WMTSTileSetModel(options) {
    if (!options ||
        !dLang.isString(options.url) ||
        !dLang.isString(options.layer) ||
        !dLang.isString(options.style) ||
        !dLang.isString(options.format) ||
        !dLang.isString(options.tileMatrixSet) ||
        !dLang.isArray(options.tileMatrices)) {
      console.dir(options);
      throw new ProgrammingError("WMTSTileSetModel::cannot construct WMTS model without url, layer, style, format and tileMatrix parameters");
    }

    UrlTileSetModel.call(this, {
      reference: options.reference,
      level0Columns: options.level0Columns,
      level0Rows: options.level0Rows,
      levelCount: options.levelCount,
      tileWidth: options.tileWidth,
      tileHeight: options.tileHeight,
      bounds: options.bounds,
      baseURL: options.url +
               "?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&" +
               "&LAYER=" + options.layer +
               "&STYLE=" + options.style +
               "&FORMAT=" + options.format +
               "&TILEMATRIXSET=" + options.tileMatrixSet
    });

    this.modelDescriptor = {
      source: options.url,
      name: options.layer || options.name,
      description: "OGC WMTS Model",
      type: "tileset/image"
    };

    this.level0Rows = options.level0Rows;
    this.tileMatrixSet = options.tileMatrixSet;
    this.tileMatrices = options.tileMatrices;
  }

  WMTSTileSetModel.prototype = Object.create(UrlTileSetModel.prototype);
  WMTSTileSetModel.constructor = WMTSTileSetModel;

  //#snippet getTileUrl
  //#description retrieve a URL representing a tiled image, using the WMTS getTile protocol.
  WMTSTileSetModel.prototype.getTileURL = function(baseURL, tile) {
    var tileMatrix = this.tileMatrices[tile.level];
    return baseURL +
           "&TILEMATRIX=" + tileMatrix +
           "&TILEROW=" + ((this.level0Rows * Math.pow(2, tile.level)) - 1 - tile.y) +
           "&TILECOL=" + tile.x;
  };
  //#endsnippet getTileUrl

  return WMTSTileSetModel;
});
