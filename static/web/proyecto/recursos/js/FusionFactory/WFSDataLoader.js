define([
  "luciad/reference/ReferenceProvider",
  "luciad/view/feature/FeatureLayer",
  "luciad/model/feature/FeatureModel",
  "luciad/model/store/WFSFeatureStore",
  "luciad/model/codec/GeoJsonCodec",
  "luciad/uom/UnitOfMeasureRegistry",
  "luciad/model/codec/GMLCodec",
], function(ReferenceProvider, FeatureLayer, FeatureModel, WFSFeatureStore, GeoJsonCodec, UnitOfMeasureRegistry, GMLCodec) {

  return {

    /**
     * Creates a layer for WFS data.
     *
     * You can style WFS data using a FeaturePainter.
     * See for example the <i>Vector Data<i> sample.
     *
     * Summary:
     * - Create a {@link luciad/model/store/WFSFeatureStore} to access the server
     * - Create a {@link luciad/model/feature/FeatureModel} for the store
     * - Create a {@link luciad/view/feature/FeatureLayer}
     * - Create a {@link luciad/view/feature/FeaturePainter} to style the layer (optional)
     */

    createLayer: function(newReference, options, url, id, methods) {
        var reference;
        if(newReference) 
            reference = newReference;
        else
            reference = ReferenceProvider.getReference("urn:ogc:def:crs:OGC::CRS84");
      //console.log(reference);
      var versions = ["3.2.0", "1.1.0"];
      methods = methods || ["GET"];
      id = id || options.label;
      //#snippet createWFSModel
      var store = new WFSFeatureStore({
        serviceURL: url,
        typeName: id,
        reference: reference,
        //codec: new GMLCodec(),
        //outputFormat: "text/xml; subtype=gml/3.1.1",
        outputFormat: "application/json",
        codec: new GeoJsonCodec(),
        versions: [versions],
        methods: methods
      });
      console.log("Conectandoce al Servidor de Fusion..." + url);
      var model = new FeatureModel(store, {reference: reference});
      options.label = options.label + " (WFS)";
      var layer = new FeatureLayer(model, options);
      //#endsnippet createWFSModel

      return layer;
    }
  };

});
