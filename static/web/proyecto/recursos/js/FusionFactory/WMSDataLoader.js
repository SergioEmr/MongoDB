    define([
  "luciad/reference/ReferenceProvider",
  "luciad/model/tileset/WMSTileSetModel",
  "luciad/view/tileset/WMSTileSetLayer",
  "luciad/model/image/WMSImageModel",
  "luciad/view/image/WMSImageLayer",
  "luciad/view/tileset/RasterTileSetLayer",
  "luciad/view/LayerType",
], function(ReferenceProvider, WMSTileSetModel, WMSTileSetLayer, WMSImageModel, WMSImageLayer, 
RasterTileSetLayer, LayerType) {

  return {

    /**
     * Creates a layer for WMS data.
     *
     * This class uses Web Mercator as reference by default.
     *
     * Summary:
     * - Create a {@link luciad/model/tileset/WMSTileSetModel}
     * - Create a {@link luciad/view/tileset/RasterTileSetLayer}
     *
     * Alternatively, to use single-image WMS:
     * - Create a {@link luciad/model/image/WMSImageModel}
     * - Create a {@link luciad/view/image/RasterImageLayer}
     */
    createLayer: function (reference, layerOptions, url, ids) {
    //createLayer: function(url, layerName, layerId, layerOptions, reference, queryable) {
        layerOptions = layerOptions || {};
        if(typeof ids === "string")
            ids = [ids];
      var modelOptions = {
        getMapRoot: url,
        version: "1.3.0",
        layers: ids,
        reference: reference || ReferenceProvider.getReference( "CRS:84" ),
       // reference: ReferenceProvider.getReference("EPSG:3857"),
        transparent: true
      };

      if (layerOptions.queryable !== false) {
        modelOptions.queryLayers = ids;
      }

      var model, layer;
     /* if(!layerOptions.label) {
          layerOptions.label = layerName + " (WMS)";
      }*/
      layerOptions.layerType = layerOptions.layerType || LayerType.BASE;
        model = new WMSTileSetModel(modelOptions);
        //layer = new WMSTileSetLayer(model, {label: layerName + " (WMS)"});
        layer = new RasterTileSetLayer(model, layerOptions);
      

      return layer;
    }
  };

});

