define([
  "luciad/shape/ShapeType",
  "luciad/view/feature/BasicFeaturePainter",
  "luciad/view/feature/FeaturePainter",
  "luciad/view/feature/transformation/ClusteringTransformer",
  "luciad/view/style/PointLabelPosition",
  "samples/common/IconFactory"
], function(ShapeType, BasicFeaturePainter, FeaturePainter, ClusteringTransformer,
            PointLabelPosition, IconFactory) {

  var size = 64;
  var DEFAULT_CLUSTER_ICON_STYLE = {
    draped: true,
    width: size + "px",
    height: size + "px",
    image: IconFactory.circle({
      width: size,
      height: size,
      fill: "rgba(255, 255, 255,1)",
      stroke: "rgba(0, 78, 146, 1)",
      strokeWidth: size / 8
    }),
    zOrder: 3
  };

  var DEFAULT_CLUSTER_ICON_SELECTED_STYLE = {
    draped: true,
    width: size + "px",
    height: size + "px",
    image: IconFactory.circle({
      width: size,
      height: size,
      fill: "rgba(255, 255, 255, 1)",
      stroke: "rgba(192, 217, 42, 1)",
      strokeWidth: size / 8
    }),
    zOrder: 3
  };

  /**
   * Painter wrapper that paints non-clustered elemenst with the given painter, and clusters separately.
   *
   * If you want to change how clusters are painted, override the paintClusterBody and/or paintClusterLabel
   *
   * @param delegatePainter The FeaturePainter with which to paint features that are not in a cluster.
   * @constructor
   */
  function ClusteringPainterWrapper(delegatePainter) {
    BasicFeaturePainter.call(this);
    this.setStyle(ShapeType.POINT, {selected: false}, DEFAULT_CLUSTER_ICON_STYLE);
    this.setStyle(ShapeType.POINT, {selected: true}, DEFAULT_CLUSTER_ICON_SELECTED_STYLE);
    this._delegatePainter = delegatePainter;
  }

  ClusteringPainterWrapper.prototype = new BasicFeaturePainter();
  ClusteringPainterWrapper.prototype.constructor = ClusteringPainterWrapper;

  ClusteringPainterWrapper.prototype.getDetailLevelScales = function(layer, map) {
    return this._delegatePainter.getDetailLevelScales(layer, map);
  };

  ClusteringPainterWrapper.prototype.paintBody = function(geoCanvas, feature, shape, layer, map, state) {
    if (ClusteringTransformer.isCluster(feature)) {
      this.paintClusterBody(geoCanvas, feature, shape, layer, map, state);
    }
    else if (typeof this._delegatePainter.paintBody === "function") {
      this._delegatePainter.paintBody(geoCanvas, feature, shape, layer, map, state);
    }
  };

  ClusteringPainterWrapper.prototype.paintClusterBody = function(geoCanvas, feature, shape, layer, map, state) {
    BasicFeaturePainter.prototype.paintBody.apply(this, arguments);
  };

  ClusteringPainterWrapper.prototype.paintLabel = function(labelCanvas, feature, shape, layer, map, state) {
    if (ClusteringTransformer.isCluster(feature)) {
      this.paintClusterLabel(labelCanvas, feature, shape, layer, map, state);
    }
    else if (typeof  this._delegatePainter.paintLabel === "function") {
      this._delegatePainter.paintLabel(labelCanvas, feature, shape, layer, map, state);
    }
  };

  ClusteringPainterWrapper.prototype.paintClusterLabel = function(labelCanvas, feature, shape, layer, map, state) {
    labelCanvas.drawLabel("<span style='color: rgb(0, 78, 146);'>" + feature.properties.clusteredElements.length + "</span>", shape.focusPoint, {
      positions: PointLabelPosition.CENTER
    });
  };

  ClusteringPainterWrapper.prototype.paintBorderBody = function(borderGeoCanvas, feature, shape, layer, map, state) {
    if (ClusteringTransformer.isCluster(feature)) {
      this.paintClusterBorderBody(borderGeoCanvas, feature, shape, layer, map, state);
    }
    else if (typeof this._delegatePainter.paintBorderBody === "function") {
      this._delegatePainter.paintBorderBody(borderGeoCanvas, feature, shape, layer, map, state);
    }
  };

  ClusteringPainterWrapper.prototype.paintClusterBorderBody = function(borderGeoCanvas, feature, shape, layer, map, state) {
    //nothing by default
  };

  ClusteringPainterWrapper.prototype.paintBorderLabel = function(borderLabelCanvas, feature, shape, layer, map, state) {
    if (ClusteringTransformer.isCluster(feature)) {
      this.paintClusterBorderLabel(borderLabelCanvas, feature, shape, layer, map, state);
    }
    else if (typeof this._delegatePainter.paintBorderLabel === "function"){
      this._delegatePainter.paintBorderLabel(borderLabelCanvas, feature, shape, layer, map, state);
    }
  };

  ClusteringPainterWrapper.prototype.paintClusterBorderLabel = function(borderLabelCanvas, feature, shape, layer, map, state) {
    //nothing by default
  };

  return ClusteringPainterWrapper;
});