/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define([
    "luciad/model/feature/FeatureModel",
    "luciad/view/feature/FeatureLayer",
    "../CustomJsonCodec",
    "./ClusterPainter",
    "./CustomClusterPainter",
    "./ClusterBalloon",
    "luciad/model/store/MemoryStore",
    "../LocalFileStore",
    "samples/common/store/RestStore",
], function (FeatureModel, FeatureLayer, CustomJsonCodec, ClusterPainter, CustomClusterPainter, ClusterBalloon,
MemoryStore, LocalFileStore, RestStore) {
    function createClusterLayer(reference, options, clusterOptions, urlDataOrFile) {
        var codec = new CustomJsonCodec();
        var store;
        if(typeof clusterOptions === 'string' && !urlDataOrFile) {
            urlDataOrFile = clusterOptions;
            clusterOptions = {};
        }
        if(urlDataOrFile) {
            if (typeof urlDataOrFile === 'string') {
                store = new RestStore({target: urlDataOrFile, codec: codec});
                if(!options.label)
                    options.label = urlDataOrFile.toLowerCase().replace(/.*\./, "");;
            } else {
                if(typeof urlDataOrFile === "array")
                    store = new MemoryStore({codec: codec, data: urlDataOrFile});
                else
                    store = new LocalFileStore(urlDataOrFile, codec);
            }
        } else {
            store = new MemoryStore({codec: codec});
        }
        var featureModel = new FeatureModel(store, {reference: reference});
        var painter = options.painter? CustomClusterPainter.newCluster(options.painter): ClusterPainter.painter;
        
        var layer = new FeatureLayer(featureModel, {
                label: options.label? options.label: "Cluster Layer",  
                selectable: options.selectable? options.selectable: true, 
                editable: options.editable? options.editable: false, 
                visible: options.visible? options.visible: true,
                painter: painter.painter,
                transformer: painter.transformer.create({
                    classifier: {
                        getClassification: function (object) {
                            return object.properties.isActive;
                        }
                    },
                    defaultParameters: {
                        clusterSize: clusterOptions.clusterSize? clusterOptions.clusterSize: 200,
                        minimumPoints: clusterOptions.minimumPoints? clusterOptions.minimumPoints: 5
                    }
                })
            });
        if(options.includeBalloon !== false) {
            layer.balloonContentProvider = function (feature) {
                return ClusterBalloon.getBalloon(feature);
            };
        }
        return layer;
    }
    return {createClusterLayer: createClusterLayer};
});
