/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define ([
    "luciad/view/feature/transformation/ClusteringTransformer",
    "./ClusterIconStyleUtil",
    "samples/common/IconFactory",
    "luciad/view/feature/BasicFeaturePainter",
    "./ClusteringPainterWrapper",
    "luciad/view/style/PointLabelPosition"
], function (ClusteringTransformer, ClusterIconStyleUtil, IconFactory, BasicFeaturePainter,
        ClusteringPainterWrapper, PointLabelPosition) {
    var armedEventsPainter;
    function createClusterPainter( painter) {
        this.armedEventsPainter = painter;
    }
    
    var EVENT_STYLE = {
        draped: true,
        width: "16px",
        height: "16px",
        image: IconFactory.circle({
            width: 16,
            height: 16,
            fill: "rgba(0, 78, 146, 0.8)",
            stroke: "rgba(155, 180, 222, 0.8)",
            strokeWidth: 1
            })
    };

    var EVENT_SELECTED_STYLE = {
        draped: true,
        width: "16px",
        height: "16px",
        image: IconFactory.circle({
            width: 16,
            height: 16,
            fill: "rgba(0, 78, 146, 0.8)",
            stroke: "rgba(192, 217, 42, 0.8)",
            strokeWidth: 1.5
        })
    };
    
    if(!armedEventsPainter) {
        armedEventsPainter = new BasicFeaturePainter();

        armedEventsPainter.getDetailLevelScales = function() {
            return [1 / 3000000, 1 / 2000000, 1 / 1000000,1 / 50000,1 / 5000];
            //return [ 1 / 2000000 ];
        };
        var labelStyle = {
          positions: (PointLabelPosition.NORTH),
          offset: 5,
          priority : 2
        };
        armedEventsPainter.paintLabel = function (labelcanvas, feature, shape, layer, map, paintState) {
            if(!feature.properties.clusteredElements && paintState.level>2){

                var name = feature.properties.TITULAR;
                var active = feature.properties.isActive ? "OK": "NOK";
                var labelText;
                if(name)
                    labelText = '<div class="name blueColorTick theader sensorLabel blueColorLabel">' + name + '</div>';
                else
                    labelText = '<div class="name blueColorTick theader sensorLabel blueColorLabel">' + feature.id + '</div>';
                labelcanvas.drawLabel( labelText, shape, labelStyle );
                
            }
        };
        
        armedEventsPainter.paintBody = function(geocanvas, feature, shape, layer, map, paintState) {
            if (paintState.selected) {
                geocanvas.drawIcon(shape.focusPoint, EVENT_SELECTED_STYLE);
            } else {
                geocanvas.drawIcon(shape.focusPoint, EVENT_STYLE);
            }
        };
    }
    
    
    var clusterPainter = new ClusteringPainterWrapper(armedEventsPainter);
    clusterPainter.paintClusterBody = function(geocanvas, feature, shape, layer, map, paintState) {
      geocanvas.drawIcon(shape.focusPoint, ClusterIconStyleUtil.getOuterStyle(feature, paintState.selected));
      geocanvas.drawIcon(shape.focusPoint, ClusterIconStyleUtil.getInnerStyle(feature, paintState.selected));
      geocanvas.drawIcon(shape.focusPoint, ClusterIconStyleUtil.getTextStyle(feature));
      if (paintState.selected) {
        // Paint clustered elements when selecting a cluster
        var clusteredFeatures = ClusteringTransformer.clusteredFeatures(feature);
        for (var i = 0; i < clusteredFeatures.length; i++) {
          var clusteredFeature = clusteredFeatures[i];
          armedEventsPainter.paintBody(geocanvas, clusteredFeature, clusteredFeature.shape, layer, map, paintState);
        }
      }
    };

    clusterPainter.paintClusterLabel = function() {
      //don't paint the default cluster label
    };
    return {
        painter: clusterPainter,
        transformer: ClusteringTransformer
    };
});
