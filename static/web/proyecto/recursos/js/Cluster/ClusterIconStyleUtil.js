define([
  "luciad/view/feature/transformation/ClusteringTransformer",
  "samples/common/IconFactory"
], function(ClusteringTransformer, IconFactory) {

  var BASE_ICON_SIZE = 25;
  var INNER_ICON_SIZE_FACTOR = 0.7;

  function createStyleInnerCircle(size) {
    return {
      draped: true,
      width: size + "px",
      height: size + "px",
      image: IconFactory.circle({
        width: size,
        height: size,
        fill: "rgba(255,255,255,1)",
        stroke: "rgba(255,255,255,1)"
      }),
      zOrder: 3
    }
  }

  function createSelectedStyleInnerCircle(size) {
    return {
      draped: true,
      width: size + "px",
      height: size + "px",
      image: IconFactory.circle({
        width: size,
        height: size,
        fill: "rgba(255,255,255,0.6)",
        stroke: "rgba(255,255,255,0.6)"
      }),
      zOrder: 3
    }
  }

  function createStyleOuterCircle(size) {
    return {
      draped: true,
      width: size + "px",
      height: size + "px",
      image: IconFactory.circle({
        width: size,
        height: size,
        fill: "rgba(0, 78, 146, 0.8)",
        stroke: "rgba(0, 78, 146, 0.8)"
      }),
      zOrder: 2
    };
  }

  function createSelectedStyleOuterCircle(size) {
    return {
      draped: true,
      width: size + "px",
      height: size + "px",
      image: IconFactory.circle({
        width: size,
        height: size,
        fill: "rgba(192, 217, 42, 0.6)",
        stroke: "rgba(192, 217, 42, 0.6)"
      }),
      zOrder: 2
    };
  }

  function createStyleText(size, clusterSize) {
    return {
      draped: true,
      width: size + "px",
      height: size + "px",
      image: IconFactory.text(clusterSize, {
        width: size,
        height: size,
        fill: "rgba(0, 78, 146, 0.8)",
        font: "9pt Arial"
      }),
      zOrder: 4
    };
  }

  function getInnerStyle(aClusterFeature, aSelected) {
    var size = _getIconSize(ClusteringTransformer.clusteredFeatures(aClusterFeature).length, true);
    return aSelected ? createSelectedStyleInnerCircle(size) : createStyleInnerCircle(size);
  }

  function getOuterStyle(aClusterFeature, aSelected) {
    var size = _getIconSize(ClusteringTransformer.clusteredFeatures(aClusterFeature).length, false);
    return aSelected ? createSelectedStyleOuterCircle(size) : createStyleOuterCircle(size);
  }

  function getTextStyle(aClusterFeature) {
    var clusterSize = ClusteringTransformer.clusteredFeatures(aClusterFeature).length;
    var size = _getIconSize(clusterSize, false);
    return createStyleText(size, clusterSize);
  }

  function _getIconSize(aClusterSize, inner) {
    // Calculate an icon size, based on the cluster size
    var scaleFactor = Math.log(aClusterSize) / Math.log(15);
    scaleFactor = Math.min(Math.max(scaleFactor, 1), 3);
    var size = scaleFactor * BASE_ICON_SIZE;
    if (inner) {
      size *= INNER_ICON_SIZE_FACTOR;
    }
    size = Math.round(size);
    if (size % 2 == 0) {
      size = size + (inner ? 1 : -1);
    }
    return size;
  }

  return {
    getInnerStyle: getInnerStyle,
    getOuterStyle: getOuterStyle,
    getTextStyle: getTextStyle
  };
});