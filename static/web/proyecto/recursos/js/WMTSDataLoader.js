define([
  "./WMTSTileSetModel",
  "luciad/view/tileset/RasterTileSetLayer",
  "luciad/view/LayerType"
], function(WMTSTileSetModel, RasterTileSetLayer, LayerType) {

  return {

    /**
     * Creates a layer for WMTS data.
     *
     * Summary:
     * - Create a WMTS tileset model (sample code) with all details about the WMTS layer
     * - Create a {@link luciad/view/tileset/RasterTileSetLayer}
     */

    createLayer: function(layerName, options) {

      var model = new WMTSTileSetModel(options);

      var layer = new RasterTileSetLayer(model, {label: layerName + " (WMTS)", layerType: LayerType.BASE});

      return layer;
    }
  };

});
