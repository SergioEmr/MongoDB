define([
  "luciad/view/WebGLMap",
  //"luciad/view/Map",
  "samples/template/MapLoader!",
  "dojo/dom",
  "dojo/dom-construct",
  "dojo/request",
  "luciad/reference/ReferenceProvider",
  "luciad/shape/ShapeFactory",
  //"navigation/MouseZoomControl",
  "samples/common/navigation/ZoomControl",
  "samples/common/LayerConfigUtil",
  "samples/common/layertreecontrol/FoldableLayerControl",
  "samples/common/mouseLocation/MouseLocationComponent",
  "samples/common/URLUtil",
  "samples/common/html5layertreecontrol/HTML5LayerTreeControl",
  "./ElevationLayerService",
  "./BingMapsDataLoader",
  "./LayerFactory"
], function(WebGLMap, Map, dom, domConstruct, request, ReferenceProvider, ShapeFactory,
            ZoomControl, LayerConfigUtil, FoldableLayerControl, MouseLocationComponent, URLUtil,
            HTML5LayerTreeControl, ElevationLayerService, BingMapsDataLoader, LayerFactory) {

  function fitOnWorldOnLoad(map) {
    var viewSize = map.viewSize;
    var b;
    if (viewSize[0] === 0 || viewSize[1] === 0) {
      b = ShapeFactory.createBounds(ReferenceProvider.getReference("CRS:84"), [-180, 360, -90, 180]);
      map.resize();
      map.mapNavigator.fit({bounds: b, animate: true});
    }
  }

  var errorTags = {};
  var warningTags = {};

  return {
    fitOnWorldOnLoad: fitOnWorldOnLoad,

    makeMapAndComponents: function(domap, mapOptions, options, Map3d) {

      options = options || {};
      mapOptions = mapOptions || {};

      if (options.useUrlQueryParameters !== false) {
        options = URLUtil.getUrlQueryParameters(options);
        mapOptions = URLUtil.getUrlQueryParameters(mapOptions);
        delete options.webgl;
        delete mapOptions.webgl;
      }

      var warn = this.warn.bind(this);

      var map;
      if(Map3d=== true){
            map = new WebGLMap(domap, mapOptions);
        }
        else{
            map = new Map(domap, mapOptions);
        }
      fitOnWorldOnLoad(map);

      //zoom control
      if (options.includeZoomControl !== false) {
        var zoomControl = new ZoomControl(map, dom.byId(domap));
      }

      //background layer
      if (options.includeBackground !== false) {
        /*LayerConfigUtil.createFusionBackgroundLayer()
            .then(function(layer) {
              return layer;
            }, function(error) {
              warn("LFImagery",
                  "LuciadFusion server at " + LayerConfigUtil.PUBLIC_LUCIADFUSION_URL + " is not reachable.<br>A local image tileset layer is added to this sample instead.<br>In some cases, clearing your browser cache can help.");
              return LayerConfigUtil.createFallbackBackgroundLayer();
            }).then(function(layer) {
          map.layerTree.addChild(layer, "bottom");
        });*/
            var world = LayerFactory.createMapaLayer(ReferenceProvider.getReference("CRS:84"), "../../proyecto/recursos/Maps/world", "WorldBase");
            map.layerTree.addChild(world, "bottom");
      }

      //elevation layer in 3D
      if (options.includeElevation !== false && map.reference.identifier === "EPSG:4978") {
          var elevation = ElevationLayerService.createRealElevationLayer();
          map.layerTree.addChild(elevation);
        /*LayerConfigUtil.createFusionElevationLayer()
            .then(function(layer) {
              map.layerTree.addChild(layer);
            }, function(error) {
              warn("LFElevation",
                  "LuciadFusion server at " + LayerConfigUtil.PUBLIC_LUCIADFUSION_URL + "is not reachable.<br>No elevation layer is added to this sample.");
              return true;
            });*/
      }
      
      if(options.includeBingLayer) {
        try{
            if(typeof options.includeBingLayer === "string") {
                var bingType = options.includeBingLayer;
                BingMapsDataLoader.createBingLayer(bingType, map);
            } else {
                var bingTypes = options.includeBingLayer;
                if(bingTypes.length >0 && bingTypes.length <4) {
                    for(var j = 0; j<bingTypes.length; j++) {
                        BingMapsDataLoader.createBingLayer(bingTypes[j], map);
                    }
                }
            }
          
        }catch(e) { console.log(e);}
      }

      //mouse location controller
      if (options.includeMouseLocation !== false) {
        var mouseLocationComponent = new MouseLocationComponent(map);
      }

      //layer control
      var control;
      var lcNode = (typeof options.layerControlNode === "string") ? options.layerControlNode : domap;
      if (options.includeLayerControl !== false) {
        if (options.oldLayerControl === true) {
            control = new FoldableLayerControl(map, dom.byId(lcNode));
        } else {
            control = new HTML5LayerTreeControl(map, {noLayerDelete: options.noLayerDelete});
        }
        
        if(options.layerControlOpen === true)
            control.open();
        else
            control.close();
        

        //on small screens, close the layer control by default
        setTimeout(function() {
          var screenwidth = document.body.offsetWidth;
          var screenheight = document.body.offsetHeight;
          if (screenheight < 300 || screenwidth < 600) {
            control.close();
          } else {
            control.open();
          }
        }, 10);
        
        
      }
      

      return {
        map: map,
        mouseLocationComponent: mouseLocationComponent,
        zoomControl: zoomControl,
        layerControl: control
      };
    },

    makeMap: function(domap, mapOptions, options) {
      return this.makeMapAndComponents(domap, mapOptions, options, false).map;
    },
    
    makeMap3d: function(domap, mapOptions, options) {
      return this.makeMapAndComponents(domap, mapOptions, options, true).map;
    },
    makeWebGLMap: function(domap, mapOptions, options) {
      return this.makeMapAndComponents(domap, mapOptions, options, true).map;
    },
    destroyMapAndComponents: function(map) {
      map.destroy();//destroy existing map.
      if (map.zoomControl) {
        map.zoomControl.destroyController();
      }
      if (map.mouseLocationComponent) {
        map.mouseLocationComponent.destroyComponent();
      }
      if (map.layerControl) {
        map.layerControl.destroyController();
      }
    },

    toast: function(className, message) {
      var toasts = dom.byId("toasts");
      var toast = domConstruct.create("div", {"class": "toast " + className, innerHTML: message}, toasts, "last");

      setTimeout(function() {
        toast.style.opacity = 0;
        var hideToast = function() {
          toast.style.display = "none";
        };
        toast.addEventListener("transitionend", hideToast, false);
        toast.addEventListener("webkitTransitionEnd", hideToast, false);
      }, 12000);
    },

    info: function(message) {
      this.toast("", message);
      console.log(message);
    },

    error: function(tag, message, error) {
      if (tag && tag in errorTags) {
        //already logged errors for tag
        return;
      }
      errorTags[tag] = true;
      console.log(error || message);
      this.toast("error", message);
    },

    warn: function(tag, message, error) {
      if (tag && tag in warningTags) {
        //already logged warnings for tag
        return;
      }
      warningTags[tag] = true;
      console.log(error || message);
      this.toast("warning", message);
    },

    /**
     * Verifies if the URL is reachable, issuing a warning if not
     */
    probeURL: function(url) {
      var self = this;
      request(url).then(function(v) {
        // OK
      }, function(e) {
        self.warn(url, "Server at " + url + " not available.");
        console.log("Server at " + url + " not available.");
        console.log(e);
      });
    }
  };
});
