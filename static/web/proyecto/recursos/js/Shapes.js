/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define([
    "luciad/shape/ShapeFactory",
    "luciad/shape/ShapeType",
    "luciad/model/feature/Feature"
], function (ShapeFactory, ShapeType, Feature) {
    
    function createPolygonRandom (reference, rangoX, rangoY, rangoZ, n) {
        var x=[], y=[], z=[], poligonos = [], puntas;
        if(rangoZ === 0 || rangoZ === "undefined") {
            for(var j=0;j<x.length;j++)
                z[j] =0;
        }
        for(var i=0;i<n;i++) {
            puntas = getRandom(4, 11);
            for(var j=0;j<puntas;j++) {
                x[j] = getRandom(rangoX.min, rangoX.max);
                y[j] = getRandom(rangoY.min, rangoY.max);
            }
            poligonos[i] = createPolygon(reference, x, y, z, i, {});
        }
        return poligonos;
    }
    
    function createRandomPointWithTime(reference, rangoX, rangoY, rangoZ, n, startDate, endDate) {
        var x, y, z, k,a, poligonos = [], properties, fechas=startDate, fechaS, avance =0;
        if(rangoZ === 0 || rangoZ === "undefined") {
            z =0;
        }
        if(n>30)
            a=30;
        else
            a=n;
        fechaS = new Date(fechas*1000).toGMTString();//.toUTCString();
        avance = (endDate - startDate)/a;
        //console.log(fechaS + "\n"+ fechas+"\n"+avance);
        k=0;
        
        for(var j=0;j<a;j++) {
            for(var i=0;i<n;i++) {
                x = getRandom(rangoX.min, rangoX.max);
                y = getRandom(rangoY.min, rangoY.max);
                properties={EventTime: fechaS};
                poligonos[k] = createPoint(reference, x, y, z, k, properties);
                k++;
            }
            fechas+=avance;
            fechaS = new Date(fechas*1000).toGMTString();
        }
        return poligonos;
    }
    /*
     * 
     * @param {type} min
     * @param {type} max
     * @returns {Number}
     */
    function getRandom(min, max) {
        return Math.random() * (max - min) + min;
    }
    
    function createRandomPoint(reference, rangoX, rangoY, rangoZ, id, properties, n) {
        var x, y, z, k,a, poligonos = [];
        
        if(!n || n ===0) {
            n=1;
        }
        k=0;
        for(var i=0;i<n;i++) {
                x = getRandom(rangoX.min, rangoX.max);
                y = getRandom(rangoY.min, rangoY.max);
                if(rangoZ === 0 || rangoZ === "undefined") {
                    z =0;
                } else {
                    z = getRandom(rangoZ.min, rangoZ.max);
                }
                poligonos[i] = createPoint(reference, x, y, z, id, properties);
                id++;
            
        }
        return poligonos;
    }
    function movePolygon(polygon, direction) {
        
    }
    /*
     * 
     * @param {type} reference
     * @param {type} x
     * @param {type} y
     * @param {type} z
     * @param {type} i
     * @param {type} properties
     * @returns {ShapesL#10.Feature}
     */
    function createPolygon(reference, x, y, z, id, properties) {
        if(z === 0) {
            z= [];
            for(var j=0;j<x.length;j++)
                z[j] =0;
        }
        if(properties === "undefined" || properties === undefined) {
            properties = {};
        }
        return new Feature(
            ShapeFactory.createPolygon(reference, multiPoints(reference, x,y,z)), properties, id
        );
    }
    /*
     * 
     * @param {object} reference
     * @param {Array [] []} x
     * @param {Array [] []} y
     * @param {Array [] []} z
     * @param {int or String} i
     * @param {object json} properties
     * @returns {ShapesL#10.Feature}
     */
    function createMultiPolygon(reference, x, y, z, id, properties) {
        var poligonos =[], n = x.length;
        for(var i=0; i< n; i++) {
            if(z[i] === 0) {
                z[i]= [];
                for(var j=0;j<x.length;j++)
                    z[i][j] =0;
            }
            poligonos[i] = ShapeFactory.createPolygon(reference, multiPoints(reference, x[i],y[i],z[i]));
        }
        if(properties === "undefined" || properties === undefined) {
            properties = {};
        }
        return new Feature( ShapeFactory.createComplexPolygon(reference, poligonos), properties, id);
    }
    
    function createPolygonCoordinates(reference, coordinates, id, properties) {
        var x =[], y =[], z =[];
        var c = coordinates, max = 0;
        try {
            while(c.length <=1 && max<20) {
                c = c[0];
                max++;
            }
        } catch(e) {
            console.log("El Feature "+id+" no tiene coordenadas para un poligono");
            return createPolygon (reference, 0, 0, 0, id, properties);
        }
        if(max >= 19) {
            console.log("El Feature "+id+" no tiene coordenadas para un poligono");
            return createPolygon (reference, 0, 0, 0, id, properties);
        } 
        coordinates = c;
        for(var k=0; k<coordinates.length; k++) {
            x[k] = coordinates[k][0];
            y[k] = coordinates[k][1];
            z[k] = coordinates[k][2]? coordinates[2]: 0;
        }
        return createPolygon (reference, x, y, z, id, properties);
    }
    /*
     * 
     * @param {type} reference
     * @param {type} x
     * @param {type} y
     * @param {type} z
     * @param {type} i
     * @param {type} properties
     * @returns {ShapesL#10.Feature}
     */
    function createPolyline(reference, x, y, z, i, properties) {
        if(z === 0 || z === undefined) {
            z= [];
            for(var j=0;j<x.length;j++)
                z[j] =0;
        }
        if(properties === "undefined" || properties === undefined) {
            properties = {};
        }
        return new Feature(
            ShapeFactory.createPolyline(reference, multiPoints(reference, x,y,z)), properties, i
        );
    }
    /*
     * 
     * @param {type} reference
     * @param {type} x
     * @param {type} y
     * @param {type} z
     * @returns {Array}
     */
    function multiPoints(reference, x, y, z) {
        var points = [];
        for(var i=0; i<x.length;i++) {
            points[i] = ShapeFactory.createPoint(reference, [x[i], y[i], z[i]]);
        }
        return points;
    }
    
    function createPoint(reference, x, y, z, i, properties) {
        if(properties === undefined)
            properties = {};
        return new Feature(
            ShapeFactory.createPoint(reference, [x, y, z]), properties, i
        );
    }
    function createMultiPoints(reference, x, y, z, i, properties) {
        var points = [];
        for(var p in x) {
            points[p] = ShapeFactory.createPoint(reference, [x[p], y[p], z[p]]);
        }
        if(properties === undefined)
            properties = {};
        return new Feature(
            ShapeFactory.createComplexPolygon(reference, points), properties, i
        );
    }
    
    function crearFeatureTime (data, reference, startTime, endTime) {
        var i, j, n,m=0,v=getRandom(0,10),x=[],y=[], features = [],timestamps=[], eventTime=[], velocities =[],
                properties, feature, coordenada, distancia, tiempoAtencion, avance;
        var sFecha = new Date(startTime).toGMTString(), nFecha = startTime;
        var sTime, eTime;
        
        for(i=0; i<data.length;i++) {
            feature = data[i];
            avance =0;
            if(feature.geometry.type === 'LineString') {
                //avance = 1200000;
                coordenada = feature.geometry.coordinates;
                n= coordenada.length;
                if(feature.properties.startTime ) 
                    sTime = Date.parse(feature.properties.startTime);
                else 
                    sTime =startTime;
                if(feature.properties.endTime)
                    eTime = Date.parse(feature.properties.endTime);
                else {
                    eTime = endTime;
                }
                tiempoAtencion = eTime - sTime;
                sFecha = formatDate(new Date(sTime));
                //sFecha = new Date(sTime).toGMTString();
                nFecha = sTime;
                avance = (eTime - sTime)/n;
                properties = feature.properties;
                for(j=0; j<n; j++) {
                    if(j<n) {
                        x[j] = coordenada[j][0];
                        y[j] = coordenada[j][1];
                    }
                    eventTime [j]= sFecha;
                    timestamps[j] = nFecha;
                    velocities[j] = v;
                    v = getRandom(5, 15);
                    nFecha += avance;
                    sFecha = formatDate(new Date(nFecha));
                    //sFecha = new Date(nFecha).toGMTString();
                }
                var hours = Math.floor( tiempoAtencion / 3600000 );  
                var minutes = Math.floor( (tiempoAtencion % 3600000) / 60000 );
                var seconds = tiempoAtencion % 60000;
                //console.log(eventTime);
                var propiedades = Object.assign({
                    timestamps: timestamps,
                    EventTime: eventTime,
                    TiempoTotal: hours+" hr. "+minutes+" min. "+seconds+" seg.",
                    velocities: velocities,
                    //startTime: sTime,
                    endTime: eventTime[eventTime.length -1]
                }, properties);
                //properties = properties.menu.concat(feature.properties);
                
                features[i] = createPolyline(reference, x, y, 0, i, propiedades);
                eventTime = [];
                timestamps =[];
                velocities =[];
                x=[];
                y=[];
                v=getRandom(0,10);
            }
        }
        return features;
    }
    function formatDate(date) {
        var h = date.getHours() <10 ? "0"+date.getHours(): date.getHours();
        var m = date.getMinutes() <10 ? "0"+date.getMinutes(): date.getMinutes();
        var s = date.getSeconds() <10 ? "0"+date.getSeconds(): date.getSeconds();
        return date.getFullYear()+"/"+(date.getMonth()+1)+"/"+date.getDate()+" "+h+":"+m+":"+s;
    }
    
    function distancia(x1, y1, x2, y2 ) {
        return Math.sqrt(Math.pow(x2-x1,2) + Math.pow(y2-y1,2));
    }
    
    function createCircleByCenterPoint (reference, x, y, z, i, properties, r) {
        return new Feature(
            ShapeFactory.createCircleByCenterPoint(reference, ShapeFactory.createPoint(reference, [x, y, z]), r),
            properties, i
        );
    }
    
    function distancia(x1, y1, x2, y2 ) {
        return Math.sqrt(Math.pow(x2-x1,2) + Math.pow(y2-y1,2));
    }
    function distancia2Puntos(punto1, punto2) {
        var x1, x2, y1, y2, aux = true;
        x1 = punto1.geometry.focusPoint.x;
        y1 = punto1.geometry.focusPoint.y;
        x2 = punto2.geometry.focusPoint.x;
        y2 = punto2.geometry.focusPoint.y;
        if(!x1 || !y1 || !x2 || !y2) {
            aux = false;
            console.log("Alguno de los puntos es incorrecto");
            console.log("x1: "+x1+" y1: "+y1+" x1: "+x2+" y2: "+y2);
        }
        var a = Math.pow(x2-x1,2), b = Math.pow(y2-y1,2);
        var d = Math.sqrt(a + b);
        return aux === true? d: null;
    }
    /*
     * 
     * @param {type} reference
     * @param {type} data
     * @param {type} ids
     * @returns {Array}
     */
    function crearRuta(reference, data, ids) {
        var i,j, k, x=[],y=[];
        var route, coordenadas, properties, features = [];
        console.log("creando ruta");
        if(data.routes) {
            for(i=0; i<data.routes.length;i++) {
                route = data.routes[i];
                for(j=0;j<route.length;j++) {
                    coordenadas = route[j].geometry.coordinates;
                    for(k=0;k<coordenadas.length;k++){ 
                        x[k] = coordenadas[k][0];
                        y[k] = coordenadas[k][1];
                    }
                    properties = {
                        cost: route[j].cost,
                        distance: route[j].distance,
                        duration: route[j].duration,
                        summary: route[j].summary
                    };
                }
                if(ids || ids >=0)
                    features[i] = createPolyline(reference, x, y, 0, ids, properties);
                else
                    features[i] = createPolyline(reference, x, y, 0, i, properties);
                ids++;
                properties = {};
                x=[];
                y=[];
                route=[];
            }
        }
        return features;
    }
    /*
     * 
     * @param {type} reference
     * @param {type} coordinates
     * @param {type} i
     * @param {type} properties
     * @returns {ShapesL#10.Feature}
     */
    function createFeaturePolygon(reference, coordinates, i, properties) {
        var x = new Array(),y= new Array(),z = new Array(), c;
        //console.log("creado feature");
        if(coordinates.length === 1) {
            try {
                x[0] = coordinates[0][0][0];
                c = coordinates[0];
            }catch(e) {
                c = coordinates;
            }
            
        } else
            c = coordinates;
        for(var l=0; l<c.length; l++) {
            x[l] = c[l][0];
            y[l] = c[l][1];
            if(c[l].length ===3)
                z[l] = c[l][2];
            else
                z[l] = 0;
        }
        if(properties === "undefined" || properties === undefined) {
            properties = {};
        }
        return new Feature(
            ShapeFactory.createPolygon(reference, multiPoints(reference, x,y,z)), properties, i
        );
    }
    
    function createArcBandByPoint(reference, center, minR, maxR, startAngle, sweepAngle, id, properties) {
        if(properties === "undefined" || properties === undefined) {
            properties = {};
        }
        if(minR > maxR) {
            var x = minR;
            minR = maxR;
            maxR = x;
        }
        return new Feature(
            ShapeFactory.createArcBand(reference, center, minR, maxR, startAngle,sweepAngle), properties, id
        );
    }
    function createArcBand(reference, x, y, minR, maxR, startAngle, sweepAngle, id, properties) {
        var ArcBand = createArcBandByPoint(reference, ShapeFactory.createPoint(reference, [x, y]),
            minR, maxR, startAngle, sweepAngle, id, properties);
        return ArcBand;
    }
    
    function createNewFeature(reference, feature, id, properties) {
        id = id || feature.id ;
        var type = feature.geometry.type ;
        type = typeof type==="number"? getShapeType(type) : type;
        var coordinates = feature.geometry.coordinates;
        properties = properties || feature.properties || {};
        var x = [], y = [], z = [];
        switch(type.toLowerCase()) {
            case "point":
                return createPoint(reference, coordinates[0], coordinates[1], coordinates[2]||0, id, properties);
            case "polyline":
            case "linestring":
            case "line":
                if(coordinates[0][0].length)
                    coordinates = coordinates[0];
                x=[]; y=[]; z=[];
                for(var i=0; i<coordinates.length; i++) {
                    x[i] = coordinates[i][0];
                    y[i] = coordinates[i][1];
                    z[i] = coordinates[i][2] || 0;
                }
                return createPolyline(reference, x, y, z, id, properties);
            case "polygon":
                coordinates = coordinates[0];
                x=[]; y=[]; z=[];
                for(var i=0; i<coordinates.length; i++) {
                    x[i] = coordinates[i][0];
                    y[i] = coordinates[i][1];
                    z[i] = coordinates[i][2] || 0;
                }
                return createPolygon(reference, x, y, z, id, properties);
            case "multipolygon":
                coordinates = coordinates[0];
                x=[]; y=[]; z=[];
                for(var poligono in coordinates) {
                    var coor = coordinates[poligono];
                    x[poligono] = new Array();
                    y[poligono] = new Array();
                    z[poligono] = new Array();
                    for(var j=0;j<coor.length;j++) {
                        x[poligono][j] = coor[j][0];
                        y[poligono][j] = coor[j][1];
                        z[poligono][j] = coor[j][2] || 0;
                   }
                }
                return createMultiPolygon(reference, x, y, z, id, properties);
            default: break;
        }
    }
    
    function getShapeType(shapeOrType) {
        var type = typeof shapeOrType === "number"? shapeOrType: shapeOrType.type;
        if(ShapeType.contains(type, ShapeType.POLYGON))
            return "Polygon";
        if(ShapeType.contains(type, ShapeType.POINT))
            return "Point";
        if(ShapeType.contains(type, ShapeType.POLYLINE))
            return "LineString";
        if(ShapeType.contains(type, ShapeType.CIRCLE))
            return "CircleByCenterPoint";
        if(ShapeType.contains(type, ShapeType.CIRCLE_BY_3_POINTS))
            return "CircleBy3Point";
        if(ShapeType.contains(type, ShapeType.CIRCLE_BY_CENTER_POINT))
            return "CircleByCenterPoint";
        if(ShapeType.contains(type, ShapeType.EXTRUDED_SHAPE))
            return "Polygon";
        if(ShapeType.contains(type, ShapeType.BOUNDS))
            return "Bounds";
        if(ShapeType.contains(type, ShapeType.GEO_BUFFER))
            return "GeoBuffer";
        if(ShapeType.contains(type, ShapeType.SHAPE_LIST)) {
            var tipo, geometries = shapeOrType.geometries, x=true;
            var tipo2 = getShapeType(geometries[0]);
            for(var g in geometries) {
                tipo = getShapeType(geometries[g]);
                if(tipo === tipo2) {
                    x = true;
                } else {
                    x = false;
                    break;
                }
            }
            if(tipo === "MultiPolygon" && x)
                return tipo;
            return x? "Multi"+tipo: "ShapeList";
        }
        if(ShapeType.contains(type, ShapeType.COMPLEX_POLYGON))
            return "MultiPolygon";
        return "Desconosido";
    }
    
    function cloneFeature(feature) {
        var shape = feature.shape;
        var newFeature = createNewFeature(shape.reference, feature);
        return newFeature;
    }
    
    return {
        createPoint: createPoint,
        createPolygon: createPolygon,
        createPolygonRandom: createPolygonRandom,
        createRandomPointWithTime: createRandomPointWithTime,
        createRandomPoint: createRandomPoint,
        crearFeatureTime: crearFeatureTime,
        createCircleByCenterPoint: createCircleByCenterPoint,
        crearRuta: crearRuta,
        createFeaturePolygon: createFeaturePolygon,
        createPolyline: createPolyline,
        createArcBandByPoint: createArcBandByPoint,
        createArcBand: createArcBand,
        getRandom: getRandom,
        distancia: distancia,
        distancia2Puntos: distancia2Puntos,
        createPolygonCoordinates: createPolygonCoordinates,
        createNewFeature: createNewFeature,
        createMultiPolygon: createMultiPolygon,
        createMultiPoints: createMultiPoints,
        getShapeType: getShapeType,
        cloneFeature: cloneFeature
    };
});





