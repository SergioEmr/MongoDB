/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define([
    "luciad/shape/ShapeFactory",
    "luciad/reference/ReferenceProvider",
    "luciad/view/PaintRepresentation",
], function (ShapeFactory, ReferenceProvider, PaintRepresentation) {
    var referenceC = ReferenceProvider.getReference("urn:ogc:def:crs:OGC::CRS84");
    function formatDate(time, format) {
        var date = time? new Date(time) : new Date();
        var y = date.getFullYear();
        var mo = (date.getMonth()+1)<10? "0"+(date.getMonth()+1): (date.getMonth()+1);
        var d = date.getDate() <10? "0"+date.getDate(): date.getDate();
        switch(format) {
            default: return mo+"/"+d+"/"+y;
            case "aaaa/mm/dd": return y+"/"+mo+"/"+d;
            case "aaaa/mm/dd hh:mm": return y+"/"+mo+"/"+d+" "+formatTime(time, false);
            case "aaaa/mm/dd hh:mm:ss": return y+"/"+mo+"/"+d+" "+formatTime(time);
            case "mm/dd/aaaa": return mo+"/"+d+"/"+y;
            case "ddmmaaaa": return d+mo+y+"";
            case "dd/mm/aaaa": return d+"/"+mo+"/"+y;
            case "dd/mm/aaaa hh:mm": return d+"/"+mo+"/"+y+" "+formatTime(time, false);
            case "dd/mm/aaaa hh:mm:ss": return d+"/"+mo+"/"+y+" "+formatTime(time);
            case "dd-mm-aaaa": return d+"-"+mo+"-"+y;
            case "mm/aaaa": return mo+"/"+y;
            case "dd/mm": return d+"/"+mo;
            case "dd-mm-aaaaThh:mm":return d+"-"+mo+"-"+y+"T"+formatTime(time, false);
            case "aaaammdd": return y+""+mo+""+d+"";
        }
        return  mo+"/"+d+"/"+y;
    }
    function formatTime(time, includeS) {
        var date = time? new Date(time) : new Date();
        var h = date.getHours() <10 ? "0"+date.getHours(): date.getHours();
        var m = date.getMinutes() <10 ? "0"+date.getMinutes(): date.getMinutes();
        var s = date.getSeconds() <10 ? "0"+date.getSeconds(): date.getSeconds();
        if(includeS===false)
            return h+":"+m;
        else
            return h+":"+m+":"+s;
    }
    
    function esNumero (texto) {
        if(typeof texto === "string") {
            var esNumero = true;
            for(var c in texto) {
                switch (texto.charAt(c)) {
                    case "1":
                    case "2":
                    case "3":
                    case "4":
                    case "5":
                    case "6":
                    case "7":
                    case "8":
                    case "9":
                    case "0":
                    case ".":
                    case "-":
                        esNumero = true;
                        break;
                    default: return false;
                }
            }
            return esNumero;
        }
        if(typeof texto === 'number')
            return true;
        return false;
    }
    function numeroMes(mes) {
        switch(mes.toLowerCase()) {
            case "ene":
            case "enero": return 1;
            case "feb":
            case "febrero": return 2;
            case "mar":
            case "marzo": return 3;
            case "abr":
            case "abril": return 4;
            case "may":
            case "mayo": return 5;
            case "jun": 
            case "junio": return 6;
            case "jil":
            case "julio": return 7;
            case "ago":
            case "agosto": return 8;
            case "sep":
            case "septiembre": return 9;
            case "oct":
            case "octubre": return 10;
            case "nov":
            case "noviembre": return 11;
            case "dic":
            case "diciembre": return 12;
            default: return 1;
        }
    }
    
    function numeroDia(dia) {
        switch(dia.toLowerCase()) {
            case "lunes": return 1;
            case "martes": return 2;
            case "miercoles": return 3;
            case "jueves": return 4;
            case "viernes": return 5;
            case "sabado": return 6;
            case "domingo": return 7;
        }
    }
    
    return {
        numeroDia: numeroDia,
        /*
         * con esta funcion daremos formato a la etiqueta del reloj, utilizando el tiempo actual del equipo.
         * @returns {String} Hora : Minutos : Segundos
         */
        formatTime: formatTime,
        /*
         * 
         * @returns {String} Mes / Dia / Año
         */
        formatDate: formatDate,
        fitBounds: function (map, bounds, animate) {
            map.mapNavigator.fit({bounds: bounds, animate: animate});
        },
        /*
         * Formato de las coordenadas:
         *      [x, ancho, y, altura, z, profundidad] o [x, ancho, y, altura]
         */
        fitCoordinates: function (map, coordinates, animate) {
            var bounds = ShapeFactory.createBounds(referenceC, coordinates);
            map.mapNavigator.fit({bounds: bounds, animate: animate});
        }, 
        getRandom: function (min, max) {
            return Math.random() * (max - min) + min;
        },
        isNumber: function (string) {
            return isNaN(string);
        },
        calcularEdad: function (fecha) {
            var date = new Date(), year="";
            var actualYear = date.getFullYear();
            var c, d=0;
            for(var i=0; i<fecha.length; i++) {
                c = fecha.charAt(i);
                if(d>1){
                    year += c;
                }
                if(c==="/" || c ==="\\")
                    d++;
            }
            year = parseInt(year);
            var edad = actualYear - year;
            return edad;
        },
        /*
        * 
        * @param {type} div
        * @param {type} opciones
        * @returns {undefined}
        */
        setOptions: function (div, options, firstOption, valueOptions) {
            
            document.getElementById(div).innerHTML = "";
            var base = "<option value='$VALUE'>$LABEL</option>", etiqueta = "";
            if(firstOption !== false) {
                if(!firstOption)
                    firstOption = firstOption || "Todos";
                etiqueta = base.replace("$VALUE", 0).replace("$LABEL", firstOption);
            }
                
            var  n = options.length, values=false;
            if(valueOptions) {
                if(valueOptions.length === n)
                    values = true;
            }
            for(var i =0; i<n; i++) {
                if(values)
                    etiqueta += base.replace("$VALUE", valueOptions[i]).replace("$LABEL", options[i]);
                else 
                    etiqueta += base.replace("$VALUE", i+1).replace("$LABEL", options[i]);
            }
            document.getElementById(div).innerHTML = etiqueta;
        },
        getIdArray: function (label, list) {
            for( var k=0;k<list.length;k++) {
                if(list[k] === label)
                    return k;
            }
        },
        exportarTablaXLS: function (boton, divTabla, name) {
            boton.preventDefault();
            var dataType = 'data:application/vnd.ms-excel';
            var tableDiv = document.getElementById(divTabla);
            var tableHTML = tableDiv.outerHTML.replace(/ /g, '%20').replace(/#/g, "");
            var a = document.createElement('a');
            a.href = dataType +', '+ tableHTML;
            a.download = (name || formatDate("","ddmmaaa"))+".xls";
            a.click();
        },
        esNumero: esNumero,
        numeroMes: numeroMes,
        setLabels: function (layer, value) {
            try {
                layer.setPaintRepresentationVisible(PaintRepresentation.LABEL, value);
            }catch (e) {
                console.log(e);
            }
        },
        MexDateToUsDate: function (date) {
            var nfecha = "", n = 0;
            var c, a = "", m = "", d = "", h="";

            for(var j=0; j<date.length;j++) {
                c = date.charAt(j);
                if(c === '/' || c === '-' || (j=== (date.length-1))|| c===' ') {
                    if(n===0) 
                        d = nfecha;
                    else {
                        if(n===1)
                            m = nfecha;
                        else {
                            if(n===2) {
                                if(c===' ')
                                    a = nfecha;
                                else
                                    a = nfecha+c;
                            } else {
                                h = nfecha+c;
                                n=0;
                            }
                        }
                    }
                    nfecha = "";
                    n++;
                } else {
                    nfecha+=c;
                }

            }
            a = parseInt(a);
            a = a<2000? a+2000: a; 
            if(esNumero(m))
                return a+'/'+m+'/'+d+" "+h;
            m = numeroMes(m);
            return a+'/'+m+'/'+d+" "+h;
        },
        readFile: function (url, handeler) {
            $.ajax({
                type:'Get',
                dataType: "json",
                url:url 
            }).done(handeler).fail(function (e, ex) {
                console.error(e);
            });
        }
    };
});









