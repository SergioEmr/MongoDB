define([
  "luciad/model/codec/GeoJsonCodec",
  "luciad/shape/ShapeFactory",
  "luciad/shape/ShapeType"
], function(GeoJsonCodec, ShapeFactory, ShapeType) {

  /**
   * A CustomJsonCodec is an illustration of a custom codec that adheres to the Codec API and builds on the
   * GeoJSONCodec which is part of the API.  The CustomJsonCodec adds support for additional geometry types that
   * are not covered by the GeoJSON format.  This codec is sample code, does not adhere to a particular standard
   * and can be adapted to the needs of application.
   *
   * The codec decodes GeoJSON and other geometries to shapes as follows:
   * - Points, LineStrings, Polygons GeoJSON geometries are mapped to LuciadRIA Points, Polylines and Polygons or
   *   ComplexPolygons respectively.  Note that a GeoJSON Polygon is decoded to a Polygon, if it only consists of
   *   a single ring.
   * - MultiPoints, MultiLineStrings, MultiPolygons are all mapped to a ShapeList, containing only LuciadRIA Points,
   *   Polylines and ComplexPolygons or Polygons.
   * - GeometryCollections are mapped to Shapelists.
   * - Other, non-GeoJSON shapes are also supported and are mapped to their corresponding shapes in the ShapeFactory:
   *   CircleBy3Points, CircleByCenterPoints, Ellipses, CircularArcBy3Points, CircularArcByCenterPoints,
   *   CircularArcByBulges, Arcs, ArcBands, GeoBuffers and ExtrudedShapes.
   *
   * The codec encodes the shape of the feature to CustomJSON geometries as follows:
   * - Points, Polylines are mapped to GeoJSON Points and LineStrings respectively.
   * - Polygons and ComplexPolygons are all mapped to GeoJSON Polygons.
   * - ShapeLists that consist solely of Points or Polylines are mapped to MultiPoint and MultiLineString geometries.
   * - Shapelists that consist of a mix of Polygons and ComplexPolygons are mapped to GeoJSON MultiPolygons.
   * - Shapelists that contain an arbitrary mix of shapes are mapped to GeoJSON GeometryCollections.
   * - Other, non-GeoJSON shapes are encoded in a customized way: CircleBy3Points, CircleByCenterPoints, Ellipses,
   *   CircularArcBy3Points, CircularArcByCenterPoint, CircularArcByBulge, Arc, ArcBand, GeoBuffers and ExtrudedShapes.
   */
  function CustomJsonCodec() {
    GeoJsonCodec.apply(this, arguments);
  }

  // inherit from the GeoJsonCodec prototype.
  function TMP(){}
  TMP.prototype = GeoJsonCodec.prototype;
  CustomJsonCodec.prototype = new TMP();
  CustomJsonCodec.prototype.constructor = CustomJsonCodec;

  /**
   * Encode a shape to JSON.  This method delegates to the superclass method for standard GeoJSON shapes
   * @param shape the shape to encode
   * @returns {Object} The JSON representation of the shape.
   */
  CustomJsonCodec.prototype.encodeShape = function(shape) {
    var geometry;
    if (ShapeType.contains(shape.type, ShapeType.CIRCLE)) {
      geometry = this._encodeCircleGeometry(shape);
    }
    else if (ShapeType.contains(shape.type, ShapeType.CIRCULAR_ARC_BY_3_POINTS)) {
      geometry = this._encodeCircularArcBy3PointsGeometry(shape);
    }
    else if (ShapeType.contains(shape.type, ShapeType.CIRCULAR_ARC_BY_BULGE)) {
      geometry = this._encodeCircularArcByBulgeGeometry(shape);
    }
    else if (ShapeType.contains(shape.type, ShapeType.CIRCULAR_ARC_BY_CENTER_POINT)) {
      geometry = this._encodeCircularArcByCenterPointGeometry(shape);
    }
    else if (ShapeType.contains(shape.type, ShapeType.ELLIPSE)) {
      geometry = this._encodeEllipseGeometry(shape);
    }
    else if (ShapeType.contains(shape.type, ShapeType.ARC)) {
      geometry = this._encodeArcGeometry(shape);
    }
    else if (ShapeType.contains(shape.type, ShapeType.ARC_BAND)) {
      geometry = this._encodeArcBandGeometry(shape);
    }
    else if (ShapeType.contains(shape.type, ShapeType.GEO_BUFFER)) {
      geometry = this._encodeGeoBufferGeometry(shape);
    }
    else if (ShapeType.contains(shape.type, ShapeType.EXTRUDED_SHAPE)) {
      geometry = this._encodeExtrudedShapeGeometry(shape);
    }
    else {
      geometry = GeoJsonCodec.prototype.encodeShape.call(this, shape);
    }
    return geometry;
  };

  /**
   * Decodes JavaScript object literals as LuciadRIA Shapes.  This methods delegates to GeoJsonCodec's
   * decodeGeometry to decode shapes that adhere to the GeoJSON standards.
   * @param {Object} geometry A Geometry object literal.
   * @param {luciad.reference.CoordinateReference} reference The reference in which the shape is defined.
   * @returns {luciad.shape.Shape} The LuciadRIA shape.
   */
  //#snippet decodeGeometry
  CustomJsonCodec.prototype.decodeGeometryObject = function(geometry, reference) {
    var geometryType = geometry.type;
    if (geometryType === "CircleByCenterPoint") {
      return this.decodeCircleByCenterPoint(geometry, reference);
    } else if (geometryType === "CircleBy3Points") {
      return this.decodeCircleBy3Points(geometry, reference);
    } else if (geometryType === "CircularArcByCenterPoint") {
      return this.decodeCircularArcByCenterPoint(geometry, reference);
    } else if (geometryType === "CircularArcBy3Points") {
      return this.decodeCircularArcBy3Points(geometry, reference);
    } else if (geometryType === "CircularArcByBulge") {
      return this.decodeCircularArcByBulge(geometry, reference);
    } else if (geometryType === "Ellipse") {
      return this.decodeEllipse(geometry, reference);
    } else if (geometryType === "Arc") {
      return this.decodeArc(geometry, reference);
    } else if (geometryType === "ArcBand") {
      return this.decodeArcBand(geometry, reference);
    } else if (geometryType === "GeoBuffer") {
      return this.decodeGeoBuffer(geometry, reference);
    } else if (geometryType === "ExtrudedShape") {
      return this.decodeExtrudedShape(geometry, reference);
    } else {
      return GeoJsonCodec.prototype.decodeGeometryObject.call(this, geometry, reference);
    }
  };
  //#endsnippet decodeGeometry

  CustomJsonCodec.prototype._encodeCircleGeometry = function(circle) {
    if (ShapeType.contains(circle.type, ShapeType.CIRCLE_BY_CENTER_POINT)) {
      return {
        type: "CircleByCenterPoint",
        center: this.encodeShape(circle.center),
        radius: circle.radius
      };
    }
    if (ShapeType.contains(circle.type, ShapeType.CIRCLE_BY_3_POINTS)) {
      return {
        type: "CircleBy3Points",
        firstPoint: this.encodeShape(circle.firstPoint),
        secondPoint: this.encodeShape(circle.secondPoint),
        thirdPoint: this.encodeShape(circle.thirdPoint)
      };
    }
  };

  CustomJsonCodec.prototype._encodeCircularArcByCenterPointGeometry = function(circularArc) {
    return {
      type: "CircularArcByCenterPoint",
      center: this.encodeShape(circularArc.center),
      radius: circularArc.radius,
      startAzimuth: circularArc.startAzimuth,
      sweepAngle: circularArc.sweepAngle
    };
  };

  CustomJsonCodec.prototype._encodeCircularArcBy3PointsGeometry = function(circularArc) {
    return {
      type: "CircularArcBy3Points",
      startPoint: this.encodeShape(circularArc.startPoint),
      intermediatePoint: this.encodeShape(circularArc.intermediatePoint),
      endPoint: this.encodeShape(circularArc.endPoint)
    };
  };

  CustomJsonCodec.prototype._encodeCircularArcByBulgeGeometry = function(circularArc) {
    return {
      type: "CircularArcByBulge",
      startPoint: this.encodeShape(circularArc.startPoint),
      endPoint: this.encodeShape(circularArc.endPoint),
      bulge: circularArc.bulge
    };
  };

  CustomJsonCodec.prototype._encodeEllipseGeometry = function(ellipse) {
    return {
      type: "Ellipse",
      center: this.encodeShape(ellipse.center),
      a: ellipse.a,
      b: ellipse.b,
      rotationAzimuth: ellipse.rotationAzimuth
    };

  };

  CustomJsonCodec.prototype._encodeArcGeometry = function(arc) {
    return {
      type: "Arc",
      center: this.encodeShape(arc.center),
      a: arc.a,
      b: arc.b,
      startAzimuth: arc.startAzimuth,
      sweepAngle: arc.sweepAngle,
      rotationAzimuth: arc.rotationAzimuth
    };

  };

  CustomJsonCodec.prototype._encodeArcBandGeometry = function(arcband) {
    return {
      type: "ArcBand",
      center: this.encodeShape(arcband.center),
      startAzimuth: arcband.startAzimuth,
      sweepAngle: arcband.sweepAngle,
      minRadius: arcband.minRadius,
      maxRadius: arcband.maxRadius
    };
  };

  CustomJsonCodec.prototype._encodeGeoBufferGeometry = function(geobuffer) {
    return {
      type: "GeoBuffer",
      baseShape: this.encodeShape(geobuffer.baseShape),
      width: geobuffer.width
    };
  };

  CustomJsonCodec.prototype._encodeExtrudedShapeGeometry = function(extruded) {
    return {
      type: "ExtrudedShape",
      baseShape: this.encodeShape(extruded.baseShape),
      minimumHeight: extruded.minimumHeight,
      maximumHeight: extruded.maximumHeight
    };
  };

  CustomJsonCodec.prototype.decodeCircleByCenterPoint = function(geometry, reference) {
    return ShapeFactory.createCircleByCenterPoint(reference,
        this.decodeGeometryObject(geometry.center, reference),
        geometry.radius);
  };

  CustomJsonCodec.prototype.decodeCircleBy3Points = function(geometry, reference) {
    return ShapeFactory.createCircleBy3Points(reference,
        this.decodeGeometryObject(geometry.firstPoint, reference),
        this.decodeGeometryObject(geometry.secondPoint, reference),
        this.decodeGeometryObject(geometry.thirdPoint, reference));
  };

  CustomJsonCodec.prototype.decodeCircularArcByCenterPoint = function(geometry, reference) {
    return ShapeFactory.createCircularArcByCenterPoint(reference,
        this.decodeGeometryObject(geometry.center, reference),
        geometry.radius,
        geometry.startAzimuth,
        geometry.sweepAngle);
  };

  CustomJsonCodec.prototype.decodeCircularArcByBulge = function(geometry, reference) {
    return ShapeFactory.createCircularArcByBulge(reference,
        this.decodeGeometryObject(geometry.startPoint, reference),
        this.decodeGeometryObject(geometry.endPoint, reference),
        geometry.bulge);
  };

  CustomJsonCodec.prototype.decodeCircularArcBy3Points = function(geometry, reference) {
    return ShapeFactory.createCircularArcBy3Points(reference,
        this.decodeGeometryObject(geometry.startPoint, reference),
        this.decodeGeometryObject(geometry.intermediatePoint, reference),
        this.decodeGeometryObject(geometry.endPoint, reference));
  };

  CustomJsonCodec.prototype.decodeArc = function(geometry, reference) {
    return ShapeFactory.createArc(reference, this.decodeGeometryObject(geometry.center, reference),
        geometry.a,
        geometry.b,
        geometry.rotationAzimuth,
        geometry.startAzimuth,
        geometry.sweepAngle);
  };

  CustomJsonCodec.prototype.decodeArcBand = function(geometry, reference) {
    return ShapeFactory.createArcBand(reference, this.decodeGeometryObject(geometry.center, reference),
        geometry.minRadius,
        geometry.maxRadius,
        geometry.startAzimuth,
        geometry.sweepAngle);
  };

  CustomJsonCodec.prototype.decodeGeoBuffer = function(geometry, reference) {
    return ShapeFactory.createGeoBuffer(reference, this.decodeGeometryObject(geometry.baseShape, reference),
        geometry.width);
  };

  CustomJsonCodec.prototype.decodeExtrudedShape = function(geometry, reference) {
    return ShapeFactory.createExtrudedShape(reference, this.decodeGeometryObject(geometry.baseShape, reference),
        geometry.minimumHeight, geometry.maximumHeight);
  };

  CustomJsonCodec.prototype.decodeEllipse = function(geometry, reference) {
    return ShapeFactory.createEllipse(reference, this.decodeGeometryObject(geometry.center, reference),
        geometry.a,
        geometry.b,
        geometry.rotationAzimuth);
  };

  return CustomJsonCodec;
});
