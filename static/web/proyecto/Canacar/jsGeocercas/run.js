//#snippet runrequire
require({
  baseUrl: "../../",

  packages: [
    {name: "dojo", location: "./samples/lib/dojo"},
    {name: "dojox", location: "./samples/lib/dojox"},
    {name: "dijit", location: "./samples/lib/dijit"},
    {name: "luciad", location: "./luciad"},
    
    {name: "demo", location: "./proyecto/Canacar/jsGeocercas"},
    {name: "datos", location: "./proyecto/Canacar/data"},
    {name: "recursos", location: "./proyecto/recursos"},
    
    {name: "template", location: "./samples/template"},
    {name: "samplecommon", location: "./samples/common"},
    {name: "samples", location: "./samples"},
    {name: "file", location: "./samples/model/File/js/file"}
  ]
//#endsnippet runrequire
  , cache: {}
//#snippet runrequirecont
}, ["demo"]);