/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define([
    "luciad/shape/ShapeFactory",
    "luciad/model/feature/Feature",
    "luciad/reference/ReferenceProvider",
    "luciad/model/store/MemoryStore",
    "luciad/model/feature/FeatureModel",
    "luciad/view/feature/FeatureLayer",
    "./painters/layerPainter"
], function (ShapeFactory, Feature, ReferenceProvider, MemoryStore, FeatureModel, 
    FeatureLayer, layerPainter) {
    var baseDate = "20AA/mes/dia hora:00";
    var baseFecha = "fecha";
    var reference = ReferenceProvider.getReference("CRS:84");
    
    function createFeatures(data) {
        var properties=[], propertie, tipo, actual, i, j, k=0, n, m;
        var features = [], feature, keys=[], longitud, latitud;
        for(var key in data) {
            actual = data[key];
            i=0;
            for(var key2 in actual) {
                if(key2 === "TipoDelito")
                    tipo = actual[key2];
                else {
                    properties[i] = actual[key2];
                    keys[i] = key2;
                    i++;
                }
            }
            n = properties.length;
            var p ={};
            propertie = properties[0];
            m = propertie.length;
            for(j=0; j<m;j++ ) {
                
                for(i=0;i<n;i++){
                    p[keys[i]] = properties[i][j];
                    if(keys[i] === "Lat")
                        latitud = properties[i][j];
                    if(keys[i] === "Lon")
                        longitud = properties[i][j];
                    
                }
                p.TipoDelito = tipo;
                if(p.MES) {
                    
                    p.EventTime = baseDate.replace("AA", A).replace("mes", getMes(p.MES)).replace("dia", p.DIANUM).replace("hora", p.HLLEGADA);
                } else 
                    p.EventTime = baseDate.replace("AA", A).replace("mes", "01").replace("dia", p.DIANUM).replace("hora", p.HLLEGADA);
                features[k] = createPoint(longitud, latitud, 0, k, p);
                k++;
                p={};
            }
        }
        return features;
    }
    
    function getMes(mes) {
        switch(mes) {
            case "ENERO": return "01";
            case "FEBRERO": return "02";
            case "MARZO": return "03";
            case "ABRIL": return "04";
            case "MAYO": return "05";
            case "JUNIO": return "06";
            case "JULIO": return "07";
            case "AGOSTO": return "08";
            case "SEPTIEMBRE": return "09";
            case "OCTUBRE": return "10";
            case "NOVIEMBRE": return "11";
            case "DICIEMBRE": return "12";
            default: console.log("No mes: " + mes); return "01";
        }
    }
    
    function createPoint(x, y, z, i, properties) {
        if(properties === undefined)
            properties = {};
        return new Feature(
            ShapeFactory.createPoint(reference, [x, y, z]), properties, i
        );
    }
    
    function createLayer(data) {
        var features = createFeatures(data);
        var store = new MemoryStore({data: features});
        var featureModel = new FeatureModel(store, {reference: reference});
        var layer = new FeatureLayer(featureModel, {
            label: "Eventos", 
            id:"eventos", 
            selectable: true, 
            editable: false, 
            painter: new layerPainter()});
        return layer;
    }
    
    function createTimeFeature(features) {
        var f = [], key = "fecha", key2 = "FECHA ROBO", fecha;
        features = corregirPropiedades(features);
        for(var i=0; i<features.length; i++) {
            var p = features[i].properties;
            var longitud = features[i].geometry.coordinates[0], latitud = features[i].geometry.coordinates[1];
            
            if(p[key]) {
                fecha = corregirFecha(p[key]);
                //fecha = p[key];
            } else {
                if(p[key2])
                    fecha = corregirFecha(p[key2]);
                else
                    console.log("No se encontro fecha para este evento "+i);
            }
            //var hora = p.hora;
            var eventTime = obtenerFecha(p);
            //var eventTime = baseFecha.replace("fecha", fecha+" "+hora);
            p.EventTime = eventTime;
            f[i] = createPoint(longitud, latitud, 0, i, p);
        }
        return f;
    }
    function obtenerFecha(properties) {
        var fecha = properties.fecha, hora = properties.hora;
        var nfecha = "", n = 0;
        var c, a = "", m = "", d = "";
        for(var j=0; j<fecha.length;j++) {
            c = fecha.charAt(j);
            if(c === '/' || (j=== (fecha.length-1))) {
                if(n===0) 
                    d = nfecha;
                else {
                    if(n===1)
                        m = nfecha;
                    else {
                        if(n===2) {
                            a = nfecha+c;
                            n=0;
                        }
                    }
                }
                nfecha = "";
                n++;
            } else {
                nfecha+=c;
            }
            
        }
        fecha = a+'/'+m+'/'+d;
        return fecha +" "+hora;
    }
    
    function corregirPropiedades(features) {
        var p, names = [[]], key, j=0, i, k=0;
        p = features[0].properties;
        for(key in p) {
            names[k][j] = key;
            j++;
        }
        k++;
        names[k] = [];
        j=0;
        for(i =1; i<features.length;i++) {
            p = features[i].properties;
            for(key in p) {
                names[k][j] = key;
                j++;
            }
            if(names[k].length !== names[k-1].length) {
                k++;
            }
            j=0;
            names[k] = [];
        }
        
        if(k===1) {
            return features;
        } else {
            var newP = {}, newNames = names[0], name, existe = false;
            k=1;
            for(i=1; i<names[k].length-1; i++) {
                name = names[k][i];
                for(j=0; j<newNames.length; j++) {
                    if(name === newNames[j] ) {
                        existe = true;
                        break;
                    } else {
                        existe = false;
                    }
                }
                if(existe === false) {
                    newNames[j] = name;
                }
            }
            var newKey;
            for(i=0; i<features.length; i++) {
                p = features[i].properties;
                for(j=0; j<newNames.length;j++) {
                    newKey = newNames[j];
                    for(key in p) {
                        if(key === newNames[j]) {
                            existe = true;
                            break;
                        } else {
                            existe = false;
                        }
                    }
                    if(existe===true) {
                        newP[newKey] = p[newKey];
                    } else {
                        newP[newKey] = null;
                    }
                }
                
                features[i].properties = newP;
                newP = {};
            }
            return features;
        }
    }
    
    function corregirFecha(fecha) {
        var nfecha = "", n = 0;
        var c, a = "", m = "", d = "";
        
        for(var j=0; j<fecha.length;j++) {
            c = fecha.charAt(j);
            if(c === '/' || (j=== (fecha.length-1))) {
                if(n===0) 
                    d = nfecha;
                else {
                    if(n===1)
                        m = nfecha;
                    else {
                        if(n===2) {
                            a = nfecha+c;
                            n=0;
                        }
                    }
                }
                nfecha = "";
                n++;
            } else {
                nfecha+=c;
            }
            
        }
        return a+'/'+m+'/'+d;
    }
    function buscarFecha(properties, key) {
        for(var keys in properties) {
            if(keys === key) {
                return properties[keys];
            }
        }
    }
    
    
    return {createLayer: createLayer,
        createFeatures: createFeatures,
        createTimeFeature: createTimeFeature
    };
});

