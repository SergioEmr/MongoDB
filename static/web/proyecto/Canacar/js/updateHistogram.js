/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define([
    'luciad/util/Promise',
    "samples/timeslider/TimeSlider",
    'luciad/shape/ShapeFactory',
    "samples/timeslider/TimeSlider",
    "time/TimeSliderWithOverview",
    'luciad/util/ColorMap',
    'luciad/transformation/TransformationFactory',
    
    "recursos/js/Graficas/Graficas",
    "./TimeLineLayer",
    "luciad/model/store/MemoryStore",
    "luciad/model/feature/FeatureModel",
    "luciad/view/feature/FeatureLayer",
    "samples/timeslider/TimeSlider",
    "time/TimeSliderWithOverview",
    
    'luciad/view/feature/ParameterizedLinePainter',
    'luciad/util/ColorMap',
    'luciad/view/feature/ShapeProvider',
    "./painters/EventTimePainter",
    "./Contador",
    "./Tablas",
    "recursos/js/Util",
    "recursos/js/MapFactory"
], function (Promise, TimeSlider, ShapeFactory,TimeSlider,TimeSliderWithOverview,ColorMap,TransformationFactory,
            Graficas, TimeLineLayer, MemoryStore, FeatureModel, FeatureLayer, TimeSlider, TimeSliderWithOverview,
            ParameterizedLinePainter, ColorMap, ShapeProvider, EventTimePainter, Contador, Tablas, Util, MapFactory) {
    
    var timeSlider;
     /*
     * updateHistogram, esta funcion se encarga de actualizar toda la informacion necesario
     * al mover la linea de tiempo
     * @returns {undefined}
     */
    var timeFilter;
    var usuario = document.getElementById("tipoUsuario").innerHTML;
    switch(usuario.toLowerCase()) {
        case "088": usuario="u"; break;
        case "dev": usuario = "u"; break;
        default: usuario="ig"; break;
    }
    function updateHistogram(map, features, layer, reference, startTime, endTime, tablaDatos, graficas, estados, filtros) {
        var store = new MemoryStore({data: features});
        var originalModel = new FeatureModel(store, { reference: reference });
        var eventPromise = originalModel.query();
        Promise.when(eventPromise, function(cursor) {
            var quakes = [];
            var index = 0;
            while (cursor.hasNext()) {
                var quake = cursor.next();
                quakes[index] = quake;
                index++;
            }
            console.log("Loading " + index + " events");

            startTime = startTime - 1000;
            endTime += 1000;
            var shapeProvider = new ShapeProvider();
            shapeProvider.reference = TimeSlider.REFERENCE;
            shapeProvider.provideShape = function(feature) {
                var time = obtenerFecha(feature.properties);
                //var time = Date.parse(feature.properties.EventTime) / 1000 | 0;
                // var depth = 800-feature.properties.depth;
                // var depth = 800-feature.shape.z;
                var depth = 100-feature.shape.z;
                return ShapeFactory.createPoint(TimeSlider.REFERENCE, [time, depth]);
            };
            var timeEventLayer = new FeatureLayer(layer.model, {
                selectable: true,
                shapeProvider: shapeProvider,
                painter: new EventTimePainter()
            });
            
            var updateTimeFilters = function() {
                var municipios=[];
                try {
                    var nuevaTabla = Graficas.actualizarTabla(tablaDatos,timeEventLayer, mapBounds);
                    for(var k =0; k<graficas.length;k++) {
                        Graficas.actualizarGraficaIdTexto(graficas[k].id, nuevaTabla, graficas[k].tipo, graficas[k].label);
                    }
                    var mun = municipiosFilterNode.selectedOptions[0].innerHTML;
                    
                    municipios = listarMunicipios(nuevaTabla);
                    if(mun === "Todos")
                        Util.setOptions("filtroMunicipios", municipios);
                    if(nuevaTabla.length !== tablaDatos.length) {
                        Tablas.crearTablaDIV("tablaCompleta", nuevaTabla, 500);
                        console.log("Tabla Actualizada");
                    }
                }catch(e) {
                    console.log("Error al actualizar grafica");
                }
                var mapBounds = timeSlider.mapBounds;
                var filterStart = (timeSlider.mapBounds.x | 0 );
                var filterWidth = (timeSlider.mapBounds.width | 0 );
                var filterEnd = filterStart + filterWidth;
                var empresa = empresasFilterNode.selectedOptions[0].value;
                var estado = estadosFilterNode.selectedOptions[0].value;
                var vehiculo = vehiculoNode.selectedOptions[0].innerHTML;
                var carga = cargaNode.selectedOptions[0].innerHTML;
                mun = municipiosFilterNode.selectedOptions[0].innerHTML;
                var puntos = [], np=0;
                if(usuario === "u") {
                    layer.filter = function(feature) {
                        var f = timeEventLayer.shapeProvider.provideShape(feature);
                        var em = feature.properties.empresa;
                        var es = feature.properties.entidad;
                        var m = feature.properties.municipio;
                        var v = feature.properties.tvehiculo;
                        var c = feature.properties.carga;
                        if(mapBounds.contains2DPoint(f)) {
                            if((em === empresa || empresa === "Todos") && (es === estado || estado === "Todos")&& 
                                    (m === mun || mun === "Todos") && (c === carga || carga==="Todos") && (v === vehiculo || vehiculo==="Todos")) {
                                return true;
                            } 
                            else
                                return false;
                        } else
                            return false;
                    };
                } else {
                    layer.filter = function(feature) {
                        var f = timeEventLayer.shapeProvider.provideShape(feature);
                        var es = feature.properties.entidad;
                        var m = feature.properties.municipio;
                        var propiedades = feature.properties, i=0;
                        var c = feature.properties.carga;
                        var v = feature.properties.tvehiculo;
                        for(var k in propiedades) {
                            i++;
                            if(i> 15) break;
                        }
                        if(mapBounds.contains2DPoint(f)) {
                            if((i> 15 || empresa === "Todos") && (es === estado || estado === "Todos")&& (m === mun || mun === "Todos") &&
                                    (c === carga || carga==="Todos") && (v === vehiculo || vehiculo==="Todos")) {
                                return true;
                            } 
                            else
                                return false;
                        } else
                            return false;
                    };
                }
                /*
                if(puntos.length===0) {
                    puntos = layer.model.query().array;
                }
                Contador.reestartLayers(municipios);
                Contador.contadorMunicipios(layer, municipios);
                */
            }; /// fin UpdateFilters
    
            var combineModeChangedFunction = function(transform) {
                timeFilter = undefined;
                var combineMode = timeSlider.getCombineMode();

                var painter = new ParameterizedLinePainter({
                    lineWidth: 10,
                    rangePropertyProvider: function(feature, shape, pointIndex) {
                    // var time = offset + feature.properties.time / 1000 | 0;
                        var time = Date.parse(feature.properties.alerta) / 1000 | 0;

                        return transform(time);
                    },
                    rangeWindow: [0, 999999999999]
                });
                painter.density = {
                    colorMap: ColorMap.createGradientColorMap([
                    {level: 0, color: "rgba(  0,   0,   0, 0.0)"},
                    {level: 1, color: "rgba(  0, 0,   255, 0.5)"},
                    {level: 5, color: "rgba(  0, 255,   255, 1.0)"},
                    {level: 10, color: "rgba(  255, 255,   0, 1.0)"},
                    {level: 20, color: "rgba(255, 0, 0, 1.0)"}
                    ])
                };

                var previousPainter = layer.painter;
                var quakesModel = layer.model;
                var layer2d = new FeatureLayer(quakesModel, {
                    label: "Eventos 2",
                    selectable: true,
                    id: "eventos",
                    painter: painter
                });
                map.layerTree.removeChild(map.layerTree.findLayerById("eventos"));
                // map3d.layerTree.removeChild(map3d.layerTree.findLayerById("12345"));
                //map.layerTree.addChild(layer2d);
                // map3d.layerTree.addChild(layer3d);

                eventos = layer2d;

                timeSlider.layerTree.removeChild(timeEventLayer);

                var shapeProvider = new ShapeProvider();
                shapeProvider.reference = TimeSlider.REFERENCE;
                shapeProvider.provideShape = function(feature) {
                    var time = obtenerFecha(feature.properties);
                    //var time = Date.parse(feature.properties.EventTime) / 1000 | 0;
                     time = transform(time);

                    var depth = 10-feature.shape.getPoint(0).z;
                    return ShapeFactory.createPoint(TimeSlider.REFERENCE, [time, depth]);
                };
                timeEventLayer = new FeatureLayer(quakesModel, {
                    selectable: true,
                    shapeProvider: shapeProvider,
                    painter: new EventTimePainter()
                });

                //timeSlider.layerTree.addChild(timeEventLayer);
            
                updateTimeFilters();
                var lonlatBounds = transformation.transformBounds(map.mapBounds);
                timeEventLayer.filter = function(feature) {
                    return lonlatBounds.contains2DPoint(feature.shape.getPoint(0));
                };
            
            // timeSlider.getHistogram().updateHistogram(histogramUpdater);
            };

            var combineModeFilterChangedFunction = function(filter) {
                timeFilter = filter;
                if (timeFilter) {
                        map.layerTree.findLayerById("eventos").filter = function(feature) {
                        var time = obtenerFecha(feature.properties);
                        //var time = Date.parse(feature.properties.EventTime) / 1000 | 0;
                        return timeFilter(time);
                    };
                    updateTimeFilters();
                    var lonlatBounds = transformation.transformBounds(map.mapBounds);
                    timeEventLayer.filter = function(feature) {
                        var time = obtenerFecha(feature.properties);
                        //var time = Date.parse(feature.properties.EventTime) / 1000 | 0;
                        if (timeFilter(time)) {
                            return lonlatBounds.contains2DPoint(feature.shape.getPoint(0));
                        }
                        else {
                            return false;
                        }
                    };
                }
            };

            timeSlider = new TimeSliderWithOverview("timeSlider", "timeSliderOverview", startTime, endTime, combineModeChangedFunction, combineModeFilterChangedFunction);


            var columnsDayLayer = TimeLineLayer.createLayer(features, "days", 36, startTime, endTime);
            timeSlider.layerTree.addChild(columnsDayLayer);
            var queryFinishedHandleSlider = columnsDayLayer.workingSet.on("QueryFinished", function() {
                if(columnsDayLayer.bounds) {
                            try{
                                timeSlider.mapNavigator.fit({
                                    bounds: columnsDayLayer.bounds,
                                    animation: {duration: 10000},
                                    allowWarpXYAxis: true,
                                    fitMargin: "5%"
                                });
                             }catch(e) {
                                    console.log(e);
                             }
                        }
                queryFinishedHandleSlider.remove();
            });
                
            var columnsHourLayer = TimeLineLayer.createLayer(features, "hours", 30*36, startTime, endTime);
            timeSlider.layerTree.addChild(columnsHourLayer);

            timeSlider.refit();

            var transformation = TransformationFactory.createTransformation(map.reference, reference);
            var histogramUpdater = function(accumulate) {
                    var lonlatBounds = transformation.transformBounds(map.mapBounds);
                    for (var i = 0; i < quakes.length; i++) {
                        if(lonlatBounds.contains2DPoint(quakes[i].shape)) {
                            var time = obtenerFecha(quakes[i].properties);
                            //var time = Date.parse(quakes[i].properties.EventTime) / 1000 | 0;
                // var magnitude = quakes[i].properties.magnitude;
                            var magnitude = quakes[i].properties.mag;
                            accumulate(time, magnitude);
                        }
                    }
                };
            timeSlider.getHistogram().updateHistogram(histogramUpdater);

            var hacerfit = false;
            var empresasFilterNode = document.getElementById( "empresasFiltro" );
            empresasFilterNode.addEventListener( "change", function () {
                //fitColumns = true;
                updateTimeFilters();
            });
            var nombresMunicipios = [], estadoSeleccionado;
            var estadosFilterNode = document.getElementById( "estadosFiltro" );
            estadosFilterNode.addEventListener( "change", function () {
                //fitColumns = true;
                var value = estadosFilterNode.selectedOptions[0].value;
                if(value !== "Todos" && estados) {
                    estados.forEach(function (capaEstado) {
                        var label = capaEstado.label;
                        var check = false;
                        switch(value.toLowerCase()) {
                            default:
                                switch(value) {
                                    case "Guanajuato": check = true; break;
                                    case "Hidalgo": check = true; break;
                                    case "Queretaro": check = true; break;
                                    default: check = false; break;
                                }
                                break;
                            case "nuevo león":
                            case "nuevo leon":
                                if(label === "NuevoLeon") check = true;
                                else check = false;
                                break;
                            case "michoacán de ocampo":
                            case "michoacan de ocampo":
                                if(label === "Michoacan")  check = true;
                                else  check = false;
                                break;
                            case "méxico":
                                if(label === "EdoMex")  check = true;
                                else  check = false;
                                break;
                            case "distrito federal":
                                if(label === "CDMX")  check = true;
                                else  check = false;
                                break;
                            case "":
                                if(label === "CDMX")  check = true;
                                else  check = false;
                                break;
                        }
                        if(check === true) {
                            capaEstado.visible = true;
                            hacerfit = true;
                            hacerFit(capaEstado.bounds);
                            estadoSeleccionado = capaEstado;
                            nombresMunicipios = getNames(capaEstado);
                        } else {
                            capaEstado.visible = false;
                        }
                    });
                    Graficas.setOptions("filtroMunicipios", nombresMunicipios);
                } else {
                    Graficas.setOptions("filtroMunicipios", []);
                }
                updateTimeFilters();
            });
            
            function getNames (capaEsatdo) {
                var municipios = capaEsatdo.model.query().array;
                var n = municipios.length, nombres = [];
                for(var i=0; i<n; i++) {
                    var municipio = municipios[i];
                    nombres[i] = municipio.properties.name? municipio.properties.name: municipio.properties.NOMBRE? municipio.properties.NOMBRE: "Sin Nombre "+municipio.id;
                }
                return nombres;
            }
            var municipiosFilterNode = document.getElementById( "filtroMunicipios" );
            municipiosFilterNode.addEventListener( "change", function (data) {
                var value = municipiosFilterNode.selectedOptions[0].value;
                hacerfit = true;
                if(estadoSeleccionado) {
                    estadoSeleccionado.filter = function (municipio) {
                        var name = municipio.properties.name? municipio.properties.name: municipio.properties.NOMBRE? municipio.properties.NOMBRE: "Sin Nombre "+municipio.id;
                        if(value === "Todos" || value === name) {
                            hacerFit(municipio.geometry.bounds);
                            return true;
                        }
                        else 
                            return false;
                    };
                }
                updateTimeFilters();
            });
            var vehiculoNode = document.getElementById( "vehiculosFiltro" );
            vehiculoNode.addEventListener( "change", function (data) {
                updateTimeFilters();
            });
            var cargaNode = document.getElementById( "cargaFiltro" );
            cargaNode.addEventListener( "change", function (data) {
                updateTimeFilters();
            });
            
            function hacerFit(bounds) {
                if(bounds && hacerfit === true) {
                    hacerfit = false;
                    map.mapNavigator.fit({bounds: bounds, animate: true});
                }
            }
            
            map.on("idle", function() {
                timeSlider.getHistogram().updateHistogram(histogramUpdater);
            });
            map.on("MapChange", function() {
                var lonlatBounds = transformation.transformBounds(map.mapBounds);

                if (timeFilter) {
                    timeEventLayer.filter = function(feature) {
                        var time = obtenerFecha(feature.properties);
                        //var time = Date.parse(feature.properties.EventTime) / 1000 | 0;
                        if (timeFilter(time)) {
                            return lonlatBounds.contains2DPoint(feature.shape);
                        }
                        else {
                            return false;
                        }
                    };
                }
                else {
                    timeEventLayer.filter = function(feature) {
                        return lonlatBounds.contains2DPoint(feature.shape);
                    };
                }
            });

            timeSlider.on("MapChange", function() {
                //if(fitColumns === false)
                if(esVisible())
                    updateTimeFilters();
            });
        
        //Fin Promise
        });
        
        function obtenerFecha(properties) {
            if(properties.EventTime) {
                return Date.parse(properties.EventTime) / 1000 | 0;
            }
            var fecha = properties.fecha, hora = properties.hora;
            var nfecha = "", n = 0;
            var c, a = "", m = "", d = "";
            for(var j=0; j<fecha.length;j++) {
                c = fecha.charAt(j);
                if(c === '/' || (j=== (fecha.length-1))) {
                    if(n===0) 
                        d = nfecha;
                    else {
                        if(n===1)
                            m = nfecha;
                        else {
                            if(n===2) {
                                a = nfecha+c;
                                n=0;
                            }
                        }
                    }
                    nfecha = "";
                    n++;
                } else {
                    nfecha+=c;
                }

            }
            fecha = a+'/'+m+'/'+d;
            var time = Date.parse(fecha +" "+hora) / 1000 | 0;
            return time;
        }
        
        function listarMunicipios(tablaDatos) {
            //console.log(tablaDatos[0]);
            var id = Util.getIdArray("municipio", tablaDatos[0]);
            var municipios = [], j=0;
            var nombre;
            for(var i=1; i<tablaDatos.length; i++) {
                nombre = tablaDatos[i][id];
                if(municipios.length===0) {
                    municipios[j]=nombre;
                    j++;
                } else {
                    var nuevoDato=true;
                    for(var k in municipios) {
                        if(nombre === municipios[k]){
                            nuevoDato=false;
                            break;
                        }
                    }
                    if(nuevoDato) {
                        municipios[j]=nombre;
                        j++;
                    }
                }
            }
            return municipios;
        }
    //fin UpdateHistogram
    }
    ////================================================================================////////

    function esVisible() {
        var value = document.getElementById("botonGraficas").value;
        if(value === "true" || value === true)
            return true;
        else
            return false;
    }
    function destruirComponentes() {
        MapFactory.destroyMapAndComponents(timeSlider.overView);
        MapFactory.destroyMapAndComponents(timeSlider);
    }
    return {
        updateHistogram: updateHistogram,
        timeSlider: timeSlider,
        destruirComponentes: destruirComponentes
    };
});


