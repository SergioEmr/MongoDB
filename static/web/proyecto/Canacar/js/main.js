var map, municipios = [], tablaIncidentes = null;

define([
    "recursos/js/MapFactory",
    "recursos/js/GoogleMap",
    "recursos/js/LayerFactory",
    "dojo/dom",
    "dojo/on",
    "dojo/query",
    "luciad/view/PaintRepresentation",
    "samplecommon/dojo/DojoMap",
    "luciad/reference/ReferenceProvider",
    "samplecommon/LayerConfigUtil",
    
    
    'luciad/util/ColorMap',
    "./createTimeLayer",
    "recursos/js/Graficas/Graficas",
    "recursos/js/Shapes",
    
    "./painters/layerPainter",
    "./painters/EventTimePainter",
    "./painters/municipiosPainter",
    "./painters/localidadesPainter",
    "./painters/rutasPainter",
    "./painters/painterTrayectorias",
    "./painters/agebPainter",
    "./painters/nuevosPainter",
    "./painters/paisPainter",
    
    "./BalloonStyles/defaultBalloon",
    "luciad/view/controller/BasicCreateController",
    "luciad/shape/ShapeType",
    "./updateHistogram",
    "./painters/heatMapPainter",
    "recursos/js/Controller/OnHoverController",
    "recursos/js/Cluster/ClusterPainter",
    
    "./painters/alertasPainter",
    "./Tablas",
    "./painters/sectoresPainter",
    "luciad/shape/ShapeFactory",
    "./painters/GeocercasPainter",
    "recursos/js/Util",
    "./Horarios",
    "./Conexiones",
    'luciad/util/Promise',
    "./BalloonStyles/eventosBalloon",
    "template/sampleReady!"
], function (MapFactory, GoogleMap, LayerFactory, dom, on,query, PaintRepresentation, DojoMap, ReferenceProvider, LayerConfigUtil,
        ColorMap, createTimeLayer, Graficas, Shapes,
        layerPainter,  EventTimePainter, municipiosPainter, localidadesPainter, rutasPainter, painterTrayectorias, agebPainter, nuevosPainter, paisPainter,
        defaultBalloon, BasicCreateController, ShapeType, updateHistogram, heatMapPainter, OnHoverController, ClusterPainter,
        alertasPainter, Tablas, sectoresPainter, ShapeFactory, GeocercasPainter, Util, Horarios, Conexiones, Promise, eventosBalloon
) {
    
    var referenceC = ReferenceProvider.getReference("CRS:84");
    var referenceE = ReferenceProvider.getReference("EPSG:4978");
    var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i
            .test(navigator.userAgent);
    var googleMap = false;
    //Layer list
    var patrulla, zonas, sectores, ageb,manzanas, eventos = null, features =null, rutas = [], puntos =[], editable, mexico;
    var nPuntos, alertas, simularAlerta, geoCercas, capa;
    
    var selected = null, tablaDatos, menuAbierto=true, menuInteligencia = false;
    //Graficas
    var graficaTipo, graficaVictima, graficaZona, graficaDia, graficaTurno;
     /*
     * Creacion de la barra de tiempo
     */
    var nuevoRegistro = 0;
    var timeFilter, timeSlider, startTime, endTime, Year, fitColumns = false;
    var cargarDatos = true;
    /*
     * Funcion encargada de crear el mala dependiendo de si esta en un dispositivo mobil o no
     * creara el mapa en 3d o 2d, ademas de si esta activado e mapa de google 
     * 
     * Google: satellite, hybrid, roadmap o terrain
     * BingMap: AerialWithLabels, Road o Aerial
    */
    var allowLayers=false;
    var baseUrl = "https://www.olly.science/"; //https://178.128.76.236/
    var baseDataUrl = "../static/web/proyecto/Canacar/data/"; //"data/"; //
    var baseRecursosUrl = "../static/web/proyecto/recursos/"; //"../recursos/"; //
    var usuario = document.getElementById("tipoUsuario").innerHTML;
    switch(usuario.toLowerCase()) {
        case "088": usuario="u"; break;
        case "dev": usuario="u"; break;
        default: usuario="ig"; break;
    }
    Start();
    function Start() {
        if(googleMap)
            map = GoogleMap.crearMapaGoogle("map", "roadmap"); 
        else {
            if(isMobile)
                map = MapFactory.makeMap3d("map", {reference: referenceC}, {
                    includeBackground: false, includeElevation: false, includeLayerControl: true,
                    includeMouseLocation: false});
            else
                map = MapFactory.makeMap3d("map", {reference: referenceE}, {
                    includeBackground: false, includeElevation: true, includeLayerControl: true,
                    includeMouseLocation: false, includeBingLayer: "Road"});
        } 
        patrulla=null; zonas=null; sectores=null; eventos = null; features =null; rutas=[];
        graficaTipo=null; graficaVictima=null; graficaZona=null; graficaDia=null; graficaTurno=null;
        selected = null; tablaDatos = null; menuAbierto=true; menuInteligencia = false;
        CrearCapas();
        onHoverController = new OnHoverController();
        map.controller = onHoverController;
    }
    /*
     * Funcion encagrada de la creacion de todas las capas que seran mostradas en el mapa principal.
     * ademas aqui se encuentra una funcion ajax con la que leemos la informacion necesaria para crear la capa de eventos.
     * 
     * @returns {undefined}
     */
    function CrearCapas() {
        
        var urls =["CDMX", "EdoMex","Guanajuato","Hidalgo","Michoacan","Queretaro", "NuevoLeon"];
        if(allowLayers=== true) {
        for(var j =0; j<urls.length; j++){
            municipios[j]  = LayerFactory.createMemoryLayer(referenceC, {label: urls[j], selectable: false, 
                editable: false, painter: new municipiosPainter()}, baseDataUrl+"municipios/"+urls[j]+".json");
                //editable: false, painter: new municipiosPainter()}, "../static/web/proyecto/Canacar/data/municipios/"+urls[j]+".json");
            map.layerTree.addChild(municipios[j]);
            municipios[j].balloonContentProvider = function(feature) {
                return defaultBalloon.getBallon(feature);
            };
        }
        }
        /*
        municipios[0] = LayerFactory.createMemoryLayer(referenceC, {label: "Municipios", selectable: true, 
                editable: false, painter: new municipiosPainter()}, "../recursos/json/Municipios2.json");
            map.layerTree.addChild(municipios[0]);
            municipios[0].balloonContentProvider = function(feature) {
                return defaultBalloon.getBallon(feature);
            };
        */
        var urlRutas=["Toluca-Quer�taro","Toluca-Quer�taro","Toluca-Quer�taro","Azcapotzalco-Quer�taro","Azcapotzalco-Quer�taro","Iztapalapa-Quer�taro","Iztapalapa-Quer�taro"];
        
        for(j=0; j<7; j++) {
            rutas[j] = LayerFactory.createFeatureLayer(referenceC, {
                label: urlRutas[j]+" R"+(j+1), selectable: true, painter: new rutasPainter(), visible: false, editable:false
            }, baseDataUrl+"Rutas/Ruta"+(j+1)+".json");
            map.layerTree.addChild(rutas[j]);
            rutas[j].setPaintRepresentationVisible(PaintRepresentation.LABEL, false);
            rutas[j].balloonContentProvider = function(feature) {
                return defaultBalloon.getBallon(feature);
            };
        }
        rutas[j] = LayerFactory.createFeatureLayer(referenceC, {
                label: "Ruta "+(j+1), selectable: true, painter: new rutasPainter(), visible: false, editable:false
            }, baseDataUrl+"capas/ruta2.geojson");
        map.layerTree.addChild(rutas[j]);
        rutas[j].setPaintRepresentationVisible(PaintRepresentation.LABEL, false);
        rutas[j].balloonContentProvider = function(feature) {
            return defaultBalloon.getBallon(feature);
        };
       
        var urlPuntos = ["puntos1","puntos2","puntos3"];
        for(j=0;j<urlPuntos.length; j++){
            puntos[j] = LayerFactory.createFeatureLayer(referenceC, {
                label: urlPuntos[j], selectable: true, painter: new localidadesPainter(), visible: false, editable:false
            }, baseDataUrl+"Rutas/"+urlPuntos[j]+".json");
            map.layerTree.addChild(puntos[j]);
            puntos[j].balloonContentProvider = function(feature) {
                return defaultBalloon.getBallon(feature);
            };
        }
        
        nPuntos = LayerFactory.createMemoryLayer(referenceC, {label:"Nuevos Incidentes", selectable: true, painter: new nuevosPainter()});
        map.layerTree.addChild(nPuntos);
        nPuntos.balloonContentProvider = function (features) {
            return defaultBalloon.getBallon(features);
        };
        
        var proxpol = LayerFactory.createFeatureLayer(referenceC, {label: "Sectores", selectable: false, editable: false, painter: new sectoresPainter()}, baseDataUrl+"cuadrantesEscobedo.json");
        map.layerTree.addChild(proxpol);
        
        geoCercas = LayerFactory.createFeatureLayer(referenceC, {label: "Geo Cercas", selectable:false, editable:false, painter: new GeocercasPainter()}, baseDataUrl+"geocercas.json"),
        map.layerTree.addChild(geoCercas);

        capa = LayerFactory.createFeatureLayer(referenceC, {label:"Capa", selectable:true, editable:false}, baseDataUrl+"capas/Capa.geojson");
        map.layerTree.addChild(capa);
        capa.balloonContentProvider = function (features) {
            return defaultBalloon.getBallon(features);
        };
        
        
        simularAlerta = LayerFactory.createMemoryLayer(referenceC, {label:"Simulacion", selectable: true, editable: false, painter: new alertasPainter()});
        map.layerTree.addChild(simularAlerta);
        simularAlerta.setPaintRepresentationVisible(PaintRepresentation.LABEL, false);
        simularAlerta.balloonContentProvider = function (features) {
            return defaultBalloon.getBallon(features);
        };
        
        
        editable = LayerFactory.createMemoryLayer(referenceC, {label: "Rutas", selectable: true, painter: new rutasPainter(), editable: true});
        map.layerTree.addChild(editable);
        editable.balloonContentProvider = function (features) {
            return defaultBalloon.getBallon(features);
        };
        
        DojoMap.configure(map);
        LayerConfigUtil.addLonLatGridLayer(map);
        //var mexicoLayer = LayerFactory.createKmlLayer({label:"Mexico", selectable:false, painter: new paisPainter()},'../recursos/estados.kml');
        //map.layerTree.addChild(mexicoLayer);
        //mexico = LayerFactory.createFeatureLayer(referenceC, {label: "Mexico", painter: new paisPainter(), editable: false}, "../static/web/proyecto/recursos/estados.json");
        mexico = LayerFactory.createFeatureLayer(referenceC, {label: "Mexico", painter: new paisPainter(), editable: false}, baseRecursosUrl+"estados.json");
        map.layerTree.addChild(mexico);
        var queryFinishedHandle = mexico.workingSet.on("QueryFinished", function() {
            if(mexico.bounds) 
                map.mapNavigator.fit({bounds: mexico.bounds, animate: true});
            queryFinishedHandle.remove();
        });
        
        alertas = LayerFactory.createMemoryLayer(referenceC, {label:"Alertas", visible: true, selectable: true, editable: true, painter: new alertasPainter()});
        map.layerTree.addChild(alertas);
        alertas.setPaintRepresentationVisible(PaintRepresentation.LABEL, false);
        alertas.balloonContentProvider = function (features) {
            return defaultBalloon.getBallon(features);
        };
        /*
         * 
         * Aqui creamos una variable la cual usaremos para identificar el a�o del cual mostraremos la informacion
         */
        var sStart = "2014/01/01 00:00:00", sEnd = "2017/01/01 00:00:00";
        startTime = Date.parse(sStart)/1000, endTime = Date.parse(sEnd)/1000;
        //leerJSONLocal();
        leerServidor();
    }
    /*
     *============================================================================================================
     *                                      Craear la capa de Incidentes
     *============================================================================================================
     */
    function leerJSONLocal() {
        
        $.ajax({
            type:'Get',
            dataType: "json",
            url: baseDataUrl+"dummy1.geojson"
        }).done(function(data) {
            console.log("archivo leido");
            features = createTimeLayer.createTimeFeature(data.features);
            var graficas = crearGraficas(features);
            
            var store = LayerFactory.MemoryStore({data: features});
            var model = LayerFactory.FeatureModel(store, {reference: referenceC});
            eventos = LayerFactory.FeatureLayer(model, {
                label: "Incidentes", 
                id: "eventos", 
                editable:false, 
                painter: new heatMapPainter(), 
                //painter: ClusterPainter.painter,
                //transformer: ClusterPainter.transformer,
                visible: true,
                selectable: true
            });
            
            eventos.balloonContentProvider = function(feature) {
                return defaultBalloon.getBallon(feature);
            };
            map.layerTree.addChild(eventos);
            updateHistogram.updateHistogram(map, features, eventos, referenceC, startTime, endTime, tablaDatos, graficas,municipios );
            
        }).fail(function(e) {
            console.log(e);
        });
    }
    function leerServidor() {
        var options = {label: "Incidentes", 
                id: "eventos", 
                editable:false, 
                painter: new heatMapPainter(), 
                visible: true,
                selectable: true
        };
        eventos = LayerFactory.createFeatureLayer(referenceC, options, baseUrl+"heatmap/data="+usuario);
        map.layerTree.addChild(eventos);
        eventos.balloonContentProvider = function(feature) {
            return eventosBalloon.getBallon(feature);
        };
        var promiseFeatures = eventos.model.query();
        Promise.when(promiseFeatures, function(cursor) {
            features = [];
            var index = 0;
            while (cursor.hasNext()) {
                var feature = cursor.next();
                features[index] = feature;
                index++;
            }
            console.log("Cargados "+index+" eventos");
            features = createTimeLayer.createTimeFeature(features);
            var graficas = crearGraficas(features);
            updateHistogram.updateHistogram(map, features, eventos, referenceC, startTime, endTime, tablaDatos, graficas,municipios );
        });
    }
    
    function reestablecerEventos() {
        map.layerTree.removeChild(eventos);
        updateHistogram.destruirComponentes();
        setTimeout(leerServidor, 500);
    }

    /*
     *============================================================================================================
     *                                     Funciones de algunos botones
     *============================================================================================================
     */
    /*
     * botones superiores en el mapa
     */
    document.getElementById("botonGraficas").value = menuAbierto;
    $("#botonGraficas").click(function () {
        if(menuAbierto) {
            $("#menuGraficas").fadeOut("slow");
            $("#afterafter").fadeOut("slow");
            menuAbierto=false;
        }else {
            //$("#menuInteligencia").fadeOut("slow");
            $("#menuGraficas").fadeIn("slow");
            $("#panelHorario").fadeOut("slow");
            $("#afterafter").fadeIn("slow");
            menuAbierto=true;
            menuInteligencia = false;
        }
        document.getElementById("botonGraficas").value = menuAbierto;
    });
    $("#botonInteligencia").click(function () {
        if(menuInteligencia) {
            $("#panelHorario").fadeOut("slow");
            //$("#menuInteligencia").fadeOut("slow");
            menuInteligencia = false;
        }else {
            $("#menuGraficas").fadeOut("slow");
            $("#afterafter").fadeOut("slow");
            $("#panelHorario").fadeIn("slow");
            //$("#menuInteligencia").fadeIn("slow");
            menuAbierto=false;
            menuInteligencia = true;
        }
    });
    $("#crearRuta").click(function() {
            $("#panelRegistro").fadeIn("slow");
            $("#menuGraficas").fadeOut("slow");
            $("#afterafter").fadeOut("slow");
            $("#pTablaToken").fadeOut("slow");
            menuAbierto=false;
    });
    /*
     *============================================================================================================
     *                                      Exportar Informacion
     *============================================================================================================
     */
    $("#exportatTodaInformacion").click(function(boton) {
        Tablas.crearTablaDIV("tablaTemporal",  tablaDatos, 1000);
        Util.exportarTablaXLS(boton, "tablaTemporal");
    });
    $("#exportatInformacion").click(function(boton) {
        Util.exportarTablaXLS(boton, "tablaCompleta");
    });
    /*
     *============================================================================================================
     *                                      Reloj
     *============================================================================================================
     */
    var X = -1, x=0, y = 30, y2 = 0;
    timeUpdated();
    function timeUpdated() {
        $('#timelabel').text(formatTime());
        
        y++;
        if(y>5) {
            y=0;
            Conexiones.verAlertasReales(baseUrl, map, alertas, referenceC, Shapes, usuario);
        }
        
        var value = document.getElementById( "estadosFiltro" ).selectedOptions[0].value;
        if(value === "Todos" && allowLayers===true) {
            for(var nm=0; nm<municipios.length; nm++) {
                municipios[nm].visible = true;
                municipios[nm].filter = function(feature) {
                    return true;
                };
            }
        }
        
        if(coordenadasNuevaAlerta.length>0) {
            var coordenadas = coordenadasNuevaAlerta;
            coordenadasNuevaAlerta=[];
            generarAlerta(coordenadas);
        }
        setTimeout(timeUpdated,500);
     }
     /*
      * con esta funcion daremos formato a la etiqueta del reloj, utilizando el tiempo actual del equipo.
      * @returns {String}
      */
     function formatTime() {
        var date = new Date();
        var h = date.getHours() <10 ? "0"+date.getHours(): date.getHours();
        var m = date.getMinutes() <10 ? "0"+date.getMinutes(): date.getMinutes();
        var s = date.getSeconds() <10 ? "0"+date.getSeconds(): date.getSeconds();
        return h+":"+m+":"+s;
    }
   
    ////========================================================================================///
    function formatDate( fecha, hora) {
        var i, j=0, y, m, d, c, newDate="";
        var baseDate = "aa/mes/dia hora:00";
        for(i=0; i<fecha.length; i++) {
            c = fecha.charAt(i);
            if(c ==='/' || c===' ' || c==='/n' || c===':') {
                switch(j) {
                    case 0: d = newDate; break;
                    case 1: m = newDate; break;
                    case 2: y = newDate; break;
                }
                newDate = "";
                j++;
            } else {
                newDate+= c;
            }
        }
        y = newDate;
        newDate = baseDate.replace("aa", y).replace("mes", m).replace("dia", d).replace("hora:00", hora);
        return newDate;
    }
    /*
     *============================================================================================================
     *                                      Ocultar Etiquetas
     *============================================================================================================
     */
    $("#labels").click(function () {
        var domId = document.getElementById("labels");
        var value;
        if(domId.value === "false" || domId.value===false || !domId.value ) {
            value = false;
        } else
            value = true;
        if(allowLayers===true) {
        for(var i=0; i<municipios.length; i++) {
            municipios[i].setPaintRepresentationVisible(PaintRepresentation.LABEL, value);
            municipios[i].selectable = !value;
        }
        }
        mexico.setPaintRepresentationVisible(PaintRepresentation.LABEL, value);
        if(value===true) 
            domId.title = "Ocultar Etiquetas";
        else
            domId.title = "Mostrar Etiquetas";
        domId.value = !value;
    });
    
    Horarios.crearHorario("horario");
    
    /*
     *============================================================================================================
     *                                      CONEXIONES CON ENDPOINTS
     *============================================================================================================
     */
    
    Conexiones.initCuestionario(baseUrl);
    Conexiones.getTokenStatus(baseUrl);
    Conexiones.getFoliosStatus(baseUrl);
    Conexiones.actualizarPlacas(baseUrl);
    Conexiones.obtenerDatosColaboradores(baseUrl);
    $("#registrarViaje").click(function () {
        Conexiones.initCuestionario(baseUrl);
        Conexiones.getTokenStatus(baseUrl);
        Conexiones.actualizarPlacas(baseUrl);
        Conexiones.registrarViaje(baseUrl);
    });
    $("#actualizarToken").click(function () {
        Conexiones.actualizarToken(baseUrl);
    });
    $("#restart").click(function (){
        Conexiones.initCuestionario(baseUrl);
        Conexiones.getTokenStatus(baseUrl);
        Conexiones.getFoliosStatus(baseUrl);
        Conexiones.actualizarPlacas(baseUrl);
        Conexiones.obtenerDatosColaboradores(baseUrl);
        reestablecerEventos();
    });
    $("#registrarEmpleado").click(function (){
        Conexiones.registrarEmpleado(baseUrl);
    });
    $("#buscarCurp").click(function (){
        Conexiones.buscarCurp();
    });
    $("#registrarVehiculo").click(function (){
        Conexiones.registrarVehiculo(baseUrl);
    });
    $("#registrarRobo").click(function (){
        Conexiones.registrarRobo(baseUrl);
    });
    try {
    $("#limpiarRegistroViaje").click(function() {
        Conexiones.initCuestionario(baseUrl);
    });
    $("#limpiarRegistroEmpleado").click(function() {
       var divs = ["regCurp","regSexo","regNombres","regEntidad","regPaterno","regMunicipio","regMaterno","regRFC","regNacimiento","regApodo"];
       limpiarCampos(divs);
    });
    $("#limpiarRegistroVahiculo").click(function() {
        var divs = ["regPlaca","regModelo", "regMarca","regSerie","regColor","regTipo","regValor","regGPS"];
        limpiarCampos(divs);
    });
    }catch(e) {
        console.log(e);
    }
    var folioNode = document.getElementById("roboFolio");
    folioNode.addEventListener("change", function () {
        var folio = folioNode.selectedOptions[0].innerHTML;
        if(folio === "Folios") {
            return 0;
        }
        Conexiones.obtenerDatosFolio(baseUrl, folio);
    });
    $("#registrarRuta").click(function (){
        var files = document.getElementById("archivoSeleccionado").files;
        var layer;
        if(files !== null) {
            var fileName, extension, layerName, options;
            for (var i = 0; i < files.length; i++) {
                fileName = files[i].name;
                extension = fileName.toLowerCase().replace(/.*\./, "");
                layerName = fileName.replace(/.*\//, "").replace(/\.[^.]*$/, "");
                var options = {label: layerName, selectable: true, painter: new rutasPainter()};
                if (extension === "json" || extension === "geojson") {
                    layer = LayerFactory.createFeatureLayer(referenceC, options, files[i]);
                } else if (extension.indexOf("gml") > -1 || extension === "xml") {
                    layer = LayerFactory.createXMLLayer(referenceC, options, files[i]);
                }
                
                if(layer) {
                    map.layerTree.addChild(layer);
                    var queryFinishedHandle = layer.workingSet.on("QueryFinished", function() {
                        if(layer.bounds) 
                            map.mapNavigator.fit({bounds: layer.bounds, animate: true});
                        queryFinishedHandle.remove();
                    });
                    document.getElementById("archivoSeleccionado").files=null;
                }
            }
        } else {
            alert("No se selecciono ningun archivo");
        }
        
    });
    var nBusquedas=0;
    $("#buscarCurp").click(function () {
        $.ajax({
            type:'Get',
            dataType: "json",
            url: baseDataUrl+"nombres.json"
        }).done(function(data) {
            var personas = data.nombres;
            var persona, aCurp;
            var curp = document.getElementById("regCurp").value;
            for(var i=0;i<personas.length; i++) {
                aCurp = personas[i].curp;
                if(curp === aCurp) {
                    persona = personas[i];
                    break;
                }
            }
            if(!persona)
                persona = personas[nBusquedas];
            document.getElementById("regCurp").value = persona.curp || "";
            document.getElementById("regNombres").value = persona.name || "";
            document.getElementById("regPaterno").value = persona.paterno || "";
            document.getElementById("regMaterno").value = persona.materno || "";
            document.getElementById("regEntidad").value = persona.entidad || "";
            document.getElementById("regMunicipio").value = persona.municipio || "";
            document.getElementById("regRFC").value = persona.rfc || "";
            document.getElementById("regNacimiento").value = persona.nacimiento || "";
            document.getElementById("regSexo").value = persona.sexo || "";
            nBusquedas = nBusquedas>=personas.length? 0: nBusquedas+1;
        }).fail(function(e) {
            console.log("No se encontro el archivo");
        });
    });
    /*
     *============================================================================================================
     *                                      CREACION DE GRAFICAS
     *============================================================================================================
     */
    var fil = ["empresasFiltro", "estadosFiltro", "vehiculosFiltro", "cargaFiltro"], lab =["empresa", "entidad", "tvehiculo", "carga"];
    var titles = ["Empresas","Entidad","Tipo de Vehiculo", "Tipo de Carga"];
    var types = ["piechart",  "piechart", "piechart", "piechart"];
    var tops = [];
    var gEmpresas={}, gNombres={}, gMotivo={};
    switch(usuario) {
        case "ig":
            lab =["entidad", "tvehiculo", "carga"];
            fil = ["estadosFiltro", "vehiculosFiltro", "cargaFiltro"];
            titles = ["Entidad","MTipo de Vehiculo", "Tipo de Carga"];
            types = ["piechart", "piechart", "piechart"];
            tops = [10,10,10];
            break;
        case "u":
            fil = ["empresasFiltro", "estadosFiltro", "vehiculosFiltro", "cargaFiltro"];
            lab =["empresa", "entidad", "tvehiculo", "carga", "placas","curp"];
            titles = ["Empresas","Entidad","Tipo de Vehiculo", "Tipo de Carga", "Top 5 Placas", "Top 5 Conductores"];
            types = ["piechart",  "piechart", "piechart", "piechart"];
            tops = [10,10,10,10,5,5];
            break;
    }
    function crearGraficas(features) {
        var datos = Graficas.crearGraficasFeatures(features, "Graficas", lab, types, fil, null, titles, null, tops);
        var graficas = datos.graficas;
        tablaDatos = datos.tablaDatos;
        Tablas.crearTablaDIV("tablaCompleta", tablaDatos, 1000);
        Tablas.crearTablaDatosExtras("tablaDatosExtras", tablaDatos, 100);
        datos = null;
        return graficas;
        
    }
    /*
     *      Control de Densidad del mapa de calor
     */
    $("#Densidad").on("change input", function() {
        var x = 1, y = 0;
        x = Math.round(parseFloat($("#Densidad").val()));
        if(x<=10 && x >0) {
            eventos.painter.density = {
                colorMap: ColorMap.createGradientColorMap([
                {level: y, color: "rgba(  0,   0,   255, 0.5)"},
                {level: y+=x, color: "rgba(  0, 100,   255, 0.5)"},
                {level: y+=x*2, color: "rgba(  0, 255,   255, 1.0)"},
                {level: y+=x*3, color: "rgba(  255, 255,   0, 1.0)"},
                {level: y+=x*4, color: "rgba(255, 0, 0, 1.0)"}
                ])
            };
        } else {
            eventos.painter.density = null;
        }
    });
    
    //actualizarTabla();
    function actualizarTabla(f) {
        var features;
        if(f) {
           features = f; 
        } else {
            var data = alertas.model.query();
            features = data.array;
        }
        
        var n = features.length;
        var tabla = '<table>#contenido</table>';
        var etiqueta = '';
        var fila = '<tr bgcolor="$COLOR"><td>Estado</td><td>Hora</td><td>Fecha</td><td style="max-width: 270px;">Token</td></tr>';
        etiqueta += fila;
        var j=0;
        for(var i=0; i<n; i++) {
            var id = features[i].properties.id;
            //var id = Shapes.getRandom(8000, 9999);
            var estado = features[i].properties.status;
            var hora = features[i].properties.hora;
            var fecha = features[i].properties.fecha;
            var color;
            var geometry = features[i].geometry;
            if(geometry.x || geometry.type === "Point") {
                if(j%2 === 0 )
                    color = '';
                else
                    color = ' #566573 ';
                etiqueta += fila.replace("Estado", estado).replace("Hora", hora).replace("Fecha", fecha).replace("Token", id).replace("$COLOR", color);
                j++;
            }
        }
        tabla = tabla.replace("#contenido", etiqueta);
        document.getElementById("tablaToken").innerHTML = tabla;
    }
    
    var pTokens = false;
    $("#bTokens").click(function () {
        $("#pTablaToken").fadeIn("slow");
    });
    
    /*
     * ===========================================================================
     *                  Geocercas
     * ===========================================================================
     */
    $("#bGeocercas").click(function () {
        //geocercas.visible = !alertas.visible;
        //bAlerta.value = value;
        if(geoCercas.bounds) 
                map.mapNavigator.fit({bounds: geoCercas.bounds, animate: true});
        var queryFinishedHandle = geoCercas.workingSet.on("QueryFinished", function() {
            if(geoCercas.bounds) 
                map.mapNavigator.fit({bounds: geoCercas.bounds, animate: true});
            queryFinishedHandle.remove();
        });
    });
    /*
     * ===========================================================================
     *                  Alertas Reales
     * ===========================================================================
     */
    $("#bAlertas").click(function () {
        //$("#menuAlertas").fadeOut("slow");
        alertas.visible = !alertas.visible;
        if(alertas.visible) {
            if(alertas.bounds)
                map.mapNavigator.fit({bounds: alertas.bounds, animate: true});
            else {
            var queryFinishedHandle = alertas.workingSet.on("QueryFinished", function() {
                if(alertas.bounds)
                    map.mapNavigator.fit({bounds: alertas.bounds, animate: true});
                queryFinishedHandle.remove();
            });
            }
        }
    });
    
    $("#bGenerarAlerta").click(function () {
        $("#menuAlertas").fadeOut("slow");
        var pAlertas = document.getElementById("bGenerarAlerta").value;
        if(pAlertas === true || pAlertas === "true") {
            $("#panelAlerta").fadeOut("slow");
            document.getElementById("bGenerarAlerta").value = false;
        } else {
            $("#panelAlerta").fadeIn("slow");
            document.getElementById("bGenerarAlerta").value = true;
        }
    });
    ////========================================================================
    var nS=0;
    $("#bSimulacion").click(function() {
        $("#menuAlertas").fadeOut("slow");
        var coordinates = [[ -100.32175, 25.78995 ], [ -100.32854, 25.77114 ],[ -100.37165, 25.80416 ]];
        var proxpol = [1,4,8];
        var point = Shapes.createPoint(referenceC, coordinates[nS][0], coordinates[nS][1], 0, nS, {Proxpol: proxpol[nS]});
        simularAlerta.model.put(point);
        alert("Se a detectado una nueva alerta");
        var coordenadas = [coordinates[nS][0],0.5,coordinates[nS][1], 0.5, 1, 0];
        map.mapNavigator.fit({
                bounds: ShapeFactory.createBounds(referenceC, coordenadas),
                animate: true
        });
        nS++;
        if(nS>coordinates.length){
            nS=0;
        }
    });
    
    
    function generarAlerta(coordenadas) {
        var folioNode = document.getElementById("sFolio");
        var folio = folioNode.selectedOptions[0].innerHTML;
        var hora = Util.formatTime(), fecha = Util.formatDate();
        var coordenada = coordenadas[0];
        var h = coordenadas[2][1];
        var w = coordenadas[2][0];
        var bounds = ShapeFactory.createBounds(referenceC, [coordenada[0], w, coordenada[1], h]);
        coordenada = [bounds.focusPoint.x, bounds.focusPoint.y];
        var ubicacion = document.getElementById("pac-input").value;
        var propiedades = {
            folio: folio,
            fecha: fecha,
            hora: hora,
            lng: coordenada[0],
            lat: coordenada[1],
            ubicacion: ubicacion,
            status: "alerta"
        };
        h = h< 0.01? 0.01 : h;
        w = w< 0.01? 0.01 : w;
        var alerta = Shapes.createPoint(referenceC, coordenada[0], coordenada[1], 0, "GAlerta", propiedades);
        simularAlerta.model.put(alerta);
        map.mapNavigator.fit({
                bounds: ShapeFactory.createBounds(referenceC, [coordenadas[0][0], w, coordenadas[0][1], h]),
                animate: true
        });
    }
    $("#bEnviarAlerta").click(function() {
        var alerta = simularAlerta.model.get("GAlerta");
        if(!alerta) {
            alert("No se selecciono ninguna ubicacion");
            return;
        } else {
            var propiedades = alerta.properties;
            var folio = propiedades.folio;
            
            if(folio === "Folios" || folio === "No Folios Activos") {
                var folioNode = document.getElementById("sFolio");
                var selectedFolio = folioNode.selectedOptions[0].innerHTML;
                if(selectedFolio === "Folios" || selectedFolio === "No Folios Activos") {
                   alert("No se selecciono un Folio");
                    return; 
                } else {
                    folio = selectedFolio;
                    propiedades.folio = folio;
                    alerta.properties = propiedades;
                    simularAlerta.model.put(alerta);
                }
            }
            console.log(propiedades);
            var confirmar = confirm("Se creara la alerta con los siguientes datos\n"+JSON.stringify(propiedades));
            if(confirmar === true)
                Conexiones.enviarAlerta(baseUrl, propiedades);
        }
    });
    /*
     * ==========================================================================================
     *              Informacion del Registro
     * ==========================================================================================
     */
    var divsConductor = ["nombre","paterno","materno","edad","sexo"];
    var divsCopiloto = ["nombre2","paterno2","materno2","edad2","sexo2"];
    var selectCurp = document.getElementById("infoConductor");
    selectCurp.addEventListener("change", function () {
        var valueCurp = selectCurp.selectedOptions[0].value;
        if(valueCurp===0 || valueCurp==="0") {
            alert("No se selecciono ningun empleado");
            limpiarCampos(divsConductor);
            return null;
        }
        valueCurp = selectCurp.selectedOptions[0].innerHTML;
        Conexiones.obtenerDatosColaboradores(baseUrl, valueCurp, divsConductor);
    });
    var selectCopiloto = document.getElementById("infoCopiloto");
    selectCopiloto.addEventListener("change", function () {
        var copiloto = selectCopiloto.selectedOptions[0].value;
        if(copiloto===0 || copiloto==="0") {
            alert("No se selecciono ningun empleado");
            limpiarCampos(divsCopiloto);
            return null;
        }
        copiloto = selectCopiloto.selectedOptions[0].innerHTML;
        var conductor = selectCurp.selectedOptions[0].innerHTML;
        if(conductor === copiloto) {
            alert("No se puede asignar dos veces el mismo CURP");
            limpiarCampos(divsCopiloto);
            return null;
        }
        Conexiones.obtenerDatosColaboradores(baseUrl, copiloto, divsCopiloto);
    });
    function limpiarCampos(divs) {
        divs.forEach(function(div) {
            document.getElementById(div).value = "";
        });
    }
        /*var nombre, paterno, materno, edad, sexo;
        var nombres = datos.nombres;
        switch(valueCurp) {
            case "1":
                nombre = nombres[0].name;
                paterno = nombres[0].paterno;
                materno = nombres[0].materno;
                edad = nombres[0].edad;
                sexo = nombres[0].sexo;
                break;
            case "2":
                nombre = nombres[1].name;
                paterno = nombres[1].paterno;
                materno = nombres[1].materno;
                edad = nombres[1].edad;
                sexo = nombres[1].sexo;
                break;
            case "3":
                nombre = nombres[2].name;
                paterno = nombres[2].paterno;
                materno = nombres[2].materno;
                edad = nombres[2].edad;
                sexo = nombres[2].sexo;
                break;
            default: 
                nombre = "";
                paterno = "";
                materno = "";
                edad = "";
                sexo = "";
                break;
        }
        document.getElementById("nombre").value = nombre;
        document.getElementById("paterno").value = paterno;
        document.getElementById("materno").value = materno;
        document.getElementById("edad").value = edad;
        document.getElementById("sexo").value = sexo;*/
    
    var selectPlaca = document.getElementById("placas");
    selectPlaca.addEventListener("change", function (data) {
        var valuePlaca = selectPlaca.selectedOptions[0].value;
        if(valuePlaca === 0 || valuePlaca ==="0") {
            alert("No se selecciono ninguna placa");
            return null;
        }
        valuePlaca = selectPlaca.selectedOptions[0].innerHTML;
        Conexiones.obtenerDatosPlacas(baseUrl, valuePlaca);
    });
        /*
        var  marca, tipo, modelo, serie, gps, color, valor;
        var nombres = datos.Autos;
        switch(valuePlaca) {
            case "U12DNA":
                marca = nombres[0].marca;
                tipo = nombres[0].tipo;
                modelo = nombres[0].modelo;
                serie = nombres[0].serie;
                gps = nombres[0].gps;
                color = nombres[0].color;
                valor = nombres[0].valor;
                break;
            case "ZMT136":
                marca = nombres[1].marca;
                tipo = nombres[1].tipo;
                modelo = nombres[1].modelo;
                serie = nombres[1].serie;
                gps = nombres[1].gps;
                color = nombres[1].color;
                valor = nombres[1].valor;
                break;
            case "A47NOT":
                marca = nombres[2].marca;
                tipo = nombres[2].tipo;
                modelo = nombres[2].modelo;
                serie = nombres[2].serie;
                gps = nombres[2].gps;
                color = nombres[2].color;
                valor = nombres[2].valor;
                break;
        }
        document.getElementById("marca").value = marca;
        document.getElementById("tipo").value = tipo;
        document.getElementById("modelo").value = modelo;
        document.getElementById("serie").value = serie;
        document.getElementById("gps").value = gps;
        document.getElementById("color").value = color;
        document.getElementById("valorVehiculo").value = valor;
        
    });
       
    /* ==========================================================================================
     *                  Seleccionar Rutas
     * ==========================================================================================
     */
    var selectRuta = document.getElementById("sruta");
    selectRuta.addEventListener("change", seleccionarRuta);
    var selectRuta2 = document.getElementById("sruta2");
    selectRuta2.addEventListener("change", seleccionarRuta);
    function  seleccionarRuta(data) {
        var target = data.currentTarget;
        var valueRuta = target.selectedOptions[0].value;
        switch(valueRuta) {
                case "1":
                    puntos[0].visible = !puntos[0].visible;
                    switch(X) {
                        case 0: 
                            rutas[0].visible = !rutas[0].visible; 
                            rutas[1].visible = false;
                            rutas[2].visible = false;
                            break;
                        case 1:
                            rutas[1].visible = !rutas[1].visible; 
                            rutas[0].visible = false;
                            rutas[2].visible = false;
                            break;
                        default: 
                            rutas[2].visible = !rutas[2].visible;
                            rutas[0].visible = false;
                            rutas[1].visible = false;
                            break;
                    }
                    break;
                case "2":
                    puntos[1].visible = !puntos[1].visible;
                    if(X === 0 ) {
                        rutas[3].visible = !rutas[3].visible;
                        rutas[4].visible = false;
                    } else {
                        rutas[3].visible = false;
                        rutas[4].visible = !rutas[4].visible;
                    }
                    break;
                case "3":
                    puntos[2].visible = !puntos[2].visible;
                    if(X === 0 ) {
                        rutas[5].visible = !rutas[5].visible;
                        rutas[6].visible = false;
                    } else {
                        rutas[5].visible = false;
                        rutas[6].visible = !rutas[6].visible;
                    }
                    break;
                case "4":
                    rutas[7].visible = !rutas[7].visible;
                    break;
                default:
                    rutas.forEach(function(ruta) {
                        ruta.visible = false;
                    });
                    puntos[0].visible = false;
                    puntos[1].visible = false;
                    puntos[2].visible = false;
                    break;
            }
    }
    
    
});


var hacerFit = false;
function filtrarMunicipio(nombre) {
    hacerFit = true;
    for(var id in municipios) {
        municipios[id].filter = function (feature) {
            var name = feature.properties.NOMBRE;
            if(name ===nombre && hacerFit === true) {
                map.mapNavigator.fit({bounds: feature.geometry.bounds, animate: true});
                hacerFit = false;
            } 
            return true;
        };
    }
}

function borrarFiltro () {
    for(var layer in municipios) {
        municipios[layer].filter = null;
    };
}

function getIncidentes() {
    return tablaIncidentes;
}


