/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define([
    
], function () {
    function getBallon(feature, b) {
        var etiqueta = "", etiquetaDatos="";
        var _etiqueta = //block, flex
                '<div class="labelDatos" style="display:block !important">' +
                '<table>'+
                 '$DATOS' +
                '</table>' +
                '</div>';
        var _etiquetaDatos = '<tr bgcolor="$COLOR">' +
                '<td align="center">' +
                '<b>$Propertie</b>' +
                '</td >' +
                '<td align="center">' +
                '<b>$Value</b>' +
                '</td >' +
                '</tr>';
        //var name = "Transmision en vivo";
        var properties = feature.properties;
        var i=0, color, label="";
        if(properties) {
            for (var key in properties) {
                if(i%2 === 0 )
                    color = '#19232c';
                else
                    color = '';
                i++;
                label = getLabel(key);
                etiquetaDatos+= _etiquetaDatos.replace("$Propertie", label+":").replace("$Value", properties[key]).replace("$COLOR", color);
            }
        }
            
        etiqueta = _etiqueta.replace("$DATOS", etiquetaDatos);
        
        return etiqueta;   
    }
    
    function getLabel(key) {
        switch(key) {
            case "alerta": return "Alerta";
            case "averiguaci": return key;
            case "averiguacion": return "Averiguacion previa";
            case "carga": return "Carga";
            case "conductor": return "Conductor";
            case "copiloto": return "Copiloto";
            case "curp": return "CURP";
            case "denuncia": return key;
            case "efectivo": return "Efectivo";
            case "empresa": return "Empresa";
            case "entidad": return "Entidad";
            case "fecha": return "Fecha del incidente";
            case "fllegada": return "Fecha de llegada";
            case "folio": return "Folio";
            case "fsalida": return "Fecha de salida";
            case "giro": return "Giro";
            case "hllegada": return "Hora de llegada";
            case "hora": return "Hora del incidente";
            case "hsalida": return "Hora de Salida";
            case "latlng": return "Coordenadas";
            case "merca": return "Valor de la mercancia";
            case "modelo": return "Modelo";
            case "modus": return "Modu operanti";
            case "municipio": return "Municipio";
            case "organizaci": return "Organizacion";
            case "peso": return "Peso";
            case "ruta": return "Ruta";
            case "tipo": return "Tipo de robo";
            case "tramo": return "Tramo";
            case "tvehiculo": return "Tipo de vehiculo";
            case "vehiculos": return "Vehiculos identificados";
            case "transportista": return "Transportista";
            case "placas": return "Placas";
            default: return "no encontrado "+key;
        }
    }
    
    return  { getBallon: getBallon };
});


