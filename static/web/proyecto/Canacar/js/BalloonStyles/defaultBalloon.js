/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define([
    
], function () {
    function getBallon(feature, b) {
        var etiqueta = "", etiquetaDatos="";
        var _etiqueta = //block, flex
                '<div class="labelDatos" style="display:block !important">' +
                '<table>'+
                 '$DATOS' +
                '</table>' +
                'boton' +
                '</div>';
        var _etiquetaDatos = '<tr bgcolor="$COLOR">' +
                '<td align="center">' +
                '<b>$Propertie</b>' +
                '</td >' +
                '<td align="center">' +
                '<b>$Value</b>' +
                '</td >' +
                '</tr>';
        //var name = "Transmision en vivo";
        var properties = feature.properties;
        var i=0, color;
        if(properties) {
            for (var key in properties) {
                if(i%2 === 0 )
                    color = '#19232c';
                else
                    color = '';
                i++;
                etiquetaDatos+= _etiquetaDatos.replace("$Propertie", key+":").replace("$Value", properties[key]).replace("$COLOR", color);
            }
        }
            
        etiqueta = _etiqueta.replace("$DATOS", etiquetaDatos);
        var boton, video;
        
        if(b) {
            if(properties.Type) {
                if(properties.Type === "Patrulla")
                    video = "panelDirecto";
                if(properties.Type === "Dron")
                    video = "panelVideo3";
            } else {
                video = "panelVideo2"
            }
            boton = "<a onclick=\"MostrarOcultar('"+video+"', null);\" role='button' class=\"btn btn-default\">Video</a>";
            etiqueta = _etiqueta.replace("$DATOS", etiquetaDatos).replace("boton", boton);
        } else {
            etiqueta = _etiqueta.replace("$DATOS", etiquetaDatos).replace("boton", "");
        }
        return etiqueta;   
    }
    
    return  { getBallon: getBallon };
});


