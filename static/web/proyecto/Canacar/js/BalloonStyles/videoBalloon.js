/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define([
    
], function () {
    function getBallon(feature, b) {
        var etiqueta = "", etiquetaDatos="";
        var _etiqueta = //block, flex
                '<div class="labelDatos" style="display:block !important">' +
                        '<label>titulo</label>'+
                '<table>'+
                 '$DATOS' +
                '</table>' +
                '<table>' +
                '<tr>video</tr>' +
                '<tr>boton</tr>' +
                '</table>'+
                '</div>';
        var _etiquetaDatos = '<tr bgcolor="$COLOR">' +
                '<td align="center">' +
                '<b>$Propertie</b>' +
                '</td >' +
                '<td align="center">' +
                '<b>$Value</b>' +
                '</td >' +
                '</tr>';
        //var name = "Transmision en vivo";
        var properties = feature.properties;
        var i=0, color;
        if(properties) {
            for (var key in properties) {
                if(i%2 === 0 )
                    color = '#19232c';
                else
                    color = '';
                i++;
                etiquetaDatos+= _etiquetaDatos.replace("$Propertie", key+":").replace("$Value", properties[key]).replace("$COLOR", color);
            }
        }
            
        etiqueta = _etiqueta.replace("$DATOS", etiquetaDatos);
        var boton, panel, video, titulo;
        video = '';
        if(b) {
            if(properties.Type) {
                if(properties.Type === "Patrulla") {
                    titulo = "Patrulla";
                    panel = "panelDirecto";
                    //video = '<iframe width="350" height="280" src="https://www.youtube.com/embed/4gt_4C5G0gE" frameborder="0"></iframe>';
                }
                if(properties.Type === "Dron") {
                    titulo = "Dron";
                    panel = "panelDirectoDron";
                    video = '';
                }
            } else {
                titulo = "Filtro Inteligente";
                panel = "panelVideo2";
                video = '';
            }
            boton = "<a onclick=\"MostrarOcultar('"+panel+"', null);\" role='button' class=\"btn btn-default\">Video</a>";
            etiqueta = _etiqueta.replace("$DATOS", etiquetaDatos).replace("boton", boton).replace("video", video).replace("titulo", titulo);
        } else {
            etiqueta = _etiqueta.replace("$DATOS", etiquetaDatos).replace("boton", "").replace("video", "").replace("titulo", "");
        }
        return etiqueta;   
    }
    
    return  { getBallon: getBallon };
});


