/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define([
    'luciad/view/LayerType',
    'luciad/view/feature/FeatureLayer',
  'luciad/reference/ReferenceProvider',
  "luciad/model/feature/FeatureModel",
  "luciad/model/store/MemoryStore",
  "luciad/shape/ShapeFactory",
  'samples/timeslider/TimeSlider',
  "luciad/model/feature/Feature",
  "luciad/view/feature/FeaturePainter",
  "luciad/view/style/PointLabelPosition"
], function (LayerType, FeatureLayer, ReferenceProvider, FeatureModel, MemoryStore, 
        ShapeFactory, TimeSlider, Feature, FeaturePainter, PointLabelPosition) {
   
    function createTimeLineModel(features, n, s, e) {
      //var data = features.properties.DATOS;
      var elements = [];
      var startTime = s, endTime = e;
      var avance = (endTime - startTime)/n;
      var firstTime = startTime;
      var secondTime = startTime + avance;
      var data = 0, feature, e;
      var mes = 1;
       if(n===36)
           e = 10;
       else
           e = 50;
      
      for ( var i = 0; i < n; i++) {
        for(var j = 0; j < features.length; j++) {
            feature = features[j];
            var time = Date.parse(feature.properties.EventTime)/1000;
            if(time >= firstTime && time < secondTime)
                data++;  
        }
        var p1 = ShapeFactory.createPoint(TimeSlider.REFERENCE, [firstTime, 0]);
        var p2 = ShapeFactory.createPoint(TimeSlider.REFERENCE, [secondTime, 0]);
        var p3 = ShapeFactory.createPoint(TimeSlider.REFERENCE, [secondTime, data*e]);
        var p4 = ShapeFactory.createPoint(TimeSlider.REFERENCE, [firstTime, data*e]);
        
        var shape = ShapeFactory.createPolygon(TimeSlider.REFERENCE, [p1, p2, p3, p4]);
        var properties = { sensorId: i, dia: mes, incidentes: data};
        elements.push( new Feature(shape, properties, i));
        firstTime = secondTime;
        secondTime += avance;
        data = 0;
        mes++;
        if(mes>12) {
            mes=1;
        }
      }
      return new FeatureModel(
          new MemoryStore( {data:elements}),
          {reference: TimeSlider.REFERENCE}
      );
    }
    
    function painterDays() {
        var Painter = new FeaturePainter();
        Painter.paintBody = function (geocanvas, feature, shape, layer, map, paintState) {
        
            if (!paintState.selected) {
          
                geocanvas.drawShape(shape, {
                    stroke: {color: "rgb(13, 20, 28)", width: 3},
                    fill: {color: "rgba(255, 255, 204, 0.5)"}
                });
            } else {
                geocanvas.drawShape(shape, {
                    stroke: {color: "rgb(13, 20, 28)", width: 3},
                    fill: {color: "rgba(210, 160, 120, 0.8)"}
                });
            }
        };
        Painter.paintLabel = function (labelcanvas, feature, shape, layer, map, paintState) {
        if (paintState.selected ) {
            var labelTextS = "<span style='color: rgb(255, 255, 255)' class='label'>" + 
                  feature.properties.incidentes + '</span>';
            labelcanvas.drawLabel( labelTextS, shape, {positions: (PointLabelPosition.NORTH)} );
        } else {
            var mes = getMes(feature.properties.dia);
            //if(mes < 10)
              //  mes = "0"+mes;
            var labelText = "<span style='color: rgb(255, 255, 255)' class='label'>" + 
                   mes+ '</span>';
            labelcanvas.drawLabel( labelText, shape, {positions: (PointLabelPosition.NORTH)} );
        }
      };
      return Painter;
    }
    
    function getMes(id) {
        switch(id) {
            case 1: return "Enero";
            case 2: return "Febrero";
            case 3: return "Marzo";
            case 4: return "Abril";
            case 5: return "Mayo";
            case 6: return "Junio";
            case 7: return "Julio";
            case 8: return "Agosto";
            case 9: return "Septiembre";
            case 10: return "Octubre";
            case 11: return "Noviembre";
            case 12: return "Diciembre";
        }
    }
    function painterLayer() {
        var Painter = new FeaturePainter();
        Painter.paintBody = function (geocanvas, feature, shape, layer, map, paintState) {
        
            if (!paintState.selected) {
          
                geocanvas.drawShape(shape, {
                    stroke: {color: "rgb(13, 20, 28)", width: 3},
                    fill: {color: "rgba(255, 174, 102, 0.5)"}
                });
            } else {
                geocanvas.drawShape(shape, {
                    stroke: {color: "rgb(13, 20, 28)", width: 3},
                    fill: {color: "rgba(210, 160, 120, 0.8)"}
                });
            }
        };
        Painter.paintLabel = function (labelcanvas, feature, shape, layer, map, paintState) {
            if (paintState.selected ) {
                var labelTextS = "<span style='color: rgb(255, 255, 255)' class='label'>" + 
                  feature.properties.incidentes + '</span>';
                labelcanvas.drawLabel( labelTextS, shape, {positions: (PointLabelPosition.NORTH),} );
            } 
        };
        return Painter;
    }
    
    function createNewFeatures ( features, selected, mapBounds) {
        var feature, f = [], j=0;
        var crimen = document.getElementById( "empresasFiltro" ).selectedOptions[0].value;
        var zona = document.getElementById( "estadosFiltro" ).selectedOptions[0].value;
        for(var i = 0; i<features.length; i++) {
            feature = features[i];
            var c = feature.properties.TIPODELITO;
            var z = feature.properties.ZONA;
            if(!selected) {
                if((c === crimen || crimen === "Todos") && (z === zona || zona === "Todos")) {
                    f[j] = feature;
                    j++;
                }
            } else {
                var featureBounds = selected.geometry.bounds;
                if(featureBounds.contains2D(feature.shape)) {
                    if((c === crimen || crimen === "Todos") && (z === zona || zona === "Todos")) {
                        f[j] = feature;
                        j++;
                    }
                }
            }
        }
        return f;
    }
    
    return {
        createNewFeatures: createNewFeatures,
        createLayer: function(features, id, n, s, e) {
            var model = createTimeLineModel( features, n , s, e);
            var painter;
            if(n === 36)
                painter = painterDays();
            else
                painter = painterLayer();
            return new FeatureLayer(
                model, {
                    id: id,
                    layerType: LayerType.STATIC,
                    selectable: true,
                    painter: painter
                });
        }
    };
});


