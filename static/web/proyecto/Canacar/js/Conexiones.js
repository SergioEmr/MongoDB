/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define(["./Tablas", "recursos/js/Util"], function (Tablas, Util) {
    var folios = [["CURP","Folio", "Estado"]], nF = 1, numerof=0;
    var divs = ["nombre","paterno","materno","sexo","edad",
        "nombre2","paterno2","materno2","sexo2","edad2",
        "marca","modelo","serie","color","tipo","valorVehiculo",
        "fecha1","hora1","fecha2","hora2",
        "peso","valorCarga","cantidadEfectivo"];
    /*
     * Reinicia los Campos para realizar un registro de ruta nuevo
     * 
     * @param {type} baseUrl
     * @returns {undefined}
     */
    function initCuestionario(baseUrl) {
        divs.forEach(function(div) {
            document.getElementById(div).value = "";
        });
        /*
        var datatext = request("/users/curp_status", {handleAs: "json"});
        datatext.then(function(data) {
            console.log(data);
        });
        */
        $.ajax({
            type:'get',
            dataType: "json",
            url: baseUrl+"formulario/colaborador",// "users/curp_status",
            success: function (users) {
                console.log("Conectado con "+baseUrl+"formulario/colaborador");
                //var users = response.users;
                var nUsers = users.length;
                var curps = [], status, j=0;
                for(var key in users) {
                    status = users[key].folio;
                    if(status === "espera") {
                        curps[j] = key;
                        j++;
                    }
                }
                if(curps.length<1) {
                    setOptions("infoConductor", curps, "No CURP disponibles");
                    setOptions("infoCopiloto", curps, "No CURP Disponibles");
                }
                else {
                    setOptions("infoConductor", curps, "Seleccionar Curp");
                    setOptions("infoCopiloto", curps, "Seleccionar Curp");

                }
            },
            error: function (error) {
                console.log(error);
                document.getElementById("infoConductor").innetHTML = '<option value="0">seleccionar Curp</option>'+
                                '<option value="2">COUM940908HDFRS00</option>'+
                                '<option value="3">JPMT940908HDFRS00</option>';
            }
        });
        
    }
    /*
    * 
    * @param {type} div
    * @param {type} opciones
    * @returns {undefined}
    */
    function setOptions(div, opciones, firstOption) {
        if(!firstOption)
            firstOption = "Todos";
        document.getElementById(div).innerHTML = "";
        var base = "<option value='$VALUE'>$LABEL</option>", etiqueta = base.replace("$VALUE", 0).replace("$LABEL", firstOption);
        var  n = opciones.length;
        for(var i =0; i<n; i++) {
            etiqueta += base.replace("$VALUE", i+1).replace("$LABEL", opciones[i]);
        }
        document.getElementById(div).innerHTML = etiqueta;
    }
    /*
     * 
     * @param {type} envio
     * @param {type} folio
     * @returns {undefined}
     */
    function registrarViajeServer(baseUrl, envio, folio) {
        var http = new XMLHttpRequest();
        var res = '{\n\
  "msg": "Registrado"\n\
}\n\
';
        var url = baseUrl+"report/registro";
            http.open("POST", url, true);
            http.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            http.onreadystatechange = function() {
                console.log("Status "+http.status, " Response "+ http.responseText);
                if(http.readyState === 4 && (http.status === 200 || http.status === 202)) { 
                    //if(http.response === res){
                    //if(http.responseText !== "CURP ya asignado" && http.responseText !== "Folio ya creado") {
                        alert('Los datos fueron Registrados\nFolio:  '+folio); 
                        getFoliosStatus(baseUrl);
                        actualizarBotonesFolios(baseUrl, nF);
                        initCuestionario(baseUrl);
                        actualizarPlacas(baseUrl);
                    //}
                }
            };
            http.send(envio);
    }
    
    function getTokenStatus(baseUrl) {
        $.ajax({
            type:'get',
            dataType: "json",
            url: baseUrl+"users/token_status",
            success: function (response) {
                var users = response.users;
                var nUsers = users.length;
                var datos = [["Usuario (Email)", "Alerta", "Confirmacion","Cuestionario"]];
                for(var i=0; i<nUsers; i++) {
                    var al = users[i].alertapp;
                    var co = users[i].confirmapp;
                    var cu = users[i].questionaryapp;
                    var em = users[i].user;
                    datos [i+1] = [em, al, co, cu];
                }
                Tablas.crearTablaToken("tablaTokenStatus", datos);
            }
        }).fail(function(e) {
            console.log("Error al conectarce a users/token_status");
            console.log(e);
        });
    }
    function actualizarToken(baseUrl) {
        $.ajax({
            type:'get',
            dataType: "json",
            url: baseUrl+"users/token_status",
            //url: "https://178.128.76.236/users/token_status",
            success: function (response) {
                var users = response.users;
                var nUsers = users.length;
                var datos = [];
                for(var i=0; i<nUsers; i++) {
                    var al = users[i].alertapp;
                    var co = users[i].confirmapp;
                    var cu = users[i].questionaryapp;
                    var em = users[i].user;
                    datos [i] = [em, al, co, cu];
                }
                var json, actualizar = false;
                for(i=0; i<nUsers; i++) {
                    var alNode = document.getElementById("al"+i);
                    var al = alNode.selectedOptions[0].value;
                    var coNode = document.getElementById("co"+i);
                    var co = coNode.selectedOptions[0].value;
                    var cuNode = document.getElementById("cu"+i);
                    var cu = cuNode.selectedOptions[0].value;
                    if(datos[i][1] !== al)
                        actualizar = true;
                    var cu2;
                    if(datos[i][2] !== "denegar")
                        cu2 = "aprobar";
                    else
                        cu2 = "denegar";
                    if(cu2 !== co && actualizar ===false)
                        actualizar = true;
                    if(datos[i][3] !== cu && actualizar === false)
                        actualizar = true;
                    if(actualizar === true) {
                        json = {
                            email: datos[i][0],
                            alertapp: al,
                            questionaryapp: cu,
                            confirmapp: co
                        };
                        actualizarTokenServer(baseUrl, json);
                        actualizar = false;
                    }
                }
            }
        }).fail(function(e) {
            console.log(e);
        });
    }
    function actualizarTokenServer(baseUrl, datos) {
        var http = new XMLHttpRequest();
        //var url = "https://178.128.76.236/users/register_token";
        var url = baseUrl+"users/register_token";
            http.open("POST", url, true);
            http.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

            http.onreadystatechange = function() {
                console.log("Status "+http.status, " Response "+ http.responseText);
                if(http.readyState === 4 && (http.status === 200 || http.status === 202)) { 
                   //aqui obtienes la respuesta de tu peticion
                   alert('Los datos fueron enviados'); 
                   alert(http.responseText);
                   getTokenStatus(baseUrl);
                   initCuestionario(baseUrl);
                }
            };
            http.send(JSON.stringify(datos));
    }
    /*
     * 
     */
    function getFoliosStatus(baseUrl) {
        $.ajax({
            type:'get',
            dataType: "json",
            url: baseUrl+"report/pendiente",
            success: function (response) {
                var curps = response.curps;
                var fol = response.pendientes;
                var estatus = response.status;
                var nFoliosP = curps.length === fol.length? curps.length: curps.length > fol.length? curps.length: fol.length;
                var f = [];
                nF=1;
                folios = [["CURP","Folio", "Estado"]];
                for(var i=0; i<nFoliosP; i++) {
                    var curp = curps[i]? curps[i]: "---";
                    var fo = fol[i]? fol[i]: "---";
                    var es = estatus[i]? estatus[i]: "espera";
                    var sv = parseInt(fo.charAt(fo.length-2)+fo.charAt(fo.length-1));
                    numerof = sv+1;
                    folios[nF] = [curp, fo, es];
                    f[i] = fo;
                    nF++;
                }
                Tablas.crearTablaFolios("tablaToken", folios);
                actualizarBotonesFolios(baseUrl, nF);
                initCuestionario(baseUrl);
                if(nFoliosP>0) {
                    setOptions("sFolio", f, "Folios");
                    setOptions("roboFolio", f, "Folios");
                } else{
                    setOptions("sFolio", [], "No Folios Activos");
                    setOptions("roboFolio", [], "No Folios Activos");
                }
                
            }
        }).fail(function(e) {
            console.log("Error al conectarce a users/token_status");
            console.log(e);
        });
    }
    /*
     * @param {string} baseUrl
     * @param {int} nf
     * @returns {undefined}* 
     */
    function actualizarBotonesFolios(baseUrl, nf) {
        for(var i=1; i<nf; i++) {
            $("#bFolio"+i).click(function(element) {
                var ids = element.target.id, idl = ids.length;
                var id = parseInt(ids.charAt(idl-1));
                var fo = folios[id][1];
                console.log("Borrar folio "+id);
                var borrar = confirm("Seguro que desea concluir con este registro? Folio: "+ fo +" \nSera borrado de la base de datos");
                if(borrar === true) {
                    eliminarRegistroServer(baseUrl, fo, id, nf);
                }
            });
        }
    }
    
    
     function eliminarRegistroServer(baseUrl, folio, id, nf) {
            var http = new XMLHttpRequest();
            var url = baseUrl+"report/remove";
            //var url = "https://178.128.76.236/report/remove";
            http.open("POST", url, true);
            http.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            http.onreadystatechange = function() {
                console.log("Status "+http.status, " Response "+ http.responseText);
                if(http.readyState === 4 && (http.status === 200 || http.status === 202)) { 
                    //aqui obtienes la respuesta de tu peticion
                    alert("El folio "+folio+" a sido Eliminado");
                    getFoliosStatus();
                    initCuestionario(divs);
                    //borrarFolioTabla(id, nf);
                }
            };
            var envio = {folio: folio};
            http.send(JSON.stringify(envio));
        }
        
    /*
     * @param {string} url
     * @param {json} datos
     * @param (Array) divs
     * @returns {undefined}* 
     */
    function registrarDatos (url, datos, divs) {
        var http = new XMLHttpRequest();
        http.open("POST", url, true);
        http.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        http.onreadystatechange = function() {
            console.log("Status "+http.status, " Response "+ http.responseText);
            if(http.readyState === 4 && (http.status === 200 || http.status === 202)) { 
                alert("Se completo el registro correctamente\n"+http.responseText);
                
            }
        };
        http.send(JSON.stringify(datos));
    }
    /*
     * @param {string} url
     * @param {json} datos
     * @param (Array) divs
     * @returns {undefined}* 
     */
    function enviarForm (url, datos, divs) {
        var http = new XMLHttpRequest();
        http.open("POST", url, true);
        http.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        http.onreadystatechange = function() {
            console.log("Status "+http.status, " Response "+ http.responseText);
            if(http.readyState === 4 && (http.status === 200 || http.status === 202)) { 
                alert("Se completo el registro correctamente\n"+http.responseText);
                divs.forEach(function(div) {
                    document.getElementById(div).value = "";
                });
            }
        };
        http.send(datos);
    }
    /*
     * 
     * @param {String} baseUrl
     * @returns {undefined}
     */
    function actualizarPlacas (baseUrl) {
        $.ajax({
            type:'get',
            dataType: "json",
            url: baseUrl+"formulario/placas",
             success: function (Placas) {
                console.log("Conectado con "+baseUrl+"formulario/placas ");
                var placas =[], i=0;
                for(var key in Placas) {
                    placas [i]= key;
                    i++;
                }
                 if(placas.length<1) {
                    setOptions("placas", placas, "No Placas disponibles");
                }
                else {
                    setOptions("placas", placas, "Seleccionar Placa");
                }
            },
            error: function (error) {
                console.log(error);
             }
        });
    }
    /*
     * 
     * @param {String} baseUrl
     * @returns {undefined}
     */
    function obtenerDatosColaboradores (baseUrl, seleccionado, divs) {
            $.ajax({
                type:'get',
                dataType: "json",
                url: baseUrl+"formulario/colaborador",
                success: function (colaboradores) {
                    console.log("Conectado con "+baseUrl+"formulario/placas ");
                    var colaborador;
                    if((!seleccionado || seleccionado ===null) && !divs) {
                        Tablas.crearTablaColaboradores("contenidoTablaEmpleados", colaboradores);
                        return null;
                    }
                    for(var key in colaboradores) {
                        if(key === seleccionado) {
                            colaborador = colaboradores[key];
                            break;
                        }
                    }
                    if(!colaborador) {
                        alert("No se encontro la placa "+seleccionado);
                        return null;
                    }
                    var edad = Util.calcularEdad(colaborador.nacimiento);
                    document.getElementById(divs[0]).value = colaborador.nombres;
                    document.getElementById(divs[1]).value = colaborador.paterno;
                    document.getElementById(divs[2]).value = colaborador.materno;
                    document.getElementById(divs[3]).value = edad;
                    document.getElementById(divs[4]).value = colaborador.sexo;
                    Tablas.actualizarTablaColaboradores(colaboradores);
                },
                error: function (error) {
                    console.log(error);
                }
            });
    }   
        
    return {
        initCuestionario: initCuestionario,
        getFoliosStatus: getFoliosStatus,
        actualizarToken: actualizarToken,
        getTokenStatus: getTokenStatus,
        registrarViaje: function (baseUrl) {
            var curpNode = document.getElementById("infoConductor");
            var isCurp = curpNode.selectedOptions[0].value;
            if(isCurp === 0 || isCurp === "0") {
                alert("No se selecciono ningun CURP para el conductor");
            } else {
                var date = new Date();
                var y = date.getFullYear();
                var mo = (date.getMonth()+1)<10? "0"+(date.getMonth()+1): (date.getMonth()+1);
                var d = date.getDate()<10? "0"+date.getDate(): date.getDate();
                var n = numerof<10? "00"+numerof: "0"+ numerof;
                var h = date.getHours() <10 ? "0"+date.getHours(): date.getHours();
                var m = date.getMinutes() <10 ? "0"+date.getMinutes(): date.getMinutes();
                var s = date.getSeconds() <10 ? "0"+date.getSeconds(): date.getSeconds();
                var fecha = d+"/"+mo+"/"+y, hora = h+":"+m+":"+s;
                var folio = d+mo+y+n;
                var curp = curpNode.selectedOptions[0].innerHTML;
                var curp2 = document.getElementById("infoCopiloto").selectedOptions[0].innetHTML;
                curp2 = (curp2==="Seleccionar Curp" || curp2==="No CURP disponibles")? "":curp2;
                var placaNode = document.getElementById("placas");
                var placa = placaNode.selectedOptions[0].innerHTML;
                var rutaNode = document.getElementById("sruta");
                var ruta = rutaNode.selectedOptions[0].innerHTML;
                var cargaNode = document.getElementById("tipoCarga");
                var carga = cargaNode.selectedOptions[0].innerHTML;
                $('#panelRegistro').fadeOut('slow');
                $("#pTablaToken").fadeIn("slow");
                var data = "&folio="+ folio+
                    "&curp_1="+ curp+
                    "&curp_2="+ curp2+
                    "&placas="+ placa+
                    "&ruta="+ ruta+
                    "&fsalida="+document.getElementById("fecha1").value+
                    "&fllegada="+ document.getElementById("fecha2").value+
                    "&hsalida="+ document.getElementById("hora1").value+
                    "&hllegada="+ document.getElementById("hora2").value+
                    "&carga="+ carga+
                    "&peso="+ document.getElementById("peso").value+
                    "&valor="+ document.getElementById("valorCarga").value+
                    "&efectivo="+ document.getElementById("cantidadEfectivo").value+
                    "&conductor="+document.getElementById("nombre").value+
                    "&copiloto="+document.getElementById("nombre2").value+
                    "&modelo="+document.getElementById("modelo").value+
                    "&tvehiculo="+document.getElementById("tipo").value;
                //initCuestionario(divs);
                console.log(data);
                registrarViajeServer(baseUrl, data, folio);
            }
        },
        eliminarRegistro: eliminarRegistroServer,
        verAlertasReales: function (baseUrl, map, layer, reference, Shapes, user) {
            $.ajax({
                type:'Get',
                dataType: "json",
                url: baseUrl+ "alerts/consult="+user//"report/consult" //report/verify  report/gps /alerts/consult
            }).done(function(data) {
                var features = data.features;
                var nf = features.length;
                for(var i =0; i<nf; i++) {
                    var feature = features[i];
                    var properties = feature.properties;
                    var id = properties.id || properties.folio;
                    var coordinates = feature.geometry.coordinates;
                    var lon = coordinates[0], lat = coordinates[1];
                    if(lon>0 || lat <0 || lon > lat) {
                        console.log("Error en coordenadas de alerta: lng: "+lon+" lat: "+lat);
                        if(lon > lat ) {
                            if(lon >0 && lat>0) {
                                lon = lon>0? lon*(-1): lon;
                                lat = lat<0? lat*(-1): lat;
                                coordinates = [lon, lat];
                            } else 
                                coordinates = [lat, lon];
                        } else {
                           
                            lon = lon>0? lon*(-1): lon;
                            lat = lat<0? lat*(-1): lat;
                            coordinates = [lon, lat];
                        }
                        console.log("Coordenadas corregidas lng: "+coordinates[0]+" lat: "+coordinates[1]);
                    }
                    var fecha = properties.fecha, hora = properties.hora;
                    var estado = properties.alerta || properties.status;
                    //var date = formatDate(fecha, hora);
                    //var numDate = new Date(date), nowDate = new Date();
                    //var dif = nowDate - numDate;
                    var anterior = layer.model.get(id);
                    var point = Shapes.createPoint(reference, coordinates[0], coordinates[1],0, id, properties);
                            layer.model.put(point);
                    switch(estado.toLowerCase()) {
                        case "conectado":
                        case "espera":
                            if(folios[nF+nf-1]) {
                                folios[nF + nf-1][2] = "Esperando";
                                Tablas.crearTablaFolios("tablaToken", folios);
                                actualizarBotonesFolios(baseUrl, nF);
                            }
                            break;
                        case "desconectado":
                            if(folios[nF + nf-1]) {
                                folios[nF + nf-1][2] = "Esperando";
                                Tablas.crearTablaFolios("tablaToken", folios);
                                actualizarBotonesFolios(baseUrl, nF);
                            }
                            
                            break;
                        case "alerta":
                            if(folios[nF + nf-1]) {
                                folios[nF + nf-1][2] = "Registrado";
                                Tablas.crearTablaFolios("tablaToken", folios);
                                actualizarBotonesFolios(baseUrl, nF);
                            }
                            if(anterior) {
                                if( anterior.properties.status !== "alerta") {
                                    Util.fitBounds(map, point.geometry.bounds, true);
                                    alert("El vehiculo ha sido comprometido, la informacion ha sido guardada");
                                    console.log("El vehiculo ha sido comprometido, la informacion ha sido guardada");
                            }
                            } else {
                                Util.fitBounds(map, point.geometry.bounds, true);
                                alert("El vehiculo ha sido comprometido, la informacion ha sido guardada");
                                console.log("El vehiculo ha sido comprometido, la informacion ha sido guardada");
                            }

                            break;
                    }
                }
                //actualizarTabla(features);
            }).fail(function(e) {
                console.log("No se contacto con el server");
                console.log(e);
            });
        },
        enviarAlerta: function (baseUrl, paquete) {
            var http = new XMLHttpRequest();
            var url = baseUrl+"alerts/alerta";// "alerts/alert2";
            http.open("POST", url, true);
            http.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            http.onreadystatechange = function() {
                console.log("Status "+http.status, " Response "+ http.responseText);
                if(http.readyState === 4 && (http.status === 200 || http.status === 202)) { 
                    //aqui obtienes la respuesta de tu peticion
                    alert("Alerta creada con exito");
                }
                if(http.readyState === 4 && (http.status === 0 || http.status === 404)) { 
                    alert("No se conecto con el servidor");
                }
            };
            http.send(JSON.stringify(paquete));
        },
        registrarEmpleado: function(baseUrl) {
            var curp = document.getElementById("regCurp").value;
            if(!curp || curp==="") {
                alert("No se a escrito informacion");
                return 0;
            }
            var datos = "&curp="+ curp+
                    "&nombres="+ document.getElementById("regNombres").value+
                    "&paterno="+ document.getElementById("regPaterno").value+
                    "&materno="+document.getElementById("regMaterno").value+
                    "&nacimiento="+document.getElementById("regNacimiento").value+
                    "&sexo="+document.getElementById("regSexo").value+
                    "&nickname="+ (document.getElementById("regApodo").value? document.getElementById("regApodo").value: "");
            console.log(datos);
            var divs = ["regCurp","regSexo","regNombres","regEntidad","regPaterno","regMunicipio","regMaterno","regRFC","regNacimiento","regApodo"];
            enviarForm(baseUrl+"formulario/colaborador", datos, divs);
            obtenerDatosColaboradores(baseUrl);
            /*
            var curp = curpNode.selectedOptions[0].value;
             else {
                var divs = ["regCurp","regSexo","regNombre","regEntidad","regPaterno","regMunicipio","regMaterno","regRFC","regNacimiento","regApodo"];
                var datos = {
                    curp: curpNode.selectedOptions[0].innerHTML,
                    nombres: document.getElementById("regNombres").value,
                    paterno: document.getElementById("regPaterno").value,
                    materno: document.getElementById("regMaterno").value,
                    nacimiento: document.getElementById("regNacimiento").value,
                    sexo: document.getElementById("regSexo").value,
                    nickname: document.getElementById("regApodo").value? document.getElementById("regApodo").value: ""
                };
                console.log(datos);
                //registrarDatos(baseUrl+"formulario/colaborador", datos, divs);
                
            }*/
        },
        buscarCurp: function () {
            
        },
        registrarVehiculo: function(baseUrl) {
            var placa = document.getElementById("regPlaca").value;
            if(!placa || placa ==="") {
                alert("No se a escrito informacion");
                return 0;
            } else {
                var datos = "&placas="+ placa+
                    "&marca=" +document.getElementById("regMarca").value+
                    "&modelo="+ document.getElementById("regModelo").value+
                    "&serie="+ document.getElementById("regSerie").value+
                    "&color="+ document.getElementById("regColor").value+
                    "&tipo="+ document.getElementById("regTipo").value+
                    "&valor="+ document.getElementById("regValor").value+
                    "&gps="+ document.getElementById("regGPS").value;
                console.log(datos);
                var divs = ["regPlaca","regModelo", "regMarca","regSerie","regColor","regTipo","regValor","regGPS"];
                enviarForm(baseUrl+"formulario/placas", datos, divs);
                actualizarPlacas(baseUrl);
            }
        },
        registrarRobo: function (baseUrl) {
            var folioNode = document.getElementById("roboFolio");
            var folio = folioNode.selectedOptions[0].value;
            if(folio === "0"|| folio===0) {
                alert("No se selecciono ningun folio");
                return null;
            }            
            var datos = "&folio="+ folioNode.selectedOptions[0].innerHTML+
                    "&tramo="+ document.getElementById("roboTramo").value+
                    "&tipo="+ document.getElementById("roboTipo").value+
                    "&denunciante="+document.getElementById("roboDenunciante").value+
                    "&organizacion="+document.getElementById("roboOrganizacion").value+
                    "&vehiculos="+document.getElementById("roboVehiculo").value+
                    "&fecha="+ document.getElementById("roboFecha").value+
                    "&hora="+ document.getElementById("roboHora").value+
                    "&lat="+ document.getElementById("roboCoordenadas").value+
                    "&lng="+ document.getElementById("roboCoordenadas").value+
                    "&carpeta="+ document.getElementById("roboCarpeta").value+
                    "&descripcion="+ document.getElementById("roboDescripcion").value;
            console.log(datos);
            var divs = ["roboCarga","roboTramo","roboPlaca","roboPeso","roboRazon","roboConductor","roboValor","roboTipo",
                "roboCopiloto","roboEfectivo","roboDenunciante","roboSalida","roboRuta","roboOrganizacion","roboLlegada",
                "roboAlerta","roboVehiculo","roboFecha","roboHora","roboCoordenadas","roboCarpeta"];
            enviarForm(baseUrl+"report/delito", datos, divs);
        },
        actualizarPlacas: actualizarPlacas,
        obtenerDatosPlacas: function (baseUrl, placaSeleccionada) {
            $.ajax({
                type:'get',
                dataType: "json",
                url: baseUrl+"formulario/placas",
                success: function (Placas) {
                    console.log("Conectado con "+baseUrl+"formulario/placas ");
                    var placa;
                    for(var key in Placas) {
                        if(key === placaSeleccionada) {
                            placa = Placas[key];
                            break;
                        }
                    }
                    if(!placa) {
                        alert("No se encontro la placa "+placaSeleccionada);
                        return null;
                    }
                    document.getElementById("tipo").value = placa.tipo;
                    document.getElementById("modelo").value = placa.modelo;
                    document.getElementById("serie").value = placa.serie;
                    document.getElementById("color").value = placa.color;
                    document.getElementById("valorVehiculo").value = placa.valor;
                    document.getElementById("marca").value = placa.marca;
                    document.getElementById("gps").value = placa.gps;
                },
                error: function (error) {
                    console.log(error);
                }
            });
        },
        obtenerDatosColaboradores: obtenerDatosColaboradores,
        obtenerDatosFolio: function (baseUrl, folioSeleccionado) {
            $.ajax({
                type:'get',
                dataType: "json",
                url: baseUrl+"report/consult",
                success: function (folios) {
                    console.log("Conectado con "+baseUrl+"formulario/placas ");
                    var folio;
                    for(var key in folios) {
                        if(key === folioSeleccionado) {
                            folio = folios[key];
                            break;
                        }
                    }
                    if(!folio) {
                        alert("No se encontro la placa "+folioSeleccionado);
                        return null;
                    }
                    document.getElementById("roboCarga").value = folio.carga || "";
                    document.getElementById("roboTramo").value = folio.tramo || "";
                    document.getElementById("roboPlaca").value = folio.placa || "";
                    document.getElementById("roboPeso").value = folio.peso || "";
                    document.getElementById("roboConductor").value = folio.conductor || "";
                    document.getElementById("roboValor").value = folio.valor || "";
                    document.getElementById("roboTipo").value = folio.tipo || "";
                    document.getElementById("roboCopiloto").value = folio.copiloto || "";
                    document.getElementById("roboEfectivo").value = folio.efectivo || "";
                    document.getElementById("roboDenunciante").value = folio.denunciante || "";
                    document.getElementById("roboSalida").value = folio.salida || "";
                    document.getElementById("roboRuta").value = folio.ruta || "";
                    document.getElementById("roboOrganizacion").value = folio.organizacion || "";
                    document.getElementById("roboLlegada").value = folio.llegada || "";
                    document.getElementById("roboAlerta").value = folio.alerta || "";
                    document.getElementById("roboVehiculo").value = folio.vehiculo || "";
                    document.getElementById("roboFecha").value = folio.fecha || "";
                    document.getElementById("roboHora").value = folio.hora || "";
                    document.getElementById("roboCoordenadas").value = folio.coordenadas || "";
                    document.getElementById("roboCarpeta").value = folio.carpeta || "";
                    document.getElementById("roboFuero").value = folio.fuero || "";
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    };
});
