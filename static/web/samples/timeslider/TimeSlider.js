define([
  "luciad/view/feature/ShapeProvider",
  "luciad/reference/ReferenceProvider",
  "luciad/shape/ShapeFactory",
  "luciad/uom/UnitOfMeasureRegistry",
  "luciad/view/Map",
  "luciad/view/controller/Controller",
  "luciad/view/controller/HandleEventResult",
  "luciad/view/input/GestureEventType"
], function(ShapeProvider, ReferenceProvider, ShapeFactory, UnitOfMeasureRegistry, Map,
            Controller, HandleEventResult, GestureEventType) {

  var REFERENCE = ReferenceProvider.createCartesianReference({
    xUnitOfMeasure: UnitOfMeasureRegistry.getUnitOfMeasure("Second"),
    yUnitOfMeasure: UnitOfMeasureRegistry.getUnitOfMeasure("Number")
  });

  function pad(str, max) {
    str = str.toString();
    return str.length < max ? pad("0" + str, max) : str;
  }

  /**
   * This component shows a timeline with labels.
   * <p>
   *   It is a LuciadRIA non-georeferenced Map where X is time and Y is an unspecified number.
   * </p>
   * <p>
   *   It adds navigation constraints so that you can only pan horizontally,
   *   and not navigate beyond the currently set time range.
   * </p>
   * <p>
   *   The replay speed is dependent on the zoom level of the timeline.
   * </p>
   * <p>
   * Notes:
   * <ul>
   *   <li>
   *     It is a LuciadRIA {@link luciad.view.Map map}, so you can add layers to it.
   *     The geometry (either in the model, or submitted through a {@link luciad.view.feature.ShapeProvider ShapeProvider}, or submitted directly on the {@link luciad.view.GeoCanvas GeoCanvas})
   *     should be in the {@link TimeSlider#REFERENCE time reference}.
   *   </li>
   *   <li>Use {@link #setCurrentTime} to programmatically move the timeline.</li>
   *   <li>Use {@link #getCurrentTime} and {@link Evented#on map change events} to listen to timeline changes.</li>
   *   <li>Use {@link #setValidRange} to set the valid time and Y range.  Navigation will be constrained to that range.</li>
   *   <li>Use {@link #mapBounds} to get the time and Y extent currently visible on the view.</li>
   * </ul>
   * </p>
   */
  function TimeSlider(domNode) {
    var node = document.getElementById(domNode);
    var height = node.clientHeight;
    var self = this;
    Map.call(this, domNode, {
      reference: REFERENCE,
      border: {
        bottom: 20
      },
      axes: {
        xAxis: {
          axisLineStyle: {
            color: "rgba(176, 179, 50, 1.0)",
            width: 0
          },
          gridLine: true,
          labelFormatter: function(timestampInSeconds) {
            var date = new Date(timestampInSeconds * 1000);
            var visibleTimeRange = self.mapBounds.width;
            if (visibleTimeRange > 5 * 356 * 24 * 60 * 60) {
              return date.getFullYear();
            }
            if (visibleTimeRange > 0.5 * 356 * 24 * 60 * 60) {
              return pad(date.getMonth() + 1, 2) + "/" + date.getFullYear();
            }
            if (visibleTimeRange > 5 * 31 * 24 * 60 * 60) {
              return pad(date.getDate(), 2) + "/" + pad(date.getMonth() + 1, 2) + "/" + date.getFullYear();
            }
            if (visibleTimeRange > 7 * 24 * 60 * 60) {
              return pad(date.getDate(), 2) + "/" + pad(date.getMonth() + 1, 2);
            }
            return pad(date.getHours(), 2) + ":" + pad(date.getMinutes(), 2);
          },
          labelRotation: 0,
          labelStyle: {
            alignmentBaseline: "middle",
            textAnchor: "center",
            offsetY: 10,
            angle: 0,
            fill: "rgba(255, 255, 255, 1.0)",
            font: "15px Tahoma, Arial, sans-serif",
            haloWidth: 0,
            strokeWidth: 0
          },
          spacing: {
            minimumTickSpacing: 60,
            mapSpacing: [1,
                         60,
                         5 * 60,
                         10 * 60,
                         30 * 60,
                         60 * 60,
                         4 * 60 * 60,
                         8 * 60 * 60,
                         24 * 60 * 60,
                         3 * 24 * 60 * 60,
                         7 * 24 * 60 * 60,
                         14 * 24 * 60 * 60,
                         1 * 31 * 24 * 60 * 60,
                         3 * 31 * 24 * 60 * 60,
                         6 * 31 * 24 * 60 * 60,
                         356 * 24 * 60 * 60,
                         5 * 356 * 24 * 60 * 60,
                         10 * 356 * 24 * 60 * 60
            ] //space on seconds, 5min, 10min, 30min..
          },
          subTickLength: height / 2,
          subTicks: 0,
          tickLineStyle: {
            color: "rgba(0, 0, 0, 0)",
            width: 0
          }
        }
      }
    });

    //custom controller that does not manipulate y-scale when zooming and also does not pan in the y-direction
    this.controller = new TimeSliderController(this);
  }

  TimeSlider.prototype = Object.create(Map.prototype);
  TimeSlider.prototype.constructor = TimeSlider;

  /**
   * The world reference of the timeline map: X-axis is time in seconds, Y-axis is a non-specified number.
   */
  TimeSlider.REFERENCE = REFERENCE;

  /**
   * Navigate the view so that the given absolute time (in seconds) is in the center of the timeline.
   */
  TimeSlider.prototype.setCurrentTime = function(timeInSeconds) {
    var worldPoint = ShapeFactory.createPoint(this.reference, [timeInSeconds, 0]);
    this.mapNavigator.pan({targetLocation: worldPoint});
  };

  /**
   * Get the time that is currently in the center of the timeline.
   * <p/>
   * Use {@link Evented#on map change events} to get notified of changes.
   */
  TimeSlider.prototype.getCurrentTime = function() {
    var centerViewPoint = ShapeFactory.createPoint(null, [this.viewSize[0] / 2, this.viewSize[1] / 2]);
    return this.viewToMapTransformation.transform(centerViewPoint).x;
  };

  /**
   * Set the valid time and Y range.
   * <p/>
   * Navigation will be constrained to these ranges.
   * The timeline will fit on the time range.
   */
  TimeSlider.prototype.setValidRange = function(startTime, endTime, yMin, yMax) {
    var timeRange = endTime - startTime;
    var restrictBounds = ShapeFactory.createBounds(REFERENCE, [startTime - timeRange, 3 * timeRange, yMin, yMax - yMin]);
    this.restrictNavigationToBounds(restrictBounds);
    var fitBounds = ShapeFactory.createBounds(REFERENCE, [startTime, timeRange, yMin, yMax - yMin]);
    this.mapNavigator.fit({bounds: fitBounds, animate: false});
  };

  /**
   * Animated fit on a time range.
   */
  TimeSlider.prototype.fitToTimeRange = function(startTime, endTime) {
    var bounds = ShapeFactory.createBounds(REFERENCE, [startTime, endTime - startTime, this.mapBounds.y, this.mapBounds.height]);
    return this.mapNavigator.fit({bounds: bounds, animate: true});
  };

  /**
   * Custom controller that:
   *  - only zooms in the x-direction
   *  - only pans in the x-direction
   *  - does not rotate
   * Useful for 1-dimensional Maps (such as TimeSliders)
   */
  function TimeSliderController() {
    Controller.call(this);
  }

  TimeSliderController.prototype = Object.create(Controller.prototype);
  TimeSliderController.prototype.constructor = TimeSliderController;

  TimeSliderController.prototype.onGestureEvent = function(event) {
    var type = event.type;
    var viewPoint = ShapeFactory.createPoint(null, [event.viewPosition[0], 0 /* ignore Y */]);

    if (type === GestureEventType.SCROLL) {

      // zoom on mousewheel: scrolling upwards => amount > 0 => zoom in
      var factor = (event.amount> 0) ? 2.0 : 0.5;
      this.map.mapNavigator.zoom({factor: {x: factor, y: 1}, location: viewPoint, animate: {duration: 250}});
      return HandleEventResult.EVENT_HANDLED;

    } else if (type === GestureEventType.DOUBLE_CLICK) {

      // zoom on double-click
      this.map.mapNavigator.zoom({factor: {x: 2, y: 1}, location: viewPoint, animate: {duration: 250}});
      return HandleEventResult.EVENT_HANDLED;

    } else if (type === GestureEventType.PINCH) {

      // zoom on pinch: pinch events come with a scaleFactor
      var scaleFactor = event.scaleFactor;
      if (scaleFactor > 0 && !isNaN(scaleFactor) && !(scaleFactor === Number.POSITIVE_INFINITY || scaleFactor === Number.NEGATIVE_INFINITY)) {
        this.map.mapNavigator.zoom({targetScale: {x: scaleFactor * this.map.mapScale[0], y: this.map.mapScale[1]}, location: viewPoint});
      }
      return HandleEventResult.EVENT_HANDLED;

    } else if (type === GestureEventType.DRAG) {

      if (this._previousViewPoint) {
        this.map.mapNavigator.pan({targetLocation: this._previousViewPoint, toViewLocation: viewPoint});
      }
      this._previousViewPoint = viewPoint;
      return HandleEventResult.EVENT_HANDLED;

    } else if (type === GestureEventType.DRAG_END) {

      this._previousViewPoint = null;
      
    }
    return HandleEventResult.EVENT_IGNORED;
  };

  return TimeSlider;

});