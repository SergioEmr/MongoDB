define([
  'samples/common/ModuleLoad' ,
  "require"
], function(ModuleLoad, require) {

  /**
   * dynamically load spot data painter. The return module depends on whether the military
   * symbology package is present.
   */
  return {
    load: function(id, amdRequire, callback) {
      ModuleLoad.loadConditional(require, callback, './SpotDataPainterMilSym',
          './SpotDataPainterFallback');
    }
  };

});