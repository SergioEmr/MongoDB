define([
  "../Gazetteer",
  "../ThemeManager",
  "luciad/model/feature/FeatureModel",
  "luciad/model/store/MemoryStore",
  "luciad/model/store/UrlStore",
  "luciad/shape/ShapeFactory",
  "luciad/view/feature/FeatureLayer",
  "luciad/view/feature/FeaturePainter",
  "luciad/view/LayerType",
  "samples/common/IconFactory",
  "samples/common/store/WebSocketNotifier"
], function(Gazetteer, ThemeManager, FeatureModel, MemoryStore, UrlStore, ShapeFactory, FeatureLayer, FeaturePainter,
            LayerType, IconFactory, WebSocketNotifier) {
  /**
   * @fileOverview
   * The air picture theme shows aviation related information such as airspaces, airport query, and tracks.
   */

  var airspacesLayer, tracksLayer;

  var theTheme = {
    name: "Air Picture",
    _initTracksLayer: function(context) {
      var tracksMemoryStore = new MemoryStore();

      this.tracksStore = new WebSocketNotifier({
        target: "ws://" + window.location.host + "/tracks/",
        delegateStore: tracksMemoryStore
      });
      var tracksModel = new FeatureModel(tracksMemoryStore, {});

      var tracksPainter = new FeaturePainter();
      var pixelDensities = [1 / 1000, 1 / 500];
      var levels = [];
      for (var i = 0, len = pixelDensities.length; i < len; i++) {
        levels.push(pixelDensities[i] / (100 * 96 / 2.54));
      }
      tracksPainter.getDetailLevelScales = function() {
        return levels;
      };
      var pin = {
        color: 'white',
        width: 1
      };
      var selpin = {
        color: '#1c72ff',
        width: 1
      };
      var style = {
        pin: pin,
        offset: 16
      };
      var selstyle = {
        pin: selpin,
        offset: 16
      };
      tracksPainter.paintLabel = function(labelCanvas, feature, shape, layer, map, state) {
        if (state.level < 2) {
          return;
        }

        var content;
        if (state.selected) {
          content = "<b> " + feature.properties.callsign + "</b><br/>  heading: " +
                    Math.round(feature.properties.heading) + "\u00B0";
          labelCanvas.drawLabel(
              "<div style=" +
              "'color:#1c72ff; background-color: #ffec1d; padding: 4px; border-radius: 3px;'>" +
              content + " </div>",
              shape,
              selstyle);
        } else {
          labelCanvas.drawLabel(
              "<div style='color: white'>" + feature.properties.callsign + ' </div>',
              shape,
              style);
        }
      };

      var trackStyle = {
        width: "12px",
        height: "12px",
        image: IconFactory.rectangle({
          fill: "rgba(0, 0, 255,0.9)",
          stroke: "rgba(255, 255, 255,1.0)",
          width: 12,
          height: 12,
          strokeWidth: 2
        })
      };
      var selectedTrackStyle = {
        width: "16px",
        height: "16px",
        image: IconFactory.rectangle({
          fill: "rgba(0, 0, 255,0.9)",
          stroke: "#1c72ff",
          width: 16,
          height: 16,
          strokeWidth: 2
        })
      };
      var trackImageStyle = {
        url: "../tracks/img/plane.png",
        width: "20px",
        height: "20px",
        draped: false
      };
      var selectedTrackImageStyle = {
        url: "../tracks/img/plane.png",
        width: "32px",
        height: "32px",
        draped: false
      };
      tracksPainter.paintBody = function(geoCanvas, feature, shape, layer, map, state) {
        var image = state.level >= 1;
        var style = state.selected ?
                    image ? selectedTrackImageStyle : selectedTrackStyle :
                    image ? trackImageStyle : trackStyle;
        if (image) {
          style.heading = feature.properties.heading;
        }
        geoCanvas.drawIcon(shape, style);
      };

      tracksLayer = new FeatureLayer(
          tracksModel,
          {
            label: "Tracks",
            layerType: LayerType.STATIC,
            painter: tracksPainter,
            incrementalRendering: false
          }
      );
      tracksLayer.selectable = true;
      context.map.layerTree.addChild(tracksLayer);
    },
    activate: function(context) {

      Gazetteer.enable(context.map);
      var airspacesStore = new UrlStore({
        target: context.resourcelocation + "/data/airspaces.json"
      });

      var airspaces = new FeatureModel(airspacesStore, {});

      var airspacePainter = new FeaturePainter();
      airspacePainter.paintBody = function(geoCanvas, feature, shape, layer, map, state) {
        if (state.selected) {
          geoCanvas.drawShape(shape, {
            stroke: {
              color: "rgba(255,0,0,1)",
              strokeWidth: 2
            },
            fill: {color: "rgba(0,0,255,0.5)"}
          });
        } else {
          geoCanvas.drawShape(shape, {
            stroke: {
              strokeWidth: 2,
              color: "rgba(0,0,255,1)"
            },
            fill: {color: "rgba(0,0,255,0.5)"}
          });

        }
      };
      airspacesLayer = new FeatureLayer(
          airspaces,
          {
            label: "Airspaces",
            layerType: LayerType.STATIC,
            painter: airspacePainter
          }
      );
      context.map.layerTree.addChild(airspacesLayer);

      this._initTracksLayer(context);
      if (!context.firstLoad) {
        context.map.mapNavigator.fit(
            {
              bounds: ShapeFactory.createBounds(airspaces.reference, [-74.5, 6.5, 40, 5]),
              animate: true
            });
      }
    },
    deactivate: function(context) {
      if (airspacesLayer) {
        context.map.layerTree.removeChild(airspacesLayer);
      }
      if (tracksLayer) {
        context.map.layerTree.removeChild(tracksLayer);
        this.tracksStore.destroy();
      }
      Gazetteer.disable();
    },
    toString: function() {
      return this.name;
    }
  };
  ThemeManager.register(theTheme);
  return theTheme;
});
