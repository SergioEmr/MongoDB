define([
  'luciad/symbology/military/MilitarySymbologyPainter',
  'luciad/symbology/SymbologyProvider!MIL_STD_2525c',
  './SpotDataPainterFallback'
], function(MilitarySymbologyPainter, MIL_STD_2525c, SpotDataPainterFallback) {

  function SpotDataPainterMilSym() {
    this._symbolPainter = new MilitarySymbologyPainter(MIL_STD_2525c, {
      codeFunction: function(feature) {
        return feature.properties.code;
      },
      symbologyServicePath: "http://" + window.location.hostname + ":8081/symbologyservice/"
    });
  }

  SpotDataPainterMilSym.prototype = Object.create(SpotDataPainterFallback.prototype);

  SpotDataPainterMilSym.prototype.constructor = SpotDataPainterMilSym;

  SpotDataPainterMilSym.prototype.paintBody = function(geoCanvas, feature, shape, layer, map, state) {
    if (state.level === 0) {
      this._paintSmallScale(geoCanvas, feature, shape, layer, map, state);
    } else {
      return this._symbolPainter.paintBody(geoCanvas, feature, shape, layer, map, state);
    }
  };

  return SpotDataPainterMilSym;

});