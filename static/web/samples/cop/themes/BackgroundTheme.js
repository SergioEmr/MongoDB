define([
  "samples/common/LayerConfigUtil"
], function(LayerConfigUtil) {

  function fallbackRaster() {
    return LayerConfigUtil.createBackgroundLayer();
  }

  function propagate(l) {
    return l;
  }

  var backgroundLayer;
  return {
    name: "BackgroundTheme",
    initialize: function(callback) {

      LayerConfigUtil
          .createBingLayer({proxyUri: '../../../bingproxy/'})
          .then(propagate, fallbackRaster)
          .then(function(layer) {
            backgroundLayer = layer;
            callback(backgroundLayer.model.reference);
          });

    },
    activate: function(context) {
      context.map.layerTree.addChild(backgroundLayer, "bottom");
    },
    deactivate: function(context) {
      if (backgroundLayer) {
        context.map.layerTree.removeChild(backgroundLayer);
      }
    },
    toString: function() {
      return this.name;
    }
  };

});
