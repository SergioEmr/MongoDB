define([
  'luciad/symbology/military/MilitarySymbologyPainter',
  'luciad/symbology/SymbologyProvider!APP_6B',
  './BlueForcesPainterFallback'
], function( MilitarySymbologyPainter, APP_6b, BlueForcesPainterFallback) {

  //#snippet symbologyServicePath
  function BlueForcesPainterMilSym() {
    this._symbolPainter = new MilitarySymbologyPainter(APP_6b, {
      codeFunction: function(feature) {
        return feature.properties.code;
      },
      symbologyServicePath: "http://" + window.location.hostname + ":8081/symbologyservice/"
    });
  }
  //#endsnippet symbologyServicePath

  BlueForcesPainterMilSym.prototype = Object.create(BlueForcesPainterFallback.prototype);

  BlueForcesPainterMilSym.prototype.constructor = BlueForcesPainterMilSym;

  BlueForcesPainterMilSym.prototype.paintBody = function(geoCanvas, feature, shape, layer, map, state) {
    if (state.level === 0) {
      BlueForcesPainterFallback.prototype.paintBody.call(this, geoCanvas, feature, shape, layer,
          map, state);
    } else {
      return this._symbolPainter.paintBody(geoCanvas, feature, shape, layer, map, state);
    }
  };

  return BlueForcesPainterMilSym;
});