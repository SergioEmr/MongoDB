define([
  'dojo/when',
  "dojo/_base/declare",
  "dijit/_WidgetBase",
  "dijit/_Templated",
  "dijit/_WidgetsInTemplateMixin",
  "luciad/view/controller/EditController",
  "dojo/text!./templates/AnnotationInfoPanel.html",
  //widgets in template
  "dijit/form/TextBox",
  "dijit/form/Button"
], function(when, declare, _WidgetBase, _Templated, _WidgetsInTemplateMixin, EditController,
            Template) {
  /**
   * @fileOverview
   * A dialog that allows editing the properties of an annotation.
   * @extends  dijit._WidgetBase
   */
  return declare([_WidgetBase, _Templated, _WidgetsInTemplateMixin], {

    templateString: Template,
    widgetsInTemplate: true,
    annotation: null,
    layer: null,
    map: null,
    constructor: function() {
      this._editContinuation = null;
    },
    _onTextChange: function(event) {

      this.annotation.properties.text = event;
      var self = this;
      this._editContinuation = when(this._editContinuation)
          .then(function() {
            return self.layer.model.put(self.annotation);
          });
    },

    _onEditClicked: function() {
      var self = this;
      this._editContinuation = when(this._editContinuation)
          .then(function() {
            self.map.hideBalloon();
            self.map.controller = new EditController(self.layer, self.annotation);
          });

    },
    _onRemoveClicked: function() { var self = this;
      this._editContinuation = when(this._editContinuation)
          .then(function() {
            self.layer.model.remove(self.annotation.id);
          });
    }
  });
});


