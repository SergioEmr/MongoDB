define([
  "luciad/view/controller/BasicCreateController"
], function(BasicCreateController) {

  /**
   * @class A create controller that adds a UUID, and optionally the current date to the properties.
   */
  function ObjectWithUUIDCreateController(shapeType, defaultProperties, options) {
    BasicCreateController.call(this, shapeType, defaultProperties, options);
  };

  ObjectWithUUIDCreateController.prototype = Object.create(BasicCreateController.prototype);

  /**
   * Generates a version 4 UUID as per RFC 4122.
   * @private
   */
  var randomUUID = function() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  };

  /**
   * Called when a new object instance needs to be created.
   *
   * @param aMapView the map view
   * @param aLayer   the layer
   *
   * @return a new object
   */
  ObjectWithUUIDCreateController.prototype.onCreateNewObject = function(aMapView, aLayer) {
    var feature = BasicCreateController.prototype.onCreateNewObject.call(this, aMapView, aLayer);
    feature.properties.uuid = randomUUID();
    if (feature.properties.hasOwnProperty("date")) {
      feature.properties.date = new Date().toISOString();
    }
    return feature;
  };

  return ObjectWithUUIDCreateController;
});