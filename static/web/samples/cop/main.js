define([
  "dojo/has",
  "dojo/on",
  "dojo/dom-geometry",
  "dojo/dom-style",
  "dojo/_base/event",
  "dojo/_base/query",
  "dijit/registry",
  "samples/template/sample",
  "samples/common/attribution/AttributionComponent",
  "samples/common/layertreecontrol/LayerTreeUI",
  "samples/common/scaleIndicator/ScaleIndicator",
  "samples/common/mouseLocation/MouseLocationComponent",
  "./ThemeManager",
  "./WorkspaceManager",
  "./themes/BackgroundTheme",

  //Load all the themes to populate
  "./themes/DefaultTheme",
  "./themes/AirPictureTheme",
  "./themes/MissionControlTheme",

  //modules that are needed while parsing index.html
  "dijit/TitlePane",
  "dijit/form/FilteringSelect",

  "dojo/domReady!"

], function(has, on, dGeometry, dStyle, dEvent, dQuery, registry, sampleTemplate, AttributionComponent, LayerTreeUI,
             ScaleIndicator, MouseLocationComponent, ThemeManager, WorkspaceManager, BackgroundTheme) {

  var wBody = document.body;
  var mapContainer = document.getElementById("mapcontainer");

  function stopEvent(e) {
    dEvent.stop(e);
  }

  if (has("touch")) {
    on(wBody, "ontouchmove", stopEvent);
    if ("ongesturestart" in window) {
      on(wBody, "gesturestart", stopEvent);
      on(wBody, "gesturechange", stopEvent);
      on(wBody, "gestureend", stopEvent);
    }
  }

  //resize the map container to fit between the security banners

  function resizeMap() {
    var pos = dGeometry.position(document.body);
    // security banners are each 20px high
    dStyle.set(mapContainer, "height", (pos.h - 40) + "px");
  }

  on(window, "resize", resizeMap);
  resizeMap();

  //create a default map in its DIV container
  BackgroundTheme.initialize(function(backgroundReference) {

    var map = sampleTemplate.makeMap({reference: backgroundReference}, {includeBackground: false, includeLayerControl: false, includeMouseLocation: false});

    new AttributionComponent(map, {domId: "attribution"});

    var scaleIndicator = new ScaleIndicator(map);

    LayerTreeUI.makeLayerTreeControl(map, "layercontroller");

    ThemeManager.context = {map: map, resourcelocation: "../resources", firstLoad: true,
      mouseLocationComponent: new MouseLocationComponent(map)};
    new WorkspaceManager(map, ThemeManager).restoreWorkspace(function() {
      //fallback setup
      ThemeManager.context.firstLoad = false;
      registry.byId("themeselector").set("value", "DefaultTheme");
    });
    BackgroundTheme.activate(ThemeManager.context);
    //only display sidebar when everything is loaded, this looks nicer
    dQuery("#sidebar")[0].style.display = "block";
  });
});
