var profile = {
  basePath: '../../..',
  action: 'release',
  optimize: 'closure',
  layerOptimize: 'closure',
  selectorEngine: 'acme',
  layers: {
    'cop/run': {
      include: ['dijit/_base', 'cop/main', 'cop/run']
    }
  },
  resourceTags: {
    amd: function(filename, mid) {
      return /\.js$/.test(filename);
    }
  }
};
