define([
  "dojo/_base/declare",
  "dijit/_WidgetBase",
  "dijit/_TemplatedMixin",
  "dijit/_WidgetsInTemplateMixin",
  "dojo/text!./templates/BlueForcesInfoPanel.html",
  //widgets in template,
  "dijit/form/TextBox",
  "dijit/form/NumberTextBox",
  "samples/cop/ui/LongitudeTextBox",
  "samples/cop/ui/LatitudeTextBox"
], function(declare, _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, Template) {
  /**
   * @fileOverview
   * A dialog that allows editing the properties of an annotation.
   * @extends  dijit._WidgetBase
   */
  return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {

        templateString: Template,
        unit: null,
        layer: null,

        postMixInProperties: function() {
        },
        refresh: function(feature, id) {
          if (id === this.unit.id) {
            this.lon.set("value", feature.shape.x);
            this.lat.set("value", feature.shape.y);
          }
        }
      }

  );
});


