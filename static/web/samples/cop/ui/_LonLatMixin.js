define([
  'dojo/_base/declare'
], function( declare) {

  return declare(null, {

    validator: function(value) {
      return (!this.required || !this._isEmpty(value)) &&
             (this._isEmpty(value) || this.parse(value) !== undefined); // Boolean
    }
  });
});
