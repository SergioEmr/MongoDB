define([
  "dojo/_base/unload",
  "dojo/_base/json",
  "dojo/cookie",
  "dojo/aspect",
  "dijit/registry"
], function( baseUnload, dJson, cookie, aspect, registry) {
  var COP_COOKIE_NAME = "COPWorkspace";

  var workspaceManagerID = 0;

  return function(map, themeManager) {

    var currentState = {
      activeTheme: null
    };
    var myID = workspaceManagerID++;

    function getCookieName() {
      return COP_COOKIE_NAME + myID;
    }

    aspect.after(themeManager, "activateTheme", function() {
      currentState.activeTheme = themeManager.activeTheme.name;
      cookie(getCookieName(), dJson.toJson(currentState), {expires: 200});
    });

    /**
     *  the unload.addOnWindowUnload and unload.addOnUnload methods
     *  are not reliable in the opera browser.
     *  They also have as a side effect that the page is never cached by the browser.
     *  Avoiding using this is recommended, but may be unavoidable in certain cases.
     */
    baseUnload.addOnUnload(function() {
      //#snippet mapState
      // Get the state from the map and store it in a cookie.
      currentState.mapState = map.saveState();
      cookie(getCookieName(), dJson.toJson(currentState), {expires: 200});
      //#endsnippet mapState

    });

    this.restoreWorkspace = function(aFallBackSetup) {
      //#snippet mapState
      // Retrieve the map state from the cookie (if it exists) and restore the map.
      var myJsonCookie = cookie(getCookieName());
      if (myJsonCookie) {
        currentState = dJson.fromJson(myJsonCookie);
        //#endsnippet mapState
        if (!currentState.mapState) {
          themeManager.context.firstLoad = false;
        }
        registry.byId("themeselector").set("value", currentState.activeTheme);
        //#snippet mapState
        if (currentState.mapState) {
          // Note: this can be null if the method onUnLoad is not supported by the browser (e.g., Opera)
          map.restoreState(currentState.mapState);
        }
      }
      //#endsnippet mapState
      else {
        aFallBackSetup();
      }
    };
  };
});
