define([
  "dojo/_base/declare",
  "dijit/_WidgetBase",
  "dijit/_TemplatedMixin",
  "dijit/_WidgetsInTemplateMixin",
  "dojo/text!../templates/PictureBox.html",
  "dojox/image/Lightbox"
], function(declare, _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, PictureBoxTemplate) {
  /**
   * @fileOverview
   * A picture box.
   * @extends  dijit._WidgetBase
   */
  return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
    templateString: PictureBoxTemplate,
    imgURL: null,
    postCreate: function() {
      this.inherited(arguments);
      this.box.startup();
    }
  });
});


