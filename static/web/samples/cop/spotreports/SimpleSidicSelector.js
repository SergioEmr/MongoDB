define([
  'dojo/_base/declare',
  "dojo/text!../templates/SimpleSidicSelector.html",
  'dijit/_WidgetBase',
  'dijit/_TemplatedMixin',
  "dijit/_WidgetsInTemplateMixin"
], function(declare, SimpleSidicSelectorTemplate, _WidgetBase, _TemplatedMixin,
            _WidgetsInTemplateMixin) {

  return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
    templateString: SimpleSidicSelectorTemplate,

    getSidic: function() {
      return this._msselect.get("value");
    },

    setSidic: function(sidic) {
      this._msselect.set("value", sidic);
    }
  });

});