define([
  'samples/common/ModuleLoad' ,
  "require"
], function(ModuleLoad, require) {

  /**
   * dynamically load the info panel. Implementation depends on whether the milsym component
   * is available
   */
  return {
    load: function(id, amdRequire, callback) {
      ModuleLoad.loadConditional(require, callback, './SpotReportInfoPanelMilSym',
          './SpotReportInfoPanelFallback');
    }
  }

});