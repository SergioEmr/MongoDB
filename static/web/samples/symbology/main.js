require([
  "dojo/_base/connect",
  "dojo/dom",
  "dojo/dom-construct",
  "luciad/model/feature/FeatureModel",
  "luciad/model/store/MemoryStore",
  "luciad/reference/ReferenceProvider",
  "luciad/shape/ShapeFactory",
  "luciad/symbology/military/MilitarySymbologyPainter",
  "luciad/symbology/SymbologyProvider!MIL_STD_2525b",
  "luciad/symbology/SymbologyProvider!MIL_STD_2525c",
  "luciad/symbology/SymbologyProvider!APP_6A",
  "luciad/symbology/SymbologyProvider!APP_6B",
  "luciad/symbology/SymbologyProvider!APP_6C",
  "luciad/view/controller/EditController",
  "luciad/view/feature/FeatureLayer",
  "samples/common/attribution/AttributionComponent",
  "samples/common/contextmenu/SampleContextMenu",
  "samples/common/LayerConfigUtil",
  "samples/symbology/FeatureFactory",
  "samples/symbology/SymbologyUtil",
  "samples/symbology/SymbologyCreationPanel",
  "samples/symbology/SymbologyPropertiesPanel",
  "samples/symbology/SymbologyClusteringManager",
  "samples/template/sample"
], function(connect, dom, domConstruct, FeatureModel, MemoryStore, ReferenceProvider, ShapeFactory,
            MilitarySymbologyPainter,
            MIL_STD_2525b, MIL_STD_2525c, APP_6A, APP_6B, APP_6C, EditController, FeatureLayer,
            AttributionComponent, SampleContextMenu, LayerConfigUtil, FeatureFactory, SymbologyUtil, SymbologyCreationPanel,
            SymbologyPropertiesPanel,
            SymbologyClusteringManager, sample) {

  var modelRef = ReferenceProvider.getReference("EPSG:4326");
  var currentSymbology = MIL_STD_2525c;

  function createMilSymModel(ref, featureData) {
    var featureStore = new MemoryStore({
      data: featureData
    });
    return new FeatureModel(featureStore, {reference: ref});
  }

  function createMilSymPainter(symbology) {
    //#snippet milSymPaintBody
    var symbologyPainter = new MilitarySymbologyPainter(
        //The military standard this painter should paint.
        symbology,
        {
          codeFunction: function(feature) {
            //We return the SIDC of the military symbol
            return feature.properties.code;
          },
          modifiers: function(feature) {
            //We return the modifiers in the properties of our feature.
            //These include both text modifiers and graphical modifiers, as defined
            //in the MS2525 and APP6 specifications.
            return feature.properties.modifiers;
          },
          style: function(feature) {
            //We return our style. Note that it is also possible to have feature-specific
            //styling if you choose to use the feature parameter.
            return {
              selectionColor: "#FF9900",
              affiliationColors: {
                "Friend": SymbologyUtil.FRIEND_COLOR,
                "Hostile": SymbologyUtil.HOSTILE_COLOR
              },
              lineWidth: 2,
              iconSize: feature.properties.iconSize ? feature.properties.iconSize : 64
            };
          },
          width: function(feature) {
            //We return the width we use for arrow-type tactical graphics.
            return feature.properties.width;
          },
          symbologyServicePath: "http://" + window.location.hostname + ":8081/symbologyservice/"
        });
    //#endsnippet milSymPaintBody

    return symbologyPainter;
  }

  function createMilSymLayer(symbology, milSymLabel) {
    var symbologyClusteringManager = new SymbologyClusteringManager(symbology);
    var featureModel = createMilSymModel(modelRef, FeatureFactory.createFeatures(modelRef, symbology));
    var featurePainter = symbologyClusteringManager.createMilSymClusteringPainter(createMilSymPainter(symbology));
    var featureLayer = new FeatureLayer(featureModel, {
      painter: featurePainter,
      selectable: true,
      editable: true,
      label: milSymLabel,
      transformer: symbologyClusteringManager.createMilSymClusteringTransformer(symbology)
    });

    featureLayer.onCreateContextMenu = LayerConfigUtil.createContextMenu;

    return featureLayer;
  }

  function createTemplateForNewSymbol(symbologyNode, shape) {
    //Retrieve bounds to place the new symbol in the same position and with a similar size
    return (!shape.pointCount || shape.pointCount === 1) ?
      //If the old symbol was an icon, use 0.03 to render the new one with a similar size
           symbologyNode.createTemplate(shape.reference, shape.bounds.x, shape.bounds.y, 0.03) :
           symbologyNode.createTemplate(shape.reference, shape.bounds.x, shape.bounds.y,
               shape.bounds.height > shape.bounds.width ? shape.bounds.height : shape.bounds.width);
  }

  /**
   * Checks whether either the new or the old symbol is an icon.
   */
  function isIconInvolved(symbologyShape, featureShape) {
    return (!featureShape.pointCount || featureShape.pointCount === 1 || !symbologyShape.pointCount ||
            symbologyShape.pointCount === 1);
  }

  /**
   * Checks whether the pointCount constraints are broken for the new symbol.
   * If so, we should use the new symbol template.
   */
  function breakPointCountConstraints(symbologyNode, featureShape) {
    return ((featureShape.pointCount > symbologyNode.maximumPointCount && symbologyNode.maximumPointCount !== -1) ||
            (featureShape.pointCount < symbologyNode.minimumPointCount && symbologyNode.minimumPointCount !== -1));
  }

  function initializeMap(map) {
    new SampleContextMenu(map);
    map.mapNavigator.fit({bounds: ShapeFactory.createBounds(modelRef, [-71.25, 0.4, 42.30, 0.15])});

    var layers = {};
    layers[MIL_STD_2525b.name] = createMilSymLayer(MIL_STD_2525b, "MIL-STD-2525b symbols");
    layers[MIL_STD_2525c.name] = createMilSymLayer(MIL_STD_2525c, "MIL-STD-2525c symbols");
    layers[APP_6A.name] = createMilSymLayer(APP_6A, "APP-6A symbols");
    layers[APP_6B.name] = createMilSymLayer(APP_6B, "APP-6B symbols");
    layers[APP_6C.name] = createMilSymLayer(APP_6C, "APP-6C symbols");
    var featureLayer = layers[currentSymbology.name];

    map.layerTree.addChild(featureLayer);

    LayerConfigUtil.addLonLatGridLayer(map);

    var creationPanel = new SymbologyCreationPanel();
    creationPanel.configureFor(currentSymbology, featureLayer, map);

    //listen to onclick handlers in symbology selection panel
    connect.connect(dom.byId("APP_6A"), "onclick", function() {
      changeMilitarySymbologyLayer(APP_6A);
    });
    connect.connect(dom.byId("APP_6B"), "onclick", function() {
      changeMilitarySymbologyLayer(APP_6B);
    });
    connect.connect(dom.byId("APP_6C"), "onclick", function() {
      changeMilitarySymbologyLayer(APP_6C);
    });
    connect.connect(dom.byId("MIL_STD_2525b"), "onclick", function() {
      changeMilitarySymbologyLayer(MIL_STD_2525b);
    });
    connect.connect(dom.byId("MIL_STD_2525c"), "onclick", function() {
      changeMilitarySymbologyLayer(MIL_STD_2525c);
    });

    function changeMilitarySymbologyLayer(symbology) {
      var previousLayer = layers[currentSymbology.name];
      map.layerTree.removeChild(previousLayer);

      currentSymbology = symbology;
      var newLayer = layers[currentSymbology.name];
      map.layerTree.addChild(newLayer, "above", map.layerTree.children[0]);

      creationPanel.configureFor(currentSymbology, newLayer, map);
    }

    var propertiesPanel = new SymbologyPropertiesPanel("propertiesPanel");
    propertiesPanel.startup();

    domConstruct.place(propertiesPanel.domNode, dom.byId("propertiesPanelInternalDiv"), "last");
    propertiesPanel.setMilitarySymbol(null, null);

    var clickHandle;

    map.on('SelectionChanged', function() {
      if (map.selectedObjects.length !== 1 || map.selectedObjects[0].selected.length !== 1) {
        propertiesPanel.updateModifiers();
        //detach listeners.
        propertiesPanel.onSymbolChanged = Function.prototype;
        propertiesPanel.onModifierUpdated = Function.prototype;
        propertiesPanel.setMilitarySymbol(null, null);
        return;
      }

      var featureToEdit = map.selectedObjects[0].selected[0];
      var layerToEdit = map.selectedObjects[0].layer;

      propertiesPanel.onSymbolChanged = function(event) {
        propertiesPanel.updateModifiers();
        var newMilitarySymbol = event.symbol;

        // Save changes before changing the symbol
        featureToEdit.properties.code = newMilitarySymbol.code;
        featureToEdit.properties.modifiers = newMilitarySymbol.textModifiers;

        var symbologyNode = featureToEdit.properties.symbology.getSymbologyNode(newMilitarySymbol.code);
        var symbologyShape = null;
        if (symbologyNode.supportsShapeType(featureToEdit.shape.type)) {
          symbologyShape = featureToEdit.shape;
        } else {
          symbologyShape = createTemplateForNewSymbol(symbologyNode, featureToEdit.shape);
        }

        if (isIconInvolved(symbologyShape, featureToEdit.shape) ||
            breakPointCountConstraints(symbologyNode, featureToEdit.shape)) {
          // If either one of the symbols is an icon or the point count constraints are not compatible then
          // use the new symbol template
          featureToEdit.shape = symbologyShape;
        }

        layerToEdit.model.store.put(featureToEdit);
        propertiesPanel.setMilitarySymbol(featureToEdit.properties.symbology, featureToEdit.properties.code, featureToEdit.properties.modifiers);
        layerToEdit.painter.invalidate(featureToEdit);
      };

      propertiesPanel.onModifierUpdated = function(event) {
        featureToEdit.properties.code = event.symbol.code;
        featureToEdit.properties.modifiers = event.symbol.textModifiers;
        layerToEdit.painter.invalidate(featureToEdit);
      };

      propertiesPanel.setMilitarySymbol(featureToEdit.properties.symbology, featureToEdit.properties.code, featureToEdit.properties.modifiers);

      clickHandle && clickHandle.remove();
      var editButton = dom.byId("editSymbol");
      clickHandle = connect.connect(editButton, "onclick", function() {
        map.selectObjects([
          {layer: layerToEdit, objects: featureToEdit}
        ]);
        map.controller = new EditController(layerToEdit, featureToEdit);
      });

    });

  }

  LayerConfigUtil.createBingLayer({type: "Aerial"})
      .then(function(bingLayer) {
        var map = sample.makeMap({reference: ReferenceProvider.getReference("EPSG:900913")},
            {includeBackground: false});
        map.layerTree.addChild(bingLayer, "bottom");
        new AttributionComponent(map, {displayNode: "attribution"});
        initializeMap(map);
      }, function(error) {
        LayerConfigUtil.createBackgroundLayer().then(function(bgLayer) {
          var map = sample.makeMap({reference: bgLayer.model.reference},
              {includeBackground: false});
          map.layerTree.addChild(bgLayer, "bottom");
          initializeMap(map);
        });
      });
});