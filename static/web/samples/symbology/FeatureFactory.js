define([
  "luciad/model/feature/Feature",
  "luciad/shape/ShapeFactory",
  "luciad/symbology/SymbologyProvider!MIL_STD_2525b",
  "luciad/symbology/SymbologyProvider!MIL_STD_2525c",
  "luciad/symbology/SymbologyProvider!APP_6A",
  "luciad/symbology/SymbologyProvider!APP_6B",
  "luciad/symbology/SymbologyProvider!APP_6C"
], function(Feature, ShapeFactory, MIL_STD_2525b, MIL_STD_2525c, APP_6A, APP_6B, APP_6C) {

  var codes = {};
  codes[MIL_STD_2525b.name] = {
    "Double Apron Fence": "G*M*OWA---****X",
    "Bridge": "G*M*BCB---****X",
    "Airport/Airbase": "SHGPIBA---H----",
    "Sea mine": "SHUPWM--------N",
    "Minesweeper": "SFSACMMS-------",
    "Battleship": "SFSPCLBB-------",
    "Carrier": "SFGAEVAA--MT---",
    "Petroleum": "SUG-IRP---H----",
    "Assault Vessel": "SFSPCALA-------",
    "Infiltration Line": "GFG-OLI-------X",
    "Decoy Mined Area, Fenced": "G*G*PY----****X",
    "Tank": "SFG-EVAT-------",
    "Intercept": "SHG-UUMSEI-----"
  };
  codes[MIL_STD_2525c.name] = {
    "Double Apron Fence": "G*M*OWA---****X",
    "Bridge": "G*M*BCB---****X",
    "Airport/Airbase": "SHGPIBA---H----",
    "Sea mine": "SHUPWM--------N",
    "Minesweeper": "SFSACMMS-------",
    "Battleship": "SFSPCLBB-------",
    "Carrier": "SFGAEVAA--MT---",
    "Petroleum": "SUG-IRP---H----",
    "Assault Vessel": "SFSPCALA-------",
    "Infiltration Line": "GFG-OLI-------X",
    "Decoy Mined Area, Fenced": "G*G*PY----****X",
    "Tank": "SFG-EVAT-------",
    "Intercept": "SHG-UUMSEI-----"
  };
  codes[APP_6A.name] = {
    "Double Apron Fence": "G*C*BOAWA-*****",
    "Bridge": "G*C*BYCB--*****",
    "Airport/Airbase": "SHGPIBA---H----",
    "Sea mine": "SHUPWM--------N",
    "Minesweeper": "SFSACMMS-------",
    "Battleship": "SFSPCLBB-------",
    "Carrier": "SFGAEVAA--MT---",
    "Petroleum": "SUG-IRP---H----",
    "Assault Vessel": "SFSPCALA-------",
    "Infiltration Line": "GFC*MOLI-------",
    "Decoy Mined Area, Fenced": "G*C*MDY---*****",
    "Tank": "SFG-EVAT-------",
    "Intercept": "SHG-UUMSEI-----"
  };
  codes[APP_6B.name] = {
    "Double Apron Fence": "G*C*BOAWA-*****",
    "Bridge": "G*C*BYCB--*****",
    "Airport/Airbase": "SHGPIBA---H----",
    "Sea mine": "SHUPWM--------N",
    "Minesweeper": "SFSACMMS-------",
    "Battleship": "SFSPCLBB-------",
    "Carrier": "SFGAEVAA--MT---",
    "Petroleum": "SUG-IRP---H----",
    "Assault Vessel": "SFSPCALA-------",
    "Infiltration Line": "GFC*MOLI-------",
    "Decoy Mined Area, Fenced": "G*C*MDY---*****",
    "Tank": "SFG-EVAT-------",
    "Intercept": "SHG-UUMSEI-----"
  };
  codes[APP_6C.name] = {
    "Double Apron Fence": "10002500002707000000",
    "Bridge": "10002500002620000000",
    "Airport/Airbase": "10062000001101000000",
    "Sea mine": "10063600001100000000",
    "Minesweeper": "10023010001204020704",
    "Battleship": "10023000001202010600",
    "Carrier": "10021510362303000000",
    "Petroleum": "10011500002507000000",
    "Assault Vessel": "10023000001203030000",
    "Infiltration Line": "10022500001410000000",
    "Decoy Mined Area, Fenced": "10002500002615000000",
    "Tank": "10021500002308000000",
    "Intercept": "10061000001505030000"
  };

  /**
   * A function the generates an array of features that represent military icons and tactical graphics.
   * @param modelRef the model reference to create the shapes of the features in.
   * @param symbology
   * @returns {*[]} An array of features that represent military icons and tactical graphics.
   */
  var createFeatures = function(modelRef, symbology){
    return [
      new Feature(
          ShapeFactory.createPolygon(modelRef, [
                ShapeFactory.createPoint(modelRef, [-71.02801080742732, 42.35820557790968]),
                ShapeFactory.createPoint(modelRef, [-71.02586504021541, 42.36395623403761]),
                ShapeFactory.createPoint(modelRef, [-71.03131528893367, 42.36721780019973]),
                ShapeFactory.createPoint(modelRef, [-71.03088613549129, 42.37039353567335]),
                ShapeFactory.createPoint(modelRef, [-71.03354688683406, 42.37206723409866]),
                ShapeFactory.createPoint(modelRef, [-71.02455612221614, 42.38032843786453]),
                ShapeFactory.createPoint(modelRef, [-71.01155277291194, 42.37148787695144]),
                ShapeFactory.createPoint(modelRef, [-71.00700374642268, 42.37994219976638]),
                ShapeFactory.createPoint(modelRef, [-70.99769111672298, 42.37698104101394]),
                ShapeFactory.createPoint(modelRef, [-71.00159641304866, 42.368870040952906]),
                ShapeFactory.createPoint(modelRef, [-70.98799224892512, 42.36187483984205]),
                ShapeFactory.createPoint(modelRef, [-70.98614688912288, 42.35775496679519]),
                ShapeFactory.createPoint(modelRef, [-70.99172588387385, 42.35350634771559]),
                ShapeFactory.createPoint(modelRef, [-71.00382801094905, 42.35599543768142]),
                ShapeFactory.createPoint(modelRef, [-71.00481506386654, 42.3471119614241]),
                ShapeFactory.createPoint(modelRef, [-71.0076903919305, 42.34672572332595])
              ]
          ),
          {
            code: codes[symbology.name]["Double Apron Fence"],
            symbology: symbology,
            modifiers: {
              uniqueDesignation: "Double Apron Fence"
            }
          },
          4
      ),
      new Feature(
          ShapeFactory.createPolyline(modelRef, [
                ShapeFactory.createPoint(modelRef, [-70.9872031751278, 42.306826536233224]),
                ShapeFactory.createPoint(modelRef, [-70.97728973060876, 42.31090349393586]),
                ShapeFactory.createPoint(modelRef, [-70.97668891578941, 42.30991644101838]),
                ShapeFactory.createPoint(modelRef, [-70.98681693702964, 42.305839483315744])
              ]
          ),
          {
            code: codes[symbology.name]["Bridge"],
            symbology: symbology
          },
          5
      ),
      new Feature(
          ShapeFactory.createPoint(modelRef, [-71.01, 42.362]),
          {
            code: codes[symbology.name]["Airport/Airbase"],
            symbology: symbology
          },
          6
      ),
      new Feature(
          ShapeFactory.createPoint(modelRef, [-70.994, 42.345]),
          {
            code: codes[symbology.name]["Sea mine"],
            symbology: symbology,
            modifiers: {
              additionalInformation: "A",
              dateTimeGroup: "121000TJAN14",
              evaluationRating: "A1",
              signatureEquipment: "!",
              reinforcedOrReduced: "RD",
              quantity: "3",
              speedLabel: "0"
            }
          },
          7
      ),
      new Feature(
          ShapeFactory.createPoint(modelRef, [-70.987, 42.35]),
          {
            code: codes[symbology.name]["Sea mine"],
            symbology: symbology,
            modifiers: {
              additionalInformation: "B",
              dateTimeGroup: "121000TJAN14",
              staffComments: "danger",
              evaluationRating: "A1",
              signatureEquipment: "!",
              reinforcedOrReduced: "D",
              quantity: "8",
              speedLabel: "0"
            }
          },
          8
      ),
      new Feature(
          ShapeFactory.createPoint(modelRef, [-70.981, 42.355]),
          {
            code: codes[symbology.name]["Sea mine"],
            symbology: symbology,
            modifiers: {
              additionalInformation: "C",
              dateTimeGroup: "121000TJAN14",
              staffComments: "danger",
              evaluationRating: "A1",
              signatureEquipment: "!",
              reinforcedOrReduced: "R",
              quantity: "17",
              speedLabel: "0"
            }
          },
          9
      ),
      new Feature(
          ShapeFactory.createPoint(modelRef, [-70.99714383248586, 42.34113315155865]),
          {
            code: codes[symbology.name]["Minesweeper"],
            symbology: symbology,
            modifiers: {
              movementDirection: 33,
              speedLabel: "4 kn"
            }
          },
          10
      ),
      new Feature(
          ShapeFactory.createPoint(modelRef, [-71.02104309610958, 42.33510874872977]),
          {
            code: codes[symbology.name]["Minesweeper"],
            symbology: symbology,
            modifiers: {
              additionalInformation: "B",
              dateTimeGroup: "121000TJAN14",
              evaluationRating: "A1",
              signatureEquipment: "!"
            }
          },
          11
      ),
      new Feature(
          ShapeFactory.createPoint(modelRef, [-71.01580742411251, 42.33545207148368]),
          {
            code: codes[symbology.name]["Minesweeper"],
            symbology: symbology
          },
          12
      ),
      new Feature(
          ShapeFactory.createPoint(modelRef, [-70.976169441195, 42.33686271356876]),
          {
            code: codes[symbology.name]["Battleship"],
            symbology: symbology
          },
          13
      ),
      new Feature(
          ShapeFactory.createPoint(modelRef, [-70.97039513916998, 42.348]),
          {
            code: codes[symbology.name]["Battleship"],
            symbology: symbology
          },
          14
      ),
      new Feature(
          ShapeFactory.createPoint(modelRef, [-71.00202986147642, 42.418981776944285]),
          {
            code: codes[symbology.name]["Carrier"],
            symbology: symbology,
            modifiers: {
              movementDirection: 200
            }
          },
          15
      ),
      new Feature(
          ShapeFactory.createPoint(modelRef, [-71.00232000495447, 42.41839888622341]),
          {
            code: codes[symbology.name]["Carrier"],
            symbology: symbology,
            modifiers: {
              movementDirection: 200
            }
          },
          16
      ),
      new Feature(
          ShapeFactory.createPoint(modelRef, [-71.00281306650876, 42.41763529923772]),
          {
            code: codes[symbology.name]["Carrier"],
            symbology: symbology,
            modifiers: {
              movementDirection: 200
            }
          },
          17
      ),
      new Feature(
          ShapeFactory.createPoint(modelRef, [-71.00332851554407, 42.416513789542684]),
          {
            code: codes[symbology.name]["Carrier"],
            symbology: symbology,
            modifiers: {
              additionalInformation: "XXX",
              dateTimeGroup: "121000TJAN14",
              staffComments: "Healthy",
              evaluationRating: "A1",
              signatureEquipment: "!",
              movementDirection: 200
            }
          },
          18
      ),
      new Feature(
          ShapeFactory.createPoint(modelRef, [-71.05580928832971, 42.39565550227383]),
          {
            code: codes[symbology.name]["Petroleum"],
            symbology: symbology
          },
          19
      ),

      new Feature(
          ShapeFactory.createPoint(modelRef, [-71.05852904827081, 42.396099205994524]),
          {
            code: codes[symbology.name]["Petroleum"],
            symbology: symbology
          },
          20
      ),

      new Feature(
          ShapeFactory.createPoint(modelRef, [-71.05715039283716, 42.3971688384809]),
          {
            code: codes[symbology.name]["Petroleum"],
            symbology: symbology
          },
          21
      ),

      new Feature(
          ShapeFactory.createPoint(modelRef, [-71.058740942783, 42.39805424050642]),
          {
            code: codes[symbology.name]["Petroleum"],
            symbology: symbology
          },
          22
      ),

      new Feature(
          ShapeFactory.createPoint(modelRef, [-71.00714383248586, 42.34113315155865]),
          {
            code: codes[symbology.name]["Assault Vessel"],
            symbology: symbology,
            modifiers: {
              movementDirection: -7,
              speedLabel: "4 kn"
            }
          },
          23
      ),

      new Feature(
          ShapeFactory.createGeoBuffer(
              modelRef,
              ShapeFactory.createPolyline(modelRef, [
                ShapeFactory.createPoint(modelRef, [-71.00714383248586, 42.34113315155865]),
                ShapeFactory.createPoint(modelRef, [-71.01, 42.362])]),
              200
          ),

          {
            code: codes[symbology.name]["Infiltration Line"],
            symbology: symbology
          },
          24
      ),

      new Feature(
          ShapeFactory.createArcBand(
              modelRef,
              ShapeFactory.createPoint(modelRef, [-70.9572031751278, 42.36226536233224]),
              500,
              1000,
              207,
              90),
          {
            code: codes[symbology.name]["Decoy Mined Area, Fenced"],
            symbology: symbology
          },
          25
      ),

      new Feature(
          ShapeFactory.createPoint(modelRef, [-71.00252419999269, 42.42022188525138]),
          {
            code: codes[symbology.name]["Tank"],
            symbology: symbology,
            modifiers: {
              movementDirection: 160
            }
          },
          26
      ),

      new Feature(
          ShapeFactory.createPoint(modelRef, [-71.04819917301573, 42.33709193733165]),
          {
            code: codes[symbology.name]["Intercept"],
            symbology: symbology
          },
          27
      ),

      new Feature(
          ShapeFactory.createPoint(modelRef, [-71.04691171268858, 42.33636826107349]),
          {
            code: codes[symbology.name]["Intercept"],
            symbology: symbology
          },
          28
      )

    ];
  };

  return {
    createFeatures:createFeatures
  };
});