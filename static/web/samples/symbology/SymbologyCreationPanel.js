define([
  'luciad/symbology/military/MilitarySymbol',
  "./SymbologyUtil",
  "dojo/_base/declare",
  "dojo/dom",
  "dojo/dom-construct",
  "dijit/_WidgetBase",
  "dijit/_Templated",
  "dijit/_WidgetsInTemplateMixin",
  "luciad/view/controller/CreateController",
  "luciad/model/feature/Feature",
  "dojo/text!samples/symbology/templates/SymbologyCreationPanel.html"
], function(MilitarySymbol, SymbologyUtil, declare, dDom, dDomConstruct, _WidgetBase, _Templated,
            _WidgetsInTemplateMixin, CreateController, Feature, Template) {

  /**
   * A property panel for the creation of new military icons and tactical graphics.
   */
  return declare([_WidgetBase, _Templated, _WidgetsInTemplateMixin], {
    templateString: Template,
    widgetsInTemplate: true,
    _symbology: null,
    _layer: null,
    _selectedCode: null,
    _map: null,
    buildRendering: function() {
      this.inherited(arguments);
      //Inject our panel into the html of the page
      dDomConstruct.place(this.domNode, dDom.byId("creationPanel"), "last");
    },
    /**
     * Configures this creation panel for the given parameters
     * @param symbology a symbology as given by the SymbologyProvider
     * @param layer the layer for which to create new objects
     * @param map the map for which to create new objects
     */
    configureFor: function(symbology, layer, map) {
      this._map = map;
      this._layer = layer;
      this._symbology = symbology;
      this.setSymbolLabelValues(symbology);
    },
    /**
     * This method is called whenever a new symbol has been selected.
     * This method was configured in SymbologyCreationPanel.html and reacts to change events.
     * @param newValue a military code
     */
    _symbolSelect: function(newValue) {

      if (!newValue) {
        this._selectedCode = null;
        //The "createSymbolAttachPoint" is the dojo-attach-point assigned to the input label of the creation panel.
        //This is set in SymbologyCreationPanel.html
        this.createSymbolAttachPoint.set('value', '');
        return;
      }

      this._selectedCode = newValue;

      //#snippet createNewObject
      var symbology = this._symbology;
      var code = this._selectedCode;
      var symbologyCreateController = new CreateController();
      symbologyCreateController.onCreateNewObject = function(map, layer) {
        var symbologyNode = symbology.getSymbologyNode(code);
        // Convert the mask to a valid SIDC
        var SIDC = new MilitarySymbol(symbology, code).code;
        var shape = symbologyNode.createTemplate(layer.model.reference, 0, 0, 1.6);
        return new Feature(shape, {code: SIDC, symbology: symbology});
      };
      this._map.controller = symbologyCreateController;
      //#endsnippet createNewObject

      this._symbolSelect(null);

    },
    /**
     * Fills the combobox with all possible military code values for the given symbology
     * @param symbology a symbology
     */
    setSymbolLabelValues: function(symbology) {
      if (symbology) {
        var symbols = SymbologyUtil.getAllSymbols(symbology);
        //The "createSymbolAttachPoint" is the dojo-attach-point assigned to the input label of the creation panel.
        //This is set in SymbologyCreationPanel.html
        var symbolStore = this.createSymbolAttachPoint.store;
        symbolStore.setData(symbols);
      }
    }
  });

});


