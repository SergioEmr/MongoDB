define([
  "luciad/view/kml/KMLLayer",
  "luciad/model/kml/KMLModel"
], function(KMLLayer, KMLModel) {

  return {

    /**
     * Creates a layer for the given URL, assuming it is KML content.
     *
     * Summary:
     * - Create a {@link luciad/model/kml/KMLModel} to load the data
     * - Create a {@link luciad/view/kml/KMLLayer}
     */

    createLayer: function(layerName, url) {

      //#snippet kml
      var model = new KMLModel(url);

      var layer = new KMLLayer(model, {label: layerName + " (KML)", selectable: true});
      //#endsnippet kml

      return layer;
    }
  };

});