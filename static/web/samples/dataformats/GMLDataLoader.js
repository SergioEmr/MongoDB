define([
  "luciad/view/feature/FeatureLayer",
  "luciad/model/feature/FeatureModel",
  "luciad/model/store/UrlStore",
  "samples/dataformats/LocalFileStore",
  "luciad/model/codec/GMLCodec"
], function(FeatureLayer, FeatureModel, UrlStore, LocalFileStore, GMLCodec) {

  return {

    /**
     * Creates a layer for the given URL of File, assuming it is GML content.
     *
     * You can style GML data using a FeaturePainter.
     * See for example the "EarthquakesPainter" in the <i>Time Line<i> sample.
     *
     * Summary:
     * - Create a store to access the data, for example {@link luciad/model/store/UrlStore}
     * - Use {@link luciad/model/codec/GMLCodec} in the store
     * - Create a {@link luciad/model/feature/FeatureModel} for the store
     * - Create a {@link luciad/view/feature/FeatureLayer}
     * - Create a {@link luciad/view/feature/FeaturePainter} to style the layer (optional)
     */

    createLayer: function(layerName, urlOrFile, painter) {

      //#snippet gml
      var codec = new GMLCodec();
      var store;

      if (typeof urlOrFile === 'string') {
        store = new UrlStore({target: urlOrFile, codec: codec});
      } else {
        store = new LocalFileStore(urlOrFile, codec);
      }

      var model = new FeatureModel(store);

      var layer = new FeatureLayer(model, {label: layerName + " (GML)", selectable: true, painter: painter});
      //#endsnippet gml

      return layer;
    }
  };

});