define([
  "samples/common/throttle",
  "samples/template/sample",
  "luciad/model/codec/GeoJsonCodec",
  "luciad/view/LayerTreeNode",
  "luciad/view/LayerTreeVisitor",
  "luciad/view/controller/Controller",
  "luciad/view/controller/HandleEventResult",
  "luciad/view/input/GestureEventType"
], function(throttle, sampleTemplate, GeoJsonCodec, LayerTreeNode, LayerTreeVisitor, Controller, HandleEventResult,
            GestureEventType) {

  /**
   * Controller that does a WMS GetFeatureInfo request for the pixel under the mouse.
   */

  function GetFeatureInfoController() {
    Controller.call(this);
    this._feature = null;
    this._featureLayer = null;
    this._callbacksToRemove = [];
    this._balloonContentProvider = function(feature) {
      var cityProps = feature.properties, node;
      node = document.createElement("ul");
      for (var key in cityProps) {
        if (cityProps.hasOwnProperty(key) && key !== "uid") {
          var item = document.createElement("li");
          item.innerHTML = key + ": " + cityProps[key];
          node.appendChild(item);
        }
      }
      return node;
    };
  }

  GetFeatureInfoController.prototype = Object.create(Controller.prototype);
  GetFeatureInfoController.prototype.constructor = GetFeatureInfoController;

  GetFeatureInfoController.prototype.onActivate = function(map) {
    this._callbacksToRemove.push(map.on("ShowBalloon", function() {
      this._balloonShowing = true;
    }, this));

    this._callbacksToRemove.push(map.on("HideBalloon", function() {
      this._balloonShowing = false;
    }, this));

    var self = this;
    this._callbacksToRemove.push(map.layerTree.on("NodeRemoved", function(event) {
      var removedLayer = event.node;
      if (self._featureLayer === removedLayer) {
        self._feature = null;
        self._featureLayer = null;
        self.map.hideBalloon();
        self.invalidate();
      }
    }));

    Controller.prototype.onActivate.apply(this, arguments);
  };

  GetFeatureInfoController.prototype.onDeactivate = function(map) {
    for (var i = 0; i < this._callbacksToRemove.length; i++) {
      this._callbacksToRemove[i].remove();
    }
    this._callbacksToRemove.length = 0;
    Controller.prototype.onDeactivate.apply(this, arguments);
  };

  GetFeatureInfoController.prototype.onDraw = function(geoCanvas) {
    if (this._feature) {
      geoCanvas.drawShape(this._feature.shape, {
        stroke: {
          color: "rgb(255,0,0)",
          width: 2
        }
      });
    }
  };

  GetFeatureInfoController.prototype.onGestureEvent = function(event) {
    if (event.type === GestureEventType.SINGLE_CLICK_CONFIRMED) {
      this._mouseClicked(event);
    } else if (event.type === GestureEventType.MOVE) {
      this._mouseMoved(event);
    }
    return HandleEventResult.EVENT_IGNORED;
  };

  GetFeatureInfoController.prototype._mouseMoved = throttle(function(event) {
    if (this._balloonShowing) {
      //don't request other feature info if the user clicked on a feature to show its properties
      //otherwise the balloon he is looking at will disappear.
      return;
    }

    this._requestFeatureInfo({
      x: event.viewPosition[0],
      y: event.viewPosition[1],
      showBalloon: false,
      ignoreResponseIfBalloonShowing: true
    });
  }, 30);

  GetFeatureInfoController.prototype._mouseClicked = function(event) {
    this.map.hideBalloon();
    this._requestFeatureInfo({
      x: event.viewPosition[0],
      y: event.viewPosition[1],
      showBalloon: true,
      ignoreResponseIfBalloonShowing: false
    });
    return true;
  };

  GetFeatureInfoController.prototype._requestFeatureInfo = function(options) {

    var queryLayer = this._findFirstVisibleAndQueryableWMSLayer();
    if (queryLayer === null) {
      return;
    }

    var x = options.x;
    var y = options.y;
    var showBalloon = options.showBalloon;
    var ignoreResponseIfBalloonShowing = options.ignoreResponseIfBalloonShowing;

    var geoJsonCodec = new GeoJsonCodec({generateIDs: true});
    var self = this;

    if (this._infoPromise) {
      return; // skip: a previous request is still busy
    }

    this._infoPromise = queryLayer.getFeatureInfo(x, y);
    if (this._infoPromise === null) {
      return;
    }

    this._infoPromise.then(
        function(response) {

          self._infoPromise = null;

          if (response.getHeader("Content-Type") !== "application/json") {
            //we can only handle GeoJson responses.
            return;
          }

          if (ignoreResponseIfBalloonShowing && self._balloonShowing) {
            //if a user is already looking at the properties of a feature, keep his balloon on-screen
            return;
          }

          var decodedFeatures = geoJsonCodec.decode({content: response.text});

          self._feature = null;
          if (decodedFeatures.hasNext()) {
            var decodedFeature = decodedFeatures.next();
            self._feature = decodedFeature;
            self._featureLayer = queryLayer;
            if (showBalloon) {
              self.map.showBalloon({object: decodedFeature, contentProvider: self._balloonContentProvider, panTo: false});
            }
          }
          self.invalidate();
        },
        function(error) {
          var wmsModel = queryLayer.model;
          var queryLayers = "" + wmsModel.queryLayers;
          sampleTemplate.error("GetFeatureInfo" + queryLayers,
              "Error while requesting feature information for layer '" + wmsModel.queryLayers + "'<br/>" +
              "Make sure:" +
              "<ul>" +
              "  <li>These WMS layers are queryable. You can look in the WMS capabilities to determine this.</li>" +
              "  <li>The WMS server supports the \"application/json\" format for feature information.</li>" +
              "  <li>the WMS server support Cross-Origin Resource Sharing (CORS)</li>" +
              "</ul>");
        }
    );
  };

  GetFeatureInfoController.prototype._findFirstVisibleAndQueryableWMSLayer = function() {

    var findQueryableLayerVisitor = {
      foundLayer: null,
      visitLayer: function(layer) {
        if (!layer.visible) {
          return LayerTreeVisitor.ReturnValue.CONTINUE;
        }

        if (typeof layer.queryable !== "undefined" && layer.queryable) {
          this.foundLayer = layer;
          return LayerTreeVisitor.ReturnValue.ABORT;
        }
        return LayerTreeVisitor.ReturnValue.CONTINUE;
      },
      visitLayerGroup: function(layerGroup) {
        layerGroup.visitChildren(this, LayerTreeNode.VisitOrder.TOP_DOWN);
        if (this.foundLayer !== null) {
          return LayerTreeVisitor.ReturnValue.ABORT;
        } else {
          return LayerTreeVisitor.ReturnValue.CONTINUE;
        }
      }
    };
    this.map.layerTree.visitChildren(findQueryableLayerVisitor, LayerTreeNode.VisitOrder.TOP_DOWN);
    return findQueryableLayerVisitor.foundLayer;
  };

  return GetFeatureInfoController;
});