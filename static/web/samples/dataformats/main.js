define([
  "./BingMapsDataLoader",
  "./GeoJsonDataLoader",
  "./GetFeatureInfoController",
  "./GMLDataLoader",
  "./KMLDataLoader",
  "./LuciadFusionDataLoader",
  "./WFSDataLoader",
  "./WMSDataLoader",
  "./WMTSDataLoader",
  "luciad/model/tileset/RasterDataType",
  "luciad/model/tileset/RasterSamplingMode",
  "luciad/reference/ReferenceProvider",
  "luciad/shape/ShapeFactory",
  "luciad/util/Promise",
  "luciad/view/LayerType",
  "samples/common/attribution/AttributionComponent",
  "samples/common/balloon/SimpleBalloonContentProvider",
  "samples/common/LayerConfigUtil",
  "samples/template/sample",
  "samples/timeline/EarthquakesPainter",
  "samples/template/sampleReady!"
], function(BingMapsDataLoader,
            GeoJsonDataLoader,
            GetFeatureInfoController,
            GMLDataLoader,
            KMLDataLoader,
            LuciadFusionDataLoader,
            WFSDataLoader,
            WMSDataLoader,
            WMTSDataLoader,
            RasterDataType,
            RasterSamplingMode,
            ReferenceProvider,
            ShapeFactory,
            Promise,
            LayerType,
            AttributionComponent,
            SimpleBalloonContentProvider,
            LayerConfigUtil,
            sampleTemplate,
            EarthquakesPainter) {

  var wgs84Reference = ReferenceProvider.getReference("EPSG:4326");
  var webMercatorReference = ReferenceProvider.getReference("EPSG:3857");

  var map = sampleTemplate.makeMap({
    reference: webMercatorReference
  }, {includeBackground: false, includeElevation: false});

  LayerConfigUtil.addLonLatGridLayer(map);

  // Controller that adds WMS GetFeatureInfo on top of the normal controller
  map.controller = new GetFeatureInfoController();

  // On-map imagery attribution component (e.g. for Bing Maps)
  //#snippet attribution
  new AttributionComponent(map, {domId: "attribution"});
  //#endsnippet attribution

  // Shared layer add handling
  var queryFinishedHandle = null;

  function addLayer(layerOrPromise) {
    Promise.when(layerOrPromise,
        function(layer) {
          // Remove any existing scheduled fit
          if (queryFinishedHandle) {
            queryFinishedHandle.remove();
            queryFinishedHandle = null;
          }

          if (layer.workingSet) {
            // On data loading error, show warning and remove the layer
            layer.workingSet.on("QueryError", function(error) {
              sampleTemplate.error(null, "Error loading " + layer.label + ":<br>" + error.message, error);
              map.layerTree.removeChild(layer);
            });

            // On data loading finished, fit on the layer
            queryFinishedHandle = layer.workingSet.on("QueryFinished", function() {
              queryFinishedHandle.remove();
              if (layer.bounds) {
                map.mapNavigator.fit({bounds: layer.bounds, animate: true});
              }
            });

            // Add simple balloon
            if (!layer.balloonContentProvider) {
              layer.balloonContentProvider = SimpleBalloonContentProvider;
            }
          } else {
            // Fit on raster layer
            map.mapNavigator.fit({bounds: layer.model.bounds, animate: true});
          }

          // Actually add the layer to the map
          map.layerTree.addChild(layer, layer.type !== LayerType.BASE ? "top" : "bottom");

          layer.visible = true;
        },
        function(error) {
          sampleTemplate.error(null, "Cannot add layer: " + error.message, error);
        });
  }

  // populate WFS versions
  var wfsVersionSelector = document.getElementById("wfs_version");

  ['2.0.0', '1.1.0'].forEach(function(version) {
    var referenceItem = document.createElement("option");
    referenceItem.text = version;
    referenceItem.value = version;
    referenceItem.selected = (version === '1.1.0');
    wfsVersionSelector.appendChild(referenceItem);
  });

  // Example data buttons

  $("#examplewmts").click(function() {
    addLayer(WMTSDataLoader.createLayer("Black marble", {
      reference: webMercatorReference,
      bounds: ShapeFactory.createBounds(webMercatorReference,
          [-20037508.34278925, 40075016.6855785, -20037508.34278925, 40075016.6855785]),
      url: LayerConfigUtil.PUBLIC_LUCIADFUSION_URL + "/wmts",
      layer: "92c09725-a9c5-46fb-bffd-d9e23b4abbf2",
      style: "default",
      format: "image/jpeg",
      tileMatrixSet: "GoogleMapsCompatible",
      level0Columns: 1,
      level0Rows: 1,
      levelCount: 8,
      dataType: RasterDataType.IMAGE,
      tileMatrices: [0, 1, 2, 3, 4, 5, 6, 7]
    }));
  });

  $("#examplefusionelevation").click(function() {
    addLayer(LuciadFusionDataLoader.createLayer("Elevation", {
      reference: wgs84Reference,
      bounds: ShapeFactory.createBounds(wgs84Reference, [-180, 360, -90, 180]),
      level0Columns: 4,
      level0Rows: 2,
      levelCount: 24,
      url: LayerConfigUtil.PUBLIC_LUCIADFUSION_URL + "/lts",
      coverageId: "e8f28a35-0e8c-4210-b2e8-e5d4333824ec",
      tileWidth: 32,
      tileHeight: 32,
      dataType: RasterDataType.ELEVATION,
      samplingMode: RasterSamplingMode.POINT
    }));
  });

  $("#examplefusionimagery").click(function() {
    addLayer(LuciadFusionDataLoader.createLayer("Black marble", {
      reference: webMercatorReference,
      bounds: ShapeFactory.createBounds(webMercatorReference,
          [-20037508.34278925, 40075016.6855785, -20037508.34278925, 40075016.6855785]),
      level0Columns: 1,
      level0Rows: 1,
      levelCount: 8,
      url: LayerConfigUtil.PUBLIC_LUCIADFUSION_URL + "/lts",
      coverageId: "92c09725-a9c5-46fb-bffd-d9e23b4abbf2",
      tileWidth: 256,
      tileHeight: 256
    }));
  });

  $("#examplewms").click(function() {
    addLayer(WMSDataLoader.createLayer(LayerConfigUtil.PUBLIC_LUCIADFUSION_URL + "/wms", "rivers", true, true));
  });

  $("#examplewfs").click(function() {
    addLayer(WFSDataLoader.createLayer(LayerConfigUtil.PUBLIC_LUCIADFUSION_URL + "/wfs", "usrivers"));
  });

  $("#examplegeojson").click(function() {
    addLayer(GeoJsonDataLoader.createLayer("Airspaces", "../resources/data/airspaces_us.json"));
  });

  $("#examplegml").click(function() {
    addLayer(GMLDataLoader.createLayer("Earthquakes", "../resources/data/gml/Events2000toPres.xml",
        new EarthquakesPainter()));
  });

  $("#examplekml").click(function() {
    addLayer(KMLDataLoader.createLayer("Twin Peaks", "../resources/data/kml/twinpeaks.kml"));
  });

  $("button").addClass("btn btn-default");

  // Only one sub-UI visible at any time
  ["file", "wms", "wfs", "bing", "example"].forEach(function(v) {
    $("#add" + v).click(function() {
      $("button").removeClass("active");
      $(".addui").hide();
      $("#add" + v).addClass("active");
      $("#add" + v + "ui").fadeIn("slow", function() {
        resetMapSize();
        map.resize();
      });
    });
  });

  // Example data UI open by default
  $("#addexample").click();

  // File UI
  if (typeof FileReader !== 'function') {
    $("#addfileui").html("<h3>Opening local files is not possible on your browser.</h3>");
  }

  $("#addfileui #add").click(function() {
    var url = $("#addfileui #url").val();
    openFilesAndUrls(document.getElementById("file").files, url ? [url] : []);

    $("#addfileui #url").val("");
    document.getElementById("file").value = "";
  });

  $("#addfileui #file").change(function() {
    $("#addfileui #add").click();
  });

  $("#addfileui #url").keypress(function(event) {
    var keyCode = event.keyCode || event.which;
    if (keyCode === 13) {
      $("#addfileui #add").click();
    }
  });

  function fileDragHover(event) {
    event.stopPropagation();
    event.preventDefault();
    event.dataTransfer.dropEffect = 'copy';
  }

  function fileDragDrop(event) {
    event.stopPropagation();
    event.preventDefault();
    openFilesAndUrls(event["dataTransfer"].files, []);
  }

  document.getElementById("map").addEventListener("dragover", fileDragHover, false);
  document.getElementById("map").addEventListener("dragleave", fileDragHover, false);
  document.getElementById("map").addEventListener("drop", fileDragDrop, false);

  function openFilesAndUrls(files, urls) {
    for (var i = 0; i < files.length; i++) {
      openFileOrUrl(files[i]);
    }
    for (var i = 0; i < urls.length; i++) {
      openFileOrUrl(urls[i]);
    }
  }

  function openFileOrUrl(urlOrFile) {
    var fileName = (typeof urlOrFile === 'string') ? urlOrFile : urlOrFile.name;
    var extension = fileName.toLowerCase().replace(/.*\./, "");
    var layerName = fileName.replace(/.*\//, "").replace(/\.[^.]*$/, "");

    if (extension === "json" || extension === "geojson") {
      addLayer(GeoJsonDataLoader.createLayer(layerName, urlOrFile));
    } else if (extension.indexOf("gml") > -1 || extension === "xml") {
      addLayer(GMLDataLoader.createLayer(layerName, urlOrFile));
    } else if (extension === "kml") {
      if (fileName !== urlOrFile) {
        sampleTemplate.warn(null, "Cannot open KML files directly, use URL instead.");
        return;
      }
      addLayer(KMLDataLoader.createLayer(layerName, urlOrFile));
    } else {
      sampleTemplate.warn(null, "Unknown file format for " + fileName);
    }
  }

  // WMS UI
  $("#addwmsui #add").click(function() {
    var url = $("#addwmsui #url").val();
    var layer = $("#addwmsui #layer").val();
    var tiled = $("#addwmsui #tiled").is(":checked");
    var queryable = $("#addwmsui #queryable").is(":checked");

    url = url.split("?")[0];
    sampleTemplate.probeURL(url + "?REQUEST=GetCapabilities&SERVICE=WMS");
    addLayer(WMSDataLoader.createLayer(url, layer, tiled, queryable), "top");
  });

  if (window.location.search.indexOf('webgl') > 0 && window.location.search.indexOf('webgl=false' < 0)) {
    // Non-tiled WMS is not yet supported in WebGLMaps
    $("#tiledwms").hide();
  }

  // WFS UI
  $("#addwfsui #add").click(function() {
    var url = $("#addwfsui #url").val();
    var layer = $("#addwfsui #layer").val();
    var urlForCapabilities = url + (url.indexOf("?") >= 0 ? "&" : "?") + "REQUEST=GetCapabilities&SERVICE=WFS";
    var options = {
      reference: $("#addwfsui #reference").val(),
      versions: [wfsVersionSelector.value]
    };

//    sampleTemplate.probeURL(urlForCapabilities);
    addLayer(WFSDataLoader.createLayer(url, layer, options), "top");
  });

  // Bing UI
  ["aerial", "road", "AerialWithLabels"].forEach(function(type) {
    $("#bing" + type).click(function() {
      var existingBingLayer = map.layerTree.findLayerById("Bing");
      if (existingBingLayer) {
        map.layerTree.removeChild(existingBingLayer);
      }
      addLayer(BingMapsDataLoader.createLayer(type));
    });
  });
});
