define([
  "luciad/transformation/TransformationFactory",
  "luciad/view/feature/ShapeProvider",
  "luciad/shape/ShapeFactory",
  "luciad/shape/ShapeType",
  "./EarthquakesPainter",
  "./EarthquakesParameterizedPointPainter",
  "./Histogram",
  "luciad/model/feature/Feature",
  "luciad/view/feature/FeaturePainter",
  "luciad/model/store/MemoryStore",
  "luciad/model/store/UrlStore",
  "luciad/model/codec/GMLCodec",
  "luciad/view/feature/FeatureLayer",
  "luciad/reference/ReferenceProvider",
  "luciad/model/feature/FeatureModel",
  "luciad/util/ColorMap",
  "luciad/util/Promise",
  "samples/timeslider/TimeSlider",
  "samples/template/sample",
  "samples/template/sampleReady!"
], function(TransformationFactory, ShapeProvider, ShapeFactory, ShapeType,
            EarthquakesPainter, EarthquakesParameterizedPointPainter, Histogram, Feature, FeaturePainter, MemoryStore, UrlStore, GMLCodec,
            FeatureLayer, ReferenceProvider, FeatureModel, ColorMap, Promise, TimeSlider, sample) {

  var map = sample.makeMap({}, {includeLayerControl: false});

  var startTime = new Date(1999, 6, 1).getTime() / 1000;
  var endTime = new Date(2011, 12, 31).getTime() / 1000;

  var timeSlider = new TimeSlider("timeslider");
  timeSlider.setValidRange(startTime, endTime, 0, 1000);

  var histogram = new Histogram(TimeSlider.REFERENCE, startTime, endTime, 12 * 12, 800);

  function getEarthquakeEventTime(quake) {
    var time = quake.properties.EventTimeAsDate;
    if (!time) {  // Cache parsed times
      time = quake.properties.EventTimeAsDate = Date.parse(quake.properties.EventTime) / 1000;
    }
    return time;
  }

  var earthQuakesLayer = createEarthquakesLayer("../resources/data/gml/Events2000toPres.xml", "Earthquakes");
  var earthQuakesTimeLayer = createEarthquakesTimeLayer(earthQuakesLayer.model);
  var tectonicPlatesLayer = createTectonicPlatesLayer("../resources/data/gml/tectonic_plates_boundaries.gml32",
      "Tectonic plates");
  var histogramLayer = createHistogramLayer(histogram);
  var currentTimeIndicatorLayer = createCurrentTimeIndicatorLayer(histogram.reference, startTime, endTime);

  map.layerTree.addChild(tectonicPlatesLayer, "top");

  map.layerTree.addChild(earthQuakesLayer, "top");
  timeSlider.layerTree.addChild(earthQuakesTimeLayer, "top");
  timeSlider.layerTree.addChild(histogramLayer, "top");
  timeSlider.layerTree.addChild(currentTimeIndicatorLayer, "top");

  syncVisibleRanges(map, earthQuakesLayer, timeSlider, earthQuakesTimeLayer, currentTimeIndicatorLayer);
  syncSelection(map, earthQuakesLayer, timeSlider, earthQuakesTimeLayer);

  earthQuakesLayer.workingSet.on("QueryFinished", updateHistogram);

  var densityColorMap = ColorMap.createGradientColorMap([
        {level: 0,  color: "rgba(     0,  96, 154, 0.0)"},
        {level: 1,  color: "rgba(     0,  96, 154, 1.0)"},
        {level: 10, color: "rgba(    19, 163, 206, 1.0)"},
        {level: 50, color: "rgba(   250, 255, 255, 1.0)"}
      ]
  );

  $("#density").addClass("btn btn-default");

  $("#density").click(function() {
    if (earthQuakesLayer.painter.density) {
      earthQuakesLayer.painter.density = null;
      $("#density").removeClass("active");
    } else {
      earthQuakesLayer.painter.density = {colorMap: densityColorMap};
      $("#density").addClass("active");
    }
  });

  $("#playbackmode").addClass("btn btn-default");

  $("#playbackmode").click(function() {
    if (earthQuakesLayer.painter.playbackModeParameter.value) {
      earthQuakesLayer.painter.playbackModeParameter.value = false;
      currentTimeIndicatorLayer.visible = false;
      $("#playbackmode").removeClass("active");
      if (playing) {
        $("#play").click();
      }
      $("#play").hide();
    } else {
      earthQuakesLayer.painter.playbackModeParameter.value = true;
      currentTimeIndicatorLayer.visible = true;
      $("#playbackmode").addClass("active");
      $("#play").show();
    }
  });

  $("#play").hide();

  $("#play").click(function() {
    play(!playing);
  });

  var playing = false;
  function play(shouldPlay) {
    playing = shouldPlay;
    if (playing) {
      playStep();
    }
    var playBtn = $("#play");
    playBtn.toggleClass("active", playing);
    playBtn.find("> span").toggleClass("glyphicon-pause", playing);
    playBtn.find("> span").toggleClass("glyphicon-play", !playing);
  }

  function playStep() {
    if (playing) {
      var delta = timeSlider.mapBounds.width / 3000;
      var newTime = timeSlider.getCurrentTime() + delta;
      if (newTime >= endTime) {
        newTime = startTime;
      }
      timeSlider.setCurrentTime(newTime);
      window.requestAnimationFrame(playStep);
    }
  }

  function createEarthquakesLayer(file, label) {
    var store = new UrlStore({codec: new GMLCodec({}), target: file});
    var model = new FeatureModel(store, {reference: ReferenceProvider.getReference("CRS:84")});
    var layer = new FeatureLayer(model, {label: label, selectable: true, painter: new EarthquakesParameterizedPointPainter()});
    return layer;
  }

  function createEarthquakesTimeLayer(model) {
    var shapeProvider = new ShapeProvider();
    shapeProvider.reference = TimeSlider.REFERENCE;
    shapeProvider.provideShape = function(feature) {
      return ShapeFactory.createPoint(TimeSlider.REFERENCE, [getEarthquakeEventTime(feature), 850]);
    };
    var layer = new FeatureLayer(model, {selectable: true, shapeProvider: shapeProvider, painter: new EarthquakesPainter()});
    return layer;
  }

  function createTectonicPlatesLayer(file, label) {
    var tectonicPlatesPainter = new FeaturePainter();
    tectonicPlatesPainter.paintBody = function(geoCanvas, feature, shape, layer, map, state) {
      geoCanvas.drawShape(shape, {stroke: {color: "rgb(255,255,255)", width: 2}});
    };

    var store = new UrlStore({codec: new GMLCodec({}), target: file});
    var model = new FeatureModel(store, {reference: ReferenceProvider.getReference("CRS:84")});
    var layer = new FeatureLayer(model, {label: label, selectable: false, painter: tectonicPlatesPainter});
    return layer;
  }

  function createHistogramLayer(histogram) {
    var histogramPainter = new FeaturePainter();
    histogramPainter.paintBody = function(geoCanvas, feature, shape) {
      geoCanvas.drawShape(shape, {
        stroke: {color: "rgb(13, 20, 28)", width: 3},
        fill: {color: "rgba(120, 160, 210, 0.8)"}
      });
    };

    return new FeatureLayer(histogram, {painter: histogramPainter, incrementalRendering: false});
  }

  function createCurrentTimeIndicatorLayer(reference, startTime, endTime) {
    var mid = (startTime + endTime) / 2;
    var currentTimeIndicatorModel = new FeatureModel(new MemoryStore(), {reference: reference});
    var currentTimeIndicator = ShapeFactory.createShape(ShapeType.POLYLINE, reference);
    currentTimeIndicator.insertPoint(0, ShapeFactory.createPoint(reference, [mid, 0]));
    currentTimeIndicator.insertPoint(1, ShapeFactory.createPoint(reference, [mid, 1000]));
    var feature = new Feature(currentTimeIndicator, {}, 1);
    currentTimeIndicatorModel.put(feature, {});
    var currentTimeIndicatorPainter = new FeaturePainter();
    currentTimeIndicatorPainter.paintBody = function(geoCanvas, feature, shape) {
      geoCanvas.drawShape(shape, {
        stroke: {color: "rgb(255, 255, 255)", width: 3}
      });
    };

    return new FeatureLayer(currentTimeIndicatorModel, {painter: currentTimeIndicatorPainter, visible: false});
  }

  // Update the histogram with the visible earthquakes
  function updateHistogram() {
    histogram.updateHistogram(function(accumulate) {
      var quakes = earthQuakesLayer.workingSet.get();
      for (var i = 0; i < quakes.length; i++) {
        if (earthQuakesTimeLayer.filter(quakes[i])) {
          var time = getEarthquakeEventTime(quakes[i]);
          var magnitude = quakes[i].properties.Magnitude;
          accumulate(time, magnitude);
        }
      }
    });
  }

  // Bi-directionally sync the spatial map with the timeline
  function syncVisibleRanges(map, layer, map2, layer2, layer3) {
    // Visible spatial area of map is used to filter time
    map.on("MapChange", function() {
      var bounds = map.mapBounds;
      var transformation = TransformationFactory.createTransformation(map.reference, layer.model.reference);
      try {
        bounds = transformation.transformBounds(bounds);
        layer2.filter = function(feature) {
          return bounds.contains2D(feature.shape);
        };
      } catch (error) {
        layer2.filter = function(feature) {
          return false;
        }
      }
      updateHistogram();
    });

    // Visible time range is used to filter spatial map
    map2.on("MapChange", function() {
      var bounds = map2.mapBounds;
      layer.painter.lowerParameter.value = bounds.x;
      layer.painter.upperParameter.value = bounds.x + bounds.width;

      //Sync current time indicator with map bounds.
      var mid = bounds.x + bounds.width /2;
      var radiusOfEffect = bounds.width * 0.03;
      layer.painter.playbackModeCurrentTimeParameter.value = mid;
      layer.painter.playbackModeRadiusOfEffect.value = radiusOfEffect;
      var model = layer3.model;
      var prom = model.query();
      Promise.when(prom, function(cursor) {
        if (cursor.hasNext()) {
          var feature = cursor.next();
          var shape = feature.shape;
          shape.translate(mid - shape.getPoint(0).x, 0);
          model.put(feature, {});
        }
      });
    });
  }

  // This assumes the two layers share the same model
  function syncSelection(map1, layer1, map2, layer2) {
    function oneWaySync(map1, layer1, map2, layer2) {
      map1.on("SelectionChanged", function(event) {
        for (var i = 0; i < event.selectionChanges.length; i++) {
          if (event.selectionChanges[i].layer === layer1) {
            var pickInfo = {
              layer: layer2,
              objects: event.selectionChanges[i].selected
            };
            map2.selectObjects([pickInfo]);
          }
        }
      });
    }

    oneWaySync(map1, layer1, map2, layer2);
    oneWaySync(map2, layer2, map1, layer1);
  }
});
