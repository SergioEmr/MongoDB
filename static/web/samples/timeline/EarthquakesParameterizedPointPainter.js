define([
  "luciad/util/IconFactory",
  "luciad/view/feature/ParameterizedPointPainter",
  "luciad/util/expression/ExpressionFactory"
], function(IconFactory, ParameterizedPointPainter, ExpressionFactory) {

  var _ = ExpressionFactory;

  function createIconExpression(levels, colors, defaultColor, small, big, thresholdMagnitude) {

    var strokeColor = "rgba(255,255,255,1)";

    var iconExpression = _.cases(_.icon({
      image: IconFactory.circle(
          {stroke: strokeColor, fill: defaultColor, width: big, height: big})
    }));

    for (var i = 0; i < levels.length; i++) {
      iconExpression = iconExpression
          .when(_.and(_.lt(_.attribute("magnitude"), _.number(thresholdMagnitude)),
                      _.lt(_.attribute("depth"), _.number(levels[i]))))
          .then(_.icon({
            image: IconFactory.circle(
                {stroke: strokeColor, fill: colors[i], width: small, height: small})
          }));

      iconExpression = iconExpression
          .when(_.lt(_.attribute("depth"), _.number(levels[i])))
          .then(_.icon({
            image: IconFactory.circle(
                {stroke: strokeColor, fill: colors[i], width: big, height: big})
          }));
    }

    iconExpression = iconExpression
        .when(_.lt(_.attribute("magnitude"), _.number(thresholdMagnitude)))
        .then(_.icon({
          image: IconFactory.circle(
              {stroke: strokeColor, fill: defaultColor, width: small, height: small})
        }));

    return iconExpression;
  }

  function EarthquakesParameterizedPointPainter() {
    var startTime = new Date(1999, 6, 1).getTime() / 1000;
    var endTime = new Date(2011, 12, 31).getTime() / 1000;
    var lowerParameter = _.numberParameter(startTime);
    var upperParameter = _.numberParameter(endTime);
    var playbackModeParameter = _.booleanParameter(false);
    var playbackModeCurrentTimeParameter = _.numberParameter((startTime + endTime)/2);
    var playbackModeRadiusOfEffectParameter = _.numberParameter(10000000);

    //Fade to grey/transparent in playback mode.
    var distanceToCurrentTimeIndex = _.distance(playbackModeCurrentTimeParameter, _.attribute("time"));
    var colorExpression = _.ifThenElse(
        playbackModeParameter,
        _.multiply(_.color("rgba(255, 255, 255, 1)"), _.add(_.divide(_.multiply(_.number(-1), distanceToCurrentTimeIndex), playbackModeRadiusOfEffectParameter), _.number(1))),
        _.color("rgba(255, 255, 255, 0.7)")
    );

    //Only show earthquakes inside time interval (regular mode).
    //Only show earthquakes shortly after they happened (playback mode).
    var visibilityExpression = _.ifThenElse(
        playbackModeParameter,
        _.and(_.lt(distanceToCurrentTimeIndex, playbackModeRadiusOfEffectParameter), _.gte(playbackModeCurrentTimeParameter, _.attribute("time"))),
        _.between(_.attribute("time"), lowerParameter, upperParameter));

    //Scaling reduces the quality of the icons.
    //To minimize the effect, we use small and big icons.
    var small = 14;
    var big = 35;
    var thresholdMagnitude = 7;

    //Gradually growing icons in playback mode.
    var scaleDistanceFactor = _.ifThenElse(
        playbackModeParameter,
        _.add(_.divide(_.multiply(_.number(2.5), distanceToCurrentTimeIndex), playbackModeRadiusOfEffectParameter), _.number(1)),
        _.number(1)
    );
    var magnitudeFactor = _.multiply(_.pow(_.number(1.5), _.attribute("magnitude")), scaleDistanceFactor);
    var scaleExpression = _.ifThenElse(_.lt(_.attribute("magnitude"), _.number(thresholdMagnitude)),
                                       _.divide(magnitudeFactor, _.number(small)),
                                       _.divide(magnitudeFactor, _.number(big)));

    var levels = [10, 25, 50, 100, 250, 400, 500];
    var colors = ["rgba(177, 0, 38, 1.0)",
                  "rgba(227, 27, 28, 1.0)",
                  "rgba(252, 78, 42, 1.0)",
                  "rgba(253, 141, 60, 1.0)",
                  "rgba(254, 178, 76, 1.0)",
                  "rgba(224, 217, 118, 1.0)",
                  "rgba(200, 237, 160, 1.0)"];
    var defaultColor = "rgba(170, 255, 204, 1.0)";
    var iconExpression = createIconExpression(levels, colors, defaultColor, small, big, thresholdMagnitude);

    var selectedIconExpression = _.ifThenElse(_.lt(_.attribute("magnitude"), _.number(thresholdMagnitude)),
                                              _.icon({
                                                image: IconFactory.circle(
                                                    {stroke: "rgba(255,255,255,1)", fill: "rgba(192, 217, 42, 1)", width: small, height: small})
                                              }),
                                              _.icon({
                                                image: IconFactory.circle(
                                                    {stroke: "rgba(255,255,255,1)", fill: "rgba(192, 217, 42, 1)", width: big, height: big})
                                              }));

    ParameterizedPointPainter.call(this, {
      draped: true,
      regular: {
        colorExpression: colorExpression,
        iconExpression: iconExpression,
        scaleExpression: scaleExpression
      },
      selected: {
        colorExpression : _.color("rgba(255,255,255,1)"),
        iconExpression: selectedIconExpression,
        scaleExpression: scaleExpression
      },
      visibilityExpression: visibilityExpression,
      attributes: {
        magnitude: "Magnitude",
        depth: "Depth",
        time: function(quake) {
          var time = quake.properties.EventTimeAsDate;
          if (!time) {  // Cache parsed times
            time = quake.properties.EventTimeAsDate = Date.parse(quake.properties.EventTime) / 1000;
          }
          return time;
        }
      }
    });

    this.lowerParameter = lowerParameter;
    this.upperParameter = upperParameter;
    this.playbackModeCurrentTimeParameter = playbackModeCurrentTimeParameter;
    this.playbackModeRadiusOfEffect = playbackModeRadiusOfEffectParameter;
    this.playbackModeParameter = playbackModeParameter;
  }

  EarthquakesParameterizedPointPainter.prototype = new ParameterizedPointPainter();
  EarthquakesParameterizedPointPainter.prototype.constructor = EarthquakesParameterizedPointPainter;

  return EarthquakesParameterizedPointPainter;

});
