define([
  "dojo/parser",
  "dijit/layout/TabContainer",
  "dijit/layout/ContentPane",
  "dojox/highlight/languages/javascript",   /*do not remove. required for syntax highlighting of code snippets*/
  "dojo/domReady!"
], function(dijParser) {

  var executed = false;

  return function() {
    if (executed){
      return;
    }
    executed = true;
    //parse the dijits
    dijParser.parse();

  };

});