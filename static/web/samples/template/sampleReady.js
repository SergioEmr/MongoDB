define([
  'dojo/_base/Deferred',
  './sample',
  'require'
], function(Deferred, sample, require) {

  var def = new Deferred();

  function sampleReady(callback) {

    if (def.isResolved()) {
      callback(true);
      return;
    }

    def.then(callback);
    
    var bootstrapModule = (window.__lcdMobile) ? './init_mobile' : './init_tabbed';
    require([bootstrapModule], function(templateInit) {
      templateInit();
      window.addEventListener("error", function(errorEvent) {
        sample.error(null, errorEvent.message);
      });
      def.resolve(true);
    });

  }

  sampleReady.load = function(id, req, load) {
    sampleReady(load);
  };

  return sampleReady;

});