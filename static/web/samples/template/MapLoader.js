/**
 * This AMD plugin will either load luciad/view/Map or luciad/view/WebGLMap, depending on the URL query parameter "webgl".
 * If webgl is defined (and not =false), luciad/view/WebGLMap is loaded.
 * Otherwise, luciad/view/Map is loaded.
 */
define({
  load: function(na, require, onload, config) {
    var map = "luciad/view/Map";

    if (window.location.search.indexOf('webgl') > 0 && window.location.search.indexOf('webgl=false' < 0)) {
      map = "luciad/view/WebGLMap";
    }

    require([map], function(value) {
      onload(value);
    });
  }
});