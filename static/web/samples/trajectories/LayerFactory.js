define([
  "luciad/geodesy/LineType",
  "luciad/model/codec/GeoJsonCodec",
  "luciad/model/feature/FeatureModel",
  "luciad/model/store/UrlStore",
  "luciad/reference/ReferenceProvider",
  "luciad/view/feature/FeatureLayer",
  "luciad/view/feature/TrajectoryPainter",
  "samples/common/airspaces/Airspaces"
], function(LineType, GeoJsonCodec, FeatureModel, UrlStore, ReferenceProvider, FeatureLayer, TrajectoryPainter, Airspaces) {

  var BLUE = "rgb(82, 116, 255)";
  var RED = "rgb(255, 82, 116)";

  return {
    createAirspacesLayer: function() {
      var airspaceModel = Airspaces.createModel(ReferenceProvider.getReference("CRS:84"), "../resources/data/airspaces_us.json", false);
      var airspacePainter = Airspaces.createPainter();
      airspacePainter.paintLabel = null;
      airspacePainter.paintBody = function(geocanvas, feature, shape, layer, map, paintState) {
        geocanvas.drawShape(shape, {
          fill: {
            color: "rgba(255, 157, 58, 0.15)"
          },
          stroke: {
            color: "rgba(255, 156, 82, 0.99)",
            width: 1
          }
        });
      };

      return Airspaces.createLayerForMap(airspaceModel, airspacePainter, false);
    },

    DEFAULT_COLOR: BLUE,
    OTHER_COLOR: RED,

    createTrajectoriesLayer: function(url, dataSetStartTime, dataSetEndTime) {
      var trajectoriesPainter = new TrajectoryPainter({
        properties: ["origin", "airline", "destination"],
        defaultColor: BLUE,
        selectionColor: "rgb(255, 0, 0)",
        lineWidth: 1,
        lineType: LineType.SHORTEST_DISTANCE,
        timeWindow: [0, dataSetEndTime - dataSetStartTime],
        timeProvider: function(feature, shape, pointIndex) {
          return feature.properties.timestamps[pointIndex];
        }
      });

      var trajectoryStore = new UrlStore({target: url , codec: new GeoJsonCodec()});
      var reference = ReferenceProvider.getReference("CRS:84");
      var trajectoryModel = new FeatureModel(trajectoryStore, {reference: reference});
      return new FeatureLayer(trajectoryModel, {
        label: "Trajectory",
        selectable: false,
        painter: trajectoriesPainter
      });
    }
  };

});