define([
  "dojo/_base/lang",
  "luciad/uom/UnitOfMeasureRegistry",
  "luciad/reference/ReferenceProvider",
  "./AxisFormat"
], function(dLang, UnitOfMeasureRegistry, ReferenceProvider, AxisFormat) {
  /*
   This file provides options for the vertical view construction
   */
  var xAxisUnit, yAxisUnit;

  function _createReference() {
//#snippet createVerticalViewReference
    xAxisUnit = UnitOfMeasureRegistry.getUnitOfMeasure("NauticalMile");
    yAxisUnit = UnitOfMeasureRegistry.getUnitOfMeasure("FlightLevel");
    var cartesianReference = ReferenceProvider.createCartesianReference({
      xUnitOfMeasure: xAxisUnit,
      yUnitOfMeasure: yAxisUnit
    });
//#endsnippet createVerticalViewReference
    return  cartesianReference;
  }

  var axisColor = 'rgb(200, 200, 200)';

  function createXAxisOptions(unitOfMeasure) {
    return {
      labelFormatter: AxisFormat.createAxisFormatPostfixLabel(unitOfMeasure),
      labelStyle: {
        fill: axisColor,
        strokeWidth: 1,
        font: '10px sans-serif',
        offsetX: 0,
        offsetY: 10,
        textAnchor: 'end',
        alignmentBaseline: 'middle'
      },
      labelRotation: -45,

      axisLineStyle: {
        color: axisColor
      },

      tickLength: 10,

      //unlabeled ticks
      subTicks: 4,
      subTickLength: 4,

      tickLineStyle: { color: axisColor },

      gridLine: false
    };
  }

  function createYAxisOptions(unitOfMeasure) {
    return dLang.mixin({}, createXAxisOptions(unitOfMeasure), {
      labelFormatter: AxisFormat.createAxisFormatPrefixLabel(unitOfMeasure),
      labelRotation: 0,
      labelStyle: {
        fill: axisColor,
        strokeWidth: 1,
        font: '10px sans-serif',
        offsetX: -10,
        offsetY: 0,
        textAnchor: 'end',
        alignmentBaseline: 'middle'
      },
      axisLineStyle: {
        color: axisColor
      },
      gridLine: true,
      subTicks: 4
    });
  }

  return{
    reference: _createReference(),
    border: {
      left: 106,
      bottom: 52
    },
    axes: {
      xAxis: createXAxisOptions(xAxisUnit),
      yAxis: createYAxisOptions(yAxisUnit)
    }
  };
});
