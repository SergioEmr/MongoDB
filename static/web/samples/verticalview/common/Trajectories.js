define([
  'samples/common/IconFactory',
  "luciad/view/Map",
  'luciad/view/feature/FeaturePainter',
  'luciad/model/feature/Feature',
  'luciad/shape/ShapeFactory',
  "luciad/model/store/MemoryStore",
  "luciad/model/feature/FeatureModel",
  "luciad/view/feature/FeatureLayer",
  "luciad/uom/UnitOfMeasureRegistry",
  "samples/common/LayerConfigUtil"
], function(IconFactory, Map, FeaturePainter, Feature, ShapeFactory, MemoryStore, FeatureModel, FeatureLayer,
            UnitOfMeasureRegistry,
            LayerConfigUtil) {

  function createTrajectoryElements(reference) {
    var trajectory1 = new Feature(
        ShapeFactory.createPolyline(reference, [
          ShapeFactory.createPoint(reference, [-84.42806667, 33.63671944, 0]),
          ShapeFactory.createPoint(reference, [-83.90991184, 33.94909830, 3400]),
          ShapeFactory.createPoint(reference, [-81.27871696, 35.45585442, 3800]),
          ShapeFactory.createPoint(reference, [-80.87131644, 35.65955468, 3000]),
          ShapeFactory.createPoint(reference, [-77.96401244, 37.18504140, 1900]),
          ShapeFactory.createPoint(reference, [-77.31760334, 37.50204662, 0])
        ]),
        { },
        1);
    var trajectory2 = new Feature(
        ShapeFactory.createPolyline(reference, [
          ShapeFactory.createPoint(reference, [-84.66006234, 39.05538843, 0]),
          ShapeFactory.createPoint(reference, [-84.23501723, 38.72355736, 3000]),
          ShapeFactory.createPoint(reference, [-81.26633925, 35.58995282, 3200]),
          ShapeFactory.createPoint(reference, [-80.92332934, 35.19189713, 0])
        ]),
        {},
        2);
    return [trajectory1, trajectory2];
  }

  var notSelected = {
    stroke: {
      color: "#228b22",
      width: 2
    }
  };

  var selected = {
    stroke: {
      color: "#7fff00",
      width: 2
    }
  };

  return {
    createPainter: function() {
      var trackPainter = new FeaturePainter();

      var image = IconFactory.rectangle({width: 12, height: 12, fill: "#228b22", stroke: "#228b22"});
      var style = {
        image: image
      };
      var bottomBorderStyle = {
        image: image,
        rotation:45,
        stem: {
          color: "#228b22"
        }
      };
      var leftBorderStyle = {
        image: image,
        rotation:45
      };
      trackPainter.paintBody = function(geocanvas, feature, shape, layer, map, paintState) {
        geocanvas.drawShape(shape, paintState.selected ? selected : notSelected);
        for (var i = 0; i < shape.pointCount; i += 1) {
          geocanvas.drawIcon(shape.getPoint(i), style);
        }
      };

      trackPainter.paintBorderBody = function(geocanvas, feature, shape, layer, map, paintState) {
        var borderStyle = (paintState.border === Map.Border.BOTTOM) ? bottomBorderStyle : leftBorderStyle;
        for (var i = 0; i < shape.pointCount; i += 1) {
          geocanvas.drawIcon(shape.getPoint(i), borderStyle);
        }
      };

      var FLUnit = UnitOfMeasureRegistry.getUnitOfMeasure("FlightLevel");
      var LABEL_TEMPLATE = '<div class="smallAirspaceLabel smallLabelGreenColor">FL %fl%</div>';
      var LABEL_STYLE = {offset: [-46, 0]};

      trackPainter.paintBorderLabel = function(labelCanvas, feature, shape, layer, map, paintState) {
        if (paintState.border === Map.Border.BOTTOM) {
          return;
        }
        var label, fl;
        for (var i = 0; i < shape.pointCount; i += 1) {
          fl = FLUnit.convertFromStandard( feature.shape.getPoint(i).z).toFixed(1);
          label = LABEL_TEMPLATE.replace("%fl%", fl);
          labelCanvas.drawLabel(label, shape.getPoint(i), LABEL_STYLE);
        }
      };

      return trackPainter;
    },

    createModel: function(reference) {
      var trajectoryArray = createTrajectoryElements(reference);
      var trajectoryStore = new MemoryStore({data: trajectoryArray});
      return new FeatureModel(trajectoryStore, {
        reference: reference
      });
    },

    createLayerForGeoMap: function(trackModel, trajectoryPainter) {

      var trackLayer = new FeatureLayer(trackModel, {
        painter: trajectoryPainter,
        selectable: true,
        label: 'Trajectories'
      });
      trackLayer.onCreateContextMenu = LayerConfigUtil.createContextMenu;

      return trackLayer;
    }
  };

});