define([
  "./AxisController",
  "./Trajectories",
  "./VerticalView",
  "dojo/_base/lang",
  "samples/common/attribution/AttributionComponent",
  "samples/common/contextmenu/SampleContextMenu",
  "samples/common/LayerConfigUtil",
  "samples/common/URLUtil",
  "samples/template/sample"
], function(AxisController, Trajectories, VerticalView, dLang,
            AttributionComponent, SampleContextMenu, LayerConfigUtil, URLUtil, sampleTemplate) {

  function createMap(reference, options) {
    var map = sampleTemplate.makeMap(
        URLUtil.getUrlQueryParameters({reference: reference}),
        dLang.mixin({includeBackground: false, includeElevation: false}, options));
    new SampleContextMenu(map);
    return map;
  }

  function propagate(l) {
    return l;
  }

  return {

    createBackgroundGISLayer: function() {
      return LayerConfigUtil
          .createBingLayer({proxyUri: '/bingproxy/'})
          .then(propagate, LayerConfigUtil.createBackgroundLayer);
    },

    createGISMap: function(backgroundLayer, options) {
      var map = createMap(backgroundLayer.model.reference, options);
      map.layerTree.addChild(backgroundLayer, "bottom");
      
      new AttributionComponent(map, {domId: "attribution"});
      return map;

    },

    createVerticalView: function(options) {

      var verticalView = new VerticalView("vview", dLang.mixin({
        trajectoryPainter: Trajectories.createPainter(),
        minScale: [7E-9, 6E-5]
      }, options));
      //Custom controller on vertical view which allows scrolling on the axis for non-uniform scaling
      verticalView.controller = new AxisController();
      return verticalView;

    }
  };

});