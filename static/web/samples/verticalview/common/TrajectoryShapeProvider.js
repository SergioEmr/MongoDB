define([
  "luciad/geodesy/GeodesyFactory",
  "luciad/geodesy/LineType",
  "luciad/shape/ShapeFactory",
  "luciad/view/feature/ShapeProvider",
  "luciad/reference/ReferenceProvider",
  "luciad/uom/UnitOfMeasureRegistry"
], function(GeodesyFactory, LineType, ShapeFactory, ShapeProvider, ReferenceProvider, UnitOfMeasureRegistry) {

  function TrajectoryShapeProvider() {
    // this provider expects all values to be in meters, and
    // returns cartesian coordinates which are expressed in meters as well
    var meterUnit = UnitOfMeasureRegistry.getUnitOfMeasure("Meter");
    this.reference = ReferenceProvider.createCartesianReference({
      xUnitOfMeasure: meterUnit,
      yUnitOfMeasure: meterUnit
    });
  }

  TrajectoryShapeProvider.prototype = new ShapeProvider();
  TrajectoryShapeProvider.prototype.constructor = TrajectoryShapeProvider;
  /**
   * Creates a cartesian shape for the passed trajectory feature. The x-coordinates
   * represent the distance along the trajectory. The corresponding y-coordinate represents the
   * altitude of the aircraft at that point.
   * @param trajectoryFeature The trajectory feature
   * @returns {luciad.shape.Shape} the cartesian shape for the trajectory
   */
  TrajectoryShapeProvider.prototype.provideShape = function(trajectoryFeature) {

    var geometry, pointCount,
        geodesy,
        point0, point1, distance,
        i, points = [];

    if (trajectoryFeature) {

      geometry = trajectoryFeature.geometry;
      pointCount = geometry.pointCount;

      geodesy = GeodesyFactory.createEllipsoidalGeodesy(geometry.reference);

      if (pointCount > 0) {
        point0 = geometry.getPoint(0);
        points[0] = ShapeFactory.createPoint(this.reference, [0, point0.z]);

        distance = 0;
        // for each of the points in the original trajectory we calculate the distance from the first point using
        // the geodesy instance. This gives us the x-coordinate of the resulting shape
        // The height is directly available in the z-coordinate of the points of the original trajectory
        for (i = 1; i < pointCount; i++) {
          point1 = geometry.getPoint(i);
          distance += geodesy.distance(point0, point1, LineType.SHORTEST_DISTANCE);
          points[i] = ShapeFactory.createPoint(this.reference, [distance, point1.z]);
          point0 = point1;
        }
      }
    }
    return ShapeFactory.createPolyline(this.reference, points);
  };

  return TrajectoryShapeProvider;
});
