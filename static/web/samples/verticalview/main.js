define([
  "luciad/reference/ReferenceProvider",
  "luciad/util/Promise",
  "luciad/view/feature/FeatureLayer",
  "samples/common/airspaces/Airspaces",
  "samples/verticalview/common/AirspaceShapeProvider",
  "samples/verticalview/common/Trajectories",
  "samples/verticalview/common/Views",
  "samples/template/sampleReady!"
], function(ReferenceProvider, Promise, FeatureLayer,
            Airspaces, AirspaceShapeProvider, Trajectories, Views) {

  var CRS84 = ReferenceProvider.getReference("EPSG:4326");

  Views
      .createBackgroundGISLayer()
      .then(boot);

  function boot(backgroundLayer) {

    var map = Views.createGISMap(backgroundLayer);

    var airspaceModel = Airspaces.createModel(CRS84, "../resources/data/airspaces_vv.json", true);
    var airspacePainter = Airspaces.createPainter();
    var airspaceLayer = Airspaces.createLayerForMap(airspaceModel, airspacePainter, true);

    var queryFinishedHandle = airspaceLayer.workingSet.on("QueryFinished", function() {
      map.mapNavigator.fit({bounds: airspaceLayer.bounds, animate: true});
      queryFinishedHandle.remove();
    });
    var trajectoryModel = Trajectories.createModel(CRS84);
    var trajectoryPainter = Trajectories.createPainter();
    var trajectoryLayer = Trajectories.createLayerForGeoMap(trajectoryModel, trajectoryPainter);

    map.layerTree.addChild(airspaceLayer, "top");
    map.layerTree.addChild(trajectoryLayer, "top");

    var verticalView = Views.createVerticalView();

    var airspaceVVLayer = new FeatureLayer(airspaceModel, {
      id: "airspaceVV",
      label: "airspacevv",
      painter: airspacePainter,
      selectable: true
    });

    airspaceVVLayer.shapeProvider = new AirspaceShapeProvider("Lower_Limit", "Upper_Limit");

    // Update vertical view after a trajectory has been edited
    trajectoryModel.on("ModelChanged", function() {
      airspaceVVLayer.shapeProvider.invalidate();
    });

    verticalView.layerTree.addChild(airspaceVVLayer, "bottom");

    syncTrajectorySelectionWithVerticalView(map, trajectoryLayer, verticalView);
    updateMainProfileAfterUpdate(trajectoryLayer, map, verticalView);

    synchronizeAirspaceLayersSelection(airspaceLayer, map, airspaceVVLayer, verticalView);
    var firstLoadHandle = trajectoryLayer.workingSet.on("QueryFinished", function() {
      firstLoadHandle.remove();
      var object = trajectoryModel.get(1);
      map.selectObjects([
        {layer: trajectoryLayer, objects: [object]}]);
    });

  }

  function syncTrajectorySelectionWithVerticalView(map, trajectoryLayer, verticalView) {
    function updateTrajectoryBasedOnSelection() {
      var selection = map.selectedObjects;
      for (var i = 0; i < selection.length; i++) {
        var layer = selection[i].layer;
        if (layer === trajectoryLayer) {
          var selectedObjects = selection[i].selected;
          if (selectedObjects.length > 0) {
            //use the first selected airspace as airspace for the vertical view
            verticalView.trajectory = selectedObjects[0];
          }
        }
      }
    }

    map.on("SelectionChanged", function(event) {
      updateTrajectoryBasedOnSelection();
    });
    updateTrajectoryBasedOnSelection();
  }

  function updateMainProfileAfterUpdate(trackLayer, map, verticalView) {
    trackLayer.workingSet.on("WorkingSetChanged", function(eventType, feature, id) {
      if (eventType === "update") {
        if (verticalView.trajectory === feature) {
          verticalView.invalidateTrajectory();
        } else {
          map.selectObjects([
            {layer: trackLayer, objects: [feature]}
          ]);
        }
      } else if (eventType === "remove") {
        var remainingElementsCursorPromise = trackLayer.model.query();
        Promise.when(remainingElementsCursorPromise, function(cursor) {
          if (cursor.hasNext()) {
            var featureToSelect = cursor.next();
            map.selectObjects([
              {layer: trackLayer, objects: [featureToSelect]}
            ]);
          } else {
            verticalView.trajectory = null;
          }
        });
      }
    });
  }

  function synchronizeAirspaceLayersSelection(airspaceLayer, map, airspaceVVLayer, verticalView) {
    var ignoreSelectionChange = false; //avoid infinite loop triggered by select in onSelectionChange
    map.on("SelectionChanged", function(event) {
      if (ignoreSelectionChange) {
        return;
      }
      try {
        ignoreSelectionChange = true;
        copySelection(event, airspaceLayer, map, airspaceVVLayer, verticalView);
      } catch (error) {
        //ignore error. Expected when selecting an object which is not included in the workingSet of the
        //other layer
      } finally {
        ignoreSelectionChange = false;
      }
    });

    verticalView.on("SelectionChanged", function(event) {
      if (ignoreSelectionChange) {
        return;
      }
      try {
        ignoreSelectionChange = true;
        copySelection(event, airspaceVVLayer, verticalView, airspaceLayer, map);
      } catch (error) {
        //ignore error. Expected when selecting an object which is not included in the workingSet of the
        //other layer
      } finally {
        ignoreSelectionChange = false;
      }
    });
  }

  function copySelection(selectionEvent, sourceLayer, sourceMap, targetLayer, targetMap) {
    var selection,
        filteredSelection,
        pickInfo;

    // Only interested in selection changes in the source layer
    filteredSelection = selectionEvent.selectionChanges.filter(function(item) {
      return item.layer === sourceLayer;
    });

    if (filteredSelection.length !== 0) {
      selection = filteredSelection[0];
      pickInfo = {
        layer: targetLayer,
        objects: selection.selected
      };

      targetMap.selectObjects([pickInfo]);
    }

  }
});
