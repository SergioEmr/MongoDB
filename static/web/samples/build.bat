@echo off

SET _JAVA_OPTIONS=-Xmx2048m

IF exist ..\release ( rd /s /q ..\release )

IF "%1" == "" (
      SET PROFILE=samples
) ELSE (
      SET PROFILE=%1
)
ECHO Compiling samples with profile '%PROFILE%'

WHERE /q node.exe

IF ERRORLEVEL 1 (
   java -Xms256m -Xmx2048m  -cp lib/util/shrinksafe/js.jar;lib/util/closureCompiler/compiler.jar;lib/util/shrinksafe/shrinksafe.jar ^
       org.mozilla.javascript.tools.shell.Main ^
       lib/dojo/dojo.js ^
       baseUrl=lib/dojo ^
       load=build ^
       -p %PROFILE%
) ELSE (
   node --max_old_space_size=4096 lib/dojo/dojo.js load=build -p %PROFILE%
)
