define([
  "luciad/model/codec/GeoJsonCodec",
  "luciad/shape/ShapeFactory"
], function(GeoJsonCodec, ShapeFactory) {

  /**
   * A GeoJson codec that creates extruded shapes for airspaces.
   */
  function AirspaceGeoJsonCodec(options) {
    GeoJsonCodec.call(this, options);
    this._lowerLimitProperty = options.lowerLimitProperty;
    this._upperLimitProperty = options.upperLimitProperty;
  }

  AirspaceGeoJsonCodec.prototype = new GeoJsonCodec();
  AirspaceGeoJsonCodec.prototype.constructor = AirspaceGeoJsonCodec;

  /**
   * Override decode to convert the decoded base shape to extruded shapes using lower and upper limit properties.
   */
  AirspaceGeoJsonCodec.prototype.decode = function(object) {

    function getHeightInMeters(height) {
      height = height.toLowerCase();

      if (height.indexOf("fl") >= 0) {
        height = height.replace("fl", "").trim();
        return parseInt(height) * 100 * 0.3048;
      }

      if (height.indexOf("ft") >= 0) {
        height = height.replace("ft", "").trim();
        return parseInt(height) * 0.3048;
      }

      if (height.indexOf("none") >= 0) {
        //FL 10000
        return 10000 * 100 * 0.3048;
      }

      return 0;
    }

    function extrude(feature, lowerLimitProperty, upperLimitProperty) {
      var baseShape = feature.shape;
      var low = getHeightInMeters(feature.properties[lowerLimitProperty]);
      var high = getHeightInMeters(feature.properties[upperLimitProperty]);
      feature.shape = ShapeFactory.createExtrudedShape(baseShape.reference, baseShape, low, high);
      return feature;
    }

    var cursor = GeoJsonCodec.prototype.decode.call(this, object);
    var self = this;

    return {
      hasNext: function() {
        return cursor.hasNext();
      },
      next: function() {
        return extrude(cursor.next(), self._lowerLimitProperty, self._upperLimitProperty);
      }
    };
  };

  return AirspaceGeoJsonCodec;
});
