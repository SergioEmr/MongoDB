define([
    "luciad/util/Evented"
], function(Evented) {

  function FullScreenSupport(domNode) {
    Evented.call(this);
    this._domNode = document.getElementById(domNode);

    document.addEventListener("fullscreenchange", FullScreenSupport.prototype._notifyListeners.bind(this), false);
    document.addEventListener("mozfullscreenchange", FullScreenSupport.prototype._notifyListeners.bind(this), false);
    document.addEventListener("webkitfullscreenchange", FullScreenSupport.prototype._notifyListeners.bind(this), false);
  }

  FullScreenSupport.prototype = Object.create(Evented.prototype);
  FullScreenSupport.prototype.constructor = FullScreenSupport;

  FullScreenSupport.prototype.requestFullScreen = function() {
    if (this._domNode.requestFullscreen) {
      this._domNode.requestFullscreen();
    } else if (this._domNode.msRequestFullscreen) {
      this._domNode.msRequestFullscreen();
    } else if (this._domNode.mozRequestFullScreen) {
      this._domNode.mozRequestFullScreen();
    } else if (this._domNode.webkitRequestFullscreen) {
      this._domNode.webkitRequestFullscreen();
    }
  };

  FullScreenSupport.prototype.cancelFullScreen = function() {
    if (document.cancelFullScreen) {
      document.cancelFullScreen();
    } else if (document.mozCancelFullScreen) {
      document.mozCancelFullScreen();
    } else if (document.webkitCancelFullScreen) {
      document.webkitCancelFullScreen();
    }
  };

  FullScreenSupport.prototype.isFullScreen = function() {
    return document.fullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement;
  };

  FullScreenSupport.prototype.toggleFullScreen = function() {
    if (this.isFullScreen()) {
      this.cancelFullScreen();
    } else {
      this.requestFullScreen();
    }
  };

  FullScreenSupport.prototype._notifyListeners = function() {
    var isFullScreen = this.isFullScreen();
    var domNode = this._domNode;
    this.emit("FullScreenChanged", isFullScreen, domNode);
  };

  return FullScreenSupport;

});