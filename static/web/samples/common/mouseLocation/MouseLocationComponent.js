define([
  'luciad/error/OutOfBoundsError',
  'luciad/shape/format/LonLatPointFormat',
  'luciad/reference/ReferenceProvider',
  'luciad/transformation/TransformationFactory',
  'luciad/shape/ShapeFactory'
], function(OutOfBoundsError, LonLatPointFormat, ReferenceProvider, TransformationFactory, ShapeFactory) {

  /**
   * Creates a mouse location readout for the given map.
   * It consists of a single div, with class .mouseLocationComponent.
   * The default styling can be found in MouseLocationComponent.css.
   * @param map The map to create the mouse location readout for.
   * @param options An options literal. The following options are supported:
   *        <ul>
   *          <li><b>options.domId</b> The dom ID of the DIV that is the mouse location readout.
   *             When omitted, this div will be created for you inside the map's DOM node.
   *             When specified and an element with that ID exists, that element will be used.
   *             When specified and no element with that ID exists, that ID will be assigned to the
   *              DOM node that was created for you inside the map's DOM node.</li>
   *           <li><b>options.outReference</b> Points under the mouse cursor will be transformed
   *             to this reference. Points passed to the formatter will be in this reference.
   *             Defaults to EPSG:4326.</li>
   *           <li><b>options.formatter</b> The {@link luciad.view.format format} used to covert
   *             points to a string. Defaults to {@link luciad.view.format.LonLatPointFormat}.</li>
   *        </ul>
   * @constructor
   */
  function MouseLocationComponent(map, options) {
    this._map = map;
    options = options || {};
    var domElement = document.getElementById(options.domId);
    if (!domElement) {
      domElement = document.createElement('div');
      if (options.domId) {
        domElement.id = options.domId;
      }
      map.domNode.appendChild(domElement);
    }
    domElement.classList.add("mouseLocationComponent");
    this._domNode = domElement;
    
    this._outRef = options.outReference || ReferenceProvider.getReference("EPSG:4326");
    this._transformation = TransformationFactory.createTransformation(
        this._map.reference,
        this._outRef);
    
    this._tempViewPoint = ShapeFactory.createPoint(null, [0, 0]);
    this._tempMapPoint = ShapeFactory.createPoint(this._map.reference, [0, 0]);
    this._tempModelPoint = ShapeFactory.createPoint(this._outRef, [0, 0]);
    this._formatter = options.formatter || new LonLatPointFormat({pattern: "lat(+DMS), lon(+DMS)"});
    this._mapNodePosition = this._map.domNode.getBoundingClientRect();
    this._mouseMovedListener = this._mouseMoved.bind(this);
    this._map.domNode.addEventListener("mousemove", this._mouseMovedListener);
    this._map.domNode.addEventListener("mousedrag", this._mouseMovedListener);
  }

  MouseLocationComponent.prototype = Object.create(Object.prototype);
  MouseLocationComponent.prototype.constructor = MouseLocationComponent;

  MouseLocationComponent.prototype._mouseMoved = function(event) {
    try {
      //#snippet transformations
      //transform points from view coordinates (in pixels) to map coordinates (in meters)
      this._tempViewPoint.move2D(
          event.clientX - this._mapNodePosition.left,
          event.clientY - this._mapNodePosition.top
      );
      this._map.viewToMapTransformation.transform(this._tempViewPoint, this._tempMapPoint);
      //transform the point in map coordinates to the output reference (latitude/longitude)
      this._transformation.transform(this._tempMapPoint, this._tempModelPoint);
      //#endsnippet transformations
      this.setValue(this._tempModelPoint);
    } catch (e) {
      if (!(e instanceof OutOfBoundsError)) {
        throw e;
      } else {
        this.setValue();
      }
    }
  };

  MouseLocationComponent.prototype.setValue = function(val) {
    if (typeof val === 'object') {
      var height = "";
      if (typeof val.z === 'number' && Math.round(val.z) !== 0) {
        height = " " + Math.round(val.z) + " m";
      }
      this._domNode.innerHTML = this._formatter.format(val) + height;
    } else {
      this._domNode.innerHTML = "";
    }
  };

  MouseLocationComponent.prototype.destroy = function() {
    this._map.domNode.removeEventListener("mousemove", this._mouseMovedListener);
    this._map.domNode.removeEventListener("mousedrag", this._mouseMovedListener);
    this._domNode.parentNode.removeChild(this._domNode);
  };

  return MouseLocationComponent;

});