define([
  "dojo/_base/declare",
  "dojo/_base/event",
  "dojo/dom-style",
  "dojo/dom-class",
  "dojo/dom-construct",
  "dijit/_Widget",
  "dijit/_TemplatedMixin",
  "./LayerTreeUI",
  "dojo/text!./templates/FoldableLayerControlDisplay.html"
], function(declare, dEvent, dDomStyle, dDomClass, dDomConstruct, _Widget, _TemplatedMixin, LayerTreeUI, template) {

  return declare([_Widget, _TemplatedMixin], {

    baseClass: "FoldableLayerControlDisplay",

    templateString: template,

    map: null,

    _isOpen: false,

    buildRendering: function() {
      this.inherited(arguments);
      dDomStyle.set(this.panelNode, "display", "none");
      dDomClass.add(this.toggleButtonNode, "FoldableLayerControlButtonClosed");
      this._isOpen = false;
      var node = dDomConstruct.create("div", {}, this.panelNode, "last");
      this.layerControl = LayerTreeUI.makeLayerTreeControl(this.map,node);
      this.layerControl.startup();
    },

    toggleControl: function() {
      if (this._isOpen) {
        this.close();
      } else {
        this.open();
      }
    },

    close: function() {
      if (this._isOpen) {
        dDomClass.remove(this.toggleButtonNode, "FoldableLayerControlButtonOpen");
        dDomClass.add(this.toggleButtonNode, "FoldableLayerControlButtonClosed");
        dDomStyle.set(this.panelNode, "display", "none");
        this._isOpen = false;
      }
    },

    open: function() {
      if (!this._isOpen) {
        dDomClass.remove(this.toggleButtonNode, "FoldableLayerControlButtonClosed");
        dDomClass.add(this.toggleButtonNode, "FoldableLayerControlButtonOpen");
        dDomStyle.set(this.panelNode, "display", "block");
        this._isOpen = true;
      }
    },

    onToggleControl: function(e) {
      this.toggleControl();
      dEvent.stop(e);
    },

    uninitialize: function() {
      this.inherited(arguments);
      this.layerControl.destroyRecursive();
    }
  });

})
;