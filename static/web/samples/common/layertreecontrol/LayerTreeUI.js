define([
  'dijit/registry',
  "dojo/_base/lang",
  "./LayerTreeModel",
  "./LayerTreeControl",
  "dijit/tree/dndSource",
  'luciad/view/LayerTreeNodeType'
], function(registry, lang, LayerTreeModel, LayerTreeControl, dndSource, LayerTreeNodeType) {

  var keys = Object.keys || function(ob) {
    var a = [];
    for (var key in ob) {
      if (ob.hasOwnProperty(key)) {
        a.push(key);
      }
    }
    return a;
  };

  function singleSelection(_, objects) {
    return objects.length === 1;
  }

  function isDropAllowed(target, refitem, where) {

    var sel = keys(refitem.selection);
    if (sel.length > 1) {//only allow single selection.
      return false;
    }

    var s = refitem.selection[sel[0]];
    var selectedNode = s.item;
    if (selectedNode.treeNodeType !== LayerTreeNodeType.LAYER_GROUP &&
        selectedNode.treeNodeType !== LayerTreeNodeType.LAYER) {
      return false;
    }

    var targetDij = registry.getEnclosingWidget(target);
    var targetNode = targetDij.item;
    if (where === 'over') {
      return  (targetNode.treeNodeType === LayerTreeNodeType.LAYER_GROUP) &&
              targetNode.canMoveChild(selectedNode, "top");
    }

    if (targetNode.treeNodeType !== LayerTreeNodeType.LAYER_GROUP &&
        targetNode.treeNodeType !== LayerTreeNodeType.LAYER) {
      return false;
    }

    //can never drop below base layers
    var parent;
    var treenode = targetDij.item;
    if (where === 'before') {
      parent = treenode.parent;
      if (parent) {
        //#snippet checkMove
        //#description Check if we can move a node to the the specified position
        return parent.canMoveChild(selectedNode, 'above', targetNode);
        //#endsnippet checkMove
      } else {
        return false;
      }
    } else if (where === 'after') {
      parent = treenode.parent;
      if (parent) {
        return parent.canMoveChild(selectedNode, 'below', targetNode);
      } else {
        return false;
      }
    } else {
      return false;
    }

  }


  var DEFAULT_OPTIONS = {
    dndController: dndSource,
    betweenThreshold: 5,
    checkAcceptance: singleSelection,
    showRoot: false,
    checkItemAcceptance: isDropAllowed
  };

  return {

    makeLayerTreeControl: function(map, node) {

      var myModel = new LayerTreeModel(map);
      var lt = new LayerTreeControl(lang.mixin(DEFAULT_OPTIONS,{
        model: myModel
      }), node);
      lt.startup();

      return lt;

    }
  };

});