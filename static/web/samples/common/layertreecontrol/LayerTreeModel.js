define([
  'luciad/view/LayerTreeNodeType'
], function(LayerTreeNodeType) {


  /**
   * This is an adapter for the map. Matches Luciad layer/layergroup/layertreenode API
   * to the dojo dijit/tree/model API.
   *
   * see also:
   *
   * http://dojotoolkit.org/documentation/tutorials/1.6/store_driven_tree/
   * http://www.thejekels.com/dojo/cbtree_AMD.html
   * http://stackoverflow.com/questions/2859188/dojo-drag-and-drop-stop-drag (check for d&d allowance)
   *
   * see dijit.tree#_createTreeNode to create custom nodes
   *
   * @constructor
   */
  function LayerTreeModel(map) {
    if (!map) {
      throw new Error('Cannot construct a layer model without a map instance');
    }

    this.map = map;

    var self = this;
    //#snippet LayerTreeChanges
    //#description subscribe to events on the map's layer tree, and update the widget accordingly
    this.map.layerTree.on('NodeAdded', function(event) {
      var node = event.node;
      self.onChildrenChange(node.parent, node.parent.children.reverse());
      self.onChange(node);
    });

    this.map.layerTree.on('NodeMoved', function(event) {
      var node = event.node;
      self.onChildrenChange(node.parent, node.parent.children.reverse());
    });

    this.map.layerTree.on('NodeRemoved', function(event) {
      var node = event.node;
      //on removal, there is no link to the parent anymore.
      //we reconstruct the tree top-down from scratch and propagate down the changes.
      var fifoQueue = [map.layerTree];
      while (fifoQueue.length > 0) {
        node = fifoQueue.pop();
        Array.prototype.push.apply(fifoQueue, node.children);
        self.onChildrenChange(node, node.children.reverse());
      }
    });
    //#endsnippet LayerTreeChanges

  }

  LayerTreeModel.prototype = {

    constructor: LayerTreeModel,

    destroy: function() {
      this.map = null;
    },

    // =======================================================================
    // Methods for traversing hierarchy
    getRoot: function(onItem) {
      onItem(this.map.layerTree);
    },

    mayHaveChildren: function(item) {
      return (item.children.length > 0 || item.treeNodeType === LayerTreeNodeType.LAYER_GROUP);
    },

    getChildren: function(parentItem, onComplete) {
      onComplete(parentItem.children.reverse());
    },

    // =======================================================================
    // Inspecting items
    isItem: function(something) {
      return (something.treeNodeType === LayerTreeNodeType.LAYER || something.treeNodeType === LayerTreeNodeType.LAYER_GROUP);
    },

    getIdentity: function(item) {
      return item.id;
    },

    getLabel: function(item) {
      return (this.map.layerTree === item) ? "Layers" : item.label;
    },

    // =======================================================================
    // Write interface
    newItem: function(args, parent, insertIndex, before) {

      // summary:
      //		Creates a new item.   See `dojo/data/api/Write` for details on args.
      // args: dijit/tree/dndSource.__Item
      // parent: Item
      // insertIndex: int?
      //		Allows to insert the new item as the n'th child of `parent`.
      // before: Item?
      //		Insert the new item as the previous sibling of this item.  `before` must be a child of `parent`.
      // tags:
      //		extension
      throw new Error('newItem not implemented');
    },

    pasteItem: function(childItem, oldParentItem, newParentItem, bCopy, insertIndex, before) {

      if (insertIndex === undefined) {//first one
        oldParentItem.removeChild(childItem);
        newParentItem.addChild(childItem, 'top');
        return;
      }

      try {
        if (oldParentItem && !newParentItem) {//removed
          oldParentItem.removeChild(childItem);
        } else if (newParentItem === oldParentItem) {//move around in same node
          if (before) {
            newParentItem.moveChild(childItem, 'above', before);
          } else {
            newParentItem.moveChild(childItem, 'bottom');
          }
        } else {//moved to different node
          oldParentItem.removeChild(childItem);
          if (before) {
            newParentItem.addChild(childItem, 'above', before);
          } else {
            newParentItem.addChild(childItem, 'bottom');
          }
        }
      } catch (e) {
        console.error('Cannot update layer. This should not happen');
        throw e;
      }

    },

    // =======================================================================
    // Callbacks
    onChange: function(item) {
    },

    onChildrenChange: function(parent, newChildrenList) {
    }

  };

  return LayerTreeModel;

});