define([
  "dojo/request"
], function(request) {

  return {
    loadConditional: function(require, callback, preferred, fallback) {
      var painterPath = require.toUrl(preferred);
      return request(painterPath + ".js")//do ajax call to check for presence of module.
          .then(function() {//the module exists -> load it using the AMD module loader
            return preferred;
          }, function(er) {//the module is not present -> return fallback
            return fallback;
          })
          .then(function(modulePath) {
            require([modulePath], function(module) {
              callback(module);
            })
          })
    }
  };

});