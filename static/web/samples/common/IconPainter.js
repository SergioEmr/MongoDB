define(["luciad/view/feature/FeaturePainter"], function(FeaturePainter) {

  function IconPainter(icon, selectedIcon) {
    this._icon = icon;
    this._selectedIcon = selectedIcon;
  }

  IconPainter.prototype = Object.create(FeaturePainter.prototype);

  IconPainter.prototype.getIconSize = function(feature, icon) {
    return icon.width;
  };

  IconPainter.prototype.paintBody = function(geoCanvas, feature, shape, layer, map, state) {
    var icon = state.selected ? this._selectedIcon : this._icon;
    var dimension = this.getIconSize(feature, icon);
    geoCanvas.drawIcon(
        shape.focusPoint,
        {
          width: dimension + "px",
          height: dimension + "px",
          image: icon
        }
    );
  };

  return IconPainter;
});
