define([],function(){

  var getUrlQueryParameters = function(defaults) {
    var query = window.location.search;
    if (!query || query.length <= 1) {
      return defaults;
    } else {
      var options = {};
      for (var attrname in defaults) {
        options[attrname] = defaults[attrname];
      }
      query.substring(1).split("&").forEach(function(param) {
        var pair = param.split("=");
        var key = pair[0];
        var value = pair.length > 1 ? pair[1] : true;
        options[key] = value;
      });
      return options;
    }
  };

  return {
    getUrlQueryParameters: getUrlQueryParameters
  };
});