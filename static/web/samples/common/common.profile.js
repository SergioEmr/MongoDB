var profile = (function() {
  function copyOnly(filename, mid) {
    var list = {
      "common/common.profile": 1,
      "common/package.json": 1
    };
    return mid in list;
  }

  return {
    resourceTags: {
      copyOnly: function(filename, mid) {
        return copyOnly(filename, mid);
      },
      amd: function(filename, mid) {
        return (/\.js$/).test(filename) && !copyOnly(filename, mid);
      }
    }
  };
})();

