define([
  "dojo/request",
  "luciad/util/Evented",
  "luciad/util/Promise",
  "luciad/model/codec/GeoJsonCodec"
], function(request, Evented, Promise, GeoJsonCodec) {

  function ArrayCursor(array) {
    // make a shallow clone of the array.
    this.array = array.slice(0);
    this.index = 0;
  }

  ArrayCursor.prototype.hasNext = function() {
    return this.index < this.array.length;
  };

  ArrayCursor.prototype.next = function() {
    if (this.index >= this.array.length) {
      throw new Error("No more elements in cursor:  " +
                      "next() should only be called when hasNext() returns true");
    }
    return this.array[this.index++];
  };

  /**
   * @description Creates a new RestStore instance. This store has capabilities to request its webservice to add,
   * modify or remove features through REST.
   *
   * @param {Object} options options for the new store instance
   * @param {String} options.target the base URL for the REST store
   * @param {String} [options.accepts="application/javascript, application/json"] The contents of the HTML accept header, which defines the
   * content-types that are acceptable for the response. You want to change this if you want to support another format. In that
   * case, you also need to change the {@link luciad.model.codec.Codec Codec} used by this <code>Store</code>.
   * @param {luciad.model.codec.Codec} [options.codec=luciad.model.codec.GeoJsonCodec] The {@link luciad.model.codec.Codec Codec} which
   * will be used to convert the server response into LuciadRIA {@link luciad.model.feature.Feature Features} and vice versa. The <code>Codec</code>
   * needs to support both the <code>decode</code> and <code>encode</code> method.You want to change this if you want to support another format.
   * In that case, you also need to change the accept header by specifying the <code>accept</code> property.
   * @class A store that can be used to access object collections from a REST web services. This class makes
   * the following assumptions wrt the REST interface of the target web service:
   * <ul>
   *   <li>The collection of objects can be obtained from a given base URL
   *   <li>The URL of individual objects can be derived from the base URL by appending an additional path component
   *       whose value is the ID of the object.
   * </ul>
   * @implements luciad.model.store.Store
   * @implements luciad.util.Evented
   * @constructs
   */
  function RestStore(options) {
    options = options || {};
    this.target = options.target;
    while (this.target[this.target.length - 1] === "/") {
      this.target = this.target.substring(0, this.target.length - 1);
    }
    this.accepts = options.accepts || "application/javascript, application/json";
    if (typeof options.idPath === "string") {
      console.warn(
          "The RestStore no longer uses the idPath option. Consult the release notes of V2013.0 for more information.");
    }
    this.codec = options.codec || new GeoJsonCodec();
  }

  RestStore.prototype = Object.create(Evented.prototype);

  RestStore.prototype.constructor = RestStore;

  /**
   * @name get
   * @function
   * @description Retrieve a {@link luciad.model.feature.Feature Feature} from this store by id.
   * @param {String | Number} id The identifier of the <code>Feature</code>.
   * @param {Object} [options] an options object
   * @param {Object} [options.headers] optional headers. These will be added to the HTTP request headers.
   * @returns {luciad.util.Promise} A promise for the <code>Feature</code> with the given id.
   */
  RestStore.prototype.get = function(id, options) {
    var headers = options || {};
    headers.Accept = this.accepts;
    var requestOptions = {
      method: "GET",
      handleAs: "text",
      headers: headers
    };
    //here we use dojo/request for the async request
    var self = this;
    var serverResponsePromise = request(this.target + "/" + id, requestOptions);
    return serverResponsePromise.response.then(
        function(response) {
          var data = response.data;
          var contentType = (typeof( response.getHeader ) === "function") ? response.getHeader("Content-Type")
              : undefined;
          return self.codec.decode({
            content: data,
            contentType: contentType
          });
        }
    ).then(
        function(cursor) {
          return cursor.next();
        }
    );
  };

  RestStore.prototype._put = function(feature, options) {
    options = options || {};
    var id = ("id" in options) ? options.id : feature.id;
    var hasId = options.overwrite;
    var encodedFeature = this.codec.encode(new ArrayCursor([feature]));

    var url = hasId ? (this.target + "/" + id) : (this.target + "/");
    var requestOptions = {
      method: (hasId && !options.incremental) ? "PUT" : "POST",
      data: encodedFeature.content,
      handleAs: "text",
      headers: {
        "Content-Type": encodedFeature.contentType,
        Accept: this.accepts,
        "If-Match": hasId ? "*" : null,
        //We only add if-none-match if we try to put a feature that already has an id
        //otherwise, the request will fail if any resource already exists
        "If-None-Match": options.overwrite === false && hasId ? "*" : null
      }
    };
    var self = this;

    var serverResponsePromise = request(url, requestOptions);
    //We use the following logic to determine the return ID. You might want to change this logic depending on the behavior of the server
    //- If the id is specified, this will be the id if the serverResponsePromise is resolved
    //- If the body of the server response is not empty, it will be passed to the codec in an attempt to convert it to a
    //  RIA feature. If this succeeds, the ID fo the Feature will be used as ID. If this fails, the contents of the
    //  server response is considered the ID (some REST server returns the whole Feature, some only the ID)
    //- If the Location header is specified, we assume this contains the location for that Feature. The ID will
    //  be parsed from that Location and returned
    //- If all the above fails, the returned Promise will be rejected
    return Promise.resolve(serverResponsePromise.response).then(
        function(response) {
          if (hasId) {
            return id;
          }
          var data = response.data;
          if (data && data !== "") {
            try {
              var contentType = (typeof( response.getHeader ) === "function") ? response.getHeader("Content-Type")
                  : undefined;
              var featureCursor = self.codec.decode({
                content: data,
                contentType: contentType
              });
              if (featureCursor.hasNext()) {
                return featureCursor.next().id;
              }
            } catch (ignore) {
            }
            //the codec failed to parse it, assume the body just contains the ID
            return data;
          } else {
            //empty body, check the location header
            var locationHeader = (typeof( response.getHeader ) === "function" ) ? response.getHeader("Location")
                : undefined;
            if (locationHeader) {
              return locationHeader.substring(self.target.length - 1);
            }
          }
          throw 'Empty body';
        }
    ).then(
        function(returnedID) {
          feature.id = returnedID;
          self.emit("StoreChanged", hasId ? "update" : "add", feature, feature.id);
          return feature.id;
        }
    );
  };

  /**
   * @name put
   * @description Update an existing object in the store.
   * @function
   * @param {Object} object the object to update.
   * @param {Object} [options] An optional object specifying options specific to the <code>Store</code>. No options are
   * currently supported.
   * @returns {luciad.util.Promise} A promise for the identifier of the object.
   */
  RestStore.prototype.put = function(object, options) {
    options = options || {};
    options.overwrite = true;
    return this._put(object, options);
  };

  /**
   * @name add
   * @description Add a new {@link luciad.model.feature.Feature Feature} to the store.
   * @function
   * @param {luciad,model.feature.Feature} feature the <code>Feature</code> to add.
   * @param {Object} [options] An optional object specifying options specific to the <code>Store</code>. No options are
   * currently supported.
   * @returns {luciad.util.Promise} a promise for the identifier of the <code>Feature</code>.
   */
  RestStore.prototype.add = function(feature, options) {
    options = options || {};
    options.overwrite = false;
    return this._put(feature, options);
  };

  /**
   * @name remove
   * @description Removes an object from the store by id.
   * @function
   * @returns {luciad.util.Promise} a promise for a truthy value. True if the removal was successful.
   */
  RestStore.prototype.remove = function(id) {
    var result = request(this.target + "/" + id, {
      method: "DELETE"
    });

    var self = this;
    return Promise.resolve(result).then(
        function() {
          self.emit("StoreChanged", "remove", undefined, id);
          return true;
        }
    );
  };

  /**
   * @name query
   * @description Query the store for objects.
   * @function
   * @param {Object} [query] a query object. This is a flat object containing key-value pairs. These are interpreted as the query parameters (key=value) of the HTTP request.
   * @param {Object} [options] An optional object specifying options specific to the <code>Store</code>
   * @param {luciad.reference.Reference} [options.reference] The reference, which will be passed to the <code>Codec</code> when specified
   * @returns {luciad.util.Promise} A promise for a {@link luciad.model.store.Cursor} over the result set.
   */
  RestStore.prototype.query = function(query, options) {
    var headers = {Accept: this.accepts};
    options = options || {};

    if (options.start >= 0 || options.count >= 0) {
      headers.Range = "items=" + (options.start || '0') + '-' +
                      (("count" in options && options.count !== Infinity) ?
                       (options.count + (options.start || 0) - 1) : '');
    }
    if (query && typeof query === "object") {
      query = Object.keys(query).reduce(function(str, key, i) {
        var delimiter, val;
        delimiter = (i === 0) ? '?' : '&';
        key = encodeURIComponent(key);
        val = encodeURIComponent(query[key]);
        return [str, delimiter, key, '=', val].join('');
      }, '');
    }
    if (options && options.sort) {
      var sortParam = this.sortParam;
      query += (query ? "&" : "?") + (sortParam ? sortParam + '=' : "sort(");
      for (var i = 0; i < options.sort.length; i++) {
        var sort = options.sort[i];
        query += (i > 0 ? "," : "") + (sort.descending ? '-' : '+') + encodeURIComponent(sort.attribute);
      }
      if (!sortParam) {
        query += ")";
      }
    }
    var url = this.target + (query || "");
    var requestOptions = {
      method: "GET",
      handleAs: "text",
      headers: headers
    };
    var self = this;
    var serverResponsePromise = request(url, requestOptions);
    return serverResponsePromise.response.then(
        function(response) {
          var data = response.data;
          var contentType = (typeof( response.getHeader ) === "function") ? response.getHeader("Content-Type")
              : undefined;
          return self.codec.decode({
            content: data,
            contentType: contentType,
            reference: options.reference
          });
        }
    );
  };

  return RestStore;
});
