define(["luciad/view/feature/FeaturePainter"], function(FeaturePainter) {

  function ShapePainter(style, selectedStyle) {
    this._style = style;
    this._selectedStyle = selectedStyle ? selectedStyle : this._style;
  }

  ShapePainter.prototype = Object(FeaturePainter.prototype);
  ShapePainter.prototype.constructor = ShapePainter;
  ShapePainter.prototype.paintBody = function(geoCanvas, feature, shape, layer, map, state) {
    geoCanvas.drawShape(shape, state.selected ? this._selectedStyle : this._style);
  }

  return ShapePainter;
});