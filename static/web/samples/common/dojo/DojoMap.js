define([
  "dijit/CheckedMenuItem",
  "dijit/focus",
  "dijit/Menu",
  "dijit/MenuItem",
  "dijit/MenuSeparator",
  "dijit/popup",
  "dojo/dom"
], function(CheckedMenuItem, focus, Menu, MenuItem, MenuSeparator,
            popup, dom) {
  function onShowContextMenu(position, contextMenu) {
    var menu = new Menu({
      title: contextMenu.title,
      showTitle: true
    });

    var items = contextMenu.items;
    for (var i = 0; i < items.length; i++) {
      var item = items[i];
      if (item.separator) {
        menu.addChild(new MenuSeparator());
      } else {
        var options = {};
        options.label = item.label;
        options.onClick = item.action;
        if (item.iconClass !== undefined) {
          options.iconClass = item.iconClass;
        }

        if (item.checked !== undefined) {
          options.checked = item.checked;
          menu.addChild(new CheckedMenuItem(options));
        } else {
          menu.addChild(new MenuItem(options));
        }
      }
    }

    var prevFocusNode = focus.get("prevNode");
    var curFocusNode = focus.get("curNode");
    var savedFocusNode = !curFocusNode || (dom.isDescendant(curFocusNode, menu.domNode)) ? prevFocusNode : curFocusNode;

    function closeAndRestoreFocus() {
      // user has clicked on a menu or popup
      if (menu.refocus && savedFocusNode) {
        savedFocusNode.focus();
      }
      popup.close(menu);
      menu.destroyRecursive();
    }

    popup.open({
      popup: menu,
      x: position[0] + 2,//offset it slightly, so it is not under the pointer
      y: position[1] + 2,
      onExecute: closeAndRestoreFocus,
      onCancel: closeAndRestoreFocus,
      orient: menu.isLeftToRight() ? 'L' : 'R'
    });
    menu.focus();

    menu._onBlur = function() {
      this.inherited('_onBlur', arguments);
      // Usually the parent closes the child widget but if this is a context
      // menu then there is no parent
      popup.close(menu);
      menu.destroyRecursive();
      // don't try to restore focus; user has clicked another part of the screen
      // and set focus there
    };
    return menu.domNode;
  }

  return {
    configure: function(map) {
      map.onShowContextMenu = onShowContextMenu;
    }
  };
});