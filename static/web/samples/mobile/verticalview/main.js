define([
  "dijit/registry",
  "dojo/dom-geometry",
  "dojo/on",
  "luciad/reference/ReferenceProvider",
  "luciad/view/feature/FeatureLayer",
  "samples/common/navigation/ZoomControl",
  "samples/common/airspaces/Airspaces",
  "samples/verticalview/common/AirspaceShapeProvider",
  "samples/verticalview/common/Trajectories",
  "samples/verticalview/common/Views",
  "dojox/mobile/deviceTheme",
  "dojox/mobile/View",
  "dojox/mobile/Heading",
  "samples/template/sampleReady!"
], function(registry, domGeometry, on, ReferenceProvider, FeatureLayer, ZoomControl, Airspaces, AirspaceShapeProvider,
            Trajectories, Views) {

  var mapMobileView,
      verticalViewMobileView,
      mapNode;

  mapMobileView = registry.byId("mapview");
  verticalViewMobileView = registry.byId("verticalview");
  mapNode = document.getElementById("map");

  Views
      .createBackgroundGISLayer()
      .then(bootMaps);

  function bootMaps(layer) {

    var map = Views.createGISMap(layer, {includeZoomControl: false});

    new ZoomControl(map, {touch : true});

    var ref = ReferenceProvider.getReference("EPSG:4326");
    var airspaceModel = Airspaces.createModel(ref, "../../resources/data/airspaces_vv.json", true);
    var airspacePainter = Airspaces.createPainter();
    var airspaceLayer = Airspaces.createLayerForMap(airspaceModel, airspacePainter, false);

    var queryFinishedHandle = airspaceLayer.workingSet.on("QueryFinished", function() {
      map.mapNavigator.fit({bounds: airspaceLayer.bounds, animate: true});
      queryFinishedHandle.remove();
    });

    var trajectoryModel = Trajectories.createModel(ref);
    var trajectoryPainter = Trajectories.createPainter();
    var trajectoryLayer = Trajectories.createLayerForGeoMap(trajectoryModel, trajectoryPainter);

    map.layerTree.addChild(airspaceLayer, "top");
    map.layerTree.addChild(trajectoryLayer, "top");

    var verticalView = Views.createVerticalView({
      border: {
        left: 48,
        bottom: 50
      }
    });
    var airspaceVVLayer = new FeatureLayer(airspaceModel, {
      id: "airspaceVV",
      label: "airspacevv",
      painter: airspacePainter,
      selectable: false
    });

    airspaceVVLayer.shapeProvider = new AirspaceShapeProvider("Lower_Limit", "Upper_Limit");

    // Update vertical view after a trajectory has been edited
    trajectoryModel.on("ModelChanged", function() {
      airspaceVVLayer.shapeProvider.invalidate();
    });

    verticalView.layerTree.addChild(airspaceVVLayer, "bottom");

    setupViewManagement(mapMobileView, verticalViewMobileView, map, verticalView);
    setupSelection(mapMobileView, map, verticalView);

    var fitbutton = document.getElementById('fitMp');
    fitbutton.onclick = function() {
      verticalView.fitOnTrajectoryLayer();
    };

    function setupSelection(mapMobileView, map, verticalView) {
      on(map, "SelectionChanged", function(event) {
        var selectionChanges = event.selectionChanges,
            trajectory;
        if ((selectionChanges.length === 1) && selectionChanges[0].selected.length > 0) {
          trajectory = selectionChanges[0].selected[0];

          mapMobileView.performTransition("verticalview", 1, "slide", null, function() {
            verticalView.trajectory = trajectory;
          });
        }
      });

    }

    function setupViewManagement(mapMobileView, verticalViewMobileView, map, verticalView) {
      on(mapMobileView, "AfterTransitionIn", function() {
        map.resize();
      });
      on(verticalViewMobileView, "AfterTransitionIn", function() {
        resizeVerticalView();
      });
    }

    var resizeVerticalView = (function() {
      var verticalViewMobileViewDomNode = document.getElementById("verticalview");
      var verticalViewDomNode = document.getElementById("vview");
      var headingDomNode = document.getElementById("vvheading");
      return function() {
        var vvbox = domGeometry.getContentBox(verticalViewMobileViewDomNode),
            headingbox = domGeometry.getMarginBox(headingDomNode);
        verticalViewDomNode.style.height = (vvbox.h - headingbox.h) + "px";
        verticalView.resize();
      }
    })();

    function fitToScreen() {
      document.body.style.minHeight = "100%";
      mapNode.style.height = "100%";
      setTimeout(function() {
        map.resize();
        resizeVerticalView();
      }, 100);

      //fit on screen at load time
      setTimeout(fitToScreen, 200);
      //listen to orientation changes
      on(window, "orientationchange", fitToScreen);
      //listen to resize
    }
  }
});
