#!/bin/sh

export _JAVA_OPTIONS=-Xmx2048m

rm -rf ../release

profile=${1:-samples}
echo Compiling samples with profile '$profile'

if which node 2>/dev/null; then
  node --max_old_space_size=4096 lib/dojo/dojo.js load=build -p $profile
else
  java -Xms256m -Xmx2048m  -cp lib/util/shrinksafe/js.jar:lib/util/closureCompiler/compiler.jar:lib/util/shrinksafe/shrinksafe.jar org.mozilla.javascript.tools.shell.Main lib/dojo/dojo.js baseUrl=lib/dojo load=build -p $profile
fi
