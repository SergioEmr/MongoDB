@echo off
REM If you have a Bing Maps key, you can place it here:
set LUCIAD_RIA_BING_KEY=vv84xyk4XhSAm8ytIonS~YjmZD8-cp3X1KVTbM-HUnw~AubzY_7mUwPyuUgu1BUKIMh2yZkY2gCAmktjSdyKkNXO8uvTjUx_Yb5ukiApUnNr

set LIGHTSPEED_HOME=..

REM Find a LuciadLightspeed license in the licenses directory, which is required for the OGC WMS service.

set LICENSEPATH=%LIGHTSPEED_HOME%\licenses;licenses
for %%i in (licenses\*jar) do set LICENSEPATH=%LICENSEPATH%;%%i
for %%i in (%LIGHTSPEED_HOME%\licenses\*jar) do set LICENSEPATH=%LICENSEPATH%;%%i

java -Xmx512m -XX:MaxPermSize=128m -XX:+UseConcMarkSweepGC -Djava.util.logging.config.file=web\samples\resources\logging.properties -cp web\samples\resources\lib\*;%LICENSEPATH%;%LIGHTSPEED_HOME%\samples\classes;%LIGHTSPEED_HOME%\samples\lib\*;%LIGHTSPEED_HOME%\lib\* com.luciad.samples.luciadria.StartService %*
