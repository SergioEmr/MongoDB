#!/bin/sh

# If you have a Bing Maps key, you can place it here:
export LUCIAD_RIA_BING_KEY=vv84xyk4XhSAm8ytIonS~YjmZD8-cp3X1KVTbM-HUnw~AubzY_7mUwPyuUgu1BUKIMh2yZkY2gCAmktjSdyKkNXO8uvTjUx_Yb5ukiApUnNr

LIGHTSPEED_HOME=..

# Find the development license in the licenses directory
LICENSEPATH=${LIGHTSPEED_HOME}/licenses:licenses
for i in ${LIGHTSPEED_HOME}/licenses/*jar; do
  LICENSEPATH=$LICENSEPATH:$i
done
for i in licenses/*jar; do
  LICENSEPATH=$LICENSEPATH:$i
done

java -Xmx512m -XX:MaxPermSize=128m -XX:+UseConcMarkSweepGC -Djava.util.logging.config.file=web/samples/resources/logging.properties -cp web/samples/resources/lib/\*:$LICENSEPATH:${LIGHTSPEED_HOME}/samples/classes:${LIGHTSPEED_HOME}/samples/lib/\*:${LIGHTSPEED_HOME}/lib/\* com.luciad.samples.luciadria.StartService "$@"
