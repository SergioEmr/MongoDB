from flask import request, jsonify
from flask_restful import Resource
from src.model.cndh.cndh_model import Cndh
from src.model.cndh.cndh_response import CndhResponse


class RequestCndh(Resource):
    def post(self):
        data = request.get_json()
        return Cndh.register(data)

    def get(self):
        return jsonify(CndhResponse(Cndh.all()).globalResponse())
