import src.model.cndh.constants as const


class CndhResponse(object):
    def __init__(self, data):
        self.data = data

    def globalResponse(self):
        crs = {"crs": {"properties": {"name": "urn:ogc:def:crs:OGC:1.3:CRS84"}, "type": "name"}}
        feature = {"features": [{"geometry": {"coordinates": [], "type": "Point"},
                                 "properties": {const.FECHA_1: "", const.STATUS: "", const.DENUNCIANTE: "",
                                                const.DIR_DENUNCIANTE: "", const.PARENTEZCO: "",
                                                const.DIR_AGRAVIADO: "", const.TELEFONO: "",
                                                const.CARGO_PERSONA: "", const.AGRAVIADO: "", const.EDAD: 4,
                                                const.SEXO: "", const.NACIONALIDAD: "",
                                                const.AUTORIDADES_RESPONSABLES: "", const.HORA_HECHOS: "",
                                                const.FECHA_HECHOS: "", const.ENTIDAD: "",
                                                const.MUNICIPIO: "", const.LUGAR_HECHOS: "",
                                                const.NUM_PERSONAS: "", const.SUJETOS_ARMAS: "",
                                                const.SUJETOS_UNIFORMADOS: "", const.VEHICULO_ACTIVO: "",
                                                const.DESCRIPCION_VEHICULO: "", const.ACTIVOS_DELINCUENCIA: "",
                                                const.PASIVO_DELINCUENCIA: "", const.NARRACION: ""},
                                 "type": "Feature"}]}
        response = {"crs": crs["crs"], "features": feature["features"]}
        iterable = []
        for document in range(len(self.data)):
            if self.data[document].lat == "espera":
                pass
            else:
                crs = {"crs": {"properties": {"name": "urn:ogc:def:crs:OGC:1.3:CRS84"}, "type": "name"}}
                feature = {"features": [{"geometry": {"coordinates": [], "type": "Point"},
                                         "properties": {const.FECHA_1: "", const.STATUS: "", const.DENUNCIANTE: "",
                                                        const.DIR_DENUNCIANTE: "", const.PARENTEZCO: "",
                                                        const.DIR_AGRAVIADO: "", const.TELEFONO: "",
                                                        const.CARGO_PERSONA: "", const.AGRAVIADO: "", const.EDAD: 4,
                                                        const.SEXO: "", const.NACIONALIDAD: "",
                                                        const.AUTORIDADES_RESPONSABLES: "", const.HORA_HECHOS: "",
                                                        const.FECHA_HECHOS: "", const.ENTIDAD: "",
                                                        const.MUNICIPIO: "", const.LUGAR_HECHOS: "",
                                                        const.NUM_PERSONAS: "", const.SUJETOS_ARMAS: "",
                                                        const.SUJETOS_UNIFORMADOS: "", const.VEHICULO_ACTIVO: "",
                                                        const.DESCRIPCION_VEHICULO: "", const.ACTIVOS_DELINCUENCIA: "",
                                                        const.PASIVO_DELINCUENCIA: "", const.NARRACION: ""},
                                         "type": "Feature"}]}
                response = {"crs": crs["crs"], "features": feature["features"]}
                feature["features"][0]["geometry"]["coordinates"] = [self.data[document].lng,
                                                                     self.data[document].lat]
                feature["features"][0]["properties"][const.FECHA_1] = self.data[document].fecha_1
                feature["features"][0]["properties"][const.STATUS] = self.data[document].status
                feature["features"][0]["properties"][const.DENUNCIANTE] = self.data[document].denunciante
                feature["features"][0]["properties"][const.DIR_DENUNCIANTE] = self.data[document].dir_denunciante
                feature["features"][0]["properties"][const.PARENTEZCO] = self.data[document].parentezco
                feature["features"][0]["properties"][const.DIR_AGRAVIADO] = self.data[document].dir_agraviado
                feature["features"][0]["properties"][const.TELEFONO] = self.data[document].telefono
                feature["features"][0]["properties"][const.CARGO_PERSONA] = self.data[document].cargo_persona
                feature["features"][0]["properties"][const.SEXO] = self.data[document].sexo
                feature["features"][0]["properties"][const.NACIONALIDAD] = self.data[document].nacionalidad
                feature["features"][0]["properties"][const.EDAD] = self.data[document].edad
                feature["features"][0]["properties"][const.AGRAVIADO] = self.data[document].agraviado
                feature["features"][0]["properties"][const.ENTIDAD] = self.data[document].entidad
                feature["features"][0]["properties"][const.MUNICIPIO] = self.data[document].municipio
                feature["features"][0]["properties"][const.AUTORIDADES_RESPONSABLES] = self.data[
                    document].autoridades_responsables
                feature["features"][0]["properties"][const.HORA_HECHOS] = self.data[document].hora_hechos
                feature["features"][0]["properties"][const.FECHA_HECHOS] = self.data[document].fecha_hechos
                feature["features"][0]["properties"][const.LUGAR_HECHOS] = self.data[document].lugar_hechos
                feature["features"][0]["properties"][const.NUM_PERSONAS] = self.data[document].num_personas
                feature["features"][0]["properties"][const.SUJETOS_ARMAS] = self.data[document].sujetos_armas
                feature["features"][0]["properties"][const.SUJETOS_UNIFORMADOS] = self.data[
                    document].sujetos_uniformados
                feature["features"][0]["properties"][const.VEHICULO_ACTIVO] = self.data[document].vehiculo_activo
                feature["features"][0]["properties"][const.DESCRIPCION_VEHICULO] = self.data[
                    document].descripcion_vehiculo
                feature["features"][0]["properties"][const.ACTIVOS_DELINCUENCIA] = self.data[
                    document].activos_delincuencia
                feature["features"][0]["properties"][const.PASIVO_DELINCUENCIA] = self.data[
                    document].pasivo_delincuencia
                feature["features"][0]["properties"][const.NARRACION] = self.data[document].narracion

                iterable.append(*feature["features"])
        response["features"] = iterable
        return response
