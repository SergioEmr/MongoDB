from flask import Blueprint, request, session, url_for, render_template, jsonify
from werkzeug.utils import redirect
from src.model.users.user import User, Token, Private
from src.model.users.token_response import TokenResponse
from src.model.users.private_response import PrivateResponse
import src.model.users.errors as UserErrors
import src.model.users.constants as UserConstants
import src.model.users.decorators as user_decorators
from flask_jwt_extended import (JWTManager, jwt_required, create_access_token, create_refresh_token,
                                jwt_refresh_token_required, get_jwt_identity)
from src.model.users.security import login_user_app_security

user_blueprint = Blueprint('users', __name__)


@user_blueprint.route('/login', methods=['GET', 'POST'])
def login_user():
    if request.method == 'POST':
        email = request.form[UserConstants.EMAIL]
        password = request.form[UserConstants.PASSWORD]
        try:
            if User.is_login_valid(email, password):
                session[UserConstants.EMAIL] = email
                print(session['email'])
                if email == "admin@intelgeo.net":
                    return redirect(url_for('home.intelgeo'))
                if email == "ceac@cns.gob.mx":
                    return redirect(url_for('home.luciad'))
                if email == 's.morales@intelgeo.net':
                    return redirect(url_for('home.user'))
                if email == 'j.molina@intelgeo.net':
                    return redirect(url_for('home.supervisor'))
                if email == 'dev@intelgeo.net':
                    return  redirect(url_for('home.index'))
        except UserErrors.UserError as e:
            return render_template('login.html')
    return render_template('login.html')  # Manda un error por al usuario


@user_blueprint.route('/login_app', methods=['GET', 'POST'])
def login_user_app():
    if request.method == 'POST':
        recive = request.get_json()
        json = login_user_app_security(recive)
        if json['status']:
            access_token = create_access_token(identity=json['user'])
            return jsonify({"access_token": access_token}), 202
        else:
            return jsonify({"access_token": json['msg']}), 202


@user_blueprint.route('/singup', methods=['GET', 'POST'])
def register_user():
    if request.method == 'POST':
        email = request.form[UserConstants.EMAIL]
        password = request.form[UserConstants.PASSWORD]
        token = request.form[UserConstants.TOKEN]
        user_agent = str(request.user_agent)
        try:
            if User.register_user(email, password, token, user_agent):
                Token.register_app_token(email, "denegar", "denegar", "denegar")
                return redirect(url_for('.login_user'))
        except UserErrors.UserError as e:
            return e.message

    return render_template('users/singup.html')  # Manda un error por al usuario


@user_blueprint.route('/register_device', methods=['POST'])
# @user_decorators.request_login
def register_user_device():
    json = request.get_json()
    application = json[UserConstants.APPLICATION]
    token = json[UserConstants.TOKEN]
    email = json[UserConstants.EMAIL]
    password = json[UserConstants.PASSWORD]
    tokendevice = json[UserConstants.TOKEN_DEVICE]
    user_agent = str(request.user_agent)
    try:
        if User.register_user_device(application, token, email, password, tokendevice, user_agent):
            return jsonify({"msg": "Correcto"}), 200
    except UserErrors.UserError as e:
        return e.message


@user_blueprint.route('/register_token', methods=['POST'])
# @user_decorators.request_login
def register_app_token():
    json = request.get_json()
    email = json[UserConstants.EMAIL]
    alert = json[UserConstants.ALERTAPP]
    questionary = json[UserConstants.QUESTIONARYAPP]
    confirm = json[UserConstants.CONFIRMAPP]
    try:
        if Token.register_app_token(email, alert, questionary, confirm):
            return jsonify({"msg": "registrado correctamente"}), 200
    except UserErrors.UserError as e:
        return e.message


@user_blueprint.route('/token_status', methods=['GET'])
# @user_decorators.request_login
def consult_token_status():
    return jsonify(TokenResponse(Token.all()).tokenJsonResponse()), 202


@user_blueprint.route('/get_token', methods=['POST'])
# @user_decorators.request_login
def get_token():
    json = request.get_json()
    email = json[UserConstants.EMAIL]
    try:
        return jsonify(Token.get_token(email)), 202
    except UserErrors.UserError as e:
        return e.message


@user_blueprint.route('/get_status', methods=['POST'])
def get_status():
    json = request.get_json()
    tokendevice = json[UserConstants.TOKEN_DEVICE]
    app = json["app"]
    return jsonify(User.get_status(tokendevice, app))


@user_blueprint.route('/curp_status', methods=['GET'])
# @user_decorators.request_login
def consult_registered_curp():
    return jsonify(PrivateResponse(Private.all()).privateJsonResponse()), 202


@user_blueprint.route('/register_curp', methods=['POST'])
# @user_decorators.request_login
def register_curp():
    email = request.form[UserConstants.EMAIL]
    curp = session.get(UserConstants.EMAIL, 'not set')
    try:
        if Private.register_curp(email, curp):
            return jsonify({"msg": "registrado correctamente"}), 202
    except UserErrors.UserError as e:
        return e.message


@user_blueprint.route('/logout')
def logout_user():
    session['email'] = None
    return redirect(url_for('.login_user'))
