import uuid
import secrets
import datetime
from src.common.database import DataBase
from src.common.utils import Utils
import src.model.users.errors as UserErrors
import src.model.users.constants as UserConstants


class Token(object):
    def __init__(self, email, alertapp, questionaryapp, confirmapp, _id=None):
        self.email = email
        self.alertapp = alertapp
        self.questionaryapp = questionaryapp
        self.confirmapp = confirmapp
        self._id = uuid.uuid4().hex if _id is None else _id

    @staticmethod
    def register_app_token(email, alert, questionary, confirm):
        """
        Registra los token y los usuarios para el accso a las aplicaciones
        :param email: String correo del usuario
        :param alert: String denegado
        :param questionary: String denegado
        :param confirm: String denegado
        :return:
        """
        user_data = DataBase.find_one(UserConstants.COLLECTION_USERS, {"email": email})
        user_token = DataBase.find_one(UserConstants.COLLECTION_TOKEN, {"email": email})

        if user_data is None:
            raise UserErrors.UserNotExistError("Usuario inexistente")
        if user_token is None:
            Token(email, alert, questionary, confirm).save_to_mongo()
        else:

            Token(email,
                  secrets.token_hex(10) if alert == "aprobar" and user_data["alertapp"] == "denegar" else
                  user_token['alertapp'] if alert == "aprobar" else "denegar",
                  secrets.token_hex(10) if questionary == "aprobar" and user_data["questionaryapp"] == "denegar" else
                  user_token['questionaryapp'] if questionary == "aprobar" else "denegar",
                  secrets.token_hex(10) if confirm == "aprobar" and user_data["confirmapp"] == "denegar" else
                  user_token['confirmapp'] if confirm == "aprobar" else "denegar").update_to_mongo()

        User(user_data['email'], user_data['password'], user_data['admin'],
             "denegar" if alert == "denegar" else user_data['alertapp'],
             "denegar" if questionary == "denegar" else user_data['questionaryapp'],
             "denegar" if confirm == "denegar" else user_data['confirmapp'], user_data["first_consult"],
             user_data["last_consult"], user_data["counting"], user_data["user_agent_desktop"],
             user_data["user_agent_mobile"]).update_to_mongo()

        return True

    @staticmethod
    def get_token(email):
        token_data = DataBase.find_one(UserConstants.COLLECTION_TOKEN, {"email": email})
        if token_data is None:
            raise UserErrors.UserNotExistError("Usuario no existe")
        return {UserConstants.ALERTAPP: token_data[UserConstants.ALERTAPP],
                UserConstants.QUESTIONARYAPP: token_data[UserConstants.QUESTIONARYAPP],
                UserConstants.CONFIRMAPP: token_data[UserConstants.CONFIRMAPP]}

    @classmethod
    def all(cls):
        return [cls(**x) for x in DataBase.find(UserConstants.COLLECTION_TOKEN, {})]

    def save_to_mongo(self):
        DataBase.insert(UserConstants.COLLECTION_TOKEN, self.json())

    def update_to_mongo(self):
        DataBase.update(UserConstants.COLLECTION_TOKEN, {UserConstants.EMAIL: self.email}, self.json())

    def json(self):
        return {
            "email": self.email,
            "alertapp": self.alertapp,
            "questionaryapp": self.questionaryapp,
            "confirmapp": self.confirmapp
        }


class Private(object):
    def __init__(self, email, curp, folio, status, _id=None):
        self.email = email
        self.curp = curp
        self.folio = folio
        self.status = status
        self._id = uuid.uuid4().hex if _id is None else _id

    def __repr__(self):
        return "Este es el CURP: {}".format(self.curp)

    @staticmethod
    def register_curp(email, curp):
        user_data = DataBase.find_one(UserConstants.COLLECTION_USERS, {UserConstants.EMAIL: email})
        private_data = DataBase.find_one(UserConstants.COLLECTION_PRIVATE, {UserConstants.EMAIL: email})
        private = DataBase.find_one(UserConstants.COLLECTION_PRIVATE, {UserConstants.CURP: curp})
        if user_data is None:
            raise UserErrors.UserNotExistError("Error")
        if private is not None:
            raise UserErrors.CurpAlreadyExistError("Error")
        if private_data is not None:
            raise UserErrors.CurpAlreadyExistError("CURP ya registrado")
        Private(email, curp, "espera", "espera").save_to_mongo()
        return True

    def save_to_mongo(self):
        DataBase.insert(UserConstants.COLLECTION_PRIVATE, self.json())

    def update_to_mongo(self):
        DataBase.update(UserConstants.COLLECTION_PRIVATE, {UserConstants.EMAIL: self.email}, self.json())

    @classmethod
    def all(cls):
        return [cls(**x) for x in DataBase.find(UserConstants.COLLECTION_PRIVATE, {})]

    def json(self):
        return {
            "email": self.email,
            "curp": self.curp,
            "folio": self.folio,
            "status": self.status
        }


class User(object):
    def __init__(self, email, password, clv, alertapp, questionaryapp, confirmapp, first_consult, last_consult,
                 counting, user_agent_desktop, user_agent_mobile, _id=None):
        self.clv = clv
        self.email = email
        self.password = password
        self.alertapp = alertapp
        self.questionaryapp = questionaryapp
        self.confirmapp = confirmapp
        self.first_consult = first_consult
        self.last_consult = last_consult
        self.counting = counting
        self.user_agent_desktop = user_agent_desktop
        self.user_agent_mobile = user_agent_mobile
        self._id = uuid.uuid4().hex if _id is None else _id

    def __repr__(self):
        return "Este es tu correo: {}".format(self.email)

    @staticmethod
    def get_status(tokendevice, app):
        token = DataBase.find_one(UserConstants.COLLECTION_USERS, {app: tokendevice})
        print(token)
        if token is None:
            return {"msg": "sin asignar"}
        return {"msg": "asignado"}

    @staticmethod
    def is_login_valid(email, password):
        """
        Verifica la existencia del email dentro de la base de datos
        Valida la combinación de email/password para iniciar sesión
        :param email: email del usuario
        :param password: hash sha512 del password
        :return: True si es valido, False si son invalidos
        """
        user_data = DataBase.find_one(UserConstants.COLLECTION_USERS, {"email": email})
        current = datetime.datetime.utcnow()

        if user_data is None:
            #  Se le indica al usuario que esa cuenta no existe
            raise UserErrors.UserNotExistError("error")

        # Si el tiempo transcurrido entra la última consulta y la primera después de 10 intentos fallidos es
        #  mayor a 5 minutos (300 segundos) terminar la busqueda, de otra forma reinicia el contador
        if (user_data["counting"] >= 10) and (
                        user_data["last_consult"].second - user_data["first_consult"].second < 300):
            waiting = current - user_data["last_consult"]
            print(waiting.seconds)
            if waiting.seconds < 20:
                raise UserErrors.MaximumLoginAttempts("Máximo número de intentos")
            else:
                print("else")
                User(user_data["email"], user_data["password"], user_data["admin"], user_data["alertapp"],
                     user_data["questionaryapp"], user_data["confirmapp"], "espera", "espera", 0,
                     user_data["user_agent_desktop"], user_data["user_agent_mobile"]).update_to_mongo()

            user_data = DataBase.find_one(UserConstants.COLLECTION_USERS, {"email": email})

        # Si existe, recibe la contraseña y hashea la contraseña
        if not Utils.check_string(password, user_data['password']):
            User(user_data["email"], user_data["password"], user_data["admin"], user_data["alertapp"],
                 user_data["questionaryapp"], user_data["confirmapp"],
                 current if user_data["first_consult"] == "espera" else user_data["first_consult"],
                 current if user_data["counting"] == 9 else user_data["last_consult"],
                 user_data["counting"] + 1, user_data["user_agent_desktop"],
                 user_data["user_agent_mobile"]).update_to_mongo()

            raise UserErrors.IncorrectPasswordError("error")
        return True

    @staticmethod
    def is_token_device_valid(app, email, tokendevice):
        """
        Este método certifica que el correo le pertenece al token-device que trata de iniciar sesión en la aplicación
        Android.
        :param email: String correo del usuario,
        :param tokendevice: String token-device único para cada dispositivo registrado en la plataforma.
        :return: True si es exitoso, False si falló
        """
        user_data = DataBase.find_one(UserConstants.COLLECTION_USERS, {"email": email})
        if user_data is None:
            #  Se le indica al usuario que esa cuenta no existe
            raise UserErrors.UserNotExistError("error")
            #  Si existe, recibe la contraseña y hashea la contraseña
        if not Utils.check_string(tokendevice, user_data[app]):
            raise UserErrors.IncorrectTokenDevice("Dispositivo incorrecto")
        return True

    @staticmethod
    def register_user(email, password, token, user_agent):
        """
        Este método registra a un nuevo usuario con su correo, contraseña y token de la empresa para el acceso a la plataforma
        :param email: String correo del usuarios, debe ser valido y verifica que no haya sido registrado previamente
        :param password: String contraseña del usuario
        :return: True si fue exitoso, False si falló
        """
        user_data = DataBase.find_one(UserConstants.COLLECTION_USERS, {'email': email})
        user_admin = DataBase.find_one(UserConstants.COLLECTION_ADMINS, {'token': token})
        alertapp = "denegar"
        questionaryapp = "denegar"
        confirmapp = "denegar"
        first_consult = "espera"
        last_consult = "espera"
        counting = 0

        if user_admin is None:
            raise UserErrors.InvalidTokenError("Token incorrecto")
        if user_data is not None:
            # Indicarle que el usuario ya se encuentra registrado
            raise UserErrors.UserAlreadyExistError("Correo ya registrado")
        if not Utils.email_valid(email):
            raise UserErrors.InvalidEmailError("El correo no tiene el formato correcto")

        User(email, password, user_admin[UserConstants.ADMIN_ID], alertapp, questionaryapp,
             confirmapp, first_consult, last_consult, counting, user_agent, "wait").save_to_mongo()

        return True

    @staticmethod
    def register_user_device(app, token, email, password, tokendevice, user_agent):
        """
        Este método registra el tokendevice del usuario y habilita la aplicación para la cual el token de entrada
        tiene efecto.
        :param app: String Nombre del paquete de la aplicación a la cual se está tratando de registrar el tokendevice
        :param token: String Token generado por el administrador para ese usuario
        :param email: String correo del usuario ingresado en la aplicación
        :param password: String contraseña del usuario para el correo indicado
        :param tokendevice: String tokendevice del dispositivo al que se trata de asociar la cuenta
        :return:
        """
        user_data = DataBase.find_one(UserConstants.COLLECTION_USERS, {"email": email})
        authorization = DataBase.find_one(UserConstants.COLLECTION_TOKEN, {"email": email})
        check_alert = DataBase.find_one(UserConstants.COLLECTION_USERS, {"alertapp": tokendevice})
        check_questionary = DataBase.find_one(UserConstants.COLLECTION_USERS, {"questionaryapp": tokendevice})
        check_confirm = DataBase.find_one(UserConstants.COLLECTION_USERS, {"confirmapp": tokendevice})
        lista = [*user_data]
        # Verifica que el token device no haya sido utilizado con otra cuenta de correo.
        if check_alert is not None:
            if not Utils.check_string(email, check_alert["email"]):
                raise UserErrors.TokenDeviceAlreadyExist("Dipositivo ya registrado")
        if check_questionary is not None:
            if not Utils.check_string(email, check_questionary["email"]):
                raise UserErrors.TokenDeviceAlreadyExist("Dipositivo ya registrado")
        if check_confirm is not None:
            if not Utils.check_string(email, check_confirm["email"]):
                raise UserErrors.TokenDeviceAlreadyExist("Dispositivo ya registrado")
        # Verifica si el correo se encuentra en la base de datos Token
        if authorization is None:
            raise UserErrors.UserNotExistError("Error")
        # Verifica que se tiene acceso a la aplicación
        if Utils.check_token(authorization[app]):
            raise UserErrors.InvalidTokenError("Token invalido")
        # Verifica que el token ingresado corresponde a la aplicación
        if not Utils.check_string(token, authorization[app]):
            raise UserErrors.InvalidTokenError(Utils.check_app(token, authorization[app]))
        # Verifica que el correo se encuentra en la base de datos de usuarios
        if user_data is None:
            #  Se le indica al usuario que esa cuenta no existe
            raise UserErrors.UserNotExistError("error")
            #  Si existe, recibe la contraseña y hashea la contraseña
        if not Utils.check_string(password, user_data['password']):
            raise UserErrors.IncorrectPasswordError("error")

        User(user_data['email'], user_data['password'], user_data['admin'],
             tokendevice if lista.index(app) == 4 else user_data['alertapp'],
             tokendevice if lista.index(app) == 5 else user_data['questionaryapp'],
             tokendevice if lista.index(app) == 6 else user_data['confirmapp'],
             user_data["first_consult"], user_data["last_consult"], user_data["counting"],
             user_data["user_agent_desktop"], user_agent).update_to_mongo()
        user = DataBase.find_one(UserConstants.COLLECTION_USERS, {"email": email})

        Token(user_data['email'], "autorizado" if user['alertapp'] != "denegar" else authorization.get("alertapp"),
              "autorizado" if user["questionaryapp"] != "denegar" else authorization.get("questionaryapp"),
              "autorizado" if user['confirmapp'] != "denegar" else authorization.get("confirmapp")).update_to_mongo()

        return True

    def save_to_mongo(self):
        DataBase.insert(UserConstants.COLLECTION_USERS, self.json())

    def update_to_mongo(self):
        DataBase.update(UserConstants.COLLECTION_USERS, {UserConstants.EMAIL: self.email}, self.json())

    def json(self):
        return {
            UserConstants.EMAIL: self.email,
            UserConstants.PASSWORD: self.password,
            UserConstants.ADMIN: self.clv,
            UserConstants.ALERTAPP: self.alertapp,
            UserConstants.QUESTIONARYAPP: self.questionaryapp,
            UserConstants.CONFIRMAPP: self.confirmapp,
            UserConstants.FIRSTCONSULT: self.first_consult,
            UserConstants.LASTCONSULT: self.last_consult,
            UserConstants.COUNTING: self.counting,
            UserConstants.USER_AGENT_DESKTOP: self.user_agent_desktop,
            UserConstants.USER_AGENT_MOBILE: self.user_agent_mobile
        }
