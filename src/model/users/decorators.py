from functools import wraps
from flask import session, redirect, url_for, request, flash


def request_login(f):
    @wraps(f)
    def decorator_function(*args, **kwargs):
        if 'email' not in session.keys() or session['email'] is None:
            flash(u'Es necesario iniciar sesión para poder acceder')
            return redirect(url_for('users.login_user', next=request.path))
        return f(*args, **kwargs)

    return decorator_function
