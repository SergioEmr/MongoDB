class TokenResponse(object):
    def __init__(self, data):
        self.data = data

    def tokenJsonResponse(self):
        iterable = []
        for document in range(len(self.data)):
            users = {"user": "user", "alertapp": "alertapp", "questionaryapp": "questionaryapp",
                     "confirmapp": "confirmapp"}
            if (self.data[document].email == 'ceac@cns.gob.mx') or (self.data[document].email == 'admin@intelgeo.net') \
                    or (self.data[document].email == 'dev@intelgeo.net'):
                continue
            users["user"] = self.data[document].email
            users["alertapp"] = self.data[document].alertapp
            users["questionaryapp"] = self.data[document].questionaryapp
            users["confirmapp"] = self.data[document].confirmapp
            iterable.append(users)
        response = {"users": iterable}
        return response
