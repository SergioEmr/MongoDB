from src.model.users.user import User
import src.model.users.errors as UserErrors


def login_user_app_security(json):
    app = json['app']
    email = json['email']
    password = json['password']
    token_device = json['tokendevice']
    try:
        if User.is_login_valid(email, password):
            if User.is_token_device_valid(app=app, email=email, tokendevice=token_device):
                return {"status": True, "user": email}
    except UserErrors.UserError as e:
        return {"status": False, "msg": e.message}
