class PrivateResponse(object):
    def __init__(self, data):
        self.data = data

    def privateJsonResponse(self):
        iterable = []
        for document in range(len(self.data)):
            users = {"user": "user", "curp": "curp", "status": "status"}
            users["user"] = self.data[document].email
            users["curp"] = self.data[document].curp
            users["folio"] = self.data[document].folio
            users["status"] = self.data[document].status
            iterable.append(users)
        response = {"users": iterable}
        return response
