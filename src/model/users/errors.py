class UserError(Exception):
    def __init__(self, message):
        self.message = message


class UserNotExistError(UserError):
    pass

class CurpAlreadyExistError(UserError):
    pass


class CurpNotExistError(UserError):
    pass


class CurpAlreadyAssigned(UserError):
    pass


class IncorrectPasswordError(UserError):
    pass


class IncorrectTokenDevice(UserError):
    pass


class TokenDeviceAlreadyExist(UserError):
    pass


class UserAlreadyExistError(UserError):
    pass


class InvalidEmailError(UserError):
    pass


class InvalidTokenError(UserError):
    pass

class MaximumLoginAttempts(UserError):
    pass