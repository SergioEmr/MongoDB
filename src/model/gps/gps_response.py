import src.model.gps.constants as const


class GpsResponse(object):
    def __init__(self, data):
        self.data = data

    def gpsJsonResponse(self):
        iterable = []
        response = {"crs": {"properties": {"name": "urn:ogc:def:crs:OGC:1.3:CRS84"}, "type": "name"},
                    "type": "FeatureCollection", "features": iterable}
        for document in range(len(self.data)):
            features = {
                "id": "id",
                "type": "type",
                "properties": {},
                "geometry": {"type": "Point", "coordinates": []}
            }
            print(self.data)
            features["geometry"]["coordinates"] = [self.data[document].lng, self.data[document].lat,
                                                   self.data[document].alt]
            features["id"] = self.data[document].identificador
            features["properties"][const.VELOCIDAD] = self.data[document].velocidad
            features["properties"][const.FECHA] = self.data[document].fecha
            features["properties"][const.HORA] = self.data[document].hora
            iterable.append(features)
        response["features"] = iterable

        return response
