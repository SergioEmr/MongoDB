from flask import Blueprint, request, jsonify
from src.model.gps.gps import GpsDevice
from src.model.gps.gps_response import GpsResponse

gps_blueprint = Blueprint('gps', __name__)


@gps_blueprint.route('/gps', methods=['GET', 'POST'])
def gps_sent():
    if request.method == 'POST':
        json = request.get_json()
        GpsDevice.register_device(json)
        return jsonify({"msg": "correctamente enviado"}), 202

    if request.method == 'GET':
        return jsonify(GpsResponse(GpsDevice.all()).gpsJsonResponse()), 202
