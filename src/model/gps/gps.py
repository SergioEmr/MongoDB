import uuid
from src.common.database import DataBase
import src.model.gps.constants as const


class GpsDevice(object):
    def __init__(self, lat, lng, alt, fecha, hora, velocidad, identificador, _id=None):
        self.lat = lat
        self.lng = lng
        self.alt = alt
        self.fecha = fecha
        self.hora = hora
        self.velocidad = velocidad
        self.identificador = identificador
        self._id = uuid.uuid4().hex if _id is None else _id

    @staticmethod
    def save_GPS(data):
        GpsDevice(data[const.LAT], data[const.LNG], data[const.ALT], data[const.FECHA], data[const.HORA],
                  data[const.VELOCIDAD], data[const.ID]).save_to_mongo()
        return True

    @staticmethod
    def register_device(json):
        data = DataBase.find_one(const.COLLECTION_GPS, {const.ID: json[const.ID]})
        if data is None:
            GpsDevice(json[const.LAT], json[const.LNG], json[const.ALT], json[const.FECHA], json[const.HORA],
                      json[const.VELOCIDAD], json[const.ID]).save_to_mongo()
        else:
            GpsDevice(json[const.LAT], json[const.LNG], json[const.ALT], json[const.FECHA], json[const.HORA],
                      json[const.VELOCIDAD], json[const.ID]).update_to_mongo()

    def save_to_mongo(self):
        DataBase.insert(const.COLLECTION_GPS, self.json())

    def update_to_mongo(self):
        DataBase.update(const.COLLECTION_GPS, {const.ID: self.identificador}, self.json())

    @classmethod
    def one(cls):
        return DataBase.find_one(const.COLLECTION_GPS, {})

    @classmethod
    def all(cls):
        return [cls(**x) for x in DataBase.find(const.COLLECTION_GPS, {})]

    def json(self):
        return {
            const.LAT: self.lat,
            const.LNG: self.lng,
            const.ALT: self.alt,
            const.FECHA: self.fecha,
            const.HORA: self.hora,
            const.VELOCIDAD: self.velocidad,
            const.ID: self.identificador
        }
