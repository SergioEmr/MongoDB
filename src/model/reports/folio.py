import uuid
from src.common.database import DataBase
import src.model.reports.constants as ReportConstants


class Folio(object):
    def __init__(self, email, folio, _id=None):
        self.email = email
        self.folio = folio
        self._id = uuid.uuid4().hex if _id is None else _id

    @staticmethod
    def register_folio(email, folio):
        email = email
        folio = folio
        folio_data = DataBase.find_one(ReportConstants.COLLECTION_FOLIO, {ReportConstants.FOLIO: folio})

        if folio_data is None:
            Folio(email, folio).save_to_mongo()
        else:
            Folio(email, folio).update_to_mongo()

    def save_to_mongo(self):
        DataBase.insert(ReportConstants.COLLECTION_FOLIO, self.json())

    def update_to_mongo(self):
        DataBase.update(ReportConstants.COLLECTION_FOLIO, {ReportConstants.FOLIO: self.folio}, self.json())

    @classmethod
    def all(cls):
        return [cls(**x) for x in DataBase.find(ReportConstants.COLLECTION_FOLIO, {})]

    def json(self):
        return {
            ReportConstants.EMAIL: self.email,
            ReportConstants.FOLIO: self.folio,
        }


class Cambio(object):
    def __init__(self, folio2, status2):
        self.folio2 = folio2
        self.status2 = status2

    @staticmethod
    def consult(folio2):
        consult_data = DataBase.find_one(ReportConstants.COLLECTION_FOLIO, {ReportConstants.FOLIO: folio2})

        if consult_data['status'] == "conectado":
            return {"msg": "conectado"}
        else:
            return {"msg": "desconectado"}

    @staticmethod
    def update_status(folio2, status2):
        folio_data = DataBase.find_one(ReportConstants.COLLECTION_FOLIO, {ReportConstants.FOLIO: folio2})

        if folio_data is None:
            return False
        else:
            Cambio(folio2, status2).update_to_mongo2()

    def update_to_mongo2(self):
        DataBase.update(ReportConstants.COLLECTION_FOLIO, {ReportConstants.FOLIO: self.folio2}, self.json2())

    def json2(self):
        return {
            ReportConstants.FOLIO: self.folio2,
            ReportConstants.STATUS: self.status2
        }
