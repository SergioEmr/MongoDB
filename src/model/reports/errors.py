class ReportError(Exception):
    def __init__(self, message):
        self.message = message


class FolioNotExistError(ReportError):
    pass


class FolioAlreadyExistError(ReportError):
    pass


class FolioAlreadyUpdateError(ReportError):
    pass


class Folio088Wait(ReportError):
    pass


class FolioPending(ReportError):
    pass


class Folio911Wait(ReportError):
    pass


class FolioStatusFail(ReportError):
    pass


class CurpNotExistError(ReportError):
    pass


class CurpAlreadyAssignedError(ReportError):
    pass


class ApplicationDeniedError(ReportError):
    pass
