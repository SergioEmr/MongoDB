import uuid
from src.common.database import DataBase
import src.model.reports.constants as ReportConstants
import src.model.reports.errors as ReportErrors
from src.model.users.user import Private


class Report(object):
    def __init__(self, alerta, averiguaci, carga, conductor, copiloto, curp1, curp2, denuncia, efectivo, empresa,
                 entidad,
                 fecha, fllegada, hllegada, fsalida, hsalida, giro, hora, lat, lng, merca, modelo, modus,
                 municipio, organizaci, peso, placas, ruta, tipo, tramo, tvehiculo, folio, status, fuero, _id=None):
        self.alerta = alerta
        self.averiguaci = averiguaci
        self.carga = carga
        self.conductor = conductor
        self.copiloto = copiloto
        self.curp1 = curp1
        self.curp2 = curp2
        self.denuncia = denuncia
        self.efectivo = efectivo
        self.empresa = empresa
        self.entidad = entidad
        self.fecha = fecha
        self.fllegada = fllegada
        self.hllegada = hllegada
        self.fsalida = fsalida
        self.hsalida = hsalida
        self.giro = giro
        self.hora = hora
        self.lat = lat
        self.lng = lng
        self.merca = merca
        self.modelo = modelo
        self.modus = modus
        self.municipio = municipio
        self.organizaci = organizaci
        self.peso = peso
        self.placas = placas
        self.ruta = ruta
        self.tipo = tipo
        self.tramo = tramo
        self.tvehiculo = tvehiculo
        self.folio = folio
        self.status = status
        self.fuero = fuero
        self._id = uuid.uuid4().hex if _id is None else _id

    def __repr__(self):
        return "Esta es la ruta: {}".format(self.ruta)

    @staticmethod
    def register_report(data):
        folio = DataBase.find_one(ReportConstants.COLLECTION_REPORTS, {"folio": data["folio"]})
        privado = DataBase.find_one(ReportConstants.COLLECTION_PRIVATE, {"curp": data["curp"]})
        if folio is not None:
            raise ReportErrors.FolioAlreadyExistError("Folio ya creado")
        if privado is None:
            raise ReportErrors.CurpNotExistError("CURP invalido")
        if privado["folio"] != "espera":
            raise ReportErrors.CurpAlreadyAssignedError("CURP ya asignado")
        else:
            Report(data["folio"], data["status"], data["curp"], data["transportista"], data["camion"], data["placa"],
                   data["carga"], data["marca"], data["valor"], data["remolques"], data["ruta"], data["fecha_salida"],
                   data["fecha_llegada"], data["hora_salida"], data["hora_llegada"], ReportConstants.ESPERA,
                   ReportConstants.ESPERA, ReportConstants.ESPERA, ReportConstants.ESPERA, ReportConstants.ESPERA,
                   ReportConstants.ESPERA, ReportConstants.ESPERA,
                   ReportConstants.ESPERA, 'espera', 'espera', 'espera', 'espera', 'espera', 'espera', 'espsera',
                   'espera', 'espera', 'espera', 'espera').save_to_mongo()
            Private(privado["email"], privado["curp"], data["folio"], "asignado").update_to_mongo()
        return True

    @staticmethod
    def registro(carga, conductor, copiloto, curp1, curp2, efectivo, fllegada, hllegada, fsalida, hsalida, merca,
                 modelo, peso, placas, ruta, tvehiculo, folio):
        Report('espera', 'espera', carga, conductor, copiloto, curp1, curp2, 'espera',
               efectivo, 'IntelGeo', 'espera', 'espera', fllegada, hllegada, fsalida,
               hsalida, 'Consulta', 'espera', 'espera', 'espera', merca, modelo, 'espera',
               'espera', 'espera', peso, placas, ruta, 'espera', 'espera', tvehiculo,
               folio, 'espera', 'espera').save_to_mongo()

    @classmethod
    def find_by_id(cls, folio):
        private_data = DataBase.find_one(ReportConstants.COLLECTION_PRIVATE, {ReportConstants.FOLIO: folio})
        Private(private_data["email"], private_data["curp"], "espera", "espera").update_to_mongo()
        return cls(**DataBase.find_one(ReportConstants.COLLECTION_REPORTS, {'folio': folio}))

    @staticmethod
    def confirmar(carpeta, denuncia, fecha, hora, lat, lng, modus, organizacion, tipo, tramo, num, fuero):
        folio = DataBase.find_one('Alerts', {"folio": num})
        if folio is None:
            print('inexistente')
            return 'Folio inexistente'
        if folio['status'] == "confirmado":
            print('confirmado')
            return 'Folio ya utilizado'
        if folio['status'] == 'alerta':
            print('alertado')
            Report(folio['alerta'], carpeta, folio['carga'], folio['conductor'], folio['copiloto'], folio['curp1'],
                   folio['curp2'], denuncia, folio['efectivo'], folio['empresa'], 'Ciudad de México', fecha,
                   folio['fllegada'], folio['hllegada'], folio['fsalida'], folio['hsalida'], folio['giro'], hora,
                   lat, lng, folio['merca'], folio['modelo'], modus, 'Benito Juarez',
                   organizacion, folio['peso'], folio['placas'], folio['ruta'], tipo, tramo,
                   folio['tvehiculo'], folio['folio'], 'confirmado', fuero).update_to_mongo()

    @staticmethod
    def confirm_report(data):

        folio = DataBase.find_one(ReportConstants.COLLECTION_REPORTS, {"folio": data["folio"]})
        private_data = DataBase.find_one(ReportConstants.COLLECTION_PRIVATE, {ReportConstants.CURP: folio["curp"]})

        if folio is None:
            raise ReportErrors.FolioNotExistError("Folio inexistente")

        if folio["status"] == "confirmado":
            raise ReportErrors.FolioAlreadyUpdateError("Folio ya utilizado")

        if (data["reporte"] == "pendiente") and (folio["folio088"] == "espera") and (folio["folio911"] == "espera"):
            status = "pendiente"
            f088 = folio["folio088"]
            f911 = folio["folio911"]
        elif (data["reporte"] == "088") and (folio["folio088"] == "espera"):
            status = "911" if folio["folio911"] == "espera" else "confirmado"
            f088 = data["folio088"]
            f911 = folio["folio911"]
        elif (data["reporte"] == "911") and (folio["folio911"] == "espera"):
            status = "088" if folio["folio088"] == "espera" else "confirmado"
            f088 = folio["folio088"]
            f911 = data["folio911"]

        elif data["reporte"] == "completo":
            status = "confirmado"
            f088 = data["folio088"]
            f911 = data["folio911"]
        else:
            raise ReportErrors.FolioStatusFail("Status incorrecto")

        Report(folio["folio"], status, folio["curp"], folio["transportista"], folio["camion"],
               folio["placa"], folio["carga"], folio["marca"], folio["valor"], folio["remolques"], folio["ruta"],
               folio["fecha_salida"], folio["fecha_llegada"], folio["hora_salida"], folio["hora_llegada"],
               data["fecha"] if folio["status"] == "espera" else folio["fecha_incidente"],
               data["hora"] if folio["status"] == "espera" else folio["hora_incidente"],
               data["ubicacion"] if folio["status"] == "espera" else folio["ubicacion"],
               data["lat"] if folio["status"] == "espera" else folio["lat"],
               data["lon"] if folio["status"] == "espera" else folio["lon"],
               data["tipo"] if folio["status"] == "espera" else folio["tipo"],
               f088, f911, 'espera', 'espera', 'espera', 'espera', 'espera', 'espera', 'espsera', 'espera',
               'espera', 'espera', 'espera').update_to_mongo()

        Private(private_data["email"], private_data["curp"], "espera", "espera").update_to_mongo()

        return True

    @staticmethod
    def status_folio(folio):
        data_folio = DataBase.find_one(ReportConstants.COLLECTION_REPORTS, {"folio": folio})

        if data_folio is None:
            raise ReportErrors.FolioNotExistError("Folio inexistente")
        if data_folio["status"] == "confirmado":
            raise ReportErrors.FolioAlreadyUpdateError("Folio ya utilizado")
        if data_folio["status"] == "pendiente":
            raise ReportErrors.FolioPending("pendiente")
        if data_folio["status"] == "088":
            raise ReportErrors.Folio088Wait("088")
        if data_folio["status"] == "911":
            raise ReportErrors.Folio911Wait("911")
        if data_folio["status"] == "espera":
            raise ReportErrors.FolioPending("espera")

        return True

    @classmethod
    def all(cls):
        return [cls(**x) for x in DataBase.find('Alerts', {})]

    def save_to_mongo(self):
        DataBase.insert('Alerts', self.json())

    def update_to_mongo(self):
        DataBase.update('Alerts', {ReportConstants.FOLIO: self.folio}, self.json())

    def remove_to_mongo(self):
        DataBase.remove('Alerts', {ReportConstants.FOLIO: self.folio})

    def json(self):
        return {
            'alerta': self.alerta,
            'averiguaci': self.averiguaci,
            'carga': self.carga,
            'conductor': self.conductor,
            'copiloto': self.copiloto,
            'curp1': self.curp1,
            'curp2': self.curp2,
            'denuncia': self.denuncia,
            'efectivo': self.efectivo,
            'empresa': self.empresa,
            'entidad': self.entidad,
            'fecha': self.fecha,
            'fllegada': self.fllegada,
            'hllegada': self.hllegada,
            'fsalida': self.fsalida,
            'hsalida': self.hsalida,
            'giro': self.giro,
            'hora': self.hora,
            'lat': self.lat,
            'lng': self.lng,
            'merca': self.merca,
            'modelo': self.modelo,
            'modus': self.modus,
            'municipio': self.municipio,
            'organizaci': self.organizaci,
            'peso': self.peso,
            'placas': self.placas,
            'ruta': self.ruta,
            'tipo': self.tipo,
            'tramo': self.tramo,
            'tvehiculo': self.tvehiculo,
            'folio': self.folio,
            'status': self.status,
            'fuero': self.fuero
        }
