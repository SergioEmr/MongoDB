class FolioResponse(object):
    def __init__(self, data):
        self.data = data

    def folioJsonResponse(self):
        crs = {"crs": {"properties": {"name": "urn:ogc:def:crs:OGC:1.3:CRS84"}, "type": "name"}}
        feature = {"features": [{"geometry": {"coordinates": [], "type": "Point"},
                                 "properties": {"Folio": "Folio", "Status": "espera", "CURP": "curp",
                                                "Transportista": "transportista", "Camión": "camion", "Placa": "placa",
                                                "Carga": "carga", "Marca": "marca", "Valor": "valor",
                                                "Remolques": "remolques", "Ruta": "ruta",
                                                "Fecha de salida": "fecha_salida", "Fecha de llegada": "fecha_llegada",
                                                "Hora de salida": "hora_salida", "Hora de llegada": "hora_llegada",
                                                "Fecha de incidente": "10/12/2018", "Hora de incidente": "15:34",
                                                "Ubicación": "ubicación", "Tipo": "tipo", "Folio 088": "folio 088",
                                                "Folio 911": "folio 911"},
                                 "type": "Feature"}]}
        response = {"crs": crs["crs"], "features": feature["features"]}
        iterable = []
        for document in range(len(self.data)):
            if self.data[document].lat == "espera":
                pass
            else:
                feature = {"features": [{"geometry": {"coordinates": [], "type": "Point"},
                                         "properties": {"Folio": "Folio", "Status": "espera", "CURP": "curp",
                                                        "Transportista": "transportista", "Camión": "camion",
                                                        "Placa": "placa", "Carga": "carga", "Marca": "marca",
                                                        "Valor": "valor", "Remolques": "remolques", "Ruta": "ruta",
                                                        "Fecha de salida": "fecha_salida",
                                                        "Fecha de llegada": "fecha_llegada",
                                                        "Hora de salida": "hora_salida",
                                                        "Hora de llegada": "hora_llegada",
                                                        "Fecha de incidente": "10/12/2018",
                                                        "Hora de incidente": "15:34",
                                                        "Ubicación": "ubicación", "Tipo": "tipo",
                                                        "Folio 088": "folio 088",
                                                        "Folio 911": "folio 911"},
                                         "type": "Feature"}]}
                response = {"crs": crs["crs"], "features": feature["features"]}
                feature["features"][0]["geometry"]["coordinates"] = [self.data[document].lng,
                                                                     self.data[document].lat]
                feature["features"][0]["properties"]["id"] = document
                feature["features"][0]["properties"]["Folio"] = self.data[document].folio
                feature["features"][0]["properties"]["Status"] = self.data[document].status
                feature["features"][0]["properties"]["CURP"] = self.data[document].curp
                feature["features"][0]["properties"]["Transportista"] = self.data[document].transportista
                feature["features"][0]["properties"]["Camíón"] = self.data[document].camion
                feature["features"][0]["properties"]["Placa"] = self.data[document].placa
                feature["features"][0]["properties"]["Carga"] = self.data[document].carga
                feature["features"][0]["properties"]["Marca"] = self.data[document].marca
                feature["features"][0]["properties"]["Valor"] = self.data[document].valor
                feature["features"][0]["properties"]["Remolques"] = self.data[document].remolques
                feature["features"][0]["properties"]["Ruta"] = self.data[document].ruta
                feature["features"][0]["properties"]["Fecha de salida"] = self.data[document].fecha_salida
                feature["features"][0]["properties"]["Fecha de llegada"] = self.data[document].fecha_llegada
                feature["features"][0]["properties"]["Hora de salida"] = self.data[document].hora_salida
                feature["features"][0]["properties"]["Hora de llegada"] = self.data[document].hora_llegada
                feature["features"][0]["properties"]["Fecha de incidente"] = self.data[document].fecha_incidente
                feature["features"][0]["properties"]["Hora de incidente"] = self.data[document].hora_incidente
                feature["features"][0]["properties"]["Ubicación"] = self.data[document].ubicacion
                feature["features"][0]["properties"]["Tipo"] = self.data[document].tipo
                feature["features"][0]["properties"]["Folio 088"] = self.data[document].folio088
                feature["features"][0]["properties"]["Folio 911"] = self.data[document].folio911
                iterable.append(*feature["features"])
        response["features"] = iterable
        return response

    def pendienteresponse(self):
        activos = []
        curp = []
        status = []
        for x in range(len(self.data)):
            if self.data[x].tipo == "espera":
                activos.append(self.data[x].folio)
                curp.append(self.data[x].curp1)
                status.append("espera")
        return {"pendientes": activos, "curps": curp, "status": status}

    def folio_response(self):
        response = {}
        for x in range(len(self.data)):
            if (self.data[x].lat == "espera") or (self.data[x].averiguaci != "espera"):
                continue
            response.update({self.data[x].folio: {"placa": self.data[x].placas, "conductor": self.data[x].curp1,
                                                  "copiloto": self.data[x].curp2,
                                                  "salida": self.data[x].fsalida + self.data[x].hsalida,
                                                  "llegada": self.data[x].fllegada + self.data[x].hllegada,
                                                  "fecha": self.data[x].fecha, "hora": self.data[x].hora,
                                                  "carpeta": self.data[x].averiguaci, "carga": self.data[x].carga,
                                                  "peso": self.data[x].peso, "valor": self.data[x].merca,
                                                  "ruta": self.data[x].ruta, "alerta": self.data[x].alerta,
                                                  "tipo": self.data[x].tipo, "denunciante": self.data[x].denuncia,
                                                  "organizacion": self.data[x].organizaci, "fuero": self.data[x].fuero,
                                                  "vehiculo": self.data[x].tvehiculo,
                                                  "coordenadas": [self.data[x].lat, self.data[x].lng]}})
        return response
