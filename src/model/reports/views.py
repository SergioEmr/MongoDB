from flask import Blueprint, request, jsonify
from src.model.gps.gps_response import GpsResponse
from src.model.reports.folio import Folio
from src.model.reports.report import Report
import src.model.reports.errors as ReportErrors
from src.model.reports.folio_response import FolioResponse
import src.model.users.decorators as user_decorators
from flask_jwt_extended import (JWTManager, jwt_required)

report_blueprint = Blueprint('reports', __name__)


@report_blueprint.route('/register', methods=['GET', 'POST'])
# @user_decorators.request_login
def register_report_web():
    if request.method == 'POST':
        json = request.get_json()

        try:
            if Report.register_report(json):
                return jsonify({'msg': "Registrado"}), 202
        except ReportErrors.ReportError as e:
            return e.message


@report_blueprint.route('/registro', methods=['POST'])
def registro():
    if request.method == "POST":
        carga = request.form['carga']
        conductor = request.form['conductor']
        copiloto = request.form['copiloto']
        curp1 = request.form['curp_1']
        curp2 = request.form['curp_2']
        efectivo = request.form['efectivo']
        fllegada = request.form['fllegada']
        hllegada = request.form['hllegada']
        fsalida = request.form['fsalida']
        hsalida = request.form['hsalida']
        merca = request.form['valor']
        modelo = request.form['modelo']
        peso = request.form['peso']
        placas = request.form['placas']
        ruta = request.form['ruta']
        tvehiculo = request.form['tvehiculo']
        folio = request.form['folio']
        print('consultado')
        Report.registro(carga, conductor, copiloto, curp1, curp2, efectivo, fllegada, hllegada, fsalida, hsalida,
                        merca, modelo, peso, placas, ruta, tvehiculo, folio)
        return 'Registrado correctamente'


@report_blueprint.route('/delito', methods=['POST'])
def confirmar():
    if request.method == 'POST':
        num = request.form['folio']
        carpeta = request.form['carpeta']
        denuncia = request.form['denunciante']
        fecha = request.form['fecha']
        hora = request.form['hora']
        lat = request.form['lat']
        lng = request.form['lng']
        modus = request.form['descripcion']
        organizacion = request.form['organizacion']
        tipo = request.form['tipo']
        tramo = request.form['tramo']
        fuero = request.form['fuero']
        Report.confirmar(carpeta, denuncia, fecha, hora, lat, lng, modus, organizacion, tipo, tramo, num, fuero)
        return 'Registrado correctamente'


@report_blueprint.route('/confirm', methods=['GET', 'POST'])
# @jwt_required
def confirm():
    if request.method == 'POST':
        json = request.get_json()

        try:
            if Report.confirm_report(json):
                return jsonify({'msg': "Folio Actualizado"}), 202
        except ReportErrors.ReportError as e:
            return jsonify({'msg': e.message})


@report_blueprint.route('/consult', methods=['POST'])
# @ jwt_required
def status():
    if request.method == 'POST':
        json = request.get_json()
        folio = json["folio"]
        try:
            return jsonify(FolioResponse(Report.all()).folioJsonResponse()), 202
            #if Report.status_folio(folio):
                #return jsonify({'msg': "aceptado"}), 202
        except ReportErrors.ReportError as e:
            return jsonify({"msg": e.message})


@report_blueprint.route('/consult', methods=['GET'])
# @user_decorators.request_login
def consult():
    if request.method == 'GET':
        return jsonify(FolioResponse(Report.all()).folio_response()), 202


@report_blueprint.route('/remove', methods=['POST'])
def delete():
    if request.method == 'POST':
        json = request.get_json()
        folio = json["folio"]
        Report.find_by_id(folio).remove_to_mongo()
        return jsonify({'msg': 'eliminado'})


@report_blueprint.route('/pendiente', methods=['GET'])
def pendiente():
    if request.method == 'GET':
        return jsonify(FolioResponse(Report.all()).pendienteresponse()), 202


@report_blueprint.route('/update', methods=['GET', 'POST'])
def update_status():
    if request.method == 'POST':
        json = request.get_json()
        pass
        return jsonify({"msg": "Folio cambiado"}), 202
    if request.method == 'GET':
        GpsResponse(Folio.all()).gpsJsonResponse()
