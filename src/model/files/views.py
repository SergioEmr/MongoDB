import os
from flask import request, send_file
from flask_restful import Resource
from src.model.files.files import Files

APP_ROOT = os.path.dirname(os.path.abspath(__file__))
base = APP_ROOT.split('\\')[:-2]
IMG_ROOT = base[0] + "\\"
for x in base[1:]:
    IMG_ROOT = IMG_ROOT + x + "\\"
IMG_ROOT = IMG_ROOT + 'image\\'


class RequestFiles(Resource):
    def get(self, model, version, item):
        return send_file(Files.get_resource_version(model, version, item))

    def post(self, model, version, item):
        target = IMG_ROOT + model
        img = request.files.getlist("img")
        return Files.register_model_version(model, version, target, img), 202
