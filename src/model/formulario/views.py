from flask import Blueprint, request, jsonify
from src.model.formulario.formulario import Placas, Colaborador
from src.model.formulario.formulario_response import FormularioResponse

formulario_blueprint = Blueprint('formulario', __name__)


@formulario_blueprint.route('/placas', methods=['POST', 'GET'])
def register_placas():
    if request.method == 'POST':
        placas = request.form['placas']
        marca = request.form['marca']
        modelo = request.form['modelo']
        serie = request.form['serie']
        color = request.form['color']
        tipo = request.form['tipo']
        valor = request.form['valor']
        gps = request.form['gps']
        Placas.registro(placas, marca, modelo, serie, color, tipo, valor, gps)
        return jsonify({"msg": "Placas registradas correctamente"})
    if request.method == 'GET':
        return jsonify(FormularioResponse(Placas.all()).placas_response()), 202


@formulario_blueprint.route('/colaborador', methods=['POST', 'GET'])
def register_colaborador():
    if request.method == 'POST':
        curp = request.form['curp']
        nombres = request.form['nombres']
        paterno = request.form['paterno']
        materno = request.form['materno']
        nacimiento = request.form['nacimiento']
        sexo = request.form['sexo']
        nickname = request.form['nickname']
        Colaborador.registro(curp, nombres, paterno, materno, nacimiento, sexo, nickname)
        return jsonify({"msg":"Colaborador registrado correctamente"})
    if request.method == 'GET':
        return jsonify(FormularioResponse(Colaborador.all()).colaboradores_response()), 202