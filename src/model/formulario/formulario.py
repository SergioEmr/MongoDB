import uuid
from src.common.database import DataBase


class Placas(object):
    def __init__(self, placas, marca, modelo, serie, color, tipo, valor, gps, _id=None):
        self.placas = placas
        self.marca = marca
        self.modelo = modelo
        self.serie = serie
        self.color = color
        self.tipo = tipo
        self.valor = valor
        self.gps = gps
        self._id = uuid.uuid4().hex if _id is None else _id

    @classmethod
    def all(cls):
        return [cls(**x) for x in DataBase.find('Placas', {})]

    def save_to_mongo(self):
        DataBase.insert('Placas', self.json())

    @staticmethod
    def registro(placas, marca, modelo, serie, color, tipo, valor, gps):
        Placas(placas, marca, modelo, serie, color, tipo, valor, gps).save_to_mongo()

    def json(self):
        return {
            "placas": self.placas,
            "marca": self.marca,
            "modelo": self.modelo,
            "serie": self.serie,
            "color": self.color,
            "tipo": self.tipo,
            "valor": self.valor,
            "gps": self.gps
        }

    pass


class Colaborador(object):
    def __init__(self, curp, nombres, paterno, materno, nacimiento, sexo, nickname, folio, _id=None):
        self.curp = curp
        self.nombres = nombres
        self.paterno = paterno
        self.materno = materno
        self.nacimiento = nacimiento
        self.sexo = sexo
        self.nickname = nickname
        self.folio = folio
        self._id = uuid.uuid4().hex if _id is None else _id

    @classmethod
    def all(cls):
        return [cls(**x) for x in DataBase.find('Colaborador', {})]

    def save_to_mongo(self):
        DataBase.insert('Colaborador', self.json())

    @staticmethod
    def registro(curp, nombres, paterno, materno, nacimiento, sexo, nickname):
        Colaborador(curp, nombres, paterno, materno, nacimiento, sexo, nickname, 'espera').save_to_mongo()

    def json(self):
        return {
            "curp": self.curp,
            "nombres": self.nombres,
            "paterno": self.paterno,
            "materno": self.materno,
            "nacimiento": self.nacimiento,
            "sexo": self.sexo,
            "nickname": self.nickname,
            "folio": self.folio
        }
