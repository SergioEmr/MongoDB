class FormularioResponse(object):
    def __init__(self, data):
        self.data = data

    def colaboradores_response(self):
        response = {}
        for x in range(len(self.data)):
            response.update({self.data[x].curp: {"nombres": self.data[x].nombres, "paterno": self.data[x].paterno,
                                                 "materno": self.data[x].materno, "nacimiento": self.data[x].nacimiento,
                                                 "sexo": self.data[x].sexo, "nickname": self.data[x].nickname,
                                                 "folio": self.data[x].folio}})
        return response

    def placas_response(self):
        response = {}
        for x in range(len(self.data)):
            response.update({self.data[x].placas: {"modelo": self.data[x].modelo, "serie": self.data[x].serie,
                                                   "color": self.data[x].color, "tipo": self.data[x].tipo,
                                                   "valor": self.data[x].valor}})
        return response
