from flask import Blueprint, request, jsonify
from src.model.alerts.alert import Alert
from src.model.reports.report import Report
from src.model.alerts.alert_response import AlertResponse
import src.model.users.errors as UserErrors
import src.model.users.decorators as user_decorators
from flask_jwt_extended import (JWTManager, jwt_required)

alert_blueprint = Blueprint('alerts', __name__)


@alert_blueprint.route('/alert', methods=['POST'])
# @ jwt_required
def alert():
    json = request.get_json()
    lat = json["lat"]
    lng = json["lng"]
    status = json["status"]
    tokendevice = json["tokendevice"]
    fecha = json["fecha"]
    hora = json["hora"]
    try:
        return jsonify(Alert.save_alert(lat, lng, status, tokendevice, fecha, hora)), 202
    except UserErrors.UserError as e:
        return jsonify({"msg": e.message})


@alert_blueprint.route('/consult=<string:user>', methods=['GET'])
# @user_decorators.request_login
def consult(user):
    if request.method == 'GET':
        if user == 'u':
            return jsonify(AlertResponse(Report.all()).globalAlert()), 202
        if user == 'ig':
            return jsonify(AlertResponse(Report.all()).alert_json_response()), 202


@alert_blueprint.route('/alerta', methods=['POST'])
def alerta_pagina():
    json = request.get_json()
    fecha = json['fecha']
    hora = json['hora']
    folio = json['folio']
    lat = json['lat']
    lng = json['lng']
    ubicacion = json['ubicacion']
    Alert.alerta(fecha, hora, folio, lat, lng, ubicacion)
    return "Alerta enviada correctamente"


@alert_blueprint.route('/alert2', methods=['POST'])
def alerta():
    if request.method == 'POST':
        json = request.get_json()
        lat = json['lat']
        lng = json['lng']
        status = "conectado"
        folio = json['folio']
        fecha = json['fecha']
        hora = json['Hora']
        try:
            return jsonify(Alert.save_alert(lat, lng, status, folio, fecha, hora)), 202
        except UserErrors.UserError as e:
            return jsonify({"msg": e.message})
