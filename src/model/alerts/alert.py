import uuid
from src.common.database import DataBase
import src.model.reports.constants as ReportConstants
import src.model.users.errors as UserErrors
import src.model.users.constants as UserConstants
from src.model.reports.report import Report


class Alert(object):
    def __init__(self, lat, lon, status, tokendevice, curp, fecha, hora, _id=None):
        self.lat = lat
        self.lon = lon
        self.status = status
        self.tokendevice = tokendevice
        self.curp = curp
        self.fecha = fecha
        self.hora = hora
        self._id = uuid.uuid4().hex if _id is None else _id

    @staticmethod
    def alerta(fecha, hora, folio, lat, lng, ubicacion):
        pagina = DataBase.find_one('Alerts', {'folio': folio})
        Report('espera', 'espera', pagina['carga'], pagina['conductor'], pagina['copiloto'], pagina['curp1'],
               pagina['curp2'], 'espera', pagina['efectivo'], pagina['empresa'], 'Ciudad de México', fecha,
               pagina['fllegada'], pagina['hllegada'], pagina['fsalida'], pagina['hsalida'], pagina['giro'], hora,
               lat, lng, pagina['merca'], pagina['modelo'], 'espera', 'Benito Juarez',
               'espera', pagina['peso'], pagina['placas'], pagina['ruta'], 'espera', ubicacion,
               pagina['tvehiculo'], folio, 'alerta', 'espera').update_to_mongo()

    @staticmethod
    def save_alert(lat, lon, status, tokendevice, fecha, hora):
        user = DataBase.find_one(ReportConstants.COLLECTION_USERS, {"alertapp": tokendevice})
        usuario = DataBase.find_one(UserConstants.COLLECTION_PRIVATE, {"folio": tokendevice})
        pagina = DataBase.find_one('Alerts', {'curp1': 'WVMZ1AJKO3POK268W2'})

        #if user is None:
        #    raise UserErrors.UserNotExistError("usuario invalido")

        #curp = DataBase.find_one(ReportConstants.COLLECTION_PRIVATE, {"email": user["email"]})
        #if curp is None:
        #   raise UserErrors.CurpNotExistError("curp invalido")

        Report('espera', 'espera', pagina['carga'], pagina['conductor'], pagina['copiloto'], pagina['curp1'],
               pagina['curp2'], 'espera', pagina['efectivo'], pagina['empresa'], 'Ciudad de México', fecha,
               pagina['fllegada'], pagina['hllegada'], pagina['fsalida'], pagina['hsalida'], pagina['giro'], hora,
               pagina['lat'], pagina['lng'], pagina['merca'], pagina['modelo'], 'espera', 'Benito Juarez',
               'espera', pagina['peso'], pagina['placas'], pagina['ruta'], 'espera', 'Espera',
               pagina['tvehiculo'], pagina['folio'], 'alerta', 'espera').update_to_mongo()
        print(DataBase.find_one(ReportConstants.COLLECTION_ALERTS, {"tokendevice": tokendevice}))
        return {"msg": "enviado correctamente"}

    def save_to_mongo(self):
        DataBase.insert(ReportConstants.COLLECTION_ALERTS, self.json())

    def update_to_mongo(self):
        DataBase.update(ReportConstants.COLLECTION_ALERTS, {ReportConstants.TOKENDEVICE: self.tokendevice}, self.json())

    @classmethod
    def all(cls):
        return [cls(**x) for x in DataBase.find(ReportConstants.COLLECTION_ALERTS, {})]

    def json(self):
        return {
            ReportConstants.LAT: self.lat,
            ReportConstants.LON: self.lon,
            ReportConstants.STATUS: self.status,
            ReportConstants.TOKENDEVICE: self.tokendevice,
            ReportConstants.CURP: self.curp,
            ReportConstants.FECHA: self.fecha,
            ReportConstants.HORA: self.hora
        }
