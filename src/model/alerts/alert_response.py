class AlertResponse(object):
    def __init__(self, data):
        self.data = data

    def alert_json_response(self):
        crs = {"crs": {"properties": {"name": "urn:ogc:def:crs:OGC:1.3:CRS84"}, "type": "name"}}
        feature1 = {"features": [{"geometry": {"coordinates": [], "type": "Point"},
                                  "properties": {"folio": "Folio",
                                                 "placas": "camion", "carga": "carga", "peso": 2000,
                                                 "fecha": "20/11/2018", "hora": "12:20:00",
                                                 "alerta": "20/11/2018 11:50:00",
                                                 "entidad": "entidad", "municipio": "mun",
                                                 "latlng": [-114.780623, 32.475760], "ruta": "ruta",
                                                 "tramo": "Carretera México Pachuca km 17", "fsalida": "10/13/2019",
                                                 "hsalida": "10:20", "fllegada": "10/30/2018", "hllegada": "13:00",
                                                 "conductor": "conductor", "copiloto": "copiloto", "curp": "CURP",
                                                 "merca": "merca", "efectivo": "2000 dolares y 14000 pesos",
                                                 "status": "alerta"},
                                  "type": "Feature"}]}
        feature2 = {"features": [{"geometry": {"coordinates": [], "type": "Point"},
                                  "properties": {"folio": "Folio",
                                                 "placas": "camion", "modelo": "placa", "carga": "carga", "peso": 2000,
                                                 "fecha": "20/11/2018", "hora": "12:20:00",
                                                 "organizaci": "organizacion", "alerta": "20/11/2018 11:50:00",
                                                 "entidad": "entidad", "municipio": "mun",
                                                 "latlng": [-114.780623, 32.475760], "ruta": "ruta",
                                                 "tramo": "Carretera México Pachuca km 17", "fsalida": "10/13/2019",
                                                 "hsalida": "10:20", "fllegada": "10/30/2018", "hllegada": "13:00",
                                                 "conductor": "conductor", "copiloto": "copiloto", "curp": "CURP",
                                                 "merca": "merca", "efectivo": "2000 dolares y 14000 pesos",
                                                 "tipo": "robo de vehículo", "denuncia": "Nombre Apellido",
                                                 "modus": "Se le cerraron 2 camionetas y amagaron al chofer",
                                                 "vehiculos": "Nissan Frontier roja", "status": "confirmado",
                                                 "fuero": "fuero"},
                                  "type": "Feature"}]}
        response = {"crs": crs["crs"], "features": []}
        iterable = []
        for document in range(len(self.data)):
            if self.data[document].status == 'espera':
                continue
            if self.data[document].status == 'alerta':
                feature1 = {"features": [{"geometry": {"coordinates": [], "type": "Point"},
                                          "properties": {"folio": "Folio",
                                                         "placas": "camion", "carga": "carga", "peso": 2000,
                                                         "fecha": "20/11/2018", "hora": "12:20:00",
                                                         "entidad": "entidad", "municipio": "mun",
                                                         "latlng": [-114.780623, 32.475760], "ruta": "ruta",
                                                         "tramo": "Carretera México Pachuca km 17",
                                                         "fsalida": "10/13/2019",
                                                         "hsalida": "10:20", "fllegada": "10/30/2018",
                                                         "hllegada": "13:00",
                                                         "conductor": "conductor", "copiloto": "copiloto",
                                                         "curp": "CURP", "tvehiculo": "vehiculo",
                                                         "merca": "merca", "efectivo": "2000 dolares y 14000 pesos",
                                                         "status": "alerta"},
                                          "type": "Feature"}]}
                feature1["features"][0]["geometry"]["coordinates"] = [self.data[document].lng, self.data[document].lat]
                feature1["features"][0]["properties"]["folio"] = self.data[document].folio
                feature1["features"][0]["properties"]["tvehiculo"] = self.data[document].tvehiculo
                feature1["features"][0]["properties"]["placas"] = self.data[document].placas
                feature1["features"][0]["properties"]["carga"] = self.data[document].carga
                feature1["features"][0]["properties"]["peso"] = self.data[document].peso
                feature1["features"][0]["properties"]["fecha"] = self.data[document].fecha
                feature1["features"][0]["properties"]["hora"] = self.data[document].hora
                feature1["features"][0]["properties"]["entidad"] = self.data[document].entidad
                feature1["features"][0]["properties"]["municipio"] = self.data[document].municipio
                feature1["features"][0]["properties"]["ruta"] = self.data[document].ruta
                feature1["features"][0]["properties"]["tramo"] = self.data[document].tramo
                feature1["features"][0]["properties"]["fsalida"] = self.data[document].fsalida
                feature1["features"][0]["properties"]["hsalida"] = self.data[document].hsalida
                feature1["features"][0]["properties"]["fllegada"] = self.data[document].fllegada
                feature1["features"][0]["properties"]["hllegada"] = self.data[document].hllegada
                feature1["features"][0]["properties"]["conductor"] = self.data[document].conductor
                feature1["features"][0]["properties"]["copiloto"] = self.data[document].copiloto
                feature1["features"][0]["properties"]["curp"] = self.data[document].curp1
                feature1["features"][0]["properties"]["merca"] = self.data[document].merca
                feature1["features"][0]["properties"]["efectivo"] = self.data[document].efectivo
                feature1["features"][0]["properties"]["latlng"] = [self.data[document].lng, self.data[document].lat]
                iterable.append(*feature1["features"])
            if self.data[document].status == 'confirmado':
                feature2 = {"features": [{"geometry": {"coordinates": [], "type": "Point"},
                                          "properties": {"folio": "Folio", "tvehiculo": "vehiculo",
                                                         "placas": "camion", "modelo": "placa", "carga": "carga",
                                                         "peso": 2000,
                                                         "fecha": "20/11/2018", "hora": "12:20:00",
                                                         "organizaci": "organizacion", "alerta": "20/11/2018 11:50:00",
                                                         "entidad": "entidad", "municipio": "mun",
                                                         "latlng": [-114.780623, 32.475760], "ruta": "ruta",
                                                         "tramo": "Carretera México Pachuca km 17",
                                                         "fsalida": "10/13/2019",
                                                         "hsalida": "10:20", "fllegada": "10/30/2018",
                                                         "hllegada": "13:00",
                                                         "conductor": "conductor", "copiloto": "copiloto",
                                                         "curp": "CURP",
                                                         "merca": "merca", "efectivo": "2000 dolares y 14000 pesos",
                                                         "tipo": "robo de vehículo", "denuncia": "Nombre Apellido",
                                                         "modus": "Se le cerraron 2 camionetas y amagaron al chofer",
                                                         "vehiculos": "Nissan Frontier roja", "status": "confirmado",
                                                         "fuero": "fuero"},
                                          "type": "Feature"}]}
                feature2["features"][0]["geometry"]["coordinates"] = [self.data[document].lng, self.data[document].lat]
                feature2["features"][0]["properties"]["folio"] = self.data[document].folio
                feature2["features"][0]["properties"]["tvehiculo"] = self.data[document].tvehiculo
                feature2["features"][0]["properties"]["placas"] = self.data[document].placas
                feature2["features"][0]["properties"]["modelo"] = self.data[document].modelo
                feature2["features"][0]["properties"]["carga"] = self.data[document].carga
                feature2["features"][0]["properties"]["peso"] = self.data[document].peso
                feature2["features"][0]["properties"]["fecha"] = self.data[document].fecha
                feature2["features"][0]["properties"]["hora"] = self.data[document].hora
                feature2["features"][0]["properties"]["organizaci"] = self.data[document].organizaci
                feature2["features"][0]["properties"]["alerta"] = self.data[document].alerta
                feature2["features"][0]["properties"]["entidad"] = self.data[document].entidad
                feature2["features"][0]["properties"]["municipio"] = self.data[document].municipio
                feature2["features"][0]["properties"]["ruta"] = self.data[document].ruta
                feature2["features"][0]["properties"]["tramo"] = self.data[document].tramo
                feature2["features"][0]["properties"]["fsalida"] = self.data[document].fsalida
                feature2["features"][0]["properties"]["hsalida"] = self.data[document].hsalida
                feature2["features"][0]["properties"]["fllegada"] = self.data[document].fllegada
                feature2["features"][0]["properties"]["hllegada"] = self.data[document].hllegada
                feature2["features"][0]["properties"]["conductor"] = self.data[document].conductor
                feature2["features"][0]["properties"]["copiloto"] = self.data[document].copiloto
                feature2["features"][0]["properties"]["curp"] = self.data[document].curp1
                feature2["features"][0]["properties"]["merca"] = self.data[document].merca
                feature2["features"][0]["properties"]["efectivo"] = self.data[document].efectivo
                feature2["features"][0]["properties"]["tipo"] = self.data[document].tipo
                feature2["features"][0]["properties"]["denuncia"] = self.data[document].denuncia
                feature2["features"][0]["properties"]["averiguaci"] = self.data[document].averiguaci
                feature2["features"][0]["properties"]["modus"] = self.data[document].modus
                feature2["features"][0]["properties"]["status"] = self.data[document].status
                feature2["features"][0]["properties"]["latlng"] = [self.data[document].lng, self.data[document].lat]
                feature2["features"][0]["properties"]["fuero"] = self.data[document].fuero
                iterable.append(*feature2["features"])
        response["features"] = iterable
        return response

    def globalAlert(self):
        crs = {"crs": {"properties": {"name": "urn:ogc:def:crs:OGC:1.3:CRS84"}, "type": "name"}}
        feature1 = {"features": [{"geometry": {"coordinates": [], "type": "Point"},
                                  "properties": {"folio": "Folio", "empresa": "espera", "tvehiculo": "transportista",
                                                 "placas": "camion", "modelo": "placa", "carga": "carga", "peso": 2000,
                                                 "fecha": "20/11/2018", "hora": "12:20:00", "giro": "giro",
                                                 "organizaci": "organizacion", "entidad": "entidad", "municipio": "mun",
                                                 "latlng": [-114.780623, 32.475760], "ruta": "ruta",
                                                 "tramo": "Carretera México Pachuca km 17", "fsalida": "10/13/2019",
                                                 "hsalida": "10:20", "fllegada": "10/30/2018", "hllegada": "13:00",
                                                 "conductor": "conductor", "copiloto": "copiloto", "curp": "CURP",
                                                 "merca": "merca", "efectivo": "2000 dolares y 14000 pesos",
                                                 "status": "alerta"},
                                  "type": "Feature"}]}
        feature2 = {"features": [{"geometry": {"coordinates": [], "type": "Point"},
                                  "properties": {"folio": "Folio", "empresa": "espera", "tvehiculo": "transportista",
                                                 "placas": "camion", "modelo": "placa", "carga": "carga", "peso": 2000,
                                                 "fecha": "20/11/2018", "hora": "12:20:00", "giro": "giro",
                                                 "organizaci": "organizacion", "alerta": "20/11/2018 11:50:00",
                                                 "entidad": "entidad", "municipio": "mun",
                                                 "latlng": [-114.780623, 32.475760], "ruta": "ruta",
                                                 "tramo": "Carretera México Pachuca km 17", "fsalida": "10/13/2019",
                                                 "hsalida": "10:20", "fllegada": "10/30/2018", "hllegada": "13:00",
                                                 "conductor": "conductor", "copiloto": "copiloto", "curp": "CURP",
                                                 "merca": "merca", "efectivo": "2000 dolares y 14000 pesos",
                                                 "tipo": "robo de vehículo", "denuncia": "Nombre Apellido",
                                                 "modus": "Se le cerraron 2 camionetas y amagaron al chofer",
                                                 "vehiculos": "Nissan Frontier roja", "status": "confirmado",
                                                 "fuero": "fuero"},
                                  "type": "Feature"}]}
        response = {"crs": crs["crs"], "features": []}
        iterable = []
        for document in range(len(self.data)):
            if self.data[document].status == 'espera':
                continue
            if self.data[document].status == 'alerta':
                feature1 = {"features": [{"geometry": {"coordinates": [], "type": "Point"},
                                          "properties": {"folio": "Folio", "empresa": "espera",
                                                         "tvehiculo": "transportista",
                                                         "placas": "camion", "modelo": "placa", "carga": "carga",
                                                         "peso": 2000,
                                                         "fecha": "20/11/2018", "hora": "12:20:00", "giro": "giro",
                                                         "organizaci": "organizacion", "entidad": "entidad",
                                                         "municipio": "mun",
                                                         "latlng": [-114.780623, 32.475760], "ruta": "ruta",
                                                         "tramo": "Carretera México Pachuca km 17",
                                                         "fsalida": "10/13/2019",
                                                         "hsalida": "10:20", "fllegada": "10/30/2018",
                                                         "hllegada": "13:00",
                                                         "conductor": "conductor", "copiloto": "copiloto",
                                                         "curp": "CURP",
                                                         "merca": "merca", "efectivo": "2000 dolares y 14000 pesos",
                                                         "status": "alerta"},
                                          "type": "Feature"}]}
                feature1["features"][0]["geometry"]["coordinates"] = [self.data[document].lng, self.data[document].lat]
                feature1["features"][0]["properties"]["folio"] = self.data[document].folio
                feature1["features"][0]["properties"]["empresa"] = self.data[document].empresa
                feature1["features"][0]["properties"]["tvehiculo"] = self.data[document].tvehiculo
                feature1["features"][0]["properties"]["placas"] = self.data[document].placas
                feature1["features"][0]["properties"]["modelo"] = self.data[document].modelo
                feature1["features"][0]["properties"]["carga"] = self.data[document].carga
                feature1["features"][0]["properties"]["peso"] = self.data[document].peso
                feature1["features"][0]["properties"]["fecha"] = self.data[document].fecha
                feature1["features"][0]["properties"]["hora"] = self.data[document].hora
                feature1["features"][0]["properties"]["giro"] = self.data[document].giro
                feature1["features"][0]["properties"]["entidad"] = self.data[document].entidad
                feature1["features"][0]["properties"]["municipio"] = self.data[document].municipio
                feature1["features"][0]["properties"]["ruta"] = self.data[document].ruta
                feature1["features"][0]["properties"]["tramo"] = self.data[document].tramo
                feature1["features"][0]["properties"]["fsalida"] = self.data[document].fsalida
                feature1["features"][0]["properties"]["hsalida"] = self.data[document].hsalida
                feature1["features"][0]["properties"]["fllegada"] = self.data[document].fllegada
                feature1["features"][0]["properties"]["hllegada"] = self.data[document].hllegada
                feature1["features"][0]["properties"]["conductor"] = self.data[document].conductor
                feature1["features"][0]["properties"]["copiloto"] = self.data[document].copiloto
                feature1["features"][0]["properties"]["curp"] = self.data[document].curp1
                feature1["features"][0]["properties"]["merca"] = self.data[document].merca
                feature1["features"][0]["properties"]["efectivo"] = self.data[document].efectivo
                feature1["features"][0]["properties"]["status"] = self.data[document].status
                feature1["features"][0]["properties"]["latlng"] = [self.data[document].lng, self.data[document].lat]
                iterable.append(*feature1["features"])
            if self.data[document].status == 'confirmado':
                feature2 = {"features": [{"geometry": {"coordinates": [], "type": "Point"},
                                          "properties": {"folio": "Folio", "empresa": "espera",
                                                         "tvehiculo": "transportista",
                                                         "placas": "camion", "modelo": "placa", "carga": "carga",
                                                         "peso": 2000,
                                                         "fecha": "20/11/2018", "hora": "12:20:00", "giro": "giro",
                                                         "organizaci": "organizacion", "alerta": "20/11/2018 11:50:00",
                                                         "entidad": "entidad", "municipio": "mun",
                                                         "latlng": [-114.780623, 32.475760], "ruta": "ruta",
                                                         "tramo": "Carretera México Pachuca km 17",
                                                         "fsalida": "10/13/2019",
                                                         "hsalida": "10:20", "fllegada": "10/30/2018",
                                                         "hllegada": "13:00",
                                                         "conductor": "conductor", "copiloto": "copiloto",
                                                         "curp": "CURP",
                                                         "merca": "merca", "efectivo": "2000 dolares y 14000 pesos",
                                                         "tipo": "robo de vehículo", "denuncia": "Nombre Apellido",
                                                         "modus": "Se le cerraron 2 camionetas y amagaron al chofer",
                                                         "vehiculos": "Nissan Frontier roja", "status": "confirmado",
                                                         "fuero": "fuero"},
                                          "type": "Feature"}]}
                feature2["features"][0]["geometry"]["coordinates"] = [self.data[document].lng, self.data[document].lat]
                feature2["features"][0]["properties"]["folio"] = self.data[document].folio
                feature2["features"][0]["properties"]["empresa"] = self.data[document].empresa
                feature2["features"][0]["properties"]["tvehiculo"] = self.data[document].tvehiculo
                feature2["features"][0]["properties"]["placas"] = self.data[document].placas
                feature2["features"][0]["properties"]["modelo"] = self.data[document].modelo
                feature2["features"][0]["properties"]["carga"] = self.data[document].carga
                feature2["features"][0]["properties"]["peso"] = self.data[document].peso
                feature2["features"][0]["properties"]["fecha"] = self.data[document].fecha
                feature2["features"][0]["properties"]["hora"] = self.data[document].hora
                feature2["features"][0]["properties"]["giro"] = self.data[document].giro
                feature2["features"][0]["properties"]["organizaci"] = self.data[document].organizaci
                feature2["features"][0]["properties"]["alerta"] = self.data[document].alerta
                feature2["features"][0]["properties"]["entidad"] = self.data[document].entidad
                feature2["features"][0]["properties"]["municipio"] = self.data[document].municipio
                feature2["features"][0]["properties"]["ruta"] = self.data[document].ruta
                feature2["features"][0]["properties"]["tramo"] = self.data[document].tramo
                feature2["features"][0]["properties"]["fsalida"] = self.data[document].fsalida
                feature2["features"][0]["properties"]["hsalida"] = self.data[document].hsalida
                feature2["features"][0]["properties"]["fllegada"] = self.data[document].fllegada
                feature2["features"][0]["properties"]["hllegada"] = self.data[document].hllegada
                feature2["features"][0]["properties"]["conductor"] = self.data[document].conductor
                feature2["features"][0]["properties"]["copiloto"] = self.data[document].copiloto
                feature2["features"][0]["properties"]["curp"] = self.data[document].curp1
                feature2["features"][0]["properties"]["merca"] = self.data[document].merca
                feature2["features"][0]["properties"]["efectivo"] = self.data[document].efectivo
                feature2["features"][0]["properties"]["tipo"] = self.data[document].tipo
                feature2["features"][0]["properties"]["denuncia"] = self.data[document].denuncia
                feature2["features"][0]["properties"]["averiguaci"] = self.data[document].averiguaci
                feature2["features"][0]["properties"]["modus"] = self.data[document].modus
                feature2["features"][0]["properties"]["status"] = self.data[document].status
                feature2["features"][0]["properties"]["latlng"] = [self.data[document].lng, self.data[document].lat]
                feature2["features"][0]["properties"]["fuero"] = self.data[document].fuero
                iterable.append(*feature2["features"])
        response["features"] = iterable
        return response
