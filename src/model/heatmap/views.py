from flask import Blueprint, request, jsonify
from src.model.heatmap.heatmap_response import HeatmapResponse
from src.model.heatmap.heatmap import Heatmap
import src.model.users.errors as UserErrors

heatmap_blueprint = Blueprint('heatmap', __name__)


@heatmap_blueprint.route('/data=<string:user>', methods=['GET'])
def geojson(user):
    if request.method == 'GET':
        if user == 'u':
            return jsonify(HeatmapResponse(Heatmap.all()).globalResponse())
        if user == 'ig':
            return jsonify(HeatmapResponse(Heatmap.all()).igResponse())


@heatmap_blueprint.route('/register', methods=['POST'])
def register():
    if request.method == 'POST':
        json = request.get_json()
        print('consultado')
        return jsonify(Heatmap.registro(json['entidad'], json['municipio'], json['lat'], json['lng'], json['fecha'],
                                        json['fsalida'], json['fllegada'], json['hora'], json['hsalida'],
                                        json['hllegada'],
                                        json['placas'], json['modelo'], json['tvehiculo'], json['empresa'],
                                        json['carga'],
                                        json['peso'], json['giro'], json['organizaci'], json['alerta'], json['ruta'],
                                        json['tramo'], json['conductor'], json['copiloto'], json['curp'], json['merca'],
                                        json['efectivo'], json['tipo'], json['denuncia'], json['averiguaci'],
                                        json['modus'],
                                        json['folio']))
