class HeatmapResponse(object):
    def __init__(self, data):
        self.data = data

    def globalResponse(self):
        crs = {"crs": {"properties": {"name": "urn:ogc:def:crs:OGC:1.3:CRS84"}, "type": "name"}}
        feature = {"features": [{"geometry": {"coordinates": [], "type": "Point"},
                                 "properties": {"folio": "Folio", "empresa": "espera", "tvehiculo": "transportista",
                                                "placas": "camion", "modelo": "placa", "carga": "carga", "peso": 2000,
                                                "fecha": "20/11/2018", "hora": "12:20:00", "giro": "giro",
                                                "organizaci": "organizacion", "alerta": "20/11/2018 11:50:00",
                                                "entidad": "entidad", "municipio": "mun",
                                                "latlng": [-114.780623, 32.475760], "ruta": "ruta",
                                                "tramo": "Carretera México Pachuca km 17", "fsalida": "10/13/2019",
                                                "hsalida": "10:20", "fllegada": "10/30/2018", "hllegada": "13:00",
                                                "conductor": "conductor", "copiloto": "copiloto", "curp": "CURP",
                                                "merca": "merca", "efectivo": "2000 dolares y 14000 pesos",
                                                "tipo": "robo de vehículo", "denuncia": "Nombre Apellido",
                                                "averiguacion": "AFUO023-QR8A",
                                                "modus": "Se le cerraron 2 camionetas y amagaron al chofer",
                                                "vehiculos": "Nissan Frontier roja"},
                                 "type": "Feature"}]}
        response = {"crs": crs["crs"], "features": feature["features"]}
        iterable = []
        for document in range(len(self.data)):
            if self.data[document].lat == "espera":
                pass
            else:
                feature = {"features": [{"geometry": {"coordinates": [], "type": "Point"},
                                         "properties": {"folio": "Folio", "empresa": "espera",
                                                        "tvehiculo": "transportista",
                                                        "placas": "camion", "modelo": "placa", "carga": "carga",
                                                        "peso": 2000,
                                                        "fecha": "20/11/2018", "hora": "12:20:00", "giro": "giro",
                                                        "organizaci": "organizacion", "alerta": "20/11/2018 11:50:00",
                                                        "entidad": "entidad", "municipio": "mun",
                                                        "latlng": [-114.780623, 32.475760], "ruta": "ruta",
                                                        "tramo": "Carretera México Pachuca km 17",
                                                        "fsalida": "10/13/2019",
                                                        "hsalida": "10:20", "fllegada": "10/30/2018",
                                                        "hllegada": "13:00",
                                                        "conductor": "conductor", "copiloto": "copiloto",
                                                        "curp": "CURP",
                                                        "merca": "merca", "efectivo": "2000 dolares y 14000 pesos",
                                                        "tipo": "robo de vehículo", "denuncia": "Nombre Apellido",
                                                        "averiguacion": "AFUO023-QR8A",
                                                        "modus": "Se le cerraron 2 camionetas y amagaron al chofer",
                                                        "vehiculos": "Nissan Frontier roja"},
                                         "type": "Feature"}]}
                response = {"crs": crs["crs"], "features": feature["features"]}
                feature["features"][0]["geometry"]["coordinates"] = [self.data[document].lng,
                                                                     self.data[document].lat]
                feature["features"][0]["properties"]["folio"] = self.data[document].folio
                feature["features"][0]["properties"]["empresa"] = self.data[document].empresa
                feature["features"][0]["properties"]["tvehiculo"] = self.data[document].tvehiculo
                feature["features"][0]["properties"]["placas"] = self.data[document].placas
                feature["features"][0]["properties"]["modelo"] = self.data[document].modelo
                feature["features"][0]["properties"]["carga"] = self.data[document].carga
                feature["features"][0]["properties"]["peso"] = self.data[document].peso
                feature["features"][0]["properties"]["fecha"] = self.data[document].fecha
                feature["features"][0]["properties"]["hora"] = self.data[document].hora
                feature["features"][0]["properties"]["giro"] = self.data[document].giro
                feature["features"][0]["properties"]["organizaci"] = self.data[document].organizaci
                feature["features"][0]["properties"]["alerta"] = self.data[document].alerta
                feature["features"][0]["properties"]["entidad"] = self.data[document].entidad
                feature["features"][0]["properties"]["municipio"] = self.data[document].municipio
                feature["features"][0]["properties"]["ruta"] = self.data[document].ruta
                feature["features"][0]["properties"]["tramo"] = self.data[document].tramo
                feature["features"][0]["properties"]["fsalida"] = self.data[document].fsalida
                feature["features"][0]["properties"]["hsalida"] = self.data[document].hsalida
                feature["features"][0]["properties"]["fllegada"] = self.data[document].fllegada
                feature["features"][0]["properties"]["hllegada"] = self.data[document].hllegada
                feature["features"][0]["properties"]["conductor"] = self.data[document].conductor
                feature["features"][0]["properties"]["copiloto"] = self.data[document].copiloto
                feature["features"][0]["properties"]["curp"] = self.data[document].curp
                feature["features"][0]["properties"]["merca"] = self.data[document].merca
                feature["features"][0]["properties"]["efectivo"] = self.data[document].efectivo
                feature["features"][0]["properties"]["tipo"] = self.data[document].tipo
                feature["features"][0]["properties"]["denuncia"] = self.data[document].denuncia
                feature["features"][0]["properties"]["averiguaci"] = self.data[document].averiguaci
                feature["features"][0]["properties"]["modus"] = self.data[document].modus
                feature["features"][0]["properties"]["latlng"] = [self.data[document].lng, self.data[document].lat]
                iterable.append(*feature["features"])
        response["features"] = iterable
        return response

    def igResponse(self):
        crs = {"crs": {"properties": {"name": "urn:ogc:def:crs:OGC:1.3:CRS84"}, "type": "name"}}
        feature1 = {"features": [{"geometry": {"coordinates": [], "type": "Point"},
                                  "properties": {"folio": "Folio",
                                                 "placas": "camion", "modelo": "placa", "carga": "carga", "peso": 2000,
                                                 "fecha": "20/11/2018", "hora": "12:20:00",
                                                 "organizaci": "organizacion", "alerta": "20/11/2018 11:50:00",
                                                 "entidad": "entidad", "municipio": "mun",
                                                 "latlng": [-114.780623, 32.475760], "ruta": "ruta",
                                                 "tramo": "Carretera México Pachuca km 17", "fsalida": "10/13/2019",
                                                 "hsalida": "10:20", "fllegada": "10/30/2018", "hllegada": "13:00",
                                                 "conductor": "conductor", "copiloto": "copiloto", "curp": "CURP",
                                                 "merca": "merca", "efectivo": "2000 dolares y 14000 pesos",
                                                 "tipo": "robo de vehículo", "denuncia": "Nombre Apellido",
                                                 "averiguacion": "AFUO023-QR8A",
                                                 "modus": "Se le cerraron 2 camionetas y amagaron al chofer",
                                                 "vehiculos": "Nissan Frontier roja"},
                                  "type": "Feature"}]}
        feature2 = {"features": [{"geometry": {"coordinates": [], "type": "Point"},
                                  "properties": {"carga": "carga", "fecha": "20/11/2018", "hora": "12:20:00",
                                                 "organizaci": "organizacion", "alerta": "20/11/2018 11:50:00",
                                                 "entidad": "entidad", "municipio": "mun",
                                                 "latlng": [-114.780623, 32.475760],
                                                 "tramo": "Carretera México Pachuca km 17",
                                                 "merca": "merca", "tipo": "robo de vehículo",
                                                 "modus": "Se le cerraron 2 camionetas y amagaron al chofer",
                                                 "vehiculos": "Nissan Frontier roja"},
                                  "type": "Feature"}]}
        response = {"crs": crs["crs"], "features": []}
        iterable = []
        for document in range(2000):
            if document <= 300:
                feature1 = {"features": [{"geometry": {"coordinates": [], "type": "Point"},
                                          "properties": {"folio": "Folio",
                                                         "placas": "camion", "modelo": "placa", "carga": "carga",
                                                         "peso": 2000,
                                                         "fecha": "20/11/2018", "hora": "12:20:00",
                                                         "organizaci": "organizacion", "alerta": "20/11/2018 11:50:00",
                                                         "entidad": "entidad", "municipio": "mun",
                                                         "latlng": [-114.780623, 32.475760], "ruta": "ruta",
                                                         "tramo": "Carretera México Pachuca km 17",
                                                         "fsalida": "10/13/2019",
                                                         "hsalida": "10:20", "fllegada": "10/30/2018",
                                                         "hllegada": "13:00",
                                                         "conductor": "conductor", "copiloto": "copiloto",
                                                         "curp": "CURP",
                                                         "merca": "merca", "efectivo": "2000 dolares y 14000 pesos",
                                                         "tipo": "robo de vehículo", "denuncia": "Nombre Apellido",
                                                         "averiguacion": "AFUO023-QR8A",
                                                         "modus": "Se le cerraron 2 camionetas y amagaron al chofer",
                                                         "vehiculos": "Nissan Frontier roja"},
                                          "type": "Feature"}]}
                feature1["features"][0]["geometry"]["coordinates"] = [self.data[document].lng,
                                                                      self.data[document].lat]
                feature1["features"][0]["properties"]["folio"] = self.data[document].folio
                feature1["features"][0]["properties"]["tvehiculo"] = self.data[document].tvehiculo
                feature1["features"][0]["properties"]["placas"] = self.data[document].placas
                feature1["features"][0]["properties"]["modelo"] = self.data[document].modelo
                feature1["features"][0]["properties"]["carga"] = self.data[document].carga
                feature1["features"][0]["properties"]["peso"] = self.data[document].peso
                feature1["features"][0]["properties"]["fecha"] = self.data[document].fecha
                feature1["features"][0]["properties"]["hora"] = self.data[document].hora
                feature1["features"][0]["properties"]["organizaci"] = self.data[document].organizaci
                feature1["features"][0]["properties"]["alerta"] = self.data[document].alerta
                feature1["features"][0]["properties"]["entidad"] = self.data[document].entidad
                feature1["features"][0]["properties"]["municipio"] = self.data[document].municipio
                feature1["features"][0]["properties"]["ruta"] = self.data[document].ruta
                feature1["features"][0]["properties"]["tramo"] = self.data[document].tramo
                feature1["features"][0]["properties"]["fsalida"] = self.data[document].fsalida
                feature1["features"][0]["properties"]["hsalida"] = self.data[document].hsalida
                feature1["features"][0]["properties"]["fllegada"] = self.data[document].fllegada
                feature1["features"][0]["properties"]["hllegada"] = self.data[document].hllegada
                feature1["features"][0]["properties"]["conductor"] = self.data[document].conductor
                feature1["features"][0]["properties"]["copiloto"] = self.data[document].copiloto
                feature1["features"][0]["properties"]["curp"] = self.data[document].curp
                feature1["features"][0]["properties"]["merca"] = self.data[document].merca
                feature1["features"][0]["properties"]["efectivo"] = self.data[document].efectivo
                feature1["features"][0]["properties"]["tipo"] = self.data[document].tipo
                feature1["features"][0]["properties"]["denuncia"] = self.data[document].denuncia
                feature1["features"][0]["properties"]["averiguaci"] = self.data[document].averiguaci
                feature1["features"][0]["properties"]["modus"] = self.data[document].modus
                feature1["features"][0]["properties"]["latlng"] = [self.data[document].lng, self.data[document].lat]
                iterable.append(*feature1["features"])
            else:
                feature2 = {"features": [{"geometry": {"coordinates": [], "type": "Point"},
                                          "properties": {"carga": "carga", "fecha": "20/11/2018", "hora": "12:20:00",
                                                         "organizaci": "organizacion", "alerta": "20/11/2018 11:50:00",
                                                         "entidad": "entidad", "municipio": "mun",
                                                         "latlng": [-114.780623, 32.475760],
                                                         "tramo": "Carretera México Pachuca km 17",
                                                         "merca": "merca", "tipo": "robo de vehículo",
                                                         "modus": "Se le cerraron 2 camionetas y amagaron al chofer",
                                                         "vehiculos": "Nissan Frontier roja"},
                                          "type": "Feature"}]}
                feature2["features"][0]["geometry"]["coordinates"] = [self.data[document].lng,
                                                                      self.data[document].lat]
                feature2["features"][0]["properties"]["tvehiculo"] = self.data[document].tvehiculo
                feature2["features"][0]["properties"]["carga"] = self.data[document].carga
                feature2["features"][0]["properties"]["fecha"] = self.data[document].fecha
                feature2["features"][0]["properties"]["hora"] = self.data[document].hora
                feature2["features"][0]["properties"]["organizaci"] = self.data[document].organizaci
                feature2["features"][0]["properties"]["alerta"] = self.data[document].alerta
                feature2["features"][0]["properties"]["entidad"] = self.data[document].entidad
                feature2["features"][0]["properties"]["municipio"] = self.data[document].municipio
                feature2["features"][0]["properties"]["tramo"] = self.data[document].tramo
                feature2["features"][0]["properties"]["merca"] = self.data[document].merca
                feature2["features"][0]["properties"]["tipo"] = self.data[document].tipo
                feature2["features"][0]["properties"]["modus"] = self.data[document].modus
                feature2["features"][0]["properties"]["latlng"] = [self.data[document].lng, self.data[document].lat]
                iterable.append(*feature2["features"])
        response["features"] = iterable
        return response
