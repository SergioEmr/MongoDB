import uuid
from src.common.database import DataBase


class Heatmap(object):
    def __init__(self, alerta, averiguaci, carga, conductor, copiloto, curp, denuncia, efectivo, empresa, entidad,
                 fecha, fllegada, hllegada, fsalida, hsalida, giro, hora, lat, lng, merca, modelo, modus,
                 municipio, organizaci, peso, placas, ruta, tipo, tramo, tvehiculo, folio, _id=None):
        self.alerta = alerta
        self.averiguaci = averiguaci
        self.carga = carga
        self.conductor = conductor
        self.copiloto = copiloto
        self.curp = curp
        self.denuncia = denuncia
        self.efectivo = efectivo
        self.empresa = empresa
        self.entidad = entidad
        self.fecha = fecha
        self.fllegada = fllegada
        self.hllegada = hllegada
        self.fsalida = fsalida
        self.hsalida = hsalida
        self.giro = giro
        self.hora = hora
        self.lat = lat
        self.lng = lng
        self.merca = merca
        self.modelo = modelo
        self.modus = modus
        self.municipio = municipio
        self.organizaci = organizaci
        self.peso = peso
        self.placas = placas
        self.ruta = ruta
        self.tipo = tipo
        self.tramo = tramo
        self.tvehiculo = tvehiculo
        self.folio = folio
        self._id = uuid.uuid4().hex if _id is None else _id

    @classmethod
    def all(cls):
        return [cls(**x) for x in DataBase.find('Heatmap', {})]

    def save_to_mongo(self):
        DataBase.insert('Heatmap', self.json())

    @staticmethod
    def registro(entidad, municipio, lat, lng, fecha, fsalida, fllegada, hora, hsalida, hllegada, placas, modelo,
                 tvehiculo, empresa, carga, peso, giro, organizaci, alerta, ruta, tramo, conductor, copiloto, curp,
                 merca, efectivo, tipo, denuncia, averiguaci, modus, folio):
        Heatmap(alerta, averiguaci, carga, conductor, copiloto, curp, denuncia, efectivo, empresa, entidad, fecha,
                fllegada, hllegada, fsalida, hsalida, giro, hora, lat, lng, merca, modelo, modus, municipio, organizaci,
                peso, placas, ruta, tipo, tramo, tvehiculo, folio).save_to_mongo()
        return {'msg': 'correcto'}

    def json(self):
        return {
            'alerta': self.alerta,
            'averiguaci': self.averiguaci,
            'carga': self.carga,
            'conductor': self.conductor,
            'copiloto': self.copiloto,
            'curp': self.curp,
            'denuncia': self.denuncia,
            'efectivo': self.efectivo,
            'empresa': self.empresa,
            'entidad': self.entidad,
            'fecha': self.fecha,
            'fllegada': self.fllegada,
            'hllegada': self.hllegada,
            'fsalida': self.fsalida,
            'hsalida': self.hsalida,
            'giro': self.giro,
            'hora': self.hora,
            'lat': self.lat,
            'lng': self.lng,
            'merca': self.merca,
            'modelo': self.modelo,
            'modus': self.modus,
            'municipio': self.municipio,
            'organizaci': self.organizaci,
            'peso': self.peso,
            'placas': self.placas,
            'ruta': self.ruta,
            'tipo': self.tipo,
            'tramo': self.tramo,
            'tvehiculo': self.tvehiculo,
            'folio': self.folio
        }
