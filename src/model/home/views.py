from flask import Blueprint, render_template
import src.model.users.decorators as user_decorators

home_blueprint = Blueprint('home', __name__)


@home_blueprint.route('/dashboard')
@user_decorators.request_login
def home():
    return render_template('Home/home.html')


@home_blueprint.route('/088')
@user_decorators.request_login
def luciad():
    return render_template('Canacar/index088.html')


@home_blueprint.route('/ig')
@user_decorators.request_login
def intelgeo():
    return render_template('Canacar/indexA.html')


@home_blueprint.route('/u')
@user_decorators.request_login
def user():
    return render_template('Canacar/indexU.html')


@home_blueprint.route('/s')
@user_decorators.request_login
def supervisor():
    return render_template('Canacar/indexS.html')


@home_blueprint.route('/dev')
@user_decorators.request_login
def index():
    return render_template('Canacar/index.html')
