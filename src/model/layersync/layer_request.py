from flask import request, jsonify
from flask_restful import Resource
from src.model.layersync.layer_model import Layer
from src.model.layersync.layer_response import LayerResponse


# from src.model.layersync.layer_response import


class RequestLayer(Resource):
    def post(self):
        data = request.get_json()
        return jsonify(Layer.register(data))

    def get(self):
        return jsonify(LayerResponse(Layer.all()).layer_response())

    def delete(self):
        data = request.get_json()
        return jsonify(Layer.remove(data))
