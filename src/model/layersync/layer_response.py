class LayerResponse(object):
    def __init__(self, data):
        self.data = data

    def layer_response(self):
        features = {"features": [{
            "id": "id",
            "type": "type",
            "properties": {},
            "geometry": {}
        }]}
        response = {"type": "FeatureCollection", "features": features["features"]}
        iterable = []
        for document in range(len(self.data)):
            features = {"features": [{
                "id": "id",
                "type": "type",
                "properties": {},
                "geometry": {"type": "Point", "coordinates": []}
            }]}
            response = {"type": "FeatureCollection", "features": features["features"]}
            features["features"][0]["geometry"]["coordinates"] = self.data[document].geometry['coordinates']
            features["features"][0]["geometry"]["type"] = self.data[document].geometry["type"]
            features["features"][0]["id"] = self.data[document].id
            features["features"][0]["type"] = self.data[document].type
            features["features"][0]["properties"] = self.data[document].properties
            iterable.append(*features["features"])
        response["features"] = iterable
        return response
