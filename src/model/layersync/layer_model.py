import uuid
from src.common.database import DataBase
import src.model.layersync.constants as const


class Layer(object):
    def __init__(self, id, type, properties, geometry, _id=None):
        self.id = id
        self.type = type
        self.properties = properties
        self.geometry = geometry
        self._id = uuid.uuid4().hex if _id is None else None

    def save_to_mongo(self):
        DataBase.insert(const.COLLECTION_LAYER, self.json())

    def update_to_mongo(self):
        DataBase.update(const.COLLECTION_LAYER, {const.ID: self.id}, self.json())

    @staticmethod
    def register(data):
        layer = DataBase.find_one(const.COLLECTION_LAYER, {const.ID: data[const.ID]})
        if layer is None:
            Layer(data[const.ID], data[const.TYPE], data[const.PROPERTIES], data[const.GEOMETRY]).save_to_mongo()
            return {"msg": "Registrado correctamente"}
        else:
            Layer(data[const.ID], data[const.TYPE], data[const.PROPERTIES], data[const.GEOMETRY]).update_to_mongo()
            return {'msg': "Actualizado correctamente"}

    @staticmethod
    def remove(data):
        layer = DataBase.find_one(const.COLLECTION_LAYER, {const.ID: data[const.ID]})
        if layer is None:
            return {'msg': 'Elemento no encontrado'}
        else:
            DataBase.remove(const.COLLECTION_LAYER, {const.ID: data[const.ID]})
            return {'msg': 'Elemento eliminado'}

    @classmethod
    def all(cls):
        return [cls(**x) for x in DataBase.find(const.COLLECTION_LAYER, {})]

    def json(self):
        return {
            const.ID: self.id,
            const.TYPE: self.type,
            const.PROPERTIES: self.properties,
            const.GEOMETRY: self.geometry
        }
