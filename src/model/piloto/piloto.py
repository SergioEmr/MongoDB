import uuid
import src.model.piloto.constants as PilotoConstants
from src.common.database import DataBase
from pyfcm import FCMNotification
import geocoder


class User(object):
    def __init__(self, tokendevice, user, password, status, supervisor, localities, _id=None):
        self.tokendevice = tokendevice
        self.user = user
        self.password = password
        self.status = status
        self.supervisor = supervisor
        self.localities = localities
        self._id = uuid.uuid4().hex if _id is None else _id

    @staticmethod
    def update_token(user, password, tokendevice):
        """
        Actualiza el Tokendevice del usuario y valida el usuario o contraseña
        :param user: String nombre de usuario
        :param password: String Password
        :param tokendevice: String Token Device
        :return: Directorio de estatus de la consulta
        """
        data_user = DataBase.find_one(PilotoConstants.COLLECTION_PILOTO, {"user": user})

        if data_user is None:
            return {"msg": "Usuario invalido"}
        if data_user["password"] != password:
            return {"msg": "Contraseña incorrecta"}

        User(tokendevice, data_user["user"], data_user["password"], data_user["status"], data_user["supervisor"],
             data_user["localities"]).update_to_mongo()
        return {"msg": "correcto"}

    @staticmethod
    def clear():
        data_users = DataBase.find(PilotoConstants.COLLECTION_PILOTO, {})
        vacia = []
        for x in data_users:
            lista = []
            if len(x["localities"]) > 0:
                for y in range(len(x["localities"])):
                    locality = {"locality": x["localities"][y]["locality"], "checkin": "wait", "checkout": "wait",
                                "status": "wait"}
                    lista.append(locality)
                User(user=x["user"], password=x["password"], tokendevice=x["tokendevice"], status="wait",
                     supervisor=x["supervisor"], localities=lista).update_to_mongo()
            else:
                User(user=x["user"], password=x["password"], tokendevice=x["tokendevice"], status="wait",
                     supervisor=x["supervisor"], localities=vacia).update_to_mongo()

    @staticmethod
    def alert(tokendevice):
        data_admin = DataBase.find_one(PilotoConstants.COLLECTION_PILOTO, {"tokendevice": tokendevice})
        data_user = DataBase.find(PilotoConstants.COLLECTION_PILOTO, {"supervisor": data_admin["user"]})
        lista = []
        for x in data_user:
            if x["status"] == "wait":
                lista.append(x["user"])
        return {"users": lista}

    @staticmethod
    def notification(token, status, location, checkin, checkout):
        """
        Recibe la actualización de los checks de cada usuario y actualiza la base de datos de los usuarios 'piloto'
        :param token: String Token Device del dispositivo cliente
        :param status: String del status para la petición de actualización
        :param location: String de la localidad a actualizar
        :param checkin: String wait
        :param checkout: String Hora en la que se está registrando el checkout
        :return: Diccionario con el status de la petición
        """

        # Token del usuario que hace el cambio de estado
        data_user = DataBase.find_one(PilotoConstants.COLLECTION_PILOTO, {"tokendevice": token})
        data_admin = DataBase.find_one(PilotoConstants.COLLECTION_PILOTO, {"user": data_user["supervisor"]})
        data_users = DataBase.find(PilotoConstants.COLLECTION_PILOTO,
                                   {"supervisor": data_admin["user"]})
        data_locality = DataBase.find_one(PilotoConstants.COLLECTION_LOCALITY,
                                          {"locality": location, "supervisor": data_admin["user"]})
        data_localities = DataBase.find(PilotoConstants.COLLECTION_LOCALITY,
                                        {"supervisor": data_admin["user"]})
        iterable = []
        estado = []
        estados = []
        checks = []
        # print(data_users[0])
        # print(data_locality)
        # print(data_user["supervisor"])
        if data_locality is None:
            return {"msg": "nulo"}
        if status == "checkin":
            User.sendNotification("empleado", "Check in locality: ", location, data_user, data_admin)

        if status == "checkout":
            User.sendNotification("checkin", "Check out location: ", location, data_user, data_admin)

        if status == "leaving":
            User.sendNotification("empleado", "Leaving location: ", location, data_user, data_admin)
            return {"msg": "enviado"}

        print("Piloto: " + token)
        if data_user is None:
            print("None")
        # print("Localities count: " + str(data_localities.count()))
        for document in range(data_localities.count()):
            if data_user["localities"][document]["locality"] == location:
                localities = {"locality": data_user["localities"][document]["locality"],
                              "checkin": checkin if data_user["localities"][document]["checkin"] == "wait" else
                              data_user["localities"][document]["checkin"],
                              "checkout": checkout if (data_user["localities"][document]["checkout"] == "wait") and (
                                  data_user["localities"][document]["checkin"] != "wait") else
                              data_user["localities"][document]["checkout"],
                              "status": "checkout" if (data_user["localities"][document]["checkin"] != "wait") and (
                                  status == "checkout") else "checkin" if (status == "checkin") and (
                                  data_user["localities"][document]["checkin"] == "wait") else
                              data_user["localities"][document]["status"]}

            else:
                localities = {"locality": data_user["localities"][document]["locality"],
                              "checkin": data_user["localities"][document]["checkin"],
                              "checkout": data_user["localities"][document]["checkout"],
                              "status": data_user["localities"][document]["status"]}

            iterable.append(localities)
            estado.append(localities["status"])
        print(estado)
        if ("checkin" in estado) or (all(x in estado for x in ["checkout", "wait"])):
            User(user=data_user["user"], password=data_user["password"], tokendevice=token, status="checkin",
                 supervisor=data_user["supervisor"], localities=iterable).update_to_mongo()
        elif "checkout" in estado:
            User(user=data_user["user"], password=data_user["password"], tokendevice=token, status="checkout",
                 supervisor=data_user["supervisor"], localities=iterable).update_to_mongo()
        else:
            User(user=data_user["user"], password=data_user["password"], tokendevice=token, status="wait",
                 supervisor=data_user["supervisor"], localities=iterable).update_to_mongo()

        user = DataBase.find_one(PilotoConstants.COLLECTION_PILOTO, {"tokendevice": token})
        # print(estado)
        for x in range(data_users.count()):
            if data_users[x]["user"] == data_user["user"]:
                status_list = {"user": data_user["user"], "status": user["status"]}
            else:
                status_list = {"user": data_users[x]["user"], "status": data_users[x]["status"]}
            checks.append(status_list)
            estados.append(status_list["status"])

        print(estados)
        if "checkin" in estados:
            Localities(location, data_locality["radio"], data_locality["lat"], data_locality['lng'], "checkin",
                       user["supervisor"], checks).update_to_mongo()
        elif "checkout" in estados:
            Localities(location, data_locality["radio"], data_locality["lat"], data_locality['lng'], "checkout",
                       user["supervisor"], checks).update_to_mongo()
        else:
            Localities(location, data_locality["radio"], data_locality["lat"], data_locality['lng'], "wait",
                       user["supervisor"], checks).update_to_mongo()

        return {"msg": "enviado"}

    def update_to_mongo(self):
        DataBase.update(PilotoConstants.COLLECTION_PILOTO, {"user": self.user}, self.json())

    @staticmethod
    def sendNotification(icon, message, location, data_user, data_admin):
        push_notification = FCMNotification(api_key=PilotoConstants.APIKEY)
        message_title = "Employee: " + data_user["user"]
        message = message + location.title()
        icon = icon
        tono = "default"
        push_notification.notify_single_device(registration_id=data_admin["tokendevice"], message_title=message_title,
                                               message_body=message, message_icon=icon, sound=tono)

    @classmethod
    def one(cls, user):

        return DataBase.find_one(PilotoConstants.COLLECTION_PILOTO, {"user": user})

    @classmethod
    def select(cls, tokendevice):
        data = DataBase.find_one(PilotoConstants.COLLECTION_PILOTO, {"tokendevice": tokendevice})
        return [cls(**x) for x in DataBase.find(PilotoConstants.COLLECTION_PILOTO, {"supervisor": data["user"]})]

    @classmethod
    def all(cls):
        return [cls(**x) for x in DataBase.find(PilotoConstants.COLLECTION_PILOTO, {})]

    def json(self):
        return {
            "tokendevice": self.tokendevice,
            "user": self.user,
            "password": self.password,
            "status": self.status,
            "supervisor": self.supervisor,
            "localities": self.localities
        }


class Localities(object):
    def __init__(self, locality, radio, lat, lng, status, supervisor, checks, _id=None):
        self.locality = locality
        self.radio = radio
        self.lat = lat
        self.lng = lng
        self.status = status
        self.supervisor = supervisor
        self.checks = checks
        self._id = uuid.uuid4().hex if _id is None else _id

    @staticmethod
    def register_localities_by_text(direccion, radio, tokendevice):
        g = geocoder.google(direccion)
        locality = DataBase.find_one(PilotoConstants.COLLECTION_LOCALITY, {"locality": direccion})
        user = DataBase.find_one(PilotoConstants.COLLECTION_PILOTO, {"tokendevice": tokendevice})
        users = DataBase.find(PilotoConstants.COLLECTION_PILOTO, {})

        if locality is None:
            iterable = []
            for x in range(users.count()):
                usuario = {"user": users[x]["user"], "status": "wait"}
                iterable.append(usuario)
            Localities(direccion.title(), radio, g.latlng[0], g.latlng[1], "wait", user["supervisor"],
                       iterable).save_to_mongo()
            data_localities = DataBase.find(PilotoConstants.COLLECTION_LOCALITY, {})

            for x in users:
                nueva = []
                for y in range(data_localities.count()):
                    locality = {"locality": data_localities[y]["locality"],
                                "checkin": "wait" if data_localities[y]["locality"] == direccion
                                else x["localities"][y]["checkin"],
                                "checkout": "wait" if data_localities[y]["locality"] == direccion
                                else x["localities"][y]["checkout"],
                                "status": "wait" if data_localities[y]["locality"] == direccion
                                else x["localities"][y]["status"]}
                    nueva.append(locality)
                User(user=x["user"], password=x["password"], tokendevice=x["tokendevice"], status="wait",
                     supervisor=x["supervisor"], localities=nueva).update_to_mongo()

        return {"msg": "correcto"}

    @staticmethod
    def test():
        data = DataBase.find(PilotoConstants.COLLECTION_LOCALITY,
                             {"supervisor": {"$regex": "^{}".format("supervisor2")}})
        print(data[0])

    @staticmethod
    def register_location(lat, lng, radio, address, tokendevice):
        """
        Registra la localidad a partir la la latitud, longitud, radio y la dirección asociada
        :param lat: float latitud
        :param lng: float longitud
        :param radio: int radio
        :param address: String dirección
        :param tokendevice String tokendevice del dispositivo que hace la consulta
        :return: Directorio con respuesta
        """

        users = DataBase.find(PilotoConstants.COLLECTION_PILOTO, {})
        # Recibe el Token device del supervisor que está registrando la localidad
        user = DataBase.find_one(PilotoConstants.COLLECTION_PILOTO, {"tokendevice": tokendevice})
        # Busca al único cleaner que tiene asignado ese supervisor
        cleaner = DataBase.find_one(PilotoConstants.COLLECTION_PILOTO, {"supervisor": user["user"]})

        if address == "espera":
            g = geocoder.google([lat, lng], method="reverse")
            address = g.country_long + " " + g.state + " " + g.city + ", " + str(
                g.postal) + " " + g.street_long + ", " + g.housenumber

        # print(address)
        locality = DataBase.find_one(PilotoConstants.COLLECTION_LOCALITY, {"locality": address})
        location = DataBase.find_one(PilotoConstants.COLLECTION_LOCALITY,
                                     {"locality": address, "supervisor": user["user"]})
        if location is None:
            usuario = {"user": cleaner["user"], "status": "wait"}
            Localities(address, radio, lat, lng, "wait", user["user"], [usuario]).save_to_mongo()

            # Busca las localidades registradas en la cuenta del supervisor del token recibido
            data_location = DataBase.find(PilotoConstants.COLLECTION_LOCALITY, {"supervisor": user["user"]})
            print(data_location.count())
            lista = []
            for x in range(data_location.count()):
                print(x)
                if data_location[x]["locality"] == address:
                    localidad = {"locality": address, "checkin": "wait", "checkout": "wait", "status": "wait"}
                    lista.append(localidad)
                else:
                    localidad = {"locality": cleaner["localities"][x]["locality"],
                                 "checkin": cleaner["localities"][x]["checkin"],
                                 "checkout": cleaner["localities"][x]["checkout"],
                                 "status": cleaner["localities"][x]["status"]}
                    lista.append(localidad)
            User(tokendevice=cleaner["tokendevice"], user=cleaner["user"], password=cleaner["password"],
                 status=cleaner["status"], supervisor=cleaner["supervisor"], localities=lista).update_to_mongo()

        if 1 < 0:
            iterable = []
            for x in range(users.count()):
                usuario = {"user": users[x]["user"], "status": "wait"}
                iterable.append(usuario)

            Localities(address, radio, lat, lng, "wait", user["supervisor"], iterable).save_to_mongo()

            data_localities = DataBase.find(PilotoConstants.COLLECTION_LOCALITY, {})
            for x in range(users.count()):
                nueva = []
                for y in range(data_localities.count()):
                    if data_localities[y]["locality"] == address:
                        locality = {"locality": address, "checkin": "wait", "checkout": "wait", "status": "wait"}
                    else:
                        locality = {"locality": data_localities[y]["locality"],
                                    "checkin": users[x]["localities"][y]["checkin"],
                                    "checkout": users[x]["localities"][y]["checkout"],
                                    "status": users[x]["localities"][y]["status"]}
                    nueva.append(locality)
                User(user=users[x]["user"], password=users[x]["password"], tokendevice=users[x]["tokendevice"],
                     supervisor=users[x]["supervisor"], status="wait", localities=nueva).update_to_mongo()

        return {"msg": "correcto"}

    def save_to_mongo(self):
        DataBase.insert(PilotoConstants.COLLECTION_LOCALITY, self.json())

    def update_to_mongo(self):
        DataBase.update(PilotoConstants.COLLECTION_LOCALITY, {"locality": self.locality, "supervisor": self.supervisor},
                        self.json())

    @classmethod
    def all(cls):
        return [cls(**x) for x in DataBase.find(PilotoConstants.COLLECTION_LOCALITY, {})]

    @classmethod
    def select(cls, tokendevice):
        # Consulta del usuario asignado a ese tokendevice
        data = DataBase.find_one(PilotoConstants.COLLECTION_PILOTO, {"tokendevice": tokendevice})
        lista = []
        # Si el token le pertenece a un supervisor
        if data["supervisor"] == "supervisor":
            return [cls(**x) for x in
                    DataBase.find(PilotoConstants.COLLECTION_LOCALITY, {"supervisor": data["user"]})], lista
        # Si el token le pertenece a un cleaner
        if data["supervisor"] != "supervisor":
            for x in range(len(data["localities"])):
                lista.append(data["localities"][x])

            return [cls(**x) for x in
                    DataBase.find(PilotoConstants.COLLECTION_LOCALITY, {"supervisor": data["supervisor"]})], lista

    def json(self):
        return {
            "locality": self.locality,
            "radio": self.radio,
            "lat": self.lat,
            "lng": self.lng,
            "status": self.status,
            "supervisor": self.supervisor,
            "checks": self.checks
        }


"""class Position(object):
    def __init__(self, tokendevice, lat, lng, status, checkin, checkout, response, _id=None):
        self.tokendevice = tokendevice
        self.lat = lat
        self.lng = lng
        self.status = status
        self.checkin = checkin
        self.checkout = checkout
        self.response = response
        self._id = uuid.uuid4().hex if _id is None else _id

    @staticmethod
    def register_position(tokendevice, lat, lng, status, checkin, checkout):
        data_user = DataBase.find_one(PilotoConstants.COLLECTION_PILOTO, {"tokendevice": tokendevice})
        data_users = DataBase.find(PilotoConstants.COLLECTION_PILOTO, {})
        data_lat = DataBase.find_one(PilotoConstants.COLLECTION_LOCALITY, {"lat": lat, "lng": lng})
        data_position_0 = DataBase.find_one(PilotoConstants.COLLECITON_POSITION, {"tokendevice": tokendevice})
        data_localities = DataBase.find(PilotoConstants.COLLECTION_LOCALITY, {})
        for y in range(len([*data_users])):
            # Encuentra el usuario del tokendevice y su posición en la base de locality
            data = DataBase.find(PilotoConstants.COLLECTION_PILOTO, {})
            if data_user["user"] == data[y]["user"]:
                print(data[y])
                # Si el usuario dentro del arreglo de la DB locality está en espera solo podrá hacer el checkin
                if data[y]["status"] == "espera":
                    v = 0
                    # Actualizar la base de datos de localidades
                    localities = DataBase.find(PilotoConstants.COLLECTION_LOCALITY, {})
                    iterable = []
                    for w in range(len([*data_localities])):
                        print("locality")
                        print(localities[w])
                        if w == y:
                            data_position = DataBase.find_one(PilotoConstants.COLLECITON_POSITION,
                                                              {"tokendevice": tokendevice})
                            a = math.pow(data_position["lat"] - data_position_0["lat"], 2)
                            b = math.pow(data_position["lng"] - data_position_0["lng"], 2)
                            d = math.sqrt((a + b))
                            D = d / PilotoConstants.KM
                            print("Distancia: " + str(D))
                            if D > 100:
                                usuario = {"user": data[w]["user"], "status": "espera"}
                                iterable.append(usuario)
                            if D < 100:
                                usuario = {"user": data[w]["user"], "status": "entrando"}
                                print("localidad")
                                print(localities)
                                iterable.append(usuario)
                            continue
                        iterable.append({"user": data[y]["user"], "status": "espera"})

                    return {"status": "espera"}

        print("marca")
        for x in range(data_localities.count()):
            print(data_localities[x]["status"])
        for x in range(len(data_user["localities"])):
            if data_user["localities"][x]["checkin"] == "espera":
                pass

        if data_user is None:
            return {"status": "error"}
        if data_user is not None:
            Position(tokendevice, lat, lng, status, checkin, checkout, "espera").update_to_mongo()
            if data_position_0 is None:
                return {"status": "init"}

        return {"status": "espera"}

    def save_to_mongo(self):
        DataBase.insert(PilotoConstants.COLLECITON_POSITION, self.json())

    def update_to_mongo(self):
        DataBase.update(PilotoConstants.COLLECITON_POSITION, {"tokendevice": self.tokendevice}, self.json())

    @classmethod
    def all(cls):
        return [cls(**x) for x in DataBase.find(PilotoConstants.COLLECITON_POSITION, {})]

    def json(self):
        return {
            "tokendevice": self.tokendevice,
            "lat": self.lat,
            "lng": self.lng,
            "status": self.status,
            "checkin": self.checkin,
            "checkout": self.checkout,
            "response": self.response
        }"""
