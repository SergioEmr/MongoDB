class PilotoResponse(object):
    def __init__(self, data):
        self.data = data

    def positionJsonResponse(self):
        crs = {"crs": {"properties": {"name": "urn:ogc:def:crs:OGC:1.3:CRS84"}, "type": "name"}}
        feature = {"features": [{"geometry": {"coordinates": [], "type": "Point"},
                                 "properties": {"id": 2, "status": "espera", "checkin": "10/12/2018",
                                                "checkout": "15:34", "token": "token"},
                                 "type": "Feature"}]}
        response = {"crs": crs["crs"], "features": feature["features"]}
        iterable = []
        for document in range(len(self.data)):
            feature = {"features": [{"geometry": {"coordinates": [], "type": "Point"},
                                     "properties": {"id": 2, "status": "espera", "checkin": "10/12/2018",
                                                    "checkout": "15:34", "token": "token"},
                                     "type": "Feature"}]}
            response = {"crs": crs["crs"], "features": feature["features"]}
            feature["features"][0]["geometry"]["coordinates"] = [self.data[document].lat,
                                                                 self.data[document].lng]
            feature["features"][0]["properties"]["id"] = document
            feature["features"][0]["properties"]["token"] = self.data[document].tokendevice
            feature["features"][0]["properties"]["status"] = self.data[document].status
            feature["features"][0]["properties"]["checkin"] = self.data[document].checkin
            feature["features"][0]["properties"]["checkout"] = self.data[document].checkout
            iterable.append(*feature["features"])
        response["features"] = iterable
        return response

    def localityResponse(self):
        iterable = []
        for document in range(len(self.data[0])):
            locality = {"id": document, "locality": self.data[0][document].locality,
                        "radio": self.data[0][document].radio,
                        "status": self.data[0][document].status if len(self.data[1]) == 0 else self.data[1][
                            document]["status"],
                        "lat": self.data[0][document].lat, "lng": self.data[0][document].lng}
            iterable.append(locality)
        return {"localites": iterable}

    def localityGlobalResponse(self):
        iterable = []
        for document in range(len(self.data)):
            locality = {"id": document, "locality": self.data[document].locality,
                        "radio": self.data[document].radio,
                        "status": self.data[document].status,
                        "lat": self.data[document].lat, "lng": self.data[document].lng}
            iterable.append(locality)
        return {"localities": iterable}

    def usersResponse(self):
        iterable = []
        for document in range(len(self.data)):
            user = {"user": self.data[document].user, "status": self.data[document].status}
            iterable.append(user)
        return {"users": iterable}

    def userResponse(self):
        user = {"localities": self.data["localities"]}
        return user
