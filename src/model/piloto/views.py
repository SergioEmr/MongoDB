from flask import Blueprint, request, jsonify
from src.model.piloto.piloto import User, Localities
from src.model.piloto.piloto_response import PilotoResponse

piloto_blueprint = Blueprint('piloto', __name__)


@piloto_blueprint.route('/tokendevice', methods=["POST", "GET"])
def tokendevice():
    if request.method == "POST":
        json = request.get_json()
        status = json["status"]
        locality = json["locality"]
        token = json["tokendevice"]
        checkin = json["checkin"]
        checkout = json["checkout"]
        print("VIEW: " + token)
        return jsonify(User.notification(token, status, locality, checkin, checkout))
    if request.method == "GET":
        User.clear()
        return "correcto"


@piloto_blueprint.route('/login_piloto', methods=["POST"])
def login_piloto():
    json = request.get_json()
    user = json["user"]
    password = json["password"]
    tokendevice = json["tokendevice"]
    ip = request.remote_addr
    print(ip)
    return jsonify(User.update_token(user, password, tokendevice))


@piloto_blueprint.route('/geocoding', methods=["POST"])
def geocoding():
    json = request.get_json()
    direccion = json["direccion"].title()
    radio = json["radio"]
    tokendevice = json["tokendevice"]
    return jsonify(Localities.register_localities_by_text(direccion, radio, tokendevice))


@piloto_blueprint.route('/geocoding_reverse', methods=["POST"])
def geocoding_reverse():
    json = request.get_json()
    lat = json["lat"]
    lng = json["lng"]
    radio = json["radio"]
    address = json["address"]
    tokendevice = json["tokendevice"]
    return jsonify(Localities.register_location(lat, lng, radio, address, tokendevice))


@piloto_blueprint.route('/localities/<string:tokendevice>', methods=["GET", "POST"])
def get_status(tokendevice):
    if request.method == "GET":
        if tokendevice == "global":
            return jsonify(PilotoResponse(Localities.all()).localityGlobalResponse())
        else:
            return jsonify(PilotoResponse(Localities.select(tokendevice)).localityResponse())


@piloto_blueprint.route('/users/<string:tokendevice>', methods=["GET"])
def get_users(tokendevice):
    return jsonify(PilotoResponse(User.select(tokendevice)).usersResponse())


@piloto_blueprint.route('/getUser', methods=['POST'])
def get_user():
    user = request.get_json()["user"]
    return jsonify(PilotoResponse(User.one(user)).userResponse())


@piloto_blueprint.route('/alerts/<string:tokendevice>', methods=["POST"])
def alert(tokendevice):
    return jsonify(User.alert(tokendevice))


@piloto_blueprint.route('/position', methods=["GET", "POST"])
def position():
    if request.method == "POST":
        json = request.get_json()
        tokendevice = json["tokendevice"]
        lat = json["lat"]
        lng = json["lng"]
        status = json["status"]
        checkin = json["checkin"]
        checkout = json["checkout"]
        return "text"

    if request.method == "GET":
        return "text"
