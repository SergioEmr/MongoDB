from passlib.hash import pbkdf2_sha512
import re


class Utils(object):
    @staticmethod
    def email_valid(email):
        email_consistente = re.compile('^[\w-]+.+@+([\w-]+\.)+[\w-]+$')
        return True if email_consistente.match(email) else False

    @staticmethod
    def hash_password(password):
        """
        Cifra la contraseña con sha512
        :param password: The sha512 password del email
        :return: SHA512
        """
        return pbkdf2_sha512.encrypt(password)

    @staticmethod
    def check_hashed_password(password, hashed_password):
        """
        Verifica que la contraseña enviada coincida con la de la base de datos
        Cifra la contraseña del usuario para dismiunir el riesgo
        :param password: sha512-hashed password
        :param hashed_password: pbkdf2_sha512 encrytp password
        :return: True si la contraseña coincide, False si falla
        """
        hashed = pbkdf2_sha512.encrypt(password)
        return pbkdf2_sha512.verify(password, hashed_password)

    @staticmethod
    def check_string(stringsent, database):
        """
        Verifica que ambas cadenas de texto, el enviado y el de la base de datos, sean iguales
        :param stringsent: String de la cadena enviada
        :param database: String de la cadena almacenada en la baes de datos
        :return: True si coincide, False si falla
        """
        return True if stringsent == database else False

    @staticmethod
    def check_token(token):
        return True if (token == "autorizado") or (token == "denegar") else False

    @staticmethod
    def check_app(token, app):
        """
        Verifica que la aplicación haya sido haibilitada
        :param token: String token enviado
        :param app: String nombre de la aplicación
        :return:
        """
        if app == "denegar":
            return "Token invalido"
        return "Token incorrecto" if token != app else token
