import os
import requests

from flask import Flask, request, url_for
from flask_restful import Api, Resource
from flask_cors import CORS
from werkzeug.utils import redirect

from src.model.home.views import home_blueprint
from src.model.users.views import user_blueprint
from src.model.alerts.views import alert_blueprint
from src.model.gps.views import gps_blueprint
from src.model.reports.views import report_blueprint
from src.model.piloto.views import piloto_blueprint
from src.model.heatmap.views import heatmap_blueprint
from src.model.formulario.views import formulario_blueprint
from src.model.cndh.cndh_request import RequestCndh
from src.model.login.login_request import RequestLogin
from src.model.layersync.layer_request import RequestLayer
from src.model.files.views import RequestFiles
from src.common.database import DataBase
from flask_jwt_extended import (JWTManager, jwt_required, create_access_token, create_refresh_token,
                                jwt_refresh_token_required, get_jwt_identity)
from src import test
import random
from twilio.rest import Client
from src.model.users.user import User
from src.common import database

app = Flask(__name__)
cors = CORS(app, resources={r"/*": {"origins": "*"}})
api = Api(app)
app.config['JWT_SECRET_KEY'] = 'intelgeo'
app.secret_key = '234'
jwt = JWTManager(app)


@app.before_first_request
def init_db():
    DataBase.initDatabase()


@app.route('/')
def login():
    return redirect(url_for('users.login_user'))


class bingproxy(Resource):
    def get(self, tipo):
        os.environ['NO_PROXY'] = '127.0.0.1'
        return requests.get('http://127.0.0.1:8072/bingproxy/' + tipo).json()


class createandedit(Resource):
    def get(self):
        os.environ['NO_PROXY'] = '127.0.0.1'
        return requests.get('http://127.0.0.1:8072/createandedit').json()

    def put(self):
        os.environ['NO_PROXY'] = '127.0.0.1'
        json = request.get_json()
        return request.put('http://127.0.0.1:8072/createandedit', data=json).json()

    def delete(self):
        os.environ['NO_PROXY'] = '127.0.0.1'
        return requests.delete('http://127.0.0.1:8072/createandedit').json()


class createandedita(Resource):
    def get(self, tipo):
        os.environ['NO_PROXY'] = '127.0.0.1'
        return requests.get('http://127.0.0.1:8072/createandedit/' + tipo).json()

    def put(self, tipo):
        os.environ['NO_PROXY'] = '127.0.0.1'
        json = request.get_json()
        return request.put('http://127.0.0.1:8072/createandedit/' + tipo, data=json).json()

    def delete(self, tipo):
        os.environ['NO_PROXY'] = '127.0.0.1'
        return requests.delete('http://127.0.0.1:8072/createandedit/' + tipo).json()


api.add_resource(bingproxy, '/bingproxy/<string:tipo>')
api.add_resource(createandedita, '/createandedit/<string:tipo>')
api.add_resource(createandedit, '/createandedit')
api.add_resource(RequestCndh, "/sedena/request")
api.add_resource(RequestLayer, "/layers/sync")
app.register_blueprint(gps_blueprint)
app.register_blueprint(user_blueprint, url_prefix='/users')
app.register_blueprint(home_blueprint, url_prefix='/home')
app.register_blueprint(report_blueprint, url_prefix='/report')
app.register_blueprint(piloto_blueprint, url_prefix='/piloto')
app.register_blueprint(alert_blueprint, url_prefix='/alerts')
app.register_blueprint(heatmap_blueprint, url_prefix='/heatmap')
app.register_blueprint(formulario_blueprint, url_prefix='/formulario')
api.add_resource(RequestFiles, "/resources/<string:model>/<string:version>/<int:item>")
api.add_resource(RequestLogin, "/acces/<string:action>")
# class SMS(Resource):
#    def get(self):
#        msg = {"msg": "SMS enviado"}
#        account = "AC4765020726ecb273c7ef9e819de3be7b"
#        token = "c9a2b43a62b17d20a8e95173c10ce0f8"
#        client = Client(account, token)
#        client.messages.create(to="+525532908004", from_="+17273255678", body="Tu código de seguridad es 7895")
#        return msg, 202


if __name__ == '__main__':
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port, debug=True)
